PROGRAM test_aer_prop

  USE splat_aerpro_ionc, ONLY: create_aerpro_nc
  USE mie_code_module,   ONLY: mie_interface
  USE GC_netcdf_module,  ONLY: netcdf_handle_error
  
  IMPLICIT NONE
  
  INCLUDE 'netcdf.inc'
  
  ! Maximum number of moments and Greek Matrices parameters
  INTEGER(KIND=4) :: nmom, ngk
  
  !!! To hold input input file information !!!
  ! Output file and refractive index input file.
  ! ofilename will not be needed in the final version
  CHARACTER(LEN=512) :: filename, ifilename, ofilename, tmpchar
  ! Distribution type
  ! Number of integration subintervals (R1 to R2), (0 to R1) 
  ! and number of gaussian points on each subinterval. These
  ! parameters need to be adjusted for each distribution type.
  INTEGER(KIND=4) :: NDISTR, N, NP, NK, NDISTR_IN
  
  ! R1 and R2 (Minimum and Maximum Radii)
  REAL(KIND=8) :: R1, R2
  ! Parameters defining each distribution type
  ! 1.
  REAL(KIND=8) :: alpha1, r_c1, gamma1
  ! 2.
  REAL(KIND=8) :: r_g2, sigma_g2
  ! 3. 
  REAL(KIND=8) :: r_eff3, v_eff3
  ! 4.
  REAL(KIND=8) :: a4, b4
  ! 5.
  REAL(KIND=8) :: alpha5
  ! 6.
  REAL(KIND=8) :: r_g16, sigma_g16, r_g26, sigma_g26, gamma6
  
  REAL(KIND=8) :: r_min, r_max
  
  ! Number of RH and their values
  INTEGER(KIND=4) :: nrh
  REAL(KIND=8), ALLOCATABLE, DIMENSION(:) :: rh
  !!! End declaration of input variables from control file !!!

  ! Local variables
  INTEGER(KIND=4) :: nwls, irh, rcode, ncid, dimid, vid, ndistpar
  REAL(KIND=8), ALLOCATABLE, DIMENSION(:) :: wls0, reff, reff0
  REAL(KIND=8), ALLOCATABLE, DIMENSION(:) :: waves, mmrs, mmis
  REAL(KIND=8), ALLOCATABLE, DIMENSION(:,:) :: qext0, ssa0
  REAL(KIND=8), ALLOCATABLE, DIMENSION(:,:,:,:) :: phfcn0
  INTEGER,      DIMENSION(1) :: data_0d
  REAL(KIND=8), DIMENSION(2) :: rlim
  REAL(KIND=8), DIMENSION(5) :: distpar
  
  INTEGER(KIND=4), PARAMETER :: ndistpar_list(7) = (/3,2,2,2,1,5,1/)
  
  ! =============================================================================
  ! test_aer_prop starts here
  ! =============================================================================
  
  ! Initialize Variables
  ! --------------------
  
  ! Distribution 1
  alpha1 = 1.0d0 ; r_c1=1.0d0        ; gamma1 = 1.0d0
  ! Distribution 2
  r_g2   = 1.0d0 ; sigma_g2 = 1.0d0 
  ! Distribution 3
  r_eff3 = 1.0d0 ; v_eff3 = 1.0d0
  ! Distribution 4
  a4 = 1.0d0     ; b4 = 1.0d0
  ! Distribution 5
  alpha5 = 1.0d0
  ! Distribution 6
  r_g16 = 0.17d0; sigma_g16 = 1.96d0
  r_g26 = 3.44d0; sigma_g26 = 2.37d0; gamma6 = 1.0d0
  
  
  ! ---------------------------------------------------
  ! The tool will spect to get the filename as argument
  ! ---------------------------------------------------
  CALL GETARG(1, ifilename)
  CALL GETARG(2, ofilename)
  nmom = 1001 ! Number of moments
  ngk = 6 ! NUMBER GREEK Coefficients
  NK=50   ! NUMBER OF GAUSSIAN DIVISION POINTS ON EACH OF THE INTEGRATION SUBINTERVALS
  N=100   ! NUMBER OF INTEGRATION SUBINTERVALS ON THE INTERVAL (r_min,r_max) OF PARTICLE RADII
  NP=4    ! NUMBER OF INTEGRATION SUBINTERVALS ON THE INTERVAL (0,r_min) FOR THE MODIFIED POWER LAW DISTRIBUTION
  
  !!! All this informatio to come from the input file !!!
  !!! Filenames for input and output from the MIE code !!!
!   ifilename = 'OC00_refidx-200nm-20um.dat'
!   ofilename = 'OC00_200nm-20um_wgphase_test.dat'
  
  ! All information now goes in input file
  
  ! Attach file
  rcode = nf_open( trim(adjustl(ifilename)), nf_Nowrite, ncid )
  CALL netcdf_handle_error( 'mie:1', rcode )
  
  ! Find the dimension of the spectrum
  rcode =    nf_inq_varid( ncid, 'Wavelength',  vid )
  rcode = nf_inq_vardimid( ncid,   vid,  dimid      )
  rcode =      nf_inq_dim( ncid, dimid,tmpchar,nwls )
  CALL netcdf_handle_error( 'mie:2', rcode          )
  
  rcode =    nf_inq_varid( ncid, 'RelativeHumidity', vid )
  rcode = nf_inq_vardimid( ncid,   vid,  dimid           )
  rcode =      nf_inq_dim( ncid, dimid,tmpchar,nrh       )
  CALL netcdf_handle_error( 'mie:3', rcode               )
  
  ! Allocate Arrays for reading RI values
  ALLOCATE( waves(nwls) ) ; waves(:) = 0.0d0
  ALLOCATE( mmrs(nwls)  ) ; mmrs(:)  = 0.0d0
  ALLOCATE( mmis(nwls)  ) ; mmis(:)  = 0.0d0
  ALLOCATE( rh(nrh)     ) ; rh(:)    = 0.0d0
  
  ! Allocate Output Arrays
  ALLOCATE( wls0(nwls)          ) ; wls0(:)    = 0.0d0
  ALLOCATE( reff(nwls)          ) ; reff(:)    = 0.0d0
  ALLOCATE( reff0(nwls)         ) ; reff0(:)   = 0.0d0
  ALLOCATE( qext0(1:nwls,1:nrh) ) ; qext0(:,:) = 0.0d0
  ALLOCATE( ssa0(1:nwls,1:nrh)  ) ; ssa0(:,:)  = 0.0d0
  ALLOCATE( phfcn0(1:nwls,1:nmom,1:ngk,1:nrh) ) ; phfcn0(:,:,:,:) =0.d0
  
  
  ! Read non-RH dependent fields
  ! ----------------------------
  
  ! Wavelength
  rcode = nf_inq_varid( ncid, 'Wavelength', vid )
  CALL netcdf_handle_error('mie:3a',rcode)
  rcode = nf_get_vara_double( ncid, vid,(/1/),(/nwls/), waves(:) )
  CALL netcdf_handle_error('mie:3b',rcode)
  
  ! Distribution Function
  rcode = nf_inq_varid( ncid, 'DistributionIndex', vid )
  CALL netcdf_handle_error('mie:4a',rcode)
  rcode = nf_get_vara_int( ncid, vid,(/1/),(/1/), data_0d(:) )
  NDISTR = data_0d(1)
  CALL netcdf_handle_error('mie:4b',rcode)
   
  ! Relative Humidity
  rcode = nf_inq_varid( ncid, 'RelativeHumidity', vid )
  CALL netcdf_handle_error('mie:5a',rcode)
  rcode = nf_get_vara_double( ncid, vid,(/1/),(/nrh/), rh(:) )
  CALL netcdf_handle_error('mie:5b',rcode)
  
  ! Number of parameters defining the particle distribution
  ndistpar = ndistpar_list( ndistr )
  
  
  
  DO irh=1,nrh
    
    ! Read RH Dependent fields
    ! ------------------------
    
    ! Real refractive Index
    rcode = nf_inq_varid( ncid, 'RealRefractiveIndex', vid )
    CALL netcdf_handle_error('mie:6a',rcode)
    rcode = nf_get_vara_double( ncid, vid,(/1,irh/),(/nwls,1/), mmrs(:) )
    CALL netcdf_handle_error('mie:6b',rcode)
    
    ! Imaginary Refractive Index
    rcode = nf_inq_varid( ncid, 'ImaginaryRefractiveIndex', vid )
    CALL netcdf_handle_error('mie:8a',rcode)
    rcode = nf_get_vara_double( ncid, vid,(/1,irh/),(/nwls,1/), mmis(:) )
    CALL netcdf_handle_error('mie:8b',rcode)
    
    ! Particle Radii Values
    rcode = nf_inq_varid( ncid, 'ParticleRadiusLimits', vid )
    CALL netcdf_handle_error('mie:9a',rcode)
    rcode = nf_get_vara_double( ncid, vid,(/1,irh/),(/2,1/), rlim(:) )
    r_min = rlim(1) ; r_max = rlim(2)
    CALL netcdf_handle_error('mie:9b',rcode)
    
    ! Particle Distribution Parameters
    rcode = nf_inq_varid( ncid, 'NumberDistributionParameters', vid )
    CALL netcdf_handle_error('mie:10a',rcode)
    rcode = nf_get_vara_double( ncid, vid,(/1,irh/),(/ndistpar,1/), distpar(1:ndistpar) )
    CALL netcdf_handle_error('mie:10b',rcode)
    
    ! Distribution parameters
    SELECTCASE( NDISTR )
      ! Modified Gamma Distribution
      CASE( 1 )
        ndistr_in = ndistr
        alpha1 = distpar(1) ; r_c1= distpar(2) ; gamma1 = distpar(3)
        
      ! Log Normal Distribution 
      CASE( 2 )
        ndistr_in = ndistr
        r_g2   = distpar(1) ; sigma_g2 = distpar(2)
        
      ! Power Law Distribution
      CASE( 3 )
        ndistr_in = ndistr
        r_eff3 = distpar(1) ; v_eff3 = distpar(2)
        
      ! Gamma Distribution
      CASE( 4 )
        ndistr_in = ndistr
        a4 = distpar(1) ; b4 = distpar(2)
      
      ! Modified Power Law Distribution
      CASE( 5 )
        ndistr_in = ndistr
        alpha5 = distpar(1)
        
      ! Bimodal volume log-normal distribution
      CASE( 6 )
        ndistr_in = ndistr
        r_g16 = distpar(1); sigma_g16 = distpar(2)
        r_g26 = distpar(3); sigma_g26 = distpar(4); gamma6 = distpar(5)
      
      ! Monodisperse
      CASE( 7 )
        
        ! Kludge to Gamma distribution ( Suggested in Mie Routine )
        ndistr_in = 4
        a4     = distpar(1) 
        b4     = 1.0d-1
        r_min  = a4*0.9999999d0
        r_max  = a4*1.0000001d0
        NK     = 1
        N      = 1
        NP     = 1
              
        print*,NK,N,NP
      CASE DEFAULT
        PRINT*,'NDISTR must range from 1-7. Value entered=',NDISTR
        STOP
        
    ENDSELECT
    
    
    CALL mie_interface( waves, mmrs, mmis, r_min, r_max,&
          alpha1, r_c1, gamma1, r_g2, &
          sigma_g2, r_eff3, v_eff3, a4, b4, alpha5, r_g16, sigma_g16, &
          r_g26, sigma_g26, gamma6, &
          nwls, ngk, nmom, NK, N, NP, NDISTR_IN, &
          wls0(1:nwls), reff(irh),  qext0(1:nwls,irh), &
          ssa0(1:nwls,irh), phfcn0(1:nwls,1:nmom,1:ngk,irh))
    
  ENDDO
  
  !filename = 'test.nc'
  CALL create_aerpro_nc(ofilename, nwls, ngk, nmom, nrh, &
       phfcn0, qext0, reff, rh, ssa0, wls0               )
  
END PROGRAM test_aer_prop
