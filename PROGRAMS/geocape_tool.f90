
program splat_vlidort

!  This is the GEOCAPE simulation tool for UV/Vis (solar)
!  Stand-alone software, apart from the VLIDORT dependencies

!  Based on the tool created by Rob Spurr
!  Versions 1.1-1.4 for the UV/Vis simulation of Stokes-vector
!  Versions 1.5-1.6 for the IR simulation of Stokes-vector
!  Versions 1.1-1.3
!  Radiances and Jacobians in a clear-sky Rayleigh-scattering atmosphere

!  ******************* Important note **************************************
!  All VLIDORT inputs are either HARD_WIRED or DERIVED from Geocape variables

!  Version 1.1: R. Spurr, RT SOLUTIONS Inc., 10-11 June 2009
!  Version 1.2: R. Spurr, RT SOLUTIONS Inc., 22 June 2009
!  Version 1.3: X. Liu, UMBC/Gest.,          23 June 2009
!  Version 1.4: R. Spurr, complete revision of UV, including aerosol
!  Version 1.5: R. Spurr, IR tool development
!  Version 1.6: V. Natraj, Caltech, Vis tool for GEOCAPE for GSFC and TES profiles, 30 Oct 2009
!  Version 1.7: X. Liu, Add interface to convolve cross sections with slit functions using
!          two methods (high-resolution calculation or effective cross section), March 30, 2011
!  Version 1.8: Modify user input atmospheric profiles
!               Output upwelling/downwelling radiances at any altitudes
!               Add ocean CoxMunk BRDF
!               Use wavelength or wavenumber
!               Improve cross section preparation (more flexible) and add more gases
!               Remove dependency on ozone for weighting functions
!               Add AMF and scattering weightings
!               Input surface albedo sepctra
!               Add satellite viewing geometry calculation
!               Implement Lambertian cloud/scattering clouds
!               Generate aerosol plume
!               Implement multiple types of aerosol/clouds, either user input or 
!                   generated profiles, compute aerosol/cloud weighting functions
!  Version 2.0: Modularization, upgraded to Vlidort 2.6, April 2013 gga

!  ##################################################################
!  ##################################################################
!          A R G U M E N T S    and    I N C L U D E  F I L E S
!  ##################################################################
!  ##################################################################

!  ------------------
!  Modules to be used
!  ------------------
  USE GC_variables_module
  USE GC_error_module,      ONLY: error_exit, open_process_logfile
  USE GC_read_input_module, ONLY: read_input_file,prepare_wavegrid,&
                                  check_input
  USE GC_profiles_module,   ONLY: prepare_profiles
  USE GC_aerosols_module,   ONLY: aerosol_profiles
  USE GC_clouds_module,     ONLY: cloud_profiles
  USE GC_solar_module,      ONLY: prepare_solar_spectrum
  USE GC_xsections_module,  ONLY: prepare_xsec_new
  USE GC_surface_module,    ONLY: prepare_surface
  USE GC_SciaUSGS_FA_module,ONLY: precompute_VLIDORT_BRDF_arrays
  USE GC_Vlidort_module,    ONLY: Vlidort_set_optical,             &
                                  Vlidort_cloud_and_calculation_npix,&
                                  initialize_vlidort_inputs
  USE GC_netcdf_module,     ONLY: allocate_output_arrays
  USE GC_diagnostics_module,ONLY: archive_diagnostics, &
                                  create_nc_diagnostic_file,&
                                  write_nc_diagnostic_file
  
  IMPLICIT NONE
  
  ! Time
  REAL    :: t1, t2,rate
  INTEGER :: c1,c2,cr,cm

  ! nc_xids & nc_yids loop index
  INTEGER :: ncxi, ncyi

!  ##################################################################
!  ##################################################################
!                  S T A R T    of    C O D E
!  ##################################################################
!  ##################################################################
   
   ! Intialize first iteration flag
   first_xy = .TRUE.
   
   ! First initialize the system_clock
  CALL system_clock(count_rate=cr)
  CALL system_clock(count_max=cm)
  rate = REAL(cr)
   
   ! CCM Time
   CALL CPU_TIME(t1)
   CALL SYSTEM_CLOCK(c1)

   ! ---------------
   ! Open error file
   ! ---------------
   CALL open_process_logfile(yn_error)
   IF (yn_error) CALL error_exit (yn_error)
   
   ! -----------------
   ! Read control file
   ! -----------------
   IF ( do_debug_geocape_tool ) print*,'==> GC Reading Control File'
   CALL read_input_file( yn_error )
   IF (yn_error) CALL error_exit (yn_error)
   
   ! ------------------------------
   ! Check for input incosistencies
   ! ------------------------------
   CALL check_input(yn_error)
   IF (yn_error) CALL error_exit (yn_error)
   
   ! -----------------------
   ! Prepare wavelength grid
   ! -----------------------
   CALL prepare_wavegrid( yn_error )
   IF( yn_error ) CALL error_exit(yn_error)

!  ###########################
!  ###########################
!  Loop over nc_xids & nc_yids
!  ###########################
!  ###########################
  DO ncxi = nc_xids(1), nc_xids(2)
    nc_xid = ncxi
  DO ncyi = nc_yids(1), nc_yids(2)
    nc_yid = ncyi

   ! ##################################################################
   ! ##################################################################
   !                  D A T A   E X T R A C T I O N
   ! ##################################################################
   ! ##################################################################
   
   ! ================================================================
   ! SET ATMOSPHERIC PROFILES
   ! ================================================================
   IF ( do_debug_geocape_tool ) print*,'==> GC preparing profiles'
   CALL prepare_profiles(yn_error)
   
   IF( skip_xy_pixel ) GOTO 101
   
   IF (yn_error) CALL error_exit (yn_error)
   
   ! ================================================================
   ! SET CLOUD PROFILES + OPTICAL PROPERTIES
   ! ================================================================
   IF ( do_debug_geocape_tool ) print*,'==> GC Preparing cloud profiles'
   CALL cloud_profiles(yn_error)
   IF (yn_error) CALL error_exit (yn_error)
   
   ! ================================================================
   ! SET AEROSOL PROFILES + OPTICAL PROPERTIES
   ! ================================================================
   IF (do_aerosols) THEN
      IF ( do_debug_geocape_tool ) print*,'==> GC Preparing aerosol profiles'
      CALL aerosol_profiles(yn_error)
      IF (yn_error) CALL error_exit(yn_error)
   ENDIF

   ! ================================================================
   ! SET SOLAR SPECTRUM
   ! ================================================================
   IF ( do_debug_geocape_tool ) print*,'==> GC Preparing solar spectrum'
   CALL prepare_solar_spectrum(yn_error)
   IF (yn_error) CALL error_exit (yn_error)

   ! ================================================================
   ! SET GAS CROSS SECTIONS
   ! ================================================================
   IF ( do_debug_geocape_tool ) print*,'==> GC Preparing gas xsects'
   CALL prepare_xsec_new(yn_error)
   IF (yn_error) CALL error_exit (yn_error)
   
   ! ================================================================
   ! SET SURFACE OPTICAL PROPERTIES
   ! ================================================================
   IF ( do_debug_geocape_tool ) print*,'==> GC Preparing surface reflectance'
   CALL prepare_surface(yn_error)
   IF (yn_error) CALL error_exit (yn_error)

   ! ================================================================
   ! Set VLIDORT Options
   ! ================================================================
   IF ( do_debug_geocape_tool ) print*,'==> Setting VLIDORT Input Control'
   CALL initialize_vlidort_inputs( yn_error )
   
   ! ================================================================
   ! Allocate diagnostic arrays
   ! ================================================================
   CALL allocate_output_arrays(yn_error)

   ! ================================================================
   ! Create diagnostic output file if necessary
   ! ================================================================
   IF ( do_debug_geocape_tool ) print*,'==> GC Creating Diagnostic Output'
   CALL create_nc_diagnostic_file( t1, yn_error )
   
   !  ##################################################################
   !  ##################################################################
   !  M A I N    W A V E L E N G T H - L O O P    C A L C U L A T I O N
   !  ##################################################################
   !  ##################################################################
   IF ( do_debug_geocape_tool ) print*,'==> GC Starting wavelength loop'
   
   DO w = 1, nlambdas
      
    ! Set optical properties for Vlidort calculation
    CALL Vlidort_set_optical (yn_error)
    IF (yn_error) CALL error_exit(yn_error)
      
    ! Do radiative transfer calculation
    CALL Vlidort_cloud_and_calculation_npix( yn_error )
    IF (yn_error) CALL error_exit(yn_error)

    ! Archive diagnostics 
    CALL archive_diagnostics( w, yn_error )
      
   ENDDO
   
   ! Write results
   CALL write_nc_diagnostic_file( yn_error )
  
   ! Set flag for first iteration to false
   first_xy = .FALSE.

  ! Skip calculation 
101 CONTINUE 

   ! Deallocate variables
   CALL deallocate_variables ! This should be moved outside the loop when we fix unecessary reallocations

!  ###############################
!  ###############################
!  End loop over nc_xids & nc_yids
!  ###############################
!  ###############################
   
   END DO ! End loop over nc_yids
   END DO ! End loop over nc_xids

   ! ----------------
   ! Close debug file
   ! ----------------
   IF ( do_debug_geocape_tool ) THEN
      CLOSE(du)
   ENDIF
  
   CALL CPU_TIME(t2)
   CALL SYSTEM_CLOCK(c2)
   print*,'===>CPU_TIME:',(t2-t1)
   print*,'===>SYS. CLK:',(c2-c1)/rate
   
   ! ##################
   ! ------------------
   ! Final call to exit
   ! ------------------
   ! ##################
   CALL error_exit(yn_error)

 end program splat_vlidort