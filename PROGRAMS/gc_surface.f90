program gc_surface
  
  USE GC_variables_module, ONLY : max_ch_len, julday, clon, clat, &
                                  latitude, longitude, month,     &
                                  ground_ler, lambdas, nlambdas
  USE GC_parameters_module
  
  USE GC_SciaUSGS_FA_module, ONLY : init_SciaUSGS_FA,&
                                    load_MODIS_pixel,&
                                    compute_RTLS_albedo,&
                                    update_SciaUSGS_FA
  
  
!   USE GC_surface_module,   ONLY : get_scia_modis_eofspec !, get_merged_surfspec
  
  
  
  IMPLICIT NONE
  INCLUDE 'netcdf.inc'
  
  ! ------------------------------------------------------
  ! Input options
  ! ------------------------------------------------------
  
  ! BRDF Geometry
  REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: sza, vza, aza
  INTEGER :: n_sza, n_vza, n_aza
  
  ! Surface Options
  LOGICAL :: do_brdf
  
  ! Geolocation
  REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE :: xmid, ymid, xedge, yedge
  INTEGER                                   :: imx, jmx
  CHARACTER(LEN=max_ch_len)                 :: gridfile, outfile
  REAL(KIND=8)                              :: dx, dy, x0, y0
  LOGICAL                                   :: read_grid_file ! Also add wavelength info
  INTEGER                                   :: year, day, doy
  
  ! Wavelength grid [nm]
  REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: wvl
  REAL(KIND=8)                            :: wvl0, wvlf, dwvl, snowfrac
  INTEGER                                 :: nwvl
  
  ! ------------------------------------------------------
  ! Output variables
  ! ------------------------------------------------------
  
  REAL(KIND=8), DIMENSION(:,:,:), ALLOCATABLE       :: LER
  REAL(KIND=8), DIMENSION(:,:,:,:,:,:), ALLOCATABLE :: BRDF
  
  ! LER diagnostics
  REAL(KIND=8), DIMENSION(:,:,:), ALLOCATABLE       :: LER_SciaFit, LER_Smod, LER_MODIS
  REAL(KIND=8), DIMENSION(:,:),   ALLOCATABLE       :: LFRAC
  
  ! Other
  INTEGER :: i, j, what_albedo
  LOGICAL :: error, read_modis
  
  LOGICAL :: first
  
  ! ======================================================
  ! gc_surface starts here
  ! ======================================================
  
  ! Read setup
  CALL read_gcs_input( )
  
  snowfrac = 0.0d0
  
  what_albedo = 3
  read_modis = .TRUE.
  ! Set wavelength grid within EOF routine
  lambdas(1:nwvl) = wvl(1:nwvl)
  nlambdas = nwvl
  
  
  first = .TRUE.
  
  ! Loop over corners
  DO i=1,imx
  DO j=1,jmx
    
    print*,i,j
    
    ! Define corner longitudes/latitudes
    clon(1) = xedge(i,  j  )
    clon(2) = xedge(i,  j+1)
    clon(3) = xedge(i+1,j+1)
    clon(4) = xedge(i+1,j  )
    clat(1) = yedge(i,  j  )
    clat(2) = yedge(i,  j+1)
    clat(3) = yedge(i+1,j  )
    clat(4) = yedge(i+1,j  )
    
    longitude = xmid(i,j)
    latitude  = ymid(i,j)
    
    ! Update the geography dependent factor analysis 
    IF( first ) THEN
       CALL init_SciaUSGS_FA(longitude,latitude)
       first = .FALSE.
    ELSE
       CALL update_SciaUSGS_FA(longitude,latitude)
    ENDIF
    
    
    ! Load MODIS observations within pixel
    CALL load_MODIS_pixel( doy, snowfrac, error )
    
    ! Compute the albedo
    CALL compute_RTLS_albedo(sza(1), aza(1), vza(1),               &
                             what_albedo,                          &
                             ground_ler(1:nlambdas), nlambdas, error    )
    
    ! Save LER
    LER(:,i,j) = ground_ler(1:nwvl)
    
!     ! Save diagnostics
!     LER_SciaFit(:,i,j) = scia_fit
!     LER_Smod(:,i,j)    = smod_y(7:22)
!     LER_MODIS(:,i,j) = alb_eof_coeff
!     LFRAC(i,j)  = alb_eof_lfrac
    
    
    
  ENDDO
  ENDDO
  
  ! Write results to disk
  CALL write_surf_output()
  
  ! ======================================================
  ! gc_surface ends here
  ! ======================================================
  
  CONTAINS
  
    SUBROUTINE read_gcs_input()
      
      USE GC_time_module, ONLY: GET_DAY_OF_YEAR
      USE GC_variables_module, ONLY : do_debug_geocape_tool, database_dir
      
      ! Manually set up options while testing
      do_debug_geocape_tool = .FALSE.
      
      ! Grid file options
      gridfile = ''
      read_grid_file = .FALSE.
      
      database_dir = '../data/'
      
      ! Non grid file options
      
      ! SouthWest US
      y0 =    32.0
      x0 =  -125.0
      dx =   0.04
      dy =   0.04
      imx = 576
      jmx = 276
      outfile = 'gc_surf_scia_sw_newsmoothv2.nc'
!       
      ! Salt flat
!       y0 =    40.2
!       x0 =  -113.5
!       dx =   0.04
!       dy =   0.04
!       imx = 5
!       jmx = 5
!       outfile = 'gc_surf_scia_salt.nc'
      
      ! Vegetation
!       y0 =    41.0
!       x0 =  -124.0
!       dx =   0.04
!       dy =   0.04
!       imx = 5
!       jmx = 5
!       outfile = 'gc_surf_scia_veg.nc'
      
      ! Bare land
!       y0 =    33.0
!       x0 =  -110.0
!       dx =   0.04
!       dy =   0.04
!       imx = 5
!       jmx = 5
!       outfile = 'gc_surf_scia_bare.nc'
      
      ! Set output options
      do_brdf = .FALSE.
      
      ! Wavelength grid
      dwvl = 10.0
      wvl0 = 335.0
      wvlf = 880.0
      
      IF( read_grid_file ) THEN
        
      ELSE
        
        ! Compute grid
        ALLOCATE( xmid(imx,jmx) ) 
        ALLOCATE( ymid(imx,jmx) ) 
        ALLOCATE( xedge(imx+1,jmx+1) )
        ALLOCATE( yedge(jmx+1,jmx+1) )
        
        DO i=1,imx
          xedge(i,:) = x0 + REAL(i-1,kind=8) * dx
          xmid(i,:)  = x0 + ( REAL(i-1,kind=8) + 0.5d0 ) * dx
        ENDDO
        xedge(imx+1,:) = xedge(imx,1) + dx
        
        DO j=1,jmx
          yedge(:,j) = y0 + REAL(j-1,kind=8) * dy
          ymid(:,j)  = y0 + ( REAL(j-1,kind=8) + 0.5d0 ) * dy
        ENDDO
        yedge(:,jmx+1) = yedge(1,jmx) + dy
        
        ! Wavelength grid
        nwvl = NINT ( (wvlf - wvl0 ) / dwvl ) + 1
        ALLOCATE( wvl(nwvl) )
        wvl(1) = wvl0
        DO i = 2, nwvl
          wvl(i) = wvl(i-1) + dwvl
        END DO
        
      ENDIF
      
      year  = 2013
      month =    7
      day   =    1
      
      ! Also compute day of year from y/m/d
      doy = GET_DAY_OF_YEAR(month,day,year,0,0,0.0d0)
      
      ! Geometries
      n_sza = 1
      n_vza = 1
      n_aza = 1
      
      ! Allocate arrays
      ALLOCATE(sza(n_sza))
      ALLOCATE(vza(n_vza))
      ALLOCATE(aza(n_aza))
      
      ! This would be where the geometries are read
      sza(1) = 45.0
      vza(1) =  0.0
      aza(1) =  0.0
      
      ! Allocate output arrays
      ALLOCATE( LER(nwvl,imx,jmx) )
      IF( do_brdf ) THEN
        ALLOCATE( BRDF(nwvl,n_sza,n_vza,n_aza,imx,jmx) )
      ENDIF
      
      ! LER Diagnostics
      ALLOCATE( LER_SciaFit(scia_nwvl,imx,jmx) )
      ALLOCATE(    LER_Smod(scia_nsub,imx,jmx) )
      ALLOCATE(   LER_MODIS(        4,imx,jmx) )
      ALLOCATE(       LFRAC(          imx,jmx) )
      
    END SUBROUTINE read_gcs_input
    
    SUBROUTINE write_surf_output
      
      INTEGER :: ncid,rcode
      INTEGER :: xid,yid,wid,lid,xeid,yeid, &
                 wsid,lsid,lmid,lsmid,lfid
      INTEGER :: xdim,ydim,wdim, xedim,yedim,&
                 wscdim,wsbdim,wmdim
      
      
      
      ! Create the file
      ncid = nccre(trim(outfile), ncclob, rcode)
      if (rcode .eq. -1) then
        print*, ' error in netcdf_out: nccre failed'
      endif
      
      ! Define dimensions
      wdim = ncddef(ncid,'nw',nwvl, rcode)
      xdim = ncddef(ncid,'nx',imx,  rcode)
      ydim = ncddef(ncid,'ny',jmx,  rcode)
      xedim = ncddef(ncid,'nxe',imx+1,  rcode)
      yedim = ncddef(ncid,'nye',jmx+1,  rcode)
      
      ! Additional diagnostics
      wscdim = ncddef(ncid,'nw_scia',scia_nwvl, rcode)
      wsbdim = ncddef(ncid,'nw_nsub',scia_nsub, rcode)
      wmdim  = ncddef(ncid,'nw_mod',         4, rcode)
      
      ! Define variables
      xid = ncvdef(ncid,'Longitude', ncdouble, 2, (/xdim,ydim/), rcode)
      yid = ncvdef(ncid,'Latitude' , ncdouble, 2, (/xdim,ydim/), rcode)
      xeid = ncvdef(ncid,'CornerLongitude', ncdouble, 2, (/xedim,yedim/), rcode)
      yeid = ncvdef(ncid,'CornerLatitude' , ncdouble, 2, (/xedim,yedim/), rcode)
      wid  = ncvdef(ncid,'Wavelength', ncdouble, 1, (/wdim/), rcode)
      lid  = ncvdef(ncid,'LER', ncdouble, 3, (/wdim,xdim,ydim/), rcode)
      
      ! Addiational diagnostics
!       wsid  = ncvdef(ncid,'WavelengthSCIA', ncdouble, 1, (/wscdim/), rcode)
      lsid  = ncvdef(ncid,'LER_SCIAFit', ncdouble, 3, (/wscdim,xdim,ydim/), rcode)
      lmid  = ncvdef(ncid,'EOF_MODIS', ncdouble, 3, (/wmdim,xdim,ydim/), rcode)
      lsmid  = ncvdef(ncid,'LER_SCIAy', ncdouble, 3, (/wsbdim,xdim,ydim/), rcode)
      lfid  = ncvdef(ncid,'LandFraction', ncdouble, 2, (/xdim,ydim/), rcode)
      
      ! switch from define to data mode
      call ncendf (ncid, rcode)
      
      ! Write the data
      call ncvpt(ncid, xid, (/1,1/),(/imx,jmx/),xmid,rcode)
      call ncvpt(ncid, yid, (/1,1/),(/imx,jmx/),ymid,rcode)
      call ncvpt(ncid, xeid, (/1,1/),(/imx+1,jmx+1/),xedge,rcode)
      call ncvpt(ncid, yeid, (/1,1/),(/imx+1,jmx+1/),yedge,rcode)
      call ncvpt(ncid, wid, (/1/),(/nwvl/),wvl,rcode)
      call ncvpt(ncid, lid, (/1,1,1/),(/nwvl,imx,jmx/),LER,rcode)
      
      ! Diagnostic vars
!       call ncvpt(ncid, wsid, (/1/),(/scia_nwvl/),scia_wvl,rcode)
      call ncvpt(ncid, lsid, (/1,1,1/),(/scia_nwvl,imx,jmx/),LER_SciaFit,rcode)
      call ncvpt(ncid, lsmid, (/1,1,1/),(/scia_nsub,imx,jmx/),LER_Smod,rcode)
      call ncvpt(ncid, lmid, (/1,1,1/),(/4,imx,jmx/),LER_MODIS,rcode)
      call ncvpt(ncid, lfid, (/1,1/),(/imx,jmx/),LFRAC,rcode)
      
      ! Close file
      call ncclos (ncid, rcode)
      
      
    END SUBROUTINE write_surf_output
    
    
END PROGRAM gc_surface



