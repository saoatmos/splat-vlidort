program hitran_lut

    USE GC_solar_module, ONLY: solarspec_prep_newkur
    USE GC_utilities_module, ONLY: gauss_f2ci0
    USE GC_get_hitran_crs_module, ONLY: get_hitran_crs
    USE GC_xsections_module,  ONLY: get_hitran_lut_crs
    USE GC_variables_module,  ONLY: max_ch_len, solar_spec_filename
    USE GC_parameters_module, ONLY: maxflambdas
    USE GC_error_module,      ONLY: error_exit
    USE GC_read_input_module, ONLY: STRREPL, &
                                    READ_ONE_LINE, &
                                    SPLIT_ONE_LINE
!    USE NETCDF
    IMPLICIT NONE
    INCLUDE 'netcdf.inc'
    
    INTEGER          :: nT ! # temperatures
    INTEGER          :: np ! # pressures
    INTEGER          :: npos    ! # wavelengths
    INTEGER          :: nposI0  ! # wavelengths
    REAL(KIND=8)     :: fwhm ! Cross section convolved FWHM
    REAL(KIND=8)     :: scale_vcd ! Norm factor for convolution 
    REAL(KIND=8)     :: dpos ! Sampling wavelength interval 
    REAL(KIND=8)     :: pos0 ! Lower wavelength limit
    REAL(KIND=8)     :: posf ! Upper wavelength limit
    REAL(KIND=8)     :: pos_sub ! Position sub interval for XS computation
    
    ! For the validation
    REAL(KIND=8)     :: odpos ! Sampling wavelength interval 
    REAL(KIND=8)     :: opos0 ! Lower wavelength limit
    REAL(KIND=8)     :: oposf ! Upper wavelength limit
    REAL(KIND=8)     :: ofwhm ! Upper wavelength limit
    
    CHARACTER(LEN=6) :: the_molecule ! GEOCAPE molecule name
    REAL(KIND=8), ALLOCATABLE, DIMENSION(:) :: temp ! Temperatures (K)
    REAL(KIND=8), ALLOCATABLE, DIMENSION(:) :: pres ! Pressures (hPa)
    REAL(KIND=8), ALLOCATABLE, DIMENSION(:) :: pos  ! Wavelength (nm)
    REAL(KIND=8), ALLOCATABLE, DIMENSION(:) :: hI0  ! Solar spectrum
    REAL(KIND=8), ALLOCATABLE, DIMENSION(:) :: posI0! Solar position
    REAL(KIND=8), ALLOCATABLE, DIMENSION(:,:,:) :: xsec ! Cross sections (wav,T,p)
    CHARACTER(LEN=max_ch_len) :: outfile
    
    ! Fields for the test set
    CHARACTER(LEN=max_ch_len) :: testset_outfile
    LOGICAL :: create_testset, create_lut
    INTEGER :: nsamp_testset
    REAL(KIND=8), ALLOCATABLE, DIMENSION(:,:) :: xsec_tset ! Cross sections (wav,nsamp)
    REAL(KIND=8), ALLOCATABLE, DIMENSION(:) :: p_tset, T_tset
    REAL(KIND=8) :: pmin, pmax, Tmin, Tmax
    
    ! Validation fields on coarse grid
    REAL(KIND=8), ALLOCATABLE, DIMENSION(:)   :: opos        ! Out position
    REAL(KIND=8), ALLOCATABLE, DIMENSION(:)   :: soltmp      ! Kludge for solar
    REAL(KIND=8), ALLOCATABLE, DIMENSION(:,:) :: xsec_truth  ! convolved with ofwhm and spline to output grid
    REAL(KIND=8), ALLOCATABLE, DIMENSION(:,:) :: xsec_interp ! Same but using LUT
    
    
    
!     REAL(KIND=8), DIMENSION(10) :: posI0, hI0 ! Kludge for solar spec
    LOGICAL, PARAMETER :: is_wavenum = .FALSE.
    LOGICAL, PARAMETER :: use_wavelength = .TRUE.
    INTEGER :: i,j, n, errstat
    
    CHARACTER(LEN=max_ch_len) :: input_filename, message
    
    REAL(KIND=8), DIMENSION(1) :: pres_atm
    INTEGER :: ios
    CHARACTER(LEN=max_ch_len) :: tmpstr
    CHARACTER(LEN=1)   :: TAB   = ACHAR(9)
    CHARACTER(LEN=1)   :: SPACE = ' '
    CHARACTER(LEN=max_ch_len) :: LINE
    CHARACTER(LEN=max_ch_len) :: SUBSTRS(max_ch_len)
    LOGICAL     :: EOF, do_i0_corr, error
    INTEGER, PARAMETER :: funit = 2
    INTEGER :: nsub, npos_sub, ntpos, nopos
    INTEGER, ALLOCATABLE, DIMENSION(:) :: idw
    INTEGER :: ncid, rcode, posdim, oposdim, pdim, Tdim, sampdim
    INTEGER :: posid, pid, Tid, xid, lxid, oposid, oxid
    
    CHARACTER(LEN=max_ch_len), ALLOCATABLE, DIMENSION(:) :: db_infile, db_outfile
    REAL(KIND=8), ALLOCATABLE, DIMENSION(:)              :: db_vcd
    INTEGER, ALLOCATABLE, DIMENSION(:)                   :: db_nxs
    INTEGER :: n_dbspec
    CHARACTER(LEN=max_ch_len) :: database_dir
    LOGICAL :: convolve_database_spec
    
    ! ======================================================================
    ! HITRAN_LUT starts here
    ! ======================================================================
    
    ! -------------------
    ! Read the input file
    ! -------------------
    ! ---------------------------------------------------
    ! The tool will spect to get the filename as argument
    ! ---------------------------------------------------
    ! CALL GETARG(1, input_filename)
    input_filename = './input.hlut' ! Fix this for now
    
    ! open file
    OPEN (UNIT=funit, FILE=TRIM(ADJUSTL(input_filename)), ACTION='READ', STATUS='OLD', IOSTAT=ios )
    IF ( ios /= 0 ) THEN
      STOP 'ERROR: Unable to open input file' 
    END IF
    
    
    ! Loop over files
    DO 
      
      ! Read a line from the file, exit if EOF
      LINE = READ_ONE_LINE( funit, EOF ) 
      IF ( EOF ) EXIT
         
      ! Replace tab characters in LINE (if any) w/ spaces
      CALL STRREPL( LINE, TAB, SPACE )
        
      IF      ( INDEX( LINE, '%%% SIMULATION MENU %%%'    ) > 0 ) THEN
        
        ! Read the simulation menu
        CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_simulation_menu:1' )
        READ( SUBSTRS(1:N), '(A)' ) outfile
        
        CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_simulation_menu:1' )
        READ( SUBSTRS(1:N), '(A)' ) the_molecule
        
        CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_simulation_menu:1' )
        READ( SUBSTRS(1:N), * ) pos0
        
        CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_simulation_menu:1' )
        READ( SUBSTRS(1:N), * ) posf
        
        CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_simulation_menu:1' )
        READ( SUBSTRS(1:N), * ) dpos
        
        CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_simulation_menu:1' )
        READ( SUBSTRS(1:N), * ) fwhm
        
        CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_simulation_menu:1' )
        READ( SUBSTRS(1:N), * ) do_i0_corr
        
        CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_simulation_menu:1' )
        READ( SUBSTRS(1:N), * ) scale_vcd
        
        CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_simulation_menu:1' )
        READ( SUBSTRS(1:N), '(A)' ) solar_spec_filename
        
        CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_simulation_menu:1' )
        READ( SUBSTRS(1:N), * ) pos_sub
        
        CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_simulation_menu:1' )
        READ( SUBSTRS(1:N), * ) convolve_database_spec
        
        CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_simulation_menu:1' )
        READ( SUBSTRS(1:N),'(A)' ) database_dir
        
        CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_simulation_menu:1' )
        READ( SUBSTRS(1:N), * ) n_dbspec
        
        ! Allocate output arrays
        ALLOCATE( db_infile(n_dbspec) )
        ALLOCATE( db_outfile(n_dbspec) )
        ALLOCATE( db_vcd(n_dbspec) )
        ALLOCATE( db_nxs(n_dbspec) )
        
        ! Skip header
        CALL SPLIT_ONE_LINE( funit, SUBSTRS, N,-1, 'read_simulation_menu:1' )
        
        print*,n_dbspec
        
        DO i=1,n_dbspec
          CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 4, 'read_simulation_menu:1' )
          print*,TRIM(ADJUSTL(substrs(1))),TRIM(ADJUSTL(substrs(2))),&
                 TRIM(ADJUSTL(substrs(3))),TRIM(ADJUSTL(substrs(4)))
          READ( SUBSTRS(1), '(A)' ) db_infile(i)
          READ( SUBSTRS(2), '(A)' ) db_outfile(i)
          READ( SUBSTRS(3),    *  ) db_vcd(i)
          READ( SUBSTRS(4),    *  ) db_nxs(i)
        ENDDO
        
        CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_simulation_menu:1' )
        READ( SUBSTRS(1:N), * ) create_lut
        
        CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_simulation_menu:1' )
        READ( SUBSTRS(1:N), * ) np
        
        ! Allocate pressure array
        ALLOCATE(pres(np))
        
        DO i=1,np
          CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_simulation_menu:1' )
          READ( SUBSTRS(1:N), * ) pres(i)
        ENDDO
        
        CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_simulation_menu:1' )
        READ( SUBSTRS(1:N), * ) nT
        
        ! Allocate pressure array
        ALLOCATE(temp(nT))
        
        DO i=1,nT
          CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_simulation_menu:1' )
          READ( SUBSTRS(1:N), * ) temp(i)
        ENDDO
        
        CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_simulation_menu:1' )
        READ( SUBSTRS(1:N), * ) create_testset
        
        CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_simulation_menu:1' )
        READ( SUBSTRS(1:N), '(A)' ) testset_outfile
        
        CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_simulation_menu:1' )
        READ( SUBSTRS(1:N), * ) nsamp_testset
        
        ! Skip line
        CALL SPLIT_ONE_LINE( funit, SUBSTRS, N,-1, 'read_simulation_menu:1' )
        
        CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_simulation_menu:1' )
        READ( SUBSTRS(1:N), * ) opos0
        
        CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_simulation_menu:1' )
        READ( SUBSTRS(1:N), * ) oposf
        
        CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_simulation_menu:1' )
        READ( SUBSTRS(1:N), * ) odpos
        
        CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_simulation_menu:1' )
        READ( SUBSTRS(1:N), * ) ofwhm
        
      ELSE IF ( INDEX( LINE, 'END OF FILE'                ) > 0 ) THEN
        EXIT
      ENDIF  
        
    ENDDO
    
    ! Close file
    CLOSE( funit )
    
    ! -----------------------------------------
    ! Compute the LUT
    ! -----------------------------------------
    
    ! Determine # of wavelengths
    npos = floor( (posf-pos0)/dpos ) + 2
    
    ALLOCATE( pos(npos) )
    ALLOCATE( xsec(npos,nT,np) )
    
    ! Used later for slit convolution in validation
    ALLOCATE( soltmp(npos) )
    soltmp(:) = 1.0d0
    
    ! Compute wavelength grid
    pos(1) = pos0
    DO i=2,npos
      pos(i) = pos(i-1) + dpos
    ENDDO
    
    ! Determine sub intervals
    IF( pos_sub .LT. 0.0 ) THEN
      
      nsub = 1
      
      ALLOCATE(idw(2))
      idw(1) = 1
      idw(2) = npos
    
    ELSE
      
      nsub = floor( (posf-pos0)/pos_sub ) 
      
      ! Underestimate the sub interval
      npos_sub = floor( pos_sub / dpos) 
      ALLOCATE(idw(nsub+1))
      
      DO i=1,nsub+1
        
        idw(i) = 1 + (i-1)*npos_sub
        print*,i,idw(i), npos
        
      ENDDO
    ENDIF
    
    ! Attach the tail end of the positions 
    IF( idw(nsub+1) < npos) idw(nsub+1) = npos
    
    print*,do_i0_corr
    
    IF( do_i0_corr ) THEN
      
      ! Buffer 20 FWHM either side for solar spectrum
      nposI0 = floor( ( posf - pos0  + 40.*fwhm)/dpos ) + 2
      
      ! Allocate 
      ALLOCATE( posI0(nposI0) )
      ALLOCATE(   hI0(nposI0) )
      
      ! Compute wavelength grid
      posI0(1) = pos0 - 20.0 * fwhm
      DO i=2,nposI0
        posI0(i) = posI0(i-1) + dpos
      ENDDO
      
      print*,TRIM(ADJUSTL(solar_spec_filename))
      CALL solarspec_prep_newkur &
             ( TRIM(ADJUSTL(solar_spec_filename)), nposI0, nposI0, 1.0d7/posI0,  & ! input
               hI0, message, error  ) 
      
    ELSE
      
      ! 
      nposI0 = 10
      ALLOCATE( posI0(nposI0) )
      ALLOCATE(   hI0(nposI0) )
      
      ! Kludge the I0 grid
      posI0(1) = 0.0d0
      DO i=2,10
        posI0(i) = posI0(i-1) + 5000.0d0
      ENDDO
      hI0(:) = 1.0d0
    
    ENDIF
    
    ! Initialize error status
    errstat = 0
    
    IF( convolve_database_spec ) THEN
      
      
      
    ENDIF
    
    IF( create_lut ) THEN
      
      DO i=1,nT
      DO j=1,np
        
        print*,i,j,'/',nT,np
    
        pres_atm(1) = pres(j)/1013.25d0
        
        DO n=1,nsub
          
          ntpos = idw(n+1)-idw(n)+1
          
          print*,idw(n),idw(n+1)
          
          ! Example call to get Hitran cross section
          call get_hitran_crs(the_molecule, ntpos, pos(idw(n):idw(n+1)), &
                    nposI0, posI0, hI0,  &
                    is_wavenum, use_wavelength, 1, pres_atm(1), temp(i), &
                    fwhm, scale_vcd, xsec(idw(n):idw(n+1), i, j), errstat)
                    
        ENDDO
      ENDDO
      ENDDO
      
      ! ------------------------------
      ! Write the netCDF lookup table
      ! ------------------------------
      
      ! Create the file
      ncid = nccre(trim(outfile), ncclob, rcode)
      if (rcode .eq. -1) then
        print*, ' error in netcdf_out: nccre failed'
      endif
      
      ! Define dimensions
      posdim = ncddef(ncid,'npos',npos, rcode)
      pdim   = ncddef(ncid,'np',np, rcode)
      Tdim   = ncddef(ncid,'nT',nT, rcode)
      
      print*,posdim,Tdim,pdim
      
      ! Define variables
      posid = ncvdef(ncid,'Wavelength', ncdouble, 1,posdim, rcode)
      Tid = ncvdef(ncid,'Temperature',ncdouble, 1,Tdim,rcode)
      pid = ncvdef(ncid,'Pressure',ncdouble,1,pdim,rcode)
      xid = ncvdef(ncid,'CrossSection',ncdouble,3,(/posdim,Tdim,pdim/),rcode)
      
      ! switch from define to data mode
      call ncendf (ncid, rcode)
      
      ! Write the data
      call ncvpt(ncid, posid,(/1/),    (/npos/),pos(1:npos),rcode)
      call ncvpt(ncid, Tid,  (/1/),    (/nT/),temp,rcode)
      call ncvpt(ncid, pid,  (/1/),    (/np/),pres,rcode)
      call ncvpt(ncid, xid,  (/1,1,1/),(/npos,nT,np/),xsec,rcode)
      
      ! Close file
      call ncclos (ncid, rcode)
    
    ENDIF
    
    IF( create_testset ) THEN
      
      ! Determine # of wavelengths
      nopos = floor( (oposf-opos0)/odpos ) + 2
      
      ALLOCATE( opos(nopos) )
      
      
      ! Compute wavelength grid
      opos(1) = opos0
      DO i=2,nopos
        opos(i) = opos(i-1) + odpos
      ENDDO
      
      ! Get p/T range
      pmin = minval(pres)
      pmax = maxval(pres)
      Tmin = minval(temp)
      Tmax = maxval(temp)
      
      ! Allocate output arrays
      ALLOCATE( p_tset(nsamp_testset) )
      ALLOCATE( T_tset(nsamp_testset) )
      ALLOCATE( xsec_tset(npos,nsamp_testset) )
      ALLOCATE( xsec_truth(nopos,nsamp_testset)  )
      ALLOCATE( xsec_interp(nopos,nsamp_testset) )
      
      ! Generate p/T within the given range
      CALL random_number (T_tset) ! Uniform range [0,1]
      CALL random_number (p_tset) 
      T_tset = T_tset *( Tmax-Tmin ) + Tmin
      p_tset = p_tset *( pmax-pmin ) + pmin
      
      DO i=1,nsamp_testset
        
        
        
        ! Convert pressure from hPa to atm
        pres_atm = p_tset(i)/1013.25d0
        
        ! Compute cross section
        DO n=1,nsub
          
          print*,i,n,'/',nsamp_testset,nsub
          
          ntpos = idw(n+1)-idw(n)+1
          
          call get_hitran_crs(the_molecule, ntpos, pos(idw(n):idw(n+1)),&
                  nposI0, posI0, hI0,  &
                  is_wavenum, use_wavelength, 1, pres_atm, T_tset(i), &
                  fwhm, scale_vcd, xsec_tset(idw(n):idw(n+1), i), errstat)
          
        ENDDO
        
        ! Convolve output spectrum with output FWHM and sample to 
        ! output grid
        
        
        
        CALL gauss_f2ci0(pos(1:npos), xsec_tset(1:npos,i) , soltmp(1:npos),&
                         npos, 1, scale_vcd, ofwhm, opos(1:nopos),         &
                         xsec_truth(1:nopos,i), nopos)
        
        
      ENDDO
      
      print*,'Computing LUT XSECTION'
      
      ! Call the LUT cross section 
      CALL get_hitran_lut_crs( outfile, max_ch_len, opos, nopos, posI0, hI0, nposI0, &
                               p_tset, T_tset, nsamp_testset, scale_vcd, ofwhm,.TRUE.,.TRUE.,  &
                               xsec_interp  )
      
      print*,'END'
      
      ! Write the validation set
      ! ------------------------
      
      ! Create the file
      ncid = nccre(trim(testset_outfile), ncclob, rcode)
      if (rcode .eq. -1) then
        print*, ' error in netcdf_out: nccre failed'
      endif
      
      ! Define dimensions
      posdim  = ncddef(ncid,'npos',npos, rcode)
      oposdim = ncddef(ncid,'nopos',nopos,rcode)
      sampdim = ncddef(ncid,'nsamp',nsamp_testset, rcode)
      
      ! Define variables
      posid = ncvdef(ncid,'Wavelength', ncdouble,1, posdim, rcode)
      oposid = ncvdef(ncid,'OutputWavelength', ncdouble,1, oposdim, rcode)
      Tid = ncvdef(ncid,'Temperature',ncdouble,1,sampdim,rcode)
      pid = ncvdef(ncid,'Pressure',ncdouble,1,sampdim,rcode)
      xid = ncvdef(ncid,'CrossSection',ncdouble,2,(/posdim, sampdim/),rcode)
      oxid = ncvdef(ncid,'OutputCrossSection',ncdouble,2,(/oposdim, sampdim/),rcode)
      lxid = ncvdef(ncid,'LUTCrossSection',ncdouble,2,(/oposdim, sampdim/),rcode)
      
      ! switch from define to data mode
      call ncendf (ncid, rcode)
      
      ! Write the data
      call ncvpt(ncid, posid, (/1/),(/npos/),pos(1:npos),rcode)
      call ncvpt(ncid, oposid, (/1/),(/nopos/),opos(1:npos),rcode)
      call ncvpt(ncid, Tid, (/1/),(/nsamp_testset/),T_tset,rcode)
      call ncvpt(ncid, pid, (/1/),(/nsamp_testset/),p_tset,rcode)
      call ncvpt(ncid, xid, (/1,1/),(/npos,nsamp_testset/),xsec_tset,rcode)
      call ncvpt(ncid, oxid, (/1,1/),(/nopos,nsamp_testset/),xsec_truth,rcode)
      call ncvpt(ncid, lxid, (/1,1/),(/nopos,nsamp_testset/),xsec_interp,rcode)
      
      ! Close file
      call ncclos (ncid, rcode)
      
    ENDIF
    
end program hitran_lut
