program xync_transfer
  
  USE GC_parameters_module
  USE GC_variables_module, ONLY : results_dir, nc_imx, nc_jmx
  USE GC_netcdf_module,    ONLY : netcdf_handle_error
  
  IMPLICIT NONE
  INCLUDE 'netcdf.inc'
  
  ! Local variables
  CHARACTER(LEN=max_ch_len) :: nc_xyfile, tmpchar, nc_sfile
  LOGICAL :: xync_exist, snc_exist, first_alloc
  INTEGER :: nvar
  CHARACTER(LEN=max_ch_len),DIMENSION(500) :: varname 
  INTEGER :: rcode, ncid, i , j, n, d
  INTEGER :: sncid, vid, nvdim, stat
  INTEGER, DIMENSION(500) :: out_vid
  INTEGER, DIMENSION(500) :: sdimid, vdim
  
  CHARACTER(LEN=13), PARAMETER :: location = 'xync_transfer'
  CHARACTER(LEN=100)           :: ixstr, iystr
  
  
  ! Dynamically allocate data array
  REAL(KIND=4), ALLOCATABLE, DIMENSION(:)         :: data_1d_r4 ! 
  REAL(KIND=4), ALLOCATABLE, DIMENSION(:,:)       :: data_2d_r4 ! 
  REAL(KIND=4), ALLOCATABLE, DIMENSION(:,:,:)     :: data_3d_r4 ! All X-Y data currently R4 (min dimension=3)
  REAL(KIND=4), ALLOCATABLE, DIMENSION(:,:,:,:)   :: data_4d_r4 ! 
  REAL(KIND=4), ALLOCATABLE, DIMENSION(:,:,:,:,:) :: data_5d_r4 ! 
  INTEGER, DIMENSION(3)                       :: max_dim_3d
  INTEGER, DIMENSION(4)                       :: max_dim_4d
  INTEGER, DIMENSION(5)                       :: max_dim_5d ! Max dimension in X-Y data
  INTEGER, DIMENSION(6)                       :: max_dim_6d
  INTEGER, DIMENSION(7)                       :: start, stride, max_dim_7d
  ! ================================================
  ! xync_transfer starts here
  ! ================================================
  
  ! Get file pre
  CALL GETARG(1, results_dir)
  
  print*,TRIM( ADJUSTL(results_dir) )
  
  nc_xyfile = TRIM(ADJUSTL(TRIM(ADJUSTL(results_dir)) // 'GC_upwelling_output.nc'))
  
  ! First check that file exists
  INQUIRE( FILE= TRIM(ADJUSTL(nc_xyfile)), EXIST=xync_exist )
  IF( .NOT. xync_exist ) THEN
    print*,TRIM(ADJUSTL(nc_xyfile)), ' does not exist'
    STOP
  ENDIF
  
  ! ------------------------------------
  ! Now open XY file and get dimensions
  ! ------------------------------------
  
  CALL get_xync_info( nc_xyfile, nvar, varname,max_dim_3d,         &
                      max_dim_4d,max_dim_5d, max_dim_6d, max_dim_7d )
  
  ! Allocate data read array
  ALLOCATE( data_1d_r4(max_dim_3d(3)) )
  ALLOCATE( data_2d_r4(max_dim_4d(3),max_dim_4d(4)) )
  ALLOCATE( data_3d_r4(max_dim_5d(3),max_dim_5d(4),max_dim_5d(5)) )
  ALLOCATE( data_4d_r4(max_dim_6d(3),max_dim_6d(4),max_dim_6d(5),    &
                       max_dim_6d(6)                             )   )
  ALLOCATE( data_5d_r4(max_dim_7d(3),max_dim_7d(4),max_dim_7d(5),    &
                       max_dim_7d(6),max_dim_7d(7)               )   )
  
  
  
  ! Reopen file in write mode
  rcode = NF_OPEN(trim(nc_xyfile), NF_WRITE, ncid)
  CALL netcdf_handle_error(location,rcode)
  
  ! Attach output variables
  DO n=1,nvar
    rcode = NF_INQ_VARID(ncid,TRIM(ADJUSTL(varname(n))), out_vid(n))
    CALL netcdf_handle_error(location,rcode)
  ENDDO
  
  DO i=1,nc_imx
    print*,'i',i
  DO j=1,nc_jmx
    
    WRITE(ixstr,'(I100)') i
    WRITE(iystr,'(I100)') j
    
    nc_sfile = TRIM(ADJUSTL(TRIM(ADJUSTL(results_dir)) // &
               TRIM(ADJUSTL(ixstr)) // '_' // & 
               TRIM(ADJUSTL(iystr)) // '_' // 'GC_upwelling_output.nc'))
    
    INQUIRE( FILE=nc_sfile, EXIST=snc_exist )
    
    IF( snc_exist ) THEN
      
!       print*,trim(nc_sfile)
      
      ! Open file
      rcode = NF_OPEN(trim(nc_sfile), NF_SHARE, sncid)
      CALL netcdf_handle_error(location,rcode)
      
      DO n=1,nvar
        
        ! Get variable index corresponding to each variable name
        rcode = NF_INQ_VARID(sncid,TRIM(ADJUSTL(varname(n))),vid)
        CALL netcdf_handle_error(location,rcode)
        
        ! Read the dimensions
        rcode = NF_INQ_VARNDIMS(sncid,vid,nvdim)
        CALL netcdf_handle_error(location,rcode)
        rcode = NF_INQ_VARDIMID(sncid,vid,sdimid(1:nvdim))
        CALL netcdf_handle_error(location,rcode)
        
        ! Initialize stride
        stride(:) = 1
        
        ! Create the start and stride variables
        start(1) = i
        start(2) = j
        stride(1) = 1
        stride(2) = 1
        
        DO d=1,nvdim
          rcode = NF_INQ_DIM(sncid,sdimid(d),tmpchar, vdim(d))
          CALL netcdf_handle_error(location,rcode)
          start(2+d) = 1
          stride(2+d) = vdim(d)
        ENDDO
        
        IF( nvdim .eq. 1 ) THEN
          
          ! Read data
          rcode = NF_GET_VARA_REAL(sncid,vid,start(3:3+nvdim-1),stride(3:3+nvdim-1), &
                                  data_1d_r4(1:stride(3))                            )
          CALL netcdf_handle_error(location,rcode)
          
          ! Write to output nf_put_vara_real
          rcode = NF_PUT_VARA_REAL(ncid,out_vid(n),start(1:nvdim+2),stride(1:nvdim+2), &
                                  data_1d_r4(1:stride(3))        )
          CALL netcdf_handle_error(location,rcode)
          
        ELSE IF( nvdim .eq. 2 ) THEN
        
          ! Read data
          rcode = NF_GET_VARA_REAL(sncid,vid,start(3:3+nvdim-1),stride(3:3+nvdim-1), &
                                  data_2d_r4(1:stride(3),1:stride(4))      )
          CALL netcdf_handle_error(location,rcode)
          
          ! Write to output nf_put_vara_real
          rcode = NF_PUT_VARA_REAL(ncid,out_vid(n),start(1:nvdim+2),stride(1:nvdim+2), &
                                  data_2d_r4(1:stride(3),1:stride(4))        )
          CALL netcdf_handle_error(location,rcode)
        
        ELSE IF( nvdim .eq. 3 ) THEN
        
          ! Read data
          rcode = NF_GET_VARA_REAL(sncid,vid,start(3:3+nvdim-1),stride(3:3+nvdim-1), &
                                  data_3d_r4(1:stride(3),1:stride(4),1:stride(5))      )
          CALL netcdf_handle_error(location,rcode)
          
          ! Write to output nf_put_vara_real
          rcode = NF_PUT_VARA_REAL(ncid,out_vid(n),start(1:nvdim+2),stride(1:nvdim+2), &
                                  data_3d_r4(1:stride(3),1:stride(4),1:stride(5))        )
          CALL netcdf_handle_error(location,rcode)
        
        ELSE IF ( nvdim .eq. 4 ) THEN
          
          ! Read data
          rcode = NF_GET_VARA_REAL(sncid,vid,start(3:3+nvdim-1),stride(3:3+nvdim-1), &
                                  data_4d_r4(1:stride(3),1:stride(4),1:stride(5),    &
                                             1:stride(6)                        )    )
          CALL netcdf_handle_error(location,rcode)
          
          ! Write to output nf_put_vara_real
          rcode = NF_PUT_VARA_REAL(ncid,out_vid(n),start(1:nvdim+2),stride(1:nvdim+2), &
                                  data_4d_r4(1:stride(3),1:stride(4),1:stride(5),      &
                                             1:stride(6)                        )      )
          CALL netcdf_handle_error(location,rcode)
          
        
        
        ELSE IF ( nvdim .eq. 5 ) THEN
          
          ! Read data
          rcode = NF_GET_VARA_REAL(sncid,vid,start(3:3+nvdim-1),stride(3:3+nvdim-1), &
                                  data_5d_r4(1:stride(3),1:stride(4),1:stride(5),    &
                                             1:stride(6),1:stride(7)            )    )
          CALL netcdf_handle_error(location,rcode)
          
          ! Write to output nf_put_vara_real
          rcode = NF_PUT_VARA_REAL(ncid,out_vid(n),start(1:nvdim+2),stride(1:nvdim+2), &
                                  data_5d_r4(1:stride(3),1:stride(4),1:stride(5),      &
                                             1:stride(6),1:stride(7)            )      )
          CALL netcdf_handle_error(location,rcode)
          
        ENDIF
        
      ENDDO
      
      ! Close file
      rcode = NF_CLOSE(sncid)
      CALL netcdf_handle_error(location,rcode)
      
    ENDIF
    
  ENDDO
  ENDDO
  
  DEALLOCATE( data_1d_r4 )
  DEALLOCATE( data_2d_r4 )
  DEALLOCATE( data_3d_r4 )
  DEALLOCATE( data_4d_r4 )
  DEALLOCATE( data_5d_r4 )
  
  ! Close file
  rcode = NF_CLOSE(ncid)
  CALL netcdf_handle_error(location,rcode)
  
!   STOP
  
  ! Remove files if succesful 
  print*,'Removing copied files'
  DO i=1,nc_imx
    print*,'i',i
  DO j=1,nc_jmx
    
    WRITE(ixstr,'(I100)') i
    WRITE(iystr,'(I100)') j
    
    nc_sfile = TRIM(ADJUSTL(TRIM(ADJUSTL(results_dir)) // &
               TRIM(ADJUSTL(ixstr)) // '_' // & 
               TRIM(ADJUSTL(iystr)) // '_' // 'GC_upwelling_output.nc'))
    
    INQUIRE( FILE=nc_sfile, EXIST=snc_exist )
    
    IF( snc_exist ) THEN
      
      ! If the file exists and it is successfully written then cleanup
      open(unit=1234, iostat=stat, file=trim(nc_sfile), status='old')
      if (stat == 0) close(1234, status='delete')
      
    ENDIF
    
  ENDDO
  ENDDO
  
end program xync_transfer

subroutine get_xync_info( ncfile, nvar, varname,max_dim_3d,max_dim_4d,max_dim_5d, max_dim_6d, max_dim_7d )
  
  USE GC_parameters_module
  USE GC_variables_module, ONLY : nc_imx, nc_jmx
  USE GC_netcdf_module,    ONLY : netcdf_handle_error
  
  IMPLICIT NONE
  
  INCLUDE 'netcdf.inc'
  
  CHARACTER(LEN=max_ch_len), INTENT(IN)  :: ncfile
  INTEGER,                   INTENT(OUT) :: nvar
  CHARACTER(LEN=max_ch_len),DIMENSION(500), INTENT(OUT) :: varname
  INTEGER, DIMENSION(3), INTENT(OUT) :: max_dim_3d
  INTEGER, DIMENSION(4), INTENT(OUT) :: max_dim_4d
  INTEGER, DIMENSION(5), INTENT(OUT) :: max_dim_5d
  INTEGER, DIMENSION(6), INTENT(OUT) :: max_dim_6d
  INTEGER, DIMENSION(7), INTENT(OUT) :: max_dim_7d
  
  ! Local variables
  CHARACTER(LEN=max_ch_len) :: tmpchar
  INTEGER :: rcode, ncid, xid, yid, i , j, vid, max_ndim, vdim, nvdim
  CHARACTER(LEN=13), PARAMETER :: location = 'get_xync_info'
  INTEGER, DIMENSION(500) :: dimid
  
  ! Open file
  rcode = NF_OPEN(trim(ncfile), NF_SHARE, ncid)
  CALL netcdf_handle_error(location,rcode)
  
  ! Get x dimension
  rcode = NF_INQ_DIMID(ncid, 'ew_npix', xid)
  CALL netcdf_handle_error(location,rcode)
  rcode = NF_INQ_DIM(ncid,xid,tmpchar, nc_imx)
  CALL netcdf_handle_error(location,rcode)
  
  ! Get Y dimension
  rcode = NF_INQ_DIMID(ncid, 'ns_npix', yid)
  CALL netcdf_handle_error(location,rcode)
  rcode = NF_INQ_DIM(ncid,yid,tmpchar, nc_jmx)
  CALL netcdf_handle_error(location,rcode)
  
  ! Get the number of variables
  rcode = NF_INQ_NVARS(ncid, nvar )
  
  max_dim_3d(:) = 1
  max_dim_4d(:) = 1
  max_dim_5d(:) = 1
  max_dim_6d(:) = 1
  max_dim_7d(:) = 1
  max_ndim   = 1
  
  ! Loop over variables
  DO i=1,nvar
    
    rcode = nf_inq_varname(ncid,i,varname(i))
    
    ! Get variable index corresponding to each variable name
    rcode = NF_INQ_VARID(ncid,TRIM(ADJUSTL(varname(i))),vid)
        
    ! Read the dimensions
    rcode = NF_INQ_VARNDIMS(ncid,vid,nvdim)
    rcode = NF_INQ_VARDIMID(ncid,vid,dimid(1:nvdim))
    
    ! Check max dimension
    IF( nvdim .GT. max_ndim ) max_ndim = nvdim
    
    print*,nvdim
    
    DO j=1,nvdim
      
      ! Read dimension
      rcode = NF_INQ_DIM(ncid,dimid(j),tmpchar, vdim)
      IF( nvdim .EQ. 3 ) THEN
        IF ( vdim .GT. max_dim_3d(j) ) max_dim_3d(j) = vdim
      ELSE IF( nvdim .EQ. 4 ) THEN
        IF ( vdim .GT. max_dim_4d(j) ) max_dim_4d(j) = vdim
      ELSE IF( nvdim .EQ. 5 ) THEN
        IF ( vdim .GT. max_dim_5d(j) ) max_dim_5d(j) = vdim
      ELSE IF( nvdim .EQ. 6 ) THEN
        IF ( vdim .GT. max_dim_6d(j) ) max_dim_6d(j) = vdim
      ELSE IF( nvdim .EQ. 7 ) THEN
        IF ( vdim .GT. max_dim_7d(j) ) max_dim_7d(j) = vdim
      ENDIF
      
    ENDDO
    
    print*,TRIM(ADJUSTL(varname(i)))
  ENDDO
  
  print*,max_dim_3d
  print*,max_dim_4d
  print*,max_dim_5d
  print*,max_dim_6d
  print*,max_dim_7d
  
  ! Close file
  rcode = NF_CLOSE(ncid)
  CALL netcdf_handle_error(location,rcode)
  
end subroutine get_xync_info

