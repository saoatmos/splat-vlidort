# ------------------------------------------------------------
# environment & interface modules external to the main package
# ------------------------------------------------------------
OBJECTS_GEOCAPE_MAIN  = $(OBJ)/geocape_tool.o
OBJECTS_MODULE = $(VLIDORT_OBJ)/vlidort_pars.o \
$(VLIDORT_OBJ)/vlidort_inputs_def.o \
$(VLIDORT_OBJ)/vlidort_outputs_def.o \
$(VLIDORT_OBJ)/vlidort_aux.o \
$(VLIDORT_OBJ)/vlidort_inputs.o \
$(VLIDORT_OBJ)/vlidort_lin_inputs_def.o \
$(VLIDORT_OBJ)/vlidort_l_inputs.o \
$(VLIDORT_OBJ)/vlidort_sup_brdf_def.o \
$(VLIDORT_OBJ)/vlidort_sup_ss_def.o \
$(VLIDORT_OBJ)/vlidort_sup_sleave_def.o \
$(VLIDORT_OBJ)/vlidort_sup_def.o \
$(VLIDORT_OBJ)/vlidort_lin_sup_brdf_def.o \
$(VLIDORT_OBJ)/vlidort_lin_sup_ss_def.o \
$(VLIDORT_OBJ)/vlidort_lin_sup_sleave_def.o \
$(VLIDORT_OBJ)/vlidort_lin_sup_def.o \
$(VLIDORT_OBJ)/vlidort_lin_outputs_def.o \
$(VLIDORT_OBJ)/vlidort_work_def.o \
$(VLIDORT_OBJ)/vlidort_lin_work_def.o \
$(VLIDORT_OBJ)/vlidort_Taylor.o \
$(VLIDORT_OBJ)/vlidort_geometry.o \
$(VLIDORT_OBJ)/vlidort_miscsetups.o \
$(VLIDORT_OBJ)/lapack_tools.o \
$(VLIDORT_OBJ)/vlidort_thermalsup.o \
$(VLIDORT_OBJ)/vlidort_multipliers.o \
$(VLIDORT_OBJ)/FO_geometry_Generic.o \
$(VLIDORT_OBJ)/FO_geometry_Routines.o \
$(VLIDORT_OBJ)/FO_SSgeometry_master.o \
$(VLIDORT_OBJ)/FO_DTgeometry_master.o \
$(VLIDORT_OBJ)/FO_VectorSS_Spherfuncs.o \
$(VLIDORT_OBJ)/FO_VectorSS_RTCalcs_I.o \
$(VLIDORT_OBJ)/FO_VectorSS_RTCalcs_ILCS.o \
$(VLIDORT_OBJ)/FO_VectorSS_RTCalcs_ILPS.o \
$(VLIDORT_OBJ)/FO_Thermal_RTCalcs_I.o \
$(VLIDORT_OBJ)/FO_Thermal_RTCalcs_ILCS.o \
$(VLIDORT_OBJ)/FO_Thermal_RTCalcs_ILPS.o \
$(VLIDORT_OBJ)/VFO_Master.o \
$(VLIDORT_OBJ)/VFO_LinMasters.o \
$(VLIDORT_OBJ)/vlidort_corrections.o \
$(VLIDORT_OBJ)/vlidort_intensity.o \
$(VLIDORT_OBJ)/vlidort_writemodules.o \
$(VLIDORT_OBJ)/vlidort_la_miscsetups.o \
$(VLIDORT_OBJ)/vlidort_lp_miscsetups.o \
$(VLIDORT_OBJ)/vlidort_la_corrections.o \
$(VLIDORT_OBJ)/vlidort_lp_corrections.o \
$(VLIDORT_OBJ)/vlidort_ls_corrections.o \
$(VLIDORT_OBJ)/vlidort_l_thermalsup.o \
$(VLIDORT_OBJ)/vlidort_ls_wfsleave.o \
$(VLIDORT_OBJ)/vlidort_ls_wfsurface.o \
$(VLIDORT_OBJ)/vlidort_lbbf_jacobians.o \
$(VLIDORT_OBJ)/vlidort_lp_wfatmos.o \
$(VLIDORT_OBJ)/vlidort_l_writemodules.o \
$(VLIDORT_OBJ)/vlidort_pack.o \
$(VLIDORT_OBJ)/vlidort_unpack.o \
$(VLIDORT_OBJ)/vlidort_l_pack.o \
$(VLIDORT_OBJ)/vlidort_l_unpack.o \
$(VLIDORT_OBJ)/vlidort_lp_pack.o \
$(VLIDORT_OBJ)/vlidort_lp_unpack.o \
$(VLIDORT_OBJ)/vlidort_solutions.o \
$(VLIDORT_OBJ)/vlidort_bvproblem.o \
$(VLIDORT_OBJ)/vlidort_lpc_bvproblem.o \
$(VLIDORT_OBJ)/vlidort_lp_bvproblem.o \
$(VLIDORT_OBJ)/vlidort_lp_solutions.o \
$(VLIDORT_OBJ)/vlidort_lpc_solutions.o \
$(VLIDORT_OBJ)/vlidort_lps_masters.o \
$(VLIDORT_OBJ)/vlidort_io_defs.o \
$(VLIDORT_OBJ)/vbrdf_sup_inputs_def.o \
$(VLIDORT_OBJ)/vbrdf_sup_outputs_def.o \
$(VLIDORT_OBJ)/vbrdf_lin_sup_inputs_def.o \
$(VLIDORT_OBJ)/vbrdf_lin_sup_outputs_def.o \
$(VLIDORT_OBJ)/vsleave_sup_inputs_def.o \
$(VLIDORT_OBJ)/vlidort_sup_accessories.o \
$(VLIDORT_OBJ)/vbrdf_sup_aux.o \
$(VLIDORT_OBJ)/vbrdf_sup_kernels.o \
$(VLIDORT_OBJ)/vbrdf_sup_routines.o \
$(VLIDORT_OBJ)/vbrdf_findpar.o \
$(VLIDORT_OBJ)/vbrdf_lin_sup_kernels.o \
$(VLIDORT_OBJ)/vbrdf_lin_sup_routines.o \
$(VLIDORT_OBJ)/vbrdf_lin_sup_masters.o \
$(GC_OBJ)/GC_time_module.o \
$(GC_OBJ)/GC_utilities_module.o \
$(GC_OBJ)/GC_get_hitran_crs_module.o \
$(GC_OBJ)/GC_parameters_module.o \
$(GC_OBJ)/GC_variables_module.o \
$(GC_OBJ)/GC_error_module.o \
$(GC_OBJ)/GC_read_input_module.o \
$(GC_OBJ)/GC_aerosols_module.o \
$(GC_OBJ)/GC_Aerloading_routines_m.o \
$(GC_OBJ)/GC_clouds_module.o \
$(GC_OBJ)/GC_solar_module.o \
$(GC_OBJ)/GC_xsections_module.o \
$(GC_OBJ)/GC_SciaUSGS_FA_module.o \
$(GC_OBJ)/pppack_module.o \
$(GC_OBJ)/GC_surface_module.o \
$(GC_OBJ)/GC_netcdf_module.o \
$(GC_OBJ)/GC_profiles_module.o \
$(GC_OBJ)/GC_Vlidort_module.o \
$(GC_OBJ)/GC_convolution_module.o \
$(GC_OBJ)/GC_diagnostics_module.o \
$(GC_OBJ)/GC_diag_amf_module.o \
$(GC_OBJ)/GC_diag_profile_module.o \
$(GC_OBJ)/GC_diag_spectrum_module.o \
$(GC_OBJ)/GC_diag_surface_module.o \
$(GC_OBJ)/GC_diag_cldaer_module.o

# --------------- #
# Surface program #
# --------------- #
OBJECTS_GCS_MAIN = $(OBJ)/gc_surface.o
OBJECTS_GCS = $(VLIDORT_OBJ)/vlidort_pars.o \
$(VLIDORT_OBJ)/vlidort_inputs_def.o \
$(VLIDORT_OBJ)/vlidort_outputs_def.o \
$(VLIDORT_OBJ)/vlidort_aux.o \
$(VLIDORT_OBJ)/vlidort_inputs.o \
$(VLIDORT_OBJ)/vlidort_lin_inputs_def.o \
$(VLIDORT_OBJ)/vlidort_l_inputs.o \
$(VLIDORT_OBJ)/vlidort_sup_brdf_def.o \
$(VLIDORT_OBJ)/vlidort_sup_ss_def.o \
$(VLIDORT_OBJ)/vlidort_sup_sleave_def.o \
$(VLIDORT_OBJ)/vlidort_sup_def.o \
$(VLIDORT_OBJ)/vlidort_lin_sup_brdf_def.o \
$(VLIDORT_OBJ)/vlidort_lin_sup_ss_def.o \
$(VLIDORT_OBJ)/vlidort_lin_sup_sleave_def.o \
$(VLIDORT_OBJ)/vlidort_lin_sup_def.o \
$(VLIDORT_OBJ)/vlidort_lin_outputs_def.o \
$(VLIDORT_OBJ)/vlidort_work_def.o \
$(VLIDORT_OBJ)/vlidort_lin_work_def.o \
$(VLIDORT_OBJ)/vlidort_Taylor.o \
$(VLIDORT_OBJ)/vlidort_geometry.o \
$(VLIDORT_OBJ)/vlidort_miscsetups.o \
$(VLIDORT_OBJ)/lapack_tools.o \
$(VLIDORT_OBJ)/vlidort_thermalsup.o \
$(VLIDORT_OBJ)/vlidort_multipliers.o \
$(VLIDORT_OBJ)/FO_geometry_Generic.o \
$(VLIDORT_OBJ)/FO_geometry_Routines.o \
$(VLIDORT_OBJ)/FO_SSgeometry_master.o \
$(VLIDORT_OBJ)/FO_DTgeometry_master.o \
$(VLIDORT_OBJ)/FO_VectorSS_Spherfuncs.o \
$(VLIDORT_OBJ)/FO_VectorSS_RTCalcs_I.o \
$(VLIDORT_OBJ)/FO_VectorSS_RTCalcs_ILCS.o \
$(VLIDORT_OBJ)/FO_VectorSS_RTCalcs_ILPS.o \
$(VLIDORT_OBJ)/FO_Thermal_RTCalcs_I.o \
$(VLIDORT_OBJ)/FO_Thermal_RTCalcs_ILCS.o \
$(VLIDORT_OBJ)/FO_Thermal_RTCalcs_ILPS.o \
$(VLIDORT_OBJ)/VFO_Master.o \
$(VLIDORT_OBJ)/VFO_LinMasters.o \
$(VLIDORT_OBJ)/vlidort_corrections.o \
$(VLIDORT_OBJ)/vlidort_intensity.o \
$(VLIDORT_OBJ)/vlidort_writemodules.o \
$(VLIDORT_OBJ)/vlidort_la_miscsetups.o \
$(VLIDORT_OBJ)/vlidort_lp_miscsetups.o \
$(VLIDORT_OBJ)/vlidort_la_corrections.o \
$(VLIDORT_OBJ)/vlidort_lp_corrections.o \
$(VLIDORT_OBJ)/vlidort_ls_corrections.o \
$(VLIDORT_OBJ)/vlidort_l_thermalsup.o \
$(VLIDORT_OBJ)/vlidort_ls_wfsleave.o \
$(VLIDORT_OBJ)/vlidort_ls_wfsurface.o \
$(VLIDORT_OBJ)/vlidort_lbbf_jacobians.o \
$(VLIDORT_OBJ)/vlidort_lp_wfatmos.o \
$(VLIDORT_OBJ)/vlidort_l_writemodules.o \
$(VLIDORT_OBJ)/vlidort_pack.o \
$(VLIDORT_OBJ)/vlidort_unpack.o \
$(VLIDORT_OBJ)/vlidort_l_pack.o \
$(VLIDORT_OBJ)/vlidort_l_unpack.o \
$(VLIDORT_OBJ)/vlidort_lp_pack.o \
$(VLIDORT_OBJ)/vlidort_lp_unpack.o \
$(VLIDORT_OBJ)/vlidort_solutions.o \
$(VLIDORT_OBJ)/vlidort_bvproblem.o \
$(VLIDORT_OBJ)/vlidort_lpc_bvproblem.o \
$(VLIDORT_OBJ)/vlidort_lp_bvproblem.o \
$(VLIDORT_OBJ)/vlidort_lp_solutions.o \
$(VLIDORT_OBJ)/vlidort_lpc_solutions.o \
$(VLIDORT_OBJ)/vlidort_lps_masters.o \
$(VLIDORT_OBJ)/vlidort_io_defs.o \
$(VLIDORT_OBJ)/vbrdf_sup_inputs_def.o \
$(VLIDORT_OBJ)/vbrdf_sup_outputs_def.o \
$(VLIDORT_OBJ)/vbrdf_lin_sup_inputs_def.o \
$(VLIDORT_OBJ)/vbrdf_lin_sup_outputs_def.o \
$(VLIDORT_OBJ)/vsleave_sup_inputs_def.o \
$(VLIDORT_OBJ)/vlidort_sup_accessories.o \
$(VLIDORT_OBJ)/vbrdf_sup_aux.o \
$(VLIDORT_OBJ)/vbrdf_sup_kernels.o \
$(VLIDORT_OBJ)/vbrdf_sup_routines.o \
$(VLIDORT_OBJ)/vbrdf_findpar.o \
$(VLIDORT_OBJ)/vbrdf_lin_sup_kernels.o \
$(VLIDORT_OBJ)/vbrdf_lin_sup_routines.o \
$(VLIDORT_OBJ)/vbrdf_lin_sup_masters.o \
$(GC_OBJ)/GC_time_module.o \
$(GC_OBJ)/GC_utilities_module.o \
$(GC_OBJ)/GC_get_hitran_crs_module.o \
$(GC_OBJ)/GC_parameters_module.o \
$(GC_OBJ)/GC_variables_module.o \
$(GC_OBJ)/GC_error_module.o \
$(GC_OBJ)/GC_read_input_module.o \
$(GC_OBJ)/GC_aerosols_module.o \
$(GC_OBJ)/GC_Aerloading_routines_m.o \
$(GC_OBJ)/GC_clouds_module.o \
$(GC_OBJ)/GC_solar_module.o \
$(GC_OBJ)/GC_xsections_module.o \
$(GC_OBJ)/GC_SciaUSGS_FA_module.o \
$(GC_OBJ)/pppack_module.o \
$(GC_OBJ)/GC_surface_module.o \
$(GC_OBJ)/GC_netcdf_module.o \
$(GC_OBJ)/GC_profiles_module.o \
$(GC_OBJ)/GC_Vlidort_module.o \
$(GC_OBJ)/GC_convolution_module.o \
$(GC_OBJ)/GC_diagnostics_module.o \
$(GC_OBJ)/GC_diag_amf_module.o \
$(GC_OBJ)/GC_diag_profile_module.o \
$(GC_OBJ)/GC_diag_spectrum_module.o \
$(GC_OBJ)/GC_diag_surface_module.o \
$(GC_OBJ)/GC_diag_cldaer_module.o


# ------------------ #
# HITRAN LUT program #
# ------------------ #
OBJECTS_HITLUT_MAIN = $(OBJ)/hitran_lut.o
OBJECTS_HITLUT = $(VLIDORT_OBJ)/vlidort_pars.o \
$(VLIDORT_OBJ)/vlidort_inputs_def.o \
$(VLIDORT_OBJ)/vlidort_outputs_def.o \
$(VLIDORT_OBJ)/vlidort_sup_brdf_def.o \
$(VLIDORT_OBJ)/vlidort_sup_ss_def.o \
$(VLIDORT_OBJ)/vlidort_sup_sleave_def.o \
$(VLIDORT_OBJ)/vlidort_sup_def.o \
$(VLIDORT_OBJ)/vlidort_io_defs.o \
$(VLIDORT_OBJ)/vlidort_lin_sup_brdf_def.o \
$(VLIDORT_OBJ)/vlidort_lin_sup_ss_def.o \
$(VLIDORT_OBJ)/vlidort_lin_sup_sleave_def.o \
$(VLIDORT_OBJ)/vlidort_lin_sup_def.o \
$(VLIDORT_OBJ)/vlidort_lin_inputs_def.o \
$(VLIDORT_OBJ)/vlidort_lin_outputs_def.o \
$(VLIDORT_OBJ)/vbrdf_sup_inputs_def.o \
$(VLIDORT_OBJ)/vbrdf_sup_outputs_def.o \
$(VLIDORT_OBJ)/vbrdf_lin_sup_inputs_def.o \
$(VLIDORT_OBJ)/vbrdf_lin_sup_outputs_def.o \
$(GC_OBJ)/GC_parameters_module.o \
$(GC_OBJ)/GC_variables_module.o \
$(GC_OBJ)/GC_time_module.o \
$(GC_OBJ)/GC_utilities_module.o \
$(GC_OBJ)/GC_error_module.o \
$(GC_OBJ)/GC_read_input_module.o \
$(GC_OBJ)/GC_solar_module.o \
$(GC_OBJ)/GC_get_hitran_crs_module.o \
$(GC_OBJ)/GC_xsections_module.o \
$(GC_OBJ)/GC_netcdf_module.o

# ---------- #
# XY program #
# ---------- #
OBJECTS_XYTRAN_MAIN = $(OBJ)/xync_transfer.o
OBJECTS_XYT = $(VLIDORT_OBJ)/vlidort_pars.o \
$(VLIDORT_OBJ)/vlidort_inputs_def.o \
$(VLIDORT_OBJ)/vlidort_outputs_def.o \
$(VLIDORT_OBJ)/vlidort_sup_brdf_def.o \
$(VLIDORT_OBJ)/vlidort_sup_ss_def.o \
$(VLIDORT_OBJ)/vlidort_sup_sleave_def.o \
$(VLIDORT_OBJ)/vlidort_sup_def.o \
$(VLIDORT_OBJ)/vlidort_io_defs.o \
$(VLIDORT_OBJ)/vlidort_lin_sup_brdf_def.o \
$(VLIDORT_OBJ)/vlidort_lin_sup_ss_def.o \
$(VLIDORT_OBJ)/vlidort_lin_sup_sleave_def.o \
$(VLIDORT_OBJ)/vlidort_lin_sup_def.o \
$(VLIDORT_OBJ)/vlidort_lin_inputs_def.o \
$(VLIDORT_OBJ)/vlidort_lin_outputs_def.o \
$(VLIDORT_OBJ)/vbrdf_sup_inputs_def.o \
$(VLIDORT_OBJ)/vbrdf_sup_outputs_def.o \
$(VLIDORT_OBJ)/vbrdf_lin_sup_inputs_def.o \
$(VLIDORT_OBJ)/vbrdf_lin_sup_outputs_def.o \
$(GC_OBJ)/GC_parameters_module.o \
$(GC_OBJ)/GC_variables_module.o \
$(GC_OBJ)/GC_error_module.o \
$(GC_OBJ)/GC_netcdf_module.o 

# ---------------------------------- #
# Aerosol optical properties program #
# ---------------------------------- #
OBJECTS_AER_MAIN = $(OBJ)/create_aeroptpro_nc.o
OBJECTS_AER = $(GC_OBJ)/splat_aerpro_ionc.o \
$(GC_OBJ)/mie_code_module.o \
$(GC_OBJ)/GC_netcdf_module.o \
$(GC_OBJ)/GC_parameters_module.o \
$(GC_OBJ)/GC_variables_module.o \
$(GC_OBJ)/GC_error_module.o 
