GEOCAPE Simulation Tool: TEMPO simulations
--------------------------------+------------------------------------------------------
%%% SIMULATION MENU %%%         : 
Output File name pre            : ./test_gdf_unnorm_
Is ASCII input file?            : T
     (T) Single ASCII           : ../test/AtmProfiles_Data_AFGLUS.asc
     (F) X-Y NetCDF             : /data/tempo1/Shared/cmiller/GEOSATpy/output/tempo_xy_nc_casestudy/TEMPO_20130715_19_ATL_GCinp.nc
          - X index             : 1 1
          - Y index             : 1 1
          - Output to X-Y       : T
            - Use file lock     : F
            - Overwrite Exist.  : T
            - Only Create File  : F
          - INR Corner Defn.    : T
Root data directory             : ../data/
Switch on debug?                : F
Debug filename                  : debug_geocape_tool.log
--------------------------------+------------------------------------------------------
%%% VLIDORT OPTIONS %%%         :
Full Stokes Vector Calc?        : T
  - Number of Vector Components : 3
Do thermal emission?            : F
Do surface emission?            : F
Output upwelling radiation?     : T
Output downwelling radiation?   : F
# of half-space streams         : 4
# of p. func. exp. coeffs       : 1000
Do Debug write?                 : F
--------------------------------+------------------------------------------------------
%%% PROFILE OPTIONS %%%         :
Use profile file footprint info : F
  (F) - Year                    : 2007
      - Month                   : 07
      - Day                     : 15
      - Hour (decimal)          : 21.0
      - Longitude (deg)         : -87.95
      - Latitude (deg)          :  32.05
      - Altitude (km)           :  0.1041
      - Pixel corners --------->:   BotLeft  TopLeft TopRight BotRight
               - Longitudes     :     -88.0    -88.0    -87.9    -87.9
               - Latitudes      :      32.0     32.1     32.1     32.0
      - Satellite Longitude     :  -100.0 
      - Satellite Latitude      :     0.0
      - Satellite Altitude (km) : 35786.0
      - SZA (deg)               :     0.015
      - SAA (deg)               :     0.015
      - VZA (deg)               :    45.0
      - AZA (deg)               :     0.0
      - Wind dir.(deg,C/W frm N):     0.0 
      - Wind speed (m/s)        :     5.0
      - [Chlorophyll] (mg/m3)   :     0.0
      - Salinity (pptv)         :   300.0
      - Land Snow Fraction      :     0.0
Recompute Viewing Geometry?     : F
Terrain Height Adjustment       : F
  (T)- Path to Elevation map    : ../data/USGS_elevation/dem90ARC_W180N90.nc
     - Interp Option (1-2)      : 2
Do user altitudes               : F
  (T)- # of altitude levels     : 2
     - Altitude level # 1       :  6.2d0
     - Altitude level # 2       : 80.0d0
  (F) Use Obs. Alt. from prof?  : F
--------------------------------+------------------------------------------------------
%%% SPECTRUM OPTIONS %%%        : 
Output in wavelength(nm) F=1/cm : T
Start wavelength                : 300.0
End wavelength                  : 1600.0
Resolution at FWHM              : 0.0
Spectral interval for output    : 10.0
Spectral interval for calc.     : 10.0
Do normalized radiance          : T
Solar spectrum                  : ../data/SolarSpectra/chance_jpl_mrg_hitran_200-16665nm.nc
Output in photons/cm^2/nm/s     : T
Normalized WF Output            : F
--------------------------------+------------------------------------------------------
%%% SURFACE OPTIONS %%%         :
Surface Reflectance  Option     : 4
 (1) Fixed Lambertian           : 
     - Albedo Value             : 0.05
 (2) Lambertian Spectrum        : 
     - Path to file             : ../data/ReflSpectra/grass_ASTER.dat
 (3) LER Climatology            : 
     - Path to file             : ../data/LER_climatologies/OMI-Aura_L3-OMLER_2005m01-2009m12_v003-2010m0503t063707.nc
     - Use Constant Wavelength? : F
        (T) LER Wavelength (nm) : 335.0
 (4) MODIS-FA                   :
     - Input FA file            : ../data/BRDF_EOF/AlbSpec/spatial_db_scia_mrg_usgs_FA_n16_12Ksamp_splib07a_extended.nc
     - Do Isotropic             : F
      (T) 1-Blck/2-Whte/3-Blue  : 3
      (F) Ocean Glint BRDF      : T
 (5) Fixed Kernel BRDF          : 
     - VLIDORT option---------->: KernelName Idx Amplitude #Par     Par1     Par2     Par3     Par4
                - Kernel 1      : Lambertian   1   1.00000    0 0.000000 0.000000 0.000000
                - Kernel 2      : LiSparse     4   0.00000    2 2.000000 1.000000 0.000000 
                - Kernel 3      : Ross-thick   3   0.00000    0 0.000000 0.000000 0.000000
 (6) BRDF Climatology           : 
     - Input climatology        : mars_TES.nc
 (7) New CM Glint Kernel        :
     - Do glint shadow          : T
     - Do whitecaps             : T
     - Do facet isotropy        : T 
Do plant fluorescence?          : F
--------------------------------+------------------------------------------------------
%%% TRACE GAS OPTIONS %%%       :
Use effective cross sections?   : T
Split RRS/Cabannes P. Func.?    : T
  (T) Consider RRS as abs.?     : F
Trace gases to include          : 
Cross Section Entries --------->:  Name CIA?  CIA1  CIA2 XsType XsectFilename (Full path:RootDataDirectory/SAO_crosssections/XsectFilename)
                                :    O3    F     -     -      2 TEMPO_o3abs_brion_270_800_vacfinal.dat
                                :   NO2    F     -     -      4 no2_tpar_vandaele_2003.dat
                                :  HCHO    F     -     -      1 TEMPO_h2co_300K.dat
                                :   SO2    F     -     -      1 TEMPO_so2_295K.dat
                                :   H2O    F     -     -      6 hitran_lut/h2o_lut_280-800nm_0p6fwhm_1e21vcd.nc
                                :  GLYX    F     -     -      1 TEMPO_glyx_296K.dat
                                :   BRO    F     -     -      1 TEMPO_bro_228K.dat
                                :    IO    F     -     -      1 io_298k_bremen.dat
                                :    CO    F     -     -      3 HITRAN
                                :   CO2    F     -     -      3 HITRAN
                                :   N2O    F     -     -      3 HITRAN
                                :   CH4    F     -     -      3 HITRAN
                                :    O2    F     -     -      3 HITRAN 
                                :    NO    F     -     -      3 HITRAN
                                :  HNO3    F     -     -      3 HITRAN
                                :   OCS    F     -     -      3 HITRAN
                                :    O4    T    O2    O2      2 TEMPO_o4_par_Thalman_250_800.dat
                                :  OCLO    F     -     -      1 oclo_213k_sciafm.dat
                                :  ###END_OF_LIST###
Assume Earth Bulk Atmosphere?   : T
  (T) CO2 Concentration (ppmv)  : 385.0
  (F) Gravitational Accel. (m/s): 3.711
      Bulk constituents to inc. : N2 O2 Ar CO2
Bulk Constituent Entries ------>: Name  MW(g/mol)  MixingRatio   RI_Name
                                :  CO2    44.01       0.000385       CO2
                                :   Ar    39.948      0.009300        Ar
                                :   N2    28.0134     0.780840        N2
                                :   O2    31.998      0.209460        O2
                                :  ###END_OF_LIST###
--------------------------------+-------------------------------------------------------
%%% AEROSOL OPTIONS %%%         :
Do aerosols?                    : T
 (T) Aerosol Names              : SU
     Aerosol Entries ---------->: Name    RHDep   AerOptFilename (Full Path:RootDataDirectory/AerCldProp/)
                                :   SU        T   SU_200nm-20um_wgphase_flexinput.nc
                                :   BC        T   BC_200nm-20um_wgphase_flexinput.nc
                                :   OC        T   OC_200nm-20um_wgphase_flexinput.nc
                                :   SF        T   SF_200nm-20um_wgphase_flexinput.nc
                                :   SC        T   SC_200nm-20um_wgphase_flexinput.nc
                                :   DU        F   DU_200nm-20um_wgphase_flexinput.nc
                                : ###END_OF_LIST### 
     AOD wavelength (nm)        : 550.0
     AOD from profile file      : F
        (F) - Profile Par.----->: Name   AOD ProfType LowerZ UpperZ PeakZ  H.Width  Relaxation  <-(km)
                                :   SU  1.00      GDF    0.0   12.0   6.0      0.5         0.0  
                                :   BC  0.01      EXP    0.0   12.0   6.0      0.5         1.0
                                :   OC  0.01      BOX    0.0   12.0   6.0      0.5         0.0
                                :   SF  0.01      GDF    0.0   12.0   6.0      0.5         0.0
                                :   SC  0.01      GDF    0.0   12.0   6.0      0.5         0.0
                                :   DU  0.01      GDF    0.0   12.0   6.0      0.5         0.0
                                : ###END_OF_LIST###
     Aerosol Column WF          : F
--------------------------------+------------------------------------------------------
%%% CLOUD OPTIONS %%%           :
Do Clouds?                      : F
 (T) Cloud Names                : 
     Cloud Entries ------------>: Name    CldOptFilename (Full Path:RootDataDirectory/AerCldProp/)
                                :   CW    WC_200nm-20um_wgphase_flexinput.nc
                                :   CI    WC_200nm-20um_wgphase_flexinput.nc
                                : ###END_OF_LIST###
     COD Wavelength (nm)        : 550.0
     Do Lambertian Clouds?      : F
       (T) - Cloud Albedo       : 0.8
     Use profile file info?     : F
       (F) - # Subpixels        : 2
           - Cloud Fractions    : 0.8    0.01
  (Lam.)-> - C.Pres(hpa)        : 500.0 200.0
 (Scat.)-> - Profile Params---->:  Name Pixel   COD ProfType LowerZ UpperZ PeakZ  H.Width  Relaxation  <-(km) 
                                :    CW     1  10.0      BOX    2.0    5.0   4.0      1.0         0.0
                                :    CW     2   0.1      BOX   10.0   12.0  11.0      1.0         0.0
                                :    CI     2   0.9      BOX   10.0   12.0  11.0      1.0         0.0
                                : ###END_OF_LIST###
     Cloud Column WF            : F
--------------------------------+------------------------------------------------------
%%% DIAGNOSTIC OPTIONS %%%      :
1- Do AMF calculation           : F
   - AMF Species                : all 
   - Output Scat. Weights?      : T
2- Write Viewing Geometry       : T
3- Write Footprint information  : T
Profile Output Options          :
  4- Altitude profile           : T
  5- Pressure profile           : T
  6- Temperature profile        : T
  7- Partial air col. profile   : T
  8- AOD @ reference wvl        : T
       -> Output total AOD      : F
       -> Species AOD           : all
  9- COD @ reference wvl        : F
 10- Total OD (all wvl)         : F
 11- Total SSA (all wvl)        : T
 12- Tot. Aerosol OD (all wvl)  : F
 13- Tot. Aerosol SSA (all wvl) : F
 14- Tot. Cloud OD (all wvl)    : F
 15- Tot. Cloud SSA (all wvl)   : F
 16- Partial Gas Columns        : T
       -> Output Species        : all
 17- BLANK                      : F
 18- Gas Cross sections         : T
       -> Also output derivs?   : F
       -> Output Species        : all
Spectrum output options         : 
 19- Wavelength grid            : T
 20- Radiance                   : T
      -> Stokes output (I,Q,U)? : F
 21- Flux                       : F
      -> Stokes output (I,Q,U)? : F
 22- Direct Flux                : F
      -> Stokes output (I,Q,U)? : F
 23- Solar irradiance           : F
Surface Output                  : 
 24- Surface albedo (lambert.)  : F
 25- BRDF Kernel Amplitudes     : T
      -> Save Kern. Par?        : T
 26- SIF                        : F
 27- MODIS Land Fraction        : F
Surface Jacobians               : 
 28- Surface Reflect. Jacobian  : T
      -> Amplitude Deriv.?      : T
      -> BRDF Parameters        : F
      -> Stokes output (I,Q,U)? : F
 29- (DEPRICATED)               : F
      -> Stokes output (I,Q,U)? : F
Met. Profile Jacobians          :
 30- Temperature                : F
 31- Surface Pressure           : F
      -> Do OCO2 Sigma style?   : F
      -> Output A.D. Jac?       : F
      -> Stokes output (I,Q,U)? : F
Trace Gas Jacobians             :
 32- Gas absorption             : F
      -> Output Species         : all
      -> Stokes output (I,Q,U)? : F
 33- RRS pseudoabsorption       : F
Aerosol Jacobians               : 
 34- AOD (WF=T/F)               : T
     -> Stokes output (I,Q,U)?  : F
     -> Total AOD               : T
     -> Species AOD             : all
     -> Species Profile Param.  : all
 35- Aer. SSA (WF=T/F)          : F
     -> Stokes output (I,Q,U)?  : F
     -> Total SSA               : F
     -> Species SSA             : all
     -> Species Profile Param.  : all
Cloud Jacobians                 :
 36- Cloud fraction             : F do_cfrac_jacobians
     -> Stokes output (I,Q,U)?  : F
 37- Cloud top pressure         : F do_ctp_jacobians
     -> Stokes output (I,Q,U)?  : F
 38- Tot. Opt. Depth (WF=T/F)   : F do_cod_Jacobians
     -> Stokes output (I,Q,U)?  : F
 39- Tot. Cld. SSA (WF=T/F)     : F do_cssa_Jacobians
     -> Stokes output (I,Q,U)?  : F
--------------------------------+------------------------------------------------------
END OF FILE                     :
--------------------------------+------------------------------------------------------
