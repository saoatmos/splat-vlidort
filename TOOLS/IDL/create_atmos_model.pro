;+
; NAME:
;      create_atmos_model
;
; PURPOSE:
;       Description: Create netcdf file to input atmospheric models
;       in SPLAT-VLIDORT.
;
;       !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
;       The user needs to modify this script to provide values with
;       geophysical meaning.
;       !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
;
;       Six dimensions are relevant to define data fields in the
;       netcdf file:
;       1. nx (horizontal)
;       2. ny (horizontal)
;       3. nz (vertical)
;       4. nc_max (cloud pixels within a pixel)
;       5. ng (number of gases)
;       6. na (number of aerosols)
;       7. nct (number of cloud types)
;
;       The user should provide data for the following
;       variables. Depending on the SPLAT-VLIDORT set up all these
;       information is not needed.
;       --> filename: Output filename
;       --> AZA: 2D [nx,ny] float array of relative azimuth angles [degrees]
;       --> Altitude: 3D [nx,ny,nz+1] float array of altitudes [km]
;       --> Chlorophyll: 2D [nx,ny] float array of chlorophyll
;       concentrations [mg/m3]
;       --> CloudPixelFractions: 3D [nx,ny,nc_max] float array of cloud fractions
;       --> CloudTopPressure: 3D [nx,ny,nc_max] float array of top cloud
;       pressure [hPa]
;       --> CornerLatitudes: 3D [nx,ny,4] float array of pixel
;       corner latitudes going from SW to NW to NE and SE
;       --> CornerLongitudes: 3D [nx,ny,4] float array of pixel
;       corner latitudes going from SW to NW to NE and SE
;       --> Day: 2D [nx,ny] integer array of day of observation (within
;       a month)
;       --> DoCalculation: 2D [nx,ny] integer array of flags to do
;       radiative transfer calculation (set to 1 for TRUE)
;       --> EtaA: 1D [nz+1] float array of EtaA coordinate
;       --> EtaB: 1D [nz+1] float array of EtaB coordinate
;       --> Hour: 2D [nx,ny] float array of UTC decimal hour of
;       observation
;       --> IsOcean: 2D [nx,ny] integer array of flag for ocean scene
;       (set to 1 for true)
;       --> LandTypeFraction: 3D [nx,ny,17] float array of land type
;       fractions  following IGBP classification
;       --> Latitude: 2D [nx,ny] float array of latitudes [degrees]
;       --> Longitude: 2D [nx,ny] float array of longitudes [degrees]
;       --> Month: 2D [nx,ny] integer array of month of observation [1-12]
;       --> NumberOfCloudPixels: [nx,ny] integer array of number of clouds
;       in  each pixel. Cloud dimension is == to
;       MAX(NumberOfCloudPixels)
;       --> ObservationAltitude: [nx,ny] long array of observation
;       altitudes [km]
;       --> OceanSalinity: 2D [nx,ny] float array of ocean water salinity
;       [ppt] 
;       --> Pressure: 3D [nx,ny,nz+1] float array of pressures [hPa]
;       --> ProfileIsLevel: Integer set to 1 if profile variables at
;       grid midpoints (Altitude,Pressure,TATM)
;       --> RH: 3D [nx,ny,nz] float array of relative humidity [%]
;       --> SAA: 2D [nx,ny] float array of solar azimuth angles [degrees]
;       --> SIF_734nm: 2D [nx,ny] float array of solar induced fluorescence
;       at 734nm [mW/m2/nm/Sr]
;       --> SZA: 2D [nx,ny] float array of solar zenith angles [degrees]
;       --> SeaIceFraction: 2D [nx,ny] float array of fraction of sea ice
;       --> SnowDepth: 2D [nx,ny] float array of snow depth [m]
;       --> SnowFraction: 2D [nx,ny] float array of land snow cover
;       fraction
;       --> SnowMass: 2D [nx,ny] float array of land snow mass [kg/m2]
;       --> SurfacePressure: 2D [nx,ny] float array of surface pressure
;       [hPa]
;       --> SurfaceTemperature: 2D [nx,ny] float array of surface
;       temperature [k]
;       --> TATM: 3D [nx,ny,nz+1] float array of temperature profile [K]
;       --> TerrainHeight: 2D [nx,ny] float array of terrain height [Km]
;       --> TopSnowAge: 2D [nx,ny] float array of top snow age in days
;       --> VAA: 2D [nx,ny] float array of veiwing azimuth angle
;       [degrees]
;       --> VZA: 2D [nx,ny] float array of veiwing zenith angle
;       [degrees]
;       --> WindDirection: 2D [nx,ny] float array of surface wind
;       directions [degrees]
;       --> WindSpeed: 2D [nx,ny] float array of surface wind speed
;       [m/s]
;       --> Year: 2D [nx,ny] integer array of year of the observation
;       --> GasList: 1D [ng] string array with names of gases to be
;       included. Recomended to use BRO, GLYX, H2O, HCHO, NO2, O3,
;       OCLO, as names for the most common ones
;       --> Gasfields: 4D [nx,ny,nz,ng] float array of gas vertical
;       profiles [VMR]
;       --> AerList: 1D [na] string array with names of aerosols to be
;       included. Recomended to use OBC, ODU, OOC, OSC, OSF, OSU as
;       names for the most common ones.
;       --> Aerfields: 4D [nx,ny,nz,na] float array of aerosol optical
;       depth vertical profiles
;       --> CldList: 1D [nct] string array with names of clouds to be
;       included. Recomended to use OCI and OCW names for the most
;       common  ones.
;       --> Cldfields: 4D [nx,ny,nz,nc_max,nct] float array of cloud optical
;       depth vertical profiles
;
; CALLING SEQUENCE:
;       create_atmos_model
;
; INPUTS:
;       NONE
;       
; MODIFICATION HISTORY:
;       Written by: Gonzalo Gonzalez Abad @ SAO, May 2018
;-
PRO create_atmos_model

; Define output filename
filename = 'SPLAT-VLIDORT-test-input.nc'

; Define dimensions
nx = 100
ny = 2000
nz = 47
nc_max = 2
ng = 5
na = 4
nct = 3

; Define variables:
AZA = RANDOMU(dummy,[nx,ny])
Altitude = RANDOMU(dummy,[nx,ny,nz+1])
Chlorophyll = RANDOMU(dummy,[nx,ny])
CloudPixelFractions = RANDOMU(dummy,[nx,ny,nc_max])
CloudTopPressure = RANDOMU(dummy,[nx,ny,nc_max])
CornerLatitudes = RANDOMU(dummy,[nx,ny,4])
CornerLongitudes = RANDOMU(dummy,[nx,ny,4])
Day = FIX(RANDOMU(dummy,[nx,ny])*31.0)
DoCalculation = FIX(RANDOMU(dummy,[nx,ny])*2.0)
EtaA = RANDOMU(dummy,[nz+1])
EtaB = RANDOMU(dummy,[nz+1])
Hour = RANDOMU(dummy,[nx,ny])
IsOcean = FIX(RANDOMU(dummy,[nx,ny])*2.0)
LandTypeFraction = RANDOMU(dummy,[nx,ny,17])
Latitude = RANDOMU(dummy,[nx,ny])
Longitude = RANDOMU(dummy,[nx,ny])
Month = FIX(RANDOMU(dummy,[nx,ny])*13.0)
NumberOfCloudPixels = FIX(RANDOMU(dummy,[nx,ny])*nc_max)
ObservationAltitude = RANDOMU(dummy,[nx,ny])
OceanSalinity = RANDOMU(dummy,[nx,ny])
Pressure = RANDOMU(dummy,[nx,ny,nz+1])
ProfileIsLevel = 0
RH = RANDOMU(dummy,[nx,ny,nz])
SAA = RANDOMU(dummy,[nx,ny])
SIF_734nm = RANDOMU(dummy,[nx,ny])
SZA = RANDOMU(dummy,[nx,ny])
SeaIceFraction = RANDOMU(dummy,[nx,ny])
SnowDepth = RANDOMU(dummy,[nx,ny])
SnowFraction = RANDOMU(dummy,[nx,ny])
SnowMass = RANDOMU(dummy,[nx,ny])
SurfacePressure = RANDOMU(dummy,[nx,ny])
SurfaceTemperature = RANDOMU(dummy,[nx,ny])
TATM = RANDOMU(dummy,[nx,ny,nz+1])
TerrainHeight = RANDOMU(dummy,[nx,ny])
TopSnowAge = RANDOMU(dummy,[nx,ny])
VAA = RANDOMU(dummy,[nx,ny])
VZA = RANDOMU(dummy,[nx,ny])
WindDirection = RANDOMU(dummy,[nx,ny])
WindSpeed = RANDOMU(dummy,[nx,ny])
Year = FIX(RANDOMU(dummy,[nx,ny])*2020.0)
GasList = ['Gas1','Gas2','Gas3','Gas4','Gas5']
GasFields = RANDOMU(dummy,[nx,ny,nz,ng])
AerList = ['Aer1','Aer2','Aer3','Aer4']
AerFields = RANDOMU(dummy,[nx,ny,nz,na])
CldList = ['Cld1','Cld2','Cld3']
CldFields = RANDOMU(dummy,[nx,ny,nz,nc_max,nct])
 
atmos_model_template, filename, nx, ny, nz, nc_max, $
                      gas_list=GasList, aer_list=AerList, $
                      cld_list=CldList

atmos_model_write,  filename, nx, ny, nz, nc_max, ng, na, nct, $
                    AZA=AZA, Altitude=Altitude, $
                    Chlorophyll=Chlorophyll, $
                    CloudPixelFractions=CloudPixelFractions, $
                    CloudTopPressure=CloudTopPressure, $
                    CornerLatitudes=CornerLatitudes, $
                    CornerLongitudes=CornerLongitudes, $
                    Day=Day, DoCalculation=DoCalculation, $
                    EtaA=EtaA, EtaB=EtaB, Hour=Hour, IsOcean=IsOcean, $
                    LandTypeFraction=LandTypeFraction, $                     
                    Latitude=Latitude, Longitude=Longitude, Month=Month, $
                    NumberOfCloudPixels=NumberOfCloudPixels, $
                    ObservationAltitude=ObservationAltitude, $
                    OceanSalinity=OceanSalinity, Pressure=Pressure, $
                    ProfileIsLevel=ProfileIsLevel, RH=RH, SAA=SAA, $
                    SIF_734nm=SIF_734nm, SZA=SZA, $
                    SeaIceFraction=SeaIceFraction, SnowDepth=SnowDepth, $
                    SnowFraction=SnowFraction, SnowMass=SnowMass, $
                    SurfacePressure=SurfacePressure, $
                    SurfaceTemperature=SurfaceTemperature, $
                    TATM=TATM, TerrainHeight=TerrainHeight,$
                    TopSnowAge=TopSnowAge, VAA=VAA, VZA=VZA, $
                    WindDirection=WindDirection, WindSpeed=WindSpeed, $
                    Year=Year, GasList=GasList, Gasfields=Gasfields, $
                    AerList=AerList, Aerfields=Aerfields, CldList=CldList, $
                    Cldfields=Cldfields

END
