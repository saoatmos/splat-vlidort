PRO GT30_TIF2NC
  
  flist = file_search('./','*.tif')
  nf = n_elements(flist)
;   nf =2
  for f=0,nf-1 do begin
    
    ; Read elevation
    d = read_elev(flist[f])
    
    ; Get file name
    frag = strsplit(flist[f],'/',/extract)
    fname = frag[n_elements(frag)-1]
    
    ; Replace extension
    frag = strsplit(fname,'.',/extract)
    outfile = './'+frag[0]
    for i=1,n_elements(frag)-2 do outfile = outfile + '.' + frag[i]
    outfile = outfile + '.nc'
    
    ; Echo outfile
    print,outfile
    
    ; Create NetCDF
    id = ncdf_create(outfile,/clobber)
    
    ; Fill
    ncdf_control,id,/fill
    
    ; Define dimensions
    xid = ncdf_dimdef(id,'x',n_elements(d.xmid))
    yid = ncdf_dimdef(id,'y',n_elements(d.ymid))
    
    ; Define variables
    hid = ncdf_vardef(id,'Altitude',[xid,yid],/short)
    lon_id = ncdf_vardef(id,'Longitude',xid,/float)
    lat_id = ncdf_vardef(id,'Latitude',yid,/float)
    
    NCDF_ATTPUT, id, hid, 'long_name', 'Altitude above sea level (m)'
    NCDF_ATTPUT, id, lon_id, 'long_name', 'Longitude (degrees)'
    NCDF_ATTPUT, id, lat_id, 'long_name', 'Latitude (degrees)'
    
    ; Put file in data mode:
    NCDF_CONTROL, id, /ENDEF
    
    ; Put data
    ncdf_varput,id,hid,d.alt
    ncdf_varput,id,lon_id,d.xmid
    ncdf_varput,id,lat_id,d.ymid
    
    NCDF_CLOSE, id ; Close the NetCDF file.
  endfor
  
END