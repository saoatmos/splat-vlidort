;+
; NAME:
;       atmos_model_template
;
; PURPOSE:
;       Description: Template to create netcdf file to input atmospheric models
;       in SPLAT-VLIDORT. Some fields are obligatory while gas, aerosol, and
;       cloud fields are optional and their creation depend on the values 
;       provided to the template.
;
; CALLING SEQUENCE:
;       atmos_model_template, filename, nx, ny, nz, nc_max,
;       gas_list=gas_list, aer_list=aer_list, cld_list=cld_list
;
; INPUTS:
;       filename: Name of model file (String)
;       nx: Size of x dimension (forced to 1 if < 1) (Integer)
;       ny: Size of y dimension (forced to 1 if < 1) (Integer)
;       nz: Size of z dimension (forced to 1 if < 1) (Integer)
;       nc_max: Size of nc_max dimension (forced to 1 if < 1)
;       (Integer)
;
; OPTIONAL INPUTS:
;       gas_list: List of gas field names (String array)
;       aer_list: List of aerosol field names (String array)
;       cld_list: List of cloud field names (String array)
;       
; MODIFICATION HISTORY:
;       Written by: Gonzalo Gonzalez Abad @ SAO, May 2018
;-
PRO atmos_model_template, filename, nx, ny, nz, nc_max, $
                          gas_list=gas_list, aer_list=aer_list, cld_list=cld_list

; Check that dimensions fillup minimum requeriments and if not force
; them
IF (nx LT 1) THEN nx = 1 & nxe = nx+1
IF (ny LT 1) THEN ny = 1 & nye = ny+1
IF (nz LT 1) THEN nz = 1 & nze = nz+1
IF (nc_max LT 1) THEN nc_max = 1
IF (keyword_set(gas_list)) THEN ngas=n_elements(gas_list)
IF (keyword_set(aer_list)) THEN naer=n_elements(aer_list)
IF (keyword_set(cld_list)) THEN ncld=n_elements(cld_list)

; Create file
id = ncdf_create(filename,/clobber,/netcdf4_format)

; Set fill values
ncdf_control, id, /fill

; Define dimensions
oneid = ncdf_dimdef(id,'one',1)
fourid = ncdf_dimdef(id,'four',4)
lid = ncdf_dimdef(id,'l',17)
nc_maxid = ncdf_dimdef(id,'nc_max',nc_max)
xid = ncdf_dimdef(id,'x',nx)
xeid = ncdf_dimdef(id,'xe',nxe)
yid = ncdf_dimdef(id,'y',ny)
yeid = ncdf_dimdef(id,'ye',nye)
zid = ncdf_dimdef(id,'z',nz)
zeid = ncdf_dimdef(id,'ze',nze)

; Create obligatory fields
vid = ncdf_vardef(id,'AZA',[xid,yid],/float)
ncdf_attput, id, vid, 'long_name','Relative Azimuth Angle in degrees'
vid = ncdf_vardef(id,'Altitude',[xid,yid,zeid],/float)
ncdf_attput, id, vid, 'long_name','Altitude in km'
vid = ncdf_vardef(id,'Chlorophyll',[xid,yid],/float)
ncdf_attput, id, vid, 'long_name','Chlorophyll concentration in mg/m3'
vid = ncdf_vardef(id,'CloudPixelFractions',[xid,yid,nc_maxid],/float)
ncdf_attput, id, vid, 'long_name','Geometric cloud fraction for each pixel and cloud'
vid = ncdf_vardef(id,'CloudTopPressure',[xid,yid,nc_maxid],/float)
ncdf_attput, id, vid, 'long_name','Cloup top pressure for each pixel and cloud in hPa'
vid = ncdf_vardef(id,'CornerLatitudes',[xid,yid,fourid],/float)
ncdf_attput, id, vid, 'long_name','Latitudes at pixel corners in degrees order: SW,NW,NE,SE'
vid = ncdf_vardef(id,'CornerLongitudes',[xid,yid,fourid],/float)
ncdf_attput, id, vid, 'long_name','Longitudes at pixel corners in degrees order: SW,NW,NE,SE'
vid = ncdf_vardef(id,'Day',[xid,yid],/short)
ncdf_attput, id, vid, 'long_name','Day of observation (within a month)'
vid = ncdf_vardef(id,'DoCalculation',[xid,yid],/short)
ncdf_attput, id, vid, 'long_name','Flag to do radiative transfer calculation'
vid = ncdf_vardef(id,'EtaA',[zeid],/float)
ncdf_attput, id, vid, 'long_name','EtaA Coordinate'
vid = ncdf_vardef(id,'EtaB',[zeid],/float)
ncdf_attput, id, vid, 'long_name','EtaB Coordinate'
vid = ncdf_vardef(id,'Hour',[xid,yid],/float)
ncdf_attput, id, vid, 'long_name','UTC decimal hour of observation'
vid = ncdf_vardef(id,'IsOcean',[xid,yid],/short)
ncdf_attput, id, vid, 'long_name','Flag for ocean scene'
vid = ncdf_vardef(id,'LandTypeFraction',[xid,yid,lid],/float)
ncdf_attput, id, vid, 'long_name','Land type fraction (IGBP Classification)'
vid = ncdf_vardef(id,'Latitude',[xid,yid],/float)
ncdf_attput, id, vid, 'long_name','Latitude in degrees'
vid = ncdf_vardef(id,'Longitude',[xid,yid],/float)
ncdf_attput, id, vid, 'long_name','Longitude in degrees'
vid = ncdf_vardef(id,'Month',[xid,yid],/short)
ncdf_attput, id, vid, 'long_name','Month of observation'
vid = ncdf_vardef(id,'NumberOfCloudPixels',[xid,yid],/short)
ncdf_attput, id, vid, 'long_name','Number of cloudy pixels'
vid = ncdf_vardef(id,'ObservationAltitude',[xid,yid],/float)
ncdf_attput, id, vid, 'long_name','Altitude in km'
vid = ncdf_vardef(id,'OceanSalinity',[xid,yid],/float)
ncdf_attput, id, vid, 'long_name','Ocean water salinity in ppt'
vid = ncdf_vardef(id,'Pressure',[xid,yid,zeid],/float)
ncdf_attput, id, vid, 'long_name','Pressure in hPa'
vid = ncdf_vardef(id,'ProfileIsLevel',[oneid],/short)
ncdf_attput, id, vid, 'long_name','Flag for profile variables at grid midpoints (Altitude,Pressure,TATM)'
vid = ncdf_vardef(id,'RH',[xid,yid,zid],/float)
ncdf_attput, id, vid, 'long_name','Relative humidity %'
vid = ncdf_vardef(id,'SAA',[xid,yid],/float)
ncdf_attput, id, vid, 'long_name','Solar azimuth angle in degrees'
vid = ncdf_vardef(id,'SIF_734nm',[xid,yid],/float)
ncdf_attput, id, vid, 'long_name','Solar induced fluorescence at 734nm (mW/m2/nm/Sr)'
vid = ncdf_vardef(id,'SZA',[xid,yid],/float)
ncdf_attput, id, vid, 'long_name','Solar zenith angle in degrees'
vid = ncdf_vardef(id,'SeaIceFraction',[xid,yid],/float)
ncdf_attput, id, vid, 'long_name','Sea ice fraction'
vid = ncdf_vardef(id,'SnowDepth',[xid,yid],/float)
ncdf_attput, id, vid, 'long_name','Snow depth in m'
vid = ncdf_vardef(id,'SnowFraction',[xid,yid],/float)
ncdf_attput, id, vid, 'long_name','Land snow cover fraction'
vid = ncdf_vardef(id,'SnowMass',[xid,yid],/float)
ncdf_attput, id, vid, 'long_name','Land snow mass in kg/m2'
vid = ncdf_vardef(id,'SurfacePressure',[xid,yid],/float)
ncdf_attput, id, vid, 'long_name','Surface pressure in hPa'
vid = ncdf_vardef(id,'SurfaceTemperature',[xid,yid],/float)
ncdf_attput, id, vid, 'long_name','Surface temperature in K'
vid = ncdf_vardef(id,'TATM',[xid,yid,zeid],/float)
ncdf_attput, id, vid, 'long_name','Temperature in K'
vid = ncdf_vardef(id,'TerrainHeight',[xid,yid],/float)
ncdf_attput, id, vid, 'long_name','Terrain height in km'
vid = ncdf_vardef(id,'TopSnowAge',[xid,yid],/float)
ncdf_attput, id, vid, 'long_name','Top snow age in days'
vid = ncdf_vardef(id,'VAA',[xid,yid],/float)
ncdf_attput, id, vid, 'long_name','Viewing azimuth angle in degrees'
vid = ncdf_vardef(id,'VZA',[xid,yid],/float)
ncdf_attput, id, vid, 'long_name','Viewing zenith anglen in degrees'
vid = ncdf_vardef(id,'WindDirection',[xid,yid],/float)
ncdf_attput, id, vid, 'long_name','Surface wind direction clockwise from north'
vid = ncdf_vardef(id,'WindSpeed',[xid,yid],/float)
ncdf_attput, id, vid, 'long_name','Surface wind speed in m/s'
vid = ncdf_vardef(id,'Year',[xid,yid],/short)
ncdf_attput, id, vid, 'long_name','Year of observation'

; Loop over gases fields
IF (keyword_set(gas_list)) THEN BEGIN
   FOR i=0,ngas-1 DO BEGIN
      vid = ncdf_vardef(id,gas_list[i],[xid,yid,zid],/float)
      ncdf_attput, id, vid, 'long_name',gas_list[i]+' VMR vertical profile'
   ENDFOR
ENDIF
; Loop over aerosols fields
IF (keyword_set(aer_list)) THEN BEGIN
   FOR i=0,naer-1 DO BEGIN
      vid = ncdf_vardef(id,aer_list[i],[xid,yid,zid],/float)
      ncdf_attput, id, vid, 'long_name',aer_list[i]+' optical depth vertical profile'
   ENDFOR
ENDIF

; Loop over cloud fields
IF (keyword_set(cld_list)) THEN BEGIN
   FOR i=0,ncld-1 DO BEGIN
      vid = ncdf_vardef(id,cld_list[i],[xid,yid,zid,nc_maxid],/float)
      ncdf_attput, id, vid, 'long_name',cld_list[i]+' optical depth vertical profile'
   ENDFOR
ENDIF

; Finish file definition mode
ncdf_control, id, /endef

; Close file
ncdf_close, id

END
