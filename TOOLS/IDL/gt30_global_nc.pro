PRO GT30_GLOBAL_NC
  
  
  dx = 0.5d0/60.0d0
  dy = 0.5d0/60.0d0
  nx = long(360.0d0 / dx)
  ny = long(180.0d0 / dy)
  
  help,nx
  help,ny
  
  ; the other coordinates
  xt_name = ['w180','w140','w100','w060','w020',$
             'e020','e060','e100','e140','w180']
  xta_name = ['w180','w120','w060','w000','e060','e120']
  yt_name = ['s60','s10','n40','n90']
  xedge_ta = [-180.0,-120.,-60.,0.0,60.0,120.0,180.0]
  
  ; Tile grid
  xedge_t = [-180.0,-140.,-100.,-60.0,-20.0,20.0,60.0,100.0,140.0,180.0]
  yedge_t = [-90.,-60.,-10.,40.,90.]
  t_dx = 40.0
  t_dy = 50.0
  ta_dx = 60.
  ta_dy = 30.
  nyt = n_elements(yt_name)
  nxt = n_elements(xt_name)
  nxta = n_elements(xta_name)
  
  ; Create NetCDF
  id = ncdf_create('./gt30_global.nc',/clobber)
  
  ; Fill
  ncdf_control,id,/fill
  
  ; Define dimensions
  xid = ncdf_dimdef(id,'x',nx)
  yid = ncdf_dimdef(id,'y',ny)
;     
  ; Define variables
  hid = ncdf_vardef(id,'Altitude',[xid,yid],/short)
  lon_id = ncdf_vardef(id,'Longitude',xid,/float)
  lat_id = ncdf_vardef(id,'Latitude',yid,/float)
  
  NCDF_ATTPUT, id, hid, 'long_name', 'Altitude above sea level (m)'
  NCDF_ATTPUT, id, lon_id, 'long_name', 'Longitude (degrees)'
  NCDF_ATTPUT, id, lat_id, 'long_name', 'Latitude (degrees)'
  
  ; Put file in data mode:
  NCDF_CONTROL, id, /ENDEF
  
  
  
  
  for j=0,nyt-1 do begin
    
    if( j eq 0) then begin
      xt_name_in = xta_name
      nxt_in = nxta
      xedge = xedge_ta
;       dx = ta_dx
    endif else begin
      xt_name_in = xt_name
      nxt_in = nxt
      xedge = xedge_t
;       dx = t_dx
    endelse
    
;     dx = 0.5d0/
    
    for i=0,nxt_in-1 do begin
        
      ; Read data
      ncdf_read,d,/all,filename='./gt30' + xt_name_in[i] + yt_name[j] + '.nc'
      print,'USGS_elevation/gt30' + xt_name_in[i] + yt_name[j] + '.nc'
      
;       help,d,/st
      
      ; Compute offset
      xoff = long((xedge[i]+180.0d0)/dx) 
      yoff = long((yedge_t[j]+90.0d0)/dy) 
      print,xoff,yoff
      
      ncdf_varput,id,hid,d.altitude,offset=[xoff,yoff]
      ncdf_varput,id,lon_id,d.longitude,offset=xoff
      ncdf_varput,id,lat_id,d.latitude,offset=yoff
    endfor
  
  endfor
  
  
  
  
;   ; Put data
;   ncdf_varput,id,hid,d.alt
;   ncdf_varput,id,lon_id,d.xmid
;   ncdf_varput,id,lat_id,d.ymid
;   
;   
  NCDF_CLOSE, id ; Close the NetCDF file.
END