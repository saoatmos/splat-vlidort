;+
; NAME:
;       atmos_model_write
;
; PURPOSE:
;       Description: Write fields to netcdf file with input
;       atmospheric  models for SPLAT-VLIDORT.
;
; CALLING SEQUENCE:
;       atmos_model_write, filename, nx, ny, nz, nc_max, ng, na, nct, $
;                       AZA=AZA, Altitude=Altitude, $
;                       Chlorophyll=Chlorophyll, $
;                       CloudPixelFractions=CloudPixelFractions, $
;                       CloudTopPressure=CloudTopPressure, $
;                       CornerLatitudes=CornerLatitudes, $
;                       CornerLongitudes=CornerLongitudes, $
;                       Day=Day, DoCalculation=DoCalculation, $
;                       EtaA=EtaA, EtaB=EtaB, Hour=Hour, IsOcean=IsOcean, $
;                       LandTypeFraction=LandTypeFraction, $                    
;                       Latitude=Latitude, Longitude=Longitude, Month=Month, $
;                       NumberOfCloudPixels=NumberOfCloudPixels, $
;                       ObservationAltitude=ObservationAltitude, $
;                       OceanSalinity=OceanSalinity, Pressure=Pressure, $
;                       ProfileIsLevel=ProfileIsLevel, RH=RH, SAA=SAA, $
;                       SIF_734nm=SIF_734nm, SZA=SZA, $
;                       SeaIceFraction=SeaIceFraction, SnowDepth=SnowDepth, $
;                       SnowFraction=SnowFraction, SnowMass=SnowMass, $
;                       SurfacePressure=SurfacePressure, $
;                       SurfaceTemperature=SurfaceTemperature, $
;                       TATM=TATM, TerrainHeight=TerrainHeight,$
;                       TopSnowAge=TopSnowAge, VAA=VAA, VZA=VZA, $
;                       WindDirection=WindDirection, WindSpeed=WindSpeed, $
;                       Year=Year, GasList=GasList, Gasfields=Gasfields, $
;                       AerList=AerList, Aerfields=Aerfields, CldList=CldList, $
;                       Cldfields=Cldfields;       
;
; INPUTS:
;       filename: Output filename
;       nx: Horizontal dimension
;       ny: Horizontal dimension
;       nz: Vertical dimension
;       nc_max: Maximum number of cloud pixels in a pixel
;       ng: Number of gases
;       na: Number of aerosols
;       nct: Nnumber of cloud types
;
; OPTIONAL INPUTS:
;       AZA: 2D [nx,ny] float array of relative azimuth angles [degrees]
;       Altitude: 3D [nx,ny,nz+1] float array of altitudes [km]
;       Chlorophyll: 2D [nx,ny] float array of chlorophyll
;          concentrations  [mg/m3]
;       CloudPixelFractions: 3D [nx,ny,nc_max] float array of cloud
;          fractions
;       CloudTopPressure: 3D [nx,ny,nc_max] float array of top cloud
;          pressure [hPa]
;       CornerLatitudes: 3D [nx,ny,4] float array of pixel
;          corner latitudes going from SW to NW to NE and SE
;       CornerLongitudes: 3D [nx,ny,4] float array of pixel corner
;          latitudes going from SW to NW to NE and SE
;       Day: 2D [nx,ny] integer array of day of observation (within a
;          month)
;       DoCalculation: 2D [nx,ny] integer array of flags to do
;          radiative transfer calculation
;       EtaA: 1D [nz+1] float array of EtaA coordinate
;       EtaB: 1D [nz+1] float array of EtaB coordinate
;       Hour: 2D [nx,ny] float array of UTC decimal hour of
;          observation
;       IsOcean: 2D [nx,ny] integer array of flag for ocean scene
;       LandTypeFraction: 3D [nx,ny,17] float array of land type
;          fractions  following IGBP classification
;       Latitude: 2D [nx,ny] float array of latitudes [degrees]
;       Longitude: 2D [nx,ny] float array of longitudes [degrees]
;       Month: 2D [nx,ny] integer array of month of observation [1-12]
;       NumberOfCloudPixels: [nx,ny] integer array of number of clouds
;          in  each pixel.
;       ObservationAltitude: [nx,ny] float array of observation
;       altitudes (mostly thinking about aircraft observations) [km].
;       OceanSalinity: 2D [nx,ny] float array of ocean water salinity
;          [ppt] 
;       Pressure: 3D [nx,ny,nz+1] float array of pressures [hPa]
;       ProfileIsLevel: Integer set to 1 if profile variables at
;          grid midpoints (Altitude,Pressure,TATM)
;       RH: 3D [nx,ny,nz] float array of relative humidity [%]
;       SAA: 2D [nx,ny] float array of solar azimuth angles [degrees]
;       SIF_734nm: 2D [nx,ny] float array of solar induced fluorescence
;          at 734nm [mW/m2/nm/Sr]
;       SZA: 2D [nx,ny] float array of solar zenith angles [degrees]
;       SeaIceFraction: 2D [nx,ny] float array of fraction of sea ice
;       SnowDepth: 2D [nx,ny] float array of snow depth [m]
;       SnowFraction: 2D [nx,ny] float array of land snow cover
;          fraction
;       SnowMass: 2D [nx,ny] float array of land snow mass [kg/m2]
;       SurfacePressure: 2D [nx,ny] float array of surface pressure
;          [hPa]
;       SurfaceTemperature: 2D [nx,ny] float array of surface
;          temperature [k]
;       TATM: 3D [nx,ny,nz+1] float array of temperature profile [K]
;       TerrainHeight: 2D [nx,ny] float array of terrain height [Km]
;       TopSnowAge: 2D [nx,ny] float array of top snow age in days
;       VAA: 2D [nx,ny] float array of veiwing azimuth angle [degrees]
;       VZA: 2D [nx,ny] float array of veiwing zenith angle [degrees]
;       WindDirection: 2D [nx,ny] float array of surface wind
;          directions [degrees]
;       WindSpeed: 2D [nx,ny] float array of surface wind speed [m/s]
;       Year: 2D [nx,ny] integer array of year of the observation
;       GasList: 1D [ng] string array with names of gases to be
;          included. Recomended to use BRO, GLYX, H2O, HCHO, NO2, O3,
;          OCLO, as names for the most common ones
;       Gasfields: 4D [nx,ny,nz,ng] float array of gas vertical
;          profiles [VMR]
;       AerList: 1D [na] string array with names of aerosols to be
;          included. Recomended to use OBC, ODU, OOC, OSC, OSF, OSU as
;          names for the most common ones.
;       Aerfields: 4D [nx,ny,nz,na] float array of aerosol optical
;          depth vertical profiles
;       CldList: 1D [nct] string array with names of clouds to be
;          included. Recomended to use OCI and OCW names for the most
;          common  ones.
;       Cldfields: 4D [nx,ny,nz,nc_max,nct] float array of cloud optical
;          depth vertical profiles;      
;       
; MODIFICATION HISTORY:
;       Written by: Gonzalo Gonzalez Abad @ SAO, May 2018
;-
PRO atmos_model_write, filename, nx, ny, nz, nc_max, ng, na, nct, $
                       AZA=AZA, Altitude=Altitude, $
                       Chlorophyll=Chlorophyll, $
                       CloudPixelFractions=CloudPixelFractions, $
                       CloudTopPressure=CloudTopPressure, $
                       CornerLatitudes=CornerLatitudes, $
                       CornerLongitudes=CornerLongitudes, $
                       Day=Day, DoCalculation=DoCalculation, $
                       EtaA=EtaA, EtaB=EtaB, Hour=Hour, IsOcean=IsOcean, $
                       LandTypeFraction=LandTypeFraction, $                     
                       Latitude=Latitude, Longitude=Longitude, Month=Month, $
                       NumberOfCloudPixels=NumberOfCloudPixels, $
                       ObservationAltitude=ObservationAltitude, $
                       OceanSalinity=OceanSalinity, Pressure=Pressure, $
                       ProfileIsLevel=ProfileIsLevel, RH=RH, SAA=SAA, $
                       SIF_734nm=SIF_734nm, SZA=SZA, $
                       SeaIceFraction=SeaIceFraction, SnowDepth=SnowDepth, $
                       SnowFraction=SnowFraction, SnowMass=SnowMass, $
                       SurfacePressure=SurfacePressure, $
                       SurfaceTemperature=SurfaceTemperature, $
                       TATM=TATM, TerrainHeight=TerrainHeight,$
                       TopSnowAge=TopSnowAge, VAA=VAA, VZA=VZA, $
                       WindDirection=WindDirection, WindSpeed=WindSpeed, $
                       Year=Year, GasList=GasList, Gasfields=Gasfields, $
                       AerList=AerList, Aerfields=Aerfields, CldList=CldList, $
                       Cldfields=Cldfields


; Open netcdf file
id = ncdf_open(filename,/Write)

; For each variable check if the Keyword is present. If it is write
; values to file otherwise print a screen warning.
if keyword_set(AZA) then begin
   vid = ncdf_varid(id, 'AZA')
   ncdf_varput, id, vid, AZA
endif else begin
   print, 'Warning: No AZA data to be written in '+filename
endelse

if keyword_set(Altitude) then begin
   vid = ncdf_varid(id, 'Altitude')
   ncdf_varput, id, vid, Altitude
endif else begin
   print, 'Warning: No Altitude data to be written in '+filename
endelse

if keyword_set(Chlorophyll) then begin
   vid = ncdf_varid(id, 'Chlorophyll')
   ncdf_varput, id, vid, Chlorophyll
endif else begin
   print, 'Warning: No Chlorophyll data to be written in '+filename
endelse

if keyword_set(CloudPixelFractions) then begin
   vid = ncdf_varid(id, 'CloudPixelFractions')
   ncdf_varput, id, vid, CloudPixelFractions
endif else begin
   print, 'Warning: No CloudPixelFractions data to be written in '+filename
endelse

if keyword_set(CloudTopPressure) then begin
   vid = ncdf_varid(id, 'CloudTopPressure')
   ncdf_varput, id, vid, CloudTopPressure
endif else begin
   print, 'Warning: No CloudTopPressure data to be written in '+filename
endelse

if keyword_set(CornerLatitudes) then begin
   vid = ncdf_varid(id, 'CornerLatitudes')
   ncdf_varput, id, vid, CornerLatitudes
endif else begin
   print, 'Warning: No CornerLatitudes data to be written in '+filename
endelse

if keyword_set(CornerLongitudes) then begin
   vid = ncdf_varid(id, 'CornerLongitudes')
   ncdf_varput, id, vid, CornerLongitudes
endif else begin
   print, 'Warning: No CornerLongitudes data to be written in '+filename
endelse

if keyword_set(Day) then begin
   vid = ncdf_varid(id, 'Day')
   ncdf_varput, id, vid, Day
endif else begin
   print, 'Warning: No Day data to be written in '+filename
endelse

if keyword_set(DoCalculation) then begin
   vid = ncdf_varid(id, 'DoCalculation')
   ncdf_varput, id, vid, DoCalculation
endif else begin
   print, 'Warning: No DoCalculation data to be written in '+filename
endelse

if keyword_set(EtaA) then begin
   vid = ncdf_varid(id, 'EtaA')
   ncdf_varput, id, vid, EtaA
endif else begin
   print, 'Warning: No EtaA data to be written in '+filename
endelse

if keyword_set(EtaB) then begin
   vid = ncdf_varid(id, 'EtaB')
   ncdf_varput, id, vid, EtaB
endif else begin
   print, 'Warning: No EtaB data to be written in '+filename
endelse

if keyword_set(Hour) then begin
   vid = ncdf_varid(id, 'Hour')
   ncdf_varput, id, vid, Hour
endif else begin
   print, 'Warning: No Hour data to be written in '+filename
endelse

if keyword_set(IsOcean) then begin
   vid = ncdf_varid(id, 'IsOcean')
   ncdf_varput, id, vid, IsOcean
endif else begin
   print, 'Warning: No IsOcean data to be written in '+filename
endelse

if keyword_set(LandTypeFraction) then begin
   vid = ncdf_varid(id, 'LandTypeFraction')
   ncdf_varput, id, vid, LandTypeFraction
endif else begin
   print, 'Warning: No LandTypeFraction data to be written in '+filename
endelse

if keyword_set(Latitude) then begin
   vid = ncdf_varid(id, 'Latitude')
   ncdf_varput, id, vid, Latitude
endif else begin
   print, 'Warning: No Latitude data to be written in '+filename
endelse

if keyword_set(Longitude) then begin
   vid = ncdf_varid(id, 'Longitude')
   ncdf_varput, id, vid, Longitude
endif else begin
   print, 'Warning: No Longitude data to be written in '+filename
endelse

if keyword_set(Month) then begin
   vid = ncdf_varid(id, 'Month')
   ncdf_varput, id, vid, Month
endif else begin
   print, 'Warning: No Month data to be written in '+filename
endelse

if keyword_set(NumberOfCloudPixels) then begin
   vid = ncdf_varid(id, 'NumberOfCloudPixels')
   ncdf_varput, id, vid, NumberOfCloudPixels
endif else begin
   print, 'Warning: No NumberOfCloudPixels data to be written in '+filename
endelse

if keyword_set(ObservationAltitude) then begin
   vid = ncdf_varid(id, 'ObservationAltitude')
   ncdf_varput, id, vid, ObservationAltitude
endif else begin
   print, 'Warning: No ObservationAltitude data to be written in '+filename
endelse

if keyword_set(OceanSalinity) then begin
   vid = ncdf_varid(id, 'OceanSalinity')
   ncdf_varput, id, vid, OceanSalinity
endif else begin
   print, 'Warning: No OceanSalinity data to be written in '+filename
endelse

if keyword_set(Pressure) then begin
   vid = ncdf_varid(id, 'Pressure')
   ncdf_varput, id, vid, Pressure
endif else begin
   print, 'Warning: No Pressure data to be written in '+filename
endelse

if (n_elements(ProfileIsLevel) EQ 1) then begin
   vid = ncdf_varid(id, 'ProfileIsLevel')
   ncdf_varput, id, vid, ProfileIsLevel
endif else begin
   print, 'Warning: No ProfileIsLevel data to be written in '+filename
endelse

if keyword_set(RH) then begin
   vid = ncdf_varid(id, 'RH')
   ncdf_varput, id, vid, RH
endif else begin
   print, 'Warning: No RH data to be written in '+filename
endelse

if keyword_set(SAA) then begin
   vid = ncdf_varid(id, 'SAA')
   ncdf_varput, id, vid, SAA
endif else begin
   print, 'Warning: No SAA data to be written in '+filename
endelse

if keyword_set(SIF_734nm) then begin
   vid = ncdf_varid(id, 'SIF_734nm')
   ncdf_varput, id, vid, SIF_734nm
endif else begin
   print, 'Warning: No SIF_734nm data to be written in '+filename
endelse

if keyword_set(SZA) then begin
   vid = ncdf_varid(id, 'SZA')
   ncdf_varput, id, vid, SZA
endif else begin
   print, 'Warning: No SZA data to be written in '+filename
endelse

if keyword_set(SeaIceFraction) then begin
   vid = ncdf_varid(id, 'SeaIceFraction')
   ncdf_varput, id, vid, SeaIceFraction
endif else begin
   print, 'Warning: No SeaIceFraction data to be written in '+filename
endelse

if keyword_set(SnowDepth) then begin
   vid = ncdf_varid(id, 'SnowDepth')
   ncdf_varput, id, vid, SnowDepth
endif else begin
   print, 'Warning: No SnowDepth data to be written in '+filename
endelse

if keyword_set(SnowFraction) then begin
   vid = ncdf_varid(id, 'SnowFraction')
   ncdf_varput, id, vid, SnowFraction
endif else begin
   print, 'Warning: No SnowFraction data to be written in '+filename
endelse

if keyword_set(SnowMass) then begin
   vid = ncdf_varid(id, 'SnowMass')
   ncdf_varput, id, vid, SnowMass
endif else begin
   print, 'Warning: No SnowMass data to be written in '+filename
endelse

if keyword_set(SurfacePressure) then begin
   vid = ncdf_varid(id, 'SurfacePressure')
   ncdf_varput, id, vid, SurfacePressure
endif else begin
   print, 'Warning: No SurfacePressure data to be written in '+filename
endelse

if keyword_set(SurfaceTemperature) then begin
   vid = ncdf_varid(id, 'SurfaceTemperature')
   ncdf_varput, id, vid, SurfaceTemperature
endif else begin
   print, 'Warning: No SurfaceTemperature data to be written in '+filename
endelse

if keyword_set(TATM) then begin
   vid = ncdf_varid(id, 'TATM')
   ncdf_varput, id, vid, TATM
endif else begin
   print, 'Warning: No ATM data to be written in '+filename
endelse

if keyword_set(TerrainHeight) then begin
   vid = ncdf_varid(id, 'TerrainHeight')
   ncdf_varput, id, vid, TerrainHeight
endif else begin
   print, 'Warning: No TerrainHeight data to be written in '+filename
endelse

if keyword_set(TopSnowAge) then begin
   vid = ncdf_varid(id, 'TopSnowAge')
   ncdf_varput, id, vid, TopSnowAge
endif else begin
   print, 'Warning: No TopSnowAge data to be written in '+filename
endelse

if keyword_set(VAA) then begin
   vid = ncdf_varid(id, 'VAA')
   ncdf_varput, id, vid, VAA
endif else begin
   print, 'Warning: No VAA data to be written in '+filename
endelse

if keyword_set(VZA) then begin
   vid = ncdf_varid(id, 'VZA')
   ncdf_varput, id, vid, VZA
endif else begin
   print, 'Warning: No VZA data to be written in '+filename
endelse

if keyword_set(WindDirection) then begin
   vid = ncdf_varid(id, 'WindDirection')
   ncdf_varput, id, vid, WindDirection
endif else begin
   print, 'Warning: No WindDirection data to be written in '+filename
endelse

if keyword_set(WindSpeed) then begin
   vid = ncdf_varid(id, 'WindSpeed')
   ncdf_varput, id, vid, WindSpeed
endif else begin
   print, 'Warning: No WindSpeed data to be written in '+filename
endelse

if keyword_set(Year) then begin
   vid = ncdf_varid(id, 'Year')
   ncdf_varput, id, vid, Year
endif else begin
   print, 'Warning: No Year data to be written in '+filename
endelse

if keyword_set(GasList) then begin
   FOR i = 0, ng-1 DO BEGIN
      vid = ncdf_varid(id, GasList[i])
      ncdf_varput, id, vid, gasfields[0:nx-1,0:ny-1,0:nz-1,i]
   ENDFOR
endif else begin
   print, 'Warning: No GAS data to be written in '+filename
endelse

if keyword_set(AerList) then begin
   FOR i = 0, na-1 DO BEGIN
      vid = ncdf_varid(id, AerList[i])
      ncdf_varput, id, vid, aerfields[0:nx-1,0:ny-1,0:nz-1,i]
   ENDFOR
endif else begin
   print, 'Warning: No AEROSOL data to be written in '+filename
endelse

if keyword_set(CldList) then begin
   FOR i = 0, nct-1 DO BEGIN
      vid = ncdf_varid(id, CldList[i])
      ncdf_varput, id, vid, cldfields[0:nx-1,0:ny-1,0:nz-1,0:nc_max-1, i]
   ENDFOR
endif else begin
   print, 'Warning: No GAS data to be written in '+filename
endelse

; Close netcdf file
ncdf_close, id
END
