FUNCTION LOAD_MODIS_BRDF, INFILE
  
  
  get_hdf_sds,d, attr, type, rank, dim, desc, file=infile,$
                 NAME='Weighted_Mean'
  
  tvmap,d,/sample,/cbar,mindata=0,maxdata=1000
  return,d
  
END

FUNCTION LOAD_MODIS_LFRAC
   
  
  get_hdf_sds,d, attr, type, rank, dim, desc, $
                 file='./MODIS_AVG_landfrac.hdf',$
                 NAME='LAND_FRAC'
  
  
  tvmap,d,/sample,/cbar
  
  return,d
END



FUNCTION LOAD_COARSE_MODIS
  
  infile  =  'MODIS_COARSE_BRDF/MODIS_COARSE_BRDF_001_2002-2011.hdf'
  get_hdf_sds,d, attr, type, rank, dim, desc, file=infile,$
                 NAME='BRDF'
  
  imx = n_elements(d[*,0,0,0])
  jmx = n_elements(d[0,*,0,0])
  
  ; Resolution
  dx = 360.0d0/double(imx)
  dy = 180.0d0/double(jmx)
  
  ; Latitude
  lat = 90.0 - ( dindgen(jmx) + 0.5d0 ) * dy
  lon = (dindgen(imx) + 0.5d0)*dx - 180.0d0
  
  window,0
  tvmap,d[*,*,0,0],lon,lat,/cbar,/continents,mindata=0,maxdata=1000.0
  
  dtmp = d[*,*,0,0]
  dtmp = reverse(dtmp,2)
  window,1
  tvmap,dtmp,lon,reverse(lat),/cbar,/continents,mindata=0,maxdata=1000.0
  
  return,d
END

PRO MODIS_BRDF_SUBSET
  
  ; Original MODIS files
  indir =  'MODIS_AVG_BRDF/'
  
  outdir = 'MODIS_AVG_BRDF_TEMPO_REGION/'
  
  ; Subsetting region
  buffer   = 2.0
  minlat   = 18.0 - buffer
  maxlat   = 58.0 + buffer
  minlon   = -140.- buffer
  maxlon   = -50. + buffer
  
  ; Stick to same file naming convention ('MODIS_COARSE_BRDF_'+DOY+'_2002-2011.hdf')
  
  
  ; Get list of MODIS AVG files
  flist = file_search(indir,'MCD43GF_AVG_geo_Band1_*_2002-2011.hdf')
  nf = n_elements(flist)
  nf = 1 ; For testing
  
  for f=0,nf-1 do begin
    
    ; Get file name
    frag  = strsplit(flist[f],'/',/extract)
    fname_geo = frag[n_elements(frag)-1]
    
    ; Get DOY string
    frag = strsplit(fname_geo,'_',/extract)
    doy_str = frag[4]
    
    
    fname_iso = indir+'MCD43GF_AVG_iso_Band1_'+doy_str+'_2002-2011.hdf'
    fname_vol = 'MCD43GF_AVG_vol_Band1_'+doy_str+'_2002-2011.hdf'
    
    ; Load isotropic reflection
    tmp = load_modis_brdf(fname_iso)
    
    
    
    tvmap,tmp,/cbar,/continents,mindata=0,maxdata=1000
    
    help,tmp,/st
    if(f eq 0) then begin
      
      ; Get dimensions
      imx = n_elements(tmp[*,0])
      jmx = n_elements(tmp[0,*])
      
      ; Resolution
      dx = 360.0d0/double(imx)
      dy = 180.0d0/double(jmx)
      
      ; Latitude
      lat = 90.0 - ( dindgen(jmx) + 0.5d0 ) * dy ; +90 -> -90
      lon = (dindgen(imx) + 0.5d0)*dx - 180.0d0  ; -180 -> 180
      
      ; Get indices
      tmp = min(abs(lat-maxlat),jmin)
      tmp = min(abs(lat-minlat),jmax)
      tmp = min(abs(lon-maxlon),imax)
      tmp = min(abs(lon-minlon),imin)
      
      ; Output dimensions
      isub = imax - imin + 1
      jsub = jmax - jmin + 1
      
      print,imin,imax,isub
      print,jmin,jmax,jsub
      
      ; Declare output arrays
      brdf_out = replicate(0.0,isub,jsub,4,3)
      
      ; Also compute the landmask
      
    endif
    
  endfor
  
  
  
  
END