FUNCTION READ_ELEV,INFILE
  
  alt = read_tiff(infile,GEOTIFF=geo)
  
    
  alt = Reverse(alt, 2)
  s = size(alt,/dimensions)
    
  xscale  = geo.ModelPixelScaleTag[0]
  yscale  = geo.ModelPixelScaleTag[1]
  tp      = geo.ModelTiePointTag[3:4]
  xOrigin = tp[0]
  yOrigin = tp[1] - (yscale * s[1])
    
    
  xOrigin = xOrigin + (xscale * 0.5)
  yOrigin = yOrigin + (yscale * 0.5)
    
  xmid = Findgen(s[0]) * xscale + xOrigin
  ymid = Findgen(s[1]) * yscale + yOrigin
    

  
  return,{alt:alt,xmid:xmid,ymid:ymid}
  
END