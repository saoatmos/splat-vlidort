FUNCTION READ_TEMPO_SPEC,INFILE,SF,NSKIP=nskip
 
 ; Read file
 if(keyword_set(nskip)) then begin
  d = read_ascii(infile,data_start=nskip)
 endif else begin
  d = read_ascii(infile)
 endelse

 ts = double(d.field1)
 spc = reform(ts[1,*])*sf
 id = where(spc eq 0.0)
 spc[id] = !Values.d_nan

 return,{pos:reform(ts[0,*]),$
         spc:spc}

 
  
END


PRO PLOT_TEMPO_SPEC,EPS=eps

  eps = keyword_set(eps)
 
  ; Read O3
  o3 = read_tempo_spec('TEMPO_o3abs_brion_270_800_vacfinal.dat',9.0d18*1.0d-20)
  no2 = read_tempo_spec('TEMPO_no2r_97.dat',6.0d15)
  hcho = read_tempo_spec('TEMPO_h2co_300K.dat',1.0d16) 
  so2 = read_tempo_spec('TEMPO_so2_295K.dat',1.0d16)
  glyx = read_tempo_spec('TEMPO_glyx_296K.dat',2.0d14)
  h2o = read_tempo_spec('TEMPO_h2o_280K.dat',3.0d23)
  bro = read_tempo_spec('TEMPO_bro_228K.dat',5.0d13)
  o2o2 = read_tempo_spec('TEMPO_o4_par_Thalman_250_800.dat',1.0)
  o2 = read_tempo_spec('TEMPO_o2_253K.dat',4.5d24)  

 
  id = where(o2o2.pos ge 443. and o2o2.pos le 447.0 )
  print,mean(o2o2.spc[id])
  sf = 0.7d-3 / mean(o2o2.spc[id])
  o2o2.spc = o2o2.spc * sf
  lthck = 5

  if( eps ) then begin
    set_eps,FILE='./tempo_optical_depth.eps',FONT_SIZE=16
  endif else begin
    window,0
  endelse

  plot,o3.pos,o3.spc,color=1,thick=lthck,/ylog,$
       position=[.15,.15,.9,.9],$
       xrange=[290,740],/xstyle,yrange=[1e-7,1e2],/ystyle
  oplot,h2o.pos,h2o.spc,thick=lthck,color=6
  oplot,no2.pos,no2.spc,thick=lthck,color=2
  oplot,hcho.pos,hcho.spc,thick=lthck,color=3
  oplot,so2.pos,so2.spc,thick=lthck,color=4
  oplot,bro.pos,bro.spc,thick=lthck,color=7
  oplot,o2.pos,o2.spc,thick=lthck,color=8
  oplot,o2o2.pos,o2o2.spc,thick=lthck,color=12
  oplot,glyx.pos,glyx.spc,thick=lthck,color=5

  if( eps ) then set_eps,/close  
 
  if( eps ) then begin
    set_eps,FILE='./tempo_optical_depth_band1.eps',FONT_SIZE=16
  endif else begin
    window,1
  endelse

  plot,o3.pos,o3.spc,color=1,thick=lthck,/ylog,$
       position=[.15,.15,.9,.9],$
       xrange=[290,490],/xstyle,yrange=[1e-7,1e2],/ystyle
  oplot,h2o.pos,h2o.spc,thick=lthck,color=6
  oplot,no2.pos,no2.spc,thick=lthck,color=2
  oplot,hcho.pos,hcho.spc,thick=lthck,color=3
  oplot,so2.pos,so2.spc,thick=lthck,color=4
  oplot,bro.pos,bro.spc,thick=lthck,color=7
  oplot,o2.pos,o2.spc,thick=lthck,color=8
  oplot,o2o2.pos,o2o2.spc,thick=lthck,color=12
  oplot,glyx.pos,glyx.spc,thick=lthck,color=5

  if( eps ) then set_eps,/close
  
  if( eps ) then begin
    set_eps,FILE='./tempo_optical_depth_band2.eps',FONT_SIZE=16
  endif else begin
    window,2
  endelse

  plot,o3.pos,o3.spc,color=1,thick=lthck,/ylog,$
       position=[.15,.15,.9,.9],$
       xrange=[540,740],/xstyle,yrange=[1e-7,1e2],/ystyle
  oplot,h2o.pos,h2o.spc,thick=lthck,color=6
  oplot,no2.pos,no2.spc,thick=lthck,color=2
  oplot,hcho.pos,hcho.spc,thick=lthck,color=3
  oplot,so2.pos,so2.spc,thick=lthck,color=4
  oplot,bro.pos,bro.spc,thick=lthck,color=7
  oplot,o2.pos,o2.spc,thick=lthck,color=8
  oplot,o2o2.pos,o2o2.spc,thick=lthck,color=12
  oplot,glyx.pos,glyx.spc,thick=lthck,color=5

  if( eps ) then set_eps,/close 



  


END
