PRO MOD_SCIA_WATER_SNOW
  
  ; Load water spectrum
  d=read_ascii('seawater_open_ocean_sw2.27262_noHeader.asc')
  
  
  ; Wavelength in nm
  wvl   = d.field1[0,*]*1e3
  alb_w = d.field1[1,*]
  
  
  ; Load snow
  d = read_ascii('/data/tempo2/xliu/ASTER_Database/data/jhu.becknic.water.snow.granular.178um.coarse.spectrum.txt')
  ts_sc = d.field1
  d = read_ascii('/data/tempo2/xliu/ASTER_Database/data/jhu.becknic.water.snow.fine.24um.fine.spectrum.txt')
  ts_sf = d.field1
  d = read_ascii('/data/tempo2/xliu/ASTER_Database/data/jhu.becknic.water.snow.granular.82um.medium.spectrum.txt')
  ts_sm = d.field1
  d = read_ascii('/data/tempo2/xliu/ASTER_Database/data/jhu.becknic.water.ice.none.solid.ice.spectrum.txt')
  ts_i = d.field1
  
  ; Interpolate to water grid
  v = reform(ts_sf[1,*])*1e-2
  x = reform(ts_sf[0,*])*1e3
  id = where(finite(v) and finite(x))
  v = v[id]
  x = x[id]
  x = [200.0d0,x]
  v = [v[0],v]
  print,x[0],v[0],x[1],v[1]
  
  alb_sf = interpol(v,x,wvl,/spline)
  
  v = reform(ts_sm[1,*])*1e-2
  x = reform(ts_sm[0,*])*1e3
  id = where(finite(v) and finite(x))
  v = v[id]
  x = x[id]
  x = [200.0d0,x]
  v = [v[0],v]
  print,x[0],v[0],x[1],v[1]
  alb_sm = interpol(v,x,wvl,/spline)
  
  
  v = reform(ts_sc[1,*])*1e-2
  x = reform(ts_sc[0,*])*1e3
  id = where(finite(v) and finite(x))
  v = v[id]
  x = x[id]
  x = [200.0,x]
  v = [v[0],v]
  print,x[0],v[0],x[1],v[1]
  alb_sc = interpol(v,x,wvl,/spline)
  
  v = reform(ts_i[1,*])*1e-2
  x = reform(ts_i[0,*])*1e3
  id = where(finite(v) and finite(x))
  v = v[id]
  x = x[id]
  x = [200.0,x]
  v = [v[0],v]
  print,x[0],v[0],x[1],v[1]
  alb_i = interpol(v,x,wvl,/spline)
  
  window,0
  plot,wvl,alb_w,color=1,thick=2,position=[.15,.15,.9,.9],$
       xtitle='Wavelength (nm)',ytitle='Reflectivity',$
       yrange=[0,0.1]
  oplot,ts_sf[0,*]*1e3,ts_sf[1,*]*1e-2,color=2,thick=2
  oplot,wvl,alb_sf,color=7
  oplot,ts_sm[0,*]*1e3,ts_sm[1,*]*1e-2,color=3,thick=2
  oplot,wvl,alb_sm,color=8
  oplot,ts_sc[0,*]*1e3,ts_sc[1,*]*1e-2,color=4,thick=2
  oplot,wvl,alb_sc,color=9
  oplot,ts_i[0,*]*1e3,ts_i[1,*]*1e-2,color=5,thick=2
  oplot,wvl,alb_i,color=10
  
  ; Write reflectance spectra to disk
  openw,lun,'./water_snow_ice_albedo.asc',/get_lun
  
  for n=0,n_elements(wvl)-1 do begin
    printf,lun,wvl[n],alb_w[n],alb_sf[n],alb_sm[n],alb_sc[n],alb_i[n],$
           format='(F12.5,X,F12.9,X,F12.9,X,F12.9,X,F12.9,X,F12.9)'
  endfor
  
  free_lun,lun
  
  
END