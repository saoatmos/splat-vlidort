PRO MODIS_BRDF_SUBTILE
  
  ; Output directory
  outdir = ''
  
  
  ; Subsetting region
  buffer   = 2.0
  minlat   = 18.0 - buffer
  maxlat   = 58.0 + buffer
  minlon   = -140.- buffer
  maxlon   = -50. + buffer
  
  
  ; Break MODIS files into 10x10 subtile
  tile_dx = 10.0d0
  tile_dy = 10.0d0
  
  
  ; =======================================================================
  
  ; Define MODIS grid ( 30 arc second )
  imx = 360L * 120L
  jmx = 180L * 120L
  
  ; Native pixel resolution
  dx = 0.5d0 / 60.0d0
  dy = dx
  
  ; MODIS Grid
  xmid = (dindgen(imx) + 0.5d0)*dx - 180.0d0
  ymid = (dindgen(jmx) + 0.5d0)*dy -  90.0d0
  
  nx_tile = long( 360.0 / tile_dx )
  ny_tile = long( 180.0 / tile_dy )
  
  nx_sub = long( imx / nx_tile )
  ny_sub = long( jmx / ny_tile )
  
  
  ; Tile information for the GEOCAPE tool ( for determining which tiles to load)
  tile_xmin = replicate(0.0D0,nx_tile)
  tile_xmax = replicate(0.0D0,nx_tile)
  tile_ymin = replicate(0.0D0,ny_tile)
  tile_ymax = replicate(0.0D0,ny_tile)
  tile_nx   = replicate(nx_sub,nx_tile)
  tile_ny   = replicate(ny_sub,ny_tile)
  
  for i=0,nx_tile-1 do begin
    tile_xmin[i] = xmid[i*nx_sub]
    tile_xmax[i] = xmid[(i+1)*nx_sub-1]
  endfor
  
  for j=0,ny_tile-1 do begin
    tile_ymin[j] = ymid[j*ny_sub]
    tile_ymax[j] = ymid[(j+1)*ny_sub-1]
  endfor
  
  ; Loop over tile regions
  yn_tile = replicate(0.0,nx_tile,ny_tile)
  
  for i=0,nx_tile-1 do begin
    
    ; Only create files for tiles in TEMPO domain
    if( tile_xmin[i] ge minlon and tile_xmax[i] le maxlon ) then begin
      
      for j=0,ny_tile-1 do begin
        
        if( tile_ymin[j] ge minlat and tile_ymax[j] le maxlat ) then begin
          
          print,i,j
          yn_tile[i,j] = 1.0
        endif
        
      endfor
      
    endif
    
  endfor
  
  tvmap,yn_tile,/sample,/cbar,/continents
  
;   ; IDs for tiles
;   xtile_id = replicate(0,imx)
;   ytile_id = replicate(0,jmx)
;   for i=0L,nx_tile-1 do xtile_id[i*nx_sub:(i+1)*nx_sub-1] = i+1
;   for j=0L,ny_tile-1 do ytile_id[j*nx_sub:(j+1)*ny_sub-1] = j+1
;   
;   ; We only want to do tiles within the TEMPO region
;   id = where( xmid ge minlon and xmid le maxlon )
;   tmp = xtile_id[id]
;   xtile_id_tempo = tmp[uniq(tmp,sort(tmp))]
;   
;   id = where( ymid ge minlat and ymid le maxlat )
;   tmp = ytile_id[id]
;   ytile_id_tempo = tmp[uniq(tmp,sort(tmp))]
;   
;   print,xtile_id_tempo
;   print,ytile_id_tempo
;   
;   nxtile_tempo = n_elements(xtile_id_tempo)
;   nytile_tempo = n_elements(ytile_id_tempo)
;   
;   ; Write files
;   for i=0,nx_tile-1 do begin
;     
; ;     if(  )
;     
;     for j=0,ny_tile-1 do begin
;       
;       
;       
;     endfor
;   endfor
;   
  
  
  ; Operation for load
;   a = reverse(a,2)
  
END