PRO GT30_CONVERT_HDF2NC
  
  ; GTOP3O DEM From SDPTK TOOLKIT
;   h = hdf_open('dem90ARC_W180N90.hdf',/read)
;   d = HDF_READ('dem90ARC_W180N90.hdf')
;   help,d,/st
  
  
   fileID = HDF_SD_Start('dem90ARC_W180N90.hdf', /read) 
   
   ; Read elevation
   dataset_index = HDF_SD_NameToIndex(fileID, 'Elevation')
   datasetID = HDF_SD_Select(fileID, dataset_index) 
   HDF_SD_GetData, datasetID, alt
   alt = Reverse(alt, 2)
   
   ; Read LW flag
   dataset_index = HDF_SD_NameToIndex(fileID, 'LandWater')
   datasetID = HDF_SD_Select(fileID, dataset_index) 
   HDF_SD_GetData, datasetID, lwflag
   lwflag = Reverse(lwflag, 2)
   
;   s = size(alt,/dimensions)
   dx = 1.5d0/60.0
   dy = dx
   
   nx = n_elements(alt[*,0])
   ny = n_elements(alt[0,*])
   
   
   xmid = findgen(nx)*dx-180.0 + dx/2.0
   ymid = findgen(ny)*dy-90.0 + dy/2.0
   
   print,min(xmid),max(xmid)
   print,min(ymid),max(ymid)
   
   window,0
   tvmap,alt,xmid,ymid,/continents,/cbar,min_valid=100,botoutofrange=14,$
         /isotropic,limit=[-25,-75,-20,-65],/sample
   
   window,1
   tvmap,lwflag,xmid,ymid,/continents,/cbar,botoutofrange=14
   
   ; Create NetCDF
   id = ncdf_create('dem90ARC_W180N90.nc',/clobber)
    
   ; Fill
   ncdf_control,id,/fill
    
   ; Define dimensions
   xid = ncdf_dimdef(id,'x',n_elements(xmid))
   yid = ncdf_dimdef(id,'y',n_elements(ymid))
    
   ; Define variables
   hid = ncdf_vardef(id,'Altitude',[xid,yid],/short)
   lon_id = ncdf_vardef(id,'Longitude',xid,/float)
   lat_id = ncdf_vardef(id,'Latitude',yid,/float)
    
   NCDF_ATTPUT, id, hid, 'long_name', 'Altitude above sea level (m)'
   NCDF_ATTPUT, id, lon_id, 'long_name', 'Longitude (degrees)'
   NCDF_ATTPUT, id, lat_id, 'long_name', 'Latitude (degrees)'
    
   ; Put file in data mode:
   NCDF_CONTROL, id, /ENDEF
   
   ; Put data
   ncdf_varput,id,hid,alt
   ncdf_varput,id,lon_id,xmid
   ncdf_varput,id,lat_id,ymid
    
   NCDF_CLOSE, id ; Close the NetCDF file.
   
END 