PRO CONVERT_AERCLD_2NC
  
  ; Types
  aertype = ['BC', 'OC', 'DU', 'SU', 'SF', 'SC', 'WC', 'IC']
  yn_rhdep= [   0,    1,    0,    1,    1,    1,    0,    0]
  
  rhstr   = ['00','50','70','80','90','95','98','99']
  naer = n_elements(aertype)
  nrh  = n_elements(rhstr)
  
  rh_dbl = double(rhstr)
  
  for n=0,naer-1 do begin
    
    ; Output file name 
    outfile = aertype[n] + '_200nm-20um_wgphase_flexinput.nc'
    print,outfile
    
    if( yn_rhdep[n] ) then begin
      nrh_out = nrh
    endif else begin
      nrh_out = 1
    endelse
    
    for r=0,nrh-1 do begin
      
      if( r gt 0 and not yn_rhdep[n] ) then begin
        
        qext0[*,r] = qext0[*,0]
        ssa0[*,r]  = ssa0[*,0] 
        phfcn0[*,*,*,r] = phfcn0[*,*,*,0]
        reff[r] = reff[0]
        
      endif else begin
        
        if( aertype[n] eq 'IC' or aertype[n] eq 'WC' ) then begin
          infile =  aertype[n] + '_vprop1000.dat'
        endif else begin
          infile =  aertype[n] + rhstr[r] + '_200nm-20um_wgphase.dat'
        endelse
        
        print,infile
        
        d = read_aer_prop( infile )
        
        if( r eq 0 ) then begin
          
          ; Declare output
          nwls   = n_elements(d.wvl)
          nmom   = n_elements(d.phi[0,*,0])
          ngk    = n_elements(d.phi[0,0,*])
          wls0   = d.wvl
          qext0  = replicate(0.0D0,nwls,nrh)
          ssa0   = replicate(0.0D0,nwls,nrh)
          phfcn0 = replicate(0.0D0,nwls,nmom,ngk,nrh)
          reff   = replicate(0.0d0,nrh)
          
        endif
        
        qext0[*,r] = d.qext
        ssa0[*,r]  = d.ssa
        phfcn0[*,*,*,r] = d.phi
        reff[r] = d.reff[0]

      endelse
    endfor 
      ; ==========================================================
      ; Write output to NETCDF
      ; ==========================================================
      
      ; Create NetCDF
      id = ncdf_create(outfile,/clobber,/NETCDF4_FORMAT)
  
      ; Fill
      ncdf_control,id,/fill
      
      ; Create dimensions
      wid = ncdf_dimdef(id,'w',nwls)
      mid = ncdf_dimdef(id,'m',nmom)
      gid = ncdf_dimdef(id,'g',ngk)
      rid = ncdf_dimdef(id,'r',nrh_out)
      one_id = ncdf_dimdef(id,'one',1)
      
      ; Define variables
      vid = ncdf_vardef(id,'nwls', [one_id], /long )
      vid = ncdf_vardef(id,'nmom',  [one_id], /long )
      vid = ncdf_vardef(id,'nrh',  [one_id], /long )
      vid = ncdf_vardef(id,'wls0', [wid],    /double)
      vid = ncdf_vardef(id,'qext0',[wid,rid],/double)
      vid = ncdf_vardef(id,'ssa0', [wid,rid],/double)
      vid = ncdf_vardef(id,'phfcn0',[wid,mid,gid,rid],/double)
      vid = ncdf_vardef(id,'reff',[rid],/double)
      vid = ncdf_vardef(id,'rh',[rid],/double)
      
      ; Put file in data mode:
      NCDF_CONTROL, id, /ENDEF
      
      vid = NCDF_VARID(id, 'nwls')
      ncdf_varput,id,vid,nwls
      
      vid = NCDF_VARID(id, 'nmom')
      ncdf_varput,id,vid,nmom 
      
      vid = NCDF_VARID(id, 'nrh')
      ncdf_varput,id,vid,nrh_out
      
      vid = NCDF_VARID(id, 'wls0')
      ncdf_varput,id,vid,wls0
      
      vid = NCDF_VARID(id, 'qext0')
      ncdf_varput,id,vid,qext0[*,0:nrh_out-1]
      
      vid = NCDF_VARID(id, 'ssa0')
      ncdf_varput,id,vid,ssa0[*,0:nrh_out-1]
      
      vid = NCDF_VARID(id, 'phfcn0')
      ncdf_varput,id,vid,phfcn0[*,*,*,0:nrh_out-1]

      vid = NCDF_VARID(id,'reff')
      ncdf_varput,id,vid,reff[0:nrh_out-1]
      
      vid = NCDF_VARID(id,'rh')
      ncdf_varput,id,vid,reff[0:nrh_out-1]
      
      NCDF_CLOSE, id ; Close the NetCDF file.
     
      print,phfcn0[*,0,0,0:nrh_out-1]
    
  
  endfor
END
