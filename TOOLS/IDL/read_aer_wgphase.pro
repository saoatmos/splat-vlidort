FUNCTION READ_AER_WGPHASE
  
  ; Path to file
  infile = 'BC00_200nm-20um_wgphase.dat'
  
  ; Open file
  openr,lun,infile,/get_lun
  
  ; Read # wavelengths
  line = ''
  readf,lun,line
  frag = strsplit(line,/extract)
  ; Read number of wavelengths
  nwvl = long(frag[0])
  
  ; Skip header
  readf,lun,line
  
  
  
  
  
  
  print,nwvl
  
  wvl  = replicate(0.0D,nwvl)
  qext = replicate(0.0D,nwvl)
  ssa  = replicate(0.0D,nwvl)
  phi  = replicate(0.0D,nwvl,nmom+1,maxgksec)
  
  for i=0,nwvl-1 do begin
    
    readf,lun,line
    frag = strsplit(line,/extract)
    
   
    wvl[i]  = double(frag[0])
    qext[i] = double(frag[1])
    ssa[i]  = double(frag[3])
    phi[i,0:nmom,0] = double(frag[4:4+nmom])
    
    for k=1,maxgksec-1 do begin
      
      readf,lun,line
      frag = strsplit(line,/extract)
      phi[i,0:nmom,k] = double(frag)
      
    endfor
    
  endfor
  
  ; Wavelength to nm
  wvl = wvl*1e3
  
  ; Close file
  free_lun,lun
  
  plot,wvl,qext,position=[.15,.15,.9,.9],xrange=[0,800.],psym=1
  
  return,{wvl:wvl,qext:qext,ssa:ssa,phi:phi}
  
END