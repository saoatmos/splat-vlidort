PRO PLOT_REFF,EPS=eps
  
  eps = keyword_set(eps)
  
  rhstr   = ['00','50','70','80','90','95','98','99']
  rh = fix(rhstr)
  nrh = n_elements(rh)

  ; Aerosol types
  aertype = ['BC', 'OC', 'DU', 'SU', 'SF', 'SC'] 
  naer = n_elements(aertype)
 
  ; reff array
  reff = replicate(0.0,nrh,naer)
  qext = replicate(0.0,nrh,naer) ; ext efficiency @ 400nm
  for n=0,naer-1 do begin
    
   ; Open file
   ncdf_read,d,variables=['reff','qext0'],filename=aertype[n]+'_200nm-20um_wgphase.nc'
   reff[*,n] = d.reff
   qext[*,n] = d.qext0[4,*] 
  endfor  
  
  if( eps ) then begin
    set_eps,FILE='./tempo_reff.eps',FONT_SIZE=16
  endif else begin
    window,0
  endelse
  plot,rh,reff[*,0],color=1,yrange=[min(reff),max(reff)],$
       position=[.2,.2,.9,.9],/nodata,/ylog,$
       xtitle='Relative Humidity (%)',$
       ytitle='Mean Radius (Microns)'

  for n=0,naer-1 do begin
    oplot,rh,reff[*,n],color=n+1,thick=10
  endfor  
  
  if(eps) then set_eps,/close 
 
  window,1
  plot,rh,qext[*,0],color=1,yrange=[min(qext),max(qext)],$
       position=[.2,.2,.9,.9],/nodata
  for n=0,naer-1 do begin
    oplot,rh,qext[*,n],color=n+1,thick=10
  endfor


END
