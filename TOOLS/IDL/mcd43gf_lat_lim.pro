FUNCTION READ_MCD43GF, INFILE
  
  
  
  newFileID = HDF_SD_START(infile, /READ)
  
  PRINT, 'Reading number of datasets and file attributes in file ...'
  HDF_SD_FILEINFO, newFileID, datasets, attributes
  numPalettes = HDF_DFP_NPALS(infile)
  PRINT, ''
  PRINT, 'No. of Datasets:   ', datasets
  PRINT, 'No. of File Attributes: ', attributes
  PRINT, 'No. of Palettes:   ', numPalettes
  
  ; Print the name of each file attribute.
  PRINT, ''
  PRINT, 'Printing name of each file attribute...'
  FOR j=0, attributes-1 DO BEGIN
    HDF_SD_ATTRINFO, newFileID, j, NAME=thisAttr
    PRINT, 'File Attribute No. ', + STRTRIM(j, 2), ': ', thisAttr
  ENDFOR
  
  ; Print the name of each SDS and associated data attributes.
  PRINT, ''
  PRINT, 'Printing name of each data set and its associated data attributes...'
  FOR j=0, datasets-1 DO BEGIN
    thisSDS = HDF_SD_SELECT(newFileID, j)
    HDF_SD_GETINFO, thisSDS, NAME=thisSDSName, NATTS=numAttributes
    PRINT, 'Dataset No. ', STRTRIM(j,2), ': ', thisSDSName
    FOR k=0,numAttributes-1 DO BEGIN
        HDF_SD_ATTRINFO, thisSDS, k, NAME=thisAttrName
        PRINT, '   Data Attribute: ', thisAttrName
    ENDFOR
    PRINT, ''
  ENDFOR
  
  ; Find the index of the "Gridded Data" SDS.

  index = HDF_SD_NAMETOINDEX(newFileID, "Weighted_Mean")

  ; Select the Gridded Data SDS.

  thisSdsID = HDF_SD_SELECT(newFileID, index)

  ; Print the names of the Gridded Data attributes.

  PRINT, ''
  PRINT, 'Printing names of each Gridded Data Attribute..."
  HDF_SD_GETINFO, thisSdsID, NATTS=numAttributes
  PRINT, 'Number of Gridded Data attributes: ', numAttributes
  FOR j=0,numAttributes-1 DO BEGIN
    HDF_SD_ATTRINFO, thisSdsID, j, NAME=thisAttr
    PRINT, 'SDS Attribute No. ', + STRTRIM(j, 2), ': ', thisAttr
  ENDFOR

    ; Get the data.
  PRINT, ''
  PRINT, 'Reading gridded data ...'
  HDF_SD_GETDATA, thisSdsID, newGriddedData
  
  imx = n_elements(newGriddedData[*,0])
  jmx = n_elements(newGriddedData[0,*])
  
  lat = 90.0d0 - dindgen(jmx) * 0.5d0/60.0d0
  lon = -180.0d0 + dindgen(imx)* 0.5d0/60.0d0
  
  return,{ts:newGriddedData,lon:lon,lat:lat}
  
END


FUNCTION LOAD_KERN,DOY,XLIM,YLIM
  
  ; Read isotropic kernel
  infile = './MODIS_AVG_BRDF/MCD43GF_AVG_iso_Band1_'+doy+'_2002-2011.hdf'
  d=read_mcd43gf( infile )
  
  ; Get limits
  id = where(d.lon ge xlim[0] and d.lon le xlim[1] )
  x0 = min(id)
  xm = max(id)
  id = where(d.lat ge ylim[0] and d.lat le ylim[1] )
  y0 = min(id)
  ym = max(id)
  lon = d.lon[x0:xm]
  lat = d.lat[y0:ym]
  iso = d.ts[x0:xm,y0:ym]
  
  infile = './MODIS_AVG_BRDF/MCD43GF_AVG_vol_Band1_'+doy+'_2002-2011.hdf'
  d=read_mcd43gf( infile )
  vol = d.ts[x0:xm,y0:ym]
  
  infile = './MODIS_AVG_BRDF/MCD43GF_AVG_geo_Band1_'+doy+'_2002-2011.hdf'
  d=read_mcd43gf( infile )
  geo = d.ts[x0:xm,y0:ym]
  
  return,{lon:lon,lat:lat,iso:iso,vol:vol,geo:geo}
  
END

PRO PLOT_MOD,D=d
  
  
  if(not keyword_set(d)) then d = load_kern('193',[-60.,-45.],[50.,60.])
  
   
  window,0
  tvmap,d.iso,d.lon,d.lat,mindata=0,maxdata=100,/continents,/hires,/cbar
  window,1
  tvmap,d.vol,d.lon,d.lat,mindata=0,maxdata=100,/continents,/hires,min_valid=0,botoutofrange=14,/cbar
  window,2
  tvmap,d.geo,d.lon,d.lat,mindata=0,maxdata=100,/continents,/hires,min_valid=0,botoutofrange=14,/cbar
  
  
  imx = n_elements(d.lon)
  jmx = n_elements(d.lat)
  
  window,3
  ts = replicate(0.0,imx,jmx)
  id = where( d.iso ge 0.0 and d.iso le 1000. and $
             (d.vol lt 0.0 or d.vol gt 1000.0) )
  if( id[0] ge 0 ) then ts[id] = 1
  tvmap,ts,d.lon,d.lat,mindata=0,maxdata=1,/continents,/hires,min_valid=0,botoutofrange=14,/cbar,/sample
  help,id
  
  window,4
  ts = replicate(0.0,imx,jmx)
  id = where( d.iso ge 0.0 and d.iso le 1000. and $
             (d.geo lt 0.0 or d.geo gt 1000.0) )
  if( id[0] ge 0 ) then ts[id] = 1
  tvmap,ts,d.lon,d.lat,mindata=0,maxdata=1,/continents,/hires,min_valid=0,botoutofrange=14,/cbar,/sample
  help,id
  
END

PRO MCD43GF_LAT_LIM,D=d
  
  indir = 'MODIS_AVG_BRDF/'
  
  flist = file_search(indir,'*iso_Band1*.hdf')
  nf = n_elements(flist)
  
  
  
;   nf = 1
  
  lat_min = replicate(0.0d0,nf)
  lat_max = replicate(0.0d0,nf)
  
  day = replicate(0,nf)
  
  for f=0,nf-1 do begin
    
    d=read_mcd43gf( flist[f] )
    imx = n_elements(d.ts[*,0])
    jmx = n_elements(d.ts[0,*])
    
    n_valid = replicate(0L,jmx)
    
    for j=0,jmx-1 do begin
      
      id = where( d.ts[*,j] le 1000 )
      
      if( id[0] ge 0 ) then n_valid[j] = n_elements(id)
      
    endfor
    
    window,0
    plot,d.lat,n_valid,position=[.15,.15,.9,.9]
    
    yid = where(n_valid gt 0)
    lat_min[f] = min(d.lat[yid])
    lat_max[f] = max(d.lat[yid])
    
    ; Get the day
    frag = strsplit(flist[f],'/',/extract)
    fname= frag[n_elements(frag)-1]
    frag = strsplit(fname,'_',/extract)
    day[f] = fix(frag[4])
    print,day[f],lat_min[f],lat_max[f]
    
  endfor
  
  write_struct2h5,{day:day,lat_min:lat_min,lat_max:lat_max},$
                  './mcd43gf_lat_lim.h5'
  
  
END