FUNCTION PNPOLY, nvert, vertx, verty, testx, testy
  
  
  c = 0
  
  j = nvert-1
  
  for i=0,nvert-1 do begin
    
    if ( ((verty[i] gt testy) ne (verty[j] gt testy)) and $
          (testx lt (vertx[j]-vertx[i]) * (testy-verty[i]) / (verty[j]-verty[i]) + vertx[i]) ) then begin
       
       if(c eq 0) then c = 1 else c = 0
;        c = not c
;         c = !c;
    endif
      
    j = i
    
  endfor
  
  return,c
  
END

PRO PNPOLY_TEST
  
  ; Create grid of test points
  inpy = replicate(0,201,201)
  tx = findgen(201)/10.
  ty = findgen(201)/10.
  
  ; Create polygon
  vx = [2.,7.,12.,4.]
  vy = [1.,2.,7.,8.]
  
  nv = n_elements(vx)
  
  for i=0,200 do begin
  for j=0,200 do begin
    
    inpy[i,j] = pnpoly(nv,vx,vy,tx[i],ty[j])
    
  endfor
  endfor
  
  tvplot,inpy,tx,ty,/cbar,/noadvance
  plots,[vx,vx[0]],[vy,vy[0]],thick=2
  
  
  
END