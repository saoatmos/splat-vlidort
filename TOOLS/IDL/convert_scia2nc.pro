PRO CONVERT_SCIA2NC
  
  infile = '/home/cmiller/dumbo/uv_ler_paper/SCIA/LER_PCA/datasets/SCIAMACHY_Envisat_surface_LER_product.hdf5'
  
  outfile = './SCIAMACHY_Envisat_surface_LER_product.nc'
  
  ; Load file
  d = h5_parse(infile,/read)
  
  imx = n_elements(d.longitude._data)
  jmx = n_elements(d.latitude._data)
  wmx = n_elements(d.wavelength._data)
  
  ; Output ler
  ler = replicate(0.0,imx,jmx,wmx,12)
  for w=0,wmx-1 do begin
  for m=0,11 do begin
    ler[*,*,w,m] = d.mode_ler._data[m,w,*,*]
  endfor
  endfor
  
  
  ; Create netcdf file
  ; ------------------
  
  ; Create NetCDF
  id = ncdf_create(outfile,/clobber)
  
  ; Fill
  ncdf_control,id,/fill
  
  ; Define dimensions
  xid = ncdf_dimdef(id,'x',imx)
  yid = ncdf_dimdef(id,'y',jmx)
  wid = ncdf_dimdef(id,'w',wmx)
  mid = ncdf_dimdef(id,'m',12)
  
  ; Define variables
  vid    = ncdf_vardef(id,'LER',[xid,yid,wid,mid],/float)
  NCDF_ATTPUT, id, vid,    'long_name', 'Lambertian Equiv. Albedo'
  vid    = ncdf_vardef(id,'xmid',[xid],/float)
  NCDF_ATTPUT, id, vid,    'long_name', 'Longitude [deg]'
  vid    = ncdf_vardef(id,'ymid',[yid],/float)
  NCDF_ATTPUT, id, vid,    'long_name', 'Latitude [deg]'
  vid    = ncdf_vardef(id,'Wavelength',[wid],/float)
  NCDF_ATTPUT, id, vid,    'long_name', 'Wavelength [nm]'
  
  ; Put file in data mode:
  NCDF_CONTROL, id, /ENDEF
  
  ; Write 
  vid = NCDF_VARID(id, 'LER')
  ncdf_varput,id,vid,ler
  vid = NCDF_VARID(id, 'xmid')
  ncdf_varput,id,vid,d.longitude._data
  vid = NCDF_VARID(id, 'ymid')
  ncdf_varput,id,vid,d.latitude._data
  vid = NCDF_VARID(id, 'Wavelength')
  ncdf_varput,id,vid,d.wavelength._data
  
  NCDF_CLOSE, id ; Close the NetCDF file.
  
END