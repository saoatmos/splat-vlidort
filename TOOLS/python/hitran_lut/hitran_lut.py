# -*- coding: iso-8859-1 -*-

import numpy as np
from hapi import *
from pylab import *
from netCDF4 import Dataset
from scipy.interpolate import CubicSpline
import os

# Parallel processing
#from joblib import Parallel, delayed
import multiprocessing
from  multiprocessing import Pool

def parallel_xsec_computation( n, T_thread, p_thread, nu_min, nu_max, nu_step, \
                                  n_wnum, wnum_xs, hit_wvl, fwhm, nwvl, wvl,   \
                                  wvl_reg, Irad_reg_hr, Irad_reg_conv, db_name,\
                                  do_I0_correction, par_I0_vcd, kernel         ):
    
    # This is here for safety but for everything but I0 vcd is probably unnecessary to make local copies
    #T_thread = hitobj.T_vec[n]
    #p_thread = hitobj.p_vec[n]
    #nu_min   = hitobj.par_nu_min
    #nu_max   = hitobj.par_nu_max
    #nu_step  = hitobj.par_nu_step
    #n_wnum   = hitobj.par_n_wnum
    
    #wnum_xs  = hitobj.par_wnum_xs
    #hit_wvl  = hitobj.par_hit_wvl
    #fwhm     = hitobj.par_fwhm
    #nwvl     = hitobj.par_nwvl
    #wvl      = hitobj.par_wvl
    #wvl_reg  = hitobj.par_wvl_reg
    
    #Irad_reg_hr = hitobj.par_Irad_reg_hr
    #Irad_reg_conv = hitobj.par_Irad_reg_conv
    
    # Initilize cross section array
    hit_xs = np.zeros( n_wnum )
    
    # Output xs
    xs_lut = np.zeros( nwvl )
    
    for iso_name in db_name:
        
        # Compute cross section for isotopologue
        nu,absco = absorptionCoefficient_HT(SourceTables=iso_name,
                              HITRAN_units=True,
                              Environment={'T':T_thread,'p':p_thread},
                              OmegaRange=[nu_min,nu_max],
                              OmegaStep=nu_step)
        
        # Abundance is preweighted, therefore just need to sum for total xs
        hit_xs += absco
        
    # Spline 
    xs_int = CubicSpline( hit_wvl, hit_xs[::-1] )
    
    # Archive 
    if( fwhm > 0.0 ):
        
        
    
        # Cross section on regular grid
        xs_reg = xs_int( wvl_reg )
                
        if( do_I0_correction ):
                    
            if( par_I0_vcd == None ):
            
                # Small OD limit - scale so max value is 1e-4
                I0_vcd = 1.0e-4/np.max( xs_reg )
            
            else:
                I0_vcd = par_I0_vcd
            
            # Calculate I0 corrected cross section
            I_true = np.convolve(Irad_reg_hr*np.exp(-1.0*I0_vcd*xs_reg),kernel,mode='same')
            xs_conv = np.log( Irad_reg_conv/I_true )/I0_vcd

        else:
            
            # Do convolution
            xs_conv = np.convolve( xs_reg, kernel, mode='same' )
                    
        # Need to interpolate convolved spectrum to output grid
        xs_c_int = CubicSpline( wvl_reg, xs_conv )
            
        xs_lut = xs_c_int( wvl )
    
    else:
        
        xs_lut = xs_int( wvl )
        
    ## Track job completion
    #hitobj.xs_ct += 1.0
    
    #frac = hitobj.xs_ct/float(hitobj.T_vec.shape[0])
    #print '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'
    #print '%%%%%%%%%%%%%%%%%%% Percent Complete: ',frac*1e2
    #print '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'
    
    return [xs_lut,n]

def par_xs_wrapper( args ):
    return parallel_xsec_computation( *args )

class hitran_lut( object ):
    
    def __init__( self, spc_name ):
        
        # Save species name
        self.spc_name = spc_name
        
        # Names of HITRAN species
        hitran_molnames = [                                                       \
            'H2O'   , 'CO2'   , 'O3'    , 'N2O'   , 'CO'    , 'CH4'   , 'O2'    , \
            'NO'    , 'SO2'   , 'NO2'   , 'NH3'   , 'HNO3'  , 'OH'    , 'HF'    , \
            'HCL'   , 'HBR'   , 'HI'    , 'CLO'   , 'OCS'   , 'H2CO'  , 'HOCL'  , \
            'N2'    , 'HCN'   , 'CH3CL' , 'H2O2'  , 'C2H2'  , 'C2H6'  , 'PH3'   , \
            'COF2'  , 'SF6'   , 'H2S'   , 'HCOOH' , 'HO2'   , 'O'     , 'CLONO2', \
            'NO+'   , 'HOBR'  , 'C2H4'  , 'CH3OH' , 'CH3BR' , 'CH3CN' , 'CF4'   , \
            'C4H2'  , 'HC3N'  , 'H2'    , 'CS'    , 'SO3'   , 'C2N2'  , 'COCL2' ]
        nhitmol = len( hitran_molnames )
        
        # Number of isotopologues
        # Check SF6,ClONO2,CF4
        n_iso = np.array([  7, 12,  5,  5,  6,  4,  3,  3,  2,  1,\
                            2,  2,  3,  2,  4,  4,  2,  2,  5,  3,\
                            2,  2,  3,  2,  1,  3,  2,  1,  2,  1,\
                            3,  1,  1,  1,  1,  1,  2,  2,  1,  2,\
                            1,  1,  1,  1,  2,  4,  1,  1,  2     ])
        
        # Get the hitran molecule number 
        idx = [i for i, x in enumerate(hitran_molnames) if( x == spc_name )]
        nidx = len(idx)
        
        # Check index exists
        if( nidx == 0 ):
            print 'Molecule name ('+spc_name+') is not in HITRAN'
            print 'Here are the choices:'
            for i in range(nhitmol):
                print str(i+1).zfill(2),':',hitran_molnames[i]
            self.molnum = -1
        else:
            self.molnum = idx[0]+1
            self.n_iso  = n_iso[idx[0]]
        
        # Initialize padding for convolution
        self.nhw1e_pad=3.0
        
    def download_linelist( self, wlim, iso_list ):
        
        # Wavenumber limits
        nu_max = 1.0e7/wlim[0]
        nu_min = 1.0e7/wlim[1]
        
        # Initiliaze database name list
        self.db_name = []
        
        # Download line lists
        for i in iso_list:
            iso_name = self.spc_name + '_' + str(i).zfill(2)
            self.db_name.append( iso_name )
            fetch(iso_name,self.molnum,i,nu_min,nu_max)
            
    def sg_lineshape( self, x, fwhm, shape, asym ):
        
        # Extract ITF parameters for convenience
        par = [fwhm,shape,asym]
        
        # Prefactor
        A = par[1]  / (2.0*par[0]*gamma(1.0/par[1]))
        
        # Return supergaussian G(x)
        return A*np.exp( -1.0*np.power(np.abs( x/par[0] ),par[1]) )
    
    def read_solar_spec( self, infile, wvl, fwhm, shape, asym, norm=True ):
        
        # Read spectrum
        f = open( infile )
        data = np.genfromtxt( f,delimiter=' ')
        
        # Interpolate to calcuation grid
        Irad_int = CubicSpline( data[:,0].squeeze(),data[:,1].squeeze() )
        Irad = Irad_int( wvl )
        # Normalize to ~1?
        
        # Half width at 1/e for gaussian (fix later for asg)
        hw1e = fwhm / 1.665511
        pad = self.nhw1e_pad*hw1e + 20.0
        
        # Convolved spectrum 
        dwvl = wvl[1]-wvl[0]
        wvl_conv = np.arange( wvl[0]-pad,wvl[-1]+pad, dwvl )
        
        if( fwhm > 0.0 ):
        
            # Calculate kernel
            ndx = int( np.ceil( hw1e*self.nhw1e_pad/dwvl ) )
            x   =  ( np.arange( 2*ndx+1 ) - float(ndx) )*dwvl
            kernel = self.sg_lineshape( x, fwhm, shape,asym ) 
            kernel = kernel/np.sum(kernel) # Check normalization
        
            Irad_conv = np.convolve( Irad_int(wvl_conv), kernel, mode='same' )
            
            # Interpolate back to regular grid
            Irad_cint = CubicSpline( wvl_conv,Irad_conv )
            Irad_conv = Irad_cint( wvl )
        
        else:
            Irad_conv = Irad
        
        if( norm ):
            mean_Irad = np.mean(Irad)
            Irad = Irad / mean_Irad
            Irad_conv = Irad_conv / mean_Irad
        
        return Irad, Irad_conv
    
        
    def create_lut( self, outfile, p, T, wlim, dlambda,    fwhm=None,             \
                    do_I0_correction=False, I0_vcd=None, solar_ref_file=None,     \
                    iso_list=None, nu_step=None, nhw1e_pad=3.0,shape=2.0,asym=0.0 ):
        
        # INputs
        # p    - Pressure    in hPa
        # T    - Temperature in K
        # wlim - Wavelength  limits [nm]
        # FWHM -  Set if you want to convolve output 
        
        # Create a linelist folder with the output file name
        fname_noext = os.path.splitext(outfile)[0]
        db_folder = fname_noext + '_linelist'
        db_begin( db_folder )
        
        # If isotopologue list is not set, do all of them
        if( iso_list is None ):
            iso_list = range( 1,self.n_iso+1 )
            
        # Set default sampling for now - later this can be determined from the linelists
        if( nu_step is None ):
            nu_step = 0.001
        
        # Determine padding
        if( fwhm is None ):
            pad = 0.0
        else:
            
            # Half width at 1/e for gaussian (fix later for asg)
            hw1e = fwhm / 1.665511
            pad = nhw1e_pad*hw1e+ 20.0
        
        self.nwh1e_pad = nhw1e_pad
        
        # Wavenumber limits
        nu_max = 1.0e7/(wlim[0]-pad)
        nu_min = 1.0e7/(wlim[1]+pad)
        
        # Output wavelength grid
        wvl = np.arange(wlim[0],wlim[1],dlambda)
        nwvl = wvl.shape[0]
        
        # Pressure in atmospheres
        p_atm = p / 1013.25
        
        # Build vector of p/T to parse to cross section calculation
        n_p = p.shape[0]
        n_T  = T.shape[0]
        n_iso = len(iso_list)
        
        # Output cross section
        xs_lut = np.zeros( (n_p,n_T,nwvl) )
        
        # Fetch the line lists
        self.download_linelist( wlim, iso_list )
        
        # Kludge - We need to compute a spectrum to get approprate dimension returned by hapi
        # Pick the smallest isomer to reduce number of lines 
        wnum_xs,absco = absorptionCoefficient_HT(SourceTables=self.db_name[-1],
                              HITRAN_units=True,
                              Environment={'T':T[0],'p':p_atm[0]},
                              OmegaRange=[nu_min,nu_max],
                              OmegaStep=nu_step)
        
        # Get Wavelength dimension
        n_wnum = wnum_xs.shape[0]
        
        # Convert to nm and reverse
        hit_wvl = 1.0e7/wnum_xs[::-1]
        
        # Minimum spacing
        hit_dwvl = hit_wvl[1]-hit_wvl[0]
                                
        # Regular wavelength grid
        wvl_reg = np.arange(hit_wvl[0],hit_wvl[-1],hit_dwvl)
        
        # If we are convolving we must create a regular grid
        if( fwhm > 0.0 ):
            
            # Compute Supergaussian convolution function
            self.ndx    = int( np.ceil( hw1e*nhw1e_pad/hit_dwvl ) )
            self.x      =  ( np.arange( 2*self.ndx+1 ) - float(self.ndx) )*hit_dwvl
            self.kernel = self.sg_lineshape( self.x, fwhm, shape,asym ) 
            self.kernel = self.kernel/np.sum(self.kernel) # Check normalization
                                
            # If doing I0 correction load solar reference file to output grid
            if( do_I0_correction ):
                Irad_reg_hr, Irad_reg_conv = self.read_solar_spec(solar_ref_file, wvl_reg,fwhm, shape,asym)
            else:
                Irad_reg_hr   = np.ones(wvl_reg.shape)
                Irad_reg_conv = np.ones(wvl_reg.shape)
                
        else:
            self.kernel = 0.0 #None
            Irad_reg_hr = 0.0 #None
            Irad_reg_conv = 0.0# None
        
        # ------------------------------------------------------------------
        # Parallel computation - vectorize inputs
        # ------------------------------------------------------------------
        i_mesh,j_mesh = np.meshgrid( range(n_p), range(n_T) )
        i_vec = i_mesh.flatten()
        j_vec = j_mesh.flatten()
        
        # Values needed in the parrallel routine
        T_vec = T[j_vec]
        p_vec = p_atm[i_vec]
         
        # Get Cores
        ncores = multiprocessing.cpu_count()
        db_name = self.db_name
        kernel = self.kernel
        
        if( True ):
        
            pp = Pool( ncores )
            task = range(len(T_vec))
            xs_vec = pp.map( par_xs_wrapper, \
                            ((n,T_vec[n],p_vec[n],nu_min, nu_max, nu_step, n_wnum, wnum_xs, \
                              hit_wvl, fwhm, nwvl, wvl,wvl_reg, Irad_reg_hr, Irad_reg_conv, \
                              db_name,do_I0_correction, I0_vcd, kernel) for n in task ) )
            
            for n in task:
                
                # Task number 
                v = xs_vec[n][1]
                
                # Store value
                xs_lut[i_vec[v],j_vec[v],:] = xs_vec[n][0][:]
        
        else:
            
            task = range(len(T_vec))
            
            # For checking against serial computation
            for n in task:
                
                xs_lut[i_vec[n],j_vec[n],:] = parallel_xsec_computation(            \
                                  n, T_vec[n], p_vec[n], nu_min, nu_max, nu_step, \
                                  n_wnum, wnum_xs, hit_wvl, fwhm, nwvl, wvl,      \
                                  wvl_reg, Irad_reg_hr, Irad_reg_conv, db_name,   \
                                  do_I0_correction, I0_vcd, kernel )[0][:]
        
        # Check for zeros
        xs_lut[xs_lut<0.0] = 0.0
        
        # ----------------------------------------------------
        # Write Cross section LUT to disk
        # ----------------------------------------------------
        
        # Open output file
        ncid = Dataset( outfile, 'w' )
        
        # Create dimensions
        ncid.createDimension('npos',nwvl)
        ncid.createDimension('np',n_p)
        ncid.createDimension('nT',n_T)
        
        # Write variables
        vid = ncid.createVariable('Wavelength' ,np.float64, ('npos'))
        ncid.variables['Wavelength'][:] = wvl
        vid = ncid.createVariable('Temperature' ,np.float64, ('nT'))
        ncid.variables['Temperature'][:] = T
        vid = ncid.createVariable('Pressure' ,np.float64, ('np'))
        ncid.variables['Pressure'][:] = p
        vid = ncid.createVariable('CrossSection' ,np.float64, ('np','nT','npos'))
        ncid.variables['CrossSection'][:,:,:] = xs_lut
        
        # Remove database folder
        os.system('rm -rf ' + db_folder)
        
