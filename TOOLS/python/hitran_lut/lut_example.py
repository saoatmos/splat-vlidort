# -*- coding: iso-8859-1 -*-

import numpy as np
from hitran_lut import hitran_lut
from pylab import *

def lut_outfile( molname, wlim, fwhm_str, I0_vcdstr, do_I0, extra='' ):
    
    # Wavelength limit string
    wlim_str = str(int(wlim[0])) + '-' + str(int(wlim[1])) + 'nm'
    
    filename = 'HITRAN2016_'+molname + '_' + wlim_str + '_' + fwhm_str + 'fwhm'
    
    if( do_I0 ):
        filename = filename + '_' + I0_vcdstr + 'I0Corr'
    
    if( extra != '' ):
        filename = filename + '_' + extra
    
    
    filename = filename + '.nc'
    
    return filename


# Reference file for 
solar_infile='../../../data/SAO_crosssections/newkpno_001_new.spc_gt600nm'

#######################################################
# Molecule parameters
#######################################################

# H2O
molname  = ['H2O','O2','CO2','CH4']

# Isomers to include [hitran indices)
iso_list = []
iso_list.append([1,2,3,4,5,6]) # H2O
iso_list.append([1,2,3]) # O2
iso_list.append([1,2,3,4,5,6,7,8]) # CO2
iso_list.append([1,2,3]) # CH4
nmol = len(molname)


#######################################################
# Grid Parameters
#######################################################
wlim     = [1200.0,1750.0]
dlambda  = 0.01
fwhm     = 0.02 ; fwhm_str = '0p02'
I0_vcd   = None ; I0_vcdstr = 'smallOD'
do_I0    = False
p = np.linspace(0.01,1100.0,23)
T = np.linspace(155.0,340.0,38)
########################################################

for n in range(nmol):

    # Name the output file
    outfile = lut_outfile( molname[n],wlim,fwhm_str,I0_vcdstr,do_I0)


    # Initialize file
    hitran = hitran_lut( molname[n] )


    # Compute lookup table
    hitran.create_lut( outfile, p, T, wlim, dlambda,\
                       iso_list=iso_list[n],fwhm=fwhm,do_I0_correction=do_I0,\
                       solar_ref_file=solar_infile)


