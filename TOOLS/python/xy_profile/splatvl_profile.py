# -*- coding: iso-8859-1 -*-
#import h5py
import numpy as np
import datetime
from sat_view_geom import ViewGeoObj
from netCDF4 import Dataset
from datetime import timedelta
from scipy import constants as cnst

class SplatVL_profile( object ):
    
    def __init__( self, pedge, Tedge, zedge=None, g=None, Mair=None ):
        
        # Check if single profile input
        if( len(pedge.shape) == 1 ):
            pedge = np.reshape(pedge,(1,1,pedge.shape[0]))
            Tedge = np.reshape(Tedge,(1,1,Tedge.shape[0]))
        # Or a 2D list
        elif( len(pedge.shape) == 2 ):
            pedge = np.reshape(pedge,(1,pedge.shape[0],pedge.shape[1]))
            Tedge = np.reshape(Tedge,(1,pedge.shape[0],pedge.shape[1]))
        
        # Get dimensions
        self.imx = pedge.shape[0]
        self.jmx = pedge.shape[1]
        self.lmx = pedge.shape[2]-1
        self.n_ltype  = 17
        
        # Set to Earth surface gravity by default
        if( g is None ):
            g = np.ones( self.lmx+1 )*9.81
        
        # Molecular weight of atmospheric gas (Earth default dry air mixing ratio
        if( Mair is None ):
            Mair = 28.97e-3 # kg/mol
            
        # Altitude profile [km]
        if( zedge is None ):
            
            # Altitude
            zedge = np.zeros( pedge.shape )
            
            # scale height
            H = np.zeros( pedge.shape[0:2] )
            for l in range(1,self.lmx+1):
                H[:,:] = (0.5*(Tedge[:,:,l]+Tedge[:,:,l-1])*cnst.R/Mair/g[l])*1e-3
                zedge[:,:,l] = zedge[:,:,l-1] + H[:,:]*np.log( pedge[:,:,l-1]/pedge[:,:,l] )
        
        # Initialize output arrays
        # ------------------------
        
        self.year  = 0
        self.month = 0
        self.day   = 0
        
        # Special fields
        self.elon = np.zeros((self.imx,self.jmx,4))
        self.elat = np.zeros((self.imx,self.jmx,4))
        
        # Initialize vertical pressure param
        self.bp = np.zeros(self.lmx+1)
        self.ap = np.zeros(self.lmx+1)
        
        # Initialize land type fraction
        self.ltype_fr = np.zeros((self.imx,self.jmx,self.n_ltype))
        
        # Clouds 
        self.nc_pix = 0
        #self.cpix_cf[:,:,:]
        #self.fld_cod = {}
        #self.fld_cod[name][:,:,:,:]
        
        
        self.fld_2d = {} ; shp_2d = (self.imx,self.jmx)
        # added
        self.fld_2d['Longitude']= np.zeros( shp_2d,dtype=np.float32 )
        self.fld_2d['Latitude']= np.zeros( shp_2d,dtype=np.float32 )
        self.fld_2d['SIF_734nm']= np.zeros( shp_2d,dtype=np.float32 )
        
        # standard
        self.fld_2d['Hour'] = np.zeros( shp_2d,dtype=np.float32 )
        self.fld_2d['SurfaceTemperature'] = np.zeros( shp_2d,dtype=np.float32 )
        self.fld_2d['SurfacePressure'] = np.zeros( shp_2d,dtype=np.float32 )
        self.fld_2d['IsOcean'] = np.zeros( shp_2d,dtype=np.int )
        self.fld_2d['WindDirection'] = np.zeros( shp_2d,dtype=np.float32 )
        self.fld_2d['WindSpeed'] = np.zeros( shp_2d, dtype=np.float32 )
        self.fld_2d['OceanSalinity'] = np.zeros( shp_2d, dtype=np.float32 )
        self.fld_2d['Chlorophyll'] = np.zeros( shp_2d, dtype=np.float32 )
        self.fld_2d['TerrainHeight'] = np.zeros( shp_2d, dtype=np.float32 )
        self.fld_2d['NumberOfCloudPixels'] = np.zeros( shp_2d, dtype=np.int )
        self.fld_2d['CloudTopPressure'] = np.zeros( shp_2d, dtype=np.float32 )
        self.fld_2d['SnowFraction'] = np.zeros( shp_2d, dtype=np.float32 )
        self.fld_2d['NumberOfCloudPixels'] = np.zeros( shp_2d, dtype=np.int )
        self.fld_2d['SnowFraction'] = np.zeros( shp_2d, dtype=np.float32 )
        self.fld_2d['SnowMass'] = np.zeros( shp_2d, dtype=np.float32 )
        self.fld_2d['SnowDepth'] = np.zeros( shp_2d, dtype=np.float32 )
        self.fld_2d['SeaIceFraction'] = np.zeros( shp_2d, dtype=np.float32 )
        self.fld_2d['TopSnowAge'] = np.zeros( shp_2d, dtype=np.float32 )
        self.fld_2d['TropopausePressure'] = np.zeros( shp_2d, dtype=np.float32 )
        self.fld_2d['DoCalculation'] = np.ones( shp_2d, dtype=np.int16 )
        
        # Geometry
        self.fld_2d['SZA'] = np.zeros( shp_2d,dtype=np.float32 )
        self.fld_2d['VZA'] = np.zeros( shp_2d,dtype=np.float32 )
        self.fld_2d['AZA'] = np.zeros( shp_2d,dtype=np.float32 )
        self.fld_2d['SAA'] = np.zeros( shp_2d,dtype=np.float32 )
        self.fld_2d['VAA'] = np.zeros( shp_2d,dtype=np.float32 )
        
        # Initialize the 3D edge fields
        self.fld_3de = {} ; dim_3de = [self.imx,self.jmx,self.lmx+1]
        self.fld_3de['Pressure'] = np.zeros( dim_3de, dtype=np.float32) ; self.fld_3de['Pressure'][:,:,:] = pedge
        self.fld_3de['TATM'] = np.zeros(dim_3de,dtype=np.float32)       ; self.fld_3de['TATM'][:,:,:] = Tedge
        self.fld_3de['Altitude'] = np.zeros(dim_3de,dtype=np.float32)   ; self.fld_3de['Altitude'][:,:,:] = zedge
        
        # Initialize 3d field
        self.fld_3d = {} ; dim_3d = [self.imx,self.jmx,self.lmx]
        self.fld_3d['RH'] = np.zeros(dim_3d,dtype=np.float32)
        
    def add_geolocation( self, lon, lat, elon=None, elat=None ):
        
        
        self.fld_2d['Longitude'][:,:] = lon
        self.fld_2d['Latitude'][:,:]  = lat
        
        # Archived for using viewing geometry
        self.lon = lon
        self.lat = lat
        
        if( elon is None ):
            
            # Try computing based on spacings between points [not implented]
            self.elon[:,:,0] = 0.0
            self.elon[:,:,1] = 0.0
            self.elon[:,:,2] = 0.0
            self.elon[:,:,3] = 0.0
        
        else:
            self.elon[:,:,:] = elon
        
        if( elat is None ):
            
            # Try computing based on spacings between points [not implented]
            self.elat[:,:,0] = 0.0
            self.elat[:,:,1] = 0.0
            self.elat[:,:,2] = 0.0
            self.elat[:,:,3] = 0.0
        else:
            self.elat[:,:,:] = elat
            
    def add_2d_field( self, name, data ):
        
        ## Check if single profile input
        #if( len(data.shape) == 1 ):
            #data = np.reshape(data,(1,1,data.shape[0]))
        ## Or a 2D list
        #elif( len(data.shape) == 2 ):
            #data = np.reshape(data,(1,data.shape[0],data.shape[1]))
        
        # Field dimension
        dim_2d = [self.imx,self.jmx]
        
        if( name in self.fld_2d ):
            self.fld_2d[name][:,:] = data
        else:
            self.fld_2d[name] = np.zeros( dim_2d,dtype=np.float32 )
            self.fld_2d[name][:,:] = data
            
    
    def add_3d_field( self, name, data ):
        
        # Check if single profile input
        if( len(data.shape) == 1 ):
            data = np.reshape(data,(1,1,data.shape[0]))
        # Or a 2D list
        elif( len(data.shape) == 2 ):
            data = np.reshape(data,(1,data.shape[0],data.shape[1]))
        
        # Field dimension
        dim_3d = [self.imx,self.jmx,self.lmx]
        
        if( name in self.fld_3d ):
            self.fld_3d[name][:,:,:] = data
        else:
            self.fld_3d[name] = np.zeros( dim_3d,dtype=np.float32 )
            self.fld_3d[name][:,:,:] = data
            
    
    def add_cld_fields( self, cpix_cf, cod ):
        
        # COD is a dictionary containing cloud fields
        cldname = cod.keys()
        
        
        # Get new dimensions
        self.nctype = len(cldname)
        self.nc_pix = len(cpix_cf[0,0:])
        
        # Update
        dim = [self.imx, self.jmx, self.nc_pix]
        self.cpix_cf = np.zeros( dim, dtype=np.float32 ) ; self.cpix_cf[:,:,:] = cpix_cf
        
        self.fld_cod = {}
        dim = [self.imx,self.jmx,self.lmx,self.ncpix]
        for n in range(nctype):
            self.fld_cod[cldname[n]] = np.zeros(dim,dtype=np.float32)
            self.fld_cod[cldname[n]][:,:,:] = cod[cldname[n]]
        
    def add_sat_earth_viewgeo( self, satlat, satlon, satalt, earthradius=None ):
        
        if(earthradius is None):
              earthradius = 6371.0 # km
        
        # Initialize viewing geometry object
        SatView = ViewGeoObj()
        
        for i in range(0,self.lon.shape[0]):
            for j in range(0,self.lat.shape[1]):
                
                # Compute viewing geometry
                SatView.compute_geom( self.lon[i,j],self.lat[i,j],
                                      satlon[i,j], satlat[i,j], satalt[i,j], earthradius,
                                      self.jday, self.inr_hr[i]                         )
                                      
                self.fld_2d['SZA'][i,j] = SatView.sza
                self.fld_2d['VZA'][i,j] = SatView.vza
                self.fld_2d['SAA'][i,j] = SatView.saa
                self.fld_2d['VAA'][i,j] = SatView.vaa
                self.fld_2d['AZA'][i,j] = SatView.aza
    
    def get_nc_att(self,name):
        
        if  ( name == 'SZA'):
            long_name = u'Solar zenith angle in degrees'
            dtype = np.float32
        elif( name == 'SAA'):
            long_name = u'Solar azimuth angle in degrees'
            dtype = np.float32
        elif( name == 'VZA' ):
            long_name = u'Viewing zenith angle in degrees'
            dtype = np.float32
        elif( name == 'AZA' ):
            long_name = u'Relative azimuth angle in degrees'
            dtype = np.float32
        elif( name == 'SurfaceTemperature' ):
            long_name = u'Surface temperature in K'
            dtype = np.float32
        elif( name == 'SurfacePressure' ):
            long_name = u'Surface pressure in hPa'
            dtype = np.float32
        elif( name == 'IsOcean' ):
            long_name = u'Flag for ocean scene'
            dtype = np.float32
        elif( name == 'WindDirection' ):
            long_name = u'Wind direction clockwise from north'
            dtype = np.float32
        elif( name == 'WindSpeed' ):
            long_name = u'Surface wind speed in m/s'
            dtype = np.float32
        elif( name == 'CloudTopPressure' ):
            long_name = u'Cloud top pressure in hPa'
            dtype = np.float32
        elif( name == 'Chlorophyll' ):
            long_name = u'Chlorophyll concentration in mg/m3'
            dtype = np.float32
        elif( name == 'OceanSalinity' ):
            long_name = u'Ocean water salinity in ppt'
            dtype = np.float32
        elif( name == 'TerrainHeight' ):
            long_name = u'Terrain height in km'
            dtype = np.float32
        elif( name == 'CornerLongitudes' ):
            long_name = u'Longitudes at pixel corners'
            dtype = np.float32
        elif( name == 'CornerLatitudes' ):
            long_name = u'Latitudes at pixel corners'
            dtype = np.float32
        elif( name == 'NumberOfCloudPixels' ):
            long_name = u'Number of cloud pixels'
            dtype = np.int16
        elif( name == 'CloudPixelFractions' ):
            long_name = u'Geometric Cloud fraction for each pixel'
            dtype = np.float32
        elif( name == 'Pressure' ):
            long_name = u'Pressure in hPa at grid box edges'
            dtype = np.float32
        elif( name == 'TATM' ):
            long_name = u'Temperature in K at grid box edges'
            dtype = np.float32
        elif( name == 'Altitude' ):
            long_name = u'Altitude in km at grid box edges'
            dtype = np.float32
        elif( name == 'SnowFraction'):
            long_name = u'Land snow cover fraction'
            dtype = np.float32
        elif( name == 'SnowMass'):
            long_name = u'Land snow mass in kg/m2'
            dtype = np.float32
        elif( name == 'SnowDepth'):
            long_name = u'Land snow depth in m'
            dtype = np.float32
        elif( name == 'SeaIceFraction'):
            long_name = u'Sea ice fraction'
            dtype = np.float32
        elif( name == 'TopSnowAge'):
            long_name = u'Top snow age in days'
            dtype = np.float32
        elif( name == 'DoCalculation'):
            long_name = u'Flag to do radiative transfer calculation'
            dtype = np.int16
        elif( name == 'EtaA' ):
            long_name = u'Eta A Coordinate'
            dtype = np.float32
        elif( name == 'EtaB' ):
            long_name = u'Eta B Coordinate'
            dtype = np.float32
        elif( name == 'Hour' ):
            long_name = u'UTC decimal hour'
            dtype = np.float32
        else:
            long_name = u''
            dtype = np.float32
        
        return long_name,dtype
        
    def write_to_nc( self, outfile ):
        
        # NB C and Python write netcdf files with 
        # index order reversed compared to fortran
        
        # Write a netcdf file
        nc_fid = Dataset( outfile , 'w', clobber=True)

        # Define dimensions
        nc_fid.createDimension('x',self.imx)
        nc_fid.createDimension('y',self.jmx)
        nc_fid.createDimension('z',self.lmx)
        if( self.nc_pix > 0 ):
            nc_fid.createDimension('nc_max',self.nc_pix)
        nc_fid.createDimension('one',1)
        nc_fid.createDimension('xe',self.imx+1)
        nc_fid.createDimension('ye',self.jmx+1)
        nc_fid.createDimension('ze',self.lmx+1)
        nc_fid.createDimension('l',self.n_ltype)
        nc_fid.createDimension('four',4)
        
        
        # Create variables
        vid = nc_fid.createVariable('ProfileIsLevel' ,np.int16, ('one'))
        vid.setncatts({'long_name': u'Flag for profile variables at grid midpoints'}) 
        nc_fid.variables['ProfileIsLevel'][:] = 0
        
        vid = nc_fid.createVariable('Year' ,np.int16, ('one'))
        vid.setncatts({'long_name': u'Year of observation'})
        nc_fid.variables['Year'][:] = self.year
        
        vid = nc_fid.createVariable('Month' ,np.int16, ('one'))
        vid.setncatts({'long_name': u'Month of observation'})
        nc_fid.variables['Month'][:] = self.month

        vid = nc_fid.createVariable('Day' ,np.int16, ('one'))
        vid.setncatts({'long_name': u'Day of observation'})
        nc_fid.variables['Day'][:] = self.day
        
        vid = nc_fid.createVariable('CornerLongitudes' ,np.float32, ('four','y','x'))
        vid.setncatts({'long_name': u'Longitudes at pixel corners'})
        nc_fid.variables['CornerLongitudes'][:,:,:] = np.transpose( self.elon, (2,1,0) )

        vid = nc_fid.createVariable('CornerLatitudes' ,np.float32, ('four','y','x'))
        vid.setncatts({'long_name': u'Latitudes at pixel corners'})
        nc_fid.variables['CornerLatitudes'][:,:,:] = np.transpose( self.elat, (2,1,0) )
            
        vid = nc_fid.createVariable('LandTypeFraction',np.float32,('l','y','x'))
        vid.setncatts({'long_name': u'Land type fraction (IGBP Classification)'})
        nc_fid.variables['LandTypeFraction'][:,:,:] = np.transpose( self.ltype_fr, (2,1,0) )
            
        vid = nc_fid.createVariable('EtaA' ,np.float32, ('ze'))
        vid.setncatts({'long_name': u'EtaA Coordinate'})
        nc_fid.variables['EtaA'][:] = self.ap
        
        vid = nc_fid.createVariable('EtaB' ,np.float32, ('ze'))
        vid.setncatts({'long_name': u'EtaB Coordinate'})
        nc_fid.variables['EtaB'][:] = self.bp
        
        # Create the 2D fields
        for name in self.fld_2d.keys():
            long_name,dtype = self.get_nc_att(name)
            vid = nc_fid.createVariable(name ,dtype, ('y','x'))
            vid.setncatts({'long_name': long_name})
            nc_fid.variables[name][:,:] = self.fld_2d[name][:,:].T

        # Create 3D fields
        for name in self.fld_3d.keys():
            vid = nc_fid.createVariable(name ,np.float32, ('z','y','x'))
            nc_fid.variables[name][:,:,:] = self.fld_3d[name][:,:,:].transpose( [2,1,0] )

        # Create 3D edge fields
        for name in self.fld_3de.keys():
            vid = nc_fid.createVariable(name ,np.float32, ('ze','y','x'))
            nc_fid.variables[name][:,:,:] = self.fld_3de[name][:,:,:].transpose( [2,1,0] )
        
        if( self.nc_pix > 0 ):
        
            # Cloud fields
            vid = nc_fid.createVariable('CloudPixelFractions',np.float32,('nc_max','y','x'))
            vid.setncatts({'long_name': u'Geometric Cloud fraction for each pixel'})
            nc_fid.variables['CloudPixelFractions'][:,:,:] = self.cpix_cf[:,:,:].transpose( [2,1,0] )

            # Cloud optical depths
            for name in self.fld_cod.keys(): 
                vid = nc_fid.createVariable(name ,np.float32, ('nc_max','z','y','x'))
                nc_fid.variables[name][:,:,:,:] = self.fld_cod[name][:,:,:,:].transpose( [3,2,1,0] )
        
        # Close file
        nc_fid.close()
        