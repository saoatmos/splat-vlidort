# -*- coding: iso-8859-1 -*-

import numpy as np
from scipy.interpolate import CubicSpline

def atan_idl( y, x ):
    
    
    arr = np.zeros(x.shape)
    idx = np.where( x == 0 )
    if( arr[idx].shape[0] > 0 ):
        arr[idx] = np.pi/2.0
    idx = np.where( x != 0 )
    if( arr[idx].shape[0] > 0 ):
        arr[idx] = np.arctan(y/x)
    
    # Check quadrants
    idx = np.where ( (y > 0) & (x < 0) )
    if( arr[idx].shape[0] > 0 ):
        arr[idx] = np.pi- arr[idx]
    idx = np.where ( (y < 0) & (x < 0) )
    if( arr[idx].shape[0] > 0 ):
        arr[idx] = arr[idx]-np.pi
    
    return arr
    
class ViewGeoObj(object):

    def __init__(self):

        # Define some parameters for zensun
        self.nday=np.array([  1.0,   6.0,  11.0,  16.0,  21.0,  26.0,  31.0,  36.0,  41.0,  46.0,\
 		                         51.0,  56.0,  61.0,  66.0,  71.0,  76.0,  81.0,  86.0,  91.0,  96.0,\
                            101.0, 106.0, 111.0, 116.0, 121.0, 126.0, 131.0, 136.0, 141.0, 146.0,\
                            151.0, 156.0, 161.0, 166.0, 171.0, 176.0, 181.0, 186.0, 191.0, 196.0,\
                            201.0, 206.0, 211.0, 216.0, 221.0, 226.0, 231.0, 236.0, 241.0, 246.0,\
                            251.0, 256.0, 261.0, 266.0, 271.0, 276.0, 281.0, 286.0, 291.0, 296.0,\
                            301.0, 306.0, 311.0, 316.0, 321.0, 326.0, 331.0, 336.0, 341.0, 346.0,\
                            351.0, 356.0, 361.0, 366.0])
        
        self.eqt=np.array([ -3.23, -5.49, -7.60, -9.48,-11.09,-12.39,-13.34,-13.95,-14.23,-14.19,\
				                   -13.85,-13.22,-12.35,-11.26,-10.01, -8.64, -7.18, -5.67, -4.16, -2.69,\
					                  -1.29, -0.02,  1.10,  2.05,  2.80,  3.33,  3.63,  3.68,  3.49,  3.09,\
		                         2.48,  1.71,  0.79, -0.24, -1.33, -2.41, -3.45, -4.39, -5.20, -5.84,\
    				                -6.28, -6.49, -6.44, -6.15, -5.60, -4.82, -3.81, -2.60, -1.19,  0.36,\
                             2.03,  3.76,  5.54,  7.31,  9.04, 10.69, 12.20, 13.53, 14.65, 15.52,\
			                      16.12, 16.41, 16.36, 15.95, 15.19, 14.09, 12.67, 10.93,  8.93,  6.70,\
		                         4.32,  1.86, -0.62, -3.23])

        self.dec=np.array([-23.06,-22.57,-21.91,-21.06,-20.05,-18.88,-17.57,-16.13,-14.57,-12.91,\
		                       -11.16, -9.34, -7.46, -5.54, -3.59, -1.62,  0.36,  2.33,  4.28,  6.19,\
                             8.06,  9.88, 11.62, 13.29, 14.87, 16.34, 17.70, 18.94, 20.04, 21.00,\
		                        21.81, 22.47, 22.95, 23.28, 23.43, 23.40, 23.21, 22.85, 22.32, 21.63,\
		                        20.79, 19.80, 18.67, 17.42, 16.05, 14.57, 13.00, 11.33,  9.60,  7.80,\
		                         5.95,  4.06,  2.13,  0.19, -1.75, -3.69, -5.62, -7.51, -9.36,-11.16,\
		                       -12.88,-14.53,-16.07,-17.50,-18.81,-19.98,-20.99,-21.85,-22.52,-23.02,\
		                       -23.33,-23.44,-23.35,-23.06])
				
        # Create spline objects
        self.cs_eqt = CubicSpline( self.nday, self.eqt )
        self.cs_dec = CubicSpline( self.nday, self.dec )
			   
    def zensun(self,day,time,lat,lon,local=False):
		    
        ####################################
        # compute the subsolar coordinates #
        ####################################
        
        # Fractional day number wirht 1 Jan 12 am = 1
        tt = ( ( np.fix(day)+time/24.-1 ) % 365.25 ) + 1.
        
        #
        eqtime = self.cs_eqt( tt ) / 60.0
        decang = self.cs_dec( tt )
        
        latsun = decang
        
        if( local ):
            #
            lonorm=((lon + 360 + 180 ) % 360 ) - 180.
            tzone=np.fix((lonorm+7.5)/15)
            index = np.where( lonorm < 0 )
            if( lonorm[index].shape[0] > 0 ):
                tzone[index] = np.fix((lonorm[index]-7.5)/15)
            ut=(time-tzone+24.) % 24.
            noon=tzone+12.-lonorm/15.
        else:
            # 
            ut=time
            noon=12.-lon/15.
        
        lonsun=-15.*(ut-12.+eqtime)

        # compute the solar zenith, azimuth and flux multiplier
        dtor = np.pi / 180.0
        t0=(90.-lat)*dtor                            # colatitude of point
        t1=(90.-latsun)*dtor                         # colatitude of sun

        p0=lon*dtor                                  # longitude of point
        p1=lonsun*dtor                               # longitude of sun

        zz=np.cos(t0)*np.cos(t1)+np.sin(t0)*np.sin(t1)*np.cos(p1-p0) # up          \
        xx=np.sin(t1)*np.sin(p1-p0)                                  # east-west    > rotated coor
        yy=np.sin(t0)*np.cos(t1)-np.cos(t0)*np.sin(t1)*np.cos(p1-p0) # north-south /
        
        
        self.saa =atan_idl(xx,yy)/dtor 
        self.sza=np.arccos(zz)/dtor                        # solar zenith

        rsun=1.-0.01673*np.cos(.9856*(tt-2.)*dtor)      # earth-sun distance in AU
        self.solfac=zz/np.power(rsun,2)                 # flux multiplier
        
        angsun = 0.
        arg=-(np.sin(angsun)+np.cos(t0)*np.cos(t1))/(np.sin(t0)*np.sin(t1))
        self.sunrise = np.zeros(arg.shape)  
        self.sunset  = np.ones(arg.shape)*24. 
        index = np.where(abs(arg) < 1.0)
        if( arg[index].shape[0] > 0 ):
            dtime=np.arccos(arg[index])/(dtor*15)
            self.sunrise[index]=noon-dtime-eqtime[index]
            self.sunset[index] =noon+dtime-eqtime[index]
            
    def zenview(self,lonp, latp, lonss, latss, h, re):
        
        # Degrees to radians
        DtoR = np.pi / 180.0
        
        # sin(angular radius of Earth at height h)
        srho = re/(re+h)
        
        deltaL = abs(lonss-lonp)
        cdeltaL = np.cos(deltaL * DtoR)
        clatss = np.cos(latss * DtoR)
        slatss = np.sin(latss * DtoR)
        clatp = np.cos(latp * DtoR)
        slatp = np.sin(latp * DtoR)
        
        # Find lambda, central angle of great circle arc connecting P and SSP.
        # use Law of Cosines for spherical triangle with vertices P, SSP, and North Pole.
        # sides are central angles (great circle arcs) 90-LatP, 90-LatSS, and Lambda.
        # Law of Cosines: cos(c) = cos(a) cos(b) +sin(a) sin(b) cos(C),
        # where a, b, c are the sides and C is the corner angle opposite side c.
        
        # cos(lambda)
        clambda = slatss * slatp + clatss * clatp * cdeltaL
        
        # sin(lambda) 
        slambda = np.sin(np.arccos(clambda))
        
        
        # cos phiv (Phi_V is azimuth of satellite measured from North at target P).
        # Use Law of Cosines on Spherical Triangle formed by P, North Pole, SSP.
        
        cphiv = (slatss - slatp * clambda) / ( clatp * slambda)
        if( cphiv > 1.0 ):
            cphiv = 1.0
        if( cphiv < -1.0 ):
            cphiv = -1.0
            
        phiv = np.arccos(cphiv)
        
        # tan eta
        
        taneta = srho * slambda / (1.0 - srho * clambda)
        eta = np.arctan(taneta)
        
        # cos epsilon
        
        ceps = np.sin(eta)/srho
        eps = np.arccos(ceps)
        
        self.vaa = phiv / DtoR
        if( lonp-lonss > 0.0 ):
            self.vaa = 360.0 - self.vaa
        self.vza = eps / DtoR
        
        # Check for spacecraft below horizon at target
        
        lambda1 = np.arccos(clambda) / DtoR
        lambda0 = np.arccos(srho) / DtoR
        if( lambda1 > lambda0 ):
            #print, 'WARNING: SPACECRAFT BELOW HORIZON AT TARGET'
            #print, 'Lambda  (Central Angle between Target and SSP) = ', lambda
            #print, 'Lambda0 (Central Angle Visible to Spacecraft)  = ', lambda0
            self.vza = -self.vza
        
        
        self.vza = 90.0 - self.vza

    def compute_geom(self, lon, lat, satlon, satlat, satalt, \
                         earthradius, jday, utc):
            
        # Zensun requires np arrays
        self.lon    = np.array( [lon]  )
        self.lat    = np.array( [lat]    )
        self.satlon = np.array( [satlon] )
        self.satlat = np.array( [satlat] )
        self.satalt = np.array( [satalt] )
        self.earthradius = np.array( [earthradius] )
        self.jday   = np.array([jday])
        self.utc    = np.array([utc] )
            
        # Compute viewing geom
        self.zenview( self.lon, self.lat, self.satlon, self.satlat, \
                      self.satalt, self.earthradius)
            
        # Compute solar geometry
        self.zensun(self.jday, self.utc, self.lat, self.lon)
            
        # Compute relative azimuth
        self.aza = self.vaa - (180. + self.saa)
        if( self.aza < 0.0 ):
            self.aza = self.aza + 360.0
        if( self.aza > 360.0 ):
            self.aza = self.aza - 360.0
                
# Test
## Create view
#TempoGeom = ViewGeoObj()

#satlat = 25.5462
#satlon = -97.553
#lat    = 26.0471
#lon    = -85.3572
#earthradius = 6378.0
#satalt=705.1633750 
#jday=195.0   
#utc= 19.9783
#TempoGeom.compute_geom(lon,lat,satlon,satlat,satalt,earthradius,jday,utc)
#print TempoGeom.sza, TempoGeom.saa, TempoGeom.vza, TempoGeom.vaa

#satlat = 25.5462
#satlon = -97.553
#lat    = 25.5207
#lon    = -97.8493
#earthradius = 6378.0
#satalt=705.1633750
#jday=195.0   
#utc= 19.9783

#TempoGeom.compute_geom(lon,lat,satlon,satlat,satalt,earthradius,jday,utc)
#print TempoGeom.sza, TempoGeom.saa, TempoGeom.vza, TempoGeom.vaa