# -*- coding: iso-8859-1 -*-
import numpy as np
from splatvl_profile import SplatVL_profile 


# Create a p/T grid
lmx = 10
pedge = np.linspace( 1000.0,0.01,lmx+1 )
tedge = np.ones( lmx+1 )*270.0

# Make Ozone profile
o3 = np.ones( lmx )*60e-9

# Create a splat profile
prf = SplatVL_profile( pedge,tedge )

# Add O3
prf.add_3d_field( 'O3', o3 )

# Write
prf.write_to_nc('test_prf.nc')