# -*- coding: iso-8859-1 -*-
import numpy as np
import os
from netCDF4 import Dataset
from pylab import *
import scipy.integrate as intgr
from scipy.special import j1
from splatVL_scatter import splatVL_scatter, aerosol_lut

class gamma_pdf( object ):
    
    def __init__( self, a, b ):
        
        # Define Unnormalized PDF
        pdf = lambda r: np.power( r, (1.0-3.0*b)/b ) * np.exp( -1.0*r/(a*b) )
        self.par   = np.array( [a,b] )
        self.ndist = 4

        # Compute integral
        self.norm = intgr.quad( pdf, 1e-5, np.inf )
        self.pdf  = pdf
        
    def evaluate( self, r ):
        
        return self.pdf(r)/self.norm[0]

def reflected_component( theta_grid, m_r, m_i ):
    
    # get norm of RI squared
    m2 = np.power(m_r,2) + np.power(m_i,2)
    
    # I_R/2*sin(theta) (Determine Cr
    pwr = np.power ; sin=np.sin ; sqrt = np.sqrt
    IFn = lambda th:  0.25*sin(th)* ( pwr( ( sin(0.5*th) - sqrt(m2-1.0+pwr(sin(0.5*th),2)) )           \
                                          /( sin(0.5*th) + sqrt(m2-1.0+pwr(sin(0.5*th),2)) ), 2.0 )    \
                                     +pwr( ( m2*sin(0.5*th) - sqrt(m2-1.0+pwr(sin(0.5*th),2)) )        \
                                          /( m2*sin(0.5*th) + sqrt(m2-1.0+pwr(sin(0.5*th),2)) ), 2.0 ) )
    
    # Determine Cr
    Cr = 1.0 / intgr.romberg( IFn, 0.0, np.pi, show=True)
    
    # Define Phase function
    Ir = lambda th: 0.5*Cr*( pwr( ( sin(0.5*th) - sqrt(m2-1.0+pwr(sin(0.5*th),2)) )           \
                                 /( sin(0.5*th) + sqrt(m2-1.0+pwr(sin(0.5*th),2)) ), 2.0 )    \
                            +pwr( ( m2*sin(0.5*th) - sqrt(m2-1.0+pwr(sin(0.5*th),2)) )        \
                                 /( m2*sin(0.5*th) + sqrt(m2-1.0+pwr(sin(0.5*th),2)) ), 2.0 ) )
    
    Qr = 1.0 / Cr
    
    return Qr, Ir( theta_grid )

def diffraction_func( x_grid, theta, approx=False  ):
    
    # Compute z 
    if( theta > np.pi/2.0 ): # It seems that the diffraction > 90 degrees is constant
        z = x_grid
        k = 0.5
    else:
        z = x_grid*np.sin(theta)
        k = 0.5*( 1.0+np.power(np.cos(theta),2) )
    
    # Valid indices
    idv = z > 0
    idv0 = z == 0.0
    
    # Compute d
    d = np.zeros( len(z) )
    d_prefac = np.power(x_grid,2)/4.0/np.pi*k
    d[idv]   = d_prefac[idv]*np.power( 2.0*j1(z[idv])/z[idv],2 )
    d[idv0]  = d_prefac[idv0]  # Taking the limit x->0 of J1(x)/x = 0.5
    
    return d

def diffracted_component( theta_grid,  pdf, r_max, wvl,  m_r, m_i, x_crit=5.0, x_deg=100, th_deg=360 ):
    
    # Set up quadrature grid
    lx_grid, wt_x = np.polynomial.legendre.leggauss( x_deg )
    
    # Max Size parameter
    x_max = 2.0*np.pi*r_max/wvl
    
    # Compute the Gaussian quadrature grids
    b_x = 2.0/(x_max-x_crit)
    a_x = 1.0-b_x*x_max
    
    # Theta grid
    lth_grid, wt_th = np.polynomial.legendre.leggauss( th_deg )
    
    th_max = np.pi ; th_min = 0.0
    b_th = 2.0/th_max
    a_th = 1.0 - b_th*th_max
    
    ntheta = len(lth_grid)
    
    if( x_max > x_crit ):
        
        # Compute grids
        x_grid = (lx_grid-a_x)/b_x
        r_grid = x_grid*wvl/2.0/np.pi
        th_grid = (lth_grid-a_th)/b_th
        
        # Compute unnormalized phase function
        Id = np.zeros( ntheta )
        
        # PDF values at points ( for size pars - must norm)
        prob_x = pdf.evaluate( r_grid )*wvl/2.0/np.pi
        
        # Compute Id
        for n in range(ntheta):
            Id[n] = np.sum(  wt_x[:]/b_x*diffraction_func( x_grid, th_grid[n] )*np.pi*np.power(x_grid,2)*prob_x )
        
        # Compute norm factor
        Cd =  1.0 / np.sum( wt_th/b_th*Id*0.5*np.sin(th_grid) )
        
        # Compute Id on output grid
        ntheta_out = len( theta_grid )
        Id = np.zeros( ntheta_out )
        for n in range(ntheta_out):
            Id[n] = Cd*np.sum(  wt_x[:]/b_x*diffraction_func( x_grid, theta_grid[n] )*np.pi*np.power(x_grid,2)*prob_x )
        
    Qd = 1.0
    
    
    return Qd, Id
    
def transmitted_component_cg( theta_gr, G ):
    
    # Compute b
    Delta = np.power( (G + 1.0)*np.exp(1.0), 2 ) - 4.0*G*np.power(np.exp(1.0),2)
    b = 2.0/np.pi*np.log( ( (G + 1.0)*np.exp(1.0) - np.sqrt( Delta ) )/(2.0*G*np.exp(1)) )
    
    # Roots for exp(pi*b/2)
    #2.0/np.pi*np.log( ( (G + 1.0)*np.exp(1.0) - np.sqrt( Delta ) )/(2.0*G*np.exp(1)) ) ,\
    #2.0/np.pi*np.log( ( (G + 1.0)*np.exp(1.0) + np.sqrt( Delta ) )/(2.0*G*np.exp(1)) )
    
    # Define unnormed phase function integrated azimuthally 
    sin = np.sin ; exp = np.exp
    Ifn = lambda th: 0.5*sin(th)*exp(1.0+b*th)
    
    Ct  = 1.0 / intgr.romberg(Ifn,0.0,np.pi,show=True)
    
    It = lambda th: Ct*exp(1.0+b*th)
    
    # Now return the phase function
    return It( theta_gr )

def transmitted_component( theta_gr, G, theta_min ):
    
    # Updated parameterization with theta_min (following Tomasko)
    b = - 2.0*np.log( G )/np.pi
    c = - 0.5*b/theta_min
    
    # Define unnormed phase function integrated azimuthally 
    sin = np.sin ; exp = np.exp ; pwr = np.power
    Ifn = lambda th:0.5*sin(th)*exp( b*th + c*pwr(th,2) )
    
    Ct  = 1.0 / intgr.romberg(Ifn,0.0,np.pi,show=True)
    
    It = lambda th: Ct*exp(b*th+c*pwr(th,2))
    
    # Now return the phase function
    return It( theta_gr )

def compute_pollack_cuzzi( theta_gr, pdf, r_min, r_max, wvl, m_r, m_i, Rs, G, theta_min, x_crit=5.0, x_deg=100, th_deg=360):

    # Set up quadrature grid
    lx_grid, wt_x = np.polynomial.legendre.leggauss( x_deg )
    
    # Min/Max Size parameter
    x_min = 2.0*np.pi*r_min/wvl
    x_max = 2.0*np.pi*r_max/wvl
    
    # Initialize object to compute mie scatter
    sct = splatVL_scatter()
    
    # Critical radius
    r_crit = wvl/2.0/np.pi*x_crit
    
    if( x_max > x_crit ):
        
        # Compute the Gaussian quadrature grids
        b_x = 2.0/(x_max-x_crit)
        a_x = 1.0-b_x*x_max
        
        # Compute components for large size parameters
        Qd, Pd = diffracted_component( theta_gr, pdf, r_max, wvl, m_r, m_i,x_deg=x_deg, th_deg=th_deg)
        Qr, Pr = reflected_component( theta_gr, m_r, m_i )
        Pt     = transmitted_component( theta_gr, G, theta_min )
        
        # Compute quadrature grids
        x_grid = (lx_grid-a_x)/b_x
        r_grid = x_grid*wvl/2.0/np.pi
        
        # PDF values at points ( for size pars - must norm)
        prob_x = pdf.evaluate( r_grid )*wvl/2.0/np.pi
        
        # Fraction of large spheres
        Fl = np.sum( wt_x/b_x*prob_x*np.pi*np.power(x_grid,2) )
        
        # Compute Mie Extinction efficiencies weighteed between [r_crit,r_max]
        sct.mie_scatter(wvl,m_r,m_i,pdf.ndist,pdf.par,r_min=r_crit,r_max=r_max)
        Qsl = sct.qext*sct.ssa
        Qal = (sct.qext-Qsl)
        
        # Transmitted component scattering efficiency
        Qt = Qsl - Qd - Qr
        
    else:
        
        Fl  = 0.0
        Qd  = 0.0 ; Pd  = 0.0
        Qr  = 0.0 ; Pr  = 0.0
        Pt  = 0.0 ; Qt  = 0.0
        Qal = 0.0 ; Qsl = 0.0
        
    if( x_min < x_crit ):
      
        r_crit = wvl/2.0/np.pi*x_crit
        
        sct.mie_scatter(wvl,m_r,m_i,pdf.ndist,pdf.par,r_min=r_min,r_max=r_crit)      
        Qss = sct.qext*sct.ssa
        Qas = sct.qext-Qss
        P_mie = sct.evaluate_scalar_phase_function( theta_gr )

        # Compute the Gaussian quadrature grids
        b_xs = 2.0/(x_crit-x_min)
        a_xs = 1.0-b_xs*x_crit
        
        # Compute quadrature grids
        xs_grid = (lx_grid-a_xs)/b_xs
        rs_grid = xs_grid*wvl/2.0/np.pi
        
        # PDF values at points ( for size pars - must norm)
        prob_xs = pdf.evaluate( rs_grid )*wvl/2.0/np.pi
        
        # Compute Small fraction cross section mean
        Fs = np.sum( wt_x/b_xs*prob_xs*np.pi*np.power(xs_grid,2) )
        
    else:
        Fs    = 0.0
        P_mie = 0.0
        Qss   = 0.0 ; Qas   = 0.0
    
    # Large number fraction
    F = Fl/(Fl+Fs)
    
    # Compute extinction efficiencies for average particle
    Qs = Qsl*F*Rs + Qss*(1.0-F)
    Qa = Qal*F    + Qas*(1.0-F)
    Qe = Qs+Qa
    
    # Weight the cross section
    P = P_mie*(1.0-F)*Qss/Qs \
      + Rs*F*Qsl/Qs*( Pd*Qd/Qsl + Pt*Qt/Qsl + Pr*Qr/Qsl )
    
    return Qa,Qs,Qe,P,Qas,Qss,P_mie,Qsl,Qal,Qd,Qt,Qr,Pd,Pr,Pt,F

def gauss_quad( x_min,x_max, npt ):
    
    # Quadrature points on [-1,1]
    lx_grid, wt_x = np.polynomial.legendre.leggauss( npt )
    
    # Grid on [x_min,x_max]
    b_x = 2.0/(x_max-x_min)
    a_x = 1.0-b_x*x_max
    x_grid = (lx_grid-a_x)/b_x
    
    # Rescale weights
    wt_x = wt_x / b_x
    
    # Return Quadrature grid
    return x_grid, wt_x
    
    
    
class semiempirical_scatter( object ):
    
    def __init__(self,theta_gr, pdf, r_min, r_max, wvl, m_r, m_i, Rs, G, theta_min, x_crit=5.0, x_deg=100, th_deg=360,nmom=1001):
        
        # Convert to Numpy arrays if needed
        wvl       = np.array(       wvl )
        m_r       = np.array(       m_r )
        m_i       = np.array(       m_i )
        G         = np.array(         G )
        theta_min = np.array( theta_min )
        rh        = np.array(     [0.0] )
        if( len(      wvl.shape) == 0 ):       wvl =       wvl.reshape( 1 )
        if( len(      m_r.shape) == 0 ):       m_r =       m_r.reshape( 1 )
        if( len(      m_i.shape) == 0 ):       m_i =       m_i.reshape( 1 )
        if( len(        G.shape) == 0 ):         G =         G.reshape( 1 )
        if( len(theta_min.shape) == 0 ): theta_min = theta_min.reshape( 1 )
        
        # Number of wavelengths to simulate
        nwvl   = len(     wvl)
        ntheta = len(theta_gr)
        nrh    = len(      rh)
        
        # Initialize Aerosol LUT Object
        self.lut = aerosol_lut()
        self.lut.allocate_arrays( wvl*1e3,rh,nmom ) # LUT wavelength in nm
        
        # Phase function
        self.P = np.zeros((ntheta,nrh,nwvl))
        
        # Arrays for Small Regime
        self.Qas = np.zeros(nwvl)
        self.Qss = np.zeros(nwvl)
        self.Ps  = np.zeros((ntheta,nwvl))
        
        # Arrays for Large regime
        self.Qsl = np.zeros(nwvl) # Mie Scattering efficiency x>x_crit
        self.Qal = np.zeros(nwvl) # Mie Absorption efficiency x>x_crit
        self.Qd  = np.zeros(nwvl) # Diffraction Scattering efficiency
        self.Qt  = np.zeros(nwvl) # Transmission Scattering efficiency
        self.Qr  = np.zeros(nwvl) # Fresnel Reflection Scattering efficiency
        self.Pd  = np.zeros((ntheta,nwvl)) # Diffraction Scattering Phase Function
        self.Pt  = np.zeros((ntheta,nwvl)) # Transmission Scattering Phase Function
        self.Pr  = np.zeros((ntheta,nwvl)) # Fresnel Reflection Scattering Phase Function
        self.F   = 0.0
        
        # Compute Effective Radius
        # ------------------------
        
        # Gauss quadrature grid
        r_grid,r_wt = gauss_quad(r_min,r_max,x_deg)
        
        # Compute pdf at quadrature points
        pdf_r = pdf.evaluate( r_grid )
        
        # Do the quadrature 
        self.lut.reff[:] = np.sum( r_wt*pdf_r*np.power(r_grid,3) ) \
                         / np.sum( r_wt*pdf_r*np.power(r_grid,2) )
        
        for w in range( nwvl ):
            
            # Compute values for wavelength
            Qa,Qs,Qe,self.P[:,0,w],self.Qas[w],self.Qss[w],self.Ps[:,w],\
            self.Qsl[w],self.Qal[w],self.Qd[w],self.Qt[w],self.Qr[w],   \
            self.Pd[:,w],self.Pr[:,w],self.Pt[:,w],F =                  \
                compute_pollack_cuzzi(theta_gr, pdf, r_min, r_max, wvl[w], m_r[w], m_i[w], Rs, G[w],\
                                      theta_min[w], x_crit=x_crit, x_deg=x_deg, th_deg=th_deg       )
            
            # Update LUT values
            self.lut.qext[0,w] = Qe
            self.lut.ssa[0,w]  = Qs/Qe
        
        self.lut.add_scalar_pfunc( theta_gr, self.P )
        
        self.lut.check_scalar_pfunc_exp()
        
        
        
# Test 
###########################################################################

# Plot libraries
from matplotlib import pyplot as plt
import matplotlib.pylab as pl
import matplotlib.colors as colors
import matplotlib.font_manager as font_manager
import matplotlib as mpl


path = '/home/cmiller/osx_ttf_fonts/HelveticaNeue.ttf'
prop = font_manager.FontProperties(fname=path)
mpl.rcParams['font.family'] = prop.get_name()
mpl.rcParams.update({'font.size':12})


'''
theta_gr = np.arange(0.0,np.pi,0.01/180.0*np.pi)
m_r = 1.5
m_i = 0.0023
a = 1600.0
b = 0.2
npt = 2001
wvl = 670.8
G = 70.0
pdf = gamma_pdf(a,b)
r_min = 10.0
r_max = 5e3
Rs    = 1.3 # Ratio of surface area of irreg. particle to equiv. volume sphere
theta_min = np.deg2rad(145.0)

Qa,Qs,Qe,P,P_mie,Pt,Pr,Pd,Qas,Qss,Qsl,Qal = semiempirical_scatter( theta_gr, pdf, r_min, r_max, wvl, m_r, m_i, Rs, theta_min )
'''

##############################
# Try replicating figure 19
##############################

theta_gr = np.arange(0.0,np.pi,0.01/180.0*np.pi)
nwvl = 4
m_r = np.ones(nwvl)*1.5
m_i = np.array([0.0122,0.0023,0.0036,0.0042])
ssa = np.array([0.755,0.949,0.940,0.937])
a = 1.6
b = 0.2
wvl = np.array([443.6,670.8,896.1,965.3])*1e-3
G = np.array([115.0,70.0,120.0,75.0])
pdf = gamma_pdf(a,b)
r_min = 10.0*1e-3
r_max = 10.0
Rs    = 1.3 # Ratio of surface area of irreg. particle to equiv. volume sphere
theta_min = np.deg2rad([138.0,145.0,150.0,164.0])

dust = semiempirical_scatter(theta_gr, pdf, r_min, r_max, wvl, m_r, m_i, Rs, G, theta_min, x_crit=5.0, x_deg=100, th_deg=360,nmom=1000)


dust.lut.write_lut( 'mars_dust_tomasko_1999_jgr.nc' )

theta_deg = np.rad2deg(theta_gr)
fig = plt.figure()
plt.semilogy(theta_deg,dust.Ps[:,0].squeeze(),color='r',label='Mie')
plt.semilogy(theta_deg,dust.Pd[:,0].squeeze(),color='g',label='Diffraction')
plt.semilogy(theta_deg,dust.Pr[:,0].squeeze(),color='m',label='Reflection')
plt.semilogy(theta_deg,dust.Pt[:,0].squeeze(),color='c',label='Transmission')
plt.semilogy(theta_deg,dust.P[:,0,0].squeeze(),color='k',label='Total')
plt.ylim((1e-2,1e3))
plt.xlabel('Scattering Angle [degrees]')
plt.ylabel('Single Scattering Phase Function')
plt.legend()
fig.tight_layout()
fig.show()

