# -*- coding: iso-8859-1 -*-
import numpy as np


def scalar_legendre( theta_in, P_in, nmom=1000, compare_fit=False ):
    
    # cos(theta)
    x = np.cos( theta_in )
    
    # Do fit
    f = np.polynomial.legendre.Legendre.fit( x, P_in,deg=nmom )
    