# -*- coding: iso-8859-1 -*-
import numpy as np
from netCDF4 import Dataset
import os
from matplotlib import pyplot as plt
from matplotlib import gridspec
class splatVL_scatter( object ):
    
    def __init__( self, exe_path='../../../MASTER_MODULE/spv_aerosol.exe' ):
        
        self.exe_path = exe_path
    
    def dist_cdf( r_grid, dist_index, dist_par ):
        
        cdf = np.zeros( len(r_grid) )
        
        if( dist_index == 1 ):
            cdf[:] = 0.0 
        elif( dist_index == 7 ):
            cdf[ r_grid > dist_par[0] ] = 1.0
        else:
            print 'dist_index: ',dist_index,' is not defined'
        
        return cdf
    
    def mie_scatter( self, wvl, ri_r, ri_i, dist_index, dist_par, rh=[0.0], r_min=None,r_max=None, tmp_inpfile='spv_aer_inp.nc',tmp_outfile='spv_aer_out.nc' ):
        
        # Make sure things are np arrays
        wvl      = np.array( wvl  )
        ri_r     = np.array( ri_r )
        ri_i     = np.array( ri_i )
        rh       = np.array( rh   )
        dist_par = np.array( dist_par )
        
        # Get dimensions
        if( len(wvl.shape) == 0 ):
            nwvl = 1
        else:
            nwvl = len(wvl)
        if( len(rh.shape) == 0 ):
            nrh = 1
        else:
            nrh = len(rh)
        if( len(dist_par.shape) == 0 ):
            npar = 1
        else:
            npar = len(dist_par)
        
        # Get Limits
        if( r_min is None ):
            
            r_min = np.zeros(nrh)
        else:
            r_min = np.array( r_min )
            r_min = r_min.reshape((nrh))
    
        if( r_max is None ):
            r_max = np.zeros(nrh)
        else:
            r_max = np.array(r_max)
            r_max = r_max.reshape((nrh))

        # Reshape to appropriate dimensions
        wvl  = wvl.reshape( nwvl )
        ri_r = ri_r.reshape( (nrh,nwvl) )
        ri_i = ri_i.reshape( (nrh,nwvl) )
        rh   = rh.reshape( nrh )
        dist_par = dist_par.reshape( (nrh,npar) )
        
        # ----------------------------------
        # Write the input file
        # ----------------------------------
        
        # Open file
        ncid = Dataset( tmp_inpfile, 'w' )
        
        # Define Dimension
        ncid.createDimension( 'w', nwvl )
        ncid.createDimension( 'r', nrh  )
        ncid.createDimension( 'p', npar )
        ncid.createDimension( 'o', 1 )
        ncid.createDimension( 't', 2 )
        
        # Create variables
        vid = ncid.createVariable('Wavelength',np.float64,('w'))
        ncid.variables['Wavelength'][:] = wvl[:]
        
        vid = ncid.createVariable('DistributionIndex',np.int16,('o'))
        ncid.variables['DistributionIndex'][:] = dist_index
        
        vid = ncid.createVariable('ParticleRadiusLimits',np.float64,('r','t'))
        ncid.variables['ParticleRadiusLimits'][:,0] = r_min[:]
        ncid.variables['ParticleRadiusLimits'][:,1] = r_max[:]
        
        vid = ncid.createVariable('RelativeHumidity',np.float64,('r'))
        ncid.variables['RelativeHumidity'][:] = rh[:]
        
        vid = ncid.createVariable('RealRefractiveIndex',np.float64,('r','w'))
        ncid.variables['RealRefractiveIndex'][:,:] = ri_r[:,:]
     
        vid = ncid.createVariable('ImaginaryRefractiveIndex',np.float64,('r','w'))
        ncid.variables['ImaginaryRefractiveIndex'][:,:] = ri_i[:,:]

        vid = ncid.createVariable('NumberDistributionParameters',np.float64,('r','p'))
        ncid.variables['NumberDistributionParameters'][:,:] = dist_par[:,:]

        # Close input file
        ncid.close()
        
        # ---------------------------------
        # Run Calculation
        # ---------------------------------
        
        # Run executable
        cmd = self.exe_path + ' ' + tmp_inpfile + ' ' + tmp_outfile
        os.system( cmd )
        
        # Cleanup Input file
        cmd = 'rm -f ' + tmp_inpfile

        # ---------------------------------
        # Load Output
        # ---------------------------------
        
        # Read Output file
        ncid  = Dataset(tmp_outfile,'r') 
        self.phfcn = ncid.variables['phfcn0'][:,:,:].squeeze()
        self.qext  = ncid.variables['qext0'][:,:].squeeze()
        self.reff  = ncid.variables['reff'][:].squeeze()
        self.ssa   = ncid.variables['ssa0'][:,:].squeeze()
        self.rh    = ncid.variables['rh'][:].squeeze()
        self.wvl   = ncid.variables['wls0'][:].squeeze()
        ncid.close()

        # Remove Output
        cmd = 'rm -f ' + tmp_outfile 
        
    def evaluate_scalar_phase_function( self, theta_grid ):
       
        # Theta quadrature grid
        th_deg = 720
        lth_grid, wt_th = np.polynomial.legendre.leggauss( th_deg )
        th_max = np.pi ; th_min = 0.0
        b_th = 2.0/th_max
        a_th = 1.0 - b_th*th_max
        th_grid = (lth_grid-a_th)/b_th 

        
        ntheta = len(theta_grid)
        
        if( len(self.phfcn.shape) ==  3):
            ncoeff = self.phfcn.shape[1]
            wmx    = self.phfcn.shape[2]
            coef = np.reshape( self.phfcn[0,:,:].squeeze(), (ncoeff,wmx) )
        else:
            ncoeff = self.phfcn.shape[1]
            wmx    = 1
            coef = np.reshape( self.phfcn[0,:].squeeze(), (ncoeff,wmx) )
        
        
        # Declare output
        P = np.zeros( (ntheta,wmx) )
        
        # Cos theta
        c_theta = np.cos( theta_grid )
        
        for w in range(wmx):
            
            f = np.polynomial.legendre.Legendre( coef[:,w].squeeze() )
                    
            P[:,w] = f( c_theta )
        
        return P.squeeze()
        
class aerosol_lut( object ):
    
    def __init__( self, filename=None ):
        
        if( not filename is None ):
            
            # Load the properties
            ncid  = Dataset(filename,'r') 
            self.phfcn = ncid.variables['phfcn0'][:,:,:]
            self.qext  = ncid.variables['qext0'][:,:]
            self.reff  = ncid.variables['reff'][:]
            self.ssa   = ncid.variables['ssa0'][:,:]
            self.rh    = ncid.variables['rh'][:]
            self.wvl   = ncid.variables['wls0'][:]
            self.nwvl  = len(self.wvl)
            self.nrh   = len(self.nrh)
            self.ngk   = self.phfcn.shape[1]
            self.nmom  = self.phfcn.shape[2]
            ncid.close()
        
        else:
            self.phfcn = None
        
        self.theta = None
        
    def allocate_arrays( self, wvl, rh, nmom ):
        
        # Get dimensions
        nrh  = len( rh)
        nwvl = len(wvl)
        ngk  = 6
        
        # Output grid
        self.wvl   = wvl
        self.nwvl  = nwvl
        self.rh    = rh
        self.nrh   = nrh
        self.ngk   = ngk
        self.nmom  = nmom
        
        # Allocate arrays
        self.phfcn = np.zeros((nrh,ngk,nmom,nwvl))
        self.qext  = np.zeros((nrh,nwvl))
        self.reff  = np.zeros((nrh))
        self.ssa   = np.zeros((nrh,nwvl))
        self.rh    = np.zeros((nrh))
        
    def scalar_pfunc_expansion( self, theta ):
        
        # Archive grid
        self.theta = theta
        ntheta = len(theta)
        
        # Compute expansion
        x = np.cos( theta )
        
        # Allocate phase function array
        self.Pexp = np.zeros( (ntheta,self.nrh,self.nwvl) )
        
        for r in range( self.nrh ):
            for w in range( self.nwvl ):
                
                # Legendre expansion
                f = np.polynomial.legendre.Legendre( self.phfcn[r,0,:,w].squeeze() )
                
                self.Pexp[:,r,w] = f( x )
        
    def add_scalar_pfunc( self, theta, P ):
        
        # Archive original
        self.P     = P
        self.theta = theta
        
        # Array to hold expansion
        self.Pexp = np.zeros( P.shape )
        
        # Legendre grid
        x = np.cos( theta )
        
        for r in range( self.nrh ):
            for w in range( self.nwvl ):
                
                # Compute the fit
                f = np.polynomial.legendre.Legendre.fit( x, P[:,r,w].squeeze(),deg=self.nmom-1 )
                
                # Compute the scaar phase function for validation
                self.Pexp[:,r,w] = f(x)
                
                # Archive expansion
                #self.phfcn[r,0,:,w] = f.coef[:] 
                self.phfcn[r,0,:,w] = f.coef[:] / f.coef[0] # Force normalization
        
    def check_scalar_pfunc_exp( self,r=0,w=0 ):
        
        fig = plt.figure()
        gs = gridspec.GridSpec(3, 3)
        ax1 = fig.add_subplot(gs[0,:])
        plt.ylabel('Residual')
        ax1.plot(np.rad2deg(self.theta),self.Pexp[:,r,w]-self.P[:,r,w],'b')
        ax2 = fig.add_subplot(gs[1:,:])
        ax2.plot(np.rad2deg(self.theta),self.P[:,r,w], 'b',label='Truth')
        ax2.plot(np.rad2deg(self.theta),self.Pexp[:,r,w], 'r',label='Expansion')
        plt.xlabel('Scattering Angle [deg]')
        plt.ylabel('Phase Function')
        plt.legend()
        gs.update(wspace=0.5, hspace=0.5)
        fig.show()
    
    def write_lut( self, outfile ):
        
        
        # Open file for write
        ncid = Dataset( outfile, 'w' )
        
        # Create dimensions
        ncid.createDimension( 'g'  , self.ngk  )
        ncid.createDimension( 'm'  , self.nmom )
        ncid.createDimension( 'r'  , self.nrh  )
        ncid.createDimension( 'w'  , self.nwvl )
        ncid.createDimension( 'one', 1         )
        
        # Write Data
        ncid.createVariable('nmom',np.int32,('one'))
        ncid.variables['nmom'][0] = self.nmom
        ncid.createVariable('nrh',np.int32,('one'))
        ncid.variables['nrh'][0] = self.nrh
        ncid.createVariable('nwls',np.int32,('one'))
        ncid.variables['nwls'][0] = self.nwvl
        ncid.createVariable('phfcn0',np.float64,('r','g','m','w'))
        ncid.variables['phfcn0'][:,:,:,:] = self.phfcn
        ncid.createVariable('qext0',np.float64,('r','w'))
        ncid.variables['qext0'][:,:] = self.qext
        ncid.createVariable('reff',np.float64,('r'))
        ncid.variables['reff'][:] = self.reff
        ncid.createVariable('ssa0',np.float64,('r','w'))
        ncid.variables['ssa0'][:,:] = self.ssa
        ncid.createVariable('wls0',np.float64,('w'))
        ncid.variables['wls0'][:] = self.wvl
        ncid.createVariable('rh',np.float64,('r'))
        ncid.variables['rh'][:] = self.rh
        
        # Close file
        ncid.close()
        

