MODULE GC_Vlidort_module
  
  USE VBRDF_LINSUP_MASTERS_M
  USE VLIDORT_PARS,        ONLY: GISSCOXMUNK_CRI_IDX, VLIDORT_SERIOUS
  USE GC_parameters_module,ONLY: max_ch_len
  USE GC_variables_module, ONLY: VLIDORT_FixIn, ndir, idix, didx, GC_do_user_altitudes,        &
                                 GC_n_user_levels, GC_user_altitudes, GC_user_levels, heights, &
                                 GC_nlayers, VLIDORT_ModIn, nstreams_choice, VLIDORT_LinModIn, &
                                 VLIDORT_LinFixIn, do_vector_calculation, ngksec, nactgkmatc,  &
                                 GC_n_sun_positions, GC_sun_positions, GC_n_view_angles,       &
                                 GC_view_angles, GC_n_azimuths, GC_azimuths, gaswfidx, twfidx, &
                                 aodwfidx, assawfidx, sfcprswfidx, codwfidx, cssawfidx,        &
                                 do_Jacobians, do_T_Jacobians, do_sfcprs_Jacobians,            &
                                 do_aod_Jacobians, do_assa_Jacobians, do_cod_Jacobians,        &
                                 do_cssa_Jacobians,do_lambertian_cld, N_TOTALPROFILE_WFS_wcld, &
                                 do_clouds, N_TOTALPROFILE_WFS_ncld, do_aerosols,              &
                                 use_lambertian, wind_speed,                                   &
                                 VBRDF_Sup_In, VBRDF_LinSup_In, VLIDORT_Out, GC_Radiances,     &
                                 do_StokesQU_output, GC_Qvalues, GC_Uvalues, ngases,           &
                                 gas_partialcolumns, gasabs, GC_Tracegas_Jacobians,            &
                                 do_normalized_WFoutput, ratio, total_gasabs, VLIDORT_LinOut,  &
                                 do_AMF_calculation, GC_Scattering_Weights, w, GC_AMFs,        &
                                 GC_Tracegas_QJacobians, GC_Tracegas_UJacobians,               &
                                 GC_Temperature_Jacobians, mid_temperatures, do_aer_columnwf,  &
                                 total_wf, aer_flags, GC_aod_Jacobians, taertau0, taer_profile,&
                                 do_QU_Jacobians, total_Qwf, total_Uwf, GC_aod_QJacobians,     &
                                 GC_aod_UJacobians, total_aersca, total_aertau, aer_opdeps,    &
                                 aer_ssalbs, total_vaerssa, GC_assa_Jacobians,                 &
                                 GC_assa_QJacobians, GC_assa_UJacobians, do_cld_columnwf,      &
                                 cld_flags, GC_cod_Jacobians, GC_cod_QJacobians,               &
                                 GC_cod_UJacobians, tcldtau0, tcld_profile, total_cldsca,      &
                                 total_cldtau, cld_opdeps, cld_ssalbs, total_cldtau,           &
                                 GC_cssa_Jacobians, GC_cssa_QJacobians, GC_cssa_UJacobians,    &
                                 total_vcldssa, ground_ler,                                    &
                                 GC_WindSpeed_Jacobians, GC_WindSpeed_QJacobians,              &
                                 GC_WindSpeed_UJacobians, deltp, pressures,                    &
                                 GC_sfcprs_Jacobians, GC_sfcprs_QJacobians,                    &
                                 GC_sfcprs_UJacobians, do_normalized_radiance, do_effcrs,      &
                                 solar_cspec_Data, depol, rayleigh_depols, beta_2, pray_22,    &
                                 pray_12, pray_41, pray_52, phasmoms_input,                    &
                                 database_dir, maxaer, naer, aer_types, solar_spec_data,       &
                                 aer_profile, lambdas, aer_reflambda, aer_relqext, aer_phfcn,  &
                                 maxcld, ncld, cld_types, cld_profile, cld_reflambda,          &
                                 cld_relqext, cld_phfcn, water_rn, water_cn, stokes_clrcld,    &
                                 stokes_flux, stokes_direct_flux,                              &
                                 profilewf_sum, surfacewf_clrcld, ipafrac,                     &
                                 cfrac, cld_uppers, lambertian_cldalb, temp, temp_sq,          &
                                 total_molabs, which_gases, gas_xsecs_type, xsec, gas_xsecs,   &
                                 o3c1_xsecs, o3c2_xsecs, total_molsca, aircolumns,             &
                                 rayleigh_xsecs, total_moltau, nscatter, scaco_input,          &
                                 total_tau, total_sca, aerscaidx, cldscaidx,                   &
                                 omega, opdeps, ssalbs, smallnum, phasmoms_total_input,        &
                                 greekmat_idxs, phasmoms_idxs, abs_o3, dxsec_dt, l_gas, l_air, &
                                 daircolumns_dt, l_phasmoms_total_input, pvar,                 &
                                 do_debug_geocape_tool, du, vlidort_sup, vlidort_linsup,       &
                                 openerrorfileflag, do_cfrac_jacobians, GC_cfrac_Jacobians,    &
                                 GC_cfrac_QJacobians, GC_cfrac_UJacobians, GC_flux,            &
                                 GC_direct_flux, GC_Qflux, GC_Uflux, GC_Qdirect_flux,          &
                                 GC_Udirect_flux, VBRDF_Sup_Out, VBRDF_LinSup_Out,             &
                                 VLIDORT_Sup, VLIDORT_LinSup, VBRDF_Sup_In, Total_brdf, GCM,   &
                                 NSTOKESSQ, do_brdf_surface, OUTPUT_WSABSA, WSA_CALCULATED,    &
                                 BSA_CALCULATED, use_footprint_info, do_sat_viewcalc,          &
                                 GC_n_user_altitudes, ilev, rh_profile, n_cpix, cpix_cfrac,    &
                                 use_cldprof, cld_uppers_npix, cld_prof_npix,                  &
                                 BRDF_Sup_ModOut,maxgksec, do_raman_jacobian,                  &
                                 rrs_depol, rrsbeta_2, pRam_12, pRam_22, pRam_52, pRam_41,     &
                                 phasmoms_rrs, o2_depols, n2_depols,o2_rrs_xsecs, n2_rrs_xsecs,&
                                 total_o2rrs,total_n2rrs, rrswfidx,                            &
                                 GC_rrs_Jacobian, do_rrs_abs, do_rrs_phasefunc, epsilon_air,   &
                                 cpix_lam_zlvl, cpix_lam_lvlfrac,rrs_gas, ngas_rrs, rrs_xsecs, &
                                 prof_obsalt, do_yn_obsalt, do_stokes_ad20, do_stokes_ad21,    &
                                 do_stokes_ad22, do_oco2_sfcprs, do_total_aod_jac,             &
                                 Diag34_InpOpt, Diag35_InpOpt,aerdiag_phasemoms, aerdiag_tau,  &
                                 aerdiag_ssa, aerdiag_qext,do_total_assa_jac,                  &
                                 write_gas_xsecs,write_xsec_deriv,gas_xsecs_deriv,total_dgasabsdT,&
                                 VBRDF_Sup_OutputStatus, do_sif, do_ocean_glint, do_eof_brdf, &
                                 BS_NMOMENTS_INPUT, DO_DEBUG_RESTORATION,  do_aerhw_Jacobians, &
                                 do_aerph_Jacobians, do_ctp_Jacobians, use_brdf_clim, use_cmglint 
  
  USE GC_error_module

  IMPLICIT NONE

CONTAINS
  
  
  SUBROUTINE initialize_vlidort_inputs( error )
    
    USE GC_variables_module, ONLY : SVL_full_stokes_calc,SVL_n_stokes,SVL_do_debug,     &
                                    SVL_do_thermal_emiss,SVL_do_surf_emiss,SVL_nstreams,&
                                    SVL_nmoments,SVL_do_upwelling,SVL_do_downwelling,   &
                                    VLIDORT_FixIn,VLIDORT_ModIn,VLIDORT_LinFixIn,       &
                                    VLIDORT_LinModIn,VLIDORT_InputStatus, do_diag
    USE VLIDORT_PARS
    USE VLIDORT_INPUTS,      ONLY : VLIDORT_INIT_CONTROL_VARS, &
                                    VLIDORT_INIT_THERMAL_VARS, &
                                    VLIDORT_INIT_MODEL_VARS
                                    
    
    LOGICAL, INTENT(INOUT) :: error

    ! ----------------
    !  Local variables
    ! ----------------
      LOGICAL ::            DO_FULLRAD_MODE
      LOGICAL ::            DO_SSCORR_NADIR
      LOGICAL ::            DO_SSCORR_OUTGOING

!  New 02 Jul 2013
      LOGICAL ::            DO_FO_CALC

      LOGICAL ::            DO_SSCORR_TRUNCATION

!  New 15 March 2012
      LOGICAL ::            DO_SS_EXTERNAL

!  New 17 May 2012
      LOGICAL ::            DO_SURFACE_LEAVING
      LOGICAL ::            DO_SL_ISOTROPIC

      LOGICAL ::            DO_SSFULL
      LOGICAL ::            DO_DOUBLE_CONVTEST
      LOGICAL ::            DO_SOLAR_SOURCES
      LOGICAL ::            DO_PLANE_PARALLEL
      LOGICAL ::            DO_REFRACTIVE_GEOMETRY
      LOGICAL ::            DO_CHAPMAN_FUNCTION
      LOGICAL ::            DO_RAYLEIGH_ONLY
      LOGICAL ::            DO_DELTAM_SCALING
      LOGICAL ::            DO_SOLUTION_SAVING
      LOGICAL ::            DO_BVP_TELESCOPING
      LOGICAL ::            DO_UPWELLING
      LOGICAL ::            DO_DNWELLING
      LOGICAL ::            DO_QUAD_OUTPUT
      LOGICAL ::            DO_USER_VZANGLES
      LOGICAL ::            DO_ADDITIONAL_MVOUT
      LOGICAL ::            DO_MVOUT_ONLY
      LOGICAL ::            DO_DEBUG_WRITE
      LOGICAL ::            DO_WRITE_INPUT
      LOGICAL ::            DO_WRITE_SCENARIO
      LOGICAL ::            DO_WRITE_FOURIER
      LOGICAL ::            DO_WRITE_RESULTS
      CHARACTER (LEN=60) :: INPUT_WRITE_FILENAME
      CHARACTER (LEN=60) :: SCENARIO_WRITE_FILENAME
      CHARACTER (LEN=60) :: FOURIER_WRITE_FILENAME
      CHARACTER (LEN=60) :: RESULTS_WRITE_FILENAME

!  Order of Taylor series (including terms up to EPS^n).
!    Introduced 2/19/14 for Version 2.7
      
      INTEGER :: TAYLOR_ORDER

      INTEGER ::            NSTOKES
      INTEGER ::            NSTREAMS
      INTEGER ::            NLAYERS
      INTEGER ::            NFINELAYERS
      INTEGER ::            NGREEK_MOMENTS_INPUT
      DOUBLE PRECISION ::   VLIDORT_ACCURACY
      DOUBLE PRECISION ::   FLUX_FACTOR
      INTEGER ::            N_SZANGLES
      DOUBLE PRECISION ::   SZANGLES ( MAX_SZANGLES )
      DOUBLE PRECISION ::   EARTH_RADIUS
      DOUBLE PRECISION ::   RFINDEX_PARAMETER
      DOUBLE PRECISION ::   GEOMETRY_SPECHEIGHT
      INTEGER ::            N_USER_RELAZMS
      DOUBLE PRECISION ::   USER_RELAZMS  ( MAX_USER_RELAZMS )
      INTEGER ::            N_USER_VZANGLES
      DOUBLE PRECISION ::   USER_VZANGLES ( MAX_USER_VZANGLES )
      INTEGER ::            N_USER_LEVELS
      DOUBLE PRECISION ::   USER_LEVELS ( MAX_USER_LEVELS )
      LOGICAL ::            DO_LAMBERTIAN_SURFACE
      DOUBLE PRECISION ::   LAMBERTIAN_ALBEDO
      LOGICAL ::            DO_OBSERVATION_GEOMETRY
      INTEGER ::            N_USER_OBSGEOMS
      DOUBLE PRECISION ::   USER_OBSGEOMS ( MAX_USER_OBSGEOMS, 3 )

      LOGICAL ::            DO_THERMAL_EMISSION
      INTEGER ::            N_THERMAL_COEFFS
      DOUBLE PRECISION ::   THERMAL_BB_INPUT ( 0:MAXLAYERS )
      LOGICAL ::            DO_SURFACE_EMISSION
      DOUBLE PRECISION ::   SURFBB
      LOGICAL ::            DO_THERMAL_TRANSONLY

      LOGICAL ::            DO_SPECIALIST_OPTION_1
      LOGICAL ::            DO_SPECIALIST_OPTION_2
      LOGICAL ::            DO_SPECIALIST_OPTION_3
      LOGICAL ::            DO_TOA_CONTRIBS

      LOGICAL ::            DO_SIMULATION_ONLY
      LOGICAL ::            DO_LINEARIZATION
      LOGICAL ::            DO_PROFILE_LINEARIZATION
      LOGICAL ::            DO_COLUMN_LINEARIZATION
      LOGICAL ::            DO_ATMOS_LINEARIZATION
!      LOGICAL ::            DO_LTE_LINEARIZATION  ! 2p6 variable replaced
      LOGICAL ::            DO_ATMOS_LBBF          ! 2p7 variable new

      INTEGER ::            N_TOTALPROFILE_WFS
      INTEGER ::            N_TOTALCOLUMN_WFS
      LOGICAL ::            LAYER_VARY_FLAG  ( MAXLAYERS )
      INTEGER ::            LAYER_VARY_NUMBER ( MAXLAYERS )
      LOGICAL ::            DO_SURFACE_LINEARIZATION
!      LOGICAL ::            DO_SURFBB_LINEARIZATION ! 2p6 variable replaced
      LOGICAL ::            DO_SURFACE_LBBF          ! 2p7 variable new
      LOGICAL ::            DO_SLEAVE_WFS
      INTEGER ::            N_SURFACE_WFS
      INTEGER ::            N_SLEAVE_WFS
      CHARACTER (LEN=31) :: PROFILEWF_NAMES ( MAX_ATMOSWFS )
      CHARACTER (LEN=31) :: COLUMNWF_NAMES  ( MAX_ATMOSWFS )

      INTEGER ::             STATUS
      INTEGER ::             NMESSAGES
      CHARACTER (LEN=120) :: MESSAGES (0:MAX_MESSAGES)
      CHARACTER (LEN=120) :: ACTIONS (0:MAX_MESSAGES)

      INTEGER ::             STATUS_SUB, FILUNIT, N
      
      ! ============================================================================
      ! initialize_vlidort_inputs starts here
      ! ============================================================================
      
      
      STATUS = VLIDORT_SUCCESS

      MESSAGES(1:MAX_MESSAGES) = ' '
      ACTIONS (1:MAX_MESSAGES) = ' '
      
      NMESSAGES       = 0
      MESSAGES(0)     = 'Successful Read of VLIDORT Input file'
      ACTIONS(0)      = 'No Action required for this Task'
      
      
      ! Initialize standard variables
      CALL VLIDORT_INIT_CONTROL_VARS (                  &
        DO_FULLRAD_MODE, DO_FO_CALC, DO_SSCORR_NADIR,   &
        DO_SSCORR_OUTGOING, DO_SSCORR_TRUNCATION,       &
        DO_SS_EXTERNAL, DO_SSFULL, DO_DOUBLE_CONVTEST,  &
        DO_PLANE_PARALLEL, DO_REFRACTIVE_GEOMETRY,      &
        DO_CHAPMAN_FUNCTION, DO_RAYLEIGH_ONLY,          &
        DO_DELTAM_SCALING, DO_SOLUTION_SAVING,          &
        DO_BVP_TELESCOPING, DO_UPWELLING,               &
        DO_DNWELLING, DO_QUAD_OUTPUT,                   &
        DO_USER_VZANGLES, DO_ADDITIONAL_MVOUT,          &
        DO_MVOUT_ONLY, DO_DEBUG_WRITE,                  &
        DO_WRITE_INPUT, DO_WRITE_SCENARIO,              &
        DO_WRITE_FOURIER, DO_WRITE_RESULTS,             &
        DO_LAMBERTIAN_SURFACE, DO_OBSERVATION_GEOMETRY, &
        DO_SPECIALIST_OPTION_1, DO_SPECIALIST_OPTION_2, &
        DO_SPECIALIST_OPTION_3, DO_TOA_CONTRIBS,        &
        DO_SURFACE_LEAVING, DO_SL_ISOTROPIC             )
      
      CALL VLIDORT_INIT_THERMAL_VARS (         &
        DO_THERMAL_EMISSION, N_THERMAL_COEFFS, &
        THERMAL_BB_INPUT, DO_SURFACE_EMISSION, &
        SURFBB, DO_THERMAL_TRANSONLY           )

      CALL VLIDORT_INIT_MODEL_VARS ( &
        TAYLOR_ORDER, NSTOKES, NSTREAMS, &
        NLAYERS, NFINELAYERS, &
        NGREEK_MOMENTS_INPUT, VLIDORT_ACCURACY, &
        FLUX_FACTOR, N_SZANGLES, &
        SZANGLES, EARTH_RADIUS, &
        RFINDEX_PARAMETER, GEOMETRY_SPECHEIGHT, &
        N_USER_RELAZMS, USER_RELAZMS, &
        N_USER_VZANGLES, USER_VZANGLES, &
        N_USER_LEVELS, USER_LEVELS, &
        N_USER_OBSGEOMS, USER_OBSGEOMS, &
        LAMBERTIAN_ALBEDO )
      
      ! The following are the initializations for linearization variables set in VLIDORTv2p7
      ! ------------------------------------------------------------------------------------
      
      !  initialize linearization variables
      DO_SIMULATION_ONLY        = .FALSE.
      DO_LINEARIZATION          = .FALSE.
      DO_PROFILE_LINEARIZATION  = .FALSE.
      DO_COLUMN_LINEARIZATION   = .FALSE.
      DO_ATMOS_LINEARIZATION    = .FALSE.
      DO_ATMOS_LBBF             = .FALSE.
      N_TOTALCOLUMN_WFS  = 0
      N_TOTALPROFILE_WFS = 0
      N_SURFACE_WFS      = 0
      N_SLEAVE_WFS       = 0
      DO N = 1, MAXLAYERS
        LAYER_VARY_FLAG(N)   = .FALSE.
        LAYER_VARY_NUMBER(N) = 0
      ENDDO

      !  initialise surface weighting function control inputs
      DO_SURFACE_LINEARIZATION = .FALSE.
      DO_SURFACE_LBBF          = .FALSE.
      DO_SLEAVE_WFS            = .FALSE.

      !  initialise atmospheric weighting function names
      PROFILEWF_NAMES = ' '
      COLUMNWF_NAMES  = ' '
      
      ! Now set the user defined inputs normally in the VLIDORT.cfg file
      ! ================================================================

      DO_FULLRAD_MODE         = .TRUE.  ! Do full Stokes vector calculation? (Error when false)
      DO_SSCORR_NADIR         = .TRUE.  ! Do nadir single scatter correction?
      DO_SSCORR_OUTGOING      = .FALSE. ! Do outgoing single scatter correction?
      DO_SSCORR_TRUNCATION    = .FALSE. ! Do delta-M scaling on single scatter corrections?
      DO_SS_EXTERNAL          = .FALSE. ! Do external single scatter correction?
      DO_SSFULL               = .FALSE. ! Do full-up single scatter calculation?
      DO_DOUBLE_CONVTEST      = .TRUE.  ! Do double convergence test?
      DO_SOLAR_SOURCES        = .TRUE.  ! Use solar sources?
      DO_PLANE_PARALLEL       = .FALSE. ! Do plane-parallel treatment of direct beam?
      DO_REFRACTIVE_GEOMETRY  = .FALSE. ! Do refractive geometry?
      DO_CHAPMAN_FUNCTION     = .TRUE.  ! Do internal Chapman function calculation?
      DO_RAYLEIGH_ONLY        = .FALSE. ! Do Rayleigh atmosphere only? NB DECIDED LATER
      DO_DELTAM_SCALING       = .TRUE.  ! Do delta-M scaling?
      DO_SOLUTION_SAVING      = .FALSE. ! Do solution saving?
      DO_BVP_TELESCOPING      = .FALSE. ! Do boundary-value telescoping?
      DO_UPWELLING            = SVL_do_upwelling ! Do upwelling output?
      DO_DNWELLING            = SVL_do_downwelling ! Do downwelling output?
      DO_USER_VZANGLES        = .TRUE.  ! Use user-defined viewing zenith angles?
      DO_SURFACE_LEAVING      = .FALSE. ! Do surface-leaving term?
      DO_SL_ISOTROPIC         = .FALSE. ! Do isotropic surface-leaving term?
      DO_ADDITIONAL_MVOUT     = .FALSE. ! Do mean-value output additionally?
      DO_MVOUT_ONLY           = .FALSE. ! Do only mean-value output?
      DO_OBSERVATION_GEOMETRY = .FALSE. ! Do Observation Geometry?
      DO_DEBUG_WRITE          = SVL_do_debug ! Do debug write?
      DO_WRITE_INPUT          = .FALSE. ! Do input control write?
      DO_WRITE_SCENARIO       = .FALSE. ! Do input scenario write?
      DO_WRITE_FOURIER        = .FALSE. ! Do Fourier component output write?
      DO_WRITE_RESULTS        = .FALSE. ! DO results write?
      INPUT_WRITE_FILENAME    = 'test2p7_input.res' ! filename for input write
      SCENARIO_WRITE_FILENAME = 'test2p7_scenario.res' ! filename for scenario write
      FOURIER_WRITE_FILENAME  = 'test2p7_fourier.res' ! filename for Fourier output write
      RESULTS_WRITE_FILENAME  = 'test2p7_results.res' ! filename for main output
      TAYLOR_ORDER            = 3 ! Number of small-number terms in Taylor series expansions
      IF( SVL_full_stokes_calc ) THEN
        NSTOKES = 1 ! Currently interpret full_stokes_calc this way
      ELSE
        NSTOKES = SVL_n_stokes ! Number of Stokes vector components
      ENDIF
      NSTREAMS                = SVL_nstreams ! Number of half-space streams
      NLAYERS                 = 23 ! Number of atmospheric layers
      NFINELAYERS             = 4 ! Number of fine layers (outgoing sphericity option only)
      NGREEK_MOMENTS_INPUT    = SVL_nmoments ! Number of scattering matrix expansion coefficients
      VLIDORT_ACCURACY        = 0.0001 ! Fourier series convergence
      FLUX_FACTOR             = 1.0 ! Solar flux constant
      N_SZANGLES              = 1 ! Number of solar zenith angles
      SZANGLES(1)             = 30.0 ! Solar zenith angles (degrees)
      EARTH_RADIUS            = 6371.0
      RFINDEX_PARAMETER       = 0.000288
      GEOMETRY_SPECHEIGHT     = 0.0    ! Input geometry specification height (km)
      N_USER_RELAZMS          = 1      ! Number of user-defined relative azimuth angles
      USER_RELAZMS(1)         = 0.01   ! User-defined relative azimuth angles (degrees)
      N_USER_VZANGLES         = 1      ! Number of user-defined viewing zenith angles
      USER_VZANGLES(1)        = 30.0   ! User-defined viewing zenith angles (degrees)
      N_USER_LEVELS           = 1      ! Number of user-defined output levels
      USER_LEVELS(1)          = 0.0    ! User-defined output levels
      DO_LAMBERTIAN_SURFACE   = .TRUE. ! Do Lambertian surface?
      LAMBERTIAN_ALBEDO       = 0.0    ! Lambertian albedo
      DO_THERMAL_EMISSION     = SVL_do_thermal_emiss ! Do thermal emission?
      N_THERMAL_COEFFS        = 2 ! Number of thermal coefficients
      DO_SURFACE_EMISSION     = SVL_do_surf_emiss ! Do surface emission?
      DO_THERMAL_TRANSONLY    = .FALSE. ! Do thermal emission, transmittance only?
      
      ! Linearization Options
      ! -----------------------------------
      
      ! CCM The following need to be decided by the jacobian diagnostics
      ! =================================================================================
      
      ! Jacobian Diagnostics
      ! SURFACE
      ! (28) Surface Albedo
      ! (29) Wind Speed Jacobian
      ! PROFILE
      ! ( 1) AMF Calculation
      ! (30) Temperature
      ! (31) Surface Pressure
      ! (32) Trace Gas
      ! (33) RRS Pseudoabsorption
      ! (34) AOD
      ! (35) ASSA
      ! (36) Cloud Fraction - Doesn't require linearization
      ! (37) Cloud Top Pressure - Not currently implemented 
      ! (38) COD
      ! (39) CSSA
      
      ! Do simulation only?
      IF( do_diag(28) .OR. do_diag(29) .OR. do_diag( 1) .OR. &
          do_diag(30) .OR. do_diag(31) .OR. do_diag(32) .OR. &
          do_diag(33) .OR. do_diag(34) .OR. do_diag(35) .OR. &
          do_diag(38) .OR. do_diag(39)                       ) THEN
        DO_SIMULATION_ONLY = .FALSE. 
      ELSE
        DO_SIMULATION_ONLY = .TRUE.
      ENDIF
      
      ! Do atmospheric profile weighting functions?
      IF( do_diag( 1) .OR.                                   &
          do_diag(30) .OR. do_diag(31) .OR. do_diag(32) .OR. &
          do_diag(33) .OR. do_diag(34) .OR. do_diag(35) .OR. &
          do_diag(38) .OR. do_diag(39)                       ) THEN
        DO_PROFILE_LINEARIZATION = .TRUE. 
      ELSE
        DO_PROFILE_LINEARIZATION = .FALSE. 
      ENDIF
      
      ! Do surface property weighting functions?
      IF( do_diag(28) .OR. do_diag(29) ) THEN
        DO_SURFACE_LINEARIZATION = .TRUE.
      ELSE
        DO_SURFACE_LINEARIZATION = .FALSE.
      ENDIF
      
      ! Set remaining flags to false since therre are currently no diagnostics requiring these
      DO_COLUMN_LINEARIZATION  = .FALSE. ! Do atmospheric column weighting functions?
      DO_ATMOS_LBBF            = .FALSE. ! Atmospheric BB emission weighting functions?
      N_TOTALPROFILE_WFS       = 1       ! Number of atmospheric profile weighting functions (total)
      DO_SURFACE_LBBF          = .FALSE. ! Surface BB emission weighting functions?
      ! =================================================================================
      
      IF( DO_COLUMN_LINEARIZATION ) THEN
        N_TOTALCOLUMN_WFS = 1! Number of atmospheric column weighting functions (total)
        LAYER_VARY_FLAG(1:NLAYERS)   = .TRUE.
        LAYER_VARY_NUMBER(1:NLAYERS) = N_TOTALCOLUMN_WFS
      ENDIF
      
      IF( DO_PROFILE_LINEARIZATION ) THEN
        PROFILEWF_NAMES(1) = 'Total_absorption_column--------' ! Atmospheric profile Jacobian names (character*31)
      ENDIF
      
      IF( DO_COLUMN_LINEARIZATION ) THEN
        COLUMNWF_NAMES(1) = 'Total_absorption_column--------' ! Atmospheric column Jacobian names (character*31)
      ENDIF
      
      DO_ATMOS_LINEARIZATION = ( DO_PROFILE_LINEARIZATION.OR. &
                                 DO_COLUMN_LINEARIZATION .OR. &
                                 DO_ATMOS_LBBF )

      DO_LINEARIZATION = ( DO_ATMOS_LINEARIZATION   .OR. &
                           DO_SURFACE_LINEARIZATION .OR. &
                           DO_SURFACE_LBBF )
      
      ! ================================================================
      
      !  New Exception handling for SPLAT User Defined inputs only 
      IF ( NSTREAMS .GT. MAXSTREAMS ) THEN
        NMESSAGES = NMESSAGES + 1
        MESSAGES(NMESSAGES) = &
               'Entry under "Number of half-space streams" >'// &
               ' allowed Maximum dimension'
        ACTIONS(NMESSAGES)  = &
             'Re-set input value or increase MAXSTREAMS dimension '// &
             'in VLIDORT_PARS'
        STATUS = VLIDORT_SERIOUS
        RETURN
      ENDIF

      IF ( DO_SSCORR_OUTGOING .or. DO_SSFULL ) THEN
        IF ( NFINELAYERS .GT. MAXFINELAYERS ) THEN
          NMESSAGES = NMESSAGES + 1
          MESSAGES(NMESSAGES) = &
               'Entry under "Number of fine layers..." >'// &
               ' allowed Maximum dimension'
          ACTIONS(NMESSAGES)  = &
            'Re-set input value or increase MAXFINELAYERS dimension '// &
            'in VLIDORT_PARS'
          STATUS = VLIDORT_SERIOUS
          RETURN
        ENDIF
      ENDIF

      IF ( NGREEK_MOMENTS_INPUT .GT. MAXMOMENTS_INPUT ) THEN
        NMESSAGES = NMESSAGES + 1
        MESSAGES(NMESSAGES) = &
         'Entry under'// &
       ' "Number of input Scattering Matrix expansion coefficients" >'// &
               ' allowed Maximum dimension'
        ACTIONS(NMESSAGES)  = &
         'Re-set input value or increase MAXMOMENTS_INPUT dimension '// &
            'in VLIDORT_PARS'
        STATUS = VLIDORT_SERIOUS
        RETURN
      ENDIF

      IF ( NSTOKES .GT. MAXSTOKES ) THEN
        NMESSAGES= NMESSAGES + 1
        MESSAGES(NMESSAGES) = &
         'Entry under'//' "Number of Stokes parameters" >'// &
               ' allowed Maximum dimension'
        ACTIONS(NMESSAGES)  = 'Re-set input value to 4 or less'
        STATUS = VLIDORT_SERIOUS
        RETURN
      ENDIF
      
      !  normal execution: Copy all variables and return
      !  -----------------------------------------------
      
!  Fixed Boolean inputs

      VLIDORT_FixIn%Bool%TS_DO_FULLRAD_MODE        = DO_FULLRAD_MODE
      VLIDORT_FixIn%Bool%TS_DO_SSCORR_TRUNCATION   = DO_SSCORR_TRUNCATION
!  New 15 march 2012
      VLIDORT_FixIn%Bool%TS_DO_SS_EXTERNAL         = DO_SS_EXTERNAL
      VLIDORT_FixIn%Bool%TS_DO_SSFULL              = DO_SSFULL
      VLIDORT_FixIn%Bool%TS_DO_THERMAL_EMISSION    = DO_THERMAL_EMISSION
      VLIDORT_FixIn%Bool%TS_DO_SURFACE_EMISSION    = DO_SURFACE_EMISSION
      VLIDORT_FixIn%Bool%TS_DO_PLANE_PARALLEL      = DO_PLANE_PARALLEL
      !VLIDORT_FixIn%Bool%TS_DO_BRDF_SURFACE        = DO_BRDF_SURFACE
      VLIDORT_FixIn%Bool%TS_DO_UPWELLING           = DO_UPWELLING
      VLIDORT_FixIn%Bool%TS_DO_DNWELLING           = DO_DNWELLING
      VLIDORT_FixIn%Bool%TS_DO_QUAD_OUTPUT         = DO_QUAD_OUTPUT
      VLIDORT_FixIn%Bool%TS_DO_TOA_CONTRIBS        = DO_TOA_CONTRIBS
      VLIDORT_FixIn%Bool%TS_DO_LAMBERTIAN_SURFACE  = DO_LAMBERTIAN_SURFACE
      VLIDORT_FixIn%Bool%TS_DO_SPECIALIST_OPTION_1 = DO_SPECIALIST_OPTION_1
      VLIDORT_FixIn%Bool%TS_DO_SPECIALIST_OPTION_2 = DO_SPECIALIST_OPTION_2
      VLIDORT_FixIn%Bool%TS_DO_SPECIALIST_OPTION_3 = DO_SPECIALIST_OPTION_3

!  New 17 May 2012
      VLIDORT_FixIn%Bool%TS_DO_SURFACE_LEAVING     = DO_SURFACE_LEAVING
      VLIDORT_FixIn%Bool%TS_DO_SL_ISOTROPIC        = DO_SL_ISOTROPIC

!  Fixed Control inputs. New 2p7 Taylor-order.

      VLIDORT_FixIn%Cont%TS_TAYLOR_ORDER     = TAYLOR_ORDER 
      VLIDORT_FixIn%Cont%TS_NSTOKES          = NSTOKES
      VLIDORT_FixIn%Cont%TS_NSTREAMS         = NSTREAMS
      VLIDORT_FixIn%Cont%TS_NLAYERS          = NLAYERS
      VLIDORT_FixIn%Cont%TS_NFINELAYERS      = NFINELAYERS
      VLIDORT_FixIn%Cont%TS_N_THERMAL_COEFFS = N_THERMAL_COEFFS
      VLIDORT_FixIn%Cont%TS_VLIDORT_ACCURACY = VLIDORT_ACCURACY

!  Fixed Beam inputs

      VLIDORT_FixIn%SunRays%TS_FLUX_FACTOR = FLUX_FACTOR

!  Fixed User Value inputs

      VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS       = N_USER_LEVELS

!  Fixed Chapman Function inputs

      !VLIDORT_FixIn%Chapman%TS_HEIGHT_GRID       = HEIGHT_GRID
      !VLIDORT_FixIn%Chapman%TS_PRESSURE_GRID     = PRESSURE_GRID
      !VLIDORT_FixIn%Chapman%TS_TEMPERATURE_GRID  = TEMPERATURE_GRID
      !VLIDORT_FixIn%Chapman%TS_FINEGRID          = FINEGRID
      VLIDORT_FixIn%Chapman%TS_RFINDEX_PARAMETER = RFINDEX_PARAMETER

!  Fixed Optical inputs

      !VLIDORT_FixIn%Optical%TS_DELTAU_VERT_INPUT    = DELTAU_VERT_INPUT
      !VLIDORT_FixIn%Optical%TS_GREEKMAT_TOTAL_INPUT = GREEKMAT_TOTAL_INPUT
      VLIDORT_FixIn%Optical%TS_THERMAL_BB_INPUT     = THERMAL_BB_INPUT
      VLIDORT_FixIn%Optical%TS_LAMBERTIAN_ALBEDO    = LAMBERTIAN_ALBEDO
      VLIDORT_FixIn%Optical%TS_SURFACE_BB_INPUT     = SURFBB

!  Fixed Write inputs

      VLIDORT_FixIn%Write%TS_DO_DEBUG_WRITE           = DO_DEBUG_WRITE

      VLIDORT_FixIn%Write%TS_DO_WRITE_INPUT           = DO_WRITE_INPUT
      VLIDORT_FixIn%Write%TS_INPUT_WRITE_FILENAME     = INPUT_WRITE_FILENAME

      VLIDORT_FixIn%Write%TS_DO_WRITE_SCENARIO        = DO_WRITE_SCENARIO
      VLIDORT_FixIn%Write%TS_SCENARIO_WRITE_FILENAME  = SCENARIO_WRITE_FILENAME

      VLIDORT_FixIn%Write%TS_DO_WRITE_FOURIER         = DO_WRITE_FOURIER
      VLIDORT_FixIn%Write%TS_FOURIER_WRITE_FILENAME   = FOURIER_WRITE_FILENAME

      VLIDORT_FixIn%Write%TS_DO_WRITE_RESULTS         = DO_WRITE_RESULTS
      VLIDORT_FixIn%Write%TS_RESULTS_WRITE_FILENAME   = RESULTS_WRITE_FILENAME

!  Modified Boolean inputs

      VLIDORT_ModIn%MBool%TS_DO_SSCORR_NADIR         = DO_SSCORR_NADIR
      VLIDORT_ModIn%MBool%TS_DO_SSCORR_OUTGOING      = DO_SSCORR_OUTGOING
      VLIDORT_ModIn%MBool%TS_DO_FO_CALC              = DO_FO_CALC

      VLIDORT_ModIn%MBool%TS_DO_DOUBLE_CONVTEST      = DO_DOUBLE_CONVTEST
      VLIDORT_ModIn%MBool%TS_DO_SOLAR_SOURCES        = DO_SOLAR_SOURCES

      VLIDORT_ModIn%MBool%TS_DO_REFRACTIVE_GEOMETRY  = DO_REFRACTIVE_GEOMETRY
      VLIDORT_ModIn%MBool%TS_DO_CHAPMAN_FUNCTION     = DO_CHAPMAN_FUNCTION

      VLIDORT_ModIn%MBool%TS_DO_RAYLEIGH_ONLY        = DO_RAYLEIGH_ONLY
      !VLIDORT_ModIn%MBool%TS_DO_ISOTROPIC_ONLY       = DO_ISOTROPIC_ONLY
      !VLIDORT_ModIn%MBool%TS_DO_NO_AZIMUTH           = DO_NO_AZIMUTH
      !VLIDORT_ModIn%MBool%TS_DO_ALL_FOURIER          = DO_ALL_FOURIER

      VLIDORT_ModIn%MBool%TS_DO_DELTAM_SCALING       = DO_DELTAM_SCALING

      VLIDORT_ModIn%MBool%TS_DO_SOLUTION_SAVING      = DO_SOLUTION_SAVING
      VLIDORT_ModIn%MBool%TS_DO_BVP_TELESCOPING      = DO_BVP_TELESCOPING

      !VLIDORT_ModIn%MBool%TS_DO_USER_STREAMS         = DO_USER_STREAMS
      VLIDORT_ModIn%MBool%TS_DO_USER_VZANGLES        = DO_USER_VZANGLES

      VLIDORT_ModIn%MBool%TS_DO_ADDITIONAL_MVOUT     = DO_ADDITIONAL_MVOUT
      VLIDORT_ModIn%MBool%TS_DO_MVOUT_ONLY           = DO_MVOUT_ONLY

      VLIDORT_ModIn%MBool%TS_DO_THERMAL_TRANSONLY    = DO_THERMAL_TRANSONLY

      VLIDORT_ModIn%MBool%TS_DO_OBSERVATION_GEOMETRY = DO_OBSERVATION_GEOMETRY

!  Modified Control inputs

      VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT = NGREEK_MOMENTS_INPUT

!  Modified Beam inputs

      !VLIDORT_ModIn%MSunRays%TS_NBEAMS      = NBEAMS
      VLIDORT_ModIn%MSunRays%TS_N_SZANGLES  = N_SZANGLES
      !VLIDORT_ModIn%MSunRays%TS_BEAM_SZAS   = BEAM_SZAS
      VLIDORT_ModIn%MSunRays%TS_SZANGLES    = SZANGLES

!  Modified User Value inputs

      VLIDORT_ModIn%MUserVal%TS_N_USER_RELAZMS      = N_USER_RELAZMS
      VLIDORT_ModIn%MUserVal%TS_USER_RELAZMS        = USER_RELAZMS

      !VLIDORT_ModIn%MUserVal%TS_N_USER_STREAMS      = N_USER_STREAMS
      VLIDORT_ModIn%MUserVal%TS_N_USER_VZANGLES     = N_USER_VZANGLES
      !VLIDORT_ModIn%MUserVal%TS_USER_ANGLES_INPUT   = USER_ANGLES
      VLIDORT_ModIn%MUserVal%TS_USER_VZANGLES_INPUT = USER_VZANGLES

      VLIDORT_ModIn%MUserVal%TS_USER_LEVELS         = USER_LEVELS

      VLIDORT_ModIn%MUserVal%TS_GEOMETRY_SPECHEIGHT = GEOMETRY_SPECHEIGHT

      VLIDORT_ModIn%MUserVal%TS_N_USER_OBSGEOMS     = N_USER_OBSGEOMS
      VLIDORT_ModIn%MUserVal%TS_USER_OBSGEOMS_INPUT = USER_OBSGEOMS

!  Modified Chapman Function inputs

      VLIDORT_ModIn%MChapman%TS_EARTH_RADIUS      = EARTH_RADIUS

!  Modified Optical inputs

      !VLIDORT_ModIn%MOptical%TS_OMEGA_TOTAL_INPUT    = OMEGA_TOTAL_INPUT

!  Fixed Linearized Control inputs

      VLIDORT_LinFixIn%Cont%TS_LAYER_VARY_FLAG    = &
                               LAYER_VARY_FLAG
      VLIDORT_LinFixIn%Cont%TS_LAYER_VARY_NUMBER  = &
                               LAYER_VARY_NUMBER

      VLIDORT_LinFixIn%Cont%TS_N_TOTALCOLUMN_WFS  = &
                               N_TOTALCOLUMN_WFS
      VLIDORT_LinFixIn%Cont%TS_N_TOTALPROFILE_WFS = &
                               N_TOTALPROFILE_WFS
      VLIDORT_LinFixIn%Cont%TS_N_SURFACE_WFS      = &
                               N_SURFACE_WFS
      VLIDORT_LinFixIn%Cont%TS_N_SLEAVE_WFS       = &
                               N_SLEAVE_WFS

      VLIDORT_LinFixIn%Cont%TS_COLUMNWF_NAMES  = &
                               COLUMNWF_NAMES
      VLIDORT_LinFixIn%Cont%TS_PROFILEWF_NAMES = &
                               PROFILEWF_NAMES

!  Modified linearized control inputs
!  (First three were formerely in Fixed Lin Cont)

      VLIDORT_LinModIn%MCont%TS_DO_SIMULATION_ONLY   = &
                                DO_SIMULATION_ONLY

!  replaced 2p6 variables
!      VLIDORT_LinFixIn%Cont%TS_DO_LTE_LINEARIZATION    = DO_LTE_LINEARIZATION
!      VLIDORT_LinFixIn%Cont%TS_DO_SURFBB_LINEARIZATION = DO_SURFBB_LINEARIZATION
      VLIDORT_LinModIn%MCont%TS_DO_ATMOS_LBBF   = DO_ATMOS_LBBF
      VLIDORT_LinModIn%MCont%TS_DO_SURFACE_LBBF = DO_SURFACE_LBBF

      VLIDORT_LinModIn%MCont%TS_DO_SLEAVE_WFS   = DO_SLEAVE_WFS

      VLIDORT_LinModIn%MCont%TS_DO_COLUMN_LINEARIZATION  = &
                                DO_COLUMN_LINEARIZATION
      VLIDORT_LinModIn%MCont%TS_DO_PROFILE_LINEARIZATION = &
                                DO_PROFILE_LINEARIZATION
      VLIDORT_LinModIn%MCont%TS_DO_ATMOS_LINEARIZATION   = &
                                DO_ATMOS_LINEARIZATION

      VLIDORT_LinModIn%MCont%TS_DO_SURFACE_LINEARIZATION = &
                                DO_SURFACE_LINEARIZATION
      VLIDORT_LinModIn%MCont%TS_DO_LINEARIZATION         = &
                                DO_LINEARIZATION
      
      
      ! Set here
      do_JACOBIANS = DO_LINEARIZATION
      
!  Exception handling

      VLIDORT_InputStatus%TS_STATUS_INPUTREAD = STATUS
      VLIDORT_InputStatus%TS_NINPUTMESSAGES   = NMESSAGES
      VLIDORT_InputStatus%TS_INPUTMESSAGES    = MESSAGES
      VLIDORT_InputStatus%TS_INPUTACTIONS     = ACTIONS
      

    ! More hardcoded values from original GC-Tool
    CALL Vlidort_GC_config( error )

    
   ! ---------------------------------
   ! Set Jacobian Flags based on input
   ! ---------------------------------
   do_aerph_Jacobians  = .FALSE.
   do_aerhw_Jacobians  = .FALSE.

   IF (.NOT. do_Jacobians) THEN
      do_QU_Jacobians     = .FALSE.
      do_T_Jacobians      = .FALSE.
      do_sfcprs_Jacobians = .FALSE.
      do_aod_Jacobians    = .FALSE.
      do_assa_Jacobians   = .FALSE.
      do_aerph_Jacobians  = .FALSE.
      do_aerhw_Jacobians  = .FALSE.
      do_cod_Jacobians    = .FALSE.
      do_cssa_Jacobians   = .FALSE.
      do_ctp_Jacobians    = .FALSE.
      do_cfrac_Jacobians  = .FALSE.
      do_AMF_calculation  = .FALSE.
   END IF

  END SUBROUTINE initialize_vlidort_inputs
  
  SUBROUTINE Vlidort_GC_config(error)
    
    IMPLICIT NONE
    
    ! ------------------
    ! Modified variables
    ! ------------------
    LOGICAL,       INTENT(INOUT) :: error ! Error variable

    ! ---------------
    ! Local variables
    ! ---------------
    INTEGER :: i,j,n

    ! ----------------
    ! Code starts here
    ! ----------------
    error = .FALSE.
    
    ! ========================
    ! SECTION 1 (Fixed inputs)
    ! ========================
    
    VLIDORT_FixIn%Bool%TS_DO_QUAD_OUTPUT  = .FALSE. ! No option in Vlidort Control file

    !  New to VERSION 2.7. Use of Internal FO calculation, controlled by single hard-wired flag
    VLIDORT_ModIn%MBool%TS_DO_FO_CALC  = .FALSE.
!!$    VLIDORT_ModIn%MBool%TS_DO_FO_CALC  = .TRUE.

    ! =====================================
    ! To clean up the GC control input file
    ! and avoid duplications between it and
    ! Vlidort control file
    ! =====================================
    do_vector_calculation = .FALSE.
    do_vector_calculation = (VLIDORT_FixIn%Cont%TS_NSTOKES==3) .OR. (VLIDORT_FixIn%Cont%TS_NSTOKES==4)
    
    ! ----------------------------------------
    ! Check for aerosols and scattering clouds
    ! ----------------------------------------
    IF ( do_aerosols .OR. (do_clouds .AND. .NOT. do_lambertian_cld)) THEN
       IF ( VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT .LT. 2 * VLIDORT_FixIn%Cont%TS_NSTREAMS) THEN
          CALL write_err_message ( .FALSE., "Not enough aerosol/cloud moments"// &
               ", must be at least 2*nstreams")
          error = .TRUE.
       END IF
    END IF
    
    ! Basic control integers
    !  -- Number of layers determined from GC profiles
    !  -- Number of fine-layer subdivisions in single-scattering = 2
    !  -- Number of expansion coefficients = 2     for Rayleigh-only
    !  -- Number of expansion coefficients = Input for Rayleigh+Aerosol
    !     NSTOKES  = Will be set in Section 2
    !     NLAYERS  = Will be set in Section 2
    
    ! Basic  numbers
    !    -- Fourier convergence accuracy (not required)
    !    -- earth radius (fixed here, but could be function of LAT & LONG)
    !    -- Refractive geometry parameter (not required)
    !    -- Geometry specification height = Botton of height grid (set later)
    VLIDORT_ModIn%MChapman%TS_EARTH_RADIUS       = 6378.d0
    VLIDORT_FixIn%Optical%TS_THERMAL_BB_INPUT(:) = 0.0d0
    VLIDORT_FixIn%Optical%TS_SURFACE_BB_INPUT    = 0.0d0
    
    
    ! ========================================================
    ! SECTION 2 (Other VLIDORT inputs depending on GC control)
    ! ========================================================
    IF (VLIDORT_FixIn%Bool%TS_DO_UPWELLING .AND. VLIDORT_FixIn%Bool%TS_DO_DNWELLING) THEN
       ndir = 2; idix(1) = 1; idix(2) = 2
    ELSEIF (VLIDORT_FixIn%Bool%TS_DO_UPWELLING .AND. .NOT. VLIDORT_FixIn%Bool%TS_DO_DNWELLING) THEN
       ndir = 1; idix(1) = 1
    ELSEIF (.NOT. VLIDORT_FixIn%Bool%TS_DO_UPWELLING .AND. VLIDORT_FixIn%Bool%TS_DO_DNWELLING) THEN
       ndir = 1; idix(1) = 2
    END IF
    
    ! --------------------------------------------------------------------------
    ! Output level: Usually it comes from VLIDORT input file. In GC control file
    ! it is possible to use altitude in km to set the output levels.
    ! --------------------------------------------------------------------------
    IF (do_yn_obsalt) THEN
       GC_user_altitudes(1) = prof_obsalt
    END IF
    IF (GC_do_user_altitudes) THEN
       DO i = 1, GC_n_user_altitudes
          IF (GC_user_altitudes(i) >= heights(0)) THEN
             GC_user_levels(i) = 0.0
          ELSE IF (GC_user_altitudes(i) <= heights(GC_nlayers)) THEN
             GC_user_levels(i)    = GC_nlayers
             GC_user_altitudes(i) = heights(GC_nlayers)
          ELSE
             DO j = 1, GC_nlayers
                IF (GC_user_altitudes(i) >= heights(j)) THEN
                   GC_user_levels(i) = (heights(j-1) - GC_user_altitudes(i)) &
                        / (heights(j-1) - heights(j)) + j - 1
                   EXIT
                ENDIF
             ENDDO
          ENDIF
       ENDDO
       VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS = GC_n_user_altitudes
       GC_n_user_levels = GC_n_user_altitudes
       DO n = 1, VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS
          VLIDORT_ModIn%MUserVal%TS_USER_LEVELS(n) = GC_user_levels(n)
       END DO
    ELSE
       GC_n_user_levels = VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS
       GC_user_levels(1:GC_n_user_levels) = VLIDORT_ModIn%MUserVal%TS_USER_LEVELS(1:GC_n_user_levels)
       DO i = 1, GC_n_user_levels
          j = INT(GC_user_levels(i))
          GC_user_altitudes(i) = heights(j) - (GC_user_levels(i) - j) &
               * (heights(j+1) - heights(j))
       ENDDO
    ENDIF

    ! Convert VLIDORT_ModIn%MUserVal%TS_USER_LEVELS(1:GC_n_user_levels) to whole numbers
    ! to prevent crash when using FO code.
    IF (VLIDORT_ModIn%MBool%TS_DO_FO_CALC) THEN
       DO i = 1, VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS
          VLIDORT_ModIn%MUserVal%TS_USER_LEVELS(i) = NINT(VLIDORT_ModIn%MUserVal%TS_USER_LEVELS(i))
          j = INT(VLIDORT_ModIn%MUserVal%TS_USER_LEVELS(i))
          GC_user_altitudes(i) = heights(j)
       END DO
    ENDIF

    ! Set the Number of Stokes parameters (1 or 3) and layers
!!$    VLIDORT_FixIn%Cont%TS_NSTOKES = 1 ! gga Now controlled in VLIDORT_Input.cfg file 06/17/15
!!$    IF ( do_vector_calculation ) VLIDORT_FixIn%Cont%TS_NSTOKES = 3 
    
    ngksec = 1
    IF ( do_vector_calculation ) ngksec = 6 
    
    nactgkmatc = 1
    IF (do_vector_calculation) nactgkmatc = 8
    
    VLIDORT_FixIn%Cont%TS_NLAYERS = GC_nlayers
    
    ! Set the height grid, and Geometry specification height
    VLIDORT_FixIn%Chapman%TS_height_grid(0:VLIDORT_FixIn%Cont%TS_NLAYERS) = &
         heights(0:VLIDORT_FixIn%Cont%TS_NLAYERS)
    VLIDORT_ModIn%MUserVal%TS_GEOMETRY_SPECHEIGHT = heights(VLIDORT_FixIn%Cont%TS_NLAYERS)

    ! Now will be done in the cloud/clear loop  
    ! Reduce # of layers for Lambertian cloud surface (now done in clear/cloud loop)
    ! if (do_clouds .and. do_lambertian_cld) NLAYERS = cld_uppers(1) - 1
    
    ! Set the Geometry. Just a straight copy of VLIDORT input into GC_ inputs if we are
    ! not using footprint information. If doing so, the angles have been set before in
    ! GC_profiles_module.f90
    
    ! CCM The following must be overwritten no matter what since 
    ! the geometry is set in the input file

    ! CCM Need to fix to work with mutliple angles? 4/11/18
!     IF (use_footprint_info .OR. do_sat_viewcalc) THEN
       VLIDORT_ModIn%MSunrays%TS_N_SZANGLES  = GC_n_sun_positions
       VLIDORT_ModIn%MSunrays%TS_SZANGLES(1) = GC_sun_positions(1)
       
       VLIDORT_ModIn%MUserVal%TS_N_USER_VZANGLES        = GC_n_view_angles
       VLIDORT_ModIn%MUserVal%TS_USER_VZANGLES_INPUT(1) = GC_view_angles(1) 
       
       VLIDORT_ModIn%MUserVal%TS_N_USER_RELAZMS  = GC_n_azimuths
       VLIDORT_ModIn%MUserVal%TS_USER_RELAZMS(1) = GC_azimuths(1)
!     ELSE
!        GC_n_sun_positions = VLIDORT_ModIn%MSunrays%TS_N_SZANGLES
!        DO n = 1, VLIDORT_ModIn%MSunrays%TS_N_SZANGLES
!           GC_sun_positions(n) = VLIDORT_ModIn%MSunrays%TS_SZANGLES(n)
!        END DO       
!        GC_n_view_angles = VLIDORT_ModIn%MUserVal%TS_N_USER_VZANGLES
!        DO n = 1, VLIDORT_ModIn%MUserVal%TS_N_USER_VZANGLES
!           GC_view_angles(n) = VLIDORT_ModIn%MUserVal%TS_USER_VZANGLES_INPUT(n)
!        END DO
!        GC_n_azimuths = VLIDORT_ModIn%MUserVal%TS_N_USER_RELAZMS
!        DO n = 1, VLIDORT_ModIn%MUserVal%TS_N_USER_RELAZMS
!           GC_azimuths(n) = VLIDORT_ModIn%MUserVal%TS_USER_RELAZMS(n)
!        END DO
!     ENDIF

    ! -----------------------------------------------------------------------
    ! Set the linearization control for profile Jacobians (cloudy conditions)
    ! Might be different for clear-sky part
    ! -----------------------------------------------------------------------
    ! Linearization inputs
    !    -- No total column Jacobians, no BRDF Jacobians
    !    -- profile and Surface Jacobians, flags set by GC input control
    VLIDORT_LinModIn%MCont%TS_DO_COLUMN_LINEARIZATION = .FALSE.
    VLIDORT_LinFixIn%Cont%TS_N_TOTALCOLUMN_WFS        = 0

!     ! Set up do_JACOBIANS variable based in Vlidort input file
!     IF ( (VLIDORT_LinModIn%MCont%TS_do_simulation_only       .EQV. .FALSE.) .AND. &
!          (VLIDORT_LinModIn%MCont%TS_do_profile_linearization .EQV. .TRUE. ) ) THEN
!        do_JACOBIANS = .TRUE.
!        VLIDORT_LinModIn%MCont%TS_do_atmos_linearization = .TRUE.
!        VLIDORT_LinModIn%MCont%TS_do_linearization       = .TRUE.
!     ELSE
!        do_JACOBIANS = .FALSE.
!        VLIDORT_LinModIn%MCont%TS_do_atmos_linearization = .FALSE.
!        VLIDORT_LinModIn%MCont%TS_do_linearization       = .FALSE.
!     ENDIF
!     
!     ! We need to catch linearization for the surface
!     IF( VLIDORT_LinModIn%MCont%TS_do_surface_linearization ) THEN
!       do_JACOBIANS = .TRUE.
!       VLIDORT_LinModIn%MCont%TS_do_linearization       = .FALSE.
!     ENDIF
    
    ! Only able to do Stokes output if do_vector_calculation
    IF ( (do_vector_calculation .EQV. .FALSE.) .AND. &
         (do_StokesQU_Output    .EQV. .TRUE. ) ) THEN
       WRITE(*,*) 'Vector calculation set to false and Stokes output to true'
       WRITE(*,*) 'Check input setup in GC_input_file'
!        STOP
    ENDIF

    ! If do vector calculation and Stokes output set do_QU_Jacobians to .TRUE.
    IF ( (do_vector_calculation .EQV. .TRUE.) .AND. &
         (do_StokesQU_Output    .EQV. .TRUE. ) ) THEN
       do_QU_Jacobians = .TRUE.
    ELSE
       do_QU_Jacobians = .FALSE.
    ENDIF

    ! If not Jacobians and then it is not possible to do AMF
    IF ( (do_JACOBIANS       .EQV. .FALSE.) .AND. &
          do_AMF_calculation .EQV. .TRUE. ) THEN
       WRITE(*,*) 'You can not do AMFs without doing Jacobians first'
       WRITE(*,*) 'Check Vlidort input and GC_Input'
       STOP
    ENDIF

    IF ( ( do_T_Jacobians   .OR. do_sfcprs_Jacobians .OR.  &
           do_aod_Jacobians .OR. do_assa_Jacobians   .OR.  &
           do_cod_Jacobians .OR. do_cssa_Jacobians)  .AND. &
           (.NOT. do_Jacobians) ) THEN
       WRITE(*,*) do_Jacobians
       WRITE(*,*) 'You have requested some jacobians while the main linearization'
       WRITE(*,*) 'flags in VLIDORT control file are not set for that:'
       WRITE(*,*) 'Do simulation only? F' 
       WRITE(*,*) 'Do atmospheric profile weighting functions? T'
       STOP
    ENDIF

    gaswfidx = 0; twfidx = 0; aodwfidx = 0; assawfidx = 0
    sfcprswfidx = 0; codwfidx = 0; cssawfidx = 0
    IF ( do_JACOBIANS ) THEN

       VLIDORT_LinFixIn%Cont%TS_N_TOTALPROFILE_WFS       = 1
       VLIDORT_LinFixIn%Cont%TS_profilewf_names(1)       = '-Trace Gas Volume Mixing Ratio-'
       gaswfidx = 1
       
       IF (do_sfcprs_Jacobians) THEN
          VLIDORT_LinFixIn%Cont%TS_N_TOTALPROFILE_WFS    = VLIDORT_LinFixIn%Cont%TS_N_TOTALPROFILE_WFS + 1
          sfcprswfidx = VLIDORT_LinFixIn%Cont%TS_N_TOTALPROFILE_WFS
          VLIDORT_LinFixIn%Cont%TS_profilewf_names(VLIDORT_LinFixIn%Cont%TS_N_TOTALPROFILE_WFS)  = &
               '-------Surface Pressure--------'
       END IF
       
       IF (do_aod_Jacobians) THEN
          
          IF( do_total_aod_jac ) THEN
          
            VLIDORT_LinFixIn%Cont%TS_N_TOTALPROFILE_WFS = VLIDORT_LinFixIn%Cont%TS_N_TOTALPROFILE_WFS + 1
            aodwfidx = VLIDORT_LinFixIn%Cont%TS_N_TOTALPROFILE_WFS
            VLIDORT_LinFixIn%Cont%TS_profilewf_names(VLIDORT_LinFixIn%Cont%TS_N_TOTALPROFILE_WFS)  = &
                '-----Aerosol Optical Depth-----'
            IF( do_debug_geocape_tool ) print*,'VL_CONFIG: Doing AOD Jacobians',aodwfidx
            
          ENDIF
          
          ! Species specific jacobians
          DO n=1,Diag34_InpOpt%njac
            VLIDORT_LinFixIn%Cont%TS_N_TOTALPROFILE_WFS = VLIDORT_LinFixIn%Cont%TS_N_TOTALPROFILE_WFS + 1
            Diag34_InpOpt%wfidx(n) = VLIDORT_LinFixIn%Cont%TS_N_TOTALPROFILE_WFS
            VLIDORT_LinFixIn%Cont%TS_profilewf_names(VLIDORT_LinFixIn%Cont%TS_N_TOTALPROFILE_WFS)  = &
                '--' // TRIM(ADJUSTL(Diag34_InpOpt%jac(n))) // ' Aerosol Optical Depth--'
            IF( do_debug_geocape_tool ) &
              print*,'VL_CONFIG: Doing '// TRIM(ADJUSTL(Diag34_InpOpt%jac(n))) //' AOD Jacobians'
          ENDDO
          
       END IF
       
       IF (do_assa_Jacobians) THEN
        
          IF( do_total_assa_jac ) THEN
            
            VLIDORT_LinFixIn%Cont%TS_N_TOTALPROFILE_WFS = VLIDORT_LinFixIn%Cont%TS_N_TOTALPROFILE_WFS + 1
            assawfidx = VLIDORT_LinFixIn%Cont%TS_N_TOTALPROFILE_WFS
            VLIDORT_LinFixIn%Cont%TS_profilewf_names(VLIDORT_LinFixIn%Cont%TS_N_TOTALPROFILE_WFS)  = &
              '---Aerosol Single Sca Albedo---'
          ENDIF
                
          ! Species specific jacobians
          DO n=1,Diag35_InpOpt%njac
            VLIDORT_LinFixIn%Cont%TS_N_TOTALPROFILE_WFS = VLIDORT_LinFixIn%Cont%TS_N_TOTALPROFILE_WFS + 1
            Diag35_InpOpt%wfidx(n) = VLIDORT_LinFixIn%Cont%TS_N_TOTALPROFILE_WFS
              VLIDORT_LinFixIn%Cont%TS_profilewf_names(VLIDORT_LinFixIn%Cont%TS_N_TOTALPROFILE_WFS)  = &
                  '--' // TRIM(ADJUSTL(Diag35_InpOpt%jac(n))) // ' Aerosol SSA--'
              IF( do_debug_geocape_tool ) &
                print*,'VL_CONFIG: Doing '// TRIM(ADJUSTL(Diag35_InpOpt%jac(n))) //' SSA Jacobians'
          ENDDO
          
       ENDIF
       
       IF (do_raman_Jacobian .and. do_rrs_abs ) THEN
       
          ! Raman "absorption"
          VLIDORT_LinFixIn%Cont%TS_N_TOTALPROFILE_WFS = VLIDORT_LinFixIn%Cont%TS_N_TOTALPROFILE_WFS + 1
          rrswfidx = VLIDORT_LinFixIn%Cont%TS_N_TOTALPROFILE_WFS
          VLIDORT_LinFixIn%Cont%TS_profilewf_names(VLIDORT_LinFixIn%Cont%TS_N_TOTALPROFILE_WFS)  = &
               '-----O2Raman Optical Depth-----'
          
          
          IF( do_debug_geocape_tool ) print*,'VL_CONFIG: Doing Raman Jacobians',rrswfidx
          
       ENDIF
       
       N_TOTALPROFILE_WFS_ncld = VLIDORT_LinFixIn%Cont%TS_N_TOTALPROFILE_WFS
       
       ! Put cloud related Jacobians at the end
       IF (do_cod_Jacobians) THEN
          VLIDORT_LinFixIn%Cont%TS_N_TOTALPROFILE_WFS = VLIDORT_LinFixIn%Cont%TS_N_TOTALPROFILE_WFS + 1
          codwfidx = VLIDORT_LinFixIn%Cont%TS_N_TOTALPROFILE_WFS
          VLIDORT_LinFixIn%Cont%TS_profilewf_names(VLIDORT_LinFixIn%Cont%TS_N_TOTALPROFILE_WFS)  = &
               '------Cloud Optical Depth------'
          IF( do_debug_geocape_tool ) print*,'VL_CONFIG: Doing COD Jacobians',codwfidx
       END IF
       
       IF (do_cssa_Jacobians) THEN
          VLIDORT_LinFixIn%Cont%TS_N_TOTALPROFILE_WFS = VLIDORT_LinFixIn%Cont%TS_N_TOTALPROFILE_WFS + 1
          cssawfidx = VLIDORT_LinFixIn%Cont%TS_N_TOTALPROFILE_WFS
          VLIDORT_LinFixIn%Cont%TS_profilewf_names(VLIDORT_LinFixIn%Cont%TS_N_TOTALPROFILE_WFS)  = &
               '----Cloud Single Sca Albedo----'
       END IF
       
       
       
       N_TOTALPROFILE_WFS_wcld = VLIDORT_LinFixIn%Cont%TS_N_TOTALPROFILE_WFS
       
       VLIDORT_LinFixIn%Cont%TS_layer_vary_flag(1:VLIDORT_FixIn%Cont%TS_NLAYERS)   = .TRUE.
       VLIDORT_LinFixIn%Cont%TS_layer_vary_number(1:VLIDORT_FixIn%Cont%TS_NLAYERS) = &
            VLIDORT_LinFixIn%Cont%TS_N_TOTALPROFILE_WFS
      
    ELSE
       VLIDORT_LinModIn%MCont%TS_do_profile_linearization = .FALSE.
       VLIDORT_LinModIn%MCont%TS_do_atmos_linearization   = .FALSE.
       VLIDORT_LinModIn%MCont%TS_do_linearization         = .FALSE.
       VLIDORT_LinFixIn%Cont%TS_N_TOTALPROFILE_WFS        = 0
       n_totalprofile_wfs_ncld  = 0
       n_totalprofile_wfs_wcld  = 0
    END IF
    
    ! Rayleigh only flag, set if no aerosols and scattering clouds
    VLIDORT_ModIn%MBool%TS_DO_RAYLEIGH_ONLY = .NOT. do_aerosols &
         .AND. .NOT. (do_clouds .AND. .NOT. do_lambertian_cld)
    
    print*,'VLIDORT_ModIn%MBool%TS_DO_RAYLEIGH_ONLY:',VLIDORT_ModIn%MBool%TS_DO_RAYLEIGH_ONLY
    
    ! Rayleigh only, no delta-M scaling, no performance enhancements, nmoms=2
    IF ( VLIDORT_ModIn%MBool%TS_DO_RAYLEIGH_ONLY ) THEN
       VLIDORT_ModIn%MBool%TS_DO_DOUBLE_CONVTEST   = .FALSE.
       VLIDORT_ModIn%MBool%TS_DO_DELTAM_SCALING    = .FALSE.
       VLIDORT_FixIn%Bool%TS_DO_SSCORR_TRUNCATION  = .FALSE. ! CCM 
       VLIDORT_ModIn%MBool%TS_DO_SOLUTION_SAVING   = .FALSE.
       VLIDORT_ModIn%MBool%TS_DO_BVP_TELESCOPING   = .FALSE.
       VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT = 2
    ENDIF
    
    ! Rayleigh+Aerosols, delta-M scaling, performance enhancements, using input moments
    IF ( .not. VLIDORT_ModIn%MBool%TS_DO_RAYLEIGH_ONLY ) THEN
       VLIDORT_ModIn%MBool%TS_DO_DOUBLE_CONVTEST   = .TRUE. ! Change after Xiong settings
       VLIDORT_ModIn%MBool%TS_DO_DELTAM_SCALING    = .TRUE. ! Change after Xiong settings
       VLIDORT_FixIn%Bool%TS_DO_SSCORR_TRUNCATION  = .TRUE. ! CCM 
       VLIDORT_ModIn%MBool%TS_DO_SOLUTION_SAVING   = .FALSE.
       VLIDORT_ModIn%MBool%TS_DO_BVP_TELESCOPING   = .FALSE.
    ENDIF
    
    ! --------------------------
    ! BRDF or Lambertian surface
    ! --------------------------
    !    -- Kernel settings are automatic, but we'll put them here
    !    -- Actual Albedo comes from the data extraction, see later on.
    ! -----------------------------------------------------------------
    VLIDORT_FixIn%Bool%TS_DO_LAMBERTIAN_SURFACE = use_lambertian 
    !   LAMBERTIAN_ALBEDO     = Will be set in Wavelength loop
    ! Security check, if TS_DO_LAMBERTIAN and BS_DO_BRDF_SURFACE are
    ! not compatible, stop program and print message to screen
    IF ( VLIDORT_FixIn%Bool%TS_DO_LAMBERTIAN_SURFACE .EQV. &
         VBRDF_Sup_In%BS_DO_BRDF_SURFACE ) THEN
       WRITE(*,*) 'Or you have a Lambertian Surface or you have '//&
            'BRDF surface but not both. Check input files.'
       STOP
    ENDIF
    
    ! Set the linearization control for surface (albedo) linearization
    IF (VLIDORT_FixIn%Bool%TS_DO_LAMBERTIAN_SURFACE) THEN
       IF ( do_JACOBIANS ) THEN
          VLIDORT_LinModIn%MCont%TS_DO_SURFACE_LINEARIZATION = .TRUE.
          VLIDORT_LinFixIn%Cont%TS_N_SURFACE_WFS = 1
       ELSE
          VLIDORT_LinModIn%MCont%TS_DO_SURFACE_LINEARIZATION = .FALSE.
       END IF
    ELSE
       IF ( DO_JACOBIANS ) THEN
          VLIDORT_LinModIn%MCont%TS_DO_SURFACE_LINEARIZATION = .TRUE.
          VLIDORT_LinFixIn%Cont%TS_N_SURFACE_WFS = VBRDF_LinSup_In%BS_N_SURFACE_WFS
       ELSE
          VLIDORT_LinModIn%MCont%TS_DO_SURFACE_LINEARIZATION = .FALSE.
          VLIDORT_LinFixIn%Cont%TS_N_SURFACE_WFS             = 0
       END IF
    END IF
    
    ! Simulation only settings
    IF ( .NOT. do_JACOBIANS ) THEN
       VLIDORT_LinFixIn%Cont%TS_N_SURFACE_WFS             = 0
       VLIDORT_LinFixIn%Cont%TS_N_TOTALPROFILE_WFS        = 0
       VLIDORT_LinFixIn%Cont%TS_LAYER_VARY_FLAG           = .FALSE.
       VLIDORT_LinFixIn%Cont%TS_LAYER_VARY_NUMBER         = 0
    END IF
    
    IF( do_debug_geocape_tool ) print*,'VL_CONFIG - TS_N_TOTALPROFILE_WFS:',&
                                VLIDORT_LinFixIn%Cont%TS_N_TOTALPROFILE_WFS
    
  END SUBROUTINE Vlidort_GC_config
  
  SUBROUTINE Vlidort_set_optical (error)
   
    USE GC_aerosols_module,  ONLY : interp_aerosol_optics
    USE GC_variables_module, ONLY : fkern_amp_wvl, fkern_par_wvl
    IMPLICIT NONE

    ! ------------------
    ! Modified variables
    ! ------------------
    LOGICAL,       INTENT(INOUT) :: error ! Error variable

    ! ---------------
    ! Local variables
    ! ---------------
    INTEGER                   :: nw1, I, J
    CHARACTER(LEN=max_ch_len) :: tmperrmessage    
    REAL(KIND=8)              :: Al, Bl, Cl, Nl
    
    ! ----------------
    ! Code starts here
    ! ----------------
    error = .FALSE.

    ! -------------------------------------------------
    ! FLUX_FACTOR = solar_cspec_data(nlambdas-w+1)*1.d4
    ! Always apply a Flux Factor here
    ! -------------------------------------------------
    IF (do_normalized_radiance) then
       VLIDORT_FixIn%Sunrays%TS_FLUX_FACTOR = 1.0d0
    ELSE 
       IF (do_effcrs) then
          VLIDORT_FixIn%Sunrays%TS_FLUX_FACTOR = solar_cspec_data(w)
       ELSE
          VLIDORT_FixIn%Sunrays%TS_FLUX_FACTOR = solar_spec_data(w)
       ENDIF
    ENDIF
    
    ! ---------------------------
    ! Rayleigh phase matrix input
    ! ---------------------------
    phasmoms_input = 0.0d0

    depol  = Rayleigh_depols(w)
    beta_2 = (1.0d0 - depol) / (2.0d0 + depol)

!!$    pRay_20 = 1.0d0
    pRay_12 = beta_2
    IF ( VLIDORT_FixIn%Cont%TS_NSTOKES .GT. 1 ) THEN
       pRay_41 = 3.0d0 * (1.0d0 - 2.0d0*depol) / (2.0d0 + depol) 
       pRay_22 = 6.0d0 * beta_2
       pRay_52 = -DSQRT(6.0d0) * beta_2
    END IF
    
    phasmoms_input(0, 1, 1) = 1.0d0   ! beta_0
    phasmoms_input(2, 1, 1) = pRay_12 ! beta_2
    IF (VLIDORT_FixIn%Cont%TS_NSTOKES > 1) THEN
       phasmoms_input(1, 4, 1) = pRay_41 ! delta_1
       phasmoms_input(2, 2, 1) = pRay_22 ! alpha_2
       phasmoms_input(2, 5, 1) = pRay_52 ! gamma_2
    ENDIF
    
    
    ! =======================================
    ! RRS phase matrices
    ! =======================================
    
    ! Based on Landgraf et al. (2004) 
    
    phasmoms_rrs = 0.0d0
    
    ! ------------------------------------
    ! (1) Cabannes line
    ! ------------------------------------
    
    Al = 3.0d0*(180.0d0+epsilon_air(w))
    Bl = 30.0d0*(36.0d0-epsilon_air(w))
    Cl = 36.0d0*epsilon_air(w)
    Nl = 40.0d0*(18.0d0+epsilon_air(w))
    
    phasmoms_rrs(0, 1, 1) = (4.0d0/3.0d0*Al+Cl)/Nl ! beta_0
    phasmoms_rrs(2, 1, 1) =  2.0d0/3.0d0*Al/Nl     ! beta_2
    IF (VLIDORT_FixIn%Cont%TS_NSTOKES > 1) THEN
       phasmoms_rrs(1, 4, 1) = Bl/Nl                   ! delta_1
       phasmoms_rrs(2, 2, 1) = 4.0d0*Al/Nl             ! alpha_2 
       phasmoms_rrs(2, 5, 1) = -DSQRT(8.0d0/3.0d0)*Al/Nl ! gamma_2
    END IF
    
    ! ------------------------------------
    ! (2) RRS
    ! ------------------------------------
    
    Al = 3.0d0
    Bl = -10.0d0
    Cl = 36.0d0
    Nl = 40.0d0
    
    phasmoms_rrs(0, 1, 2) = (4.0d0/3.0d0*Al+Cl)/Nl     ! beta_0
    phasmoms_rrs(2, 1, 2) =  2.0d0/3.0d0*Al/Nl         ! beta_2
    IF (VLIDORT_FixIn%Cont%TS_NSTOKES > 1) THEN
       phasmoms_rrs(1, 4, 2) = Bl/Nl                   ! delta_1
       phasmoms_rrs(2, 2, 2) = 4.0d0*Al/Nl             ! alpha_2 
       phasmoms_rrs(2, 5, 2) = -DSQRT(8.0d0/3.0d0)*Al/Nl ! gamma_2
    END IF
    
    ! ------------------------------------------------------------------
    ! Get aerosol optical properties (use 1 aerosol type for all layers)
    ! ------------------------------------------------------------------
    IF (do_aerosols) THEN
       nw1=1

       CALL interp_aerosol_optics( lambdas(w), GC_nlayers,                                    &
              VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT, ngksec,                            &
              aer_opdeps(w, 1:GC_nlayers), aer_ssalbs(w, 1:GC_nlayers),                       &
              aer_relqext(w, 1:GC_nlayers),                                                   &
              aer_phfcn(1:GC_nlayers,0:VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT,1:ngksec), &
              error                                                                           )

       IF (error) CALL write_err_message (.FALSE., tmperrmessage)
       
    END IF
    
    !------------------------------------------------------------------
    ! Set Surface Optical Properties
    !------------------------------------------------------------------
    
    IF( do_eof_brdf .OR. use_brdf_clim .OR. use_cmglint ) THEN
      
      ! Set wavelength
      VBRDF_Sup_In%BS_WAVELENGTH = lambdas(w)

      ! Set Kernel factor amplitudes
      DO i=1,VBRDF_Sup_In%BS_N_BRDF_KERNELS
        
        ! Non-fixed amplitude cases
        IF( do_eof_brdf .OR. use_brdf_clim  ) THEN
          VBRDF_Sup_In%BS_BRDF_FACTORS(i) = fkern_amp_wvl(w,i)
        ENDIF

        ! Set parameters if we are using the climatology (fixed for MODIS-EOF)
        IF( use_brdf_clim ) THEN
          DO j=1,VBRDF_Sup_In%BS_N_BRDF_PARAMETERS(i)
            VBRDF_Sup_In%BS_BRDF_PARAMETERS(i,j) = fkern_par_wvl(w,i,j)
          ENDDO
        ENDIF
        
      ENDDO
      
      ! Compute kernels at wavelength
      CALL VBRDF_LIN_MAINMASTER (    &
            DO_DEBUG_RESTORATION,    & ! Inputs
            BS_NMOMENTS_INPUT,       & ! Inputs
            VBRDF_Sup_In,            & ! Inputs
            VBRDF_LinSup_In,         & ! Inputs
            VBRDF_Sup_Out,           & ! Outputs
            VBRDF_LinSup_Out,        & ! Outputs
            VBRDF_Sup_OutputStatus )   ! Output Status
      
      ! -------------------------------------------
      ! Copy VBRDF outputs to VLIDORT's BRDF inputs
      ! -------------------------------------------
      CALL VBRDF_TO_VLIDORT(error)
      
      !  Exception handling
      IF ( VBRDF_Sup_OutputStatus%BS_STATUS_OUTPUT .EQ. VLIDORT_SERIOUS ) THEN
        write(*,*)'geocape_tool_v2p6: program failed, VBRDF calculation aborted'
        write(*,*)'Here are the error messages from the VBRDF supplement : - '
        write(*,*)' - Number of error messages = ',VBRDF_Sup_OutputStatus%BS_NOUTPUTMESSAGES
        DO i = 1, VBRDF_Sup_OutputStatus%BS_NOUTPUTMESSAGES
          write(*,*) '  * ',adjustl(Trim(VBRDF_Sup_OutputStatus%BS_OUTPUTMESSAGES(I)))
        ENDDO
      ENDIF
      
    ENDIF
    
    ! Include SIF if requested
    IF( do_sif ) THEN
      CALL VL_UPDATE_SIF( w )
    ENDIF
    
  END SUBROUTINE Vlidort_set_optical
  
  SUBROUTINE Vlidort_update_cldopt_npix( i_pix, error )
    
    USE GC_clouds_module,  ONLY : interp_cloud_optics
    USE VLIDORT_LPS_MASTERS
    USE VLIDORT_AUX

    IMPLICIT NONE
    
    ! ------------------
    ! Modified variables
    ! ------------------
    INTEGER,       INTENT(IN)    :: i_pix
    LOGICAL,       INTENT(INOUT) :: error ! Error variable

    ! ---------------
    ! Local variables
    ! ---------------
    INTEGER                   :: nw1, i
    CHARACTER(LEN=max_ch_len) :: tmperrmessage
    
    
    ! Do 1 wavelength
    nw1 = 1
    
    ! Set the cloud profile for pixel i_pix
    cld_profile(1:ncld, 1:GC_nlayers) = 0.0
    IF( do_clouds ) THEN
      cld_profile(1:ncld, 1:GC_nlayers) = cld_prof_npix(1:ncld, 1:GC_nlayers ,i_pix)
    ENDIF
    
    ! Set cloud flags
    cld_flags(1:GC_nlayers) = .FALSE.
    DO i = 1, ncld
      WHERE (cld_profile(i, 1:GC_nlayers) > 0.0d0)
        cld_flags(1:GC_nlayers) = .TRUE.
      END WHERE
    ENDDO
    
    !print*,'DEBUG CLOUD'
    ! Total optical depth for each layer
    DO i = 1, GC_nlayers
      IF (cld_flags(i)) tcld_profile(i) = tcld_profile(i) + &
                                          SUM(cld_profile(1:ncld, i)) * cpix_cfrac(i_pix)
    END DO
    
    ! Total cloud optical depth for given pixel fraction
    tcldtau0 = SUM(tcld_profile)
    
    ! Prepare optical properties
    CALL interp_cloud_optics( lambdas(w), GC_nlayers,                                       &
            VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT, ngksec,                            &
            cld_opdeps(w, 1:GC_nlayers), cld_ssalbs(w, 1:GC_nlayers),                       &
            cld_relqext(w, 1:GC_nlayers),                                                   &
            cld_phfcn(1:GC_nlayers,0:VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT,1:ngksec), &
            error                                                                           )
    
    

    IF (error) CALL write_err_message (.FALSE., tmperrmessage)
    
  END SUBROUTINE Vlidort_update_cldopt_npix
  
  SUBROUTINE Vlidort_cloud_and_calculation_npix (error)
    
    USE VLIDORT_LPS_MASTERS
    USE VLIDORT_AUX
    USE GC_xsections_module,     ONLY : compute_xsec_vl, compute_dxsecdT_vl
    USE GC_diag_profile_module,  ONLY : archive_prof_pixavg_diags
    USE GC_aerosols_module,      ONLY : interp_aerosol_spc_optics
    IMPLICIT NONE
    
    ! ------------------
    ! Modified variables
    ! ------------------
    LOGICAL,       INTENT(INOUT) :: error ! Error variable

    ! ---------------
    ! Local variables
    ! ---------------
    INTEGER                   :: ic, n, q, g, j, k, ip, jp, nd
    REAL(KIND=8)              :: cfrac_tot, rrs_tot, rrs_frac, dxdT, psrf
    
    ! ----------------------------------------------------------------------------
    ! Array to convert mie code greek moment sign convention to vlidort convention
    ! ----------------------------------------------------------------------------
    INTEGER, PARAMETER, DIMENSION(maxgksec) :: mie_smask = (/1,1,1,1,-1,-1/)
    
    ! ----------------
    ! Code starts here
    ! ----------------
    error = .FALSE.
    
    ! ==============================================================
    !            Loop for clear and cloudy part
    !  Set up cloud dependent vlidort options and optical properties
    !  Weight results based Indepdent Pixel Approximation (IPA)
    ! ==============================================================
    ! ---------------------------------------------------------------------
    ! Initialize output: stokes, profile & surface jacobians at each lambda
    ! ---------------------------------------------------------------------
    stokes_clrcld(1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,1:VLIDORT_Out%Main%TS_N_GEOMETRIES,          &
         1:VLIDORT_FixIn%Cont%TS_NSTOKES, :, :)   = 0.0d0
    stokes_flux(1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,1:VLIDORT_ModIn%MSunrays%TS_N_SZANGLES,        &
         1:VLIDORT_FixIn%Cont%TS_NSTOKES, :, :) = 0.0d0
    stokes_direct_flux(1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,1:VLIDORT_ModIn%MSunrays%TS_N_SZANGLES, &
         1:VLIDORT_FixIn%Cont%TS_NSTOKES, :) = 0.0d0
    profilewf_sum(1:n_totalprofile_wfs_wcld, 1:GC_nlayers, 1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,    &
         1:VLIDORT_Out%Main%TS_N_GEOMETRIES, &
         1:VLIDORT_FixIn%Cont%TS_NSTOKES, :) = 0.0d0
    surfacewf_clrcld(1:VBRDF_LinSup_in%BS_N_SURFACE_WFS, 1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,      &
         1:VLIDORT_Out%Main%TS_N_GEOMETRIES, 1:VLIDORT_FixIn%Cont%TS_NSTOKES, :, :) = 0.0d0
    
    ! Total cloud fraction
    cfrac_tot = SUM( cpix_cfrac(:) )
    
    ! New loop 0 = clear portion
    DO ic=0, n_cpix
      
      ! Set number of layers
      VLIDORT_FixIn%Cont%TS_NLAYERS = GC_nlayers
      
      IF (ic == 0)  THEN     ! Clear
        ipafrac = 1.0d0 - cfrac_tot
      ELSE
        ipafrac = cpix_cfrac(ic)  ! Cloudy part
        IF (do_lambertian_cld) THEN
          !VLIDORT_FixIn%Cont%TS_NLAYERS = cld_uppers_npix(1,ic) - 1
          
          ! New n-pixel implementation
          VLIDORT_FixIn%Cont%TS_NLAYERS = cpix_lam_zlvl(ic)
          
        ELSE
          ! Sets cld_opdeps, cld_ssalbs, cld_phfcn for pixel ic
          IF( do_aerosols .or. do_clouds ) THEN
            CALL Vlidort_update_cldopt_npix(ic, error)
          ENDIF
        ENDIF
      END IF
   
      
      ! Cycle through calculation if the pixel fraction is zero
      IF (ipafrac .LE. 0.0d0) CYCLE
      
      ! ---------------------------
      ! Set lambertian cloud albedo
      ! ---------------------------
      
      IF (ic .GT. 0 .AND. do_lambertian_cld) THEN
          VLIDORT_FixIn%Optical%TS_LAMBERTIAN_ALBEDO  = lambertian_cldalb
          VLIDORT_FixIn%Bool%TS_DO_LAMBERTIAN_SURFACE = .TRUE.
      ELSE
          VLIDORT_FixIn%Optical%TS_LAMBERTIAN_ALBEDO = ground_ler(w)
      END IF
      
      ! ------------------------------------------------------------------
       ! Reset Rayleigh only flag, set if no aerosols and scattering clouds
       ! ------------------------------------------------------------------
       IF (.NOT. do_aerosols .AND. .NOT. do_lambertian_cld) THEN
          IF (ic == 0) THEN
             VLIDORT_ModIn%MBool%TS_DO_RAYLEIGH_ONLY     = .TRUE.
             VLIDORT_ModIn%MBool%TS_DO_DOUBLE_CONVTEST   = .FALSE.
             VLIDORT_ModIn%MBool%TS_DO_DELTAM_SCALING    = .FALSE.
             VLIDORT_ModIn%MBool%TS_DO_SOLUTION_SAVING   = .FALSE.
             VLIDORT_ModIn%MBool%TS_DO_BVP_TELESCOPING   = .FALSE.
             VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT = 2
          ELSE
             VLIDORT_ModIn%MBool%TS_DO_RAYLEIGH_ONLY     = .FALSE.
             VLIDORT_ModIn%MBool%TS_DO_DOUBLE_CONVTEST   = .TRUE.
             VLIDORT_ModIn%MBool%TS_DO_DELTAM_SCALING    = .TRUE.
             VLIDORT_FixIn%Bool%TS_DO_SSCORR_TRUNCATION  = .TRUE.
             VLIDORT_ModIn%MBool%TS_DO_SOLUTION_SAVING   = .FALSE.
             VLIDORT_ModIn%MBool%TS_DO_BVP_TELESCOPING   = .FALSE.
          END IF
       END IF
       
      
       IF (do_Jacobians) THEN
          IF (ic == 0) THEN
             VLIDORT_LinFixIn%Cont%TS_N_TOTALPROFILE_WFS = N_TOTALPROFILE_WFS_ncld
             VLIDORT_LinFixIn%Cont%TS_LAYER_VARY_NUMBER(1:VLIDORT_FixIn%Cont%TS_NLAYERS) = &
                  N_TOTALPROFILE_WFS_ncld
          ELSE
             VLIDORT_LinFixIn%Cont%TS_N_TOTALPROFILE_WFS = N_TOTALPROFILE_WFS_wcld
             VLIDORT_LinFixIn%Cont%TS_LAYER_VARY_NUMBER(1:VLIDORT_FixIn%Cont%TS_NLAYERS) = &
                  N_TOTALPROFILE_WFS_wcld
          END IF
       END IF
      
       ! ----------------------------------------------------------
       ! Here, we just make the Bulk  properties at each wavelength
       ! Need to derive optical properties for ic = 1 or ic = 2 
       ! .and. ipafrac = 1.0) .and. scattering clouds
       ! ----------------------------------------------------------
       
       ! Can't skip for any case now
       IF( .TRUE. ) THEN
       !IF (ic == 0 .OR. (ic .GT. 0 .AND. ipafrac >= 1.0) &
       !     .OR. (ic .gt. 0 .AND. .NOT. do_lambertian_cld) ) THEN
         
          ! ---------------------------------------------------
          ! VLIDORT variable | ** Initialize optical properties
          ! ---------------------------------------------------
          VLIDORT_FixIn%Optical%TS_GREEKMAT_TOTAL_INPUT(0:VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT, &
               1:GC_nlayers, :) = 0.0d0
          VLIDORT_FixIn%Optical%TS_DELTAU_VERT_INPUT(1:GC_nlayers)       = 0.0d0
          VLIDORT_ModIn%MOptical%TS_OMEGA_TOTAL_INPUT(1:GC_nlayers)      = 0.0d0
          
          ! --------------------------------------------------------------
          ! VLIDORT variable | ** Initialize linearized optical properties
          ! --------------------------------------------------------------
          IF (VLIDORT_LinModIn%MCont%TS_DO_ATMOS_LINEARIZATION) THEN
             VLIDORT_LinFixIn%Optical%TS_L_GREEKMAT_TOTAL_INPUT(1:n_totalprofile_wfs_wcld, &
                  0:VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT, 1:GC_nlayers, :)                     = 0.0d0
             VLIDORT_LinFixIn%Optical%TS_L_DELTAU_VERT_INPUT(1:n_totalprofile_wfs_wcld, 1:GC_nlayers) = 0.0d0
             VLIDORT_LinFixIn%Optical%TS_L_OMEGA_TOTAL_INPUT(1:n_totalprofile_wfs_wcld, 1:GC_nlayers) = 0.0d0
          END IF
          
          DO n= 1, GC_nlayers !VLIDORT_FixIn%Cont%TS_NLAYERS
            
             phasmoms_total_input   = 0.0d0
             l_phasmoms_total_input = 0.0d0

             ! -----------------------------------------
             ! Temperature factors for O3 cross-sections
             ! -----------------------------------------
             temp    = mid_temperatures(n) - 273.15d0
             temp_sq = temp * temp
             
             ! ---------------------
             ! Trace gas absorptions
             ! ---------------------
             total_molabs = 0.0d0
             DO g = 1, ngases
                
                ! Zero gas absorption
                gasabs(n,g) = 0.0d0
                
                ! Get absorption cross section for gas
                CALL compute_xsec_vl(w,n,g,xsec)
                
                ! Compute absorption
                gasabs(n,g) = gas_partialcolumns(n,g)*xsec
                
                ! Add to total molecular absorption
                total_molabs = total_molabs + gasabs(n,g)
                
                ! Archive gas xsec
                gas_xsecs(w,n,g) = xsec
                
              ENDDO
            
             ! Save total gas absorption
             total_gasabs(n) = total_molabs
             
             ! ---------------------------------
             ! Rayleigh scattering optical depth
             ! ---------------------------------            
             total_molsca = aircolumns(n) * Rayleigh_xsecs(w)
             
             ! Raman cross section
             rrs_tot = 0.0d0
             IF( do_rrs_phasefunc .or. do_rrs_abs ) THEN
               DO g=1,ngas_rrs
                 rrs_gas(g) = aircolumns(n) * rrs_xsecs(w,n,g)
                 rrs_tot = rrs_tot + rrs_gas(g)
               ENDDO
             ENDIF
             
             ! Split Rayleigh into Cabannes and RRS phase functions
             IF( do_rrs_phasefunc ) THEN
                
                ! To track elastic scatter, treat raman component as an absorption
                IF( do_rrs_abs ) THEN
                  
                  
                  ! Subtract raman component from scattering
                  total_molsca = total_molsca - rrs_tot
                  
                  ! Add RRS component to absorption
                  total_gasabs(n) = total_gasabs(n) + rrs_tot
                  total_molabs    = total_molabs    + rrs_tot
                  
                  ! Phase function for Cabannes line
                  phasmoms_input(0:2, 1:maxgksec,1) = phasmoms_rrs(:,:,1)
                  
                ELSE
                  
                  ! Compute fraction of inelastically scattered photons
                  !rrs_tot  = total_o2rrs + total_n2rrs
                  rrs_frac = rrs_tot / total_molsca
                  
                  ! Reweight the phase function in terms of elastic and inelastic components
                  phasmoms_input(0:2, 1:maxgksec,1) = (1.0d0-rrs_frac)*phasmoms_rrs(:,:,1) &
                                                    + rrs_frac*phasmoms_rrs(:,:,2)
                  
                ENDIF
             ENDIF
             
             ! IF we are on the last layer for the lambertian cloud case
             ! we need to scale by the layer fraction
             IF( do_lambertian_cld .AND. ic .GT. 0 .AND. &
                 n .EQ.  VLIDORT_FixIn%Cont%TS_NLAYERS ) THEN
               
               total_molabs = total_molabs * cpix_lam_lvlfrac(ic)
               total_molsca = total_molsca * cpix_lam_lvlfrac(ic)
               rrs_tot      = rrs_tot      * cpix_lam_lvlfrac(ic)
               
             ENDIF
             
             ! -----------
             ! Total value
             ! -----------
             total_moltau = total_molabs + total_molsca
             
             nscatter = 1
             total_tau = total_moltau
             total_sca = total_molsca
             scaco_input(nscatter) = total_molsca
             
             IF( ALLOCATED(aer_flags) ) THEN
               IF ( aer_flags(n) ) THEN
                  nscatter = nscatter + 1
                  aerscaidx = nscatter
                  total_aertau = aer_opdeps(w, n)
                  
                  ! Check for lambertian clouds
                  IF( do_lambertian_cld .AND. ic .GT. 0 .AND. &
                      n .EQ.  VLIDORT_FixIn%Cont%TS_NLAYERS ) THEN
                    total_aertau = total_aertau * cpix_lam_lvlfrac(ic)
                  ENDIF
                  
                  total_aersca = total_aertau * aer_ssalbs(w, n)
                  total_tau = total_tau + total_aertau
                  total_sca = total_sca + total_aersca
                  scaco_input(nscatter) = total_aersca
                  
                  DO ip=0,VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT
                  DO jp=1,ngksec
                    phasmoms_input(ip, jp, nscatter) = &
                      aer_phfcn(n, ip,jp)*mie_smask(jp)
                  ENDDO
                  ENDDO
                  
               ENDIF
             ENDIF
             ! --------------------------------------------
             ! Only for cloudy parts with scattering clouds
             ! --------------------------------------------
             IF (ALLOCATED(cld_flags)) THEN
             IF ( cld_flags(n) .AND. ic .GT. 0 .AND. .NOT. do_lambertian_cld ) THEN
                nscatter = nscatter + 1
                cldscaidx = nscatter
                total_cldtau = cld_opdeps(w, n) ! CCM Fetch these for individual cases
                total_cldsca = total_cldtau * cld_ssalbs(w, n) ! CCM 
                
                total_tau = total_tau + total_cldtau
                total_sca = total_sca + total_cldsca
                scaco_input(nscatter) = total_cldsca
                
                DO ip=0,VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT
                DO jp=1,ngksec
                  phasmoms_input(ip, jp, nscatter) = &
                     cld_phfcn(n, ip,jp)*mie_smask(jp)
                ENDDO
                ENDDO
                
             END IF
             END IF
             omega = total_sca / total_tau
             opdeps(w, n) = total_tau
             ssalbs(w, n) = omega
             IF (omega .GT. 1.0 - smallnum ) omega = 1.0d0 - smallnum
             IF (omega .LT. smallnum) omega = smallnum
             VLIDORT_ModIn%MOptical%TS_OMEGA_TOTAL_INPUT(n) = omega
             VLIDORT_FixIn%Optical%TS_DELTAU_VERT_INPUT(n)  = total_tau
             
             DO j = 0, VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT
                DO k = 1, ngksec
                   phasmoms_total_input(j, k) = SUM(phasmoms_input(j, k, 1:nscatter) &
                        * scaco_input(1:nscatter)) / total_sca
                END DO
             END DO

             VLIDORT_FixIn%Optical%TS_GREEKMAT_TOTAL_INPUT                                 &
                  (0:VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT, n,                       &
                  greekmat_idxs(1:nactgkmatc)) =                                           &
                  phasmoms_total_input(0:VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT,      &
                  phasmoms_idxs(1:nactgkmatc))
         
             IF ( nactgkmatc > 1 ) THEN

                VLIDORT_FixIn%Optical%TS_GREEKMAT_TOTAL_INPUT                              &
                  (0:VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT, n, 15) =                 &
                  -VLIDORT_FixIn%Optical%TS_GREEKMAT_TOTAL_INPUT(                          &
                  0:VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT, n, 15)
             ENDIF
    
             ! ----------------------------------------------------------------------------------
             ! This should always be 1, but may be slightly different due to numerical truncation
             ! ----------------------------------------------------------------------------------
             VLIDORT_FixIn%Optical%TS_GREEKMAT_TOTAL_INPUT(0, n, 1) = 1.d0  
             
             ! -----------------------------------------------
             ! Set linearized VLIDORT inputs
             ! First Jacobian  ( q = 1 ), use total absorption
             ! -----------------------------------------------
             IF ( do_jacobians ) THEN
                q = gaswfidx
                DO g = 1, ngases
                   IF ( which_gases(g) == 'O3  ' ) abs_o3(n) = gasabs(n,g)
                END DO
                ratio = total_gasabs(n) / total_tau
                VLIDORT_LinFixIn%Optical%TS_L_DELTAU_VERT_INPUT(q,n) =   ratio
                VLIDORT_LinFixIn%Optical%TS_L_OMEGA_TOTAL_INPUT(q,n) = - ratio
             END IF
             
             ! -------------------------
             ! RRS "absorption" jacobian
             ! -------------------------
             IF( do_raman_jacobian .AND. do_rrs_abs ) THEN
                 
               q = rrswfidx
               ratio = rrs_tot / total_tau
               VLIDORT_LinFixIn%Optical%TS_L_DELTAU_VERT_INPUT(q,n) =   ratio
               VLIDORT_LinFixIn%Optical%TS_L_OMEGA_TOTAL_INPUT(q,n) = - ratio
               
             ENDIF
             
             ! ---------------------------------------------------------------
             ! Second Jacobian ( q = 2 ). Temperature. Only if flagged.
             ! L_gas is the contribution from the T-dependent O3 cross-section
             ! L_air is the contribution from the Air columns
             ! ---------------------------------------------------------------
             
             IF ( do_T_jacobians ) THEN
                
                ! Since only w.r.t absorption now we don't need to compute another Jacobian
                total_dgasabsdT(n) = 0.0
                
                DO g=1,ngases
                  
                  CALL compute_dxsecdT_vl(w,n,g,dxsec_dT )
                  
                  total_dgasabsdT(n) = total_dgasabsdT(n) &
                                     + gas_partialcolumns(n,g)*dxsec_dT
                  
                  IF( write_gas_xsecs .and. write_xsec_deriv ) THEN
                    
                    gas_xsecs_deriv(w,n,g) = dxsec_dT
                    
                  ENDIF
                  
                ENDDO
                
             END IF ! End Temperature Jacobians IF
             
             
             
             
             ! ---------------------------------------------------------------------------------
             ! Surface Pressure Jacobian
             ! ---------------------------------------------------------------------------------
             IF( do_sfcprs_Jacobians ) THEN
               
               q = sfcprswfidx
               
               ! -------------------------------------------------------------------------------
               ! New "OCO2" Surface Pressure Calculation - Consider all molecular abs/scattering
               ! -------------------------------------------------------------------------------
               IF( do_oco2_sfcprs ) THEN
               
                
                ! Here we use VL to compute the derivative w.r.t gas "optical depth" C.F. AOD 
                ! Apply chain rule in the diagnostic module
                
                ! Delta
                VLIDORT_LinFixIn%Optical%TS_L_DELTAU_VERT_INPUT(q, n) = &
                      + total_moltau / total_tau
                
                ! Omega
                VLIDORT_LinFixIn%Optical%TS_L_OMEGA_TOTAL_INPUT(q, n) = &
                     (total_molsca / total_moltau - VLIDORT_ModIn%MOptical%TS_OMEGA_TOTAL_INPUT(n)) &
                     / VLIDORT_ModIn%MOptical%TS_OMEGA_TOTAL_INPUT(n)  * total_moltau / total_tau
                
                ! Zero Psi
                VLIDORT_LinFixIn%Optical%TS_L_GREEKMAT_TOTAL_INPUT(q,   &
                     0:VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT, n, :) = 0.0d0
                
                ! Compute Psi
                DO j = 0,VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT
                DO k = 1, ngksec
                  IF (phasmoms_total_input(j, k) /= 0.d0) THEN
                    l_phasmoms_total_input(q, j, k) = ( phasmoms_input(j, k, 1)      &
                         - phasmoms_total_input(j, k) ) / phasmoms_total_input(j, k) &
                         * total_molsca / total_sca
                  ELSE
                    l_phasmoms_total_input(q, j, k) = 0.0d0
                  ENDIF
                ENDDO
                ENDDO
                
                ! Add Psi to VLIDORT linearized inputs
                VLIDORT_LinFixIn%Optical%TS_L_GREEKMAT_TOTAL_INPUT(q,                         &
                     0:VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT, n,                        &
                     greekmat_idxs(1:nactgkmatc)) =                                           &
                     l_phasmoms_total_input(q, 0:VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT, &
                     phasmoms_idxs(1:nactgkmatc))
                IF ( nactgkmatc > 1 )  THEN
                   VLIDORT_LinFixIn%Optical%TS_L_GREEKMAT_TOTAL_INPUT(q,        &
                     0:VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT, n, 15)      &
                     = -VLIDORT_LinFixIn%Optical%TS_L_GREEKMAT_TOTAL_INPUT(q,   &
                     0:VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT, n, 15)
                ENDIF
                
              ! ------------------------------------------------------------
              ! Ground surface pressure Jacobians, only if flagged
              ! Rayleigh only, decouple Rayleigh from gases, aerosol, clouds
              ! ------------------------------------------------------------
              ELSE
             
                IF ( n == GC_nlayers ) THEN
                    pvar = total_molsca / total_tau
                    VLIDORT_LinFixIn%Optical%TS_L_OMEGA_TOTAL_INPUT(q, n) =     &
                        (1.0 - VLIDORT_ModIn%MOptical%TS_OMEGA_TOTAL_INPUT(n)) &
                        * total_molsca / total_tau / VLIDORT_ModIn%MOptical%TS_OMEGA_TOTAL_INPUT(n)
                    VLIDORT_LinFixIn%Optical%TS_L_DELTAU_VERT_INPUT(q, n) = pvar
                    VLIDORT_LinFixIn%Optical%TS_L_GREEKMAT_TOTAL_INPUT(q,   &
                        0:VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT, n, :) = 0.0d0
                    DO j = 0, VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT
                    DO k = 1, ngksec
                      IF (phasmoms_total_input(j, k) /= 0.0) THEN
                        l_phasmoms_total_input(q, j, k) = (phasmoms_input(j, k, 1) -  &
                             phasmoms_total_input(j, k)) / phasmoms_total_input(j, k) &
                             * total_molsca / total_sca
                      ELSE
                        l_phasmoms_total_input(q, j, k) = 0.0
                      ENDIF
                    ENDDO
                    ENDDO
                  ENDIF
                  
                ENDIF
                
             ENDIF ! do_sfcprs_jacobians
             
             ! ------------------------------------------
             ! Aerosol optical thickness, only if flagged
             ! ------------------------------------------
             IF( ALLOCATED(aer_flags) ) THEN
             IF (do_aod_Jacobians .AND. aer_flags(n) ) THEN
                
                IF( do_total_aod_jac ) THEN
                  
                  q = aodwfidx
                  
                  VLIDORT_LinFixIn%Optical%TS_L_DELTAU_VERT_INPUT(q, n) = &
                      + total_aertau / total_tau
                  VLIDORT_LinFixIn%Optical%TS_L_OMEGA_TOTAL_INPUT(q, n) = &
                      (total_aersca / total_aertau - VLIDORT_ModIn%MOptical%TS_OMEGA_TOTAL_INPUT(n)) &
                      / VLIDORT_ModIn%MOptical%TS_OMEGA_TOTAL_INPUT(n)  * total_aertau / total_tau
                  VLIDORT_LinFixIn%Optical%TS_L_GREEKMAT_TOTAL_INPUT(q,   &
                      0:VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT, n, :) = 0.0d0
                  
                  DO j = 0, VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT
                    DO k = 1, ngksec
                        IF (phasmoms_total_input(j, k) /= 0.d0) THEN
                          l_phasmoms_total_input(q, j, k) = ( phasmoms_input(j, k, aerscaidx) &
                                - phasmoms_total_input(j, k) ) / phasmoms_total_input(j, k) &
                                * total_aersca / total_sca
                        ELSE
                          l_phasmoms_total_input(q, j, k) = 0.0d0
                        END IF
                    END DO
                  END DO
                  
                  VLIDORT_LinFixIn%Optical%TS_L_GREEKMAT_TOTAL_INPUT(q,                         &
                      0:VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT, n,                        &
                      greekmat_idxs(1:nactgkmatc)) =                                           &
                      l_phasmoms_total_input(q, 0:VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT, &
                      phasmoms_idxs(1:nactgkmatc))
                  
                IF ( nactgkmatc > 1 )  THEN

                    VLIDORT_LinFixIn%Optical%TS_L_GREEKMAT_TOTAL_INPUT(q, &
                      0:VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT, n, 15)                   &
                      = -VLIDORT_LinFixIn%Optical%TS_L_GREEKMAT_TOTAL_INPUT(q,                &
                      0:VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT, n, 15)
                ENDIF
                
              ENDIF
              
              
              ! Species specific AOD
              DO nd=1,Diag34_InpOpt%njac
                
                ! Jacobian index
                q = Diag34_InpOpt%wfidx(nd)
                
                ! Interpolate aerosol species optical properties
                CALL interp_aerosol_spc_optics( lambdas(w), Diag34_InpOpt%jac_idx(nd), n,          &
                  VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT,ngksec,                              &
                  aerdiag_tau, aerdiag_ssa, aerdiag_qext,                                          &
                  aerdiag_phasemoms(0:VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT,1:ngksec), error )
                
                ! Set linear inputs
                VLIDORT_LinFixIn%Optical%TS_L_DELTAU_VERT_INPUT(q, n) = aerdiag_tau / total_tau
                VLIDORT_LinFixIn%Optical%TS_L_OMEGA_TOTAL_INPUT(q, n) = &
                  ( aerdiag_ssa - VLIDORT_ModIn%MOptical%TS_OMEGA_TOTAL_INPUT(n) )/ &
                  VLIDORT_ModIn%MOptical%TS_OMEGA_TOTAL_INPUT(n)  * aerdiag_tau / total_tau
                
                ! Compute Phase moments
                VLIDORT_LinFixIn%Optical%TS_L_GREEKMAT_TOTAL_INPUT(q,   &
                      0:VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT, n, :) = 0.0d0
                DO j = 0, VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT
                DO k = 1, ngksec
                  IF(phasmoms_total_input(j, k) /= 0.d0) THEN
                    l_phasmoms_total_input(q, j, k) = ( aerdiag_phasemoms(j,k)    &
                      - phasmoms_total_input(j, k) ) / phasmoms_total_input(j, k) &
                      * aerdiag_tau / total_sca
                  ELSE
                    l_phasmoms_total_input(q, j, k) = 0.0d0
                  ENDIF
                ENDDO
                ENDDO
                
                ! Set phase moments
                VLIDORT_LinFixIn%Optical%TS_L_GREEKMAT_TOTAL_INPUT(q,                          &
                      0:VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT, n,                        &
                      greekmat_idxs(1:nactgkmatc)) =                                           &
                      l_phasmoms_total_input(q, 0:VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT, &
                      phasmoms_idxs(1:nactgkmatc))
                IF ( nactgkmatc > 1 )  THEN
                  VLIDORT_LinFixIn%Optical%TS_L_GREEKMAT_TOTAL_INPUT(q,        &
                    0:VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT, n, 15)      &
                      = -VLIDORT_LinFixIn%Optical%TS_L_GREEKMAT_TOTAL_INPUT(q, &
                      0:VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT, n, 15)
                ENDIF
                
              ENDDO

             ENDIF
             ENDIF
             
             ! ------------------------------------------------------
             ! Aerosol SSA thickness, only if flagged (AOD unchanged)
             ! ------------------------------------------------------
             IF( ALLOCATED(aer_flags) ) THEN
             IF (do_assa_Jacobians .AND. aer_flags(n)) THEN
                
                IF( do_total_assa_jac ) THEN
                
                  q = assawfidx
                  VLIDORT_LinFixIn%Optical%TS_L_DELTAU_VERT_INPUT(q,n) = 0.0d0
                  VLIDORT_LinFixIn%Optical%TS_L_OMEGA_TOTAL_INPUT(q,n) = total_aersca / &
                      VLIDORT_ModIn%MOptical%TS_OMEGA_TOTAL_INPUT(n) / total_tau
                  VLIDORT_LinFixIn%Optical%TS_L_GREEKMAT_TOTAL_INPUT(q, &
                      0:VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT, n, :) = 0.0d0
                  DO j = 0, VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT
                    DO k = 1, ngksec
                        IF (phasmoms_total_input(j, k) /= 0.d0) THEN
                          l_phasmoms_total_input(q, j, k) = ( phasmoms_input(j, k, aerscaidx) &
                                - phasmoms_total_input(j, k) ) / phasmoms_total_input(j, k) &
                                * total_aersca / total_sca
                        ELSE
                          l_phasmoms_total_input(q, j, k) = 0.d0
                        END IF
                    END DO
                  END DO

                  VLIDORT_LinFixIn%Optical%TS_L_GREEKMAT_TOTAL_INPUT(q,                                 &
                      0:VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT, n, greekmat_idxs(1:nactgkmatc)) = &
                      l_phasmoms_total_input(q, 0:VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT,         &
                      phasmoms_idxs(1:nactgkmatc))

                  IF ( nactgkmatc > 1 )  THEN

                    VLIDORT_LinFixIn%Optical%TS_L_GREEKMAT_TOTAL_INPUT(q, &
                      0:VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT, n, 15)                   &
                      = - VLIDORT_LinFixIn%Optical%TS_L_GREEKMAT_TOTAL_INPUT(q,               &
                      0:VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT, n, 15)
                  ENDIF
                ENDIF
                
                ! Species specific ASSA
                DO nd=1,Diag35_InpOpt%njac
                  
                  ! Jacobian Index
                  q = Diag35_InpOpt%wfidx(nd)
                  
                  ! Interpolate aerosol species optical properties
                  CALL interp_aerosol_spc_optics( lambdas(w), Diag35_InpOpt%jac_idx(nd), n,          &
                    VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT,ngksec,                              &
                    aerdiag_tau, aerdiag_ssa, aerdiag_qext,                                          &
                    aerdiag_phasemoms(0:VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT,1:ngksec), error )
                  
                  ! Set linear inputs
                  VLIDORT_LinFixIn%Optical%TS_L_DELTAU_VERT_INPUT(q, n) = 0.0d0
                  VLIDORT_LinFixIn%Optical%TS_L_OMEGA_TOTAL_INPUT(q, n) = aerdiag_tau*aerdiag_ssa / &
                    VLIDORT_ModIn%MOptical%TS_OMEGA_TOTAL_INPUT(n) / total_tau
                  VLIDORT_LinFixIn%Optical%TS_L_GREEKMAT_TOTAL_INPUT(q, &
                      0:VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT, n, :) = 0.0d0
                  
                  ! Compute phase moments
                  DO j = 0, VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT
                  DO k = 1, ngksec
                    IF(phasmoms_total_input(j, k) /= 0.d0) THEN
                      l_phasmoms_total_input(q, j, k) = ( aerdiag_phasemoms(j,k) &
                              - phasmoms_total_input(j, k) ) / phasmoms_total_input(j, k) &
                              * aerdiag_tau*aerdiag_ssa  / total_sca
                    ELSE
                      l_phasmoms_total_input(q, j, k) = 0.0d0
                    ENDIF
                  ENDDO
                  ENDDO
                  
                  ! Set phase moments
                  VLIDORT_LinFixIn%Optical%TS_L_GREEKMAT_TOTAL_INPUT(q,                          &
                        0:VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT, n,                        &
                        greekmat_idxs(1:nactgkmatc)) =                                           &
                        l_phasmoms_total_input(q, 0:VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT, &
                        phasmoms_idxs(1:nactgkmatc))
                  IF ( nactgkmatc > 1 )  THEN
                    VLIDORT_LinFixIn%Optical%TS_L_GREEKMAT_TOTAL_INPUT(q,        &
                      0:VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT, n, 15)      &
                        = -VLIDORT_LinFixIn%Optical%TS_L_GREEKMAT_TOTAL_INPUT(q, &
                        0:VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT, n, 15)
                  ENDIF
                  
                ENDDO
                
             ENDIF
             ENDIF
             
             ! ----------------------------------------
             ! Cloud optical thickness, only if flagged
             ! ----------------------------------------
             IF( ALLOCATED(cld_flags) ) THEN
             IF (do_cod_Jacobians .AND. cld_flags(n) .AND. ic .GT. 0 ) THEN
                
                ! COD weight function index
                q = codwfidx
                
                VLIDORT_LinFixIn%Optical%TS_L_DELTAU_VERT_INPUT(q, n) = + total_cldtau / total_tau
                VLIDORT_LinFixIn%Optical%TS_L_OMEGA_TOTAL_INPUT(q, n) = (total_cldsca / total_cldtau &
                     - VLIDORT_ModIn%MOptical%TS_OMEGA_TOTAL_INPUT(n))                               &
                     / VLIDORT_ModIn%MOptical%TS_OMEGA_TOTAL_INPUT(n) * total_cldtau / total_tau
                VLIDORT_LinFixIn%Optical%TS_L_GREEKMAT_TOTAL_INPUT(q, &
                     0:VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT, n, :) = 0.0d0
                DO j = 0, VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT
                   DO k = 1, ngksec
                      IF (phasmoms_total_input(j, k) /= 0.d0) THEN
                         l_phasmoms_total_input(q, j, k) = ( phasmoms_input(j, k, cldscaidx) &
                              - phasmoms_total_input(j, k) ) / phasmoms_total_input(j, k)    &
                              * total_cldsca / total_sca
                      ELSE
                         l_phasmoms_total_input(q, j, k) = 0.d0
                      END IF
                   END DO
                END DO
                VLIDORT_LinFixIn%Optical%TS_L_GREEKMAT_TOTAL_INPUT(q,                         &
                     0:VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT, n,                        &
                     greekmat_idxs(1:nactgkmatc)) =                                           &
                     l_phasmoms_total_input(q, 0:VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT, &
                     phasmoms_idxs(1:nactgkmatc))

                IF ( nactgkmatc > 1 ) THEN

                   VLIDORT_LinFixIn%Optical%TS_L_GREEKMAT_TOTAL_INPUT(q,  &
                     0:VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT, n, 15)                    &
                     = -VLIDORT_LinFixIn%Optical%TS_L_GREEKMAT_TOTAL_INPUT(q,                 &
                     0:VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT, n, 15)
                ENDIF
                
             ENDIF
             ENDIF
             
             ! ----------------------------------------------------
             ! Cloud SSA thickness, only if flagged (AOD unchanged)
             ! ----------------------------------------------------
             IF( ALLOCATED(cld_flags) ) THEN
             IF (do_cssa_Jacobians .AND. cld_flags(n) .AND. ic .GT. 0 .AND. &
                 .NOT. do_lambertian_cld                                    ) THEN
                q = cssawfidx
                VLIDORT_LinFixIn%Optical%TS_L_DELTAU_VERT_INPUT(q,n) = 0.0d0
                VLIDORT_LinFixIn%Optical%TS_L_OMEGA_TOTAL_INPUT(q,n) = total_cldsca / &
                     VLIDORT_ModIn%MOptical%TS_OMEGA_TOTAL_INPUT(n) / total_tau
                VLIDORT_LinFixIn%Optical%TS_L_GREEKMAT_TOTAL_INPUT(q, &
                     0:VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT, n, :) = 0.0d0
                DO j = 0, VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT
                   DO k = 1, ngksec
                      IF (phasmoms_total_input(j, k) /= 0.d0) THEN
                         l_phasmoms_total_input(q, j, k) = ( phasmoms_input(j, k, cldscaidx) &
                              - phasmoms_total_input(j, k) ) / phasmoms_total_input(j, k) &
                              * total_cldsca / total_sca
                      ELSE
                         l_phasmoms_total_input(q, j, k) = 0.d0
                      END IF
                   END DO
                END DO
                VLIDORT_LinFixIn%Optical%TS_L_GREEKMAT_TOTAL_INPUT(q, &
                     0:VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT, n,&
                     greekmat_idxs(1:nactgkmatc)) =                   &
                     l_phasmoms_total_input(q,                        &
                     0:VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT,   &
                     phasmoms_idxs(1:nactgkmatc))

                IF ( nactgkmatc > 1 ) THEN

                   VLIDORT_LinFixIn%Optical%TS_L_GREEKMAT_TOTAL_INPUT(q, &
                     0:VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT, n, 15)                   &
                     = - VLIDORT_LinFixIn%Optical%TS_L_GREEKMAT_TOTAL_INPUT(q,               &
                     0:VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT, n, 15)
                ENDIF

             ENDIF
             ENDIF
             
             VLIDORT_LinFixIn%Optical%TS_L_GREEKMAT_TOTAL_INPUT(:, 0, n, 1) = 0.d0
            
          ENDDO ! End of layer loop
          
       END IF
          
       !  Debug optical properties
       
       if ( do_debug_geocape_tool ) then
          write(du,'(/a,i4,a, i4)')'Debug optical properties for wavelength # ',w, ' ic = ', ic
          if ( do_Jacobians ) then
             total_tau = 0.0d0
             do n = 1, VLIDORT_FixIn%Cont%TS_NLAYERS
                total_tau = total_tau + VLIDORT_FixIn%Optical%TS_DELTAU_VERT_INPUT(n)
                write(du,'(i3,12(1pe20.10))') n, VLIDORT_FixIn%Optical%TS_DELTAU_VERT_INPUT(n), &
                     VLIDORT_ModIn%MOptical%TS_OMEGA_TOTAL_INPUT(n),                          &
                     VLIDORT_FixIn%Optical%TS_GREEKMAT_TOTAL_INPUT(1,n,1),                    &
                     VLIDORT_FixIn%Optical%TS_GREEKMAT_TOTAL_INPUT(2,n,1),                    &
                     VLIDORT_LinFixIn%Optical%TS_L_DELTAU_VERT_INPUT(1,n),                    &
                     VLIDORT_LinFixIn%Optical%TS_L_OMEGA_TOTAL_INPUT(1,n),                    & 
                     VLIDORT_LinFixIn%Optical%TS_L_DELTAU_VERT_INPUT(2,n),                    &
                     VLIDORT_LinFixIn%Optical%TS_L_OMEGA_TOTAL_INPUT(2,n),                    &
                     VLIDORT_LinFixIn%Optical%TS_L_GREEKMAT_TOTAL_INPUT(1,1,n,1),             &
                     VLIDORT_LinFixIn%Optical%TS_L_GREEKMAT_TOTAL_INPUT(2,1,n,1),             &
                     VLIDORT_LinFixIn%Optical%TS_L_GREEKMAT_TOTAL_INPUT(1,2,n,1),             &
                     VLIDORT_LinFixIn%Optical%TS_L_GREEKMAT_TOTAL_INPUT(2,2,n,1)

             enddo
             write(du,'(a,f10.5)')'--------Total optical depth = ',total_tau
          else
             total_tau = 0.0d0
             do n = 1, VLIDORT_FixIn%Cont%TS_NLAYERS
                total_tau = total_tau + VLIDORT_FixIn%Optical%TS_DELTAU_VERT_INPUT(n)
                write(du,'(i3,1x,4e20.10)')n,VLIDORT_FixIn%Optical%TS_DELTAU_VERT_INPUT(n), &
                     VLIDORT_ModIn%MOptical%TS_OMEGA_TOTAL_INPUT(n),                       &
                     VLIDORT_FixIn%Optical%TS_GREEKMAT_TOTAL_INPUT(1,n,1),                 &
                     VLIDORT_FixIn%Optical%TS_GREEKMAT_TOTAL_INPUT(2,n,1)
             enddo
             write(du,'(a,f10.5)')'--------Total optical depth = ',total_tau
          endif
       endif
       
       ! ------------
       ! VLIDORT call
       ! ------------
       ! ----------------
       ! Now call VLIDORT
       ! -----------------
       IF(do_debug_geocape_tool) WRITE(*,*)'doing VLIDORT calculation (npix) # ', w, ic
       CALL VLIDORT_LPS_MASTER ( &
            VLIDORT_FixIn,     &
            VLIDORT_ModIn,     &
            VLIDORT_Sup,       &
            VLIDORT_Out,       &
            VLIDORT_LinFixIn,  &
            VLIDORT_LinModIn,  &
            VLIDORT_LinSup,    &
            VLIDORT_LinOut )

       ! ------------------
       ! Exception handling
       ! ------------------
       OPENERRORFILEFLAG = .FALSE.
       CALL VLIDORT_WRITE_STATUS (            &
            'V2p7_VLIDORT_LPS_Execution.log', &
            35, OPENERRORFILEFLAG,            &
            VLIDORT_Out%Status )

       ! --------------
       ! Stop if failed
       ! --------------
       IF ( VLIDORT_Out%Status%TS_STATUS_INPUTCHECK .EQ. VLIDORT_SERIOUS ) THEN
          WRITE(*,*)'VLIDORT input abort, thread number = ', w
          STOP
       ELSE IF ( VLIDORT_Out%Status%TS_STATUS_INPUTCHECK .NE. VLIDORT_SERIOUS .AND. &
            VLIDORT_Out%Status%TS_STATUS_CALCULATION .EQ. VLIDORT_SERIOUS ) THEN
          WRITE(*,*)'VLIDORT calculation abort, thread number = ', w
          STOP
       END IF
       
       IF(ic == 0) THEN
       
          ! --------------
          ! Copy radiances
          ! --------------
          stokes_clrcld(1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,1:VLIDORT_Out%Main%TS_N_GEOMETRIES, &
                1:VLIDORT_FixIn%Cont%TS_NSTOKES, 1:ndir,1) = &
                VLIDORT_Out%Main%TS_STOKES(1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS, &
                1:VLIDORT_Out%Main%TS_N_GEOMETRIES, 1:VLIDORT_FixIn%Cont%TS_NSTOKES, 1:ndir)
          
          ! ---------
          ! Copy flux
          ! ---------
          stokes_flux(1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,1:VLIDORT_ModIn%MSunrays%TS_N_SZANGLES, &
                1:VLIDORT_FixIn%Cont%TS_NSTOKES, 1:ndir, 1) = &
                VLIDORT_Out%Main%TS_FLUX_STOKES(1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS, &
                1:VLIDORT_ModIn%MSunrays%TS_N_SZANGLES, 1:VLIDORT_FixIn%Cont%TS_NSTOKES, 1:ndir)
          
          ! -----------------------------------
          ! Copy direct flux (only downwelling)
          ! -----------------------------------
          stokes_direct_flux(1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,1:VLIDORT_ModIn%MSunrays%TS_N_SZANGLES, &
                1:VLIDORT_FixIn%Cont%TS_NSTOKES, 1) = &
                VLIDORT_Out%Main%TS_FLUX_DIRECT(1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS, &
                1:VLIDORT_ModIn%MSunrays%TS_N_SZANGLES, &
                1:VLIDORT_FixIn%Cont%TS_NSTOKES)
       
       ELSE
          
          ! Need to sum over the cloudy pixels
          
          ! --------------
          ! Copy radiances
          ! --------------
          stokes_clrcld(1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,1:VLIDORT_Out%Main%TS_N_GEOMETRIES, &
                1:VLIDORT_FixIn%Cont%TS_NSTOKES, 1:ndir,2) = &
                stokes_clrcld(1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,1:VLIDORT_Out%Main%TS_N_GEOMETRIES, &
                1:VLIDORT_FixIn%Cont%TS_NSTOKES, 1:ndir,2) + &
                VLIDORT_Out%Main%TS_STOKES(1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS, &
                1:VLIDORT_Out%Main%TS_N_GEOMETRIES, 1:VLIDORT_FixIn%Cont%TS_NSTOKES, 1:ndir) * ipafrac
          
          ! ---------
          ! Copy flux
          ! ---------
          stokes_flux(1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,1:VLIDORT_ModIn%MSunrays%TS_N_SZANGLES, &
                1:VLIDORT_FixIn%Cont%TS_NSTOKES, 1:ndir, 2) = &
                stokes_flux(1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,1:VLIDORT_ModIn%MSunrays%TS_N_SZANGLES, &
                1:VLIDORT_FixIn%Cont%TS_NSTOKES, 1:ndir, 2) + &
                VLIDORT_Out%Main%TS_FLUX_STOKES(1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS, &
                1:VLIDORT_ModIn%MSunrays%TS_N_SZANGLES, 1:VLIDORT_FixIn%Cont%TS_NSTOKES, 1:ndir) * ipafrac

          ! -----------------------------------
          ! Copy direct flux (only downwelling)
          ! -----------------------------------
          stokes_direct_flux(1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,1:VLIDORT_ModIn%MSunrays%TS_N_SZANGLES, &
                1:VLIDORT_FixIn%Cont%TS_NSTOKES, 2) = &
                stokes_direct_flux(1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,1:VLIDORT_ModIn%MSunrays%TS_N_SZANGLES, &
                1:VLIDORT_FixIn%Cont%TS_NSTOKES, 2) + &
                VLIDORT_Out%Main%TS_FLUX_DIRECT(1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS, &
                1:VLIDORT_ModIn%MSunrays%TS_N_SZANGLES, &
                1:VLIDORT_FixIn%Cont%TS_NSTOKES) * ipafrac
          
       ENDIF
       
       IF (do_jacobians) THEN
          
          profilewf_sum(1:VLIDORT_LinFixIn%Cont%TS_N_TOTALPROFILE_WFS,      &
               1:VLIDORT_FixIn%Cont%TS_NLAYERS,                             &
               1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,                    &
               1:VLIDORT_Out%Main%TS_N_GEOMETRIES,                          &
               1:VLIDORT_FixIn%Cont%TS_NSTOKES, 1:ndir) =                   &
               profilewf_sum(1:VLIDORT_LinFixIn%Cont%TS_N_TOTALPROFILE_WFS, &
               1:VLIDORT_FixIn%Cont%TS_NLAYERS,                             &
               1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,                    &
               1:VLIDORT_Out%Main%TS_N_GEOMETRIES,                          &
               1:VLIDORT_FixIn%Cont%TS_NSTOKES, 1:ndir) +                   &
               VLIDORT_LinOut%Prof%TS_PROFILEWF                             &
               (1:VLIDORT_LinFixIn%Cont%TS_N_TOTALPROFILE_WFS,              &
               1:VLIDORT_FixIn%Cont%TS_NLAYERS,                             &
               1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,                    &
               1:VLIDORT_Out%Main%TS_N_GEOMETRIES,                          &
               1:VLIDORT_FixIn%Cont%TS_NSTOKES, 1:ndir)*ipafrac
          
          IF( ic == 0 ) THEN
            surfacewf_clrcld(1:VLIDORT_LinFixIn%Cont%TS_N_SURFACE_WFS,       &
                1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,                    &
                1:VLIDORT_Out%Main%TS_N_GEOMETRIES,                          &
                1:VLIDORT_FixIn%Cont%TS_NSTOKES, 1:ndir, 1) =                &
                VLIDORT_LinOut%Surf%TS_SURFACEWF                             &
                (1:VLIDORT_LinFixIn%Cont%TS_N_SURFACE_WFS,                   &
                1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,                    &
                1:VLIDORT_Out%Main%TS_N_GEOMETRIES,                          &
                1:VLIDORT_FixIn%Cont%TS_NSTOKES, 1:ndir)
          ELSE
            surfacewf_clrcld(1:VLIDORT_LinFixIn%Cont%TS_N_SURFACE_WFS,       &
                1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,                    &
                1:VLIDORT_Out%Main%TS_N_GEOMETRIES,                          &
                1:VLIDORT_FixIn%Cont%TS_NSTOKES, 1:ndir, 2) =                &
                surfacewf_clrcld(1:VLIDORT_LinFixIn%Cont%TS_N_SURFACE_WFS,   &
                1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,                    &
                1:VLIDORT_Out%Main%TS_N_GEOMETRIES,                          &
                1:VLIDORT_FixIn%Cont%TS_NSTOKES, 1:ndir, 2) +                &
                VLIDORT_LinOut%Surf%TS_SURFACEWF                             &
                (1:VLIDORT_LinFixIn%Cont%TS_N_SURFACE_WFS,                   &
                1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,                    &
                1:VLIDORT_Out%Main%TS_N_GEOMETRIES,                          &
                1:VLIDORT_FixIn%Cont%TS_NSTOKES, 1:ndir) * ipafrac
          ENDIF

       END IF
       
       ! Archive pixel average profile results
       CALL archive_prof_pixavg_diags( w, ipafrac )
       
    ENDDO ! Cloud fraction loop
    
    VLIDORT_FixIn%Cont%TS_NLAYERS = GC_nlayers  ! use the most # of layers
    
    ! ------------------------------------------------------------------
    ! Independent pixel approximation for radiance, flux and direct flux
    ! !------------------------------------------------------------------
    VLIDORT_Out%Main%TS_STOKES(1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS, &
         1:VLIDORT_Out%Main%TS_N_GEOMETRIES,                             &
         1:VLIDORT_FixIn%Cont%TS_NSTOKES, 1:ndir) =                      &
         stokes_clrcld(1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,           &
         1:VLIDORT_Out%Main%TS_N_GEOMETRIES,                               &
         1:VLIDORT_FixIn%Cont%TS_NSTOKES, 1:ndir, 1) * (1.0 - cfrac_tot )  &
         + stokes_clrcld(1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,         &
         1:VLIDORT_Out%Main%TS_N_GEOMETRIES,                               &
         1:VLIDORT_FixIn%Cont%TS_NSTOKES, 1:ndir, 2) * cfrac_tot

    VLIDORT_Out%Main%TS_FLUX_STOKES(1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS, &
         1:VLIDORT_ModIn%MSunrays%TS_N_SZANGLES,                              &
         1:VLIDORT_FixIn%Cont%TS_NSTOKES, 1:ndir) =                           &
         stokes_flux(1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,         &
         1:VLIDORT_ModIn%MSunrays%TS_N_SZANGLES,                       &
         1:VLIDORT_FixIn%Cont%TS_NSTOKES, 1:ndir, 1) * (1.0 - cfrac_tot )  &
         + stokes_flux(1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,       &
         1:VLIDORT_ModIn%MSunrays%TS_N_SZANGLES,                       &
         1:VLIDORT_FixIn%Cont%TS_NSTOKES, 1:ndir, 2) * cfrac_tot

    VLIDORT_Out%Main%TS_FLUX_DIRECT(1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS, &
         1:VLIDORT_ModIn%MSunrays%TS_N_SZANGLES,                              &
         1:VLIDORT_FixIn%Cont%TS_NSTOKES) =                                   &
         stokes_direct_flux(1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,   &
         1:VLIDORT_ModIn%MSunrays%TS_N_SZANGLES,                        &
         1:VLIDORT_FixIn%Cont%TS_NSTOKES, 1) * (1.0 - cfrac_tot )       &
         + stokes_direct_flux(1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS, &
         1:VLIDORT_ModIn%MSunrays%TS_N_SZANGLES,                        &
         1:VLIDORT_FixIn%Cont%TS_NSTOKES, 2) * cfrac_tot
    
    IF (do_jacobians) THEN
       VLIDORT_LinOut%Prof%TS_PROFILEWF(1:VLIDORT_LinFixIn%Cont%TS_N_TOTALPROFILE_WFS, &
            1:GC_nlayers, 1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,                    &
            1:VLIDORT_Out%Main%TS_N_GEOMETRIES,                                        &
            1:VLIDORT_FixIn%Cont%TS_NSTOKES, 1:ndir) =                                 &
            profilewf_sum(1:VLIDORT_LinFixIn%Cont%TS_N_TOTALPROFILE_WFS, 1:GC_nlayers, &
            1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,                                  &
            1:VLIDORT_Out%Main%TS_N_GEOMETRIES,                                        &
            1:VLIDORT_FixIn%Cont%TS_NSTOKES, 1:ndir)  
       
       ! CCM check this - Ithink the cloud fraction is multiplied twice  
       IF (cfrac_tot <= 0.0d0) THEN
          VLIDORT_LinOut%Surf%TS_SURFACEWF(1:VLIDORT_LinFixIn%Cont%TS_N_SURFACE_WFS,          &
               1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,                                      &
               1:VLIDORT_Out%Main%TS_N_GEOMETRIES, 1:VLIDORT_FixIn%Cont%TS_NSTOKES, 1:ndir) = &
               surfacewf_clrcld(1:VLIDORT_LinFixIn%Cont%TS_N_SURFACE_WFS,                     &
               1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,                                      &
               1:VLIDORT_Out%Main%TS_N_GEOMETRIES, 1:VLIDORT_FixIn%Cont%TS_NSTOKES, 1:ndir, 1)  
       ELSE IF (cfrac_tot >= 1.0d0) THEN
          VLIDORT_LinOut%Surf%TS_SURFACEWF(1:VLIDORT_LinFixIn%Cont%TS_N_SURFACE_WFS,    &
               1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,                                &
               1:VLIDORT_Out%Main%TS_N_GEOMETRIES,                                      &
               1:VLIDORT_FixIn%Cont%TS_NSTOKES, 1:ndir) =                               &
               SURFACEWF_CLRCLD(1:VLIDORT_LinFixIn%Cont%TS_N_SURFACE_WFS,               &
               1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,                                &
               1:VLIDORT_Out%Main%TS_N_GEOMETRIES, 1:VLIDORT_FixIn%Cont%TS_NSTOKES, 1:ndir, 2) 
       ELSE IF (do_lambertian_cld .AND. VLIDORT_FixIn%Bool%TS_DO_LAMBERTIAN_SURFACE) THEN ! from clear-sky only
          VLIDORT_LinOut%Surf%TS_SURFACEWF(1:VLIDORT_LinFixIn%Cont%TS_N_SURFACE_WFS, &
               1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,                             &
               1:VLIDORT_Out%Main%TS_N_GEOMETRIES, 1:VLIDORT_FixIn%Cont%TS_NSTOKES,  &
               1:ndir) = surfacewf_clrcld(1:VLIDORT_LinFixIn%Cont%TS_N_SURFACE_WFS,  &
               1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,                             &
               1:VLIDORT_Out%Main%TS_N_GEOMETRIES, 1:VLIDORT_FixIn%Cont%TS_NSTOKES,  &
               1:ndir, 1) * ( 1.0-cfrac_tot)
       ELSE IF (do_lambertian_cld .AND. .NOT. VLIDORT_FixIn%Bool%TS_DO_LAMBERTIAN_SURFACE) THEN ! Should not happen
          PRINT *, 'Should not happen!!!'; STOP
       ELSE
          VLIDORT_LinOut%Surf%TS_SURFACEWF(1:VLIDORT_LinFixIn%Cont%TS_N_SURFACE_WFS, &
               1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,                             &
               1:VLIDORT_Out%Main%TS_N_GEOMETRIES, 1:VLIDORT_FixIn%Cont%TS_NSTOKES, 1:ndir) =    &
               surfacewf_clrcld(1:VLIDORT_LinFixIn%Cont%TS_N_SURFACE_WFS,                        &
               1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,                                         &
               1:VLIDORT_Out%Main%TS_N_GEOMETRIES, 1:VLIDORT_FixIn%Cont%TS_NSTOKES, 1:ndir, 1) * &
               ( 1.0-cfrac_tot) + surfacewf_clrcld(1:VLIDORT_LinFixIn%Cont%TS_N_SURFACE_WFS,         &
               1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,                                         &
               1:VLIDORT_Out%Main%TS_N_GEOMETRIES, 1:VLIDORT_FixIn%Cont%TS_NSTOKES, 1:ndir, 2) * cfrac_tot
       ENDIF
       
    ENDIF
    
  END SUBROUTINE Vlidort_cloud_and_calculation_npix
  
  SUBROUTINE VBRDF_TO_VLIDORT(error)

    IMPLICIT NONE

    ! ------------------
    ! Modified variables
    ! ------------------
    LOGICAL,       INTENT(INOUT) :: error ! Error variable

    ! ----------------
    ! Code starts here
    ! ----------------
    error = .FALSE.
    
    VLIDORT_Sup%BRDF%TS_BRDF_F_0        = VBRDF_Sup_Out%BS_BRDF_F_0
    VLIDORT_Sup%BRDF%TS_BRDF_F          = VBRDF_Sup_Out%BS_BRDF_F
    VLIDORT_Sup%BRDF%TS_USER_BRDF_F_0   = VBRDF_Sup_Out%BS_USER_BRDF_F_0
    VLIDORT_Sup%BRDF%TS_USER_BRDF_F     = VBRDF_Sup_Out%BS_USER_BRDF_F
    VLIDORT_Sup%BRDF%TS_EXACTDB_BRDFUNC = VBRDF_Sup_Out%BS_DBOUNCE_BRDFUNC
    VLIDORT_Sup%BRDF%TS_EMISSIVITY      = VBRDF_Sup_Out%BS_EMISSIVITY
    VLIDORT_Sup%BRDF%TS_USER_EMISSIVITY = VBRDF_Sup_Out%BS_USER_EMISSIVITY
    
    VLIDORT_LinSup%BRDF%TS_LS_BRDF_F_0        = VBRDF_LinSup_Out%BS_LS_BRDF_F_0
    VLIDORT_LinSup%BRDF%TS_LS_BRDF_F          = VBRDF_LinSup_Out%BS_LS_BRDF_F
    VLIDORT_LinSup%BRDF%TS_LS_USER_BRDF_F_0   = VBRDF_LinSup_Out%BS_LS_USER_BRDF_F_0
    VLIDORT_LinSup%BRDF%TS_LS_USER_BRDF_F     = VBRDF_LinSup_Out%BS_LS_USER_BRDF_F
    VLIDORT_LinSup%BRDF%TS_LS_EXACTDB_BRDFUNC = VBRDF_LinSup_Out%BS_LS_DBOUNCE_BRDFUNC
    VLIDORT_LinSup%BRDF%TS_LS_USER_EMISSIVITY = VBRDF_LinSup_Out%BS_LS_USER_EMISSIVITY
    VLIDORT_LinSup%BRDF%TS_LS_EMISSIVITY      = VBRDF_LinSup_Out%BS_LS_EMISSIVITY
    
    
!     VLIDORT_LinSup%BRDF%TS_N_SURFACE_WFS       = VBRDF_LinSup_In%BS_N_SURFACE_WFS
!     VLIDORT_LinSup%BRDF%TS_N_KERNEL_FACTOR_WFS = VBRDF_LinSup_In%BS_N_KERNEL_FACTOR_WFS
!     VLIDORT_LinSup%BRDF%TS_N_KERNEL_PARAMS_WFS = VBRDF_LinSup_In%BS_N_KERNEL_PARAMS_WFS 

    GCM = (VBRDF_Sup_In%BS_which_brdf(1) .eq. 10 .or. &
           VBRDF_Sup_In%BS_which_brdf(2) .eq. 10 .or. &
           VBRDF_Sup_In%BS_which_brdf(3) .eq. 10)
    
    if ( GCM ) then
       NSTOKESSQ = VBRDF_Sup_In%BS_NSTOKES * VBRDF_Sup_In%BS_NSTOKES
    else
       NSTOKESSQ = 1
    endif
    Total_brdf = VLIDORT_Sup%BRDF%TS_EXACTDB_BRDFUNC
    OUTPUT_WSABSA = .FALSE.
    IF (VBRDF_Sup_In%BS_DO_WSABSA_OUTPUT) THEN
       WSA_CALCULATED = VBRDF_Sup_Out%BS_WSA_CALCULATED
       BSA_CALCULATED = VBRDF_Sup_Out%BS_BSA_CALCULATED
       OUTPUT_WSABSA = .TRUE.
    END IF

  END SUBROUTINE VBRDF_TO_VLIDORT
  
  SUBROUTINE MODIS_VBRDF_TO_VLIDORT(widx,error)
    
    USE GC_variables_module, ONLY : BS_DBOUNCE_BRDFUNC, &
                                    BS_BRDF_F,&
                                    BS_BRDF_F_0,&
                                    BS_USER_BRDF_F_0,&
                                    BS_USER_BRDF_F,&
                                    BS_EMISSIVITY,&
                                    BS_USER_EMISSIVITY, &
                                    VBRDF_Sup_In, &
                                    BS_NBEAMS, &
                                    BS_N_USER_RELAZMS, &
                                    BS_N_USER_STREAMS, &
                                    BS_NMOMENTS
    IMPLICIT NONE

    ! ------------------
    ! Modified variables
    ! ------------------
    INTEGER,       INTENT(IN)    :: widx  ! Wavelength index
    LOGICAL,       INTENT(INOUT) :: error ! Error variable
    
    
    ! ---------------
    ! Local variables
    ! ---------------
    LOGICAL :: DO_USER_OBSGEOMS, DO_SOLAR_SOURCES, DO_USER_STREAMS
    INTEGER :: IA, IB, UM, M, I
    INTEGER :: NBEAMS, N_USER_RELAZMS, N_USER_STREAMS, NMOMENTS, NSTREAMS
    
    ! ----------------
    ! Code starts here
    ! ----------------
    error = .FALSE.
    
    ! BRDF Options
    DO_SOLAR_SOURCES = VBRDF_Sup_In%BS_DO_SOLAR_SOURCES
    DO_USER_OBSGEOMS = VBRDF_Sup_In%BS_DO_USER_OBSGEOMS
    DO_USER_STREAMS  = VBRDF_Sup_In%BS_DO_USER_STREAMS
    
    ! Shorthand variable names
    NSTREAMS = VBRDF_Sup_In%BS_NSTREAMS
    NBEAMS = BS_NBEAMS
    N_USER_RELAZMS = BS_N_USER_RELAZMS
    N_USER_STREAMS = BS_N_USER_STREAMS
    NMOMENTS = BS_NMOMENTS
    
    ! First Zero arrays
    VLIDORT_Sup%BRDF%TS_BRDF_F_0 = 0.0d0
    VLIDORT_Sup%BRDF%TS_BRDF_F   = 0.0d0
    VLIDORT_Sup%BRDF%TS_USER_BRDF_F_0 = 0.0d0
    VLIDORT_Sup%BRDF%TS_USER_BRDF_F = 0.0d0
    VLIDORT_Sup%BRDF%TS_EXACTDB_BRDFUNC = 0.0d0
    VLIDORT_Sup%BRDF%TS_EMISSIVITY = 0.0d0
    VLIDORT_Sup%BRDF%TS_USER_EMISSIVITY = 0.0d0
    
    ! Emissivity
    VLIDORT_Sup%BRDF%TS_EMISSIVITY = BS_EMISSIVITY
    VLIDORT_Sup%BRDF%TS_USER_EMISSIVITY = BS_USER_EMISSIVITY
    
    IF( DO_USER_OBSGEOMS ) THEN
       VLIDORT_Sup%BRDF%TS_EXACTDB_BRDFUNC(1,1,1,1:NBEAMS) = &
                        BS_DBOUNCE_BRDFUNC(  1,1,1:NBEAMS,widx)
    ELSE
      DO IA = 1, N_USER_RELAZMS
      DO IB = 1, NBEAMS
      DO UM = 1, N_USER_STREAMS
         VLIDORT_Sup%BRDF%TS_EXACTDB_BRDFUNC(1,UM,IA,IB) = &
                          BS_DBOUNCE_BRDFUNC(  UM,IA,IB,widx)
      ENDDO
      ENDDO
      ENDDO
    ENDIF
    
    DO M=0, NMOMENTS
      
      DO I=1, NSTREAMS
      
        VLIDORT_Sup%BRDF%TS_BRDF_F(M,1,I,1:NSTREAMS) = &
                         BS_BRDF_F(M,  I,1:NSTREAMS,widx)
        
        IF( DO_SOLAR_SOURCES ) THEN
          VLIDORT_Sup%BRDF%TS_BRDF_F_0(M,1,I,1:NBEAMS) = &
                           BS_BRDF_F_0(M,  I,1:NBEAMS,widx)
        ENDIF
        
        IF( DO_USER_STREAMS ) THEN
          
          DO UM=1, N_USER_STREAMS
            
            VLIDORT_Sup%BRDF%TS_USER_BRDF_F(M,1,UM,1:NSTREAMS) = &
                             BS_USER_BRDF_F(M,  UM,1:NSTREAMS,widx)
            
            IF( DO_SOLAR_SOURCES ) THEN
              
              IF( DO_USER_OBSGEOMS ) THEN
                VLIDORT_Sup%BRDF%TS_USER_BRDF_F_0(M,1,1,1:NBEAMS) = &
                                 BS_USER_BRDF_F_0(M,  1,1:NBEAMS,widx)
              ELSE
                VLIDORT_Sup%BRDF%TS_USER_BRDF_F_0(M,1,UM,1:NBEAMS) = &
                                 BS_USER_BRDF_F_0(M,  UM,1:NBEAMS,widx)
              ENDIF
              
            ENDIF
            
          ENDDO
          
          
        ENDIF
        
      ENDDO
      
    ENDDO
    
    GCM = (VBRDF_Sup_In%BS_which_brdf(1) .eq. 10 .or. &
           VBRDF_Sup_In%BS_which_brdf(2) .eq. 10 .or. &
           VBRDF_Sup_In%BS_which_brdf(3) .eq. 10)
    
    
    if ( GCM ) then
       NSTOKESSQ = VBRDF_Sup_In%BS_NSTOKES * VBRDF_Sup_In%BS_NSTOKES
    else
       NSTOKESSQ = 1
    endif
    Total_brdf = VLIDORT_Sup%BRDF%TS_EXACTDB_BRDFUNC
    OUTPUT_WSABSA = .FALSE.
    IF (VBRDF_Sup_In%BS_DO_WSABSA_OUTPUT) THEN
       WSA_CALCULATED = VBRDF_Sup_Out%BS_WSA_CALCULATED
       BSA_CALCULATED = VBRDF_Sup_Out%BS_BSA_CALCULATED
       OUTPUT_WSABSA = .TRUE.
    END IF

  END SUBROUTINE MODIS_VBRDF_TO_VLIDORT
  
  SUBROUTINE VL_UPDATE_SIF( widx)
    
    USE GC_variables_module, ONLY : sif_spc
    
    ! ------------------
    ! Modified variables
    ! ------------------
    INTEGER,       INTENT(IN)    :: widx  ! Wavelength index
    
    ! =======================================================
    ! VL_UPDATE_SIF starts here
    ! =======================================================
    
    ! Switch on surface leaving
    VLIDORT_FixIn%Bool%TS_DO_SURFACE_LEAVING = .TRUE.
    
    ! Assume isotropic emission for now
    VLIDORT_FixIn%Bool%TS_DO_SL_ISOTROPIC = .TRUE.
    
    ! First zero isotropic leaving term
    VLIDORT_Sup%SLEAVE%TS_SLTERM_ISOTROPIC(:,:) = 0.0
    
    ! Add leaving radiance
    VLIDORT_Sup%SLEAVE%TS_SLTERM_ISOTROPIC(1,1:GC_n_sun_positions) = sif_spc(widx)
    
  END SUBROUTINE VL_UPDATE_SIF
  
END MODULE GC_Vlidort_module
