MODULE GC_variables_module

  USE VLIDORT_PARS
  USE VLIDORT_IO_DEFS
  USE VLIDORT_LININPUTS_DEF
  USE VLIDORT_LINSUP_INOUT_DEF
  USE VLIDORT_LINOUTPUTS_DEF
  USE VLIDORT_OUTPUTS_DEF

  USE VBRDF_SUP_INPUTS_DEF
  USE VBRDF_LINSUP_INPUTS_DEF
  USE VBRDF_SUP_OUTPUTS_DEF
  USE VBRDF_LINSUP_OUTPUTS_DEF

  USE GC_parameters_module

  IMPLICIT NONE
! ----------------------------
! Number of greek coefficients
! ----------------------------
  INTEGER           :: ngksec, nactgkmatc

! -------------------
! Temporal wavelength
! -------------------
!   REAL(KIND=8), DIMENSION(maxflambdas) :: tmpwaves, tmpcwaves
  REAL(KIND=8), ALLOCATABLE, DIMENSION(:) :: tmpwaves, tmpcwaves
  
! --------------------------------------------------------------------------------
! Variables to store partial clear/cloudy calculations directly from VLIDORT
! Dimensions, (view_geometries,stokes_components,direction(up/down_welling),cloud)
! --------------------------------------------------------------------------------
  REAL(KIND=8), DIMENSION(GC_maxuserlevels,max_geometries, maxstokes, 2, 2)                       :: stokes_clrcld
  REAL(KIND=8), DIMENSION(GC_maxuserlevels,max_szangles, maxstokes, 2, 2)                         :: stokes_flux
  REAL(KIND=8), DIMENSION(GC_maxuserlevels,max_szangles, maxstokes, 2)                            :: stokes_direct_flux
  REAL(KIND=8), DIMENSION(max_atmoswfs, maxlayers, GC_maxuserlevels,max_geometries, maxstokes, 2) :: profilewf_sum
  REAL(KIND=8), DIMENSION(max_surfacewfs, GC_maxuserlevels,max_geometries, maxstokes, 2, 2)       :: surfacewf_clrcld
  REAL(KIND=8), DIMENSION(0:max_atmoswfs, 0:maxmoms, 1:maxgksec)                                  :: l_phasmoms_total_input
  
  
  ! User defined VLIDORT settings
  LOGICAL :: SVL_full_stokes_calc
  INTEGER :: SVL_n_stokes
  LOGICAL :: SVL_do_thermal_emiss
  LOGICAL :: SVL_do_surf_emiss
  LOGICAL :: SVL_do_upwelling
  LOGICAL :: SVL_do_downwelling
  INTEGER :: SVL_nstreams
  INTEGER :: SVL_nmoments
  LOGICAL :: SVL_do_debug
  
! =======================
! ***********************
! =======================
! Control Input variables
! =======================
! ***********************
! =======================

! ------------------------------------------
! Paths to the input/output data directories
! ------------------------------------------
   CHARACTER(LEN=max_ch_len) ::  database_dir, results_dir

! -------------------
! File names for data
! -------------------
   CHARACTER(LEN=max_ch_len) :: profile_data_filename, debug_filename, &
                                albspectra_fname, aerfile, cldfile, &
                                dem_fname, profile_data_ncfile

! ASCII profile
   LOGICAL :: yn_ascii_profile

! .nc profile file x, y indices
  INTEGER  :: nc_xid, nc_yid
  INTEGER, DIMENSION(1:2) :: nc_xids, nc_yids 
  INTEGER  :: nc_imx, nc_jmx ! Max X,Y dimensions in output file
  
! XY NC control
  LOGICAL :: do_output_as_xy_nc,&
             do_xy_nc_file_lock,&
             overwrite_existing_xy_nc,&
             create_xy_nc_only,&
             skip_xy_pixel
  
!  ----------------------------------------------------------
!  Aerosol/clouds flag (true if you want to include aerosols)
! -----------------------------------------------------------
   LOGICAL :: do_aerosols, do_clouds, do_lambertian_cld
   LOGICAL :: use_aerprof, use_cldprof
  
! --------------------------------------------------------
! Vector calculation flag (true if you want polarization)
! --------------------------------------------------------
   LOGICAL :: do_vector_calculation

! --------------------
! Stream control input
! --------------------
   INTEGER :: nstreams_choice

! -------------------------------
! Upwelling & downwelling control
! -------------------------------
   INTEGER               :: ndir ! Number of directions (1 or 2)
   INTEGER               :: idir ! Directions loop index
   INTEGER, DIMENSION(2) :: idix ! Directions
   INTEGER :: didx ! = 1, upwelling; 2. downwelling

! -------------
! GC geometries
! -------------
   INTEGER      :: GC_n_sun_positions
   REAL(KIND=8) :: GC_sun_positions(GC_maxgeometries)
   REAL(KIND=8) :: GC_sun_azimuths(GC_maxgeometries) ! needed for sea glint
   INTEGER      :: GC_n_view_angles
   REAL(KIND=8) :: GC_view_angles(GC_maxgeometries)
   INTEGER      :: GC_n_azimuths
   REAL(KIND=8) :: GC_azimuths(GC_maxgeometries)
   INTEGER      :: GC_n_user_levels
   REAL(KIND=8) :: GC_user_levels(GC_maxuserlevels)
   INTEGER      :: GC_n_user_altitudes
   REAL(KIND=8) :: GC_user_altitudes(GC_maxuserlevels)
   LOGICAL      :: GC_do_user_altitudes
   REAL(KIND=8) :: GC_TerrainHeight
   LOGICAL      :: GC_do_terrain_adjust

! --------------------
! Observation altitude
! --------------------
   REAL(KIND=8) :: prof_obsalt
   LOGICAL :: do_yn_obsalt

! ---------------------
! Digital elevation map
! ---------------------
   INTEGER      :: DEM_interp_opt, nx_dem, ny_dem
   REAL(KIND=4),         ALLOCATABLE, DIMENSION(:)   :: lon_dem, lat_dem
   INTEGER(KIND=2),      ALLOCATABLE, DIMENSION(:,:) :: alt_dem
  
! --------------------------------
! Solar induced plant fluorescence 
! --------------------------------
  
  REAL(KIND=8)                            :: sif_734nm    ! Fluorescence at 734nm (mW/m2/nm/Sr)
  REAL(KIND=8)                            :: solflx_734nm ! Solar irradiance at 734nm (mW/m2/nm)
  REAL(KIND=8), ALLOCATABLE, DIMENSION(:) :: sif_spc      ! Output sif normed to solar input
! ---------------------
! Output levels control
! ---------------------
   INTEGER :: ilev ! Level index

! ---------------
! Spectral inputs
! ---------------
   REAL(KIND=8) :: lambda_start
   REAL(KIND=8) :: lambda_finish
   LOGICAL      :: use_wavelength        ! T: input in wavelength (nm) F: cm^-1
   LOGICAL      :: do_effcrs             ! Use effective cross sections
   REAL(KIND=8) :: lambda_resolution     ! Spectral resolution at FWHM
   REAL(KIND=8) :: lambda_dw, lambda_dfw ! Spectral interval for output and fine radiances
   
! ---------------------------
! Derived spectral quantities
! ---------------------------
   INTEGER                                 :: nlambdas
   REAL(KIND=8), ALLOCATABLE, DIMENSION(:) :: lambdas, wavnums
  
! -----------------------------------------------------
! Add wavelength grids for fine/coarse wavelength grids
! -----------------------------------------------------
   INTEGER                              :: nflambdas, nclambdas
   REAL(KIND=8)                         :: edge_dw
   REAL(KIND=8), ALLOCATABLE, DIMENSION(:) :: flambdas, fwavnums
   REAL(KIND=8), ALLOCATABLE, DIMENSION(:) :: clambdas, cwavnums
  
! ------------------------------------------------------
! Use footprint information (lon,lat,yy,mm,dd,utc,
! sza,vza,aza,Ts,Ps,fc,ws,ctp) from atmospheric profiles
! ------------------------------------------------------
   LOGICAL :: use_footprint_info

! ---------------------
! Outputs for debugging 
! ---------------------
   LOGICAL :: do_debug_geocape_tool

! -------------------------
! Use solar photons logical
! -------------------------
   LOGICAL :: use_solar_photons ! use photons/cm^2/s /nm	

! --------------------------------------------------------------------
! Possible gases are labeled as follows: O3, NO2, HCHO, SO2, H2O, GLYX
! , BRO, OCLO, IO, CO, CO2, N2O, CH4, O2, NO, HNO3, OCS
! --------------------------------------------------------------------
  INTEGER                                   :: ngases
  CHARACTER(LEN=4), ALLOCATABLE, DIMENSION (:)  :: which_gases
  
! ------------------------
! Surface albedo variables
! ------------------------
   LOGICAL                             :: use_lambertian, do_brdf_surface
   LOGICAL                             :: use_fixedalbedo, use_albspectra
   LOGICAL                             :: do_ocean_glint
   REAL(KIND=8)                        :: fixed_albedo
   LOGICAL                             :: is_ocean
   REAL(KIND=8), DIMENSION(maxlambdas) :: wavlens_um  ! wavelength in microns
   REAL(KIND=8)                        :: wind_speed, wind_dir, chlorophyll_conc, salinity
   REAL(KIND=8), DIMENSION(maxlambdas) :: water_rn ! real part of refr index
   REAL(KIND=8), DIMENSION(maxlambdas) :: water_cn ! imag part of refr index
   
   ! For LER climatology
   LOGICAL      ::  use_ler_climatology, do_ler_const_wvl
   REAL(KIND=8), DIMENSION(1) ::  ler_clim_wvl
   CHARACTER(LEN=max_ch_len) :: ler_clim_file
   
   
   INTEGER :: modis_fa_alb_type
   
! -------------------------------------------------
! Geographical, time, satellite positions, geometry
! -------------------------------------------------
   LOGICAL                    :: do_sat_viewcalc
   INTEGER(KIND=4)            :: year, month, day, day_of_year
   REAL(KIND=8)               :: latitude, longitude, satlon, satlat, &
                                 satalt
   REAL(KIND=8)               :: utc
   REAL(KIND=8), DIMENSION(4) :: geometry_data

   ! Footprint pixel corner longitude / latitude data
   REAL(KIND=8), DIMENSION(4) :: clon, clat
  
   ! Logical for INR Corner definition
   LOGICAL                    :: yn_inr_corner

! ===========================
! ***************************
! ===========================
! End control Input variables
! ===========================
! ***************************
! ===========================

! =========================
! *************************
! =========================
! Aerosol related varaibles
! =========================
! *************************
! =========================

! ----------------------------------
! Logicals for aerosols calculations
! ----------------------------------
   LOGICAL :: do_aod_Jacobians, do_assa_Jacobians, do_aerph_jacobians, &
             do_aerhw_jacobians, do_aer_columnwf

! -------------
! Aerosol Stuff 
! -------------
   INTEGER                         :: naer, naer0
   REAL(KIND=8)                    :: aer_reflambda, taertau0
   REAL(KIND=8), ALLOCATABLE, DIMENSION(:) :: aer_tau0s, aer_z_upperlimit,       &
                                              aer_z_lowerlimit, aer_z_peakheight,&
                                              aer_half_width, aer_relaxation
   LOGICAL, ALLOCATABLE,      DIMENSION(:) :: yn_aer_rhdep
   CHARACTER(LEN=max_ch_len), ALLOCATABLE  :: aer_data_filenames(:)
   CHARACTER(LEN=3), ALLOCATABLE           :: aer_proftype(:)
   
   
   REAL(KIND=8), ALLOCATABLE, DIMENSION(:,:)   :: aer_profile,         &
                                                  aer_d_profile_dtau,  &
                                                  aer_d_profile_dpkh,  &
                                                  aer_d_profile_dhfw,  &
                                                  aer_d_profile_drel,  &
                                                  aer_profile0,        &
                                                  aer_profile_ssa
   REAL(KIND=8), ALLOCATABLE, DIMENSION(:)     :: taer_profile
   REAL(KIND=8), ALLOCATABLE, DIMENSION(:)     :: rh_profile
   CHARACTER(LEN=4), ALLOCATABLE, DIMENSION(:) :: aer_types
   LOGICAL,      ALLOCATABLE, DIMENSION(:)     :: aer_flags
   REAL(KIND=8), ALLOCATABLE, DIMENSION(:,:)   :: aer_opdeps
   REAL(KIND=8), ALLOCATABLE, DIMENSION(:,:)   :: aer_ssalbs
   
   REAL(KIND=8), ALLOCATABLE, DIMENSION(:,:)   :: aer_spc_ssalbs
   
   TYPE AerOptPropType
      INTEGER :: nWavelength
      INTEGER :: nMoments
      INTEGER :: nRH
      REAL(KIND=8), ALLOCATABLE, DIMENSION(:,:,:,:) :: PhFcn0
      REAL(KIND=8), ALLOCATABLE, DIMENSION(:,:) :: Qext0, SSA0
      REAL(KIND=8), ALLOCATABLE, DIMENSION(:)   :: Wavelength,RH
      
   ENDTYPE AerOptPropType
   
   TYPE (AerOptPropType ), DIMENSION(:), ALLOCATABLE :: AerOptProp
   
! --------------------------------------------------------
! Extinction coefficients relative to specified wavelength
! --------------------------------------------------------
   REAL(KIND=8), ALLOCATABLE, DIMENSION(:,:) :: aer_relqext 
   
! --------------------------------------------------------
! To save space, get aerosol properties at each wavelength
! --------------------------------------------------------
   REAL(KIND=8), ALLOCATABLE, DIMENSION(:,:,:) :: aer_phfcn  
   REAL(KIND=8), DIMENSION(:,:,:,:), ALLOCATABLE              :: aer_phfcn_all
   
! =============================
! *****************************
! =============================
! End aerosol related variables
! =============================
! *****************************
! =============================

! ========================
! ************************
! ========================
! Clouds related varaibles
! ========================
! ************************
! ========================

! ---------------------------------------------------------------------
! Cloud Stuff (Either Lambertian or scattering clouds, multiple clouds)
! ---------------------------------------------------------------------
   LOGICAL :: do_cod_Jacobians, do_cssa_Jacobians, do_ctp_Jacobians,     &
              do_cld_columnwf, do_cfrac_Jacobians

   LOGICAL                                    :: use_zcldspec
   REAL(KIND=8)                               :: lambertian_cldalb, cfrac, ipafrac
   INTEGER                                    :: ncld
   CHARACTER(len=2), ALLOCATABLE, DIMENSION(:)          :: cld_types
   CHARACTER(len=max_ch_len), ALLOCATABLE, DIMENSION(:) :: cld_data_filenames
   TYPE (AerOptPropType ), DIMENSION(:), ALLOCATABLE :: CldOptProp
   
   ! Options to allow for n cloudy pixels
   LOGICAL                                     :: do_npix_cloud
   INTEGER                                     :: n_cpix    ! # of cloudy pixels
   REAL(KIND=4), ALLOCATABLE, DIMENSION(:)     :: cpix_cfrac ! Cloud fraction for each pixel
   REAL(KIND=4), ALLOCATABLE, DIMENSION(:)     :: cpix_pcld  ! Cloud pressures for each pixel
   REAL(KIND=4), ALLOCATABLE, DIMENSION(:)     :: cpix_zcld  ! Cloud heights for each pixel
   REAL(KIND=8), ALLOCATABLE, DIMENSION(:,:,:) :: cld_prof_npix ! Cloud profiles
   
   ! For lambertian cloud case
   INTEGER,      ALLOCATABLE, DIMENSION(:) :: cpix_lam_zlvl
   REAL(KIND=8), ALLOCATABLE, DIMENSION(:) :: cpix_lam_lvlfrac
   
   ! For computing jacobians 
   REAL(KIND=8), ALLOCATABLE, DIMENSION(:,:,:) :: cld_d_profile_dtau, &
                                                  cld_d_profile_dpkh, &
                                                  cld_d_profile_dhfw, &
                                                  cld_d_profile_drel
   
   INTEGER,      ALLOCATABLE, DIMENSION(:,:)   :: cld_lowers_npix,       &
                                                  cld_uppers_npix,       &
                                                  cld_opdeps_npix,       &
                                                  cld_bots_npix,         &
                                                  cld_tops_npix
                                                  
    ! New cloud profiles based on generic aerosol types
    ! -------------------------------------------------
    REAL(KIND=8),ALLOCATABLE, DIMENSION(:,:)      :: cld_tau0s, &
                                                     cld_z_upperlimit, &
                                                     cld_z_lowerlimit, &
                                                     cld_z_peakheight, &
                                                     cld_relaxation,   &
                                                     cld_half_width
    CHARACTER(LEN=3), ALLOCATABLE, DIMENSION(:,:) :: cld_proftype
    
! -------------------------------
! Assume homogeneuos distribution
! -------------------------------
   REAL(KIND=8), ALLOCATABLE, DIMENSION(:)             :: cld_bots,         &
                                                          cld_tops,         &
                                                          cld_taus
   LOGICAL, ALLOCATABLE,      DIMENSION(:)             :: cld_flags
   INTEGER, ALLOCATABLE,      DIMENSION(:)             :: cld_lowers,       &
                                                          cld_uppers
   REAL(KIND=8), ALLOCATABLE, DIMENSION(:,:)           :: cld_opdeps
   REAL(KIND=8), ALLOCATABLE, DIMENSION(:,:)           :: cld_ssalbs
   REAL(KIND=8), ALLOCATABLE, DIMENSION(:,:)           :: cld_profile
   REAL(KIND=8), ALLOCATABLE, DIMENSION(:)             :: tcld_profile
   
! --------------------------------------------------------
! Extinction coefficients relative to specified wavelength
! --------------------------------------------------------
   REAL(KIND=8)                              :: cld_reflambda,   &
                                                tcldtau0
   REAL(KIND=8), ALLOCATABLE, DIMENSION(:,:) :: cld_relqext 
  
! -------------------------------------------------------
! To save space, get clouds properties at each wavelength
! -------------------------------------------------------
   REAL(KIND=8), ALLOCATABLE, DIMENSION(:,:,:) :: cld_phfcn
   
! ============================
! ****************************
! ============================
! End clouds related variables
! ============================
! ****************************
! ============================

! =====================
! *********************
! =====================
! Sun related varaibles
! =====================
! *********************
! =====================

! --------------
! Time variables
! --------------
   REAL(KIND=8)           :: jday, jday0, rdis
   REAL(KIND=8), EXTERNAL :: julday
   
! --------------
! Solar spectrum
! --------------
   REAL(KIND=8), ALLOCATABLE, DIMENSION(:)  :: solar_spec_data
   REAL(KIND=8), ALLOCATABLE, DIMENSION(:)  :: solar_cspec_data
   
! -----------------------
! Solar spectrum filename
! -----------------------
   CHARACTER(LEN=max_ch_len) :: solar_spec_filename

! =========================
! *************************
! =========================
! End Sun related variables
! =========================
! *************************
! =========================

! =======================
! ***********************
! =======================
! X-sec related varaibles
! =======================
! ***********************
! =======================

! -------------------
! Cross-section stuff 
! --------------------------------------------------------
! cross sections types: 
! 1: does not denpendent on P, T (same for all layers)
! 2: 2nd parameterized T-dependent coefficients (e.g., O3)
! 3: depends on P, and T (e.g., those read from HITRAN)
! --------------------------------------------------------
  
   ! New structure based variable
   TYPE XSectType
       INTEGER                                     :: nXSect
       INTEGER                                     :: XSectTypeID
       CHARACTER(LEN=max_ch_len)                   :: XSectDataFilename
       CHARACTER(LEN=4)                            :: Name      ! which_gases
       REAL(KIND=8), ALLOCATABLE, DIMENSION(:,:)   :: XSect     ! gas_xsecs
       REAL(KIND=8), ALLOCATABLE, DIMENSION(:,:)   :: dXSectdT  ! gas_xsecs - T derivative
       LOGICAL                                     :: is_CIA
       CHARACTER(LEN=4)                            :: CIA_Name1
       CHARACTER(LEN=4)                            :: CIA_Name2
   ENDTYPE XSectType
   
   ! Gas absorption cross sections
   TYPE (XSectType), DIMENSION(:), ALLOCATABLE :: GasAbsXS
   
  CHARACTER(LEN=max_ch_len), ALLOCATABLE :: xsec_data_filenames(:)
  CHARACTER(LEN=max_ch_len) :: xsec_data_path
  
   
   ! Solar file for HITRAN I0 correction 
   ! -----------------------------------
   LOGICAL                   :: do_hitran
   REAL(KIND=8), ALLOCATABLE, DIMENSION(:) :: hitran_sol_wvl, hitran_sol_spc
   INTEGER                                 :: n_hitran_solar
   
   ! Number of absorption coefficents (1, 3, and n for the above example)
   INTEGER, ALLOCATABLE, DIMENSION(:) :: gas_nxsecs
   INTEGER, ALLOCATABLE, DIMENSION(:) :: gas_xsecs_type
   
   REAL(KIND=8), ALLOCATABLE, DIMENSION(:,:,:) :: gas_xsecs, gas_xsecs_deriv
   REAL(KIND=8), ALLOCATABLE, DIMENSION(:)     :: o3c1_xsecs
   REAL(KIND=8), ALLOCATABLE, DIMENSION(:)     :: o3c2_xsecs
   REAL(KIND=8), ALLOCATABLE, DIMENSION(:)     :: Rayleigh_xsecs
   REAL(KIND=8), ALLOCATABLE, DIMENSION(:)     :: Rayleigh_depols
   
   
   ! For the Raman parameterization
   REAL(KIND=8), ALLOCATABLE, DIMENSION(:)   :: o2_depols, n2_depols, epsilon_air
   REAL(KIND=8), ALLOCATABLE, DIMENSION(:,:) :: o2_rrs_xsecs, n2_rrs_xsecs
   
   ! New generalized Raman parameterization
   INTEGER                                      :: ngas_rrs
   REAL(KIND=8), ALLOCATABLE, DIMENSION(:,:,:)  :: rrs_xsecs
   REAL(KIND=8), ALLOCATABLE, DIMENSION(:,:)    :: rrs_depols
   CHARACTER(LEN=10), ALLOCATABLE, DIMENSION(:) :: rrs_gasname
   REAL(KIND=8), ALLOCATABLE, DIMENSION(:)      :: rrs_gas
   
   ! Options for RRS phase function
   LOGICAL :: do_rrs_abs
   LOGICAL :: do_rrs_phasefunc
   
   ! Arrays for bulk composition
   LOGICAL                                     :: do_earth_bulk_comp
   REAL(KIND=8)                                :: grav_accel
   INTEGER                                     :: n_bulk_gas
   REAL(KIND=8),     ALLOCATABLE, DIMENSION(:) :: bulk_gas_mixr
   REAL(KIND=8),     ALLOCATABLE, DIMENSION(:) :: bulk_gas_mw
   CHARACTER(LEN=4), ALLOCATABLE, DIMENSION(:) :: bulk_gas_name
   CHARACTER(LEN=4), ALLOCATABLE, DIMENSION(:) :: bulk_gas_ri_name
   
   
! ===========================
! ***************************
! ===========================
! End X-sec related variables
! ===========================
! ***************************
! ===========================

! =========================
! *************************
! =========================
! Surface related varaibles
! =========================
! *************************
! =========================

  REAL(KIND=8), ALLOCATABLE, DIMENSION(:) :: ground_ler
  REAL(KIND=8), dimension(MAXSTOKES_SQ, MAX_USER_STREAMS, &
                          MAX_USER_RELAZMS, MAXBEAMS ) :: Total_brdf
  REAL(KIND=8) :: WSA_CALCULATED, BSA_CALCULATED
  LOGICAL :: GCM, OUTPUT_WSABSA
  INTEGER :: NSTOKESSQ
  
  ! For the BRDF EOF subroutines
  CHARACTER(LEN=max_ch_len)  :: atmdbdir ! Base directory for EOF BRDF
  LOGICAL                    :: use_albeofs
  LOGICAL                    :: do_eof_brdf
  LOGICAL                    :: do_fixed_brdf
  LOGICAL                    :: do_sif
  REAL(KIND=8), DIMENSION(3) :: temis_uv_alb ! TEMIS albedos for wavelengths < 400 nm
  REAL(KIND=8), DIMENSION(3) :: temis_uv_wav ! Corresponding wavelengths
  REAL(KIND=8), DIMENSION(4) :: alb_eof_coeff ! EOF coefficients
  REAL(KIND=8)               :: alb_eof_lfrac ! Land fraction 
  REAL(KIND=8)               :: alb_eof_iflg ! Ice flag
  REAL(KIND=8)               :: temis_sf  ! 416 nm scale factor between TEMIS and EOF-derived reflectance 
  
  ! Snow reflectance parameters
  REAL(KIND=8)               :: prof_snowfrac,   prof_snowdepth,&
                                prof_seaicefrac, prof_snowage, prof_snowfrac_eff
  REAL(KIND=8), DIMENSION(17) :: ltype_frac
  
  ! New SCIA-USGS Merged Factors
  ! =================================================================================
  REAL(KIND=4), ALLOCATABLE, DIMENSION(:)   :: fa_mu   ! Mean
  REAL(KIND=4), ALLOCATABLE, DIMENSION(:)   :: fa_mu_m ! Mean at each MODIS band
  REAL(KIND=4), ALLOCATABLE, DIMENSION(:,:) :: fa_W  ! Factor loading matrix
  REAL(KIND=4), ALLOCATABLE, DIMENSION(:,:) :: fa_G  ! Gain matrix for MODIS bands
  REAL(KIND=4), ALLOCATABLE, DIMENSION(:)   :: fa_wvl
  REAL(KIND=4), ALLOCATABLE, DIMENSION(:)   :: fa_lon 
  REAL(KIND=4), ALLOCATABLE, DIMENSION(:)   :: fa_lat
  REAL(KIND=8), ALLOCATABLE, DIMENSION(:)   :: fa_wvl_r8 ! Interpolated to output grid
  REAL(KIND=8), ALLOCATABLE, DIMENSION(:)   :: fa_mu_int ! Interpolated to output grid
  REAL(KIND=8), ALLOCATABLE, DIMENSION(:)   :: fa_tmp    ! Interpolated to output grid
  REAL(KIND=8), ALLOCATABLE, DIMENSION(:)   :: fa_tmp_int! Interpolated to output grid
  REAL(KIND=8), ALLOCATABLE, DIMENSION(:,:) :: fa_W_int  ! Interpolated to output grid
  INTEGER                                   :: fa_xidx, fa_yidx
  INTEGER(KIND=2), ALLOCATABLE, DIMENSION(:,:) :: fa_exist
  REAL(KIND=8), ALLOCATABLE, DIMENSION(:)   :: usgs_water_int
  REAL(KIND=8), ALLOCATABLE, DIMENSION(:)   :: usgs_snowf_int
  REAL(KIND=8), ALLOCATABLE, DIMENSION(:)   :: usgs_snowc_int
  CHARACTER(LEN=max_ch_len)                 :: fa_infile ! Input file
  
  ! FA Dimensions
  INTEGER :: fa_nwvl ! # Wavelengths
  INTEGER :: fa_nmod ! # MODIS bands
  INTEGER :: fa_nfa  ! # factors
  INTEGER :: fa_imx  ! # x locations
  INTEGER :: fa_jmx  ! # y locations
  
  ! BRDF Climatology
  ! ======================================================================================
  LOGICAL                    :: use_brdf_clim
  CHARACTER(LEN=max_ch_len)  :: brdf_clim_infile
  
  ! CM Glint (option 7)
  LOGICAL :: use_cmglint
  LOGICAL :: do_cm_facet_iso, do_cm_whitecap, do_cm_shadow
  
  ! Archive MODIS information
  REAL(KIND=8), DIMENSION(4,3) :: modis_pix_kern
  REAL(KIND=8)                 :: modis_pix_lfrac
  
  ! Fixed Kernel options
  CHARACTER(LEN=10), DIMENSION(3)   :: fkern_name
  INTEGER(KIND=4),   DIMENSION(3)   :: fkern_idx
  REAL(KIND=8),      DIMENSION(3)   :: fkern_amp
  INTEGER(KIND=4),   DIMENSION(3)   :: fkern_npar
  REAL(KIND=8),      DIMENSION(3,MAX_BRDF_PARAMETERS) :: fkern_par

  ! Wavelength dependent kernel amplitudes
  REAL(KIND=8), ALLOCATABLE   :: fkern_amp_wvl(:,:)   ! wvl,kernel
  REAL(KIND=8), ALLOCATABLE   :: fkern_par_wvl(:,:,:) ! wvl,kernel,par
  
  ! For using the MODIS high resolution data
  REAL(KIND=8), ALLOCATABLE, DIMENSION(:,:,:,:) :: modis_brdfs
  REAL(KIND=8), ALLOCATABLE, DIMENSION(:,:)     :: modis_lfrac
  INTEGER(KIND=2), ALLOCATABLE, DIMENSION(:,:)  :: modis_field_i2
  INTEGER(KIND=1), ALLOCATABLE, DIMENSION(:,:)  :: modis_field_i1
  REAL(KIND=8), ALLOCATABLE, DIMENSION(:)       :: modis_lon, modis_lat
  INTEGER(KIND=4)                               :: nx_modis, ny_modis
  
  ! MODIS kernels For the VLIDORT supplemental BRDF routines
  TYPE(VBRDF_Sup_ModOutputs ) :: BRDF_Sup_ModOut
  INTEGER                     :: NMOMENTS_INPUT ! Fourier azimuthanl expansion of BRDF
  TYPE(VBRDF_Sup_Outputs), ALLOCATABLE, DIMENSION(:) :: MODIS_VBRDF_Sup_Out ! This wastes too much memory
  
  ! To speed up the BRDF computation
  REAL(KIND=8), ALLOCATABLE, DIMENSION(:)   :: out_snowspec_f, out_snowspec_c, out_snowspec
  REAL(KIND=8), ALLOCATABLE, DIMENSION(:)   :: out_waterspec
  REAL(KIND=8), ALLOCATABLE, DIMENSION(:)   :: out_mean
  REAL(KIND=8), ALLOCATABLE, DIMENSION(:,:) :: out_eofs
  
  ! Only allocate memory for what is needed
  REAL(KINd=fpk), ALLOCATABLE, DIMENSION(:,:,:,:) :: BS_DBOUNCE_BRDFUNC
  REAL(KINd=fpk), ALLOCATABLE, DIMENSION(:,:,:,:) :: BS_BRDF_F
  REAL(KINd=fpk), ALLOCATABLE, DIMENSION(:,:,:,:) :: BS_BRDF_F_0
  REAL(KINd=fpk), ALLOCATABLE, DIMENSION(:,:,:,:) :: BS_USER_BRDF_F_0
  REAL(KINd=fpk), ALLOCATABLE, DIMENSION(:,:,:,:) :: BS_USER_BRDF_F
  REAL(KINd=fpk)                                  :: BS_EMISSIVITY
  REAL(KINd=fpk)                                  :: BS_USER_EMISSIVITY
  INTEGER                                         :: BS_NBEAMS, &
                                                     BS_N_USER_RELAZMS, &
                                                     BS_N_USER_STREAMS, &
                                                     BS_NMOMENTS
  
! =============================
! *****************************
! =============================
! End surface related variables
! =============================
! *****************************
! =============================

! =========================
! *************************
! =========================
! Profile related varaibles
! =========================
! *************************
! =========================  
! ------------------------------------------------------
! Profile stuff (Output from "geocape_profile_setter_1")
! ------------------------------------------------------
! Profile data
! ------------
  REAL(kind=8), DIMENSION(pmaxl,pmaxc) :: profile_data
  REAL(kind=8), DIMENSION(28)          :: footprint_data
  CHARACTER(len=max_ch_len)            :: profids
  logical                              :: profile_is_level
  INTEGER                              :: pnc   ! # of data fields in atmospheric profiles

! ---------------------------------------------------------------------   
! Number of layers has 'GC_' prefix (distinguish with VLIDORT variable)
! ---------------------------------------------------------------------
  INTEGER                    :: GC_nlayers

! ------------------------
! Derived quantities (PTH)
! ------------------------
  REAL(kind=8), ALLOCATABLE, DIMENSION (:) :: heights
  REAL(kind=8), ALLOCATABLE, DIMENSION (:) :: mid_temperatures, mid_pressures, mid_heights
  REAL(kind=8), ALLOCATABLE, DIMENSION (:) :: pressures, temperatures
  
  
! ---------------------------------
! Partial columns and T-derivatives
! ---------------------------------
  REAL(kind=8), ALLOCATABLE, DIMENSION (:)         :: aircolumns
  REAL(kind=8), ALLOCATABLE, DIMENSION (:)         :: daircolumns_dT
  REAL(kind=8), ALLOCATABLE, DIMENSION (:,:)       :: gas_partialcolumns
  REAL(kind=8), ALLOCATABLE, DIMENSION (:)         :: gas_totalcolumns

! =============================
! *****************************
! =============================
! End profile related variables
! =============================
! *****************************
! =============================
   REAL(KIND=8), ALLOCATABLE, DIMENSION(:,:) :: opdeps
   REAL(KIND=8), ALLOCATABLE, DIMENSION(:,:) :: ssalbs

! ----------------------------
! Exception handling variables
! ----------------------------
!  Tool status

   logical            :: fail
   INTEGER            :: nmessages
   CHARACTER(Len=200) :: messages (maxmessages)

! -------------------
! Geocape Tool output 
! -------------------

!  I-component of Stokes vector + Jacobians
   REAL(kind=8), ALLOCATABLE, DIMENSION(:,:,:,:) :: GC_Radiances
   REAL(kind=8), ALLOCATABLE, DIMENSION(:,:,:,:) :: GC_flux
   REAL(kind=8), ALLOCATABLE, DIMENSION(:,:,:,:) :: GC_direct_flux

   REAL(kind=8), ALLOCATABLE, DIMENSION(:,:,:,:,:)   :: GC_AMFs ! do_AMF_calculation
   REAL(kind=8), ALLOCATABLE, DIMENSION(:,:,:,:,:)   :: GC_Temperature_Jacobians ! do_T_Jacobians
   REAL(kind=8), ALLOCATABLE, DIMENSION(:,:,:,:,:,:) :: GC_Tracegas_Jacobians ! do_Jacobians
   REAL(kind=8), ALLOCATABLE, DIMENSION(:,:,:,:,:)   :: GC_Scattering_Weights ! do_AMF_calculation
   REAL(kind=8), ALLOCATABLE, DIMENSION(:,:,:,:,:)   :: GC_cod_Jacobians ! do_cod_Jacobians
   REAL(kind=8), ALLOCATABLE, DIMENSION(:,:,:,:,:)   :: GC_cssa_Jacobians ! do_cssa_Jacobians
   
   REAL(kind=8), ALLOCATABLE, DIMENSION(:,:,:,:)   :: GC_cfrac_Jacobians ! do_cfrac_jacobians
   REAL(kind=8), ALLOCATABLE, DIMENSION(:,:,:,:,:) :: GC_Surfalbedo_Jacobians ! do_jacobians
   REAL(kind=8), ALLOCATABLE, DIMENSION(:,:,:,:)   :: GC_Windspeed_Jacobians ! do_jacobians
   REAL(kind=8), ALLOCATABLE, DIMENSION(:,:,:,:)   :: GC_sfcprs_Jacobians  ! do_sfcprs_Jacobians
   
  ! Option to archive full profile air density jacobians 
  REAL(kind=8), ALLOCATABLE, DIMENSION(:,:,:,:,:)   :: GC_airdens_Jacobians   ! do_airdens_jac
  REAL(kind=8), ALLOCATABLE, DIMENSION(:,:,:,:,:)   :: GC_airdens_QJacobians  ! do_airdens_jac
  REAL(kind=8), ALLOCATABLE, DIMENSION(:,:,:,:,:)   :: GC_airdens_UJacobians  ! do_airdens_jac


   ! New AOD 
   REAL(kind=8), ALLOCATABLE, DIMENSION(:,:,:,:,:)   :: GC_aod_Jacobians     ! do_aod_Jacobians .AND. do_total_aod_jac
   REAL(kind=8), ALLOCATABLE, DIMENSION(:,:,:,:,:,:) :: GC_spcaod_Jacobians  ! do_aod_Jacobians 
   REAL(kind=8), ALLOCATABLE, DIMENSION(:,:,:,:,:,:) :: GC_spcaod_QJacobians ! do_aod_Jacobians .AND. do_stokes_ad34
   REAL(kind=8), ALLOCATABLE, DIMENSION(:,:,:,:,:,:) :: GC_spcaod_UJacobians ! do_aod_Jacobians .AND. do_stokes_ad34
   REAL(kind=8), ALLOCATABLE, DIMENSION(:,:,:,:,:)   :: GC_assa_Jacobians ! do_assa_Jacobians
   REAL(kind=8), ALLOCATABLE, DIMENSION(:,:,:,:,:,:) :: GC_spcassa_Jacobians  ! do_assa_Jacobians 
   REAL(kind=8), ALLOCATABLE, DIMENSION(:,:,:,:,:,:) :: GC_spcassa_QJacobians ! do_assa_Jacobians .AND. do_stokes_ad35
   REAL(kind=8), ALLOCATABLE, DIMENSION(:,:,:,:,:,:) :: GC_spcassa_UJacobians ! do_assa_Jacobians .AND. do_stokes_ad35
   ! RRS Jacobian (O2+N2)
!    REAL(kind=8), DIMENSION(maxlambdas,GC_maxlayers,GC_maxuserlevels,GC_maxgeometries, 2)          :: GC_rrs_Jacobian
   REAL(kind=8), ALLOCATABLE, DIMENSION(:,:,:,:,:) :: GC_rrs_Jacobian ! do_raman_jacobian
   
!  Suggested additional code for Q and U. etc ......................
!    ---Only bothering with Q/U Jacobians for the trace gases.............. GRONK !

   REAL(kind=8), ALLOCATABLE, DIMENSION(:,:,:,:) :: GC_Qvalues ! do_vector_calculation .AND. do_StokesQU_output
   REAL(kind=8), ALLOCATABLE, DIMENSION(:,:,:,:) :: GC_Uvalues ! do_vector_calculation .AND. do_StokesQU_output
   REAL(kind=8), ALLOCATABLE, DIMENSION(:,:,:,:) :: GC_Qflux   ! do_vector_calculation .AND. do_StokesQU_output 
   REAL(kind=8), ALLOCATABLE, DIMENSION(:,:,:,:) :: GC_Qdirect_flux ! do_vector_calculation .AND. do_StokesQU_output 
   REAL(kind=8), ALLOCATABLE, DIMENSION(:,:,:,:) :: GC_Uflux   ! do_vector_calculation .AND. do_StokesQU_output 
   REAL(kind=8), ALLOCATABLE, DIMENSION(:,:,:,:) :: GC_Udirect_flux ! do_vector_calculation .AND. do_StokesQU_output 
   
   REAL(kind=8), ALLOCATABLE, DIMENSION(:,:,:,:,:,:) :: GC_Tracegas_QJacobians ! do_QU_Jacobians 
   REAL(kind=8), ALLOCATABLE, DIMENSION(:,:,:,:,:,:) :: GC_Tracegas_UJacobians ! do_QU_Jacobians 
   REAL(kind=8), ALLOCATABLE, DIMENSION(:,:,:,:,:)   :: GC_aod_QJacobians ! do_aod_Jacobians .AND. do_QU_Jacobians
   REAL(kind=8), ALLOCATABLE, DIMENSION(:,:,:,:,:)   :: GC_aod_UJacobians ! do_aod_Jacobians .AND. do_QU_Jacobians
   REAL(kind=8), ALLOCATABLE, DIMENSION(:,:,:,:,:)   :: GC_assa_QJacobians ! do_assa_Jacobians .AND. do_QU_Jacobians
   REAL(kind=8), ALLOCATABLE, DIMENSION(:,:,:,:,:)   :: GC_assa_UJacobians ! do_assa_Jacobians .AND. do_QU_Jacobians
   REAL(kind=8), ALLOCATABLE, DIMENSION(:,:,:,:,:)   :: GC_cod_QJacobians ! do_cod_Jacobians .AND. do_QU_Jacobians
   REAL(kind=8), ALLOCATABLE, DIMENSION(:,:,:,:,:)   :: GC_cod_UJacobians ! do_cod_Jacobians .AND. do_QU_Jacobians
   REAL(kind=8), ALLOCATABLE, DIMENSION(:,:,:,:,:)   :: GC_cssa_QJacobians ! do_cssa_Jacobians .AND. do_QU_Jacobians
   REAL(kind=8), ALLOCATABLE, DIMENSION(:,:,:,:,:)   :: GC_cssa_UJacobians ! do_cssa_Jacobians .AND. do_QU_Jacobians
   
   
   
   
   REAL(kind=8), ALLOCATABLE, DIMENSION(:,:,:,:)   :: GC_cfrac_QJacobians ! do_cfrac_jacobians .AND. do_QU_Jacobians
   REAL(kind=8), ALLOCATABLE, DIMENSION(:,:,:,:)   :: GC_cfrac_UJacobians ! do_cfrac_jacobians .AND. do_QU_Jacobians
   REAL(kind=8), ALLOCATABLE, DIMENSION(:,:,:,:,:) :: GC_Surfalbedo_QJacobians ! do_QU_Jacobians
   REAL(kind=8), ALLOCATABLE, DIMENSION(:,:,:,:,:) :: GC_Surfalbedo_UJacobians ! do_QU_Jacobians
   REAL(kind=8), ALLOCATABLE, DIMENSION(:,:,:,:)   :: GC_Windspeed_QJacobians ! do_QU_Jacobians
   REAL(kind=8), ALLOCATABLE, DIMENSION(:,:,:,:)   :: GC_Windspeed_UJacobians ! do_QU_Jacobians
   REAL(kind=8), ALLOCATABLE, DIMENSION(:,:,:,:)   :: GC_sfcprs_QJacobians ! do_sfcprs_Jacobians .AND. do_QU_Jacobians
   REAL(kind=8), ALLOCATABLE, DIMENSION(:,:,:,:)   :: GC_sfcprs_UJacobians ! do_sfcprs_Jacobians .AND. do_QU_Jacobians
   
   
   
   ! ===================================================================================
   ! Diagnostic handling
   ! ===================================================================================
   
   ! Logicals for output control (eventually create a diagnostic type var)
   LOGICAL :: write_view_geom
   LOGICAL :: write_footprint_info
   LOGICAL :: write_altitude_prof
   LOGICAL :: write_pres_prof
   LOGICAL :: write_temp_prof
   LOGICAL :: write_aircol_prof
   LOGICAL :: write_aod_refwvl, write_totaod_refwvl
   LOGICAL :: write_cod_refwvl
   LOGICAL :: write_od_prof
   LOGICAL :: write_ssa_prof
   LOGICAL :: write_aod_prof
   LOGICAL :: write_assa_prof
   LOGICAL :: write_cod_prof
   LOGICAL :: write_cssa_prof
   LOGICAL :: write_gas_prof
   LOGICAL :: write_gas_names
   LOGICAL :: write_gas_xsecs, write_xsec_deriv
   LOGICAL :: write_wvl_grid
   LOGICAL :: write_rad_spc
   LOGICAL :: write_flx_spc
   LOGICAL :: write_dflx_spc
   LOGICAL :: write_irad_spc
   LOGICAL :: write_alb_spc
   LOGICAL :: write_fix_brdf_par
   LOGICAL :: write_full_brdf
   LOGICAL :: write_sif
   LOGICAL :: write_modis_eof_par
   LOGICAL :: do_surfalb_jacobian
   LOGICAL :: do_wspd_jacobian
   LOGICAL :: do_gas_jacobians
   LOGICAL :: do_raman_jacobian
   LOGICAL :: do_oco2_sfcprs
   LOGICAL :: do_airdens_jac
   LOGICAL :: do_total_aod_jac
   LOGICAL :: do_total_assa_jac
   
   LOGICAL, DIMENSION(n_total_diag) :: do_diag
   
   ! Full name of output file
   CHARACTER(LEN=max_ch_len) :: nc_diag_outfile

   ! Options for specific diagnostics
   ! --------------------------------
   
   ! 01 - AMF Diagnostic
   TYPE Diag01_InpOptType
     LOGICAL                                :: output_scatterwt
     INTEGER                                :: n_amf
     CHARACTER(LEN=max_ch_len), ALLOCATABLE :: amf_spc(:)
     INTEGER,                   ALLOCATABLE :: amf_spc_idx(:)
   ENDTYPE Diag01_InpOptType
   TYPE( Diag01_InpOptType ) :: Diag01_InpOpt
   
   ! 16 - Gas partial Columns diagnostic
   TYPE DiagGas_InpOptType
     INTEGER                                :: n_gas
     CHARACTER(LEN=max_ch_len), ALLOCATABLE :: spc(:)
     INTEGER,                   ALLOCATABLE :: spc_idx(:)
   ENDTYPE DiagGas_InpOptType
   TYPE( DiagGas_InpOptType ) :: Diag16_InpOpt, Diag18_InpOpt, Diag32_InpOpt
   
   ! Even though they arent gases we will use this for some aerosol quantities
   TYPE( DiagGas_InpOptType ) :: Diag08_InpOpt
   
   ! Aerosol/Cloud Jacobians
   TYPE DiagAer_InpOptType
     INTEGER                                :: nspc,      npar,       njac
     CHARACTER(LEN=max_ch_len), ALLOCATABLE :: spc(:),    par(:),     jac(:)
     INTEGER,                   ALLOCATABLE :: spc_idx(:),par_idx(:), jac_idx(:), wfidx(:)
   ENDTYPE DiagAer_InpOptType
   
   TYPE DiagSurf_InpOptType
     LOGICAL                                :: do_factor_jac, do_par_jac
     INTEGER                                :: njac
     CHARACTER(LEN=max_ch_len), ALLOCATABLE :: jac_name(:)
     REAL(KIND=8),              ALLOCATABLE :: var(:,:)  ! For holding normalized jacobian values 
   ENDTYPE DiagSurf_InpOptType
   
   TYPE( DiagSurf_InpOptType ) :: Diag28_InpOpt

   ! Need array to hold phase moments for aerosol diag
   REAL(KIND=8), ALLOCATABLE, DIMENSION(:,:) :: aerdiag_phasemoms
   REAL(KIND=8)                              :: aerdiag_tau, aerdiag_ssa, aerdiag_qext
   
   TYPE( DiagAer_InpOptType ) :: Diag34_InpOpt, Diag35_InpOpt
   
   ! Archiving arrays for certain diagnostics
   REAL(KIND=8), ALLOCATABLE :: ad09(:),   ad10(:,:), ad11(:,:), &
                                ad14(:,:), ad15(:,:)
   
   ! Individual Stokes output control
   LOGICAL :: do_stokes_ad20, do_stokes_ad21, do_stokes_ad22, &
              do_stokes_ad28, do_stokes_ad29, do_stokes_ad31, &
              do_stokes_ad32, do_stokes_ad33, do_stokes_ad34, &
              do_stokes_ad35, do_stokes_ad36, do_stokes_ad37, &
              do_stokes_ad38, do_stokes_ad39

   ! Diag 25 - Save kernel parameters
   LOGICAL :: diag25_write_kpar
   
! ----------------------------------------------------------
! Optional output of Q and U values and their linearizations
!    ( only if the vector calculation flag has been set )
! ----------------------------------------------------------
   LOGICAL :: do_StokesQU_output

! ----------------------------------------------------------
! Jacobian flags (trace gas, temperatures, surface pressure)
! ----------------------------------------------------------
   LOGICAL :: do_Jacobians, do_QU_Jacobians
   LOGICAL :: do_AMF_calculation
   LOGICAL :: do_T_Jacobians
   LOGICAL :: do_sfcprs_Jacobians

! -----------------------------------------
! Flag for using Normalized Jacobian output
! -----------------------------------------
   LOGICAL :: do_normalized_WFoutput ! T: dI/dx*x, F; dI/dx
   LOGICAL :: do_normalized_radiance ! T: I/F, F: I
   
   ! -------------------------------------------------------
   ! Diagnostic type for managing NetCDF output
   ! -------------------------------------------------------
   TYPE NCDimType
      CHARACTER(LEN=max_ch_len)             :: NAME
      INTEGER                               :: DIMSIZE
      INTEGER                               :: NCID
      CHARACTER(LEN=max_ch_len)             :: DESCRIPTION
   ENDTYPE NCDimType
   
   TYPE( NCDimType ), ALLOCATABLE           :: nc_dimlist(:)
   INTEGER                                  :: nc_ndimlist
   
! --------------------------------------
! Local variables for optical properties
! --------------------------------------
   INTEGER                             :: nscatter
   REAL(kind=8), DIMENSION(maxscatter) :: scaco_input
   REAL(kind=8), DIMENSION(0:maxmoms, 1:maxgksec, maxscatter)     :: phasmoms_input
   REAL(kind=8), DIMENSION(0:maxmoms, 1:maxgksec)                 :: phasmoms_total_input
   REAL(kind=8), DIMENSION(      0:2, 1:maxgksec,          2)     :: phasmoms_rrs
   
   REAL(kind=8) :: depol, beta_2, pRay_12, pRay_22, pRay_52, pRay_41
   REAL(kind=8) :: total_sca, omega, total_tau, total_wf, total_Qwf, total_Uwf
   REAL(kind=8) :: total_molabs, total_molsca, total_moltau
   REAL(kind=8) :: total_aersca, total_aertau, total_cldtau, total_cldsca
   REAL(kind=8) :: total_vaerssa, total_vcldssa
   REAL(kind=8) :: total_gasabs(GC_maxlayers), total_dgasabsdT(GC_maxlayers), temp, temp_sq
   REAL(kind=8) :: ratio, xsec, dxsec_dT, L_gas, L_air, pvar
   REAL(kind=8) :: gasabs(GC_maxlayers,maxgases), abs_o3(GC_maxlayers)
   REAL(kind=8) :: O3_partialcolumns(GC_maxlayers)
   INTEGER      :: aerscaidx, cldscaidx         ! aerosol and clouds indices in the scatters
   ! Index for weighting functions
   INTEGER      :: gaswfidx, twfidx, aodwfidx, assawfidx,  codwfidx, cssawfidx, sfcprswfidx!, ramanwfidx
   INTEGER      :: N_TOTALPROFILE_WFS_ncld, N_TOTALPROFILE_WFS_wcld
   
   ! RRS
   INTEGER      :: rrswfidx
   
   ! For the Raman WF
   REAL(kind=8) :: rrs_depol, rrsbeta_2, pRam_12, pRam_22, pRam_52, pRam_41
   REAL(kind=8) :: total_o2rrs, total_n2rrs
  
   ! --------------
   ! Time variables
   ! --------------
   INTEGER(KIND=4) :: hour, minute
   REAL(KIND=8)    :: second, tmzn
   
   ! --------------
   ! Help variables
   ! --------------
   INTEGER       :: w, v, g, n, i, j, k, q, du, nspec, ib, um, ua, il, ic, nw1
   REAL(kind=8)  :: deltp
   !REAL(kind=8), DIMENSION(maxlambdas, GC_maxgeometries) :: temp_rad
   !REAL(kind=8), DIMENSION(maxlambdas, GC_maxlayers)     :: temp_wf
   REAL(kind=8), ALLOCATABLE, DIMENSION(:,:) :: temp_rad
   REAL(kind=8), ALLOCATABLE, DIMENSION(:,:) :: temp_wf
   LOGICAL :: first_xy ! Logical for first spectrum simulation
   
   CHARACTER(LEN=256) :: surface_data_path
   
   ! --------------
   ! Error handling
   ! --------------
   LOGICAL :: yn_error
   
   ! -----------------------
   ! File names for the data
   ! -----------------------   
   CHARACTER(LEN=256) :: fnamep, unitname, fname, varname, ds_fname

   INTEGER            :: unitlen, flen, varlen

   ! --------------------------
   ! VBRDF supplement variables
   ! --------------------------
   LOGICAL :: DO_DEBUG_RESTORATION
   INTEGER :: BS_NMOMENTS_INPUT
   
   ! =========================
   ! *************************
   ! =========================
   ! Vlidort related varaibles
   ! =========================
   ! *************************
   ! =========================  

   ! ---------------------------------
   ! Filename for Vlidort control file
   ! ---------------------------------
   CHARACTER(LEN=max_ch_len) :: vlidort_control_file
   CHARACTER(LEN=max_ch_len) :: vlidort_vbrdf_control_file
   LOGICAL                   :: OPENERRORFILEFLAG
   INTEGER                   :: NA,NF

   ! VLIDORT file inputs status structure   
   TYPE(VLIDORT_Input_Exception_Handling) :: VLIDORT_InputStatus

   ! VLIDORT input structures   
   TYPE(VLIDORT_Fixed_Inputs)             :: VLIDORT_FixIn
   TYPE(VLIDORT_Modified_Inputs)          :: VLIDORT_ModIn

   ! VLIDORT linearized input structures
   TYPE(VLIDORT_Fixed_LinInputs)          :: VLIDORT_LinFixIn
   TYPE(VLIDORT_Modified_LinInputs)       :: VLIDORT_LinModIn

   ! VLIDORT output structure
   TYPE(VLIDORT_Outputs)                  :: VLIDORT_Out

   ! VLIDORT supplements i/o structure
   TYPE(VLIDORT_Sup_InOut)                :: VLIDORT_Sup

   ! VLIDORT linearized supplements i/o structure
   TYPE(VLIDORT_LinSup_InOut)             :: VLIDORT_LinSup

   ! VLIDORT linearized output structure
   TYPE(VLIDORT_LinOutputs)               :: VLIDORT_LinOut

   ! VBRDF supplement input structure
   TYPE(VBRDF_Sup_Inputs)                 :: VBRDF_Sup_In

   ! VBRDF supplement linearized input structure
   TYPE(VBRDF_LinSup_Inputs)              :: VBRDF_LinSup_In

   !  VLIDORT VBRDF supplement output structure
   TYPE(VBRDF_Sup_Outputs)                :: VBRDF_Sup_Out
   
   !  VLIDORT VBRDF supplement linearized output structure
   TYPE(VBRDF_LinSup_Outputs)             :: VBRDF_LinSup_Out

   ! VBRDF supplement input status
   TYPE(VBRDF_Input_Exception_Handling)   :: VBRDF_Sup_InputStatus

   !  VLIDORT VBRDF supplement output status structure
   TYPE(VBRDF_Output_Exception_Handling)  :: VBRDF_Sup_OutputStatus
   
   !  VBRDF supplement / VLIDORT VBRDF-related inputs consistency check status
   TYPE(VLIDORT_Exception_Handling)       :: VLIDORT_VBRDFCheck_Status


   ! =============================
   ! *****************************
   ! =============================
   ! End vlidort related varaibles
   ! =============================
   ! *****************************
   ! =============================

CONTAINS

  SUBROUTINE deallocate_variables

    IMPLICIT NONE

    INTEGER :: i

    ! Variables allocated in GC_profiles_module
    IF (ALLOCATED(heights)) DEALLOCATE(heights,pressures,temperatures, &
         mid_heights,mid_pressures,mid_temperatures,aircolumns,daircolumns_dT, &
         rh_profile,gas_partialcolumns,gas_totalcolumns)
    IF (ALLOCATED(GasAbsXS)) THEN
       DO i = 1, ngases
          IF(ALLOCATED(GasAbsXS(i)%Xsect)) DEALLOCATE(GasAbsXS(i)%Xsect)
       END DO
    END IF
    IF (ALLOCATED(gas_xsecs)) DEALLOCATE(gas_xsecs,o3c1_xsecs,o3c2_xsecs, &
         Rayleigh_xsecs,Rayleigh_depols,epsilon_air)
    IF( ALLOCATED(gas_xsecs_deriv)) DEALLOCATE( gas_xsecs_deriv )
    IF (ALLOCATED(cld_bots)) DEALLOCATE(cld_bots,cld_tops,cld_taus,cld_flags, &
         cld_lowers,cld_uppers,cld_opdeps,cld_ssalbs,cld_profile,tcld_profile, &
         cld_relqext,cld_phfcn)
    IF (ALLOCATED(aer_profile)) DEALLOCATE(aer_profile,aer_d_profile_dtau, &
         aer_d_profile_dpkh,aer_d_profile_dhfw,aer_d_profile_drel, &
         aer_profile0,taer_profile,aer_flags,aer_opdeps,aer_ssalbs,aer_relqext, &
         aer_phfcn)
    IF (ALLOCATED(opdeps)) DEALLOCATE(opdeps, ssalbs, ground_ler)
    IF (ALLOCATED(sif_spc)) DEALLOCATE(sif_spc)

    ! Variables allocated in GC_netcdf_module
    IF (ALLOCATED(gc_radiances)) DEALLOCATE(gc_radiances,gc_flux,gc_direct_flux, &
         GC_Tracegas_Jacobians,GC_Windspeed_Jacobians)
    IF( ALLOCATED(GC_Surfalbedo_Jacobians) ) DEALLOCATE(GC_Surfalbedo_Jacobians)
    IF (ALLOCATED(GC_AMFs)) DEALLOCATE(GC_AMFs,GC_Scattering_Weights)
    IF (ALLOCATED(GC_Qvalues)) DEALLOCATE(GC_Qvalues,GC_Uvalues,GC_Qflux,GC_Uflux, &
         GC_Qdirect_flux,GC_Udirect_flux,GC_Windspeed_QJacobians,GC_Windspeed_UJacobians)
    IF( ALLOCATED(GC_surfalbedo_QJacobians ) ) DEALLOCATE( &
         GC_surfalbedo_QJacobians,GC_surfalbedo_UJacobians ) 
    IF (ALLOCATED(GC_Tracegas_QJacobians)) DEALLOCATE(GC_Tracegas_QJacobians, &
         GC_Tracegas_UJacobians)
    IF (ALLOCATED(GC_Temperature_Jacobians)) DEALLOCATE(GC_Temperature_Jacobians)
    IF (ALLOCATED(GC_aod_Jacobians)) DEALLOCATE(GC_aod_Jacobians)
    IF (ALLOCATED(GC_aod_QJacobians)) DEALLOCATE(GC_aod_QJacobians,GC_aod_UJacobians)
    IF (ALLOCATED(GC_assa_Jacobians)) DEALLOCATE(GC_assa_Jacobians)
    IF (ALLOCATED(GC_assa_QJacobians)) DEALLOCATE(GC_assa_QJacobians,GC_assa_UJacobians)
    IF (ALLOCATED(GC_cod_Jacobians)) DEALLOCATE(GC_cod_Jacobians)
    IF (ALLOCATED(GC_cod_QJacobians)) DEALLOCATE(GC_cod_QJacobians,GC_cod_UJacobians)
    IF (ALLOCATED(GC_cssa_Jacobians)) DEALLOCATE(GC_cssa_Jacobians)
    IF (ALLOCATED(GC_cssa_QJacobians)) DEALLOCATE(GC_cssa_QJacobians,GC_cssa_UJacobians)
    IF (ALLOCATED(GC_cfrac_Jacobians)) DEALLOCATE(GC_cfrac_Jacobians)
    IF (ALLOCATED(GC_cfrac_QJacobians)) DEALLOCATE(GC_cfrac_QJacobians,GC_cfrac_UJacobians)
    IF (ALLOCATED(GC_sfcprs_Jacobians)) DEALLOCATE(GC_sfcprs_Jacobians)
    IF (ALLOCATED(GC_sfcprs_QJacobians)) DEALLOCATE(GC_sfcprs_QJacobians,GC_sfcprs_UJacobians)
    IF (ALLOCATED(GC_rrs_Jacobian)) DEALLOCATE(GC_rrs_Jacobian)
    IF (ALLOCATED(GC_spcaod_Jacobians)) DEALLOCATE(GC_spcaod_Jacobians)
    IF (ALLOCATED(GC_spcaod_QJacobians)) DEALLOCATE(GC_spcaod_QJacobians)
    IF (ALLOCATED(GC_spcaod_UJacobians)) DEALLOCATE(GC_spcaod_UJacobians)
    IF (ALLOCATED(GC_spcassa_Jacobians)) DEALLOCATE(GC_spcassa_Jacobians)
    IF (ALLOCATED(GC_spcassa_Jacobians)) DEALLOCATE(GC_spcassa_QJacobians)
    IF (ALLOCATED(GC_spcassa_Jacobians)) DEALLOCATE(GC_spcassa_UJacobians)
    
    IF (ALLOCATED(GC_airdens_Jacobians)) DEALLOCATE(GC_airdens_Jacobians)
    IF (ALLOCATED(GC_airdens_QJacobians)) DEALLOCATE(GC_airdens_QJacobians,GC_airdens_UJacobians)

    ! Variables allocated in GC_solar_module
    IF (ALLOCATED(solar_spec_data)) DEALLOCATE(solar_spec_data,solar_cspec_data)

    ! Variables allocated in GC_xsections_module
    IF (ALLOCATED(rrs_xsecs)) DEALLOCATE(rrs_xsecs,rrs_depols,rrs_gasname,rrs_gas)

    ! Variables allocated in GC_SciaUSGS_FA_module
    IF(ALLOCATED(fa_mu)     ) DEALLOCATE( fa_mu      )
    IF(ALLOCATED(fa_mu_m)   ) DEALLOCATE( fa_mu_m    )
    IF(ALLOCATED(fa_wvl)    ) DEALLOCATE( fa_wvl     )
    IF(ALLOCATED(fa_wvl_r8) ) DEALLOCATE( fa_wvl_r8  )
    IF(ALLOCATED(fa_W)      ) DEALLOCATE( fa_W       )
    IF(ALLOCATED(fa_G)      ) DEALLOCATE( fa_G       )
    IF(ALLOCATED(fa_lon)    ) DEALLOCATE( fa_lon     )
    IF(ALLOCATED(fa_lat)    ) DEALLOCATE( fa_lat     )
    IF(ALLOCATED(fa_exist)  ) DEALLOCATE( fa_exist   )
    IF(ALLOCATED(fa_mu_int) ) DEALLOCATE( fa_mu_int  )
    IF(ALLOCATED(fa_W_int)  ) DEALLOCATE( fa_W_int   )
    IF(ALLOCATED(fa_tmp)    ) DEALLOCATE( fa_tmp     )
    IF(ALLOCATED(fa_tmp_int)) DEALLOCATE( fa_tmp_int )
    IF(ALLOCATED(usgs_water_int) ) DEALLOCATE(usgs_water_int)
    IF(ALLOCATED(usgs_snowf_int) ) DEALLOCATE(usgs_snowf_int)
    IF(ALLOCATED(usgs_snowc_int) ) DEALLOCATE(usgs_snowc_int)
    IF(ALLOCATED(out_snowspec)) DEALLOCATE(out_snowspec)
    IF(ALLOCATED(bs_dbounce_brdfunc)) DEALLOCATE(bs_dbounce_brdfunc)
    IF(ALLOCATED(bs_brdf_f)) DEALLOCATE(bs_brdf_f)
    IF(ALLOCATED(bs_brdf_f_0)) DEALLOCATE(bs_brdf_f_0)
    IF(ALLOCATED(bs_user_brdf_f)) DEALLOCATE(bs_user_brdf_f)
    IF(ALLOCATED(bs_user_brdf_f_0)) DEALLOCATE(bs_user_brdf_f_0)
    IF(ALLOCATED(nc_dimlist)) DEALLOCATE(nc_dimlist)
    
  END SUBROUTINE deallocate_variables
 
END MODULE GC_variables_module
