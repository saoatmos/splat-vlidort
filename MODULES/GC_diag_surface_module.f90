!------------------------------------------------------------------------------
!          Harvard University Atmospheric Chemistry Modeling Group            !
!------------------------------------------------------------------------------
!BOP
!
! !MODULE: GC_diag_surface_module.f90
!
! !DESCRIPTION: This module contains routines for writing surface diagnostics
!\\
!\\
! !INTERFACE: 
!
MODULE GC_diag_surface_module

  USE GC_parameters_module
  USE GC_netcdf_module,      ONLY : create_ncvar, write_ncvar, &
                                      match_names_in_dimlist,  &
                                      nc_fld_1d, nc_fld_2d,    &
                                      nc_fld_3d
    
  IMPLICIT NONE
  INCLUDE 'netcdf.inc'
  
  CONTAINS
  
  SUBROUTINE do_surface_diag( NCID, ACTION, DO_XY, DIAG_NUM )
    
    USE GC_variables_module, ONLY : ground_ler, sif_spc, modis_pix_lfrac
    
    ! ---------------------------
    ! Subroutine arguments
    ! ---------------------------
    INTEGER, INTENT(IN) :: NCID
    INTEGER, INTENT(IN) :: ACTION
    LOGICAL, INTENT(IN) :: DO_XY
    INTEGER, INTENT(IN) :: DIAG_NUM
    
    ! ---------------
    ! Local variables
    ! ---------------
    INTEGER                                 :: VAR_ID
    LOGICAL                                 :: error, dummy_err
    INTEGER, DIMENSION(1)                   :: size_1d, tmp_1d
    CHARACTER(LEN=max_ch_len), DIMENSION(1) :: dim_1d
    REAL(KIND=8),              DIMENSION(1) :: lftmp
    
    ! =====================================================
    ! do_surface_diag starts here
    ! =====================================================
    
    ! Initialize error
    error = .FALSE.
    
    SELECTCASE( DIAG_NUM )
    
    ! ======================================================
    ! Surface Albedo
    ! ======================================================
    CASE( 24 )
      
      ! The dimension of the output
      dim_1d(1) = 'nw'
        
      ! Get dimension
      CALL match_names_in_dimlist( dim_1d, 1, tmp_1d, size_1d, dummy_err )
      
      CALL nc_fld_1d( dim_1d, size_1d(1), ground_ler(1:size_1d(1)), 'surfalb', &
                      NCID,   ACTION,     DO_XY                                )
    
    ! ======================================================
    ! BRDF kernels
    ! ======================================================
    CASE( 25 )
      
      CALL diag_25( NCID, ACTION, DO_XY )
      
    ! ======================================================
    ! SIF
    ! ======================================================
    CASE( 26 )
      
      ! The dimension of the output
      dim_1d(1) = 'nw'
      
      ! Get dimension
      CALL match_names_in_dimlist( dim_1d, 1, tmp_1d, size_1d, dummy_err )
      
      CALL nc_fld_1d( dim_1d, size_1d(1), sif_spc(1:size_1d(1)), 'sif', &
                      NCID,   ACTION,     DO_XY                         )
      
    ! ======================================================
    ! MODIS Land Fraction
    ! ======================================================
    CASE( 27 )
      
      ! The dimension of the output
      dim_1d(1) = 'one'
      
      ! Get dimension
      CALL match_names_in_dimlist( dim_1d, 1, tmp_1d, size_1d, dummy_err )
      
      ! put as array
      lftmp(1) = modis_pix_lfrac
      CALL nc_fld_1d( dim_1d, size_1d(1), lftmp, 'land_fraction', &
                      NCID,   ACTION,     DO_XY                   )
      
    CASE DEFAULT
        
        print*,'The selected diagnostic is not in the surface diags:', diag_num
        STOP
        
    ENDSELECT
    
  END SUBROUTINE do_surface_diag
  
  SUBROUTINE do_surface_jacobians( NCID, ACTION, DO_XY, DIAG_NUM )
    
    USE GC_variables_module, ONLY : do_stokes_ad28, do_stokes_ad29,  &
                                    do_QU_Jacobians,                 &
                                    GC_Surfalbedo_Jacobians,         &
                                    GC_Surfalbedo_QJacobians,        &
                                    GC_Surfalbedo_UJacobians,        &
                                    GC_Windspeed_Jacobians,          &
                                    GC_Windspeed_QJacobians,         &
                                    GC_Windspeed_UJacobians,         &
                                    VLIDORT_FixIn, didx,             &
                                    Diag28_InpOpt
    ! ---------------------------
    ! Subroutine arguments
    ! ---------------------------
    INTEGER, INTENT(IN) :: NCID
    INTEGER, INTENT(IN) :: ACTION
    LOGICAL, INTENT(IN) :: DO_XY
    INTEGER, INTENT(IN) :: DIAG_NUM
    
    ! ---------------
    ! Local variables
    ! ---------------
    INTEGER                                 :: VAR_ID
    LOGICAL                                 :: error, dummy_err
    INTEGER, DIMENSION(1)                   :: size_1d, tmp_1d
    CHARACTER(LEN=max_ch_len), DIMENSION(1) :: dim_1d
    INTEGER, DIMENSION(2)                   :: size_2d, tmp_2d
    CHARACTER(LEN=max_ch_len), DIMENSION(2) :: dim_2d
    INTEGER, DIMENSION(3)                   :: size_3d, tmp_3d
    CHARACTER(LEN=max_ch_len), DIMENSION(3) :: dim_3d
    INTEGER                                 :: n

    ! =====================================================
    ! do_surface_jacobians starts here
    ! =====================================================
    
    SELECTCASE( DIAG_NUM )
    
    ! ======================================================
    ! Surface albedo jacobian
    ! ======================================================
    CASE( 28 )
      
      ! Define dimensions
      dim_3d(1) = 'nw' ; dim_3d(2) = 'ngeom' ; dim_3d(3) = 'noutputlevel'
      
      ! Get dimensions
      CALL match_names_in_dimlist( dim_3d, 3, tmp_3d, size_3d, dummy_err )
      
      DO n=1,Diag28_InpOpt%njac
        
        CALL nc_fld_3d( dim_3d, size_3d(1), size_3d(2), size_3d(3),                                &
                          GC_Surfalbedo_Jacobians(1:size_3d(1),1:size_3d(2),1:size_3d(3),n,didx),  &
                          TRIM(ADJUSTL(Diag28_InpOpt%jac_name(n))) // '_jac',                      &
                          NCID, ACTION, DO_XY                                                      )
        
        IF( do_stokes_ad28 .and. do_QU_Jacobians ) THEN
          
          CALL nc_fld_3d( dim_3d, size_3d(1), size_3d(2), size_3d(3),                              &
                          GC_Surfalbedo_QJacobians(1:size_3d(1),1:size_3d(2),1:size_3d(3),n,didx), &
                          TRIM(ADJUSTL(Diag28_InpOpt%jac_name(n))) // '_qjac',                     &
                          NCID, ACTION, DO_XY                                                      )
          
          CALL nc_fld_3d( dim_3d, size_3d(1), size_3d(2), size_3d(3),                              &
                          GC_Surfalbedo_UJacobians(1:size_3d(1),1:size_3d(2),1:size_3d(3),n,didx), &
                          TRIM(ADJUSTL(Diag28_InpOpt%jac_name(n))) // '_ujac',                     &
                          NCID, ACTION, DO_XY                                                      )
          
        ENDIF


      ENDDO
      
    ! ======================================================
    ! Wind speed jacobian (ocean kernels)
    ! ======================================================
    CASE( 29 )
      
      ! Define dimensions
      dim_3d(1) = 'nw' ; dim_3d(2) = 'ngeom' ; dim_3d(3) = 'noutputlevel'
      
      ! Get dimensions
      CALL match_names_in_dimlist( dim_3d, 3, tmp_3d, size_3d, dummy_err )
      
      CALL nc_fld_3d( dim_3d, size_3d(1), size_3d(2), size_3d(3),                           &
                      GC_Windspeed_Jacobians(1:size_3d(1),1:size_3d(2),1:size_3d(3),didx),  &
                      'windspeed_jac',                                                      &
                      NCID, ACTION, DO_XY                                                   )
        
      IF( do_stokes_ad29 .and. do_QU_Jacobians ) THEN
        
        CALL nc_fld_3d( dim_3d, size_3d(1), size_3d(2), size_3d(3),                            &
                        GC_Windspeed_QJacobians(1:size_3d(1),1:size_3d(2),1:size_3d(3),didx),  &
                        'windspeed_qjac',                                                      &
                        NCID, ACTION, DO_XY                                                    )
          
        CALL nc_fld_3d( dim_3d, size_3d(1), size_3d(2), size_3d(3),                            &
                        GC_Windspeed_UJacobians(1:size_3d(1),1:size_3d(2),1:size_3d(3),didx),  &
                        'windspeed_ujac',                                                      &
                        NCID, ACTION, DO_XY                                                    )
          
      ENDIF
      
    CASE DEFAULT
        
      print*,'The selected diagnostic is not in the surface jacobian  diags:', diag_num
      STOP
        
    ENDSELECT
    
  END SUBROUTINE do_surface_jacobians
  
  SUBROUTINE diag_25( NCID, ACTION, DO_XY )
    
    USE GC_variables_module,   ONLY : do_fixed_brdf, do_eof_brdf, nlambdas,&
                                      fkern_idx, fkern_amp, fkern_par, &
                                      modis_pix_kern, modis_pix_lfrac, &
                                      diag25_write_kpar, fkern_amp_wvl, &
                                      VBRDF_Sup_In, use_brdf_clim, &
                                      fkern_par_wvl, use_cmglint, &
                                      wind_speed, salinity
    USE GC_SciaUSGS_FA_module, ONLY : compute_SciaUSGS_Coeff
    
    ! ---------------------------
    ! Subroutine arguments
    ! ---------------------------
    INTEGER, INTENT(IN) :: NCID
    INTEGER, INTENT(IN) :: ACTION
    LOGICAL, INTENT(IN) :: DO_XY
    
    ! ---------------
    ! Local variables
    ! ---------------
    INTEGER                                 :: VAR_ID, N, I, J
    LOGICAL                                 :: error, dummy_err
    INTEGER, DIMENSION(1)                   :: size_1d, tmp_1d
    CHARACTER(LEN=max_ch_len), DIMENSION(1) :: dim_1d
    CHARACTER(LEN=max_ch_len)               :: fld_name
    CHARACTER(LEN=max_ch_len)               :: numstr
    REAL(KIND=8), DIMENSION(nlambdas)       :: kern
    
    
    ! Parameters for EOF kernels
    INTEGER, DIMENSION(4), PARAMETER :: sfa_kern_idx = (/1,3,4,15/)
    
    ! =====================================================
    ! diag_25 starts here
    ! =====================================================
    
    ! The dimension of the output
    dim_1d(1) = 'nw'
        
    ! Get dimension
    CALL match_names_in_dimlist( dim_1d, 1, tmp_1d, size_1d, dummy_err )
    
    IF( do_fixed_brdf ) THEN
      
      DO n=1,3
        
        ! Kernel name
        fld_name = 'BRDF_f_' // short_kern_name( fkern_idx(n) )
        
        kern(:) = fkern_amp(n)
        
        CALL nc_fld_1d( dim_1d, size_1d(1), kern(1:size_1d(1)), TRIM(ADJUSTL(fld_name)), &
                      NCID,   ACTION,     DO_XY                                          )
        
        
      ENDDO
      
      
      
    ENDIF
    
    IF( do_eof_brdf ) THEN
      
      ! These values are constant for MODIS
      fkern_idx(:) = (/ 1,3,4/)
      fkern_par(:,:) = 0.0d0 ! All par = 0 except for the LiSparse Kernel
      fkern_par(3,1) = 2.0 ; fkern_par(3,2) = 1.0 ! Geo
      !fkern_par(4,1) = wind_speed ; fkern_par(4,2) = salinity
      DO n=1,4
        
        ! Kernel name
        fld_name = 'BRDF_f_' // short_kern_name( sfa_kern_idx(n) )
        
        ! 
        CALL nc_fld_1d( dim_1d, size_1d(1), fkern_amp_wvl(1:size_1d(1),n), &
                        TRIM(ADJUSTL(fld_name)), NCID,   ACTION,   DO_XY   )
      
      ENDDO
      
    ENDIF

    IF( use_cmglint ) THEN
    
      fkern_idx(1) = 16
      fkern_par(1,1) = wind_speed ; fkern_par(1,2) = salinity


    ENDIF
    
    IF( use_brdf_clim ) THEN
      
      DO n=1,VBRDF_Sup_In%BS_N_BRDF_KERNELS
        
        ! Kernel name
        fld_name = 'BRDF_f_' // short_kern_name( fkern_idx(n) )
        
        ! 
        CALL nc_fld_1d( dim_1d, size_1d(1), fkern_amp_wvl(1:size_1d(1),n), &
                        TRIM(ADJUSTL(fld_name)), NCID,   ACTION,   DO_XY   )
        
        
        
      ENDDO
    ENDIF
    
    ! Write the kernel parameters
    ! ---------------------------
    
    IF( diag25_write_kpar ) THEN
      
      ! Define dimension
      dim_1d(1) = 'nw'
      
      ! Get dimension
      CALL match_names_in_dimlist( dim_1d, 1, tmp_1d, size_1d, dummy_err )
      
      ! Loop over parameters
      DO i=1,VBRDF_Sup_In%BS_N_BRDF_KERNELS
      DO j=1,VBRDF_Sup_In%BS_N_BRDF_PARAMETERS(i)
        
        ! Parameter values are fixed for these cases
        IF( do_fixed_brdf .OR. do_eof_brdf .OR. use_cmglint ) THEN
          kern(:) = fkern_par(i,j)
        ELSE
          kern(:) = fkern_par_wvl(:,i,j)
        ENDIF
        
        ! Write string
        WRITE(numstr,'(I500)') j
        
        ! Field name
        fld_name = 'BRDF_p' // TRIM(ADJUSTL(numstr)) // '_' &
                            // short_kern_name( fkern_idx(i) )
        
        ! Create field
        CALL nc_fld_1d( dim_1d, size_1d(1), kern(1:size_1d(1)),            &
                        TRIM(ADJUSTL(fld_name)), NCID,   ACTION,   DO_XY   )
        
      ENDDO
      ENDDO
      
      
    ENDIF

  END SUBROUTINE diag_25
  
  SUBROUTINE archive_surface_diag( iw, dir )
    
    USE GC_variables_module, ONLY : do_stokes_ad28,  do_stokes_ad29, &
                                    do_QU_jacobians, do_jacobians,   &
                                    GC_Surfalbedo_Jacobians,         &
                                    GC_Surfalbedo_QJacobians,        &
                                    GC_Surfalbedo_UJacobians,        &
                                    GC_Windspeed_Jacobians,          &
                                    GC_Windspeed_QJacobians,         &
                                    GC_Windspeed_UJacobians,         &
                                    VLIDORT_FixIn, ground_ler,       &
                                    VLIDORT_LINOUT, wind_speed,      &
                                    VLIDORT_Out, use_lambertian,     &
                                    do_normalized_WFoutput,          &
                                    Diag28_InpOpt, do_diag
    
    ! --------------------
    ! Subroutine arguments
    ! --------------------
    INTEGER, INTENT(IN) :: iw, dir
    
    ! ---------------
    ! Local variables
    ! ---------------
    INTEGER :: IB, UM, UA, V, q, g, n, il, nwf
    
    ! =====================================================
    ! archive_surface_diag starts here
    ! =====================================================
    
    ! ==========================================================================================
    ! DIAG 28 - Surface Albedo Jacobian
    ! ==========================================================================================
    IF( do_diag(28) ) THEN
       
        ! Shortcut for number of weighting functions
        nwf = Diag28_InpOpt%njac

       ! ----------------------------------------------------------------------
       ! Save Surface albedo Jacobian, *** Note that: always unnormalized  ****
       ! ----------------------------------------------------------------------
       IF ( nwf .GT. 0 ) THEN
          DO n = 1, nwf
          DO v = 1, VLIDORT_Out%Main%TS_N_GEOMETRIES
             IF( do_normalized_WFoutput  ) THEN
                GC_Surfalbedo_Jacobians(iw,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,n,dir) = &
                     VLIDORT_LINOUT%SURF%TS_SURFACEWF(n,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,1,dir) * Diag28_InpOpt%var(iw,n) 
             ELSE                                 !  Non-normalized output
                GC_Surfalbedo_Jacobians(iw,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,n,dir) = &
                     VLIDORT_LINOUT%SURF%TS_SURFACEWF(n,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,1,dir) 
             ENDIF
          ENDDO
          ENDDO
       ENDIF
       
       ! ----------------------------------------------------------------------------------
       ! Save Surface albedo Jacobian for Q and U, *** Note that: always unnormalized  ****
       ! ----------------------------------------------------------------------------------
       IF ( nwf .GT. 0. .AND. do_QU_Jacobians .AND. do_stokes_ad28 ) THEN
          DO n = 1, nwf
          DO v = 1, VLIDORT_Out%Main%TS_N_GEOMETRIES
             IF ( do_normalized_WFoutput ) THEN   !  Normalized output 
                GC_Surfalbedo_QJacobians(iw,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,n,dir) =  &
                     VLIDORT_LINOUT%SURF%TS_SURFACEWF(n,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,2,dir) * Diag28_InpOpt%var(iw,n) 
                GC_Surfalbedo_UJacobians(iw,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,n,dir) = &
                     VLIDORT_LINOUT%SURF%TS_SURFACEWF(n,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,3,dir) * Diag28_InpOpt%var(iw,n) 
             ELSE                                !  Non-normalized output
                GC_Surfalbedo_QJacobians(iw,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,n,dir) = &
                     VLIDORT_LINOUT%SURF%TS_SURFACEWF(n,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,2,dir)  
                GC_Surfalbedo_UJacobians(iw,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,n,dir) = &
                     VLIDORT_LINOUT%SURF%TS_SURFACEWF(n,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,3,dir)  
             ENDIF
          ENDDO
          ENDDO
       ENDIF
    ENDIF

    ! ==========================================================================================
    ! DIAG 29 - Wind Speed Jacobian (DEPRICATED- Future use as a placeholder for surface emission?)
    ! ==========================================================================================
    IF( do_diag(29) ) THEN
       
       ! -------------------------------------------------------------
       ! Save wind speed Jacobian - VLIDORT output ALWAYS unnormalized
       ! -------------------------------------------------------------
       IF ( do_Jacobians ) THEN
          DO v = 1, VLIDORT_Out%Main%TS_N_GEOMETRIES
             IF ( do_normalized_WFoutput ) THEN  !  Normalized output
                GC_Windspeed_Jacobians(iw,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,dir) = &
                     wind_speed * VLIDORT_LINOUT%SURF%TS_SURFACEWF(1,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,1,dir)
             ELSE                                !  Non-normalized output
                GC_Windspeed_Jacobians(iw,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,dir) = &
                     VLIDORT_LINOUT%SURF%TS_SURFACEWF(1,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,1,dir)
             END IF
          END DO
       END IF
       
       ! -------------------------------------------------------------------------
       ! Save wind speed Jacobian for Q and U - VLIDORT output ALWAYS unnormalized
       ! -------------------------------------------------------------------------
       IF ( do_QU_Jacobians .and. do_stokes_ad29 ) THEN
          DO v = 1, VLIDORT_Out%Main%TS_N_GEOMETRIES
             IF ( do_normalized_WFoutput ) THEN   !  Normalized output
                GC_Windspeed_QJacobians(iw,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,dir) = &
                     wind_speed * VLIDORT_LINOUT%SURF%TS_SURFACEWF(1,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,2,dir)
                GC_Windspeed_UJacobians(iw,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,dir) = &
                     wind_speed * VLIDORT_LINOUT%SURF%TS_SURFACEWF(1,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,3,dir)
             ELSE                                 !  Non-normalized output
                GC_Windspeed_QJacobians(iw,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,dir) = &
                     VLIDORT_LINOUT%SURF%TS_SURFACEWF(1,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,2,dir)
                GC_Windspeed_UJacobians(iw,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,dir) = &
                     VLIDORT_LINOUT%SURF%TS_SURFACEWF(1,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,3,dir)
             ENDIF
          ENDDO
       ENDIF
       
    ENDIF
    
  END SUBROUTINE archive_surface_diag
  
END MODULE GC_diag_surface_module