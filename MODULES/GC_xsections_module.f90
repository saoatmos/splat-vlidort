MODULE GC_xsections_module

  USE GC_parameters_module, ONLY: maxgases, maxflambdas, maxgases,   &
                                  GC_maxlayers, ctrunit, max_ch_len, &
                                  maxlambdas, maxgksec
  USE GC_variables_module,  ONLY: nmessages, messages, tmpwaves,       &
                                  GC_nlayers, mid_pressures,           &
                                  mid_temperatures, tmpcwaves,         &
                                  gas_totalcolumns, aircolumns,        &
                                  gas_partialcolumns,                  &
                                  nspec, ngases, database_dir,         &
                                  which_gases, flambdas, nflambdas,    &
                                  use_wavelength, do_effcrs, clambdas, &
                                  nclambdas, fwavnums, cwavnums,       &
                                  lambda_resolution, solar_spec_data,  &
                                  xsec_data_filenames, xsec_data_path, &
                                  gas_nxsecs, gas_xsecs_type,          &
                                  gas_xsecs, o3c1_xsecs, o3c2_xsecs,   &
                                  Rayleigh_xsecs, Rayleigh_depols,     & 
                                  o2_depols, n2_depols,                &
                                  o2_rrs_xsecs, n2_rrs_xsecs,          &
                                  do_rrs_phasefunc, epsilon_air,       &
                                  solar_spec_filename, GasAbsXS,       &
                                  do_debug_geocape_tool, nlambdas,     &
                                  lambdas, wavnums, ngas_rrs,          &
                                  rrs_xsecs,rrs_depols, rrs_gasname,   &
                                  rrs_gas, do_rrs_abs, do_earth_bulk_comp,&
                                  hitran_sol_wvl, hitran_sol_spc,      &
                                  n_hitran_solar, do_hitran 
                                  
  USE GC_read_input_module, ONLY: skip_to_filemark
  USE GC_error_module

  IMPLICIT NONE

  CONTAINS
        
    SUBROUTINE prepare_xsec_new( error )
    
      IMPLICIT NONE

      ! Subroutine arguments
      LOGICAL, INTENT(OUT) :: error
      
      ! Local Variables
      CHARACTER(max_ch_len) :: tmpchar
      
      
      ! =====================================================================
      ! prepare_xsec_new starts here
      ! =====================================================================
      
      ! Init error
      error = .FALSE.
      
      ! ---------------------------------
      ! Load the trace gas cross sections 
      ! ---------------------------------
      
      ! Sets GasAbsXS
      CALL load_gas_abs_xsecs( error, tmpchar )
      
      ! ---------------------------------
      ! Case 1 (air)
      ! ---------------------------------
      IF( do_earth_bulk_comp ) THEN 
        
        ! Call the air calculation
        CALL rayleigh_function_air()
        
      ! ---------------------------------
      ! Case 2 - User defined gas mixture
      ! ---------------------------------
      ELSE
        
        ! Here we call a generalized function 
        CALL rayleigh_function_mix()
        
      ENDIF
      
    END SUBROUTINE prepare_xsec_new
    
    SUBROUTINE compute_xsec_vl( widx, lidx, g, this_xsec  )
      
      ! Computes cross section based on parameterization
      ! for given wavelength and level index for VLIDORT calc.
        
      IMPLICIT NONE
      
      ! --------------------
      ! Subroutine arguments
      ! --------------------
      INTEGER,      INTENT(IN)  :: widx ! Wavelength index in VLIDORT setup
      INTEGER,      INTENT(IN)  :: lidx ! Level index in VLIDORT setup
      INTEGER,      INTENT(IN)  :: g    ! Trace gas index
      REAL(KIND=8), INTENT(OUT) :: this_xsec
      
      ! ---------------
      ! Local variables
      ! ---------------
      REAL(KIND=8) :: temp, tempsq
      
      ! =====================================================================
      ! compute_xsec_vl starts here
      ! =====================================================================
      
      ! Case 1 - Single cross section
      IF( GasAbsXS(g)%XsectTypeID == 1 ) THEN
          this_xsec = GasAbsXS(g)%Xsect(widx,1)
        
      ! Case 2 - 2nd degree pol
      ELSE IF ( GasAbsXS(g)%XsectTypeID == 2 ) THEN
          
        ! Temperature in C
        temp    = mid_temperatures(lidx) - 273.15d0
        tempsq  = temp*temp
        
        ! Compute cross section
        this_xsec = GasAbsXS(g)%Xsect(widx,1)           &
                  + GasAbsXS(g)%Xsect(widx,2)*temp      &
                  + GasAbsXS(g)%Xsect(widx,3)*tempsq
        
        
      ! Case 3 HITRAN LBL
      ELSE IF ( GasAbsXS(g)%XsectTypeID == 3 ) THEN
        this_xsec = GasAbsXS(g)%Xsect(widx,lidx)
        
      ! Case 5 - Linear temperature param.
      ELSE IF ( GasAbsXS(g)%XsectTypeID == 4 ) THEN
        
        ! Temperature in almost C
        temp    = mid_temperatures(lidx) - 273.00d0
        
        ! Compute cross section
        this_xsec = GasAbsXS(g)%Xsect(widx,1)      &
                  + GasAbsXS(g)%Xsect(widx,2)*temp
        
      ! Case 5 - HITRAN LUT
      ELSE IF ( GasAbsXS(g)%XsectTypeID == 5 ) THEN
        this_xsec = GasAbsXS(g)%Xsect(widx,lidx)
        
      ! Case 6 - HITRAN LUT Preconvolved
      ELSE IF ( GasAbsXS(g)%XsectTypeID == 6 ) THEN
        this_xsec = GasAbsXS(g)%Xsect(widx,lidx)
      ELSE
        print*,'Unknown Cross section type index:',&
               GasAbsXS(g)%XsectTypeID
        STOP
      ENDIF
      
      ! Check cross section > 0.0
      IF(this_xsec < 0.0d0 ) this_xsec = 0.0d0
      
      
    END SUBROUTINE compute_xsec_vl
    
    SUBROUTINE compute_dxsecdT_vl( widx, lidx, g, this_dxsecdT  )
    
      ! Computes cross section based on parameterization
      ! for given wavelength and level index for VLIDORT calc.
        
      IMPLICIT NONE
      
      ! --------------------
      ! Subroutine arguments
      ! --------------------
      INTEGER,      INTENT(IN)  :: widx ! Wavelength index in VLIDORT setup
      INTEGER,      INTENT(IN)  :: lidx ! Level index in VLIDORT setup
      INTEGER,      INTENT(IN)  :: g    ! Trace gas index
      REAL(KIND=8), INTENT(OUT) :: this_dxsecdT
      
      ! ---------------
      ! Local variables
      ! ---------------
      REAL(KIND=8) :: temp
      
      ! =====================================================================
      ! compute_dxsecdT_vl starts here
      ! =====================================================================
      
      ! Case 1 - Single cross section
      IF( GasAbsXS(g)%XsectTypeID == 1 ) THEN
          this_dxsecdT = 0.0d0
          
      ! Case 2 - 2nd degree pol
      ELSEIF ( GasAbsXS(g)%XsectTypeID == 2 ) THEN
          
        ! Temperature in C
        temp    = mid_temperatures(lidx) - 273.15d0
        
        ! Compute cross section
        this_dxsecdT =     GasAbsXS(g)%Xsect(widx,2)    &
                     + 2.0*GasAbsXS(g)%Xsect(widx,3)*temp
      
      ! Case 3 HITRAN LBL
      ELSE IF ( GasAbsXS(g)%XsectTypeID == 3 ) THEN
        this_dxsecdT = GasAbsXS(g)%dXsectdT(widx,lidx)
      
      ! Case 5 - Linear temperature param.
      ELSE IF ( GasAbsXS(g)%XsectTypeID == 4 ) THEN
        
        ! Temperature in almost C
        temp    = mid_temperatures(lidx) - 273.00d0
        
        ! Compute cross section
        this_dxsecdT = GasAbsXS(g)%Xsect(widx,2)
        
      ! Case 5 - HITRAN LUT
      ELSE IF ( GasAbsXS(g)%XsectTypeID == 5 ) THEN
        this_dxsecdT = GasAbsXS(g)%dXsectdT(widx,lidx)
        
      ! Case 6 - HITRAN LUT Preconvolved
      ELSE IF ( GasAbsXS(g)%XsectTypeID == 6 ) THEN
        this_dxsecdT = GasAbsXS(g)%dXsectdT(widx,lidx)
      ELSE
        print*,'Unknown Cross section type index:',&
               GasAbsXS(g)%XsectTypeID
        STOP
      ENDIF
      
    END SUBROUTINE compute_dxsecdT_vl
    
    SUBROUTINE load_gas_abs_xsecs( fail, message )

      USE GC_utilities_module,      ONLY: reverse, reverse_idxs, gauss_f2ci0
      USE GC_get_hitran_crs_module, ONLY: get_hitran_crs
      USE GC_variables_module,      ONLY: do_T_Jacobians
      IMPLICIT NONE
      ! 
      LOGICAL,       INTENT(INOUT) :: fail
      CHARACTER*(*), INTENT(INOUT) :: message
      
      ! Local variables
      INTEGER                   :: g, io, i, n, ni0, ngas_check, nxs_max, nxs_g
      INTEGER                   :: nbuff, ndata, errstat, fz, lz, nz1
      REAL(KIND=8)              :: lamb1, lamb2, conversion, wav, val, xsec
      REAL(KIND=8)              :: c1, c2, fwhm, scalefac, scalex
      character(LEN=max_ch_len) :: filename
      LOGICAL                   :: reading, is_wavenum, convolve_slit
      
      ! For the ultra-HR solar spectrum - This needs to be replaced 
      integer, parameter              :: maxio = 600000
      real(kind=8), dimension(maxio)  :: hwave, hi0
      
      ! Arrays for original cross sections
      INTEGER, PARAMETER :: maxspline = 100000
      REAL(KIND=8), DIMENSION(maxspline) :: x, y, y2, yc1, yc2
      
      ! For the spectra on the fine grid
      REAL(KIND=8), ALLOCATABLE, DIMENSION(:,:) :: y_f
      
      ! HITRAN
      REAL(KIND=8), ALLOCATABLE, DIMENSION(:) :: pres_atm
      CHARACTER(LEN=6)                        :: the_molecule
      
      ! For reversing grids if we are using wavenumbers 
      ! (splines need points in ascending order)
      INTEGER,      ALLOCATABLE, DIMENSION(:) :: r_fidx, r_idx
      REAL(KIND=8), ALLOCATABLE, DIMENSION(:) :: flambdas_tmp, lambdas_tmp
      
      ! For HITRAN temperature perturbations
      REAL(KIND=8),   PARAMETER                 :: dT = 5.0 ! Temperature perturbation (K)
      REAL(KIND=8), ALLOCATABLE,   DIMENSION(:) :: mid_T_plus_dT
      ! =====================================================================
      ! load_gas_xsecs starts here
      ! =====================================================================
      
      ! Initialize exception handling
      fail = .FALSE.
      
      ! Intialize output
      DO g=1, ngases
        GasAbsXS(g)%Xsect(:,:) = 0.0d0
      ENDDO
      o3c1_xsecs = 0.0d0      ; o3c2_xsecs = 0.0d0
      
      ! Initialize variables
      lamb1 = -999.9
      lamb2 = -999.9
      ngas_check = 0
      nbuff = 0
      ndata = 0
      
      ! Allocate wavelength arrays and assign values
      ALLOCATE( flambdas_tmp(nflambdas) )
      ALLOCATE(  lambdas_tmp(nlambdas)  )
      flambdas_tmp(1:nflambdas) = flambdas(1:nflambdas)
      lambdas_tmp(1:nlambdas)   = lambdas(1:nlambdas)
      
      ! Reverse grids if we are using wavenumbers
      IF (.NOT. use_wavelength) THEN
         
        ! Allocate reverse index arrays
        ALLOCATE( r_fidx(nflambdas) )
        ALLOCATE( r_idx(nlambdas)   )
        
        ! Compute reverse indices
        CALL reverse_idxs(nflambdas, r_fidx(1:nflambdas))
        CALL reverse_idxs(nlambdas,  r_idx(1:nlambdas)  )
        
        ! Flip wavelength arrays so they are in ascending order
        flambdas_tmp = flambdas_tmp(r_fidx(1:nflambdas))
        lambdas_tmp  = lambdas_tmp(r_idx(1:nlambdas))
        
       ENDIF
      
      ! Hitran needs the pressure in atmospheres
      IF( do_hitran ) THEN
        
        ! If we are using HITRAN we also need to allocate following arrays
          ALLOCATE( pres_atm(GC_nlayers))
          
          ! Pressure in atmospheres
          pres_atm(1:GC_nlayers) = mid_pressures(1:GC_nlayers) / 1013.25d0
          
          ! Temperature layer perturbation
          ALLOCATE( mid_T_plus_dT(GC_nlayers))
          DO n=1,GC_nlayers
            mid_T_plus_dT(n) = mid_temperatures(n) + dT
          ENDDO
          
      ENDIF
      
      ! Allocate the appropriate amount of memory for array
      nxs_max = 0
      DO g=1,ngases
        IF     ( GasAbsXS(g)%XSectTypeID == 1 ) THEN
          nxs_max = MAX(nxs_max,1)
        ELSE IF( GasAbsXS(g)%XSectTypeID == 2 ) THEN
          nxs_max = MAX(nxs_max,3)
        ELSE IF( GasAbsXS(g)%XSectTypeID == 3 ) THEN
          nxs_max = MAX(nxs_max,0)
        ELSE IF( GasAbsXS(g)%XSectTypeID == 4 ) THEN
          nxs_max = MAX(nxs_max,2)
        ELSE IF( GasAbsXS(g)%XSectTypeID == 5 ) THEN
          nxs_max = MAX(nxs_max,0)
        ELSE IF( GasAbsXS(g)%XSectTypeID == 6 ) THEN
          nxs_max = MAX(nxs_max,0)
        ENDIF
      ENDDO
      
      ! Allocate memory
      IF( nxs_max > 0 ) ALLOCATE( y_f(nflambdas,nxs_max) )
      
      ! -----------------------------------------------------------------------------------
      ! Load the absorption cross section for each gas
      ! -----------------------------------------------------------------------------------
      DO g=1,ngases
        
        ! Buffer control initialization
        lamb1 = flambdas_tmp(1)         - 0.05d0
        lamb2 = flambdas_tmp(nflambdas) + 0.05d0
        conversion = 1.0d+20 ! For numerical stability
        nbuff = 0
        ndata = 0
        
        ! Filename
        filename = TRIM(ADJUSTL(xsec_data_path)) &
                // TRIM(ADJUSTL(GasAbsXS(g)%XSectDataFilename))
        
        ! Zero the cross section on the fine lambda grid
        IF (nxs_max .GT. 0 ) y_f(:,:) = 0.0d0
        
        ! -----------------------------------------------------------------------------------
        ! Case 1 - Temperature independent cross section
        ! -----------------------------------------------------------------------------------
        IF     ( GasAbsXS(g)%XSectTypeID == 1 ) THEN
          
          ! Open file (ASCII)
          OPEN(1, FILE=filename, err=90, STATUS='old')
          
          ! Read first line + check window covered
          read(1,*, iostat=io) wav, val
          IF (lamb1 .lt. wav ) THEN
            message = GasAbsXS(g)%Name // ' Xsec data does not cover input window at lower end'
          ENDIF
          
          ! Read spectrum within calculation range
          reading = .TRUE.
          DO WHILE( reading .AND. io .EQ. 0 )
            ndata = ndata + 1
            IF( wav .GT. lamb1 ) THEN
              IF( wav .LT. lamb2 ) THEN
                nbuff = nbuff + 1
                x(nbuff) = wav
                y(nbuff) = val*conversion
              ENDIF
              IF( wav .GE. lamb2 ) reading = .FALSE.
            ENDIF
            READ(1,*,IOSTAT=io) wav, val
          ENDDO
          
          ! Close file
          CLOSE(1)
          
          ! Check if range is covered
          IF( reading ) THEN
            message = GasAbsXS(g)%Name // ' Xsec data does not cover input window at upper end'
          ENDIF
          
          ! Check for out of bounds
          IF (nbuff .gt. maxspline) then
            fail = .TRUE.
            message = 'need to increase maxspline in geocape_xsec_setter_1: '
            GOTO 91
          ENDIF
          
          !  Spline the data to the fine lambda grid
          CALL spline(x,y,nbuff,0.0d0,0.0d0,y2)
          DO n=1, nflambdas
            IF( flambdas_tmp(n) >= x(1) .AND. flambdas_tmp(n) <= x(nbuff) ) THEN
              CALL splint(x,y,y2,nbuff,flambdas_tmp(n),xsec)
              IF( xsec .GE. 0.0 ) y_f(n,1) = xsec / conversion
            ENDIF
          ENDDO
          
          ! Reverse grid if using wavenumbers
          IF( .NOT. use_wavelength ) THEN
            y_f(1:nflambdas, 1:nxs_max) = y_f(r_fidx(1:nflambdas),1:nxs_max)
          ENDIF
          
          ! Counter for later convolutions
          nxs_g = 1
          
          ! Update gas count
          ngas_check = ngas_check + 1
          
        ! -----------------------------------------------------------------------------------
        ! Case 2 - quadratic temperature param.(e.g. O3)
        ! -----------------------------------------------------------------------------------
        ELSE IF( GasAbsXS(g)%XSectTypeID == 2 ) THEN
          
          ! Open file
          OPEN(1, FILE=filename, err=90, STATUS='old')
          
          ! First line is a dummy
          read(1,*)
          
          ! Read second line + check window covered
          READ(1,*, iostat=io) wav, val, c1, c2
          IF (lamb1 .lt. wav ) THEN
            message = GasAbsXS(g)%Name // ' Xsec data does not cover input window at lower end'
          ENDIF
          
          ! Read spectrum within calculation range
          reading = .TRUE.
          DO WHILE( reading .AND. io .EQ. 0 )
            ndata = ndata + 1
            IF( wav .GT. lamb1 ) THEN
              IF( wav .LT. lamb2 ) THEN
                nbuff = nbuff + 1
                x(nbuff) = wav
                y(nbuff) = val
                yc1(nbuff) = c1
                yc2(nbuff) = c2
              ENDIF
              IF( wav .GE. lamb2 ) reading = .FALSE.
            ENDIF
            READ(1,*,IOSTAT=io) wav, val, c1, c2
          ENDDO
          
          ! Close file
          CLOSE(1)
          
          ! Check if range is covered
          IF( reading ) THEN
            message = GasAbsXS(g)%Name // ' Xsec data does not cover input window at upper end'
          ENDIF
          
          ! Check for out of bounds
          IF (nbuff .gt. maxspline) then
            fail = .TRUE.
            message = 'need to increase maxspline in geocape_xsec_setter_1: '
            GOTO 91
          ENDIF
          
          !  Spline C0 to the fine lambda grid
          CALL spline(x,y,nbuff,0.0d0,0.0d0,y2)
          DO n=1, nflambdas
            IF( flambdas_tmp(n) >= x(1) .AND. flambdas_tmp(n) <= x(nbuff) ) THEN
              CALL splint(x,y,y2,nbuff,flambdas_tmp(n),xsec)
              y_f(n,1) = xsec / conversion
            ENDIF
          ENDDO
          
          !  Spline C1 to the fine lambda grid
          CALL spline(x,yc1,nbuff,0.0d0,0.0d0,y2)
          DO n=1, nflambdas
            IF( flambdas_tmp(n) >= x(1) .AND. flambdas_tmp(n) <= x(nbuff) ) THEN
              CALL splint(x,yc1,y2,nbuff,flambdas_tmp(n),xsec)
              y_f(n,2) = xsec / conversion
            ENDIF
          ENDDO
          
          !  Spline C2 to the fine lambda grid
          CALL spline(x,yc2,nbuff,0.0d0,0.0d0,y2)
          DO n=1, nflambdas
            IF( flambdas_tmp(n) >= x(1) .AND. flambdas_tmp(n) <= x(nbuff) ) THEN
              CALL splint(x,yc2,y2,nbuff,flambdas_tmp(n),xsec)
              y_f(n,3) = xsec / conversion
            ENDIF
          ENDDO
          
          ! Reverse grid if using wavenumbers
          IF( .NOT. use_wavelength ) THEN
            y_f(1:nflambdas, 1:nxs_max) = y_f(r_fidx(1:nflambdas),1:nxs_max)
          ENDIF
          
          ! Counter for later convolutions
          nxs_g = 3
          
          ! Update gas count
          ngas_check = ngas_check + 1
          
        ! -----------------------------------------------------------------------------------
        ! Case 3 - HITRAN -Online calculation
        ! -----------------------------------------------------------------------------------
        ELSE IF( GasAbsXS(g)%XSectTypeID == 3) THEN
          
          ! HITRAN Molecule string
          the_molecule = GasAbsXS(g)%Name // '  '
          
          ! HITRAN xsect computed on wavelength grid. Routine converts 
          ! FWHM to wavelength units if use_wavelength == false
          is_wavenum = .FALSE.
          
          !xliu, 04/06/2015, assuming fwhm=0.0 will force the line-by-line calculation at input wavelength grid
          !which is typically much coarser than the required spectral sampling (~0.0003 nm for visible H2O/O2)
          !so assuming a very small fwhm so that line-by-line calculation will be convolved from very high 
          !resolution to input wavelength grid with solar-I0 effect
          IF (do_effcrs) THEN
            fwhm = lambda_resolution ! in nm for wavelength grid, and cm^-1 for wavenumber grid
          ELSE
            fwhm = 4.0 * MINVAL(ABS(lambdas_tmp(1:nlambdas-1) - lambdas_tmp(2:nlambdas))) ! fwhm=0.d0
          ENDIF

          ! do line by line calculation only at layers where there is given molecule 
          fz = MINVAL(MINLOC(pres_atm(1:GC_nlayers), MASK = (gas_partialcolumns(1:GC_nlayers, g) > 0.d0)))
          lz = MINVAL(MAXLOC(pres_atm(1:GC_nlayers), MASK = (gas_partialcolumns(1:GC_nlayers, g) > 0.d0)))
          nz1 = lz - fz + 1
          
          ! For solar I0 effect correction
          scalefac = SUM(gas_partialcolumns(fz:lz, g)) / 2.0
          
          ! Compute HITRAN cross section 
          CALL get_hitran_crs(the_molecule, nlambdas, lambdas_tmp(1:nlambdas),&
                              n_hitran_solar,hitran_sol_wvl,hitran_sol_spc,   &
                              is_wavenum,                                     &
                              use_wavelength, nz1, pres_atm(fz:lz),           &
                              mid_temperatures(fz:lz), fwhm, scalefac,        &
                              GasAbsXS(g)%XSect(1:nlambdas,fz:lz), errstat    )
          
          IF( do_T_Jacobians ) THEN
            
            ! Compute X-sect with T perturbation
            CALL get_hitran_crs(the_molecule, nlambdas, lambdas_tmp(1:nlambdas),   &
                              n_hitran_solar,hitran_sol_wvl,hitran_sol_spc,        &
                              is_wavenum,                                          &
                              use_wavelength, nz1, pres_atm(fz:lz),                &
                              mid_T_plus_dT(fz:lz), fwhm, scalefac,                &
                              GasAbsXS(g)%dXSectdT(1:nlambdas,fz:lz), errstat      )
            
            ! Complete derivative
            GasAbsXS(g)%dXSectdT(1:nlambdas,fz:lz) = &
              (GasAbsXS(g)%dXSectdT(1:nlambdas,fz:lz)-GasAbsXS(g)%XSect(1:nlambdas,fz:lz))/dT
            
          ENDIF
          
          ! Reverse grid if using wavenumbers to restore ascending order
          IF( .NOT. use_wavelength ) THEN
            GasAbsXS(g)%XSect(1:nlambdas,1:GC_nlayers) = &
            GasAbsXS(g)%XSect(r_idx(1:nlambdas),1:GC_nlayers)
            IF( do_T_Jacobians ) THEN
              GasAbsXS(g)%dXSectdT(1:nlambdas,1:GC_nlayers) = &
              GasAbsXS(g)%dXSectdT(r_idx(1:nlambdas),1:GC_nlayers)
            ENDIF
          ENDIF
          
          ! Counter for later convolutions
          nxs_g = 0
          
          ! Error check
          IF (errstat == 1) THEN
            message='Error in getting HITRAN cross sections for molecule ' // TRIM(the_molecule)
            print*,message
            fail = .true.
            GOTO 91
          ELSE
            ! Check off this gas
            ngas_check = ngas_check + 1
          ENDIF
          
        ! -----------------------------------------------------------------------------------
        ! Case 4 - Linear temperature parameterization (e.g NO2)
        ! -----------------------------------------------------------------------------------
        ELSE IF( GasAbsXS(g)%XSectTypeID == 4 ) THEN
          
          ! Open file
          OPEN(1, FILE=filename, err=90, STATUS='old')
          
          ! Read first line + check window covered
          read(1,*, iostat=io) wav, val, c1
          IF (lamb1 .lt. wav ) THEN
            message = GasAbsXS(g)%Name // ' Xsec data does not cover input window at lower end'
          ENDIF
          
          ! Read spectrum within calculation range
          reading = .TRUE.
          DO WHILE( reading .AND. io .EQ. 0 )
            ndata = ndata + 1
            IF( wav .GT. lamb1 ) THEN
              IF( wav .LT. lamb2 ) THEN
                nbuff = nbuff + 1
                x(nbuff) = wav
                y(nbuff) = val*conversion
                yc1(nbuff) = c1*conversion
              ENDIF
              IF( wav .GE. lamb2 ) reading = .FALSE.
            ENDIF
            READ(1,*,IOSTAT=io) wav, val, c1
          ENDDO
          
          ! Close file
          CLOSE(1)
          
          ! Check if range is covered
          IF( reading ) THEN
            message = GasAbsXS(g)%Name // ' Xsec data does not cover input window at upper end'
          ENDIF
          
          ! Check for out of bounds
          IF (nbuff .gt. maxspline) then
            fail = .TRUE.
            message = 'need to increase maxspline in geocape_xsec_setter_1: '
            GOTO 91
          ENDIF
          
          !  Spline C0 to the fine lambda grid
          CALL spline(x,y,nbuff,0.0d0,0.0d0,y2)
          DO n=1, nflambdas
            IF( flambdas_tmp(n) >= x(1) .AND. flambdas_tmp(n) <= x(nbuff) ) THEN
              CALL splint(x,y,y2,nbuff,flambdas_tmp(n),xsec)
              y_f(n,1) = xsec / conversion
            ENDIF
          ENDDO
          
          !  Spline C1 to the fine lambda grid
          CALL spline(x,yc1,nbuff,0.0d0,0.0d0,y2)
          DO n=1, nflambdas
            IF( flambdas_tmp(n) >= x(1) .AND. flambdas_tmp(n) <= x(nbuff) ) THEN
              CALL splint(x,yc1,y2,nbuff,flambdas_tmp(n),xsec)
              y_f(n,2) = xsec / conversion
            ENDIF
          ENDDO
          
          ! Reverse grid if using wavenumbers
          IF( .NOT. use_wavelength ) THEN
            y_f(1:nflambdas, 1:nxs_max) = y_f(r_fidx(1:nflambdas),1:nxs_max)
          ENDIF
          
          ! Counter for later convolutions
          nxs_g = 2
          
          ! Update gas count
          ngas_check = ngas_check + 1
          
        ! -----------------------------------------------------------------------------------
        ! Case 5 - HITRAN LUT + Case 6 (Preconvolved)
        ! -----------------------------------------------------------------------------------
        ELSE IF( GasAbsXS(g)%XSectTypeID == 5 .OR. GasAbsXS(g)%XSectTypeID == 6 ) THEN
          
          ! Determine the spectral resolution
          IF (do_effcrs) THEN
            fwhm = lambda_resolution ! in nm for wavelength grid, and cm^-1 for wavenumber grid
          ELSE
            fwhm = 0.0d0
          ENDIF
          
          ! For solar I0 effect correction
          scalefac = SUM(gas_partialcolumns(1:GC_nlayers, g)) / 2.0
          
          ! Option 6 - LUT has been preconvolved
          convolve_slit = .TRUE.
          IF( GasAbsXS(g)%XSectTypeID .eq. 6 .or. fwhm .eq. 0.0 ) convolve_slit = .FALSE.
          
          ! Get cross section from LUT
          CALL get_hitran_lut_crs( filename,                                            &
                                   max_ch_len, lambdas_tmp(1:nlambdas), nlambdas,       &
                                   hitran_sol_wvl,hitran_sol_spc,n_hitran_solar,        &
                                   mid_pressures(1:GC_nlayers),                         &
                                   mid_temperatures(1:GC_nlayers), GC_nlayers,          &
                                   scalefac, fwhm, convolve_slit,use_wavelength,        &
                                   GasAbsXS(g)%XSect(1:nlambdas,1:GC_nlayers)           )
          
          IF( do_T_Jacobians ) THEN
            
            ! Cross section with temperature perturbation
            CALL get_hitran_lut_crs( filename,                                            &
                                     max_ch_len, lambdas_tmp(1:nlambdas), nlambdas,       &
                                     hitran_sol_wvl,hitran_sol_spc,n_hitran_solar,        &
                                     mid_pressures(1:GC_nlayers),                         &
                                     mid_T_plus_dT(1:GC_nlayers), GC_nlayers,             &
                                     scalefac, fwhm, convolve_slit,use_wavelength,        &
                                     GasAbsXS(g)%dXSectdT(1:nlambdas,1:GC_nlayers)        )
            
            ! Complete derivative
            GasAbsXS(g)%dXSectdT(1:nlambdas,1:GC_nlayers) = &
              (GasAbsXS(g)%dXSectdT(1:nlambdas,1:GC_nlayers)-GasAbsXS(g)%XSect(1:nlambdas,1:GC_nlayers))/dT
            
          ENDIF
          
          ! Reverse grid if using wavenumbers to restore ascending order
          IF( .NOT. use_wavelength ) THEN
            GasAbsXS(g)%XSect(1:nlambdas,1:GC_nlayers) = &
               GasAbsXS(g)%XSect(r_idx(1:nlambdas),1:GC_nlayers)
            IF( do_T_Jacobians ) THEN
              GasAbsXS(g)%dXSectdT(1:nlambdas,1:GC_nlayers) = &
                GasAbsXS(g)%dXSectdT(r_idx(1:nlambdas),1:GC_nlayers)
            ENDIF
          ENDIF
          
          ! Counter for later convolutions (they are done in the HITRAN routine)
          nxs_g = 0
          
          ! Update gas count
          ngas_check = ngas_check + 1
          
        ENDIF
        
        ! Set cross section for non-HITRAN gases
        IF( GasAbsXS(g)%XSectTypeID .ne. 3 .AND. &
            GasAbsXS(g)%XSectTypeID .ne. 5 .AND. &
            GasAbsXS(g)%XSectTypeID .ne. 6       ) THEN
            
           ! Convolve spectra if necessary
           IF( do_effcrs .AND. nxs_g > 0 ) THEN
              
              ! Column for I0 correction
              scalex = gas_totalcolumns(g) * 2.68668D16 * 2.0D0
              
              ! Perform convolution
              IF( use_wavelength ) THEN
                 
                 CALL gauss_f2ci0( flambdas(1:nflambdas), y_f(1:nflambdas,1:nxs_g), &
                      solar_spec_data(1:nflambdas), nflambdas, nxs_g,  &
                      scalex, lambda_resolution, lambdas(1:nlambdas),  &
                      GasAbsXS(g)%XSect(1:nlambdas,1:nxs_g), nlambdas  )
                 
              ELSE
            
                 CALL gauss_f2ci0( fwavnums(1:nflambdas), y_f(1:nflambdas,1:nxs_g), &
                      solar_spec_data(1:nflambdas), nflambdas, nxs_g,  &
                      scalex, lambda_resolution, wavnums(1:nlambdas),  &
                      GasAbsXS(g)%XSect(1:nlambdas,1:nxs_g), nlambdas  )
                 
              ENDIF
              
           ELSE
        
              ! Just need to transfer spectra to array
              GasAbsXS(g)%XSect(1:nlambdas,1:nxs_g) = y_f(1:nlambdas,1:nxs_g)
              
           ENDIF
        END IF
        
!         ! Archive O3 for temperature jacobian (CCM This should be depricated)
!         IF( GasAbsXS(g)%Name .EQ. 'O3  ' ) THEN
!           
!           o3c1_xsecs(1:nlambdas) = GasAbsXS(g)%XSect(1:nlambdas,2)
!           o3c2_xsecs(1:nlambdas) = GasAbsXS(g)%XSect(1:nlambdas,3)
!           
!         ENDIF
        
      ENDDO
      
      !  Check that all gases have been accounted for
      IF(do_debug_geocape_tool) print *, 'ngas_check = ', ngas_check, ' ngases = ', ngases
      IF ( ngas_check .ne. ngases ) THEN
          fail    = .true.
          message = ' Not all gases accounted for; Reduce choices!'
          GOTO 91
      ENDIF
      
      ! Cleanup
      IF( ALLOCATED(y_f     )     ) DEALLOCATE(y_f         )
      IF( ALLOCATED(pres_atm)     ) DEALLOCATE(pres_atm    )
      IF( ALLOCATED(r_fidx)       ) DEALLOCATE(r_fidx      )
      IF( ALLOCATED(r_idx)        ) DEALLOCATE(r_idx       )
      IF( ALLOCATED(flambdas_tmp) ) DEALLOCATE(flambdas_tmp)
      IF( ALLOCATED(lambdas_tmp)  ) DEALLOCATE(lambdas_tmp )
      
      ! Normal Return
      RETURN
      
      ! Error Returns
90    CONTINUE
      fail = .TRUE.
      message = 'Open failure for Xsec file = '//filename(1:LEN(filename))
      RETURN
   
91    CONTINUE
      RETURN
      
    END SUBROUTINE load_gas_abs_xsecs
    
    SUBROUTINE rayleigh_function_air
      
      ! Computes rayleigh cross sections and depolarization
      ! ratios for air following Bodhaine et al. (1999)
      ! Wavelengths in nm

      USE GC_utilities_module, ONLY: gauss_f2ci0
      USE GC_variables_module, ONLY: n_bulk_gas, bulk_gas_mixr, &
           bulk_gas_ri_name
      
      IMPLICIT NONE
      
      ! STP air density [molec/cm3]
      REAL(KIND=8),      PARAMETER               :: airdens_std = 2.546899D19
      
      ! ---------------
      ! Local Variables
      ! ---------------
      INTEGER      :: i,j,l,g, co2_idx
      REAL(KIND=8) :: F_air, wvl_microns, n_300_m1, wvl_m2, Ftmp, sclf
      REAL(KIND=8) :: n_air, n_air2, wvl_cm2, pi, rayleigh_prefactor, term
      REAL(KIND=8) :: n2_gamma, o2_gamma, sigsq, sig4
      
      ! Additional variables for Raman
      REAL(KIND=8), DIMENSION( 48)  :: N2POS   ! Position 
      REAL(KIND=8), DIMENSION( 48)  :: N2_LSTR ! Line strength
      REAL(KIND=8), DIMENSION(185)  :: O2POS
      REAL(KIND=8), DIMENSION(185)  :: O2_LSTR
      
      ! Initially compute rayleigh on fine grid
      REAL(KIND=8), ALLOCATABLE, DIMENSION(:)   :: Rayleigh_fine,RayDepols_fine,RayEps_fine
      REAL(KIND=8), ALLOCATABLE, DIMENSION(:,:) :: RRS_xs_fine
      REAL(KIND=8), ALLOCATABLE, DIMENSION(:,:) :: RRS_depols_fine 
      
      ! ==========================================================================
      ! rayleigh_function_air starts here
      ! ==========================================================================
      
      
      ! Pi
      pi = DATAN(1.0D0)*4.0D0
      
      ! Constant term in front of rayleigh cross section
      rayleigh_prefactor = 24.0D0 * PI**3 / (airdens_std**2)
      
      ! Allocate and initialize RRS cross sections
      ngas_rrs = 2
      ALLOCATE( rrs_xsecs(nlambdas,GC_nlayers,ngas_rrs)  ) ! Assumed relative to air_column (NOT partial col)
      ALLOCATE( rrs_depols(nlambdas,ngas_rrs)            )
      ALLOCATE( rrs_gasname(ngas_rrs)                    )
      ALLOCATE( rrs_gas(ngas_rrs)                        )
      
      rrs_gasname(1) = 'N2'
      rrs_gasname(2) = 'O2'
      
      ! Allocate values for the fine grid
      ALLOCATE( Rayleigh_fine(nflambdas) )
      ALLOCATE( RayDepols_fine(nflambdas))
      ALLOCATE( RayEps_fine(nflambdas))
      ALLOCATE( RRS_xs_fine(nflambdas,ngas_rrs)    )
      ALLOCATE (RRS_depols_fine(nflambdas,ngas_rrs))
      
      
      ! Find CO2 index
      co2_idx = 0
      DO i=1,n_bulk_gas
        IF( TRIM(ADJUSTL(bulk_gas_ri_name(i))) .EQ. 'CO2' ) co2_idx = i 
      ENDDO
      
      !o2_rrs_xsecs(:) = 0.0 ; n2_rrs_xsecs = 0.0d0
      
      ! Loop over wavelengths
      DO i=1,nflambdas
        
        ! Wavelength in microns
        wvl_microns = 1.0d-3 * flambdas(i)
        
        ! 1/wavelength^2 (microns^-2)
        wvl_m2 = 1.0d0/(wvl_microns**2)
        
        ! Wavelength squared in cm^2
        wvl_cm2 = (flambdas(i)*1.0d-7)**2
        
        ! Compute king factor at wavelength
        CALL compute_king_factor( flambdas(i), n_bulk_gas,         &
                                  bulk_gas_ri_name, bulk_gas_mixr, &
                                  F_air, RayDepols_fine(i)          )
        
        ! Anisotropy of polarizability
        RayEps_fine(i) = 4.5d0*(F_air-1.0d0)
        
        ! We also may need to save the individual O2/N2 depolarization ratios
        ! to compute Raman phase functions
        CALL compute_king_factor( flambdas(i), 1, (/'N2        '/), (/1.0d0/), &
                                  Ftmp, RRS_depols_fine(i,1)                   )
        CALL compute_king_factor( flambdas(i), 1, (/'O2        '/), (/1.0d0/), &
                                  Ftmp, RRS_depols_fine(i,2)                   )
        
        ! Refractive index-1 of dry air w/ 300ppm CO2 
        ! Peck and Reeder (1972)
        n_300_m1 = 1.0d-8*( 8060.51D0 + 2.48099D+06/( 132.274D0-wvl_m2) &
                                      + 1.74557D+04/(39.32957d0-wvl_m2) )
        
        ! Refractive index of air at true CO2 concentration
        n_air = 1.0d0 + n_300_m1*( 1.0d0 + 0.54d0*(bulk_gas_mixr(co2_idx)-3.0d-4) )
        
        ! Square refractive index
        n_air2 = n_air**2
        
        ! Compute rayleigh cross section
        term = (n_air2-1.0d0)/(n_air2+2.0d0)/wvl_cm2
        
        ! Compute rayleigh cross section
        Rayleigh_fine(i) = rayleigh_prefactor * term**2 * F_air
        
        
      ENDDO ! nflambdas
      
      ! Do convolution if necessary
      IF( do_effcrs ) THEN
        
        ! Scale factor for I0 correction
        sclf = SUM(aircolumns(1:GC_nlayers)) * 2.0
        
        ! Rayleigh cross section
        CALL gauss_f2ci0( flambdas(1:nflambdas), Rayleigh_fine(1:nflambdas), &
                          solar_spec_data(1:nflambdas), nflambdas,     1,    &
                          sclf, lambda_resolution,   lambdas(1:nlambdas),    &
                          Rayleigh_xsecs(1:nlambdas), nlambdas               )
        
        ! Scale factor for I0 correction
        sclf = 1.0d0
        
        ! Rayleigh depolarization
        CALL gauss_f2ci0( flambdas(1:nflambdas), RayDepols_fine(1:nflambdas), &
                          solar_spec_data(1:nflambdas), nflambdas,      1,    &
                          sclf, lambda_resolution,   lambdas(1:nlambdas),     &
                          Rayleigh_depols(1:nlambdas), nlambdas               )
        
        IF( do_rrs_phasefunc ) THEN
        
          ! Air anisotropy of polarizability
          CALL gauss_f2ci0( flambdas(1:nflambdas), RayEps_fine(1:nflambdas), &
                            solar_spec_data(1:nflambdas), nflambdas,      1, &
                            sclf, lambda_resolution,   lambdas(1:nlambdas),  &
                            epsilon_air(1:nlambdas), nlambdas                )
          
          ! Depolarization ratios for RRS gases
          CALL gauss_f2ci0( flambdas(1:nflambdas),                             &
                            RRS_depols_fine(1:nflambdas,1:ngas_rrs),           &
                            solar_spec_data(1:nflambdas), nflambdas, ngas_rrs, &
                            sclf, lambda_resolution,   lambdas(1:nlambdas),    &
                            rrs_depols(1:nlambdas,1:ngas_rrs), nlambdas        )
        
        ENDIF
        
      ELSE
        
        ! We just need to copy the values
        Rayleigh_xsecs(1:nlambdas)  = Rayleigh_fine(1:nlambdas)
        Rayleigh_depols(1:nlambdas) = RayDepols_fine(1:nlambdas)
        epsilon_air(1:nlambdas)     = RayEps_fine(1:nlambdas)
        rrs_depols(1:nlambdas,1:ngas_rrs) = RRS_depols_fine(1:nlambdas,1:ngas_rrs)
        
      ENDIF 
      
      ! Also compute the RRS "absorption" cross sections for the parameterization
      ! Get Raman cross sections for each layer temperature
      DO l=1,GC_nlayers
        
        ! Initialize Raman cross sections
        RRS_xs_fine(:,:) = 0.0d0
        
        ! Compute the line strengths For given layer
        CALL RAMAN_N2( mid_temperatures(l), N2POS, N2_LSTR(:))
        CALL RAMAN_O2( mid_temperatures(l), O2POS, O2_LSTR(:))
        
        ! Wavelength loop
        DO i=1,nflambdas
          
          ! Compute polarization anistropy for o2 and n2
          SIGSQ = 1.0d6 / (flambdas(i)**2)
          N2_GAMMA = ( -6.01466 + 2385.57 / (186.099 - sigsq) )*1.0d-25
          O2_GAMMA = (  0.07149 + 45.9364 / (48.2716 - sigsq) )*1.0d-24
          
        ! N2 Integrated cross section
          DO J=1,48
            
            SIG4 = 1.0d7 / flambdas(i) - N2POS(j)
            RRS_xs_fine(i,1) = RRS_xs_fine(i,1) &
                             + (SIG4**4)*(N2_GAMMA**2)*N2_LSTR(j)
          ENDDO
          
          ! O2 Integrated cross section
          DO j=1,185
            
            SIG4 = 1.0d7 / flambdas(i) - O2POS(j)
            RRS_xs_fine(i,2) = RRS_xs_fine(i,2) &
                           + (SIG4**4)*(O2_GAMMA**2)*O2_LSTR(j)
          ENDDO
        
        ENDDO
        
        ! Perform convolution if needed
        IF( do_effcrs .AND. (do_rrs_phasefunc .OR. do_rrs_abs)  ) THEN
          
          ! Scale factor for I0 correction
          sclf = SUM(aircolumns(1:GC_nlayers)) * 2.0
          
          DO g=1,ngas_rrs
            
            
            ! Scale factor for I0 correction
            sclf = SUM(aircolumns(1:GC_nlayers)) * 2.0
            CALL gauss_f2ci0( flambdas(1:nflambdas),                               &
                              RRS_xs_fine(1:nflambdas,g),                          &
                              solar_spec_data(1:nflambdas), nflambdas, 1,          &
                              sclf, lambda_resolution,   lambdas(1:nlambdas),      &
                              rrs_xsecs(1:nlambdas,l,g), nlambdas                  )
          ENDDO
        ELSE
          DO g=1,ngas_rrs
            rrs_xsecs(1:nlambdas,l,g) = RRS_xs_fine(1:nlambdas,g)
          ENDDO
        ENDIF 
        
      ENDDO
      
      ! Deallocate temporary arrays
      IF( ALLOCATED(Rayleigh_fine)   ) DEALLOCATE(Rayleigh_fine)
      IF( ALLOCATED(RayDepols_fine)  ) DEALLOCATE(RayDepols_fine)
      IF( ALLOCATED(RayEps_fine)     ) DEALLOCATE(RayEps_fine)
      IF( ALLOCATED(RRS_xs_fine)     ) DEALLOCATE(RRS_xs_fine)
      IF( ALLOCATED(RRS_depols_fine) ) DEALLOCATE(RRS_depols_fine)
      
      DO g=1,nlambdas
        WRITE(981,'(3(E15.7))') lambdas(g),Rayleigh_xsecs(g),Rayleigh_depols(g)
      ENDDO
      
    END SUBROUTINE rayleigh_function_air
    
    SUBROUTINE rayleigh_function_mix()
      
      ! Computes rayleigh function for a supplied mixture of gasses
      
      ! TO DO
      ! Maybe add correction for lorentz field
      
      USE GC_utilities_module, ONLY: gauss_f2ci0
      USE GC_variables_module, ONLY: n_bulk_gas, bulk_gas_mixr, &
           bulk_gas_ri_name
      
      IMPLICIT NONE
      
      ! ---------------
      ! Local Variables
      ! ---------------
      INTEGER      :: i,g
      REAL(KIND=8) :: Ftmp, sclf
      REAL(KIND=8) :: pi, rayleigh_prefactor, xs_g
      
      REAL(KIND=8) :: n_g, ad_g, F_g, depol_g, lmb4, mr_tot, n_g2
      
      ! Initially compute rayleigh on fine grid
      REAL(KIND=8), ALLOCATABLE, DIMENSION(:)   :: Rayleigh_fine,RayDepols_fine,RayEps_fine
      REAL(KIND=8), ALLOCATABLE, DIMENSION(:,:) :: RRS_xs_fine
      REAL(KIND=8), ALLOCATABLE, DIMENSION(:,:) :: RRS_depols_fine 
      
      ! ==========================================================================
      ! rayleigh_function_air starts here
      ! ==========================================================================
      
      ! Pi
      pi = DATAN(1.0D0)*4.0D0
      
      ! Constant term in front of rayleigh cross section
      rayleigh_prefactor = 24.0D0 * PI**3 !/ (airdens_std**2)
      
      ! Allocate values for the fine grid
      ALLOCATE( Rayleigh_fine(nflambdas) )
      ALLOCATE( RayDepols_fine(nflambdas))
      ALLOCATE( RayEps_fine(nflambdas))
!       ALLOCATE( RRS_xs_fine(nflambdas,ngas_rrs)    )
!       ALLOCATE (RRS_depols_fine(nflambdas,ngas_rrs))
      
      ! Zero arrays
      Rayleigh_fine(:)  = 0.0d0
      RayDepols_fine(:) = 0.0d0
      RayEps_fine(:)    = 0.0d0
      ngas_rrs = 0
      
      ! Compute sum of mixing ratio
      mr_tot = SUM( bulk_gas_mixr )
      
      
      
      ! Loop over gases
      DO i=1,nflambdas
        
        ! 1/lambda^4 (cm^-4)
        lmb4 = 1.0d28 / (flambdas(i)**4)
        
        ! Zero the temp. concentration weighted king factor
        Ftmp = 0.0d0
        
        DO g=1,n_bulk_gas
          
          ! Get refractive index for gas
          CALL compute_gas_refractive_index( flambdas(i), bulk_gas_ri_name(g), &
                                             n_g, ad_g                         )
          
          ! Get King factor
          CALL compute_king_factor( flambdas(i), 1, (/bulk_gas_ri_name(g)/), &
                                    (/1.0d0/), F_g, depol_g                  )
          
          ! Square the refractive index
          n_g2 = n_g**2
          
          ! Compute Cross section for given gas
          xs_g = rayleigh_prefactor * F_g * lmb4 / (ad_g**2)* &
                 ( (n_g2-1)/(n_g2+2.0d0) )**2
          
          ! Update Rayleigh
          Rayleigh_fine(i) = Rayleigh_fine(i) + bulk_gas_mixr(g)*xs_g
          
          ! Weight depolarization by mixing ratio for now (approximate) for phase func calc
          RayDepols_fine(i) = RayDepols_fine(i) + bulk_gas_mixr(g)*depol_g
          Ftmp = Ftmp + bulk_gas_mixr(g)*F_g
          
        ENDDO
        
        ! Normalize (in case mixing ratios don't add to 1)
        Rayleigh_fine(i)  = Rayleigh_fine(i)/mr_tot
        RayDepols_fine(i) = Raydepols_fine(i)/mr_tot
        Ftmp = Ftmp/mr_tot
        
        ! Anisotropy of polarizability
        RayEps_fine(i) = 4.5d0*(Ftmp-1.0d0)
        
      ENDDO
      
      ! Do convolution if necessary
      IF( do_effcrs ) THEN
        
        ! Scale factor for I0 correction
        sclf = SUM(aircolumns(1:GC_nlayers)) * 2.0
        
        ! Rayleigh cross section
        CALL gauss_f2ci0( flambdas(1:nflambdas), Rayleigh_fine(1:nflambdas), &
                          solar_spec_data(1:nflambdas), nflambdas,     1,    &
                          sclf, lambda_resolution,   lambdas(1:nlambdas),    &
                          Rayleigh_xsecs(1:nlambdas), nlambdas               )
        
        ! Scale factor for I0 correction
        sclf = 1.0d0
        
        ! Rayleigh depolarization
        CALL gauss_f2ci0( flambdas(1:nflambdas), RayDepols_fine(1:nflambdas), &
                          solar_spec_data(1:nflambdas), nflambdas,      1,    &
                          sclf, lambda_resolution,   lambdas(1:nlambdas),     &
                          Rayleigh_depols(1:nlambdas), nlambdas               )
        
        IF( do_rrs_phasefunc ) THEN
        
          ! Air anisotropy of polarizability
          CALL gauss_f2ci0( flambdas(1:nflambdas), RayEps_fine(1:nflambdas), &
                            solar_spec_data(1:nflambdas), nflambdas,      1, &
                            sclf, lambda_resolution,   lambdas(1:nlambdas),  &
                            epsilon_air(1:nlambdas), nlambdas                )
          
        ENDIF
        
      ELSE
        
        ! We just need to copy the values
        Rayleigh_xsecs(1:nlambdas)  = Rayleigh_fine(1:nlambdas)
        Rayleigh_depols(1:nlambdas) = RayDepols_fine(1:nlambdas)
        epsilon_air(1:nlambdas)     = RayEps_fine(1:nlambdas)
        
      ENDIF 
      
      
      ! Deallocate temporary arrays
      IF( ALLOCATED(Rayleigh_fine)   ) DEALLOCATE(Rayleigh_fine)
      IF( ALLOCATED(RayDepols_fine)  ) DEALLOCATE(RayDepols_fine)
      IF( ALLOCATED(RayEps_fine)     ) DEALLOCATE(RayEps_fine)
      IF( ALLOCATED(RRS_xs_fine)     ) DEALLOCATE(RRS_xs_fine)
      IF( ALLOCATED(RRS_depols_fine) ) DEALLOCATE(RRS_depols_fine)
      
    END SUBROUTINE rayleigh_function_mix
    
    SUBROUTINE compute_king_factor( wvl, nscatgas, scatgas_list, scatgas_mixratio, king_factor, depol_ratio )
      
      ! Computes the king factor for a mixture of gases
      
      IMPLICIT NONE
      
      ! --------------------
      ! Subroutine arguments
      ! --------------------
      REAL(KIND=8),                           INTENT(IN)  :: wvl ! nm
      INTEGER,                                INTENT(IN)  :: nscatgas
      CHARACTER(LEN=4), DIMENSION(nscatgas),  INTENT(IN)  :: scatgas_list
      REAL(KIND=8), DIMENSION(nscatgas),      INTENT(IN)  :: scatgas_mixratio
      REAL(KIND=8),                           INTENT(OUT) :: king_factor, depol_ratio
      
      ! ---------------
      ! Local variables
      ! ---------------
      INTEGER                         :: n
      REAL(KIND=8)                    :: F_gas, mr_tot, wvl_m2, depol_p_tmp
      
      ! ==========================================================================
      ! compute_king_factor starts here
      ! ==========================================================================
      
      ! Compute wvl^-2 in (microns^-2)
      wvl_m2 = 1.0d6/wvl/wvl
      
      ! Initialize output king factor and mixing ratio total
      king_factor = 0.0d0 ; mr_tot = 0.0d0
      
      DO n=1,nscatgas
        
        ! Get king factor for each gas
        ! ----------------------------
        
        ! Nitrogen
        IF     ( TRIM(ADJUSTL(scatgas_list(n))) == 'N2'  ) THEN
          
          ! Bates et al. (1984)
          F_gas = 1.034d0 + 3.17d-4*wvl_m2
          
        ! Oxygen
        ELSE IF( TRIM(ADJUSTL(scatgas_list(n))) == 'O2'  ) THEN
          
          ! Bates et al. (1984)
          F_gas = 1.096d0 + 1.385d-3*wvl_m2 + 1.448d-4*(wvl_m2**2)
          
        ELSE IF( TRIM(ADJUSTL(scatgas_list(n))) == 'CO2'  ) THEN
          
          ! Bates et al. (1984)
          F_gas = 1.15d0
          
        ELSE IF( TRIM(ADJUSTL(scatgas_list(n))) == 'Ar'  ) THEN
          
          ! Bates et al. (1984)
          F_gas = 1.0d0
        
        ELSE IF( TRIM(ADJUSTL(scatgas_list(n))) == 'H2O'  ) THEN
          
          ! Marshall and Smith (1990) (depolarization ratio = 0.17)
          F_gas = 1.3534304d0
        
        ELSE IF( TRIM(ADJUSTL(scatgas_list(n))) == 'CH4'  ) THEN
          
          ! Sneep and Ubachs(2005) Value at 532nm
          F_gas = 1.0d0
          
        ELSE IF( TRIM(ADJUSTL(scatgas_list(n))) == 'N2O'  ) THEN
          
          ! Depolarization for polarized light (Sneep and Ubachs 2005, Ref [16])
          depol_p_tmp = 0.0577d0 + 11.8d-4*wvl_m2
          
          ! Compute king factor from depolarization of polarized light
          F_gas = (3.0d0+6.0d0*depol_p_tmp)/&
                  (3.0d0-4.0d0*depol_p_tmp)
          
!           ! Sneep and Ubachs(2005) Value at 532nm
!           F_gas = 1.225d0
          
        ELSE IF( TRIM(ADJUSTL(scatgas_list(n))) == 'SF6'  ) THEN
          
          ! Sneep and Ubachs(2005) Value at 532nm
          F_gas = 1.0d0
          
        ELSE IF( TRIM(ADJUSTL(scatgas_list(n))) == 'CO'  ) THEN
          
          ! Sneep and Ubachs(2005) Ref. [21] Value at 632.8nm 
          ! Dispersion can be neglected
          F_gas = 1.016d0
          
        ELSE 
          
          print*,'Could not find King factor for ' // TRIM(ADJUSTL(scatgas_list(n)))
          STOP 
          
        ENDIF
        
        ! Update king factor
        king_factor = king_factor + F_gas * scatgas_mixratio(n)
        
        ! Update mixing ratio total
        mr_tot = mr_tot + scatgas_mixratio(n)
        
      ENDDO
      
      ! Normalize king factor
      king_factor = king_factor / mr_tot
      
      ! Compute depolarization ratio from king factor
      depol_ratio = 6.0D0*(king_factor -1.0D0)/&
                          (3.0D0+7.0D0*king_factor )
      
    END SUBROUTINE compute_king_factor
    
    SUBROUTINE compute_gas_refractive_index( wvl, scatgas, n_gas, std_ad )
      
      
      ! Computes the refractive index for a mixture of gases
      
      ! Dispersion relations are from refractiveindex.info
      ! Closely examine values when I have more time
      
      ! Current References 
      ! N2 
      !      E. R. Peck and B. N. Khanna. Dispersion of nitrogen, J. Opt. Soc. Am. 56, 1059-1063 (1966)
      ! O2
      !      J. Zhang, Z. H. Lu, and L. J. Wang. Precision refractive index measurements of air, N2, 
      !                      O2, Ar, and CO2 with a frequency comb, Appl. Opt. 47, 3143-3151 (2008)
      ! CO2 
      !      J. G. Old, K. L. Gentili, and E. R. Peck. Dispersion of Carbon Dioxide, J. Opt. Soc. Am. 61, 89-90 (1971)
      ! Ar
      !      A. Bideau-Mehu, Y. Guern, R. Abjean, A. Johannin-Gilles. Measurement of refractive indices of neon, argon, 
      !              krypton and xenon in the 253.7-140.4 nm wavelength range. Dispersion relations and estimated 
      !              oscillator strengths of the resonance lines. J. Quant. Spectrosc. Rad. Transfer 25, 395-402 (1981)
      ! H2O
      !      P. Ciddel  Refractive index of air: new equations for the visible and near infrared 
      !                 Appl. Opt. Vol. 35, Issue 9, pp. 1566-1573 (1996)
      ! N2O, CH4, CO, SF6 
      !      M. Sneep and W. Ubachs Direct measurement of the Rayleigh scattering cross section in various gases
      !               J. Quant. Spectrosc. Rad. Transfer 92  293?310 (2005)
      !
      IMPLICIT NONE
      
      ! --------------------
      ! Subroutine arguments
      ! --------------------
      REAL(KIND=8),                           INTENT(IN)  :: wvl ! nm
      CHARACTER(LEN=4),                       INTENT(IN)  :: scatgas
      REAL(KIND=8),                           INTENT(OUT) :: n_gas
      REAL(KIND=8),                           INTENT(OUT) :: std_ad ! Air density for RI
      
      ! ---------------
      ! Local variables
      ! ---------------
      REAL(KIND=8)                    :: wvl_m2, wvl_cm2
      
      ! ==========================================================================
      ! compute_king_factor starts here
      ! ==========================================================================
      
      ! Compute wvl^-2 in (microns^-2)
      wvl_m2 = 1.0d6/wvl/wvl
      
      ! And in cm^-2
      wvl_cm2 = wvl_m2*1.0d8
      
      ! Get refractive indexfor each gas
      ! --------------------------------
      
      ! Nitrogen
      IF     ( TRIM(ADJUSTL(scatgas)) == 'N2'  ) THEN
        
        ! Refractive index @ OC, 101325Pa
        n_gas = 1.0d0 + 6.8552d-5 + 3.243157d-2/(144.0d0-wvl_m2)
        
        ! Air density @ OC, 101325Pa [molec/cm3]
        std_ad = 2.6867811d19
          
      ! Oxygen
      ELSE IF( TRIM(ADJUSTL(scatgas)) == 'O2'  ) THEN
        
        ! Refractive index @ 20C, 101325Pa
        n_gas = 1.0d0 + 1.181494d-4 + 9.708931d-3/(75.4d0-wvl_m2)
        
        ! Air density @ 20C, 101325Pa [molec/cm3]
        std_ad = 2.5034769e+19
        
      ! Carbon dioxide
      ELSE IF( TRIM(ADJUSTL(scatgas)) == 'CO2'  ) THEN
        
        ! Refractive index @ 0C, 101325Pa
        n_gas = 1.0d0 + 0.00000154489d0/(0.0584738d0-wvl_m2) &
              + 0.083091927d0/(210.9241d0-wvl_m2)            &
              + 0.0028764190d0/(60.122959d0-wvl_m2)
          
        ! Air density @ OC, 101325Pa [molec/cm3]
        std_ad = 2.6867811d19
        
      ! Argon
      ELSE IF( TRIM(ADJUSTL(scatgas)) == 'Ar'  ) THEN
        
        ! Refractive index @ 0C, 101325Pa
        n_gas = 1.0d0 + 2.50141d-3/(91.012-wvl_m2) &
              + 5.00283d-4/(87.892-wvl_m2)         &
              + 5.22343e-2/(214.02-wvl_m2)
        
        ! Air density @ OC, 101325Pa [molec/cm3]
        std_ad = 2.6867811d19
        
      ELSE IF( TRIM(ADJUSTL(scatgas)) == 'H2O'  ) THEN
        
        ! Refractive index @ 20C, 1333Pa <--- Pa!!!!
        n_gas = 1.0d0 + 1.022d-8*( 295.235d0 &
              + 2.6422*wvl_m2                &
              - 0.032380*wvl_m2**2           &
              + 0.004028*wvl_m2**3           )
        
        ! Air density @ 20C, 1333Pa [molec/cm3]
        std_ad = 3.2934959d17
        
      ELSE IF( TRIM(ADJUSTL(scatgas)) == 'CO'  ) THEN
        
        ! Refractive index @ 15C, 101300Pa
        n_gas = 1.0d0 + 1.0d-8*( 22851.0d0     &
              + 0.456d12/(5.1018163d9-wvl_cm2) )
        
        ! Air density @ 15C, 101300Pa [molec/cm3]
        std_ad = 2.5462890d19
        
      ELSE IF( TRIM(ADJUSTL(scatgas)) == 'CH4'  ) THEN
        
        ! Refractive index @ 15C, 101300Pa
        n_gas = 1.0d0 + 1.0d-8*( 46662.0d0 &
              + 4.02d-6*wvl_cm2            )
        
        ! Air density @ 15C, 101300Pa [molec/cm3]
        std_ad = 2.5462890d19
        
      ELSE IF( TRIM(ADJUSTL(scatgas)) == 'N2O'  ) THEN
        
        ! Refractive index @ 15C, 101300Pa
        n_gas = 1.0d0 + 1.0d-8*( 46890.0d0 &
              + 4.12d-6*wvl_cm2            )
        
        ! Air density @ 15C, 101300Pa [molec/cm3]
        std_ad = 2.5462890d19
        
      ELSE IF( TRIM(ADJUSTL(scatgas)) == 'SF6'  ) THEN
        
        ! Refractive index @ 15C, 101300Pa
        n_gas = 1.0d0 + 1.0d-8 *( 71517.0d0 &
              + 4.996d-6*wvl_cm2            )
        
        ! Air density @ 15C, 101300Pa [molec/cm3]
        std_ad = 2.5462890d19
        
      ELSE 
        
        print*,'Could not find Refractive Index for ' // TRIM(ADJUSTL(scatgas))
        STOP 
        
      ENDIF
      
    END SUBROUTINE compute_gas_refractive_index
    
    SUBROUTINE RAMAN_N2 (temp, n2pos, n2xsec)
      
      !implicit real*8 (a - h, o - z)
      
      
      REAL(KIND=8), INTENT(IN)                 :: temp
      REAL(KIND=8), DIMENSION(48), INTENT(OUT) :: n2pos, n2xsec
      
      ! Statistical parameters, for partition function calculation
      real(kind=8) n2stat_1 (31), n2stat_2 (31), n2stat_3 (31)
      ! Rotational Raman line-specific parameters
      real(kind=8) n2term (48), n2plactel (48), n2deg (48), n2nuc (48)
      real(kind=8) n2frac (48)
      
      data n2stat_1 /  0., 1., 2., 3., 4., 5., 6., 7., 8.,      &
           9., 10., 11., 12., 13., 14., 15., 16., 17., 18., 19.,&
           20., 21., 22., 23., 24., 25., 26., 27., 28., 29., 30. /
      
      data n2stat_2 /  6., 3., 6., 3., 6., 3., 6., 3., 6.,&
           3., 6., 3., 6., 3., 6., 3., 6., 3., 6., 3.,&
           6., 3., 6., 3., 6., 3., 6., 3., 6., 3., 6. /
      
      data n2stat_3 / 0.0000, 3.9791, 11.9373, 23.8741, 39.7892,&
           59.6821, 83.5521, 111.3983, 143.2197, 179.0154,&
           218.7839, 262.5240, 310.2341, 361.9126, 417.5576,&
           477.1673, 540.7395, 608.2722, 679.7628, 755.2090,&
           834.6081, 917.9574, 1005.2540, 1096.4948, 1191.6766,&
           1290.7963, 1393.8503, 1500.8350, 1611.7467, 1726.5816,&
           1845.3358 /
      
      data n2term /&
           1290.7963, 1191.6766, 1096.4948, 1005.2540, 917.9574,&
           834.6081, 755.2090, 679.7628, 608.2722, 540.7395,&
           477.1673, 417.5576, 361.9126, 310.2341, 262.5240,&
           218.7839, 179.0154, 143.2197, 111.3983, 83.5521,&
           59.6821, 39.7892, 23.8741, 11.9373, 0.0000,&
           3.9791, 11.9373, 23.8741, 39.7892, 59.6821,&
           83.5521, 111.3983, 143.2197, 179.0154, 218.7839,&
           262.5240, 310.2341, 361.9126, 417.5576, 477.1673,&
           540.7395, 608.2722, 679.7628, 755.2090, 834.6081,&
           917.9574, 1005.2540, 1096.4948 /
      
      data n2plactel / &
           3.601d-1, 3.595d-1, 3.589d-1, 3.581d-1, 3.573d-1,&
           3.565d-1, 3.555d-1, 3.544d-1, 3.532d-1, 3.519d-1,&
           3.504d-1, 3.487d-1, 3.467d-1, 3.443d-1, 3.416d-1,&
           3.383d-1, 3.344d-1, 3.294d-1, 3.231d-1, 3.147d-1,&
           3.030d-1, 2.857d-1, 2.571d-1, 2.000d-1, 1.000d+0,&
           6.000d-1, 5.143d-1, 4.762d-1, 4.545d-1, 4.406d-1,&
           4.308d-1, 4.235d-1, 4.180d-1, 4.135d-1, 4.099d-1,&
           4.070d-1, 4.044d-1, 4.023d-1, 4.004d-1, 3.988d-1,&
           3.974d-1, 3.961d-1, 3.950d-1, 3.940d-1, 3.931d-1,&
           3.922d-1, 3.915d-1, 3.908d-1 /
      
      data n2deg / 25., 24., 23., 22., 21., 20., 19., 18., 17., 16.,&
           15., 14., 13., 12., 11., 10., 9., 8., 7., 6.,&
           5., 4., 3., 2., 0., 1., 2., 3., 4., 5.,&
           6., 7., 8., 9., 10., 11., 12., 13., 14., 15.,&
           16., 17., 18., 19., 20., 21., 22., 23. /
      
      data n2nuc / &
           3., 6., 3., 6., 3., 6., 3., 6., 3., 6., 3., 6., 3., 6.,&
           3., 6., 3., 6., 3., 6., 3., 6., 3., 6., 6., 3., 6., 3., 6.,&
           3., 6., 3., 6., 3., 6., 3., 6., 3., 6., 3., 6., 3., 6., 3.,&
           6., 3., 6., 3. /
      
      real(KIND=8)  :: qn2, emult, prefix
      integer :: i
      
      real(KIND=8), parameter :: pi = 3.14159265358979d0
      real(KIND=8), parameter :: c2 = 1.438769d0
      
      ! ================================================================
      
      
      ! N2 rotational raman line positions
      n2pos = (/ &
           -194.3015, -186.4226, -178.5374, -170.6459, -162.7484,&
           -154.8453, -146.9368, -139.0233, -131.1049, -123.1819,&
           -115.2547, -107.3235, -99.3886, -91.4502, -83.5086,&
           -75.5642, -67.6171, -59.6676, -51.7162, -43.7629,&
           -35.8080, -27.8519, -19.8950, -11.9373, 11.9373,&
           19.8950, 27.8519, 35.8080, 43.7629, 51.7162,&
           59.6676, 67.6171, 75.5642, 83.5086, 91.4502,&
           99.3886, 107.3235, 115.2547, 123.1819, 131.1049,&
           139.0233, 146.9368, 154.8453, 162.7484, 170.6459,&
           178.5374, 186.4226, 194.3015 /)
      
      
      emult = - c2 / temp
      
      ! Calculate partition function
      qn2 = 0.0d0
      DO i = 1, 31
         qn2 = qn2 + (2. * n2stat_1 (i) + 1.) * n2stat_2 (i) * &
              exp (emult * n2stat_3 (i))
      ENDDO
      
      prefix = 256.d0 * (pi**5) * 0.79d0 / 27.d0
      
      ! Calculate population fractions for rotational Raman lines and
      ! the cross sections, except for gamma**2/lambda**4
      DO i = 1, 48
         n2frac(i) = (2. * n2deg (i) + 1.) * n2nuc(i) * exp(emult * n2term (i)) / qn2
         n2xsec(i) = prefix * n2frac(i) * n2plactel(i)
      ENDDO
      
      RETURN
    END SUBROUTINE RAMAN_N2
    
    SUBROUTINE RAMAN_O2 (temp, o2pos, o2xsec)

      !implicit real*8 (a - h, o - z)
      
      REAL(KIND=8),                  INTENT(IN) :: temp
      REAL(KIND=8), DIMENSION(185), INTENT(OUT) :: o2pos, o2xsec
      
      ! Statistical parameters, for partition function calculation
      real(kind=8) ::  o2stat_1 (54), o2stat_2 (54)
      ! Rotational Raman line-specific parameters
      real(kind=8) :: o2term (185), o2plactel (185), o2deg (185)
      real(kind=8) :: o2frac (185) 

      data o2stat_1 / 0., 2., 1., 2.,&
       4., 3., 4., 6., 5., 8., 6., 7., 10., 8.,&
       9., 12., 10., 11., 14., 12., 13., 16., 14., 15.,&
       18., 16., 17., 20., 18., 19., 22., 20., 21., 24.,&
       22., 23., 26., 24., 25., 28., 26., 27., 30., 28.,&
       29., 32., 30., 31., 34., 32., 33., 36., 34., 35. /

      data o2stat_2 / &
       0.0000, 2.0843, 3.9611, 16.2529, 16.3876, 18.3372,&
       42.2001, 42.2240, 44.2117, 79.5646, 79.6070, 81.5805,&
       128.3978, 128.4921, 130.4376, 188.7135, 188.8532, 190.7749,&
       260.5011, 260.6826, 262.5829, 343.7484, 343.9697, 345.8500,&
       438.4418, 438.7015, 440.5620, 544.5658, 544.8628, 546.7050,&
       662.1030, 662.4368, 664.2610, 791.0344, 791.4045, 793.2100,&
       931.3390, 931.7450, 933.5330, 1082.9941, 1083.4356,&
       1085.2060, 1245.9750, 1246.4518, 1248.2040, 1420.2552,&
       1420.7672, 1422.5020, 1605.8064, 1606.3533, 1608.0710,&
       1802.5983, 1803.1802, 1804.8810 /

      data o2term / &
       1606.3533, 1608.0710, 1605.8064, 1420.7672, 1422.5020,&
       1420.2552, 1246.4518, 1248.2040, 1245.9750, 1083.4356,&
       1085.2060, 1082.9941, 931.7450, 933.5330, 931.3390,&
       791.4045, 793.2100, 791.0344, 662.4368, 664.2610,&
       662.1030, 546.7050, 544.8628, 546.7050, 544.5658,&
       544.8628, 440.5620, 438.7015, 440.5620, 438.4418,&
       438.7015, 345.8500, 343.9697, 345.8500, 343.7484,&
       343.9697, 262.5829, 260.6826, 262.5829, 260.5011,&
       260.6826, 190.7749, 188.8532, 190.7749, 188.7135,&
       188.8532, 130.4376, 128.4921, 130.4376, 128.3978,&
       128.4921, 42.2001, 81.5805, 79.6070, 81.5805,&
       79.5646, 79.6070, 44.2117, 42.2001, 44.2117,&
       42.2240, 42.2001, 42.2001, 16.2529, 18.3372,&
       18.3372, 16.3876, 16.2529, 16.2529, 546.7050,&
       440.5620, 345.8500, 2.0843, 18.3372, 262.5829,&
       190.7749, 130.4376, 81.5805, 44.2117, 44.2117,&
       81.5805, 18.3372, 130.4376, 190.7749, 262.5829,&
       345.8500, 3.9611, 440.5620, 546.7050, 16.3876,&
       16.2529, 544.8628, 438.7015, 2.0843, 343.9697,&
       260.6826, 188.8532, 128.4921, 16.3876, 79.6070,&
       42.2240, 42.2001, 79.5646, 128.3978, 188.7135,&
       260.5011, 0.0000, 16.2529, 343.7484, 438.4418,&
       544.5658, 3.9611, 2.0843, 2.0843, 3.9611,&
       0.0000, 2.0843, 18.3372, 16.3876, 16.3876,&
       18.3372, 16.2529, 16.3876, 44.2117, 42.2240,&
       44.2117, 42.2001, 42.2240, 2.0843, 81.5805,&
       79.5646, 81.5805, 79.6070, 79.5646, 130.4376,&
       128.3978, 130.4376, 128.4921, 128.3978, 190.7749,&
       188.7135, 190.7749, 188.8532, 188.7135, 262.5829,&
       260.5011, 262.5829, 260.6826, 260.5011, 345.8500,&
       343.7484, 345.8500, 343.9697, 343.7484, 440.5620,&
       438.4418, 440.5620, 438.7015, 438.4418, 546.7050,&
       544.5658, 546.7050, 544.8628, 544.5658, 662.1030,&
       664.2610, 662.4368, 791.0344, 793.2100, 791.4045,&
       931.3390, 933.5330, 931.7450, 1082.9941, 1085.2060,&
       1083.4356, 1245.9750, 1248.2040, 1246.4518, 1420.2552,&
       1422.5020, 1420.7672, 1605.8064, 1608.0710, 1606.3533 /

      data o2plactel / &
       3.630d-1, 3.630d-1, 3.637d-1, 3.622d-1, 3.622d-1,&
       3.630d-1, 3.612d-1, 3.613d-1, 3.622d-1, 3.602d-1,&
       3.602d-1, 3.612d-1, 3.589d-1, 3.589d-1, 3.602d-1,&
       3.574d-1, 3.574d-1, 3.589d-1, 3.555d-1, 3.556d-1,&
       3.574d-1, 2.003d-3, 3.533d-1, 3.534d-1, 3.555d-1,&
       2.275d-3, 2.492d-3, 3.504d-1, 3.506d-1, 3.533d-1,&
       2.874d-3, 3.184d-3, 3.467d-1, 3.471d-1, 3.505d-1,&
       3.743d-3, 4.209d-3, 3.417d-1, 3.422d-1, 3.468d-1,&
       5.076d-3, 5.822d-3, 3.345d-1, 3.354d-1, 3.418d-1,&
       7.271d-3, 8.577d-3, 3.233d-1, 3.251d-1, 3.347d-1,&
       1.127d-2, 1.030d-3, 1.387d-2, 3.037d-1, 3.077d-1,&
       3.236d-1, 1.979d-2, 2.613d-2, 2.599d-1, 2.727d-1,&
       3.045d-1, 1.458d-3, 4.342d-2, 9.234d-2, 6.596d-2,&
       1.714d-1, 2.627d-1, 1.775d-2, 1.615d-1, 1.905d-3,&
       2.356d-3, 2.989d-3, 1.077d-1, 7.690d-2, 3.916d-3,&
       5.353d-3, 7.753d-3, 1.222d-2, 2.842d-2, 2.207d-2,&
       1.470d-2, 5.132d-2, 8.967d-3, 6.036d-3, 4.338d-3,&
       3.268d-3, 2.308d-1, 2.550d-3, 2.045d-3, 2.073d-4,&
       3.731d-4, 2.156d-3, 2.705d-3, 1.385d-1, 3.493d-3,&
       4.685d-3, 6.610d-3, 1.002d-2, 3.991d-2, 1.696d-2,&
       1.867d-2, 3.474d-2, 1.078d-2, 7.014d-3, 4.924d-3,&
       3.646d-3, 5.383d-1, 1.077d-1, 2.808d-3, 2.229d-3,&
       1.812d-3, 2.692d-1, 1.775d-2, 4.729d-1, 4.000d-1,&
       4.617d-1, 9.234d-2, 5.583d-2, 1.458d-3, 4.398d-1,&
       4.286d-1, 4.678d-1, 3.193d-2, 2.339d-2, 4.232d-1,&
       4.196d-1, 4.387d-1, 1.600d-2, 1.854d-3, 1.278d-2,&
       4.134d-1, 4.118d-1, 4.228d-1, 9.586d-3, 8.037d-3,&
       4.069d-1, 4.060d-1, 4.132d-1, 6.377d-3, 5.517d-3,&
       4.022d-1, 4.017d-1, 4.068d-1, 4.546d-3, 4.020d-3,&
       3.988d-1, 3.985d-1, 4.022d-1, 3.403d-3, 3.059d-3,&
       3.961d-1, 3.959d-1, 3.988d-1, 2.643d-3, 2.405d-3,&
       3.940d-1, 3.938d-1, 3.961d-1, 2.112d-3, 1.941d-3,&
       3.922d-1, 3.921d-1, 3.940d-1, 1.726d-3, 3.908d-1,&
       3.907d-1, 3.922d-1, 3.896d-1, 3.895d-1, 3.908d-1,&
       3.884d-1, 3.885d-1, 3.896d-1, 3.876d-1, 3.876d-1,&
       3.884d-1, 3.868d-1, 3.868d-1, 3.876d-1, 3.861d-1,&
       3.861d-1, 3.868d-1, 3.855d-1, 3.855d-1, 3.861d-1 /

      data o2deg / &
       32., 33., 34., 30., 31., 32., 28., 29., 30., 26., 27.,&
       28., 24., 25., 26., 22., 23., 24., 20., 21., 22., 19., 18.,&
       19., 20., 18., 17., 16., 17., 18., 16., 15., 14., 15., 16.,&
       14., 13., 12., 13., 14., 12., 11., 10., 11., 12., 10., 9., 8.,&
       9., 10., 8., 4., 7., 6., 7., 8., 6., 5., 4., 5., 6., 4., 4.,&
       2., 3., 3., 4., 2., 2., 19., 17., 15., 2., 3., 13., 11., 9.,&
       7., 5., 5., 7., 3., 9., 11., 13., 15., 1., 17., 19., 4., 2.,&
       18., 16., 2., 14., 12., 10., 8., 4., 6., 6., 4., 8., 10., 12.,&
       14., 0., 2., 16., 18., 20., 1., 2., 2., 1., 0., 2., 3., 4., 4.,&
       3., 2., 4., 5., 6., 5., 4., 6., 2., 7., 8., 7., 6., 8., 9.,&
       10., 9., 8., 10., 11., 12., 11., 10., 12., 13., 14., 13., 12.,&
       14., 15., 16., 15., 14., 16., 17., 18., 17., 16., 18., 19.,&
       20., 19., 18., 20., 22., 21., 20., 24., 23., 22., 26., 25.,&
       24., 28., 27., 26., 30., 29., 28., 32., 31., 30., 34., 33.,&
       32. /
      
      ! Local variables
      real(kind=8) :: emult,qo2,prefix
      integer      :: i
      
      real(kind=8), parameter :: pi = 3.14159265358979d0
      real(kind=8), parameter :: c2 = 1.438769d0
      emult = - c2 / temp
      
      ! ============================================================


      ! O2 rotational raman line positions
      o2pos = (/ &
      -185.5861, -185.5690, -185.5512, -174.3154, -174.2980,&
      -174.2802, -163.0162, -162.9980, -162.9809, -151.6906,&
      -151.6730, -151.6551, -140.3405, -140.3230, -140.3046,&
        -128.9677, -128.9490, -128.9314, -117.5740, -117.5560,&
       -117.5372, -108.2632, -106.1613, -106.1430, -106.1240,&
       -104.3008, -96.8136, -94.7318, -94.7120, -94.6934,&
       -92.8515, -85.3489, -83.2871, -83.2671, -83.2473,&
       -81.3868, -73.8694, -71.8294, -71.8080, -71.7876,&
       -69.9077, -62.3771, -60.3611, -60.3373, -60.3157,&
       -58.4156, -50.8730, -48.8851, -48.8571, -48.8332,&
       -46.9116, -40.1158, -39.3565, -37.4069, -37.3688,&
       -37.3406, -35.3953, -27.8241, -25.9472, -25.8745,&
       -25.8364, -25.8125, -23.8629, -16.2529, -16.2529,&
       -14.3761, -14.3033, -14.1686, -12.2918, -2.1392,&
       -2.1202, -2.1016, -2.0843, -2.0843, -2.0818,&
       -2.0614, -2.0398, -2.0159, -2.0116, -1.9877,&
       -1.9735, -1.9496, -1.9455, -1.9217, -1.9003,&
       -1.8803, -1.8768, -1.8605, -1.8422, -0.1347,&
       0.1347, 1.8422, 1.8605, 1.8768, 1.8803,&
       1.9003, 1.9217, 1.9455, 1.9496, 1.9735,&
       1.9877, 2.0116, 2.0159, 2.0398, 2.0614,&
       2.0818, 2.0843, 2.0843, 2.1016, 2.1202,&
       2.1392, 12.2918, 14.1686, 14.3033, 14.3761,&
       16.2529, 16.2529, 23.8629, 25.8125, 25.8364,&
       25.8745, 25.9472, 27.8241, 35.3953, 37.3406,&
       37.3688, 37.4069, 39.3565, 40.1158, 46.9116,&
       48.8332, 48.8571, 48.8851, 50.8730, 58.4156,&
       60.3157, 60.3373, 60.3611, 62.3771, 69.9077,&
       71.7876, 71.8080, 71.8294, 73.8694, 81.3868,&
       83.2473, 83.2671, 83.2871, 85.3489, 92.8515,&
       94.6934, 94.7120, 94.7318, 96.8136, 104.3008,&
       106.1240, 106.1430, 106.1613, 108.2632, 115.7318,&
       117.5372, 117.5560, 117.5740, 119.6952, 128.9314,&
       128.9490, 128.9677, 140.3046, 140.3230, 140.3405,&
       151.6551, 151.6730, 151.6906, 162.9809, 162.9980,&
       163.0162, 174.2802, 174.2980, 174.3154, 185.5512,&
       185.5690, 185.5861, 196.7919, 196.8100, 196.8269 /)
      
      
      ! Calculate partition function
      qo2 = 0.
      DO i = 1, 54
        qo2 = qo2 + (2. * o2stat_1 (i) + 1.) * &
              exp (emult * o2stat_2 (i))
      ENDDO

      prefix = 256.d0 * (pi**5) * 0.21d0 / 27.d0

      ! Calculate population fractions for rotational Raman lines and
      ! the cross sections, except for gamma**2/lambda**4
      DO i = 1, 185
        o2frac (i) = (2. * o2deg (i) + 1.) * &
                     exp (emult * o2term (i)) / qo2
        o2xsec (i) = prefix * o2frac (i) * o2plactel (i)
      ENDDO

      RETURN
  END SUBROUTINE RAMAN_O2 
  
    
  SUBROUTINE spline(x,y,n,yp1,ypn,y2)
      INTEGER            ::  n
      REAL(KIND=8)       :: yp1,ypn,x(n),y(n),y2(n)
!xl      INTEGER, PARAMETER :: NMAX=6094
      INTEGER            :: i,k
!xl     REAL(Kind=8)       :: p,qn,sig,un,u(NMAX)
      REAL(Kind=8)       :: p,qn,sig,un,u(n)

!  Check dimension

!xl      if (n.gt.NMAX) then
!xl        write(*,*)'Error in spline routine: too small NMAX =',NMAX
!xl        stop
!xl      endif

      if (yp1.gt..99e30) then
        y2(1)=0.0d0
        u(1)=0.0d0
      else
        y2(1)=-0.5d0
        u(1)=(3.0d0/(x(2)-x(1)))*((y(2)-y(1))/(x(2)-x(1))-yp1)
      endif

      do i=2,n-1
        sig=(x(i)-x(i-1))/(x(i+1)-x(i-1))
        p=sig*y2(i-1)+2.
        y2(i)=(sig-1.)/p
        u(i)=(6.0d0*( (y(i+1)-y(i)) / (x(i+1)-x(i)) - &
                     (y(i)-y(i-1)) / (x(i)-x(i-1))   &
                   ) / (x(i+1)-x(i-1)) - sig*u(i-1)  &
            )/p
      enddo

      if (ypn.gt..99d30) then
        qn=0.0d0
        un=0.d0
      else
        qn=0.5d0
        un=(3.0d0/(x(n)-x(n-1)))*(ypn-(y(n)-y(n-1))/(x(n)-x(n-1)))
      endif

      y2(n)=(un-qn*u(n-1))/(qn*y2(n-1)+1.)
      do k=n-1,1,-1
        y2(k)=y2(k)*y2(k+1)+u(k)
      enddo

      return
  END SUBROUTINE SPLINE


  SUBROUTINE splint(xa,ya,y2a,n,x,y)
      INTEGER            ::  n
      REAL(KIND=8)       :: x, y, xa(n),ya(n),y2a(n)
      INTEGER            ::  k,khi,klo
      REAL(KIND=8)       :: a,b,h

      klo=1
      khi=n

 1    if (khi-klo.gt.1) then
          k=(khi+klo)/2
          if(xa(k).gt.x)then
            khi=k
          else
            klo=k
          endif
       goto 1
       endif
       h=xa(khi)-xa(klo)
       if (h.eq.0.0d0) stop
       a=(xa(khi)-x)/h
       b=(x-xa(klo))/h
       y=a*ya(klo)+b*ya(khi)+ &
        ((a**3-a)*y2a(klo)+(b**3-b)*y2a(khi))*(h**2)/6.0d0

      return
  END SUBROUTINE SPLINT

  SUBROUTINE get_hitran_lut_crs( filename, max_ch_len, wvl, nwvl, solwvl, solspc, nsol,          &
       pres, temp, nz, scl_vcd, fwhm, convolve_ils , fwhm_in_wvl, xsec )
    
    USE GC_utilities_module, ONLY: bspline, gauss_f2ci0
    IMPLICIT NONE
    INCLUDE 'netcdf.inc'
    
    
    ! Inputs
    INTEGER, INTENT(IN)                          :: nwvl, nz, nsol, max_ch_len
    REAL(KIND=8),                  INTENT(IN)    :: scl_vcd, fwhm
    REAL(KIND=8), DIMENSION(nwvl), INTENT(IN)    :: wvl
    REAL(KIND=8), DIMENSION(nsol), INTENT(IN)    :: solspc, solwvl
    REAL(KIND=8), DIMENSION(nz  ), INTENT(IN)    :: pres, temp
    CHARACTER(LEN=max_ch_len),     INTENT(IN)    :: filename
    LOGICAL,                       INTENT(IN)    :: convolve_ils, fwhm_in_wvl
    ! Output 
    REAL(KIND=8),DIMENSION(nwvl,nz), INTENT(OUT) :: xsec
    
    ! LUT 
    REAL(KIND=8), ALLOCATABLE, DIMENSION(:)     :: T_lut, p_lut, wvl_lut
    REAL(KIND=8), ALLOCATABLE, DIMENSION(:,:,:) :: xs_lut
    REAL(KIND=8), ALLOCATABLE, DIMENSION(:)     :: xs_int, sol_lut
    INTEGER :: nwvl_lut, np_lut, nT_lut
    INTEGER :: ncid, rcode, var_id
    INTEGER :: posdim, pdim, Tdim
    LOGICAL :: fail
    INTEGER :: errstat
    CHARACTER(len=max_ch_len) :: message
    CHARACTER(len=31) :: dimname
    REAL(KIND=8) :: pmin, pmax, Tmin, Tmax, wmin, wmax,min_wvl, max_wvl
    INTEGER      :: idx0, idy0, wid0,widf, nwlsub
    REAL(KIND=8) :: xwt0, xwt1, ywt0, ywt1
    INTEGER :: i, j
    
    ! ================================================================
    ! get_hitran_lut_crs starts here
    ! ================================================================
    
    ! ================================================================
    ! Open netCDF file and allocate arrays
    ! ================================================================
    
    ! Stop code if using wavenumbers ( need to add this option )
    IF(.NOT. fwhm_in_wvl) STOP 'Currently have not implemented wavenumber grids for HITRAN LUT XS'
    
    ! Open file in read mode
    ncid = ncopn(trim(adjustl(filename)), nf_Nowrite, rcode)
    if (rcode  .eq. -1 ) then
       message =  ' error in read_xy_nc_prof: ncopn failed'
       fail = .true.; return
    endif
    
    ! Get the wavelength dimension
    posdim = ncdid(ncid, 'npos', RCODE)
    if (rcode  .eq. -1 ) then
       message =  ' error in read_xy_nc_prof: ncdid failed(npos)'
       fail = .true.; return
    endif
    
    ! Read wavelength dimension
    call ncdinq(ncid, posdim, dimname, nwvl_lut, rcode)
    if (rcode  .eq. -1 ) then
       message =  ' error in read_xy_nc_prof: ncdinq failed(npos)'
       fail = .true.; return
    endif
    
    ! Get the temperature dimension
    Tdim = ncdid(ncid, 'nT', RCODE)
    if (rcode  .eq. -1 ) then
       message =  ' error in read_xy_nc_prof: ncdid failed(nT)'
       fail = .true.; return
    endif
    
    ! Get temperature  dimension
    call ncdinq(ncid, Tdim, dimname, nT_lut, rcode)
    if (rcode  .eq. -1 ) then
       message =  ' error in read_xy_nc_prof: ncdinq failed(nT)'
       fail = .true.; return
    endif
    
    ! Get the temperature dimension
    pdim = ncdid(ncid, 'np', RCODE)
    if (rcode  .eq. -1 ) then
       message =  ' error in read_xy_nc_prof: ncdid failed(np)'
       fail = .true.; return
    endif
    
    ! Get temperature  dimension
    call ncdinq(ncid, pdim, dimname, np_lut, rcode)
    if (rcode  .eq. -1 ) then
       message =  ' error in read_xy_nc_prof: ncdinq failed(np)'
       fail = .true.; return
    endif
    
    ! Allocate arrays
    ALLOCATE(wvl_lut(nwvl_lut))
    ALLOCATE(T_lut(nT_lut))
    ALLOCATE(p_lut(np_lut))
    ALLOCATE(xs_lut(nwvl_lut,nT_lut,np_lut))
    ALLOCATE(xs_int(nwvl_lut))
    ALLOCATE(sol_lut(nwvl_lut))
    
    ! ================================================================
    ! Read the variables
    ! ================================================================
    
    ! ---------------------
    ! Read wavelength grid
    ! ---------------------
    
    var_id = ncvid(ncid, 'Wavelength', rcode)
    if (rcode  .eq. -1 ) then
       message =  ' error in netcdf_rd_dim: ncvid failed(Wavelength)'
       fail = .true.; return
    endif
    call ncvgt(ncid, var_id, (/1/), (/nwvl_lut/), wvl_lut, rcode)
    if (rcode  .eq. -1 ) then
       message =  ' error in netcdf_rd_dim: ncvgt failed(Longitude)'
       fail = .true.; return
    endif
    
    ! ----------------------
    ! Read temperature grid
    ! ----------------------
    
    var_id = ncvid(ncid, 'Temperature', rcode)
    if (rcode  .eq. -1 ) then
       message =  ' error in netcdf_rd_dim: ncvid failed(Temperature)'
       fail = .true.; return
    endif
    call ncvgt(ncid, var_id, (/1/), (/nT_lut/), T_lut, rcode)
    if (rcode  .eq. -1 ) then
       message =  ' error in netcdf_rd_dim: ncvgt failed(Longitude)'
       fail = .true.; return
    endif
    
    ! ----------------------
    ! Read pressure grid
    ! ----------------------
    
    var_id = ncvid(ncid, 'Pressure', rcode)
    if (rcode  .eq. -1 ) then
       message =  ' error in netcdf_rd_dim: ncvid failed(Pressure)'
       fail = .true.; return
    endif
    call ncvgt(ncid, var_id, (/1/), (/np_lut/), p_lut, rcode)
    if (rcode  .eq. -1 ) then
       message =  ' error in netcdf_rd_dim: ncvgt failed(Pressure)'
       fail = .true.; return
    endif
    
    ! ----------------------
    ! Read cross sections
    ! ----------------------
    
    var_id = ncvid(ncid, 'CrossSection', rcode)
    if (rcode  .eq. -1 ) then
       message =  ' error in netcdf_rd_dim: ncvid failed(Pressure)'
       fail = .true.; return
    endif
    call ncvgt(ncid, var_id, (/1,1,1/), (/nwvl_lut,nT_lut,np_lut/), xs_lut, rcode)
    if (rcode  .eq. -1 ) then
       message =  ' error in netcdf_rd_dim: ncvgt failed(Pressure)'
       fail = .true.; return
    endif
    
    ! ----------
    ! Close file
    ! ----------
    
    call ncclos(ncid, rcode)
    if (rcode .eq. -1) then
       message =  ' error in netcdf_rd_dim: ncclos'
       fail = .true.; return
    endif
    
    ! Get max/min p and T
    pmin = minval(p_lut)
    pmax = maxval(p_lut)
    Tmin = minval(T_lut)
    Tmax = maxval(T_lut)
    wmin = minval(wvl_lut)
    wmax = maxval(wvl_lut)
    
    ! ================================================================
    ! Interpolate spectra and convolve to output grid
    ! ================================================================
    
    ! Use the solar limits to determine the output range
    min_wvl = minval(solwvl(1:nsol))
    max_wvl = maxval(solwvl(1:nsol))
    
    ! Find the indices
    wid0   = MINVAL(MINLOC( wvl_lut(1:nwvl_lut), MASK = ( wvl_lut(1:nwvl_lut) .GE. min_wvl ) ))
    widf   = MINVAL(MAXLOC( wvl_lut(1:nwvl_lut), MASK = ( wvl_lut(1:nwvl_lut) .LE. max_wvl ) ))
    
    ! Pad indices
    wid0 = MAX(wid0-1,1)
    widf = MIN(widf+1,nwvl_lut)
    
    ! # of indices
    nwlsub = widf-wid0+1
    
    IF( convolve_ils ) THEN
       ! solar reference at the LUT grid
       CALL bspline (solwvl(1:nsol), solspc(1:nsol), nsol, &
            wvl_lut(wid0:widf), sol_lut(wid0:widf), nwlsub, &
            errstat)
       
       IF ( errstat < 0 ) THEN
          print*,'SOLWVL RANGE:',minval(solwvl(1:nsol)),maxval(solwvl(1:nsol))
          print*,'WVL_LUT RNG :',minval(wvl_lut(wid0:widf)),maxval(wvl_lut(wid0:widf))
          WRITE(*, *) 'BSPLINE interpolation error!!!'
          errstat = 1; RETURN
          RETURN
       ENDIF
    ENDIF
    
    ! In the current version of the code, the LUT must cover the wavelength range
    if ( wvl(1) .LE. wvl_lut(wid0) ) then
       message =  filename // ' Xsec data does not cover input window at lower end'
       print*,message,wvl(1),wvl_lut(1),fwhm
       STOP
    endif
    
    if ( wvl(nwvl) .GE. wvl_lut(widf) ) then
       message =  filename // ' Xsec data does not cover input window at upper end'
       print*,message,wvl(nwvl),wvl_lut(1),fwhm
       STOP
    endif
    
    
    DO i=1,nz
       
       ! Bilinear interpolation to p,T
       
       ! Get pressure coeff
       IF( pres(i) .LE. pmin ) THEN
          idy0 = 1
          ywt0 = 1.0
          ywt1 = 0.0
       ELSEIF( pres(i) .GE. pmax ) THEN 
          idy0 = np_lut-1
          ywt0 = 0.0
          ywt1 = 1.0
       ELSE 
          
          idy0 = -1
          j    =  1
          
          DO WHILE ( idy0 .LT. 0 )
             
             IF( p_lut( j ) .GT. pres(i) ) THEN
                idy0 = j-1
                ywt0 = (p_lut(idy0+1)-pres(i))/(p_lut(idy0+1)-p_lut(idy0))
                ywt1 = (pres(i)-p_lut(idy0)  )/(p_lut(idy0+1)-p_lut(idy0))
             ENDIF
             
             j = j+1
             
          ENDDO
          
       ENDIF
       
       
       ! Get T coeff
       IF( temp(i) .LE. Tmin ) THEN
          idx0 = 1
          xwt0 = 1.0
          xwt1 = 0.0
       ELSEIF( temp(i) .GE. Tmax ) THEN
          idx0 = nT_lut-1
          xwt0 = 0.0
          xwt1 = 1.0
       ELSE 
          
          idx0 = -1
          j    =  1
          DO WHILE (idx0 .LT. 0)
             
             IF( T_lut( j ) .GT. temp(i) ) THEN
                idx0 = j-1
                xwt0 = (T_lut(idx0+1)-temp(i))/(T_lut(idx0+1)-T_lut(idx0))
                xwt1 = (temp(i)-T_lut(idx0)  )/(T_lut(idx0+1)-T_lut(idx0))
             ENDIF
             
             j = j+1
             
          ENDDO
          
       ENDIF
       
       
       ! Do the interpolation
       DO j=1,nwvl_lut
          
          ! Interp over temperature first
          xs_int(j) = ywt0 * ( xwt0*xs_lut(j,idx0,idy0  ) + xwt1*xs_lut(j,idx0+1,idy0  ) ) + &
               ywt1 * ( xwt0*xs_lut(j,idx0,idy0+1) + xwt1*xs_lut(j,idx0+1,idy0+1) )
          
          
          ! Interp over pressure first
          !       xs_int(j) = xwt0 * ( ywt0*xs_lut(j,idx0,idy0  ) + ywt1*xs_lut(j,idx0,idy0+1) ) + &
          !                   xwt1 * ( ywt0*xs_lut(j,idx0+1,idy0  ) + ywt1*xs_lut(j,idx0+1,idy0+1) )
          
       ENDDO
       
       IF( convolve_ils ) THEN
          
          ! Convolve spectrum with slit function and spline to output grid
          CALL gauss_f2ci0(wvl_lut(wid0:widf), xs_int(wid0:widf) , sol_lut(wid0:widf),  &
               nwlsub, 1, scl_vcd, fwhm, wvl(1:nwvl), xsec(1:nwvl,i), nwvl)
          
       ELSE
          
          ! Cross sections in LUT have been preconvolved. Just interpolate to output grid
          CALL bspline (wvl_lut(wid0:widf), xs_int(wid0:widf),  nwlsub, &
               wvl(1:nwvl),        xsec(1:nwvl,i),     nwvl,   &
               errstat                                         )
          
       ENDIF
    ENDDO
    
    ! Deallocate arrays
    DEALLOCATE(wvl_lut)
    DEALLOCATE(T_lut)
    DEALLOCATE(p_lut)
    DEALLOCATE(xs_lut)
    DEALLOCATE(xs_int)
    DEALLOCATE(sol_lut)
    
  END SUBROUTINE get_hitran_lut_crs
  
END MODULE GC_xsections_module
