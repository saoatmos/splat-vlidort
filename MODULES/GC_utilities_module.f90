MODULE GC_utilities_module

PRIVATE
PUBLIC :: bspline, spline, splint
PUBLIC :: satellite_angles
PUBLIC :: ibin
PUBLIC :: reverse, reverse_idxs
PUBLIC :: gauss_f2c, gauss_f2ci0
CONTAINS

SUBROUTINE spline(x,y,n,yp1,ypn,y2)

  IMPLICIT NONE
  INTEGER            ::  n
  REAL(KIND=8)       :: yp1,ypn,x(n),y(n),y2(n)
  INTEGER            :: i,k
  REAL(Kind=8)       :: p,qn,sig,un,u(n)
      
  if (yp1.gt..99e30) then
     y2(1)=0.0d0
     u(1)=0.0d0
  else
     y2(1)=-0.5d0
     u(1)=(3.0d0/(x(2)-x(1)))*((y(2)-y(1))/(x(2)-x(1))-yp1)
  endif
    
  do i=2,n-1
     sig=(x(i)-x(i-1))/(x(i+1)-x(i-1))
     p=sig*y2(i-1)+2.
     y2(i)=(sig-1.)/p
     u(i)=(6.0d0*( (y(i+1)-y(i)) / (x(i+1)-x(i)) - &
          (y(i)-y(i-1)) / (x(i)-x(i-1))   &
          ) / (x(i+1)-x(i-1)) - sig*u(i-1)  &
          )/p
  enddo
    
  if (ypn.gt..99d30) then
     qn=0.0d0
     un=0.d0
  else
     qn=0.5d0
     un=(3.0d0/(x(n)-x(n-1)))*(ypn-(y(n)-y(n-1))/(x(n)-x(n-1)))
  endif
  
  y2(n)=(un-qn*u(n-1))/(qn*y2(n-1)+1.)
  do k=n-1,1,-1
     y2(k)=y2(k)*y2(k+1)+u(k)
  enddo
  
  return
END SUBROUTINE SPLINE

SUBROUTINE splint(xa,ya,y2a,n,x,y)
  INTEGER            ::  n
  REAL(KIND=8)       :: x, y, xa(n),ya(n),y2a(n)
  INTEGER            ::  k,khi,klo
  REAL(KIND=8)       :: a,b,h

  klo=1
  khi=n
  
1 if (khi-klo.gt.1) then
     k=(khi+klo)/2
     if(xa(k).gt.x)then
        khi=k
     else
        klo=k
     endif
     goto 1
  endif
  h=xa(khi)-xa(klo)
  if (h.eq.0.0d0) stop
  a=(xa(khi)-x)/h
  b=(x-xa(klo))/h
  y=a*ya(klo)+b*ya(khi)+ &
       ((a**3-a)*y2a(klo)+(b**3-b)*y2a(khi))*(h**2)/6.0d0
  
  return
END SUBROUTINE SPLINT

! This subroutine combines spline and splint function
! in Numberical Recipes by Press et al., 1997.
SUBROUTINE BSPLINE(xa, ya, n, x, y, np, errstat)

  IMPLICIT NONE
  INTEGER, PARAMETER  :: dp = KIND(1.0D0)
  
  INTEGER, INTENT (IN)                      :: n, np
  INTEGER, INTENT (OUT)                     :: errstat
  REAL (KIND=dp), DIMENSION(n),  INTENT(IN) :: xa, ya
  REAL (KIND=dp), DIMENSION(np), INTENT(IN) :: x
  REAL (KIND=dp), DIMENSION(np), INTENT(OUT):: y

  
  REAL (KIND=dp), DIMENSION(n)              :: y2a, xacpy
  REAL (KIND=dp), DIMENSION(np)             :: xcpy
  REAL (KIND=dp), DIMENSION(n-1)            :: diff
  REAL (KIND=dp)                            :: xmin, xmax, xamin, xamax
  
  errstat = 0
  IF (n < 3) THEN
     errstat = - 1; RETURN
  ENDIF
  
  diff = xa(2:n) - xa(1:n-1)
  IF (.NOT. (ALL(diff > 0) .OR. ALL(diff < 0))) THEN
     errstat =  -2; RETURN
  ENDIF

  xmin = MINVAL(x); xmax = MAXVAL(x)
  xamin = MINVAL(xa); xamax = MAXVAL(xa)
  IF (xmin < xamin .OR. xmax > xamax) THEN
     errstat =  -3; RETURN
  ENDIF
  
  IF (xa(1) < xa(n)) THEN
     CALL SPLINE1(xa, ya, n, y2a)
     CALL SPLINT1(xa, ya, y2a, n, x, y, np)
  ELSE
     xacpy = -xa; xcpy = -x
     CALL SPLINE1(xacpy, ya, n, y2a)
     CALL SPLINT1(xacpy, ya, y2a, n, xcpy, y, np)
  ENDIF

  RETURN
END SUBROUTINE BSPLINE

! modified to always use "natural" boundary conditions
SUBROUTINE SPLINE1 (x, y, n, y2)
  
  IMPLICIT NONE
  INTEGER, PARAMETER  :: dp = KIND(1.0D0)
  INTEGER, INTENT(IN) :: n
  REAL (KIND=dp), DIMENSION(n), INTENT(IN) :: x, y  
  REAL (KIND=dp), DIMENSION(n), INTENT(OUT) :: y2
  
  REAL (KIND=dp), DIMENSION(n) :: u
  INTEGER       :: i, k
  REAL(KIND=dp) :: sig, p, qn, un
  
  y2 (1) = 0.0
  u (1) = 0.0
  
  DO i = 2, n - 1
     sig = (x (i) - x (i - 1)) / (x (i + 1) -x (i - 1))
     p = sig * y2 (i - 1) + 2.D0
     y2 (i) = (sig - 1.) / p
     u (i) = (6._dp * ((y (i + 1) - y (i)) / (x (i + 1) - x (i)) -  & 
          (y (i) - y (i - 1)) / (x (i) - x (i - 1))) / (x (i + 1) - &
          x (i - 1)) - sig * u (i - 1)) / p
  ENDDO
  
  qn = 0.0
  un = 0.0
  y2 (n) = (un - qn * u (n - 1)) / (qn * y2 (n - 1) + 1.0)
  DO k = n - 1, 1, -1
     y2 (k) = y2 (k) * y2 (k + 1) + u (k)
  ENDDO
  
  RETURN
END SUBROUTINE SPLINE1

! This code could be optimized if x is in increasing/descreasing order
SUBROUTINE SPLINT1 (xa, ya, y2a, n, x, y, m)
  
  IMPLICIT NONE
  INTEGER, PARAMETER  :: dp = KIND(1.0D0)
  INTEGER, INTENT(IN) :: n, m
  REAL (KIND=dp), DIMENSION(n), INTENT(IN) :: xa, ya, y2a
  REAL (KIND=dp), DIMENSION(m), INTENT(IN) :: x
  REAL (KIND=dp), DIMENSION(m), INTENT(OUT):: y
  
  INTEGER        :: ii, klo, khi, k 
  REAL (KIND=dp) :: h, a, b

  !klo = 1; khi = n
  DO ii = 1, m 
     klo = 1; khi = n
    
     !IF ( khi - klo == 1) THEN
     !   IF (x(ii) > xa(khi) )   THEN
     !       khi = n
     !   ENDIF
     !ENDIF

     DO WHILE (khi - klo > 1)
        k = (khi + klo) / 2
        IF (xa (k) > x(ii)) THEN
           khi = k
        ELSE
           klo = k
        ENDIF
     ENDDO
     
     h = xa (khi) - xa (klo)
     IF (h == 0.0) STOP 'Bad xa input in: splint!!!'
     a = (xa (khi) - x(ii)) / h
     b = (x(ii) - xa (klo)) / h
     
     y(ii) = a * ya (klo) + b * ya (khi) + ((a**3 - a) * y2a (klo) + &
          (b**3 - b) * y2a (khi)) * (h**2) / 6.0
  ENDDO
  
  RETURN
END SUBROUTINE SPLINT1

!;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
! NAME
!    solar_angles
!
! PURPOSE
!   Calculate Elevation and Azimuth Angles of Sun at given location and time
!
! INPUT
!    yr  = 4-digit year
!    mon = month of year, 1-12
!    day = day of month
!    hr  = hour, 0-23
!    min = minute, 0-59
!    sec = second, [0.d0,60.d0)
!    tz  = local time zone (0.d0 = UTC, -8.d0 = PST)
!    lat = Latitude of location in degrees, positive North, negative South
!    lon = Longitude of location in degrees, positive East, negative West
!    
! OUTPUT
!    solar_angles is returned array of length 2.
!    solar_angles(1) is Solar Azimuth Angle in Degrees, 0-360.
!    solar_angles(2) is Solar Elevation Angle in Degrees, 0-90. Negative is Sun below Horizon.
!
! REFERENCES
!    NOAA Solar Calculator at http://www.esrl.noaa.gov/gmd/grad/solcalc/
!      (Decl, Eqtime, Az, and EL agree with values returned by above.)
!    General info about calculation details is at 
!      http://www.srrb.noaa.gov/highlights/sunrise/calcdetails.html
!         "The calculations in the NOAA Sunrise/Sunset and Solar Position Calculators
!          are based on equations from Astronomical Algorithms, by Jean Meeus."
!    Code used by NOAA Solar Calculator to get Declination and Equation of Time is at
!       http://www.srrb.noaa.gov/highlights/sunrise/program.txt
!
! HISTORY
!    written February 2010 by Joyce Wolf for Annmarie Eldering and Susan Kulawik (JPL)
!    translated into Fortran90 24 Feb 2012 by Vijay Natraj (JPL)
!;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;    
FUNCTION solar_angles(yr,mo,day,hr,min,sec,tz,lat,lon)

  USE GC_time_module, only: julday
  implicit none
  
  !  Inputs
  integer(kind=4), intent(in)  :: yr
  integer(kind=4), intent(in)  :: mo
  integer(kind=4), intent(in)  :: day
  integer(kind=4), intent(in)  :: hr
  integer(kind=4), intent(in)  :: min
  real(kind=8),    intent(in)  :: sec
  real(kind=8),    intent(in)  :: tz
  real(kind=8),    intent(in)  :: lat
  real(kind=8),    intent(in)  :: lon
  
  !  Outputs
  real(kind=8)                 :: solar_angles(2)
  
  !  Local variables
  real(kind=8)                 :: d2r, r2d
  real(kind=8)                 :: julianday
  real(kind=8)                 :: t, meanlon, mean_anom
  real(kind=8)                 :: mrad, sinm, sin2m, sin3m, c
  real(kind=8)                 :: truelon, true_anom
  real(kind=8)                 :: omega, app_lon, e, seconds, eps0
  real(kind=8)                 :: epsilon, sint, decl
  real(kind=8)                 :: m, lz, y, sin2lz, cos2lz, sin4lz
  real(kind=8)                 :: eqtime, utc, ha
  real(kind=8)                 :: declr, latr, har
  real(kind=8)                 :: elr, yaz, xaz, azr, az, el
  
  d2r = acos(-1.d0)/180.d0
  r2d = 180.d0/acos(-1.d0)
  
  ! t = Julian centuries past J2000.0
  ! Adjust if time is local  
  julianday = JULDAY(mo,day,yr,hr,min,sec)
  t = (julianday-2451545.d0)/36525.d0 - tz/24.d0
  
  ! Geometric Mean Longitude of Sun in degrees  
  meanlon = 280.46646d0 + t * (36000.76983d0 + 0.0003032d0 * t)
  do while (meanlon .GT. 360.d0)
     meanlon = meanlon-360.d0
  end do
  do while (meanlon .LT. 0.d0)
     meanlon = meanlon + 360.d0
  end do
  
  ! Geometric Mean Anomaly of Sun in degrees  
  mean_anom = 357.52911d0 + t * (35999.05029d0 - 0.0001537d0 * t)
  
  ! Sun Equation of Center  
  mrad = d2r*mean_anom
  sinm = sin(mrad)
  sin2m = sin(2.d0*mrad)
  sin3m = sin(3.d0*mrad)
  c = sinm * (1.914602d0 - t * (0.004817d0 + 0.000014d0 * t)) + &
       sin2m * (0.019993d0 - 0.000101d0 * t) + sin3m * 0.000289d0
  
  ! Sun's True Longitude in degrees  
  truelon = meanlon + c
  
  ! Sun's True Anomaly in degrees  
  true_anom = mean_anom + c
  
  ! Sun's Apparent Longitude in degrees  
  omega = 125.04d0 - 1934.136d0 * t
  app_lon = truelon - 0.00569d0 - 0.00478d0 * sin(d2r*omega)
  
  ! Eccentricity of Earth Orbit  
  e = 0.016708634d0 - t * (0.000042037d0 + 0.0000001267d0 * t)
  
  ! Mean Obliquity of Ecliptic in degrees  
  seconds = 21.448d0 - t*(46.8150d0 + t*(0.00059d0 - t*(0.001813d0)))
  eps0 = 23.d0 + (26.d0 + (seconds/60.d0)) / 60.d0
  
  ! Corrected Obliquity 
  epsilon = eps0 + 0.00256d0 * cos(d2r*omega)
  
  ! Sun's Declination in degrees  
  sint = sin(d2r*epsilon) * sin(d2r*app_lon)
  decl = r2d*asin(sint)
  
  ! Equation of Time in minutes  
  m = mean_anom
  lz = meanlon
  y = (tan(d2r*epsilon/2.d0))**2
  sin2lz = sin(2.d0 * d2r*lz)
  sinm   = sin(d2r*m)
  cos2lz = cos(2.d0 * d2r*lz)
  sin4lz = sin(4.d0 * d2r*lz)
  sin2m  = sin(2.d0 * d2r*m)
  eqtime = y * sin2lz - 2.d0 * e * sinm &
       + 4.d0 * e * y * sinm * cos2lz &
       - 0.5d0 * y * y * sin4lz - 1.25d0 * e * e * sin2m
  eqtime = r2d * eqtime * 4.d0
  
  ! Now use DECL and EQTIME to calculate Solar Elevation and Azimuth at (Lat,Lon) 
  ! for time specified by yr,mo,day,hr,min,sec
  
  ! UTC time in Decimal Hours  
  utc = real(hr,kind=8) + (real(min,kind=8)+sec/60.d0) / 60.d0 - tz
  
  ! Hour Angle in degrees, negative is before noon.  
  ha = 180.d0 - 15.d0*utc - lon - eqtime/4.d0
  
  ! Convert to Radians  
  declr = d2r*decl
  latr = d2r*lat
  har = d2r*ha
  
  ! Solar Elevation Angle  
  elr = asin(sin(latr)*sin(declr)+cos(latr)*cos(declr)*cos(har))
  
  ! Solar Azimuthal Angle
  ! atan2(y,x) = arctan(y,x) is in range (-pi,pi].  
  yaz = -sin(har)*cos(declr)
  xaz = cos(har)*cos(declr)*sin(latr) - sin(declr)*cos(latr)
  
  ! Above formulas are for az measured from due South
  ! So rotate by 180 to to get az measured from due North  
  azr = atan2(-yaz,-xaz)
  az = r2d* azr
  if (az .LT. 0.d0) az = az + 360.d0
  el = r2d*elr
  
  solar_angles(1) = az
  solar_angles(2) = el
  
  RETURN
end FUNCTION solar_angles

!;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
! NAME
!    view_angles
!
! PURPOSE
!    Calculate viewing angles of satellite at a given target location P, 
!    subsatellite point SSP, and satellite altitude h.
!
! INPUT
!    latp  = latitude of target in degrees (-90 to 90)
!    lonp  = longitude of target in degrees (-180 to 180)
!    latss = latitude of subsatellite point in degrees (-90 to 90)
!    lonss = longitude of subsatellite point in degrees (-180 to 180)
!    h     = altitude above Earth in km
!
! OUTPUT
!    view_angles is returned array of length 2.
!    view_angles(1) is Satellite Azimuth Angle in Degrees, 0-360, clockwise from North at Target.
!    view_angles(2) is Satellite Elevation Angle in Degrees, 0-90, measured at target between
!    satellite and local horizontal (Angle EPSILON in Reference).
!    Example in Reference: Given LATP,LONP = 22, -160; LATSSP,LONSSP = 10, -175; h=1000 km;
!    view_angles = [232.5, 14.42] NOTE: We need view angles for satellite as viewed from target, 
!    hence results are for this scenario and not that in the reference.
!    
! REFERENCE
!    Space Mission Analysis and Design, 
!        by James R. Wertz, Wiley J. Larson. 
!        Chapter 5, Space Mission Geometry, pp 112-114. 
!        (These pages are available online in
!         http://astrobooks.com/files/SMAD3Err3rd.pdf)
!    See also http://www.aoe.vt.edu/~cdhall/courses/aoe4140/missa.pdf
!    and http://en.wikipedia.org/wiki/Spherical_law_of_cosines
!
! HISTORY
!    written 1 April 2010 by Joyce Wolf for Annemarie Eldering and Susan Kulawik (JPL)
!    updated 30 April 2010 to add warning if target is outside 
!    region visible to spacecraft (spacecraft is below horizon at target).
!    translated into Fortran90 24 Feb 2012 by Vijay Natraj
!;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;    
FUNCTION view_angles(latp, lonp, latss, lonss, h)

  implicit none

  !  Inputs  
  real(kind=8),    intent(in)  :: latp
  real(kind=8),    intent(in)  :: lonp
  real(kind=8),    intent(in)  :: latss
  real(kind=8),    intent(in)  :: lonss
  real(kind=8),    intent(in)  :: h
  
  !  Outputs
  real(kind=8)                 :: view_angles(2)
  
  !  Local variables
  real(kind=8)                 :: d2r, re, srho
  real(kind=8)                 :: deltaL, cdeltaL, clatss, slatss, clatp, slatp
  real(kind=8)                 :: clambda, slambda, cphiv, phiv
  real(kind=8)                 :: taneta, eta, ceps, eps, r2d
  real(kind=8)                 :: lambda, lambda0
  
  d2r = acos(-1.d0)/180.d0
  
  ! Earth radius in km
  re = 6378.d0
  
  ! sin(angular radius of Earth at height h)  
  srho = re/(re+h)
  
  deltaL = abs(lonss-lonp)
  cdeltaL = cos(d2r*deltaL)
  clatss = cos(d2r*latss)
  slatss = sin(d2r*latss)
  clatp = cos(d2r*latp)
  slatp = sin(d2r*latp)
  
  ! Find lambda, central angle of great circle arc connecting P and SSP.
  ! use Law of Cosines for spherical triangle with vertices P, SSP, and North Pole.
  ! sides are central angles (great circle arcs) 90-LatP, 90-LatSS, and Lambda.
  ! Law of Cosines: cos(c) = cos(a) cos(b) +sin(a) sin(b) cos(C),
  ! where a, b, c are the sides and C is the corner angle opposite side c.
  
  ! cos(lambda)
  clambda = slatss*slatp + clatss*clatp*cdeltaL
  
  ! sin(lambda) 
  slambda = sin(acos(clambda))
  
  ! cos phiv (Phi_V is azimuth of satellite measured from North at target P).
  ! Use Law of Cosines on Spherical Triangle formed by P, North Pole, SSP.  
  cphiv = (slatss - slatp*clambda) / ( clatp*slambda)
  if (cphiv .GT. 1.d0) cphiv = 1.d0
  if (cphiv .LT. -1.d0) cphiv = -1.d0
  phiv = acos(cphiv)
  
  ! tan eta  
  taneta = srho*slambda / (1.d0-srho*clambda)
  eta = atan(taneta)
  
  ! cos epsilon
  ceps = sin(eta)/srho
  eps = acos(ceps)
  
  r2d = 180.d0/acos(-1.d0)
  view_angles(1) = r2d*phiv
  if (lonp-lonss .GT. 0.d0) view_angles(1) = 360.d0 - view_angles(1)
  view_angles(2) = r2d*eps
  
  ! Check for spacecraft below horizon at target
  lambda = r2d*acos(clambda)
  lambda0 = r2d*acos(srho)
  if (lambda .GT. lambda0) then
     write(0,*) 'WARNING: SPACECRAFT BELOW HORIZON AT TARGET'
     write(0,*) 'Lambda  (Central Angle between Target and SSP) = ', lambda
     write(0,*) 'Lambda0 (Central Angle Visible to Spacecraft)  = ', lambda0
     view_angles(2) = -view_angles(2)
  endif
  
  return
end function view_angles

!;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
! NAME
!    satellite_angles
!
! PURPOSE
!    For geostationary satellite GEOCAPE, at a given target location,
!    calculate spacecraft Azimuth Viewing Angle VAA
!    and spacecraft Zenith Viewing Angle VZA;
!    also given UTC time calculate
!    Solar Zenith Angle (SZA) and Solar Azimuth Angle(SAA).
!    (User may then calculate Relative Azimuth Angle RAA = VAA-SAA)
!
! INPUTS
!    Yr,Mo,Day,Hr,Min,Sec = UTC Time
!    tz    = local time zone (0.d0 = UTC, -8.d0 = PST)
!    Latp  = latitude of target in degrees (-90 to 90)
!    Lonp  = longitude of target in degrees (-180 to 180)
!    Latss = latitude of satellite in degrees (-90 to 90)
!    Lonss = longitude of satellite in degrees (-180 to 180)
!    hgtss = altitude of satellite in km
!
! OUTPUTS
!   satellite_angles    = returned array of length 4
!   satellite_angles(1) = Spacecraft Viewing Azimuth Angle in deg (0-360)
!   satellite_angles(2) = Spacecraft Viewing Zenith Angle in deg (0-90)
!   satellite_angles(3) = Solar Azimuth Angle in degrees (0-360)
!   satellite_angles(4) = Solar Zenith Angles in degrees (0-180, >90 is below horizon)
!
! REQUIRED
!   solar_angles.f90
!   view_angles.f90
!   julday.f90
!
! HISTORY
!   written 29 April 2010 by Joyce Wolf for Annmarie Eldering & S. Kulawik
!   translated into Fortran90 24 Feb 2012 by Vijay Natraj
!;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
FUNCTION satellite_angles(yr,mo,day,hr,min,sec,tz,latp,lonp,latss,lonss,hgtss)

  implicit none
  !  Inputs
  
  integer(kind=4), intent(in) :: yr
  integer(kind=4), intent(in) :: mo
  integer(kind=4), intent(in) :: day
  integer(kind=4), intent(in) :: hr
  integer(kind=4), intent(in) :: min
  real(kind=8),    intent(in) :: sec
  real(kind=8),    intent(in) :: tz
  real(kind=8),    intent(in) :: latp
  real(kind=8),    intent(in) :: lonp
  real(kind=8),    intent(in) :: latss
  real(kind=8),    intent(in) :: lonss
  real(kind=8),    intent(in) :: hgtss
  
  !  Outputs  
  real(kind=8)                :: satellite_angles(4)
  
  !  Local variables
  real(kind=8)                :: vangles(2), phiv, thetav
  real(kind=8)                :: azel(2), saa, sza
  
  vangles = view_angles(latp,lonp,latss,lonss,hgtss)
  phiv    = vangles(1)
  thetav  = 90.d0 - vangles(2)
  
  azel = solar_angles(yr,mo,day,hr,min,sec,tz,latp,lonp)
  saa = azel(1)
  sza = 90.d0 - azel(2)
  
  satellite_angles(1) = phiv
  satellite_angles(2) = thetav
  satellite_angles(3) = saa
  satellite_angles(4) = sza
  
  return
end function satellite_angles  
  
FUNCTION ibin (vtarget, array, nentries) RESULT(idx)
  
  ! binary search in an array of real numbers in increasing order.
  ! returned is the number of the last entry which is less than target, or
  ! 0 if not within array. (this was written to find values enclosing
  ! target for a linear interpolation scheme.) 4/9/84 john lavagnino;
  ! adapted from jon bentley, cacm february 1984, vol. 27, no. 2, p. 94.
  
  IMPLICIT NONE
  INTEGER, INTENT(IN)      :: nentries
  REAL(KIND=8), INTENT(IN) :: vtarget
  REAL(KIND=8), DIMENSION(nentries), INTENT(IN) :: array
  
  INTEGER :: upper, lower, middle
  INTEGER :: idx
  
  lower = 0
  upper = nentries + 1
  
  DO WHILE (lower + 1 /= upper)
     middle = (lower + upper) / 2
     IF (array(middle) < vtarget) THEN
        lower = middle
     ELSE
        upper = middle
     ENDIF
  ENDDO
  
  ! at this point, either array (lower) <= target <= array (upper), or
  ! lower = 0, or upper = nentries + 1 (initial values).
  IF (lower > 0 .and. upper /= nentries + 1) THEN
     idx = lower
  ELSE
     idx = 0
  ENDIF
  
END FUNCTION ibin

  SUBROUTINE reverse ( inarr, num )
    IMPLICIT NONE
    INTEGER, PARAMETER :: dp = KIND(1.0D0)
    
    INTEGER, INTENT(IN) :: num
    INTEGER             :: i
    REAL (KIND=dp), DIMENSION(1: num), INTENT(INOUT) :: inarr
    REAL (KIND=dp), DIMENSION(1: num)                :: temp
    
    DO i = 1, num
       temp(i) = inarr(num - i + 1)
    ENDDO
    inarr = temp
    
    RETURN
  END SUBROUTINE reverse
  
  SUBROUTINE reverse_idxs (num, idxs )
    IMPLICIT NONE
    
    INTEGER,                  INTENT(IN) :: num
    INTEGER, DIMENSION(num), INTENT(OUT) :: idxs
    INTEGER                              :: i
    
    DO i = 1, num
       idxs(i) = num - i + 1
    ENDDO
    
    RETURN
  END SUBROUTINE reverse_idxs

  ! =========================================================================
  !
  ! Convolves input spectrum with an asymmetric Gaussian slit function of
  ! specified HW1E (half-width at 1/e intensity)
  !
  ! The :symetric Gaussian g(x) is defined as
  !                   _                _
  !                  |         x^2      |
  !      g(x) =  EXP | - -------------- |
  !                  |_     hw1e ^2    _|
  !  FWHM = 2.0 * sqrt(ln(2.0)) * hw1e = 1.66551 * hw1e
  ! =========================================================================
  ! convolve high-resolution spectra to low resolution spectra
  ! fwave: high-resolution wavelength grid
  ! fspec: high-resolution reference spectra
  ! nf:    number of wavelengths at high-resolution
  ! nspec: number of spectra
  ! fwhm:  slit function in terms of FWHM (nm)
  ! cwave: low-resolution wavelength grid (could be the same as high-resolution grid)
  ! cspec: spectra at fwhm
  ! nc:    number of wavelengths for the low-resolution grid
  ! Updates on April 13, 2015
  ! Make gauss_f2c more generic to handle special cases: points outside the fine spectral grid
  SUBROUTINE gauss_f2c (fwave, fspec, nf, nspec, fwhm, cwave, cspec, nc)
    
    IMPLICIT NONE
    
    ! =======================
    ! Input/Output variables
    ! =======================
    INTEGER,                               INTENT (IN) :: nc, nf, nspec
    REAL (KIND=8),                         INTENT (IN) :: fwhm  
    REAL (KIND=8), DIMENSION (nf), INTENT (IN)         :: fwave
    REAL (KIND=8), DIMENSION (nf, nspec), INTENT (IN)  :: fspec
    REAL (KIND=8), DIMENSION (nc), INTENT (IN)         :: cwave
    REAL (KIND=8), DIMENSION (nc, nspec), INTENT (OUT) :: cspec
    
    ! ===============
    ! Local variables
    ! ===============
    INTEGER                       :: i, j, midx, sidx, eidx, nhalf
    REAL (KIND=8)                 :: hw1esq, dfw, ssum, hw1e
    REAL (KIND=8), PARAMETER      :: slit_trunc = 2.65 ! Truncate slit values < 1/1000th of maximum
    REAL (KIND=8), DIMENSION (nf) :: slit
    
    if (fwhm == 0.0) then
       return
    endif
    
    dfw  = MIN(fwave(2) - fwave(1), fwave(nf) - fwave(nf-1))
    hw1e = fwhm / 1.66551; hw1esq = hw1e ** 2
    nhalf  = INT(hw1e / ABS(dfw) * slit_trunc,KIND=4)
    
    DO i = 1, nc
       ! Find the closest pixel
       if (dfw > 0) then
          midx = MINVAL(MAXLOC(fwave, MASK=(fwave <= cwave(i))))
       else
          midx = MINVAL(MINLOC(fwave, MASK=(fwave <= cwave(i))))
       endif
       IF (midx < 0) midx = 1
       
       IF ( ABS(cwave(i)-fwave(midx)) < slit_trunc * hw1e ) THEN
          sidx = MAX(midx - nhalf, 1)
          eidx = MIN(nf, midx + nhalf)
          slit(sidx:eidx) = EXP(-(cwave(i) - fwave(sidx:eidx))**2 / hw1esq )
          ssum = SUM(slit(sidx:eidx))
          DO j = 1, nspec
             cspec(i, j) = SUM(fspec(sidx:eidx, j) * slit(sidx:eidx)) / ssum
          ENDDO
       ELSE  ! cwave(i) is too far away from find spectral points
          cspec(i, 1:nspec) = 0.d0
       ENDIF
    ENDDO
  
    RETURN
    
  END SUBROUTINE gauss_f2c
  
  ! Same as above, except for correcting solar I0 effect using high resolution
  ! solar reference spectrum following the Beer's Law
  ! i0: high resolution solar reference
  ! scalex: scaling, normally number of molecules
  ! April 13, 2015: does not allow division by zero (i.e., when ci0 = 0.0)
  SUBROUTINE gauss_f2ci0(fwave, fspec, i0, nf, nspec, scalex, fwhm, cwave, cspec, nc)
    
    IMPLICIT NONE
    
    ! =======================
    ! Input/Output variables
    ! =======================
    INTEGER,                               INTENT (IN) :: nc, nf, nspec
    REAL (KIND=8),                         INTENT (IN) :: fwhm, scalex  
    REAL (KIND=8), DIMENSION (nf), INTENT (IN)         :: fwave, i0
    REAL (KIND=8), DIMENSION (nf, nspec), INTENT (IN)  :: fspec
    REAL (KIND=8), DIMENSION (nc), INTENT (IN)         :: cwave
    REAL (KIND=8), DIMENSION (nc, nspec), INTENT (OUT) :: cspec
    
    ! ===============
    ! Local variables
    ! ===============
    INTEGER                             :: i, ntemp
    REAL (KIND=8), DIMENSION(nc)        :: ci0
    REAL (KIND=8), DIMENSION(nc, nspec) :: cabspec
    REAL (KIND=8), DIMENSION(nf, nspec) :: abspec
    
    if (fwhm == 0.0) then
       return
    endif
    
    ntemp = 1
    CALL gauss_f2c (fwave, i0, nf, ntemp, fwhm, cwave, ci0, nc)
    
    ! Follow Beer's Law
    DO i = 1, nspec
       abspec(:, i) = i0 * EXP(-fspec(:, i) * scalex)
    ENDDO
    
    CALL gauss_f2c (fwave, abspec, nf, nspec, fwhm, cwave, cabspec, nc)
    
    DO i = 1, nspec
       WHERE (ci0 /= 0.d0)
          cspec(:, i) = - LOG(cabspec(:, i) / ci0) / scalex
       ENDWHERE
    ENDDO
    
    RETURN
    
  END SUBROUTINE gauss_f2ci0
  
END MODULE GC_utilities_module
