MODULE GC_surface_module

  USE GC_parameters_module, ONLY: maxlambdas, malbspc, scia_nwvl, scia_nsub,  &
                                  short_kern_name
  USE GC_variables_module,  ONLY: tmpwaves, surface_data_path, do_clouds,     &
                                  do_lambertian_cld, use_lambertian,          &
                                  nlambdas, use_fixedalbedo, fixed_albedo,    &
                                  use_wavelength, lambdas, use_albspectra,    &
                                  albspectra_fname, database_dir, cfrac,      &
                                  lambertian_cldalb, messages, nmessages,     &
                                  latitude, longitude, month, ground_ler,     &
                                  wavlens_um, water_rn, water_cn,             &
                                  use_albeofs, day_of_year, clon, clat,       &
                                  GC_sun_positions, GC_azimuths,              &
                                  GC_view_angles,GC_sun_azimuths,             &
                                  GC_n_sun_positions, GC_n_view_angles,       &
                                  GC_n_azimuths,                              &
                                  temis_uv_alb, temis_uv_wav,                 &
                                  alb_eof_coeff, alb_eof_lfrac,               &
                                  alb_eof_iflg, temis_sf,                     &
                                  VBRDF_Sup_In, VBRDF_LinSup_In,              &
                                  VBRDF_Sup_InputStatus,use_cmglint,          &
                                  vlidort_vbrdf_control_file,                 &
                                  do_fixed_brdf, do_eof_brdf, fkern_name,     &
                                  fkern_idx, fkern_amp, fkern_npar,           &
                                  fkern_par,BRDF_Sup_ModOut, lambdas,         &
                                  salinity, wind_speed, do_ocean_glint,       &
                                  wind_dir, do_debug_geocape_tool,            &
                                  VLIDORT_FixIn,prof_seaicefrac,prof_snowfrac,&
                                  out_snowspec_f,out_snowspec_c, do_sif,      &
                                  solar_cspec_data, use_solar_photons,        &
                                  sif_spc, use_ler_climatology,               &
                                  modis_fa_alb_type, do_diag,                 &
                                  do_cm_facet_iso, do_cm_whitecap,            &
                                  do_cm_shadow, do_surfalb_jacobian
                                  
  USE GC_error_module
  IMPLICIT NONE
 
  ! For the EOF wavelength range 
  INTEGER :: n_below, n_above, n_within, nb_ext

  CONTAINS
  
  SUBROUTINE initialize_vbrdf_inputs(  )
    
    ! This routine intializes VBRDF 
    ! Some of these values are overwritten later as needed
    
    USE GC_variables_module, ONLY : SVL_full_stokes_calc,SVL_n_stokes,SVL_do_debug,     &
                                    SVL_do_thermal_emiss,SVL_do_surf_emiss,SVL_nstreams,&
                                    SVL_nmoments,VBRDF_Sup_In,VBRDF_LinSup_In,          &
                                    VBRDF_Sup_InputStatus, first_xy, Diag28_InpOpt
    
    
    ! Control Inputs
    VBRDF_Sup_In%BS_DO_USER_STREAMS     = .TRUE.
    VBRDF_Sup_In%BS_DO_BRDF_SURFACE     = .TRUE.
    VBRDF_Sup_In%BS_DO_SURFACE_EMISSION = .FALSE.
    VBRDF_Sup_In%BS_DO_SOLAR_SOURCES    = .TRUE.
    VBRDF_Sup_In%BS_DO_USER_OBSGEOMS    = .FALSE.
    
    ! Geometry Results
    VBRDF_Sup_In%BS_NSTOKES              = SVL_n_stokes
    VBRDF_Sup_In%BS_NSTREAMS             = SVL_nstreams
    VBRDF_Sup_In%BS_NBEAMS               = 1
    VBRDF_Sup_In%BS_BEAM_SZAS(:)         = 0.0d0 ; VBRDF_Sup_In%BS_BEAM_SZAS(1) = 0.01
    VBRDF_Sup_In%BS_N_USER_RELAZMS       = 1
    VBRDF_Sup_In%BS_USER_RELAZMS(:)      = 0.0d0 ; VBRDF_Sup_In%BS_USER_RELAZMS(1) = 0.01
    VBRDF_Sup_In%BS_N_USER_STREAMS       = 1
    VBRDF_Sup_In%BS_USER_ANGLES_INPUT(:) = 0.0d0 ; VBRDF_Sup_In%BS_USER_ANGLES_INPUT(1) = 0.01
    VBRDF_Sup_In%BS_N_USER_OBSGEOMS      = 0
    VBRDF_Sup_In%BS_USER_OBSGEOMS(:,:)   = 0.0d0
    
    ! BRDF Inputs
    VBRDF_Sup_In%BS_N_BRDF_KERNELS              = 3
    VBRDF_Sup_In%BS_BRDF_NAMES(1:3)             = (/'Lambertian','Ross-thick','Li-sparse '/)
    VBRDF_Sup_In%BS_WHICH_BRDF(1:3)             = (/1,3,4/)
    VBRDF_Sup_In%BS_N_BRDF_PARAMETERS(1:3)      = (/0,0,2/)
    VBRDF_Sup_In%BS_BRDF_PARAMETERS(:,:)        = 0.0
    VBRDF_Sup_In%BS_LAMBERTIAN_KERNEL_FLAG(1:3) = (/.TRUE.,.FALSE.,.FALSE./)
    VBRDF_Sup_In%BS_BRDF_FACTORS(:)             = 1.0d-5
    VBRDF_Sup_In%BS_NSTREAMS_BRDF               = 100 ! Azimuth angles
    VBRDF_Sup_In%BS_DO_SHADOW_EFFECT            = .FALSE.
    VBRDF_Sup_In%BS_DO_DIRECTBOUNCE_ONLY        = .FALSE.
    
    VBRDF_Sup_In%BS_DO_GLITTER_MSRCORR           = .FALSE.
    VBRDF_Sup_In%BS_DO_GLITTER_MSRCORR_DBONLY    = .FALSE.
    VBRDF_Sup_In%BS_GLITTER_MSRCORR_ORDER        = 0
    VBRDF_Sup_In%BS_GLITTER_MSRCORR_NMUQUAD      = 0
    VBRDF_Sup_In%BS_GLITTER_MSRCORR_NPHIQUAD     = 0
    
    VBRDF_Sup_In%BS_DO_WSABSA_OUTPUT = .TRUE.
    VBRDF_Sup_In%BS_DO_WSA_SCALING = .FALSE.
    VBRDF_Sup_In%BS_DO_BSA_SCALING = .FALSE.
    VBRDF_Sup_In%BS_WSA_VALUE      = 0.0
    VBRDF_Sup_In%BS_BSA_VALUE      = 0.0
    
    
    ! NewCM options
    VBRDF_Sup_In%BS_DO_NewCMGLINT = .FALSE.
    VBRDF_Sup_In%BS_SALINITY      = 0.0d0
    VBRDF_Sup_In%BS_WAVELENGTH    = 0.0d0
    VBRDF_Sup_In%BS_WINDSPEED     = 0.0d0
    VBRDF_Sup_In%BS_WINDDIR       = 0.0d0
    VBRDF_Sup_In%BS_DO_GlintShadow   = .FALSE.
    VBRDF_Sup_In%BS_DO_FoamOption    = .FALSE.
    VBRDF_Sup_In%BS_DO_FacetIsotropy = .FALSE.
!     
!     ! Linearizations
    VBRDF_LinSup_In%BS_DO_KERNEL_FACTOR_WFS(:) = .FALSE.
    VBRDF_LinSup_In%BS_DO_KERNEL_PARAMS_WFS    = .FALSE.
    VBRDF_LinSup_In%BS_DO_KPARAMS_DERIVS(:)    = .FALSE.
    VBRDF_LinSup_In%BS_N_SURFACE_WFS           = 0
    VBRDF_LinSup_In%BS_N_KERNEL_FACTOR_WFS     = 0
    VBRDF_LinSup_In%BS_N_KERNEL_PARAMS_WFS     = 0
    VBRDF_LinSup_In%BS_DO_WSAVALUE_WF          = .FALSE.         ! New Version 2.7
    VBRDF_LinSup_In%BS_DO_BSAVALUE_WF          = .FALSE.        ! New Version 2.7
    VBRDF_LinSup_In%BS_DO_WINDSPEED_WF         = .FALSE.       ! New Version 2.7
    
    ! Exception handling
    VBRDF_Sup_InputStatus%BS_STATUS_INPUTREAD              = 0
    VBRDF_Sup_InputStatus%BS_NINPUTMESSAGES                = 0
    VBRDF_Sup_InputStatus%BS_INPUTMESSAGES(:)   = ' '
    VBRDF_Sup_InputStatus%BS_INPUTACTIONS(:)    = ' '
    VBRDF_Sup_InputStatus%BS_INPUTMESSAGES(0)              = 'Successful Read of VLIDORT Input file'
    VBRDF_Sup_InputStatus%BS_INPUTACTIONS                  = 'No Action required for this Task'
    
    ! For linearization diagnostics
    Diag28_InpOpt%njac = 0
    
  END SUBROUTINE initialize_vbrdf_inputs
  
  SUBROUTINE prepare_surface(error)
    
    
    USE VBRDF_LINSUP_MASTERS_M 
    USE VLIDORT_PARS
    USE GC_Vlidort_module,     ONLY : VBRDF_TO_VLIDORT
    USE GC_variables_module,   ONLY : BS_NMOMENTS_INPUT,     &
                                      DO_DEBUG_RESTORATION,  &
                                      VBRDF_LinSup_Out,      &
                                      VBRDF_Sup_Out,         &
                                      VBRDF_Sup_OutputStatus,&
                                      VLIDORT_LinFixIn,      &
                                      use_brdf_clim,         &
                                      Diag28_InpOpt, first_xy
    USE GC_SciaUSGS_FA_module, ONLY : init_SciaUSGS_FA,      &
                                      load_MODIS_pixel,      &
                                      compute_RTLS_albedo,   &
                                      set_SciaUSGS_vl_kernels
    
    IMPLICIT NONE

    LOGICAL, INTENT(INOUT) :: error
    
    ! Local variables
    INTEGER                                         :: i, j, nler_flag, ct, np
    CHARACTER(LEN=max_ch_len)                       :: numstr
    LOGICAL :: LAMBERTIAN_KERNEL_FLAG(3)

    ! ----------------
    ! Code starts here
    ! ----------------
    
    ! Initialize the BRDF inputs
    IF( first_xy ) CALL initialize_vbrdf_inputs()
    
    
    ! =================================================================
    ! LAMBERTIAN SURFACE REFLECTANCE CASES
    ! =================================================================
    
    IF (use_lambertian) THEN
    
      ! --------------------------------------
      ! (0) Set cloud surface 
      ! --------------------------------------
      IF (do_clouds .AND. do_lambertian_cld .AND. cfrac .GE. 1.0d0) THEN 
        
        ground_ler(1:nlambdas) = lambertian_cldalb
        
      ! --------------------------------------
      ! (1) Fixed Albedo
      ! --------------------------------------
      ELSE IF (use_fixedalbedo) THEN  ! Used fixed albedo from input file
        
        ground_ler(1:nlambdas) = fixed_albedo
      
      ! --------------------------------------
      ! (2) MODIS EOF Lambertian Reflectance
      ! --------------------------------------
      ELSE IF (use_albeofs) THEN
        
        IF( .NOT. use_wavelength ) STOP 'MODIS EOF LER Does not currently support wavenumber grids'
        
        ! Initial the FA spectra
        CALL  init_SciaUSGS_FA(longitude,latitude)
          
        ! Load MODIS observations within pixel
        CALL load_MODIS_pixel( day_of_year,prof_snowfrac,error )
        
        ! Compute albedo
        CALL compute_RTLS_albedo(GC_sun_positions(1),GC_azimuths(1), GC_view_angles(1),&
                                 modis_fa_alb_type,                      &
                                 ground_ler(1:nlambdas), nlambdas, error                )
        
      ELSE IF( use_ler_climatology ) THEN
        
        CALL get_ler_clim_alb(  )
        
       
      ELSE IF( use_albspectra ) THEN
      
       ! Direct input
       albspectra_fname = TRIM(ADJUSTL(albspectra_fname))
       
       CALL geocape_surface_setter                             &
               ( albspectra_fname, maxlambdas, nlambdas, lambdas(1:nlambdas), & ! inputs
                 ground_ler,  messages(nmessages+1), error )                    ! outputs
      ELSE
        
        
        STOP 'No Lambertian surface albedo option selected'

      ENDIF
       
       ! Check that multiple options are not set
       nler_flag = 0
       IF( use_fixedalbedo     ) nler_flag = nler_flag+1
       IF( use_albeofs         ) nler_flag = nler_flag+1
       IF( use_ler_climatology ) nler_flag = nler_flag+1
       
       IF( nler_flag > 1 ) THEN 
          print*, 'More than one lambertian surface option set: ', nler_flag
          STOP
       ENDIF
       
       ! Configure VBRDF options for lambertian surface
       VBRDF_Sup_In%BS_DO_BRDF_SURFACE = .FALSE.

       ! Check Linearization for the Linearized cases
       IF( do_diag(28) .AND. Diag28_InpOpt%do_factor_jac  ) THEN
         
         ! Only 1 weighting function
         Diag28_InpOpt%njac = 1
         
         ! Create Jacobian name
         ALLOCATE( Diag28_InpOpt%jac_name(1) )
         Diag28_InpOpt%jac_name(1) = 'BRDF_f_' // short_kern_name( 1 )
         
         ! Allocate array to store diagnostic LER
         ALLOCATE( Diag28_InpOpt%var(nlambdas,1) )
         Diag28_InpOpt%var(:,1) = ground_ler(:)

       ENDIF  
       
    ! =================================================================
    ! BRDF SURFACE REFLECTANCE CASES
    ! =================================================================
    ELSE
      
      ! Ignore the Viewing geometry in the BRDF file
      VBRDF_Sup_In%BS_NBEAMS               = GC_n_sun_positions
      VBRDF_Sup_In%BS_N_USER_STREAMS       = GC_n_view_angles
      VBRDF_Sup_In%BS_N_USER_RELAZMS       = GC_n_azimuths
      VBRDF_Sup_In%BS_BEAM_SZAS(1)         = GC_sun_positions(1)
      VBRDF_Sup_In%BS_USER_ANGLES_INPUT(1) = GC_view_angles(1)
      VBRDF_Sup_In%BS_USER_RELAZMS(1)      = GC_azimuths(1)
      
      ! ----------------------------------------
      ! (1) Fixed 3 Kernel BRDF
      ! ----------------------------------------
      IF( do_fixed_brdf ) THEN

        ! Max number of parameters 
        np = MAX_BRDF_PARAMETERS
        
        ! Set lambertian kernel flag
        lambertian_kernel_flag(:) = .FALSE.
        DO i=1,3
          IF( fkern_idx(i) .EQ. 1 ) lambertian_kernel_flag(i) = .TRUE.
        ENDDO
        
        ! Specify the kernels
        VBRDF_Sup_In%BS_N_BRDF_KERNELS              = 3
        VBRDF_Sup_In%BS_BRDF_NAMES(1:3)             = fkern_name(1:3)
        VBRDF_Sup_In%BS_WHICH_BRDF(1:3)             = fkern_idx(1:3)
        VBRDF_Sup_In%BS_N_BRDF_PARAMETERS(1:3)      = fkern_npar(1:3)
        VBRDF_Sup_In%BS_BRDF_PARAMETERS(1:3,1:np)   = fkern_par(1:3,1:np) ! (kernel,par)
        VBRDF_Sup_In%BS_LAMBERTIAN_KERNEL_FLAG(1:3) = lambertian_kernel_flag(1:3)
        VBRDF_Sup_In%BS_BRDF_FACTORS(1:3)           = fkern_amp(1:3)

        ! Do BRDF surface
        VBRDF_Sup_In%BS_DO_BRDF_SURFACE = .TRUE.
        
        ! Don't do MODIS
        VBRDF_Sup_In%BS_MODISPCA_choice = .FALSE.
        
        ! For the IsoRossThickLiSparse kernel calculation
        BRDF_Sup_ModOut%BS_DO_MODISPCA_KERNELS = .FALSE.
        
        IF( do_diag(28) .AND. first_xy ) THEN
          
          ! Enable linearizations if necessary
          VBRDF_LinSup_In%BS_N_SURFACE_WFS       = 0
          VBRDF_LinSup_In%BS_N_KERNEL_FACTOR_WFS = 0 
          VBRDF_LinSup_In%BS_N_KERNEL_PARAMS_WFS = 0
          
          ! Count Jacobians
          ! ---------------

          ! Initialize 
          Diag28_InpOpt%njac = 0

          ! If flag for surface albedo set then add the 3 kernels
          IF( Diag28_InpOpt%do_factor_jac ) THEN

            ! Update Number of kernels
            VBRDF_LinSup_In%BS_N_KERNEL_FACTOR_WFS = 3

            Diag28_InpOpt%njac = Diag28_InpOpt%njac +                 &
                                 VBRDF_LinSup_In%BS_N_KERNEL_FACTOR_WFS

            ! Turn kernel factor linearization on
            VBRDF_LinSup_In%BS_DO_KERNEL_FACTOR_WFS(1:3) = .TRUE.

          ENDIF

          ! Add the number of parameter linearizations
          IF( Diag28_InpOpt%do_par_jac ) THEN

            VBRDF_LinSup_In%BS_N_KERNEL_PARAMS_WFS = SUM(fkern_npar(1:3))
            Diag28_InpOpt%njac = Diag28_InpOpt%njac +                 &
                                 VBRDF_LinSup_In%BS_N_KERNEL_PARAMS_WFS


          ENDIF

          ! Total number of weighting functions
          VBRDF_LinSup_In%BS_N_SURFACE_WFS = Diag28_InpOpt%njac

          ! Allocate Output name array
          IF( Diag28_InpOpt%njac .GT. 0 ) THEN
            ALLOCATE( Diag28_InpOpt%jac_name( Diag28_InpOpt%njac ) )
            ALLOCATE( Diag28_InpOpt%var(nlambdas,Diag28_InpOpt%njac ) )
          ENDIF


          ! Now loop over species to set jacobian names
          ct = 0
          DO i=1,3

            ! Add factor jacobian
            IF( Diag28_InpOpt%do_factor_jac ) THEN
              ct = ct + 1
              Diag28_InpOpt%jac_name(ct)   = 'BRDF_f_' // short_kern_name( fkern_idx(i) )
              Diag28_InpOpt%var(:,ct) = fkern_amp(i)
            ENDIF

            ! Add parameter jacobians
            IF( Diag28_InpOpt%do_par_jac ) THEN

              ! Set parameter jacobians 
              VBRDF_LinSup_In%BS_DO_KERNEL_PARAMS_WFS(i,1:fkern_npar(i)) = .TRUE.
              VBRDF_LinSup_In%BS_DO_KPARAMS_DERIVS(i) = .TRUE.

              DO j=1,fkern_npar(i)
                ct = ct + 1

                ! Write string
                WRITE(numstr,'(I500)') j

                ! Create output name
                Diag28_InpOpt%jac_name(ct) = 'BRDF_p' // TRIM(ADJUSTL(numstr)) // '_' &
                                           // short_kern_name( fkern_idx(i) )
                Diag28_InpOpt%var(:,ct) = fkern_par(i,j)

              ENDDO

            ENDIF

          ENDDO
          
        ENDIF

      ! ----------------------------------------
      ! (2) MODIS EOF BRDF
      ! ----------------------------------------
      ELSEIF ( do_eof_brdf ) THEN
        
        ! Initialize the FA model
        CALL  init_SciaUSGS_FA(longitude,latitude)
          
        ! Load MODIS observations within pixel
        CALL load_MODIS_pixel( day_of_year, prof_snowfrac, error )
        
        ! Set Kernels and linearizations
        CALL set_SciaUSGS_vl_kernels( error )
      
      ! -----------------------------------------
      ! (3) BRDF Climatology
      ! -----------------------------------------
      ELSEIF( use_brdf_clim ) THEN

        ! Initialize the climatology
        CALL compute_brdf_clim_kernels( longitude, latitude )
      
      ! -----------------------------------------
      ! (4) NewCM Glint Kernel
      ! -----------------------------------------
      ELSEIF( use_cmglint ) THEN

        ! Set Glint Options
        VBRDF_Sup_In%BS_N_BRDF_KERNELS            = 1
        VBRDF_Sup_In%BS_BRDF_NAMES(1)             = 'NewCMGlint'
        VBRDF_Sup_In%BS_WHICH_BRDF(1)             = NewCMGLINT_IDX
        VBRDF_Sup_In%BS_N_BRDF_PARAMETERS(1)      = 2
        VBRDF_Sup_In%BS_BRDF_PARAMETERS(1,1:2)    = (/wind_speed,salinity/)
        VBRDF_Sup_In%BS_LAMBERTIAN_KERNEL_FLAG(1) = .FALSE. 
        VBRDF_Sup_In%BS_BRDF_FACTORS(1)           = 1.0 ! <- Set later with fkern_amp_wvl
        VBRDF_Sup_In%BS_DO_NewCMGLINT    = .TRUE.

        ! Parameters specific to Cox-Munk param.
        VBRDF_Sup_In%BS_SALINITY         = salinity
        VBRDF_Sup_In%BS_WAVELENGTH       = 0.0d0 ! Set later
        VBRDF_Sup_In%BS_WINDSPEED        = wind_speed
        VBRDF_Sup_In%BS_WINDDIR(1)       = wind_dir - GC_sun_azimuths(1)
        IF( VBRDF_Sup_In%BS_WINDDIR(1) .LT. 0.0 ) THEN
          VBRDF_Sup_In%BS_WINDDIR(1) = VBRDF_Sup_In%BS_WINDDIR(1) + 360.0d0
        ENDIF
        VBRDF_Sup_In%BS_DO_GlintShadow    = do_cm_shadow
        VBRDF_Sup_In%BS_DO_FoamOption     = do_cm_whitecap
        VBRDF_Sup_In%BS_DO_FacetIsotropy  = do_cm_facet_iso

        ! Count Jacobians
        ! ---------------

        ! Initialize 
        Diag28_InpOpt%njac = 0

        ! If flag for surface albedo set then add the 3 kernels
        IF( Diag28_InpOpt%do_factor_jac ) THEN

          ! Update Number of kernels
          VBRDF_LinSup_In%BS_N_KERNEL_FACTOR_WFS = 1

          Diag28_InpOpt%njac = Diag28_InpOpt%njac +                 &
                               VBRDF_LinSup_In%BS_N_KERNEL_FACTOR_WFS

          ! Turn kernel factor linearization on
          VBRDF_LinSup_In%BS_DO_KERNEL_FACTOR_WFS(1:3) = .TRUE.

        ENDIF

        ! Add the number of parameter linearizations
        IF( Diag28_InpOpt%do_par_jac ) THEN

          VBRDF_LinSup_In%BS_N_KERNEL_PARAMS_WFS = 2
          Diag28_InpOpt%njac = Diag28_InpOpt%njac + 2

        ENDIF

        ! Total number of weighting functions
        VBRDF_LinSup_In%BS_N_SURFACE_WFS = Diag28_InpOpt%njac

        ! Allocate Output name array
        IF( Diag28_InpOpt%njac .GT. 0 ) THEN
          ALLOCATE( Diag28_InpOpt%jac_name( Diag28_InpOpt%njac ) )
          ALLOCATE( Diag28_InpOpt%var(nlambdas,Diag28_InpOpt%njac ) )
        ENDIF

        ! Set Jacobian Names
        ct = 0

        ! Add factor jacobian
        IF( Diag28_InpOpt%do_factor_jac ) THEN
          ct = ct + 1
          Diag28_InpOpt%jac_name(ct)   = 'BRDF_f_' // short_kern_name(NewCMGLINT_IDX)
          Diag28_InpOpt%var(:,ct) = 1.0d0
        ENDIF

        ! Add parameter jacobians
        IF( Diag28_InpOpt%do_par_jac ) THEN

          ! Set parameter jacobians 
          VBRDF_LinSup_In%BS_DO_KERNEL_PARAMS_WFS(1,1:2) = .TRUE.
          VBRDF_LinSup_In%BS_DO_KPARAMS_DERIVS(1) = .TRUE.
          DO j=1,2
            ct = ct + 1

            ! Write string
            WRITE(numstr,'(I500)') j

            ! Create output name
            Diag28_InpOpt%jac_name(ct) = 'BRDF_p' // TRIM(ADJUSTL(numstr)) // '_' &
                                      // short_kern_name( fkern_idx(i) )
            Diag28_InpOpt%var(:,ct) = fkern_par(i,j)

          ENDDO

        ENDIF

      ENDIF
    ENDIF
    
    ! Solar induced plant fluorescence
    IF( do_sif ) THEN
      
      CALL init_sif( )
      
    ENDIF
    
    ! Do if using BRDF
    IF( VBRDF_Sup_In%BS_DO_BRDF_SURFACE ) THEN

      ! -----------------------------
      ! Do not want debug restoration
      ! -----------------------------
      DO_DEBUG_RESTORATION = .false.
      
      ! ---------------------------------
      ! A normal calculation will require
      ! ---------------------------------
      BS_NMOMENTS_INPUT = 2 * VBRDF_Sup_In%BS_NSTREAMS - 1

      ! If we are doing the fixed BRDF then we only need to set the 
      ! kernels once
      IF( do_fixed_brdf  ) THEN

        ! Call supplement to compute BRDF
        CALL VBRDF_LIN_MAINMASTER (  &
            DO_DEBUG_RESTORATION,    & ! Inputs
            BS_NMOMENTS_INPUT,       & ! Inputs
            VBRDF_Sup_In,            & ! Inputs
            VBRDF_LinSup_In,         & ! Inputs
            VBRDF_Sup_Out,           & ! Outputs
            VBRDF_LinSup_Out,        & ! Outputs
            VBRDF_Sup_OutputStatus )   ! Output Status
        
        !  Exception handling
        IF ( VBRDF_Sup_OutputStatus%BS_STATUS_OUTPUT .EQ. VLIDORT_SERIOUS ) THEN
          WRITE(*,*)'geocape_tool_v2p6: program failed, VBRDF calculation aborted'
          WRITE(*,*)'Here are the error messages from the VBRDF supplement : - '
          WRITE(*,*)' - Number of error messages = ',VBRDF_Sup_OutputStatus%BS_NOUTPUTMESSAGES
          DO i = 1, VBRDF_Sup_OutputStatus%BS_NOUTPUTMESSAGES
            WRITE(*,*) '  * ',adjustl(Trim(VBRDF_Sup_OutputStatus%BS_OUTPUTMESSAGES(I)))
          ENDDO
        ENDIF

        ! Transfer BRDF arrays to VLIDORT input
        CALL VBRDF_TO_VLIDORT( error )
        
      ENDIF
    ENDIF

    ! --------------------
    ! If there is an error
    ! --------------------
    IF (error) CALL write_err_message(.FALSE., messages(nmessages+1))
    
  END SUBROUTINE prepare_surface

  SUBROUTINE geocape_surface_setter                               &
       ( albspectra_fname, maxlambdas, nlambdas, lambdas,         & ! inputs
       ground_ler,  message, fail )                               ! outputs & Exception handling

    USE GC_utilities_module, ONLY: bspline
    implicit none
    
    !  inputs
    !  ------
    
    !  filename
    
    character*(*), intent(in) :: albspectra_fname
    
    !  Dimensioning
    
    integer, intent(in)       :: maxlambdas
    
    !  wavelengths/data/dumbo/cmiller/marci_aod/surface_clim/MGSTES_Hapke_clim.nc
    
    integer, intent(in)       :: nlambdas
    real(kind=8), intent(in)  :: lambdas ( maxlambdas )
    
    !  output
    !  ------
    
    real(kind=8), intent(out) :: ground_ler ( maxlambdas )
    
    logical,       intent(inout)  :: fail
    character*(*), intent(inout)  :: message
    
    !  local
    !  -----
    integer, parameter :: maxnalb = 3000
    integer :: nalb, fidx, lidx, i, ntmp, np, errstat
    real(kind=8), dimension(maxnalb) :: albspectra_lams, albspectra
    
    fail = .false.; errstat = 0
    message = ' '
    
    np = 1
    do while (albspectra_fname(np:np).ne. ' ')
       np = np + 1 
    enddo
    np = np - 1
    
    open(1, file=albspectra_fname(1:np), err =90, status='old')
    read(1, *, err=90) nalb
    do i = 1, nalb
       read(1, *, err=90) albspectra_lams(i), albspectra(i)
    enddo
    close(1)
    
    albspectra_lams(1:nalb) = albspectra_lams(1:nalb) * 1000.0
    albspectra(1:nalb) = albspectra(1:nalb) / 100.0
    
    fidx = minval(minloc(lambdas(1:nlambdas), mask=(lambdas(1:nlambdas) >= albspectra_lams(1))))
    if (fidx > 1) ground_ler(1:fidx-1) = albspectra(1)
    
    lidx = minval(maxloc(lambdas(1:nlambdas), mask=(lambdas(1:nlambdas) <= albspectra_lams(nalb))))
    if (lidx > 0 .and. lidx < nlambdas) ground_ler(lidx+1:nlambdas) = albspectra(nalb)
    
    if (fidx > 0 .and. lidx > 0 .and. fidx < lidx) then
       ntmp = lidx - fidx + 1
       call bspline(albspectra_lams(1:nalb), albspectra(1:nalb), nalb, &
            lambdas(fidx:lidx), ground_ler(fidx:lidx), ntmp, errstat)
    endif
    
    if (errstat < 0) go to 90
    return
    
    ! Finish
    
90  continue
    
    message = 'Error in surface albedo interpolation!!!'
    fail = .true.
    
    return
  end subroutine geocape_surface_setter
  
  SUBROUTINE init_sif

    USE GC_utilities_module, ONLY: bspline
    USE GC_parameters_module, ONLY: sifunit, max_ch_len
    USE GC_variables_module,  ONLY: sif_734nm ! mW/m2/Sr/nm
    
    REAL(KIND=8) :: sif_cf ! Sif conversion factor to solar units
    
    ! Local variables for normed sif spectrum
    REAL(KIND=8) :: sif_norm(726), sif_wvl(726)
    INTEGER      :: i, sif_below, sif_within, sif_above, errstat
    
    ! ===========================================================
    ! init_sif starts here
    ! ===========================================================
    
    ! Determine scale factor to apply to normed fluorescence spectrum
    ! ----------------------------------------------------------------
    
    ! Get solar flux @ 734 nm
    
    
    IF( use_solar_photons ) THEN ! Solar spectrum in ! photons/cm^2/nm/s
      
      ! mW/m2/Sr/nm -> photons/cm2/nm/s/Sr
      sif_cf = sif_734nm*1.0d-16 * 734.0d0 / ( 2.99792458d8*6.62607004d-34 )
      
      
    ELSE ! Solar spectrum in W/m2/cm-1
      
      ! mW/m2/Sr/nm -> W/m2/cm-1/Sr ( Need to test conversion)
      sif_cf =sif_734nm*1.0d-10*734.0d0**2
      
    ENDIF
    
    
    ! Load fluorescence spectrum (normed to 1 @ 734nm)
    ! ------------------------------------------------
    
    ! Read LUT of ratio of direct irradiance to total downwelling irradiance
    OPEN(UNIT=sifunit, file=trim(database_dir)//"/ReflSpectra/chlorophyll_fl_734nm_norm.dat", status='old')
    ! Load normed spectrum
    DO i=1,726
      READ(sifunit,*) sif_wvl(i), sif_norm(i)
    ENDDO
    
    CLOSE( UNIT=sifunit )
    
    ! Determine indices within range
    sif_below = 0
    sif_within= 0
    sif_above = 0
    DO i=1,nlambdas
      
      IF( lambdas(i) .LT. sif_wvl(1) ) THEN
        sif_below = sif_below + 1
      ELSEIF( lambdas(i) .GT. sif_wvl(726) ) THEN
        sif_above = sif_above + 1
      ELSE
        sif_within = sif_within + 1
      ENDIF
      
    ENDDO
    
    ! -------------------------------------------------
    ! Compute sif
    ! -------------------------------------------------
    
    sif_spc(:) = 0.0d0
    
    IF ( n_within .GT. 0 ) THEN
      
      CALL bspline (sif_wvl(1:726), sif_norm(1:726), 726, &
                   lambdas(sif_below+1:sif_below+1+sif_within), &
                   sif_spc(sif_below+1:sif_below+sif_within)  , &
                   sif_within,errstat                      )
      
      ! Scale result 
      sif_spc(sif_below+1:sif_below+sif_within) = &
            sif_spc(sif_below+1:sif_below+sif_within)*sif_cf/&
            solar_cspec_data(sif_below+1:sif_below+sif_within)
      
    ENDIF
    
  END SUBROUTINE init_sif
  
  SUBROUTINE get_ler_clim_alb( )

    USE GC_utilities_module, ONLY: bspline
    USE GC_parameters_module, ONLY : max_ch_len, scia_nwvl
    USE GC_variables_module,  ONLY : do_ler_const_wvl, &
                                     ler_clim_file,    &
                                     ler_clim_wvl
    
    INCLUDE 'netcdf.inc'
    
    ! ===============
    ! Local variables
    ! ===============
    
    INTEGER :: ncid, rcode, var_id, errstat
    INTEGER :: xid, yid, wid, w0, wf, w
    INTEGER :: ler_imx, ler_jmx, ler_wmx
    INTEGER :: nler_below, nler_above, nler_within
    INTEGER, DIMENSION(1) :: idx, idy
    character(len=31) :: xdimname, ydimname, wdimname
    
    REAL(KIND=4), ALLOCATABLE, DIMENSION(:) :: clim_wvl, clim_ler, &
                                               clim_lon, clim_lat
    REAL(KIND=8), ALLOCATABLE, DIMENSION(:) :: clim_wvl_r8, clim_ler_r8
    REAL(KIND=8), DIMENSION(1) :: fixed_clim_ler
    CHARACTER(LEN=max_ch_len) :: message
    LOGICAL :: fail
    
    CHARACTER (LEN=16), PARAMETER  :: modulename = 'get_ler_clim_alb'
    
    ! ===================================================
    ! get_ler_clim_alb starts here
    ! ===================================================
    
    fail = .false.
    
    ! Open file
    ncid = ncopn(trim(adjustl(ler_clim_file)), nf_Nowrite, rcode)
    if (rcode  .eq. -1 ) then
      print*,' error in get_ler_clim_alb: ncopn failed'
      STOP 
    endif
    
    ! Get the X dimension
    xid = ncdid(ncid, 'x', RCODE)
    if (rcode  .eq. -1 ) then
      message =  ' error in get_ler_clim_alb: ncdid failed(x)'
      fail = .true.; return
    endif 
  
    ! Read x dimension
    call ncdinq(ncid, xid, xdimname, ler_imx, rcode)
    if (rcode  .eq. -1 ) then
      message =  ' error in read_xy_nc_prof: ncdinq failed(x)'
      fail = .true.; return
    endif
    
    ! Get the y dimension
    yid = ncdid(ncid, 'y', RCODE)
    if (rcode  .eq. -1 ) then
      message =  ' error in get_ler_clim_alb: ncdid failed(y)'
      fail = .true.; return
    endif 
  
    ! Read y dimension
    call ncdinq(ncid, yid, ydimname, ler_jmx, rcode)
    if (rcode  .eq. -1 ) then
      message =  ' error in read_xy_nc_prof: ncdinq failed(y)'
      fail = .true.; return
    endif
    
    ! Get the wavelength dimension
    wid = ncdid(ncid, 'w', RCODE)
    if (rcode  .eq. -1 ) then
      message =  ' error in get_ler_clim_alb: ncdid failed(w)'
      fail = .true.; return
    endif 
  
    ! Read y dimension
    call ncdinq(ncid, wid, wdimname, ler_wmx, rcode)
    if (rcode  .eq. -1 ) then
      message =  ' error in read_xy_nc_prof: ncdinq failed(y)'
      fail = .true.; return
    endif
    
    ! Allocate arrays
    ALLOCATE( clim_wvl( ler_wmx ) )
    ALLOCATE( clim_ler( ler_wmx ) )
    ALLOCATE( clim_lon( ler_imx ) )
    ALLOCATE( clim_lat( ler_jmx ) )
    ALLOCATE( clim_wvl_r8( ler_wmx ) )
    ALLOCATE( clim_ler_r8( ler_wmx ) )

    ! ==================
    ! read the variables
    ! ==================
    
    ! (1) Wavelength grid
    var_id = ncvid(ncid, 'Wavelength', rcode)
    if (rcode  .eq. -1 ) then
      print*,' error in read_xy_nc_prof: ncvid failed(Wavelength)'
      STOP
    endif
    call ncvgt(ncid, var_id, (/1/), (/ler_wmx/), clim_wvl, rcode)
    if (rcode  .eq. -1 ) then
      print*,' error in read_xy_nc_prof: ncvgt failed(Wavelength)'
      STOP
    endif
    
    ! (2) Longitude midpoints
    var_id = ncvid(ncid, 'xmid', rcode)
    if (rcode  .eq. -1 ) then
      print*,' error in read_xy_nc_prof: ncvid failed(xmid)'
      STOP
    endif
    call ncvgt(ncid, var_id, (/1/), (/ler_imx/), clim_lon, rcode)
    if (rcode  .eq. -1 ) then
      print*,' error in read_xy_nc_prof: ncvgt failed(xmid)'
      STOP
    endif
    
    ! (3) Latitude midpoints
    var_id = ncvid(ncid, 'ymid', rcode)
    if (rcode  .eq. -1 ) then
      print*,' error in read_xy_nc_prof: ncvid failed(ymid)'
      STOP
    endif
    call ncvgt(ncid, var_id, (/1/), (/ler_jmx/), clim_lat, rcode)
    if (rcode  .eq. -1 ) then
      print*,' error in read_xy_nc_prof: ncvgt failed(ymid)'
      STOP
    endif
    
    ! Find indices
    idx = MINLOC( ABS(clim_lon-longitude) )
    idy = MINLOC( ABS(clim_lat-latitude ) )
    
    ! Load closest LER spectrum
    var_id = ncvid(ncid, 'LER', rcode)
    print*,var_id
    if (rcode  .eq. -1 ) then
      print*,' error in read_xy_nc_prof: ncvid failed(LER)'
      STOP
    endif
    call ncvgt(ncid, var_id, (/idx,idy,1,month/), (/1,1,ler_wmx,1/), clim_ler, rcode)
    if (rcode  .eq. -1 ) then
      print*,' error in read_xy_nc_prof: ncvgt failed(LER)'
      STOP
    endif
    
    ! Close the file
    call ncclos(ncid, rcode)
    if (rcode .eq. -1) then
      print*,' error in load_scia_ler_eof: ncclos'
      STOP
    endif
    
    ! ===============================================
    ! Interpolate spectrum to output grid
    ! ===============================================
    
    ! Convert LER to real 8
    clim_ler_r8 = REAL( clim_ler, KIND=8 )
    clim_wvl_r8 = REAL( clim_wvl, KIND=8 )
    
    IF( do_ler_const_wvl ) THEN
      
      ! 
      CALL BSPLINE(clim_wvl_r8, clim_ler_r8, ler_wmx, &
                   ler_clim_wvl(1), fixed_clim_ler(1), 1, errstat)
      IF (errstat < 0) THEN
        PRINT*, modulename, ': BSPLINE error, errstat = ', errstat
        STOP
      ENDIF
    
      ground_ler(1:nlambdas) = fixed_clim_ler(1)
      
    ELSE
      
      ! Find values within range
      nler_below  = 0
      nler_within = 0
      nler_above  = 0
      DO w=1,nlambdas
        IF( lambdas(w) .LT. clim_wvl_r8(1) ) THEN
          nler_below = nler_below + 1
        ELSEIF( lambdas(w) .GT. clim_wvl_r8(ler_wmx) ) THEN
          nler_above = nler_above + 1
        ELSE
          nler_within = nler_within + 1
        ENDIF
      ENDDO
      
      print*,nler_below,nler_above,nler_within
      
      IF( nler_below > 0 ) THEN
        ground_ler(1:nler_below) = clim_ler_r8(1)
      ENDIF
      
      IF( nler_within > 0 ) THEN
        w0 = nler_below+1
        wf = w0+nler_within-1
        CALL BSPLINE(clim_wvl_r8, clim_ler_r8, ler_wmx, &
                   lambdas(w0:wf),ground_ler(w0:wf), wf-w0+1, errstat)
        IF (errstat < 0) THEN
          PRINT*, modulename, ': BSPLINE error, errstat = ', errstat
          STOP
        ENDIF
      ENDIF
      
      IF( nler_above > 0 ) THEN
        w0 = nler_below+nler_within+1
        wf = w0+nler_above-1
        ground_ler(w0:wf) = clim_ler_r8(ler_wmx)
      ENDIF
      
    ENDIF
    
    ! Free memory
    DEALLOCATE( clim_wvl    )
    DEALLOCATE( clim_ler    )
    DEALLOCATE( clim_lon    )
    DEALLOCATE( clim_lat    )
    DEALLOCATE( clim_wvl_r8 )
    DEALLOCATE( clim_ler_r8 )
    
    
  END SUBROUTINE get_ler_clim_alb
  
  SUBROUTINE compute_brdf_clim_kernels( lon, lat )
    
    USE GC_utilities_module, ONLY: bspline
    USE GC_variables_module, ONLY : fkern_amp_wvl, fkern_par_wvl, &
                                    fkern_idx, brdf_clim_infile, &
                                    Diag28_InpOpt, first_xy
    USE GC_netcdf_module,    ONLY : netcdf_handle_error
    USE GC_parameters_module,ONLY : vl_brdf_npar,short_kern_name,&
                                    BRDF_CHECK_NAMES
    USE VLIDORT_PARS
    INCLUDE 'netcdf.inc'
    
    ! --------------------
    ! Subroutine arguments
    ! --------------------

    REAL(KIND=8), INTENT(IN) :: lon, lat

    ! ---------------
    ! Local Variables
    ! ---------------
    INTEGER                   :: nk, ncid, rcode, vid, dimid, w,   &
                                 nw, nx, ny, np, nblw, nwth, nabv, &
                                 w0, wf, errstat, k, p, ct, kidx,  &
                                 i, j, npar
    INTEGER, DIMENSION(1)     :: idx, idy
    CHARACTER(LEN=max_ch_len) :: tmpchar
    REAL(KIND=8), ALLOCATABLE :: fkern_wvl(:), fkern_amp(:), &
                                 clim_lon(:), clim_lat(:), clim_wvl(:)
    REAL(KIND=4), ALLOCATABLE :: clim_amp_r4(:,:), clim_par_r4(:,:,:)
    REAL(KIND=8), ALLOCATABLE :: clim_amp(:,:), clim_par(:,:,:)
    
    CHARACTER(LEN=25), PARAMETER :: location = 'compute_brdf_clim_kernels'
    CHARACTER(LEN=max_ch_len)    :: numstr
    ! ===========================================================
    ! compute_brdf_clim_kernels starts here
    ! ===========================================================
    
    
    ! ----------------------------
    ! Open file and get dimensions
    ! ----------------------------

    ! Attach file
    rcode = nf_open( trim(adjustl(brdf_clim_infile)), nf_Nowrite, ncid )
    CALL netcdf_handle_error(location,rcode)

    ! Climatology wavelength dimension
    rcode =    nf_inq_varid( ncid, 'clim_wvl', vid  )
    rcode = nf_inq_vardimid( ncid,   vid,  dimid    )
    rcode =      nf_inq_dim( ncid, dimid,tmpchar,nw )

    ! Climatology Longitude Dimension
    rcode =    nf_inq_varid( ncid, 'clim_lon', vid  )
    rcode = nf_inq_vardimid( ncid,   vid,  dimid    )
    rcode =      nf_inq_dim( ncid, dimid,tmpchar,nx )

    ! Climatology latitude dimension
    rcode =    nf_inq_varid( ncid, 'clim_lat', vid  )
    rcode = nf_inq_vardimid( ncid,   vid,  dimid    )
    rcode =      nf_inq_dim( ncid, dimid,tmpchar,ny )

    ! Climatology number of kernels
    rcode =    nf_inq_varid( ncid, 'fkern_idx', vid )
    rcode = nf_inq_vardimid( ncid,   vid,  dimid    )
    rcode =      nf_inq_dim( ncid, dimid,tmpchar,nk )

    IF( nk > 3 ) THEN
      print*,'Error:fkern_idx dimension currently hardcoded to 3'
      print*,'This needs to be modified to accomodate more kernels'
      STOP
    ENDIF

    ! Set number of parameters = VLIDORT environment variable
    np = MAX_BRDF_PARAMETERS
    
    ! ------------------------------------
    ! Allocate geolocation arrays and red
    ! ------------------------------------

    ! Allocate arrays
    IF( .NOT. ALLOCATED(clim_wvl) ) ALLOCATE(clim_wvl(nw))
    IF( .NOT. ALLOCATED(clim_lon) ) ALLOCATE(clim_lon(nx))
    IF( .NOT. ALLOCATED(clim_lat) ) ALLOCATE(clim_lat(ny))
    
    ! Zero arrays
    clim_wvl = 0.0d0 ; clim_lon = 0.0d0 ; clim_lat = 0.0d0

    ! (1) Wavelength
    ! --------------
    
    ! get variable index
    rcode = nf_inq_varid( ncid, 'clim_wvl', vid )
    CALL netcdf_handle_error(location,rcode)
    
    ! Read array
    rcode = nf_get_var_double( ncid, vid, clim_wvl )
    CALL netcdf_handle_error(location,rcode)

    ! (2) Longitude
    ! --------------

    ! get variable index
    rcode = nf_inq_varid( ncid, 'clim_lon', vid )
    CALL netcdf_handle_error(location,rcode)
    
    ! Read array
    rcode = nf_get_var_double( ncid, vid, clim_lon )
    CALL netcdf_handle_error(location,rcode)

    ! (3) Latitude
    ! --------------

    ! get variable index
    rcode = nf_inq_varid( ncid, 'clim_lat', vid )
    CALL netcdf_handle_error(location,rcode)
    
    ! Read array
    rcode = nf_get_var_double( ncid, vid, clim_lat )
    CALL netcdf_handle_error(location,rcode)

    ! (4) Kernel indices
    ! ------------------
    
    ! Get variable index
    rcode = nf_inq_varid( ncid, 'fkern_idx', vid )
    CALL netcdf_handle_error(location,rcode)
    
    ! Read array
    rcode = nf_get_vara_int( ncid, vid, (/1/),(/nk/), &
                             fkern_idx(1:nk)          )
    CALL netcdf_handle_error(location,rcode)
    
    ! ----------------------------------
    ! Sample nearest index
    ! ----------------------------------

    ! Currently just nearest neighbour sample
    idx = MINLOC( ABS(clim_lon-lon) )
    idy = MINLOC( ABS(clim_lat-lat) )
    
    ! ------------------------------------
    ! Read the sampled kernels
    ! ------------------------------------
    
    ! Allocate climatology arrays
    ALLOCATE(clim_amp_r4(nw,nk)   ) ; clim_amp_r4(:,:)   = 0.0
    ALLOCATE(clim_par_r4(nw,nk,np)) ; clim_par_r4(:,:,:) = 0.0
    ALLOCATE(clim_amp(nw,nk)      ) ; clim_amp(:,:)      = 0.0d0
    ALLOCATE(clim_par(nw,nk,np)   ) ; clim_par(:,:,:)    = 0.0d0
    ! Get variable index for kernel amplitudes
    rcode = nf_inq_varid( ncid, 'fkern_amp', vid )
    CALL netcdf_handle_error(location,rcode)
    
    ! Read kernel amplitudes
    rcode = nf_get_vara_real( ncid, vid,             &
                             (/idx(1),idy(1), 1, 1/),&
                             (/      1,    1,nw,nk/),&
                              clim_amp_r4(1:nw,1:nk) )
    clim_amp(:,:) = REAL(clim_amp_r4,KIND=8)
    CALL netcdf_handle_error(location,rcode)
    
    ! Get variable index for kernel parameters
    rcode = nf_inq_varid( ncid, 'fkern_par', vid )
    CALL netcdf_handle_error(location,rcode)
    
    ! Read kernel amplitudes
    rcode = nf_get_vara_real( ncid, vid,                 &
                             (/idx(1),idy(1), 1, 1, 1/), &
                             (/      1,    1,nw,nk,np/), &
                              clim_par_r4(1:nw,1:nk,1:np))
    clim_par(:,:,:) = REAL(clim_par_r4,KIND=8)
    CALL netcdf_handle_error(location,rcode)
    
    ! Deallocate the temporary r4 arrays
    DEALLOCATE(clim_par_r4)
    DEALLOCATE(clim_amp_r4)
    
    ! Close netcdf file
    rcode = nf_close( ncid )
    CALL netcdf_handle_error(location,rcode)
    
    ! ----------------------------------
    ! Interpolate to output wavelengths
    ! ----------------------------------
    
    ! Allocate output arrays
    IF(.NOT. ALLOCATED(fkern_amp_wvl)) ALLOCATE(fkern_amp_wvl(nlambdas,nk)   )
    IF(.NOT. ALLOCATED(fkern_par_wvl)) ALLOCATE(fkern_par_wvl(nlambdas,nk,np))
    fkern_amp_wvl(:,:) = 0.0d0 ; fkern_par_wvl(:,:,:) = 0.0d0
    
    ! Find values within range
    nblw = 0 
    nwth = 0
    nabv = 0
    DO w=1,nlambdas
      IF( lambdas(w) .LT. clim_wvl(1) ) THEN
        nblw = nblw + 1
      ELSEIF( lambdas(w) .GT. clim_wvl(nw) ) THEN
        nabv = nabv + 1
      ELSE
        nwth = nwth + 1
      ENDIF
    ENDDO
    
    ! Fix values below range
    IF( nblw > 0 ) THEN
      DO k=1,nk
        fkern_amp_wvl(1:nblw,k) = clim_amp(1,k)
        DO p=1,np
          fkern_par_wvl(1:nblw,k,p) = clim_par(1,k,p)
        ENDDO
      ENDDO
    ENDIF
    
    ! Spline values within range
    
    IF( nwth > 0) THEN
      w0 = nblw+1
      wf = w0+nwth-1
      
      DO k=1,nk
        
        ! Interpolate climatology amplitudes
        CALL BSPLINE(clim_wvl, clim_amp(1:nw,k), nw, &
                     lambdas(w0:wf),fkern_amp_wvl(w0:wf,k), wf-w0+1, errstat)
        IF (errstat < 0) THEN
          PRINT*, 'compute_brdf_clim_kernels: BSPLINE error, errstat = ', errstat
          STOP
        ENDIF
        DO p=1,np
          CALL BSPLINE(clim_wvl, clim_par(1:nw,k,p), nw, &
                     lambdas(w0:wf),fkern_par_wvl(w0:wf,k,p), wf-w0+1, errstat)
          IF (errstat < 0) THEN
            PRINT*, 'compute_brdf_clim_kernels: BSPLINE error, errstat = ', errstat
            STOP
          ENDIF
        ENDDO
      ENDDO
      
    ENDIF
    
    IF( nabv > 0 ) THEN
      
      w0 = nblw+nwth+1
      wf = w0+nabv-1
      
      DO k=1,nk
        fkern_amp_wvl(w0:wf,k) = clim_amp(nw,k)
        DO p=1,np
          fkern_par_wvl(w0:wf,k,p) = clim_par(nw,k,p)
        ENDDO
      ENDDO
      
    ENDIF
    
    ! --------------------------------
    ! Set kernels for VBRDF supplement
    ! --------------------------------
    
    ! Generic Options
    VBRDF_Sup_In%BS_DO_BRDF_SURFACE        = .TRUE.
    VBRDF_Sup_In%BS_N_BRDF_KERNELS         = nk

    ! Its not MODIS
    VBRDF_Sup_In%BS_MODISPCA_choice        = .FALSE.
    BRDF_Sup_ModOut%BS_DO_MODISPCA_KERNELS = .FALSE.
    
    DO k=1,nk
      VBRDF_Sup_In%BS_BRDF_NAMES(k)             = BRDF_CHECK_NAMES(fkern_idx(k))
      VBRDF_Sup_In%BS_WHICH_BRDF(k)             = fkern_idx(k)
      VBRDF_Sup_In%BS_N_BRDF_PARAMETERS(k)      = vl_brdf_npar(fkern_idx(k))
      VBRDF_Sup_In%BS_BRDF_PARAMETERS(k,:)      = 0.0 ! <- Set later with fkern_par_wvl
      IF( fkern_idx(k) .EQ. 1 ) VBRDF_Sup_In%BS_LAMBERTIAN_KERNEL_FLAG(k) = .TRUE. 
      VBRDF_Sup_In%BS_BRDF_FACTORS(k)           = 0.0 ! <- Set later with fkern_amp_wvl
    ENDDO
    
    ! Check if we are doing the kernel jacobians
    IF( do_diag(28) ) THEN
      
      IF( first_xy ) THEN
        
        ! Count Jacobians
        Diag28_InpOpt%njac = 0

        ! Get number of weighting functions
        VBRDF_LinSup_In%BS_N_KERNEL_FACTOR_WFS = 0 
        VBRDF_LinSup_In%BS_N_KERNEL_PARAMS_WFS = 0
        IF( Diag28_InpOpt%do_factor_jac  ) VBRDF_LinSup_In%BS_N_KERNEL_FACTOR_WFS = nk
        IF( Diag28_InpOpt%do_par_jac     ) THEN
          DO k=1,nk
            VBRDF_LinSup_In%BS_N_KERNEL_PARAMS_WFS = VBRDF_LinSup_In%BS_N_KERNEL_PARAMS_WFS &
                                                  + VBRDF_Sup_In%BS_N_BRDF_PARAMETERS(k)
          ENDDO
        ENDIF
        VBRDF_LinSup_In%BS_N_SURFACE_WFS = VBRDF_LinSup_In%BS_N_KERNEL_FACTOR_WFS &
                                        + VBRDF_LinSup_In%BS_N_KERNEL_PARAMS_WFS
        Diag28_InpOpt%njac = VBRDF_LinSup_In%BS_N_SURFACE_WFS 
        
        ! Allocate arrays
        IF( Diag28_InpOpt%njac > 0 ) THEN
          ALLOCATE( Diag28_InpOpt%jac_name( Diag28_InpOpt%njac ) )
          ALLOCATE( Diag28_InpOpt%var(nlambdas,Diag28_InpOpt%njac ) )
        ENDIF
        
      ENDIF
      
      ! Now loop over species to set jacobian names
      ct = 0
      DO i=1,nk
        
        ! Get the index
        kidx   = VBRDF_Sup_In%BS_WHICH_BRDF(i)
        npar   = VBRDF_Sup_In%BS_N_BRDF_PARAMETERS(i)

        ! Add factor jacobian
        IF( Diag28_InpOpt%do_factor_jac ) THEN
          ct = ct + 1
          Diag28_InpOpt%jac_name(ct)   = 'BRDF_f_' // short_kern_name( kidx )
          Diag28_InpOpt%var(:,ct) = fkern_amp_wvl(:,i)

          ! Turn kernel factor linearization on
          VBRDF_LinSup_In%BS_DO_KERNEL_FACTOR_WFS(i) = .TRUE.

        ENDIF

        ! Add parameter jacobians
        IF( Diag28_InpOpt%do_par_jac ) THEN

          ! Set parameter jacobians 
          VBRDF_LinSup_In%BS_DO_KERNEL_PARAMS_WFS(i,1:npar) = .TRUE.
          VBRDF_LinSup_In%BS_DO_KPARAMS_DERIVS(i) = .TRUE.

          DO j=1,npar

            ct = ct + 1

            ! Write string
            WRITE(numstr,'(I500)') j

            ! Create output name
            Diag28_InpOpt%jac_name(ct) = 'BRDF_p' // TRIM(ADJUSTL(numstr)) // '_' &
                                       // short_kern_name( kidx )
            
            Diag28_InpOpt%var(1:nlambdas,ct) = fkern_par_wvl(1:nlambdas,i,j)
            
          ENDDO
        ENDIF
        
      ENDDO
      
    ENDIF
    
  END SUBROUTINE compute_brdf_clim_kernels
  
END MODULE GC_surface_module
