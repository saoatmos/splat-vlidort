!------------------------------------------------------------------------------
!          Harvard University Atmospheric Chemistry Modeling Group            !
!------------------------------------------------------------------------------
!BOP
!
! !MODULE: GC_diagnostics_module.f90
!
! !DESCRIPTION: This module contains routines for writing diagnostics
!\\
!\\
! !INTERFACE: 
!
MODULE GC_diagnostics_module

    USE GC_parameters_module
    USE GC_netcdf_module,        ONLY : create_ncvar, write_ncvar, &
                                        netcdf_handle_error
    USE GC_diag_amf_module,      ONLY : diag_01
    USE GC_diag_profile_module,  ONLY : do_profile_diag, do_profile_jacobians
    USE GC_diag_spectrum_module, ONLY : do_spectrum_diag
    USE GC_diag_surface_module,  ONLY : do_surface_diag, do_surface_jacobians
    USE GC_diag_cldaer_module,   ONLY : do_cldaer_jacobians
    IMPLICIT NONE
    INCLUDE 'netcdf.inc'
    
    CONTAINS
    
    SUBROUTINE create_nc_diagnostic_file( t0, ERROR )

      USE GC_variables_module, ONLY : idix, do_output_as_xy_nc, &
                                      ndir, create_xy_nc_only,&
                                      do_debug_geocape_tool, &
                                      nc_diag_outfile, first_xy, &
                                      didx

      ! --------------------
      ! Subroutine Arguments
      ! --------------------

      LOGICAL, INTENT(INOUT) :: ERROR
      REAL, INTENT(IN)       :: t0
      ! ---------------
      ! Local Variables
      ! ---------------
      INTEGER                   :: d, i
      REAL                      :: tf

      ! ======================================================
      ! create_xy_ncfile starts here
      ! ======================================================
      
      ! Create the output file
      DO i = 1, ndir
        
        ! Call routine to create output file
        d = idix(i)
        
        ! Some routines still use this global variable - fix later
        didx = d
        
        ! Get the file name
        CALL get_diag_outfilename( do_output_as_xy_nc, d, nc_diag_outfile )
        
        ! Write the file
        CALL diagnostic_outfile( nc_diag_outfile, 1, do_output_as_xy_nc, ERROR )
          
      ENDDO
      


      ! Check if we are only creating the output file
      IF( create_xy_nc_only ) THEN
        
        IF( do_debug_geocape_tool ) THEN
          print*,'STOP: Creating X-Y NetCDF Only'
        ENDIF
        
        CALL CPU_TIME(tf)
        print*,'===>CPU_TIME:',(tf-t0)
        
        ! We are done
        STOP
        
      ENDIF

    END SUBROUTINE create_nc_diagnostic_file

    SUBROUTINE write_nc_diagnostic_file( ERROR )
      
      USE GC_variables_module,  ONLY : idix, do_output_as_xy_nc,    &
                                       ndir, nc_diag_outfile,       &
                                       do_effcrs, lambda_resolution, &
                                       didx
                                       
      USE GC_convolution_module, ONLY: convolve_slit

      ! --------------------
      ! Subroutine Arguments
      ! --------------------

      LOGICAL, INTENT(INOUT) :: ERROR
      ! ---------------
      ! Local Variables
      ! ---------------
      INTEGER                   :: d, i
      REAL                      :: tf

      ! ======================================================
      ! write_xy_ncfile starts here
      ! ======================================================

      DO i = 1, ndir

        ! Get direction index
        d = idix(i)
        
        ! Some routines still use this global variable - fix later
        didx = d
        
        ! Get the file name
        CALL get_diag_outfilename( do_output_as_xy_nc, d, nc_diag_outfile )
        
        ! ----------------------------------------------
        ! Check if we need to convolve the output
        ! ----------------------------------------------
        IF (.NOT. do_effcrs .AND. lambda_resolution /= 0.0d0 ) THEN
          print*, 'Convolving effective cross sections'
          CALL convolve_slit( ERROR )
        ENDIF

        ! Write file
        CALL diagnostic_outfile( nc_diag_outfile, 2, do_output_as_xy_nc, ERROR  )

      ENDDO 
      
    END SUBROUTINE write_nc_diagnostic_file

    SUBROUTINE diagnostic_outfile( outfile, ACTION, DO_XY, ERROR  )
      
      ! ACTION = 1 -> Define variables/Create file
      !        = 2 -> Write Variables
      
      USE GC_variables_module, ONLY : do_diag, nc_ndimlist, nc_dimlist,&
                                      do_output_as_xy_nc,&
                                      do_xy_nc_file_lock,&
                                      overwrite_existing_xy_nc, first_xy
      USE GC_netcdf_module,    ONLY : define_ncdf_dim
      
      ! ---------------------------
      ! Subroutine arguments
      ! ---------------------------
      CHARACTER(LEN=*), INTENT(IN)  :: outfile
      INTEGER,          INTENT(IN)  :: ACTION
      LOGICAL,          INTENT(IN)  :: do_xy
      LOGICAL,          INTENT(OUT) :: ERROR
      
      ! ---------------------------
      ! Local variables
      ! ---------------------------
      LOGICAL :: DO_ACTION ! no_clobber prevents overwrite
      LOGICAL :: xync_exist, lock_exists
      LOGICAL :: use_filelock ! To prevent multiple processes writing to file
      INTEGER :: rcode, ncid, i, n, open_stat, ierr
      
      ! =====================================================
      ! diagnostic_outfile begins here
      ! =====================================================
      
      ! Initialize error
      ERROR = .FALSE.
      
      ! Do action ( Define file or write data to file )
      DO_ACTION = .TRUE.
      
      IF( ACTION == 1 ) THEN
        
        ! Check if we need to create a new file
        IF( DO_XY .AND. .NOT. overwrite_existing_xy_nc ) THEN
          INQUIRE( FILE=TRIM(ADJUSTL(outfile)), EXIST=xync_exist )
          IF( xync_exist ) DO_ACTION = .FALSE.
        ELSE
          
          ! IF we are overwriting an X-Y file check if its the first call
          IF( DO_XY .AND. .NOT. first_xy ) DO_ACTION = .FALSE.
          
        ENDIF
        
      ENDIF
      
      IF( DO_ACTION ) THEN
        
        ! Check if we need to create a file lock
        IF( do_xy .AND. do_xy_nc_file_lock ) THEN
          
          lock_exists = .TRUE.
          
          DO WHILE( lock_exists )
            
            ! Attempt to open lock file
            OPEN(STATUS='NEW',unit=lckunit,file=TRIM(ADJUSTL(outfile)) // '.lock' ,IOSTAT=open_stat)
            
            IF( open_stat == 0 ) THEN
              
              lock_exists = .FALSE.
              
            ELSE
              
              print*,'Waiting...'
              CALL SLEEP( 1 )
              
            ENDIF
            
          ENDDO
          
        ENDIF
        
        ! Create File
        IF( ACTION == 1 ) THEN
        
          ! Open file
          rcode = NF_CREATE(trim(ADJUSTL(outfile)), NF_NETCDF4, ncid)
          
          ! Check for error
          CALL netcdf_handle_error( 'create_nc_diag_outfile', rcode )
          
          ! Define dimensions
          DO i=1, nc_ndimlist
            CALL define_ncdf_dim( ncid, nc_dimlist(i), error )
            IF( error ) THEN
              STOP 'Error in defining NetCDF dimensions'
            ENDIF
          ENDDO
        
        ! Write to file
        ELSEIF( ACTION == 2 ) THEN
          
          ! Just open the file
          rcode = NF_OPEN(TRIM(ADJUSTL(outfile)), NF_WRITE, ncid)
          
        ELSE
          
          print*,'diagnostic_outfile: No task for ACTION =',ACTION
          STOP
          
        ENDIF
        
        ! AMF Diagnostic
        IF( do_diag( 1) ) CALL diag_01( ncid, ACTION, do_xy )
        
        ! Viewing Geometry
        IF( do_diag( 2) ) CALL diag_02( ncid, ACTION, do_xy )
        
        ! Footprint Information
        IF( do_diag( 3) ) CALL diag_03( ncid, ACTION, do_xy )
        
        ! Profile Diagnostics
        DO n=4,18
          IF( do_diag(n) ) CALL do_profile_diag( ncid, ACTION, do_xy, n )
        ENDDO
        
        ! Spectrum Diagnostics
        DO n=19,23
          if( do_diag(n) ) CALL do_spectrum_diag(ncid, ACTION, do_xy, n )
        ENDDO
        
        ! Surface Diagnostics
        DO n=24,27
          if( do_diag(n) ) CALL do_surface_diag(ncid, ACTION, do_xy, n )
        ENDDO
        
        ! Surface jacobians
        DO n=28,29
          if( do_diag(n) ) CALL do_surface_jacobians(ncid, ACTION, do_xy, n )
        ENDDO
        
        ! Profile jacobians
        DO n=30,33
          if( do_diag(n) ) CALL do_profile_jacobians(ncid, ACTION, do_xy, n )
        ENDDO

        DO n=34,39
          if( do_diag(n) ) CALL do_cldaer_jacobians(ncid, ACTION, do_xy, n )
        ENDDO
        
        ! Close file
        rcode = NF_CLOSE(ncid)
        CALL netcdf_handle_error( 'create_nc_diag_outfile', rcode )
        
        ! Remove file lock if needed
        IF( do_xy .AND. do_xy_nc_file_lock ) THEN
          close(lckunit,status='DELETE',iostat=ierr)
        ENDIF
        
      ENDIF
      
      
    END SUBROUTINE diagnostic_outfile
    
    SUBROUTINE archive_diagnostics( iw, error )
    
      USE GC_diag_amf_module,      ONLY : archive_amf_diag
      USE GC_diag_spectrum_module, ONLY : archive_spectrum_diag
      USE GC_diag_surface_module,  ONLY : archive_surface_diag
      USE GC_diag_profile_module,  ONLY : archive_profile_diag
      USE GC_diag_cldaer_module,   ONLY : archive_cldaer_diag
      USE GC_variables_module,     ONLY : ndir, idix, didx
      ! --------------------
      ! Subroutine Arguments
      ! --------------------
      INTEGER,       INTENT(IN)    :: iw
      LOGICAL,       INTENT(INOUT) :: error ! Error variable
      
      ! ---------------
      ! Local Variables
      ! ---------------
      INTEGER :: nd
      ! =====================================================
      ! create_nc_diag_outfile begins here
      ! =====================================================
      
      ! Initialize error
      error = .FALSE.
      
      DO nd=1,ndir
        
        ! Get direction index
        didx = idix(nd)

        ! AMF Diagnostics
        CALL archive_amf_diag( iw, didx )
        
        ! Update spectrum diagnostic
        CALL archive_spectrum_diag( iw, didx )
        
        ! Update profile diagnostic
        CALL archive_profile_diag( iw, didx )
        
        ! Surface diagnostics
        CALL archive_surface_diag( iw, didx )
        
        ! Aerosol/Cloud Jacobians
        CALL archive_cldaer_diag( iw, didx )
      
      ENDDO

    END SUBROUTINE archive_diagnostics
    
    SUBROUTINE get_diag_outfilename( do_xy, dir, outfile )
    
    USE GC_variables_module, ONLY : yn_ascii_profile, results_dir, nc_xid, nc_yid
    IMPLICIT NONE
    
    ! --------------------
    ! Subroutine arguments
    ! --------------------
    LOGICAL, INTENT(IN) :: do_xy
    INTEGER, INTENT(IN) :: dir
    CHARACTER(LEN=max_ch_len), INTENT(OUT) :: outfile
    
    ! ---------------
    ! Local variables
    ! ---------------
    character(len=100) :: ixstr, iystr
    
    !==============================================================================
    ! cget_diag_outfilename starts here
    !==============================================================================
    
    IF( yn_ascii_profile .OR. do_xy ) THEN
    
      IF (dir .EQ. 1) THEN
        outfile = TRIM(ADJUSTL(TRIM(ADJUSTL(results_dir)) // 'GC_upwelling_output.nc'))
      ELSEIF (dir .EQ. 2) THEN
        outfile = TRIM(ADJUSTL(TRIM(ADJUSTL(results_dir)) // 'GC_downwelling_output.nc'))
      ELSE
        WRITE(*,*) 'No upwelling or downwelling selected... no output'
        STOP
      END IF
      
    ELSE 
    
      WRITE(ixstr,'(I100)') nc_xid
      WRITE(iystr,'(I100)') nc_yid
      
      IF (dir .EQ. 1) THEN
        outfile = TRIM(ADJUSTL(TRIM(ADJUSTL(results_dir))  // &
                   TRIM(ADJUSTL(ixstr)) // '_' // & 
                   TRIM(ADJUSTL(iystr)) // '_' // 'GC_upwelling_output.nc'))
      ELSEIF (dir .EQ. 2) THEN
        outfile = TRIM(ADJUSTL(TRIM(ADJUSTL(results_dir))  // &
                   TRIM(ADJUSTL(ixstr)) // '_' // & 
                   TRIM(ADJUSTL(iystr)) // '_' // 'GC_downwelling_output.nc'))
      ELSE
        WRITE(*,*) 'No upwelling or downwelling selected... no output'
        STOP
      END IF
      
    ENDIF
    
  END SUBROUTINE get_diag_outfilename
    
    ! ======================================================
    ! (2) Viewing Geometry
    ! ======================================================
    SUBROUTINE diag_02( NCID, ACTION, DO_XY )
      
      USE GC_variables_module, ONLY : VLIDORT_ModIn,                 &
                                      GC_n_sun_positions,            &
                                      GC_n_view_angles, GC_n_azimuths
      
      ! ---------------------------
      ! Subroutine arguments
      ! ---------------------------
      INTEGER, INTENT(IN) :: NCID
      INTEGER, INTENT(IN) :: ACTION
      LOGICAL, INTENT(IN) :: DO_XY
      
      ! ---------------
      ! Local variables
      ! ---------------
      INTEGER :: VAR_ID
      LOGICAL :: error
      
      ! =====================================================
      ! diag_02 starts here
      ! =====================================================
      
      ! Intialize error
      error = .FALSE.
      
      ! Create NetCDF variables
      IF( ACTION == 1 ) THEN
        
        ! Solar Zenith angle
        CALL create_ncvar( NCID,   'Solarzenithangle',    (/ 'nsza' /), 1,    &
                           ncfloat, DO_XY,                VAR_ID,       ERROR )
        
        ! Viewing Zenith angle
        CALL create_ncvar( NCID,   'Viewingzenithangle',  (/ 'nvza' /), 1,    &
                           ncfloat, DO_XY,                VAR_ID,       ERROR )
        
        ! Relative Azimuth angle
        CALL create_ncvar( NCID,   'Relativeazimuthangle',(/ 'nvza' /), 1,    &
                           ncfloat, DO_XY,                VAR_ID,       ERROR )
      
      ! Write NetCDF variables
      ELSEIF( ACTION == 2 ) THEN
        
        ! Write Solar Zenith angle
        CALL write_ncvar( NCID, 'Solarzenithangle', (/1/), (/GC_n_sun_positions/), 1,      &
                          GC_n_sun_positions, ncfloat, DO_XY, ERROR,                       &
             DATA_r8=REAL(VLIDORT_ModIn%MSunrays%TS_SZANGLES(1:GC_n_sun_positions),KIND=8) )
        
        ! Write Viewing Zenith angle
        CALL write_ncvar( NCID, 'Viewingzenithangle', (/1/), (/GC_n_view_angles/), 1,     &
                          GC_n_view_angles, ncfloat, DO_XY, ERROR,                        &
             DATA_r8=REAL(VLIDORT_ModIn%MUserVal%TS_USER_VZANGLES_INPUT(1:GC_n_view_angles),KIND=8) )
        
        ! Write Viewing Zenith angle
        CALL write_ncvar( NCID, 'Relativeazimuthangle', (/1/), (/GC_n_azimuths/),  1,    &
                          GC_n_azimuths, ncfloat, DO_XY, ERROR,                          &
             DATA_r8=REAL(VLIDORT_ModIn%MUserVal%TS_USER_RELAZMS(1:GC_n_azimuths),KIND=8) )
        
      ELSE
        
        print*,'diag 02: No action for case ',ACTION
        STOP
        
      ENDIF
      
    END SUBROUTINE diag_02
    
    ! ======================================================
    ! (3) Footprint information
    ! ======================================================
    SUBROUTINE diag_03( NCID, ACTION, DO_XY )
      
      USE GC_variables_module, ONLY : longitude, latitude,wind_speed,       &
                                      month, year, day, utc,                &
                                      cfrac, lambertian_cldalb,             &
                                      VLIDORT_ModIn, aer_reflambda,         &
                                      cld_reflambda, lambda_resolution,     &
                                      lambda_dw, lambda_dfw, VLIDORT_FixIn, &
                                      GC_n_sun_positions, GC_n_view_angles, &
                                      GC_n_azimuths
      ! ---------------------------
      ! Subroutine arguments
      ! ---------------------------
      INTEGER, INTENT(IN) :: NCID
      INTEGER, INTENT(IN) :: ACTION
      LOGICAL, INTENT(IN) :: DO_XY
      
      ! ---------------
      ! Local variables
      ! ---------------
      INTEGER :: VAR_ID
      LOGICAL :: error
      
      INTEGER,      DIMENSION(1) :: vdata_i4
      REAL(KIND=8), DIMENSION(1) :: vdata_r8
      
      ! =====================================================
      ! diag_ starts here
      ! =====================================================
      
      ! Intialize error
      error = .FALSE.
      
      
      ! Create NetCDF variables
      IF( ACTION == 1 ) THEN
        
        CALL create_ncvar(      NCID,           'lon',  (/ 'one' /),      1, &
                            nf_float,           DO_XY,       VAR_ID,  ERROR  )
        CALL create_ncvar(      NCID,           'lat',  (/ 'one' /),      1, &
                            nf_float,           DO_XY,       VAR_ID,  ERROR  )
        CALL create_ncvar(      NCID,         'month',  (/ 'one' /),      1, &
                              nf_int,           DO_XY,       VAR_ID,  ERROR  )
        CALL create_ncvar(      NCID,          'year',  (/ 'one' /),      1, &
                              nf_int,           DO_XY,       VAR_ID,  ERROR  )
        CALL create_ncvar(      NCID,           'day',  (/ 'one' /),      1, &
                              nf_int,           DO_XY,       VAR_ID,  ERROR  )
        CALL create_ncvar(      NCID,           'utc',  (/ 'one' /),      1, &
                            nf_float,           DO_XY,       VAR_ID,  ERROR  )
        CALL create_ncvar(      NCID,         'cfrac',  (/ 'one' /),      1, &
                            nf_float,           DO_XY,       VAR_ID,  ERROR  )
        CALL create_ncvar(      NCID,        'cldalb',  (/ 'one' /),      1, &
                            nf_float,           DO_XY,       VAR_ID,  ERROR  )
        CALL create_ncvar(      NCID,       'userlvl',  (/ 'one' /),      1, &
                            nf_float,           DO_XY,       VAR_ID,  ERROR  )
        CALL create_ncvar(      NCID,     'windspeed',  (/ 'one' /),      1, &
                            nf_float,           DO_XY,       VAR_ID,  ERROR  )
        CALL create_ncvar(      NCID,      'lam_reso',  (/ 'one' /),      1, &
                            nf_float,           DO_XY,       VAR_ID,  ERROR  )
        CALL create_ncvar(      NCID,    'aer_reflam',  (/ 'one' /),      1, &
                            nf_float,           DO_XY,       VAR_ID,  ERROR  )
        CALL create_ncvar(      NCID,    'cld_reflam',  (/ 'one' /),      1, &
                            nf_float,           DO_XY,       VAR_ID,  ERROR  )
        CALL create_ncvar(      NCID,        'lam_dw',  (/ 'one' /),      1, &
                            nf_float,           DO_XY,       VAR_ID,  ERROR  )
        CALL create_ncvar(      NCID,       'lam_dfw',  (/ 'one' /),      1, &
                            nf_float,           DO_XY,       VAR_ID,  ERROR  )
        CALL create_ncvar(      NCID,      'nstreams',  (/ 'one' /),      1, &
                              nf_int,           DO_XY,       VAR_ID,  ERROR  )
        CALL create_ncvar(      NCID,  'aercld_nmoms',  (/ 'one' /),      1, &
                              nf_int,           DO_XY,       VAR_ID,  ERROR  )
        CALL create_ncvar(      NCID,          'nsza',  (/ 'one' /),      1, &
                              nf_int,           DO_XY,       VAR_ID,  ERROR  )
        CALL create_ncvar(      NCID,          'nvza',  (/ 'one' /),      1, &
                              nf_int,           DO_XY,       VAR_ID,  ERROR  )
        CALL create_ncvar(      NCID,          'naza',  (/ 'one' /),      1, &
                              nf_int,           DO_XY,       VAR_ID,  ERROR  )
        CALL create_ncvar(      NCID,   'ngeometries',  (/ 'one' /),      1, &
                              nf_int,           DO_XY,       VAR_ID,  ERROR  )
        CALL create_ncvar(      NCID, 'noutputlevels', (/ 'one' /),       1, &
                              nf_int,           DO_XY,      VAR_ID,   ERROR  )
        
        
      ELSEIF( ACTION == 2 ) THEN
        
        vdata_r8(:) = longitude
        CALL write_ncvar( NCID,    'lon', &
                         (/1/), (/1/), 1,1,nf_float, DO_XY, ERROR, DATA_r8=vdata_r8 )
        vdata_r8(:) = latitude
        CALL write_ncvar( NCID,    'lat', &
                         (/1/), (/1/), 1,1,nf_float, DO_XY, ERROR, DATA_r8=vdata_r8 )
        vdata_i4(:) = month
        CALL write_ncvar( NCID,    'month', &
                         (/1/), (/1/), 1,1,  nf_int, DO_XY, ERROR, DATA_i4=vdata_i4 )
        vdata_i4(:) = year
        CALL write_ncvar( NCID,    'year', &
                         (/1/), (/1/), 1,1,  nf_int, DO_XY, ERROR, DATA_i4=vdata_i4 )
        vdata_i4(:) = day
        CALL write_ncvar( NCID,    'day', &
                         (/1/), (/1/), 1,1,  nf_int, DO_XY, ERROR, DATA_i4=vdata_i4 )
        vdata_r8(:) = utc
        CALL write_ncvar( NCID,    'utc', &
                         (/1/), (/1/), 1,1,nf_float, DO_XY, ERROR, DATA_r8=vdata_r8 )
        vdata_r8(:) = cfrac
        CALL write_ncvar( NCID,    'cfrac', &
                         (/1/), (/1/), 1,1,nf_float, DO_XY, ERROR, DATA_r8=vdata_r8 )
        vdata_r8(:) = lambertian_cldalb
        CALL write_ncvar( NCID,    'cldalb', &
                         (/1/), (/1/), 1,1,nf_float, DO_XY, ERROR, DATA_r8=vdata_r8 )
        vdata_r8(:) = VLIDORT_ModIn%MUserVal%TS_USER_LEVELS(1)
        CALL write_ncvar( NCID,    'userlvl', &
                         (/1/), (/1/), 1,1,nf_float, DO_XY, ERROR, DATA_r8=vdata_r8 )
        vdata_r8(:) = wind_speed
        CALL write_ncvar( NCID,    'windspeed', &
                         (/1/), (/1/), 1,1,nf_float, DO_XY, ERROR, DATA_r8=vdata_r8 )
        vdata_r8(:) = lambda_resolution
        CALL write_ncvar( NCID,    'lam_reso', &
                         (/1/), (/1/), 1,1,nf_float, DO_XY, ERROR, DATA_r8=vdata_r8 )
        vdata_r8(:) = aer_reflambda
        CALL write_ncvar( NCID,    'aer_reflam', &
                         (/1/), (/1/), 1,1,nf_float, DO_XY, ERROR, DATA_r8=vdata_r8 )
        vdata_r8(:) = cld_reflambda
        CALL write_ncvar( NCID,    'cld_reflam', &
                         (/1/), (/1/), 1,1,nf_float, DO_XY, ERROR, DATA_r8=vdata_r8 )
        
        vdata_r8(:) = lambda_dw
        CALL write_ncvar( NCID,    'lam_dw', &
                         (/1/), (/1/), 1,1,nf_float, DO_XY, ERROR, DATA_r8=vdata_r8 )
        vdata_r8(:) = lambda_dfw
        CALL write_ncvar( NCID,    'lam_dfw', &
                         (/1/), (/1/), 1,1,nf_float, DO_XY, ERROR, DATA_r8=vdata_r8 )
        vdata_i4(:) = VLIDORT_FixIn%Cont%TS_NSTREAMS
        CALL write_ncvar( NCID,    'nstreams', &
                         (/1/), (/1/), 1,1,  nf_int, DO_XY, ERROR, DATA_i4=vdata_i4 )
        vdata_i4(:) = VLIDORT_ModIn%MCont%TS_NGREEK_MOMENTS_INPUT
        CALL write_ncvar( NCID,    'aercld_nmoms', &
                         (/1/), (/1/), 1,1,  nf_int, DO_XY, ERROR, DATA_i4=vdata_i4 )
        vdata_i4(:) = GC_n_sun_positions
        CALL write_ncvar( NCID,    'nsza', &
                         (/1/), (/1/), 1,1,  nf_int, DO_XY, ERROR, DATA_i4=vdata_i4 )
        vdata_i4(:) = GC_n_view_angles
        CALL write_ncvar( NCID,    'nvza', &
                         (/1/), (/1/), 1,1,  nf_int, DO_XY, ERROR, DATA_i4=vdata_i4 )
        vdata_i4(:) = GC_n_azimuths
        CALL write_ncvar( NCID,    'naza', &
                         (/1/), (/1/), 1,1,  nf_int, DO_XY, ERROR, DATA_i4=vdata_i4 )
        vdata_i4(:) = GC_n_sun_positions * GC_n_view_angles * GC_n_azimuths
        CALL write_ncvar( NCID,    'ngeometries', &
                         (/1/), (/1/), 1,1,  nf_int, DO_XY, ERROR, DATA_i4=vdata_i4 )
        vdata_i4(:) = VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS
        CALL write_ncvar( NCID,    'noutputlevels', &
                         (/1/), (/1/), 1,1,  nf_int, DO_XY, ERROR, DATA_i4=vdata_i4 )
        
      ELSE
        
        print*,'diag 03: No action for case ',ACTION
        STOP
        
      ENDIF
      
    END SUBROUTINE diag_03
    
END MODULE GC_diagnostics_module