!!  THE PROGRAM COMPUTES LIGHT SCATTERING BY POLYDISPERSE SPHERICAL    
!!  PARTICLES USING THE LORENZ-MIE THEORY     
                                                                     
!!  LAST MODIFIED 11/20/2000                                            
                                                                        
!!  THE PROGRAM IS BASED ON THE PAPERS                                  
!!                                                                      
!!    1. W. A. DE ROOIJ AND C. C. A. H. VAN DER STAP, EXPANSION OF MIE  
!!       SCATTERING MATRICES IN GENERALIZED SPHERICAL FUNCTIONS,        
!!       ASTRONOMY AND ASTROPHYSICS, VOL. 131, PP. 237-248, 1984        
!!                                                                      
!!    2. M. I. MISHCHENKO, J. M. DLUGACH, E. G. YANOVITSKIJ,            
!!       AND N. T. ZAKHAROVA, BIDIRECTIONAL REFLECTANCE OF FLAT,        
!!       OPTICALLY THICK PARTICULATE LATERS:  AN EFFICIENT RADIATIVE    
!!       TRANSFER SOLUTION AND APPLICATIONS TO SNOW AND SOIL SURFACES,  
!!       J. QUANT. SPECTROSC. RADIAT. TRANSFER, VOL. 63, 409-432 (1999).
!!
  !!  THE SECOND PAPER IS AVAILABLE UPON REUEST FROM MICHAEL MISHCHENKO
!!  (SEND A MESAGE TO CRMIM@GISS.NASA.GOV) AND IS ALSO AVAILABLE
!!  IN THE PDF AND PS FORMATS AT 
!!  http://www.giss.nasa.gov/~crmim/publications/index.html 
                                                                       
!!  NOTE THAT THE F11 ELEMENT OF THE SCATTERING MATRIX (I.E. THE PHASE  
!!  FUNCTION) IS NORMALIZED TO UNITY RATHER THAN TO THE SINGLE          
!!  SCATTERING ALBEDO [SEE REMARK AFTER EQUATION (32) IN REF. 1         
!!  AND EQUATION (7) IN REF. 2].                                        
!!  THE PAPER BY DE ROOIJ AND VAN DER STAP DESCRIBES NUMERICAL          
!!  ASPECTS OF MIE COMPUTATIONS.                                        
                                                                       
!!  THE CODE HAS BEEN DEVELOPED BY MICHAEL MISHCHENKO AT THE NASA       
!!  GODDARD INSTITUTE FOR SPACE STUDIES, NEW YORK.  THE DEVELOPMENT     
!!  OF THE CODE WAS SUPPORTED BY THE NASA RADIATION SCIENCE PROGRAM.    
                                                                       
!!  THE CODE CAN BE USED WITHOUT LIMITATIONS IN ANY NOT-FOR-            
!!  PROFIT SCIENTIFIC RESEARCH.  WE ONLY REQUEST THAT IN ANY            
!!  PUBLICATION USING THE CODE THE SOURCE OF THE CODE BE ACKNOWLEDGED   
!!  AND PROPER REFERENCES BE MADE.                                      
                                                                       
                                                                       
!!  INPUT PARAMETERS:                                                   
!!     NDISTR SPECIFIES THE TYPE OF PARTICLE SIZE DISTRIBUTION         
!!     NDISTR = 1 - MODIFIED GAMMA DISTRIBUTION                         
!!          [Eq. (20) of Ref. 2]                                        
!!              AA=alpha                                                
!!              BB=r_c                                                  
!!              GAM=gamma                                               
!!     NDISTR = 2 - LOG NORMAL DISTRIBUTION                             
!!          [Eq. (21) of Ref. 2]                                        
!!              AA=r_g                                                  
!!              BB=[ln(sigma_g)]**2                                      
!!     NDISTR = 3 - POWER LAW DISTRIBUTION                              
!!          [Eq. (22) of Ref. 2]                                        
!!               AA=r_eff (EFFECTIVE RADIUS)                            
!!               BB=v_eff (EFFECTIVE VARIANCE)                          
!!               PARAMETERS R1 AND R2 (SEE BELOW) ARE CALCULATED        
!!               AUTOMATICALLY FOR GIVEN AA AND BB                      
!!     NDISTR = 4 - GAMMA DISTRIBUTION                                  
!!          [Eq. (23) of Ref. 2]                                        
!!               AA=a                                                   
!!               BB=b                                                   
!!     NDISTR = 5 - MODIFIED POWER LAW DISTRIBUTION                     
!!          [Eq. (24) of Ref. 2]                                        
!!               BB=alpha                                               
!!     NDISTR = 6 - BIMODAL VOLUME LOG NORMAL DISTRIBUTION              
!!              [Eq. (11) of Mishchenko et al, Appl. Opt. 38,           
!!              7325-7341 (1999); available at                          
!!              http://www.giss.nasa.gov/~crmim/publications/index.html]
!!              AA1=r_g1                                                
!!              BB1=[ln(sigma_g1)]**2                                   
!!              AA2=r_g2                                                
!!              BB2=[ln(sigma_g2)]**2                                   
!!              GAM=GAMMA                                               
!!   - R1 AND R2 - MINIMUM AND MAXIMUM                                  
!!         RADII IN THE SIZE DISTRIBUTION FOR NDISTR=1-4 AND 6.         
!!         R1 AND R2 ARE CALCULATED AUTOMATICALLY                       
!!         FOR THE POWER LAW DISTRIBUTION WITH GIVEN r_eff AND v_eff    
!!         BUT MUST BE SPECIFIED FOR OTHER DISTRIBUTIONS.               
!!         FOR THE MODIFIED POWER LAW DISTRIBUTION (NDISTR=5), THE      
!!         MINIMUM RADIUS IS 0, R2 IS THE MAXIMUM RADIUS,               
!!         AND R1 IS THE INTERMEDIATE RADIUS AT WHICH THE               
!!         n(r)=const DEPENDENCE IS REPLACED BY THE POWER LAW           
!!         DEPENDENCE.                                                  
!!   - LAM - WAVELENGTH OF THE INCIDENT LIGHT IN THE SURROUNDING MEDIUM 
!!   - MRR AND MRI - REAL AND IMAGINARY PARTS OF THE RELATIVE REFRACTIVE
!!                   INDEX (MRI MUST BE NON-NEGATIVE)                   
!!   - IMPORTANT:  r_c, r_g, r_eff, a, LAM, R1, AND R2                  
!!                 MUST BE IN THE SAME UNITS OF LENGTH (E.G., MICRONS)  
!!   - N - NUMBER OF INTEGRATION SUBINTERVALS ON THE INTERVAL (R1,R2)   
!!         OF PARTICLE RADII                                            
!!   - NP - NUMBER OF INTEGRATION SUBINTERVALS ON THE INTERVAL (0,R1)   
!!         FOR THE MODIFIED POWER LAW DISTRIBUTION                      
!!   - NK - NUMBER OF GAUSSIAN DIVISION POINTS ON EACH OF THE           
!!          INTEGRATION SUBINTERVALS                                    
!!   - NPNA - NUMBER OF SCATTERING ANGLES AT WHICH THE SCATTERING       
!!        MATRIX IS COMPUTED                                            
!!        (SEE THE PARAMETER STATEMENT IN SUBROUTINE MATR).             
!!        THE CORRESPONDING SCATTERING ANGLES ARE GIVEN BY              
!!        180*(I-1)/(NPNA-1) (DEGREES), WHERE I NUMBERS THE ANGLES.     
!!        THIS WAY OF SELECTING SCATTERING ANGLES CAN BE EASILY CHANGED 
!!        AT THE BEGINNING OF SUBROUTINE MATR BY PROPERLY MODIFYING     
!!        THE FOLLOWING LINES:                                          
!!                                                                      
!!          N=NPNA                                                      
!!          DN=1D0/DFLOAT(N-1)                                          
!!          DA=DACOS(-1D0)*DN                                           
!!          DB=180D0*DN                                                 
!!          TB=-DB                                                      
!!          TAA=-DA                                                     
!!          DO 500 I1=1,N                                               
!!             TAA=TAA+DA                                               
!!             TB=TB+DB                                                 
!!                                                                      
!!        AND LEAVING THE REST OF THE SUBROUTINE INTACT.                
!!        THIS FLEXIBILITY IS PROVIDED BY THE FACT THAT                 
!!        AFTER THE EXPANSION COEFFICIENTS AL1,...,BET2 (SEE BELOW)     
!!        ARE COMPUTED BY SUBROUTINE SPHER, THE SCATTERING MATRIX       
!!        CAN BE COMPUTED FOR ANY SET OF SCATTERING ANGLES WITHOUT      
!!        REPEATING LORENZ-MIE CALCULATIONS.                            
!!   - DDELT - DESIRED NUMERICAL ACCURACY OF COMPUTING THE SCATTERING   
!!        MATRIX ELEMENTS	                                              
                                                                        
                                                                        
!!  OUTPUT INFORMATION:                                                 
!!                                                                      
!!   - REFF AND VEFF - EFFECTIVE RADIUS AND EFFECTIVE VARIANCE          
!!                     OF THE SIZE DISTRIBUTION                         
!!          [Hansen and Travis, Space Sci. Rev., vol. 16, 527-610,      
!!          1974, Eqs. (2.53) and (2.54)]                               
!!   - CEXT AND CSCA - AVERAGE EXTINCTION AND SCATTERING CROSS SECTIONS 
!c                     PER PARTICLE                                     
!!   - <COS> - ASYMMETRY PARAMETER OF THE PHASE FUNCTION                
!!   - ALBEDO - SINGLE-SCATTERING ALBEDO                                
!!   - <G> - AVERAGE PROJECTED AREA PER PARTICLE                        
!!   - <V> - AVERAGE VOLUME PER PARTICLE                                
!!   - Rvw - VOLUME WEIGHTED AVERAGE RADIUS                             
!!   - <R> - MEAN RADIUS                                                
!!   - F11, F33, F12, AND F34 - ELEMENTS OF THE SCATTERING MATRIX       
!!          [Eq. (39) of Ref. 1].                                       
!!          NOTE THAT FOR SPHERES, F22=F11, F44=F33, F21=F12,           
!!          AND F43=-F34.  ALL OTHER ELEMENTS ARE EQUAL TO ZERO.        
!!   - ALPHA1,...,BETA2 - COEFFICIENTS APPEARING IN THE EXPANSIONS      
!!          OF THE ELEMENTS OF THE SCATTERING MATRIX IN                 
!!          GENERALIZED SPHERICAL FUNCTIONS                             
!!          [EQS. (41)-(46) OF REF. 1;  EQ. (6) OF REF. 2].             
!!
!!   THE FIRST LINE OF FILE 10 PROVIDES THE SINGLE-SCATTERING
!!   ALBEDO AND THE NUMBER OF NUMERICALLY SIGNIFICANT 
!!   EXPANSION COEFFIEIENTS ALPHA1,...,BETA2, FOLLOWED
!!   BY THE VALUES OF THE EXPANSION COEFFICIENTS.
!!                                                                      
!!   OPTICAL EFFICIENCY FACTORS QEXT AND QSCA ARE COMPUTED AS           
!!   QEXT=CEXT/<G> AND QSCA=CSCA/<G>.  THE ABSORPTION CROSS SECTION     
!!   IS GIVEN BY CABS=CEXT-CSCA.  THE ABSORPTION EFFICIENCY FACTOR      
!!   IS EQUAL TO QABS=QEXT-QSCA.                                        
                                                                       
!!   TO CALCULATE SCATTERING AND ABSORPTION BY A MONODISPERSE 
!!   PARTICLE WITH RADIUS R = AA, USE THE FOLLOWING OPTIONS:          
!!                                                                      
!!        BB=1D-1                                                       
!!        NDISTR=4                                                      
!!        NK=1                                                          
!!        N=1                                                           
!!        R1=AA*0.9999999 D0                                            
!!        R2=AA*1.0000001 D0                                            
                                                                       
!!   ALTHOUGH THE MATHEMATICAL VALUE OF THE PARAMETER R2 FOR            
!!   MODIFIED GAMMA, LOG NORMAL, AND GAMMA DISTRIBUTIONS IS INFINITY,   
!!   IN PRACTICE A FINITE VALUE MUST BE CHOSEN.  TWO INTERPRETATIONS    
!!   OF A TRUNCATED SIZE DISTRIBUTIONS CAN BE GIVEN.  FIRST, R2 CAN     
!!   BE INCREASED UNTIL THE SCATTERING CROSS SECTIONS AND THE           
!!   SCATTERING MATRIX ELEMENTS CONVERGE WITHIN SOME NUMERICAL          
!!   ACCURACY.  IN THIS CASE THE CONVERGED TRUNCATED                    
!!   SIZE DISTRIBUTION IS NUMERICALLY EQUIVALENT TO THE DISTRIBUTION    
!!   WITH R2=INFINITY.  SECOND, A TRUNCATED DISTRIBUTION CAN BE         
!!   CONSIDERED A SPECIFIC DISTRIBUTION WITH OPTICAL CROSS SECTIONS     
!!   AND SCATTERING MATRIX ELEMENTS DIFFERENT FROM THOSE FOR THE        
!!   DISTRIBUTION WITH R2=INFINITY.  IN THIS LATTER CASE THE EFFECTIVE  
!!   RADIUS AND EFFECTIVE VARIANCE OF THE TRUNCATED DISTRIBUTION        
!!   WILL ALSO BE DIFFERENT FROM THOSE COMPUTED ANALYTICALLY FOR THE    
!!   RESPECTIVE DISTRIBUTION WITH R2=INFINITY.              
!!   SIMILAR CONSIDERATIONS APPLY TO PARAMETER R1 WHOSE THEORETICAL     
!!   VALUE FOR MODIFIED GAMMA, LOG NORMAL, AND GAMMA DISTRIBUTIONS      
!!   IS ZERO, BUT IN PRACTICE CAN BE ANY NUMBER SMALLER THAN R2.        
                                                                       
!!   IMPORTANT:  FOR GIVEN SIZE DISTRIBUTION PARAMETERS NDISTR, AA,     
!!   BB, GAM, R1, AND R2, THE                                           
!!   PARAMETERS N, NP, AND/OR NK SHOULD BE INCREASED UNTIL CONVERGENT   
!!   RESULTS ARE OBTAINED FOR THE EXTINCTION AND SCATTERING CROSS SECTIONS     
!!   AND, ESPECIALLY, THE ELEMENTS OF THE SCATTERING MATRIX.                 
 
!!   I WOULD HIGHLY APPRECIATE INFORMATION ON ANY PROBLEMS ENCOUNTERED
!!   WITH THIS CODE.  PLEASE SEND YOUR MESSAGE TO MICHAEL MISHCHENKO
!!   AT CRMIM@GISS.NASA.GOV.

!!   WHILE THIS COMPUTER PROGRAM HAS BEEN TESTED FOR A VARIETY OF CASES,
!!   IT IS NOT INCONCEIVABLE THAT IT CONTAINS UNDETECTED ERRORS. ALSO,
!!   INPUT PARAMETERS CAN BE USED WHICH ARE OUTSIDE THE ENVELOPE OF
!!   VALUES FOR WHICH RESULTS ARE COMPUTED ACCURATELY. FOR THIS REASON,
!!   THE AUTHOR AND HIS ORGANIZATION DISCLAIM ALL LIABILITY FOR
!!   ANY DAMAGES THAT MAY RESULT FROM THE USE OF THE PROGRAM. 

MODULE mie_code_module

IMPLICIT NONE

INTEGER(KIND=4), PARAMETER :: NMIE=10000, NPL=2*NMIE, &
     NDRDI=3*NMIE, NGRAD=100000
REAL(KIND=8) :: D6
REAL(KIND=8), DIMENSION(1:NMIE) :: COEFF1, COEFF2, COEFF3
REAL(KIND=8), DIMENSION(1:NPL) :: COEF1, COEF2, COEF3, &
     COEF4, COEF5, COEF6, COEF7, COEF8, P1, P2, P3, P4

PRIVATE
PUBLIC :: mie_interface

CONTAINS
SUBROUTINE mie_interface( waves, mmrs, mmis, r_min, r_max, & 
     alpha1, r_c1, gamma1, r_g2, &
     sigma_g2, r_eff3, v_eff3, a4, b4, alpha5, r_g16, sigma_g16, &
     r_g26, sigma_g26, gamma6, &
     nslam, ngk, nmom, NK, N, NP, NDISTR, &
     wls, r_eff, qext, ssa, phfcn )
  IMPLICIT NONE

  ! Variables to hold results and inputs
  ! Input variables
  
  
  
  REAL(KIND=8), INTENT(IN) :: alpha1, r_c1, gamma1, r_g2, &
       sigma_g2, r_eff3, v_eff3, a4, b4, alpha5, r_g16, sigma_g16, &
       r_g26, sigma_g26, gamma6
  REAL(KIND=8), INTENT(IN) :: r_min, r_max
  INTEGER(KIND=4), INTENT(IN) :: nslam, ngk, nmom, NK, N, NP, NDISTR
  
  
  REAL(KIND=8), DIMENSION(nslam) :: waves, mmrs, mmis
  
  ! Output variables
  REAL(KIND=8), INTENT(OUT) :: r_eff!, r_eff0
  REAL(KIND=8), DIMENSION(1:nslam), INTENT(OUT) :: wls, qext, ssa
  REAL(KIND=8), DIMENSION(1:nslam,1:nmom,1:ngk), INTENT(OUT) :: phfcn

  ! Local variables
  REAL(KIND=8), DIMENSION(NPL) :: AL1, AL2, AL3, AL4, BET1, BET2
  REAL(KIND=8) :: MRR, MRI, LAM,&
       VOL, RVW, RMEAN, reff, QE, q1, gam, ddelt, csca, cext, &
       bb2, bb1, bb, area, alb, aa2, aa1, aa, R1, R2
  INTEGER(KIND=4) :: j, LMAX, L1, islam, i
  
  ! Read refractive index (This file may be modified to remove reading
  ! parameters such as r_min, r_max, r_sig, r_wet, r_dry that are now
  ! read in the control file.
!   OPEN (9, file=TRIM(infile))
!   READ(9,*) r_min, r_max, r_sig, r_wet, r_dry
!   PRINT*,r_min, r_max, r_sig, r_wet, r_dry
!   READ(9, *)
!   DO i = 1, nslam
!      READ(9, *) waves(i), mmrs(i), mmis(i)
!   ENDDO
!   CLOSE(9)

  R1 = r_min ! Minimum particle size
  R2 = r_max ! Max particle size
  DDELT=1D-3

  ! Prepare inputs for each case
  SELECT CASE (NDISTR)
  CASE(1)
     AA = alpha1; BB = r_c1; GAM = gamma1
  CASE(2)
     AA = r_g2; BB = (LOG(sigma_g2))**2
  CASE(3)
     AA = r_eff3; BB = v_eff3
  CASE(4)
     AA = a4; BB = b4
  CASE(5)
     BB = alpha5
  CASE(6)
     AA1 = r_g16; BB1 = (LOG(sigma_g16))**2
     AA2 = r_g26; BB2 = (LOG(sigma_g26))**2
     GAM = gamma6
  END SELECT
  
  ! Zero Arrays
  phfcn(:,:,:) = 0.0d0 ; qext(:) = 0.0d0 ; ssa(:) = 0.0d0
  
  
  
!   ! Write output
!   OPEN (9, file=TRIM(outfile))
!   WRITE(9, *) 'w(um)  Q     r-eff ss-alb  pmom(0:1000)'
!   WRITE(9,2001) r_wet,r_sig
!   WRITE(9,*) nslam
!   
! 2001 FORMAT('Black carbon (lognormal, r=',F4.2,'um, sigma=', &
!           F4.2,' RI and size distribution from R. Martin)')
  
  !BB = (LOG(r_sig))**2.0
  !AA = r_wet
     
  DO islam = 1, nslam
     LAM = waves(islam)
     MRR = MMRS(islam)
     MRI = MMIS(islam)
        
     CALL SPHER (AA,BB,GAM,LAM,MRR,MRI,R1,R2,N,NP,NDISTR,         &
          NK,L1,AL1,AL2,AL3,AL4,BET1,BET2,CEXT,CSCA,AREA,VOL,RVW, &
          RMEAN,AA1,BB1,AA2,BB2,DDELT,ALB,Q1,r_eff                )
     QE=CEXT/AREA
     LMAX=L1-1
        
     ! Save values in output variables
     wls(islam) = waves(islam)
     qext(islam) = QE
!      r_eff = reff
!      r_eff0 = reff
     ssa(islam) = ALB
     phfcn(islam,1:nmom,1) = AL1(1:nmom)
     phfcn(islam,1:nmom,2) = AL2(1:nmom)
     phfcn(islam,1:nmom,3) = AL3(1:nmom)
     phfcn(islam,1:nmom,4) = AL4(1:nmom)
     phfcn(islam,1:nmom,5) = BET1(1:nmom)
     phfcn(islam,1:nmom,6) = BET2(1:nmom)
     
!      WRITE(9,'(F5.2,1X,F6.4,1X,F6.3,1X,F11.9,1X,1001(1X,E10.3))') &
!           waves(islam), QE, r_wet, ALB, &
!           (AL1(j),j=1,1001) 
!      WRITE(9,'(32X,1001(1X,E10.3))') &
!           (AL2(j),j=1,1001)
!      WRITE(9,'(32X,1001(1X,E10.3))') &
!           (AL3(j),j=1,1001) 
!      WRITE(9,'(32X,1001(1X,E10.3))') &
!           (AL4(j),j=1,1001) 
!      WRITE(9,'(32X,1001(1X,E10.3))') &
!           (BET1(j),j=1,1001) 
!      WRITE(9,'(32X,1001(1X,E10.3))') &
!           (BET2(j),j=1,1001) 
     
  ENDDO
  
!   ! Close file
!   CLOSE(9)

END SUBROUTINE mie_interface

!!$!!**************************************************************** 
!!$!!    CALCULATION OF THE SCATTERING MATRIX FOR GIVEN EXPANSION
!!$!!    COEFFICIENTS
!!$ 
!!$!!    A1,...,B2 - EXPANSION COEFFICIENTS
!!$!!    LMAX - NUMBER OF COEFFICIENTS MINUS 1
!!$!!    NPNA - NUMBER OF SCATTERING ANGLES
!!$!!        THE CORRESPONDING SCATTERING ANGLES ARE GIVEN BY
!!$!!        180*(I-1)/(NPNA-1) (DEGREES), WHERE I NUMBERS THE ANGLES
!!$!!****************************************************************
!!$SUBROUTINE MATR (A1,A2,A3,B1,B2,LMAX)
!!$  IMPLICIT NONE
!!$
!!$  INTEGER(KIND=4), PARAMETER :: NMIE=10000, NPNA=19
!!$  INTEGER(KIND=4), PARAMETER :: NPL=2*NMIE
!!$
!!$  REAL(KIND=8), DIMENSION(NPL), INTENT(IN) :: A1, A2, A3 ,B1 ,B2
!!$  INTEGER(KIND=4), INTENT(IN) :: LMAX
!!$
!!$  INTEGER(KIND=4) :: L1MAX, N, L, I1, L1
!!$
!!$  REAL(KIND=8) :: D6, U, TB, TAA, PP4, PP3, PP2, PP1, PL4, PL3, &
!!$       PL2, PL1, P4, P3, P2, P1, P, F44, F34, F33, F3, F2, F12, &
!!$       F11, DN, DL1, DL, DB, DA
!!$
!!$  L1MAX=LMAX+1
!!$  D6=DSQRT(6D0)*0.25D0
!!$  PRINT 1003
!!$1003 FORMAT(' ',5X,'<',11X,'F11',6X,'     F33',6X,'     F12', &
!!$          6X,'     F34')
!!$  N=NPNA
!!$  DN=1D0/DFLOAT(N-1)
!!$  DA=DACOS(-1D0)*DN
!!$  DB=180D0*DN
!!$  TB=-DB
!!$  TAA=-DA
!!$  DO I1=1,N
!!$     TAA=TAA+DA
!!$     TB=TB+DB
!!$     U=DCOS(TAA)
!!$     F11=0D0
!!$     F2=0D0
!!$     F3=0D0
!!$     F44=0D0
!!$     F12=0D0
!!$     F34=0D0
!!$     P1=0D0
!!$     P2=0D0
!!$     P3=0D0
!!$     P4=0D0
!!$     PP1=1D0
!!$     PP2=0.25D0*(1D0+U)*(1D0+U)
!!$     PP3=0.25D0*(1D0-U)*(1D0-U)
!!$     PP4=D6*(U*U-1D0)
!!$     DO L1=1,L1MAX
!!$        L=L1-1
!!$        DL=DFLOAT(L)
!!$        DL1=DFLOAT(L1)
!!$        F11=F11+A1(L1)*PP1
!!$        IF(L.EQ.LMAX) CONTINUE
!!$        PL1=DFLOAT(2*L+1)
!!$        P=(PL1*U*PP1-DL*P1)/DL1
!!$        P1=PP1
!!$        PP1=P
!!$        IF(L.LT.2) CONTINUE
!!$        F2=F2+(A2(L1)+A3(L1))*PP2
!!$        F3=F3+(A2(L1)-A3(L1))*PP3
!!$        F12=F12+B1(L1)*PP4
!!$        F34=F34+B2(L1)*PP4
!!$        IF(L.EQ.LMAX) CONTINUE
!!$        PL2=DFLOAT(L*L1)*U
!!$        PL3=DFLOAT(L1)*DFLOAT(L*L-4)
!!$        PL4=1D0/(DFLOAT(L)*DFLOAT(L1*L1-4))
!!$        P=(PL1*(PL2-4D0)*PP2-PL3*P2)*PL4
!!$        P2=PP2
!!$        PP2=P
!!$        P=(PL1*(PL2+4D0)*PP3-PL3*P3)*PL4
!!$        P3=PP3
!!$        PP3=P
!!$        P=(PL1*U*PP4-DSQRT(DFLOAT(L*L-4))*P4)/DSQRT(DFLOAT(L1*L1-4))
!!$        P4=PP4
!!$        PP4=P
!!$     END DO
!!$     F33=(F2-F3)*0.5D0
!!$     WRITE (10,1004) TB,F11,F33,F12,F34
!!$  END DO
!!$1004 FORMAT(' ',F6.2,6F14.5)
!!$  RETURN
!!$END SUBROUTINE MATR

!!***************************************************************
SUBROUTINE SPHER (AA,BB,GAM,LAM,MRR,MRII,R1,R2,N,NP,NDISTR,  &          
     NK,L1,AL1,AL2,AL3,AL4,BET1,BET2,CEXT,CSCA,AREA, &
     VOLUME,RVW,RMEAN,AA1,BB1,AA2,BB2,DDELT,ALB,Q1,REFF)              

  IMPLICIT NONE
  INTEGER, INTENT(IN) :: NDISTR, NK, NP, N
  REAL(KIND=8), INTENT(IN) :: AA, BB, GAM, LAM, MRR, MRII, &
       AA1, BB1, AA2, BB2, &
       DDELT
  REAL(KIND=8), INTENT(INOUT) :: REFF, AREA, VOLUME, RVW, RMEAN
  REAL(KIND=8), INTENT(OUT) :: CEXT, CSCA, ALB, Q1, R1, R2
  REAL(KIND=8), DIMENSION(NPL), INTENT(OUT) :: AL1, AL2, AL3, AL4, &
       BET1, BET2
  INTEGER, INTENT(INOUT) :: L1

  INTEGER :: NNPK, NNK, NG, M4, M3, M2, M1, M, L1MAX, L, K, J2, J1, J, &
       IJ, I1, I
  REAL(KIND=8) :: ZW, ZJ, Z2, Z1, YBR, YBI, YAR, YAI, Y, WN, WI, VOL, &
       VEFF, TJ, SPR, SPI, SMR, SMI, RXR, RXI, RX, RMR, RMI, QMAX, &
       PSI2, PSI1, PP, PM, PJ, PI, P4L1, P1L1, ORI, OR2, OR1, OR, &
       OI2, OI1, OI, OAB, MRI, HI2, HI1, FR, FM, FI, FF34, FF33, &
       FF12, FF11, DS, FP, DR1, DJ, DI1, DD2, DD1, DD, DC, D4, &
       D2, D1, CXI, CX, CS, CM, CL, CJ, CE, CDD, CDB, CDA, CD, BRJ, &
       BIJ, CXR, ARJ, AIJ, A3, A2
  REAL(KIND=8), DIMENSION(NMIE) :: PSI, AR, AI, BR, BI, PIN, TAUN
  REAL(KIND=8), DIMENSION(NDRDI) :: HI, RPSI, DR, DI
  REAL(KIND=8), DIMENSION(NPL) :: F11, F33, F12, &
       F34, X, W, XX
  REAL(KIND=8), DIMENSION(NGRAD) :: YY, WY
  
  
  ! Norm
  AL1(:) = 0.0d0
  AL2(:) = 0.0d0
  AL3(:) = 0.0d0
  AL4(:) = 0.0d0
  BET1(:) = 0.0d0
  BET2(:) = 0.0d0
  
  MRI=-MRII
  IF (NDISTR.EQ.3) CALL POWER (AA,BB,R1,R2)  
  !!  CALCULATION OF GAUSSIAN DIVISION POINTS AND WEIGHTS ************
  IF(NK.GT.NPL) PRINT 3336                                              
  IF(NK.GT.NPL) STOP                                                      
3336 FORMAT ('NK IS GREATER THAN NPL. EXECUTION TERMINATED')              
  CALL GAUSS (NK,0,0,X,W)                                                   
  NNK=N*NK                                                                  
  NNPK=NP*NK
  IF (NDISTR.NE.5) NNPK=0
  NNK=NNK+NNPK
  IF (NNK.GT.NGRAD) PRINT 3334                                             
  IF (NNK.GT.NGRAD) STOP                                                   
3334 FORMAT ('NNK IS GREATER THAN NGRAD, EXECUTION TERMINATED')               
  PI=3.1415926535897932D0                                               
  WN=2D0*PI/LAM                                                             
  RX=R2*WN                                                                  
  M=IDINT(RX+4.05D0*RX**0.33333D0+8D0)                                     
  IF (M.GT.NMIE) PRINT 3335                                                 
  IF (M.GT.NMIE) STOP                                                       
3335 FORMAT ('TOO MANY MIE COEFFICIENTS. INCREASE NMIE.')                
  DO I=1,M                                                               
     DD=DFLOAT(I)                                                           
     DD1=DD+1D0                                                             
     COEFF1(I)=DD1/DD                                                       
     DD2=(2D0*DD+1D0)                                                       
     COEFF2(I)=DD2                                                          
     COEFF3(I)=0.5D0*DD2/(DD*DD1)                                           
  END DO
  NG=2*M-1                                                                  
  L1MAX=2*M                                                                 
  CM=1D0/(MRR*MRR+MRI*MRI)                                                  
  RMR=MRR*CM                                                                
  RMI=-MRI*CM                                                               
  
  IF (NDISTR.EQ.5) THEN
     Z1=R1/DFLOAT(NP)                                                      
     Z2=Z1*0.5D0                                                               
     DO I=1,NK                                                              
        XX(I)=Z2*X(I)+Z2                     
     END DO
     DO J=1,NP                                                           
        J1=J-1                                                              
        ZJ=Z1*DFLOAT(J1)                                                    
        IJ=J1*NK                                                            
        DO I=1,NK                                                           
           I1=I+IJ                                                             
           YY(I1)=XX(I)+ZJ
           WY(I1)=W(I)*Z2                                                      
        END DO
     END DO
  ELSE
     Z1=(R2-R1)/DFLOAT(N)                                               
     Z2=Z1*0.5D0                                                        
     DO I=1,NK                                                       
        XX(I)=Z2*X(I)+Z2                        
     END DO
     DO J=1,N                                                     
        J1=J-1                                                      
        ZJ=Z1*DFLOAT(J1)+R1                          
        IJ=J1*NK+NNPK                            
        DO I=1,NK                                              
           I1=I+IJ                                                 
           YY(I1)=XX(I)+ZJ  
           WY(I1)=W(I)*Z2                                         
        END DO
     END DO
  END IF
  CALL DISTRB (NNK,YY,WY,NDISTR,AA,BB,GAM,R1,REFF,VEFF, &
       AREA,VOLUME,RVW,RMEAN,PI,AA1,BB1,AA2,BB2)
  
  CEXT=0D0                            
  CSCA=0D0                                
  CALL GAUSS (NG,0,0,X,W)                   
  DO I=1,NG                                  
     F11(I)=0D0                      
     F33(I)=0D0                                
     F12(I)=0D0                      
     F34(I)=0D0                    
  END DO
                                                                                
!!  AVERAGING OVER SIZES ************************************* 
  DO I=1,NNK                              
     Y=YY(I)                      
     ZW=WY(I)                
     RX= Y*WN                
     RXR=MRR*RX              
     RXI=MRI*RX              
     CDD=RXR*RXR+RXI*RXI     
     CD=DSQRT(CDD)           
     DC=DCOS(RX)             
     DS=DSIN(RX)             
     CX=1D0/RX               
     CXR=RXR/CDD             
     CXI=-RXI/CDD            
     
     !!  CALCULATION OF THE MIE COEFFICIENTS  ********************************       
     M1=IDINT(RX+4.05D0*RX**0.33333D0+8D0) 
     M2=M1+2+IDINT(1.2D0*DSQRT(RX))+5                
     M3=M2-1                                         
     IF (M2.GT.NDRDI) PRINT 3338                     
     IF (M2.GT.NDRDI) STOP                           
3338 FORMAT ('M2.GT.NDRDI. EXECUTION TERMINATED')  
     QMAX=DMAX1(DFLOAT(M1),CD)                       
     M4=IDINT(6.4D0*QMAX**0.33333D0+QMAX)+8          
     IF (M4.GT.NDRDI) PRINT 3337                     
     IF (M4.GT.NDRDI) STOP                           
3337 FORMAT ('M4.GT.NDRDI. EXECUTION TERMINATED')  
     D4=DFLOAT(M4+1)                                 
     DR(M4)=D4*CXR                                   
     DI(M4)=D4*CXI                                   
     HI(1)=DS+DC*CX                                  
     HI(2)=3.0D0*HI(1)*CX-DC                         
     PSI(1)=CX*DS-DC                                 
     RPSI(M2)=RX/DFLOAT(2*M2+1)                      
     DO J=2,M3                    
        J1=M2-J+1                                    
        RPSI(J1)=1D0/(DFLOAT(2*J1+1)*CX-RPSI(J1+1))  
     END DO
     DO J=2,M4                                     
        J1=M4-J+2                                    
        J2=J1-1                                      
        DJ=DFLOAT(J1)                                
        FR=DJ*CXR                                    
        FI=DJ*CXI                                    
        OR=DR(J1)+FR                                 
        OI=DI(J1)+FI                                 
        ORI=1D0/(OR*OR+OI*OI)                        
        DR(J2)=FR-OR*ORI                             
        DI(J2)=FI+OI*ORI                             
     END DO
     M2=M1-1                   
     DO J=2,M2                   
        J1=J-1                   
        J2=J+1            
        HI(J2)=DFLOAT(2*J+1)*HI(J)*CX-HI(J1) 
     END DO
     DO J=2,M1                  
        PSI(J)=RPSI(J)*PSI(J-1)      
     END DO
     PSI1=PSI(1)                         
     DR1=DR(1)                      
     DI1=DI(1)                      
     HI1=HI(1)                      
     OR=DR1*RMR-DI1*RMI+CX          
     OR1=OR*PSI1-DS                 
     OI=DR1*RMI+DI1*RMR             
     OI1=OI*PSI1                    
     OR2=OR*PSI1-OI*HI1-DS          
     OI2=OR*HI1+OI*PSI1-DC          
     OAB=1D0/(OR2*OR2+OI2*OI2)      
     AR(1)=(OR1*OR2+OI1*OI2)*OAB    
     AI(1)=(OR2*OI1-OR1*OI2)*OAB    
     OR=MRR*DR1-MRI*DI1+CX          
     OI=MRR*DI1+MRI*DR1             
     OR1=OR*PSI1-DS                 
     OR2=OR*PSI1-OI*HI1-DS          
     OI1=OI*PSI1                    
     OI2=OR*HI1+OI*PSI1-DC          
     OAB=1D0/(OR2*OR2+OI2*OI2)      
     BR(1)=(OR1*OR2+OI1*OI2)*OAB    
     BI(1)=(OR2*OI1-OR1*OI2)*OAB    
     DO J=2,M1                   
        J1=J-1                     
        DJ=DFLOAT(J)*CX            
        PSI1=PSI(J)                
        PSI2=PSI(J1)               
        HI1=HI(J)                  
        HI2=HI(J1)                 
        DR1=DR(J)                  
        DI1=DI(J)                  
        OR=DR1*RMR-DI1*RMI+DJ      
        OI=DR1*RMI+DI1*RMR         
        OR1=OR*PSI1-PSI2           
        OI1=OI*PSI1                
        OR2=OR*PSI1-OI*HI1-PSI2    
        OI2=OR*HI1+OI*PSI1-HI2     
        OAB=1D0/(OR2*OR2+OI2*OI2)  
        YAR=(OR1*OR2+OI1*OI2)*OAB  
        YAI=(OR2*OI1-OR1*OI2)*OAB  
        AR(J)=YAR                  
        AI(J)=YAI                  
        OR=MRR*DR1-MRI*DI1+DJ      
        OI=MRR*DI1+MRI*DR1         
        OR1=OR*PSI1-PSI2           
        OR2=OR*PSI1-OI*HI1-PSI2    
        OI1=OI*PSI1                
        OI2=OR*HI1+OI*PSI1-HI2     
        OAB=1D0/(OR2*OR2+OI2*OI2)  
        YBR=(OR1*OR2+OI1*OI2)*OAB  
        YBI=(OR2*OI1-OR1*OI2)*OAB  
        BR(J)=YBR                  
        BI(J)=YBI                  
        YAR=YAR*YAR+YAI*YAI+YBR*YBR+YBI*YBI
     END DO
                                                                                
     !!  END OF COMPUTING THE MIE COEFFICIENTS  **************************           
     CE=0D0                          
     CS=0D0                        
     DO J=1,M1                         
        CJ=COEFF2(J)             
        ARJ=AR(J)                
        AIJ=AI(J)                
        BRJ=BR(J)                
        BIJ=BI(J)                
        CDA=ARJ*ARJ+AIJ*AIJ      
        CDB=BRJ*BRJ+BIJ*BIJ      
        CE=CE+CJ*(ARJ+BRJ)       
        CS=CS+CJ*(CDA+CDB)       
        CJ=COEFF3(J)             
        AR(J)=CJ*(ARJ+BRJ)       
        AI(J)=CJ*(AIJ+BIJ)       
        BR(J)=CJ*(ARJ-BRJ)       
        BI(J)=CJ*(AIJ-BIJ)       
     END DO
     CEXT=CEXT+ZW*CE              
     CSCA=CSCA+ZW*CS              
     DO K=1,NG                 
        CALL ANGL (M1,X(K),PIN,TAUN)
        SPR=0D0  
        SPI=0D0                 
        SMR=0D0                 
        SMI=0D0                 
        DO J=1,M1            
           PJ=PIN(J)           
           TJ=TAUN(J)          
           PP=TJ+PJ            
           PM=TJ-PJ            
           SPR=SPR+AR(J)*PP    
           SPI=SPI+AI(J)*PP    
           SMR=SMR+BR(J)*PM    
           SMI=SMI+BI(J)*PM    
        END DO
        D1=(SPR*SPR+SPI*SPI)*ZW 
        D2=(SMR*SMR+SMI*SMI)*ZW 
        F11(K)=F11(K)+D1+D2     
        F33(K)=F33(K)+D1-D2     
        F12(K)=F12(K)+(SPR*SMR+SPI*SMI)*ZW*2D0
        F34(K)=F34(K)+(SPR*SMI-SPI*SMR)*ZW*2D0      
     END DO
  END DO
                                                                                
  !!  END OF AVERAGING OVER SIZES  ********************************                
     
  !!  ELEMENTS OF THE SCATTERING MATRIX  **************************                
  DD=2D0/CSCA           
  DO I=1,NG         
     F11(I)=F11(I)*DD  
     F12(I)=F12(I)*DD  
     F33(I)=F33(I)*DD  
     F34(I)=F34(I)*DD  
  END DO
                                                                                
!!  CROSS SECTIONS AND SINGLE SCATTERING ALBEDO  ***************                
  VOL=2D0*PI/(WN*WN) 
  CEXT=CEXT*VOL      
  CSCA=CSCA*VOL      
  ALB=CSCA/CEXT      
  
  !!  CALCULATION OF THE EXPANSION COEFFICIENTS  *******************              
  DO L1=3,L1MAX
     L=L1-1
     COEF1(L1)=1D0/DFLOAT(L+1)
     COEF2(L1)=DFLOAT(2*L+1)                        
     COEF3(L1)=1D0/DSQRT(DFLOAT((L+1)*(L+1)-4))
     COEF4(L1)=DSQRT(DFLOAT(L*L-4))                 
     COEF5(L1)=1D0/(DFLOAT(L)*DFLOAT((L+1)*(L+1)-4))
     COEF6(L1)=DFLOAT(2*L+1)*DFLOAT(L*(L+1))
     COEF7(L1)=DFLOAT((2*L+1)*4)
     COEF8(L1)=DFLOAT(L+1)*DFLOAT(L*L-4)
  END DO
  DO L1=1,L1MAX
     AL1(L1)=0D0  
     AL2(L1)=0D0  
     AL3(L1)=0D0  
     AL4(L1)=0D0  
     BET1(L1)=0D0 
     BET2(L1)=0D0 
  END DO
  D6=0.25D0*DSQRT(6D0)
  DO I=1,NG                                                             
     CALL GENER (X(I),L1MAX)                                               
     WI=W(I)                                                               
     FF11=F11(I)*WI                                                        
     FF33=F33(I)*WI                                                        
     FF12=F12(I)*WI                                                        
     FF34=F34(I)*WI                                                        
     FP=FF11+FF33                                                          
     FM=FF11-FF33                                                          
     DO L1=1,L1MAX                                                     
        P1L1=P1(L1)                                                       
        P4L1=P4(L1)                                                       
        AL1(L1)=AL1(L1)+FF11*P1L1                                         
        AL4(L1)=AL4(L1)+FF33*P1L1                                         
        AL2(L1)=AL2(L1)+FP*P2(L1)                                         
        AL3(L1)=AL3(L1)+FM*P3(L1)                                         
        BET1(L1)=BET1(L1)+FF12*P4L1                                       
        BET2(L1)=BET2(L1)+FF34*P4L1                                       
     END DO
  END DO
  DO L1=1,L1MAX                                                         
     CL=DFLOAT(L1-1)+0.5D0                                                 
     L=L1                                                                  
     AL1(L1)=AL1(L1)*CL                                                    
     A2=AL2(L1)*CL*0.5D0                                                   
     A3=AL3(L1)*CL*0.5D0                                                   
     AL2(L1)=A2+A3                                                         
     AL3(L1)=A2-A3                                                         
     AL4(L1)=AL4(L1)*CL                                                    
     BET1(L1)=BET1(L1)*CL                                                  
     BET2(L1)=-BET2(L1)*CL                                                  
     IF (DABS(AL1(L1)).LE.DDELT) EXIT
  END DO
  L1MAX=L                                                                   
  
  !!  PRINTOUT   **********************************************************        
  PRINT 504,R1,R2                                                           
504 FORMAT ('R1 = ', d12.6,'   R2=',d12.6)                                    
  PRINT 515, REFF,VEFF                                              
515 FORMAT ('REFF=',d12.6,'   VEFF=',d12.6)      
  PRINT 500,LAM,MRR,MRII                                     
500 FORMAT('LAM =',F 8.4,'  MRR=',D12.6,'  MRI=',D12.6)            
  !505  FORMAT (' X1 =',d12.6,'   X2=',d12.6)                                    
  PRINT 506,NK,N,NP   
506 FORMAT('NK=',I4,'   N=',I4,'   NP=',I4)  
  Q1=AL1(2)/3D0                                                             
  PRINT 511,Q1                                                         
511 FORMAT('<COS> =',d14.8)                                
  PRINT 508,CEXT,CSCA,ALB                                                  
508 FORMAT('CEXT=',d14.8,'   CSCA=',d14.8,'   ALBEDO =',d14.8)    
  PRINT 555,M                                                               
  !c      PRINT 550                                                               
  !c      PRINT 510                                                              
  !c  510 FORMAT('*********   EXPANSION COEFFICIENTS   *********')                
  !c  570 FORMAT('   S     ALPHA 1    ALPHA 2    ALPHA 3',          
  !c     *'    ALPHA 4     BETA 1     BETA 2')                       
  !c      PRINT 570                                                               
  !c      DO 520 L=1,L1MAX                                                        
  !c         LL=L-1                                                               
  !c         PRINT 728,LL,AL1(L),AL2(L),AL3(L),AL4(L),BET1(L),BET2(L)             
  !c  520 CONTINUE                                                                
  !c  728 FORMAT(I4,1X,6D14.7)
  !c  550 FORMAT(' ')                                                             
555 FORMAT('MAXIMAL ORDER OF MIE COEFFICIENTS = ',I4)                       
  !       WRITE (9,580) L1MAX
  !       DO L=1,L1MAX
  !          WRITE (9,575) AL1(L),AL2(L),AL3(L),AL4(L),BET1(L),BET2(L)           
  !       ENDDO   
  !575  FORMAT(6D14.6)
  !580  FORMAT(I8)
  RETURN                                                                    
END SUBROUTINE SPHER
   
!!**********************************************************************       
!!   CALCULATION OF THE ANGULAR FUNCTIONS PI(N) AND TAU(N) FOR                  
!!   GIVEN ARGUMENT U=COS(THETA)                                                
!!   N.LE.NMAX                                                                    
SUBROUTINE ANGL (NMAX,U,PIN,TAUN)                                         
  IMPLICIT NONE
  INTEGER, INTENT(IN) :: NMAX
  REAL(KIND=8), INTENT(IN) :: U
  REAL(KIND=8), INTENT(OUT), DIMENSION(NMAX) :: PIN, TAUN
  INTEGER :: N
  REAL(KIND=8) :: P1, P2, S, T
  
  P1=0D0                                                                    
  P2=1D0                                                                    
  N=1         
  DO N = 1, NMAX
     S=U*P2                                                                    
     T=S-P1                                                                    
     TAUN(N)=DFLOAT(N)*T-P1                                                    
     PIN(N)=P2                                                                 
     P1=P2                                                                     
     P2=S+COEFF1(N)*T                                                                  
  END DO
  RETURN                                                                    
END SUBROUTINE ANGL
 
!!***********************************************************************      
!!  CALCULATION OF THE GENERALIZED SPHERICAL FUNCTIONS                         
SUBROUTINE GENER (U,L1MAX)                                                
  IMPLICIT NONE
  REAL(KIND=8), INTENT(IN) :: U
  INTEGER, INTENT(IN) :: L1MAX
  REAL(KIND=8) :: DUP, DUM, DU, DL, C1, C2, C3, C4, C5, C6, C7, C8, &
       CU1, CU2
  INTEGER :: LMAX, L1, L2, L3
  DUP=1D0+U                                                                 
  DUM=1D0-U                                                                 
  DU=U*U                                                                    
  P1(1)=1D0                                                                 
  P1(2)=U                                                                   
  P1(3)=0.5D0*(3D0*DU-1D0)                                                  
  P2(1)=0D0                                                                 
  P2(2)=0D0                                                                 
  P2(3)=0.25D0*DUP*DUP                                                      
  P3(1)=0D0                                                                 
  P3(2)=0D0                                                                 
  P3(3)=0.25D0*DUM*DUM                                                      
  P4(1)=0D0                                                                 
  P4(2)=0D0                                                                 
  P4(3)=D6*(DU-1D0)                                                         
  LMAX=L1MAX-1                                                              
  DO L1=3,LMAX                                                          
     C1=COEF1(L1)                                                           
     C2=COEF2(L1)                                                           
     C3=COEF3(L1)                                                           
     C4=COEF4(L1)                                                           
     C5=COEF5(L1)                                                           
     C6=COEF6(L1)                                                           
     C7=COEF7(L1)                                                           
     C8=COEF8(L1)                                                           
     CU1=C2*U                                                               
     CU2=C6*U                                                               
     L2=L1+1                                                                
     L3=L1-1                                                                
     DL=DFLOAT(L3)                                                          
     P1(L2)=C1*(CU1*P1(L1)-DL*P1(L3))                                       
     P2(L2)=C5*((CU2-C7)*P2(L1)-C8*P2(L3))                                  
     P3(L2)=C5*((CU2+C7)*P3(L1)-C8*P3(L3))                                  
     P4(L2)=C3*(CU1*P4(L1)-C4*P4(L3))                                       
  END DO
  RETURN                                                                    
END SUBROUTINE GENER

!!*********************************************************************           
SUBROUTINE DISTRB (NNK,YY,WY,NDISTR,AA,BB,GAM,R1,REFF, &
     VEFF,AREA,VOLUME,RVW,RMEAN,PI,AA1,BB1,AA2,BB2)
  IMPLICIT NONE
  INTEGER, INTENT(IN) :: NNK, NDISTR                                        
  REAL(KIND=8), INTENT(IN) :: AA, BB, GAM, R1, &
       PI, AA1, BB1, AA2, BB2
  REAL(KIND=8), INTENT(INOUT) :: REFF
  REAL(KIND=8), INTENT(OUT) :: VOLUME, RVW, RMEAN, VEFF, AREA
  REAL(KIND=8), INTENT(OUT), DIMENSION(NNK) :: YY, WY

  REAL(KIND=8) :: Y, Y1, Y2, X, XI, SUM, G, DA, DA1, DA2, DAB, DB, B2, A2
  INTEGER :: I

  SELECT CASE (NDISTR)
  CASE (1)
     PRINT 1001,AA,BB,GAM                                                      
1001 FORMAT('MODIFIED GAMMA DISTRIBUTION, ALPHA=',F6.4,'  r_c=',               &
          F6.4,'  GAMMA=',F6.4)                                                   
     A2=AA/GAM                                                                 
     DB=1D0/BB
     DO I=1,NNK                                                             
        X=YY(I)                                                             
        Y=X**AA                                                                
        X=X*DB
        Y=Y*DEXP(-A2*(X**GAM))                                                 
        WY(I)=WY(I)*Y                                                       
     END DO
  CASE (2)
     PRINT 1002,AA,BB                                                          
1002 FORMAT('LOG NORMAL DISTRIBUTION, r_g=',F8.4,'  [ln(sigma_g)]**2=', &
          F6.4)                                                              
     DA=1D0/AA                                                                 
     DO I=1,NNK                                                            
        X=YY(I)                                                                
        Y=DLOG(X*DA)                                                          
        Y=DEXP(-Y*Y*0.5D0/BB)/X                                             
        WY(I)=WY(I)*Y                                                          
     END DO
  CASE (3)
     PRINT 1003                                                                
1003 FORMAT('POWER LAW DISTRIBUTION OF HANSEN & TRAVIS 1974')                 
     DO I=1,NNK                                                            
        X=YY(I)                                                                
        WY(I)=WY(I)/(X*X*X)                                                 
     END DO
  CASE (4)
     PRINT 1004,AA,BB     
1004 FORMAT ('GAMMA DISTRIBUTION,  a=',F8.4,'  b=',F6.4)
     B2=(1D0-3D0*BB)/BB                                                        
     DAB=1D0/(AA*BB)                                                          
     DO I=1,NNK                                                            
        X=YY(I)                                                                
        X=(X**B2)*DEXP(-X*DAB)                                                 
        WY(I)=WY(I)*X                                                       
     END DO
  CASE (5)
     PRINT 1005,BB                                                             
1005 FORMAT ('MODIFIED POWER LAW DISTRIBUTION,  ALPHA=',D10.4)
     DO I=1,NNK                                                            
        X=YY(I)                                                                
        IF (X.LE.R1) WY(I)=WY(I)
        IF (X.GT.R1) WY(I)=WY(I)*(X/R1)**BB
     END DO
  CASE (6)
     PRINT 1006                                    
     PRINT 1007,AA1,BB1                                    
     PRINT 1008,AA2,BB2,GAM                                    
1006 FORMAT('BIMODAL VOLUME LOG NORMAL DISTRIBUTION')
1007 FORMAT('r_g1=',F8.4,'  [ln(sigma_g1)]**2=',F6.4)
1008 FORMAT('r_g2=',F8.4,'  [ln(sigma_g2)]**2=',F6.4, &
          ' gamma=',F7.3)
     DA1=1D0/AA1       
     DA2=1D0/AA2                  
     DO I=1,NNK                                                            
        X=YY(I)                                                                
        Y1=DLOG(X*DA1)                                                   
        Y2=DLOG(X*DA2)                                           
        Y1=DEXP(-Y1*Y1*0.5D0/BB1)                                             
        Y2=DEXP(-Y2*Y2*0.5D0/BB2)                                             
        WY(I)=WY(I)*(Y1+GAM*Y2)/(X*X*X*X)                        
     ENDDO
  END SELECT
  SUM=0D0
  DO I=1,NNK
     SUM=SUM+WY(I)
  END DO
  SUM=1D0/SUM
  DO I=1,NNK
     WY(I)=WY(I)*SUM
  END DO
  G=0D0
  DO I=1,NNK
     X=YY(I)
     G=G+X*X*WY(I)
  END DO
  REFF=0D0
  DO I=1,NNK
     X=YY(I)
     REFF=REFF+X*X*X*WY(I)
  END DO
  REFF=REFF/G
  VEFF=0D0
  VOLUME=0D0
  RVW=0D0
  RMEAN=0D0
  DO I=1,NNK
     X=YY(I)
     XI=X-REFF
     VEFF=VEFF+XI*XI*X*X*WY(I)
     VOLUME=VOLUME+X*X*X*WY(I)
     RVW=RVW+X*X*X*X*WY(I)
     RMEAN=RMEAN+X*WY(I)
  END DO
  VEFF=VEFF/(G*REFF*REFF)
  AREA=G*PI  
  RVW=RVW/VOLUME
  VOLUME=VOLUME*4D0*PI/3D0
  RETURN                                                                    
END SUBROUTINE DISTRB

!!*********************************************************
SUBROUTINE GAUSS ( N,IND1,IND2,Z,W )
  IMPLICIT NONE
  INTEGER, INTENT(IN) :: N, IND1, IND2
  REAL(KIND=8), DIMENSION(N), INTENT(OUT) :: Z, W
  
  INTEGER(KIND=4) :: IND, I, J, K, M, NITER
  REAL(KIND=8) :: A, B, C, F, X, DJ, PA, PB, PC, ZZ, CHECK
  DATA A,B,C /1D0,2D0,3D0/
  
  IND=MOD(N,2)
  K=N/2+IND
  F=DFLOAT(N)
  DO I=1,K
     M=N+1-I
     IF(I.EQ.1) X=A-B/((F+A)*F)
     IF(I.EQ.2) X=(Z(N)-A)*4D0+Z(N)
     IF(I.EQ.3) X=(Z(N-1)-Z(N))*1.6D0+Z(N-1)
     IF(I.GT.3) X=(Z(M+1)-Z(M+2))*C+Z(M+3)
     IF(I.EQ.K.AND.IND.EQ.1) X=0D0
     NITER=0
     CHECK=1D-16
     DO 
        PB=1D0
        NITER=NITER+1
        IF (NITER.GT.100) CHECK=CHECK*10D0 
        PC=X
        DJ=A
        DO J=2,N
           DJ=DJ+A
           PA=PB
           PB=PC
           PC=X*PB+(X*PB-PA)*(DJ-A)/DJ
        END DO
        PA=A/((PB-X*PC)*F)
        PB=PA*PC*(A-X*X)
        X=X-PB
        IF(DABS(PB).LE.check*DABS(X)) EXIT
     END DO
     Z(M)=X
     W(M)=PA*PA*(A-X*X)
     IF(IND1.EQ.0) W(M)=B*W(M)
     IF(I.EQ.K.AND.IND.EQ.1) CYCLE
     Z(I)=-Z(M)
     W(I)=W(M)
  END DO
  IF(IND2.EQ.1) THEN
     PRINT 1100,N
1100 FORMAT(' ***  POINTS AND WEIGHTS OF GAUSSIAN QUADRATURE FORMULA', &
          ' OF ',I4,'-TH ORDER')
     DO I=1,K
        ZZ=-Z(I)
        PRINT 1200,I,ZZ,I,W(I)
     END DO
1200 FORMAT(' ',4X,'X(',I4,') = ',F17.14,5X,'W(',I4,') = ',F17.14)
  ENDIF
  IF(IND1.NE.0) THEN 
     DO I=1,N
        Z(I)=(A+Z(I))/B
     END DO
  END IF
  RETURN
END SUBROUTINE GAUSS
!!********************************************************************
!!  COMPUTATION OF R1 AND R2 FOR THE POWER LAW SIZE DISTRIBUTION WITH
!!  EFFECTIVE RADIUS A AND EFFECTIVE VARIANCE B
SUBROUTINE POWER (A,B,R1,R2)
  IMPLICIT NONE
  REAL(KIND=8), INTENT(IN) :: A, B
  REAL(KIND=8), INTENT(OUT) :: R1, R2
  REAL(KIND=8) :: AA, BB, AX, BX
  COMMON AA, BB
  AA=A
  BB=B
  AX=1D-12
  BX=A  
  R1=ZEROIN(AX,BX,0D0)
  R2=(1D0+B)*2D0*A-R1
  RETURN
END SUBROUTINE POWER

!!***********************************************************************
FUNCTION FUNC(R1) RESULT(F)
  IMPLICIT NONE
  REAL(KIND=8), INTENT(IN) :: R1
  REAL(KIND=8) :: F
  REAL(KIND=8) :: R2, A, B
  COMMON A, B
  R2=(1D0+B)*2D0*A-R1
  F=(R2-R1)/DLOG(R2/R1)-A
  RETURN
END FUNCTION FUNC

!!***********************************************************************
FUNCTION ZEROIN (AX,BX,TOL)
  IMPLICIT NONE
  REAL(KIND=8), INTENT(IN) :: AX, BX, TOL
  REAL(KIND=8) :: ZEROIN
  REAL(KIND=8) :: EPS, TOL1, A, B, C, D, E, P, Q, R, S, &
       FA, FB, FC, XM
  EPS=1D0
  DO 10
     EPS=0.5D0*EPS
     TOL1=1D0+EPS
     IF (TOL1.LE.1D0) EXIT
10 END DO
  A=AX
  B=BX
  FA=FUNC(A)
  FB=FUNC(B)
20 C=A
  FC=FA
  D=B-A
  E=D
30 IF (DABS(FC).GE.DABS(FB)) GO TO 40
  A=B
  B=C
  C=A
  FA=FB
  FB=FC
  FC=FA
40 TOL1=2D0*EPS*DABS(B)+0.5D0*TOL
  XM=0.5D0*(C-B)
  IF (DABS(XM).LE.TOL1) GO TO 90
  IF (FB.EQ.0D0) GO TO 90
  IF (DABS(E).LT.TOL1) GO TO 70
  IF (DABS(FA).LE.DABS(FB)) GO TO 70
  IF (A.NE.C) GO TO 50
  S=FB/FA
  P=2D0*XM*S
  Q=1D0-S
  GO TO 60
50 Q=FA/FC
  R=FB/FC
  S=FB/FA
  P=S*(2D0*XM*Q*(Q-R)-(B-A)*(R-1D0))
  Q=(Q-1D0)*(R-1D0)*(S-1D0)
60 IF (P.GT.0D0) Q=-Q
  P=DABS(P)
  IF ((2D0*P).GE.(3D0*XM*Q-DABS(TOL1*Q))) GO TO 70
  IF (P.GE.DABS(0.5D0*E*Q)) GO TO 70
  E=D
  D=P/Q
  GO TO 80
70 D=XM
  E=D
80 A=B
  FA=FB
  IF (DABS(D).GT.TOL1) B=B+D
  IF (DABS(D).LE.TOL1) B=B+DSIGN(TOL1,XM)
  FB=FUNC(B)
  IF ((FB*(FC/DABS(FC))).GT.0D0) GO TO 20
  GO TO 30
90 ZEROIN=B
  RETURN
END FUNCTION ZEROIN

END MODULE mie_code_module
