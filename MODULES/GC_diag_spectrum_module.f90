!------------------------------------------------------------------------------
!          Harvard University Atmospheric Chemistry Modeling Group            !
!------------------------------------------------------------------------------
!BOP
!
! !MODULE: GC_diag_spectrum_module.f90
!
! !DESCRIPTION: This module contains routines for writing spectrum diagnostics
!\\
!\\
! !INTERFACE: 
!
MODULE GC_diag_spectrum_module

  USE GC_parameters_module
  USE GC_netcdf_module,      ONLY : create_ncvar, write_ncvar, &
                                      match_names_in_dimlist,  &
                                      nc_fld_1d, nc_fld_2d,    &
                                      nc_fld_3d
    
  IMPLICIT NONE
  INCLUDE 'netcdf.inc'
  
  CONTAINS
  
  SUBROUTINE do_spectrum_diag( NCID, ACTION, DO_XY, DIAG_NUM )
    
    
    USE GC_variables_module, ONLY : lambdas, clambdas, do_effcrs,        &
                                    lambda_resolution, didx,             &
                                    do_stokes_ad20, do_stokes_ad21,      &
                                    do_stokes_ad22, GC_flux,             &
                                    GC_radiances, GC_Qvalues, GC_Uvalues,&
                                    do_vector_calculation, GC_Qflux,     &
                                    GC_Uflux, GC_Qdirect_flux,           &
                                    GC_Udirect_flux, GC_direct_flux,     &
                                    solar_cspec_data
    
    ! ---------------------------
    ! Subroutine arguments
    ! ---------------------------
    INTEGER, INTENT(IN) :: NCID
    INTEGER, INTENT(IN) :: ACTION
    LOGICAL, INTENT(IN) :: DO_XY
    INTEGER, INTENT(IN) :: DIAG_NUM
      
    ! ---------------
    ! Local variables
    ! ---------------
    INTEGER                                 :: VAR_ID
    LOGICAL                                 :: error, dummy_err
    INTEGER, DIMENSION(1)                   :: size_1d, tmp_1d
    CHARACTER(LEN=max_ch_len), DIMENSION(1) :: dim_1d
    INTEGER, DIMENSION(2)                   :: size_2d, tmp_2d
    CHARACTER(LEN=max_ch_len), DIMENSION(2) :: dim_2d
    INTEGER, DIMENSION(3)                   :: size_3d, tmp_3d
    CHARACTER(LEN=max_ch_len), DIMENSION(3) :: dim_3d
      
    ! =====================================================
    ! do_spectrum_diag starts here
    ! =====================================================
    
    ! Initialize error
    error = .FALSE.
    
    SELECTCASE( DIAG_NUM )
      
      ! ======================================================
      ! Wavelength Grid
      ! ======================================================
      CASE( 19 )
        
        ! The dimension of the output
        dim_1d(1) = 'nw'
        
        ! Get dimension
        CALL match_names_in_dimlist( dim_1d, 1, tmp_1d, size_1d, dummy_err )
        
        IF ( .NOT. do_effcrs .AND. lambda_resolution /= 0.0d0) THEN
          CALL nc_fld_1d( dim_1d, size_1d(1), clambdas(1:size_1d(1)), 'Wavelength', &
                          NCID,   ACTION,     DO_XY                                 )
        ELSE
          CALL nc_fld_1d( dim_1d, size_1d(1), lambdas(1:size_1d(1)), 'Wavelength', &
                          NCID,   ACTION,     DO_XY                                )
        ENDIF
      
      ! ======================================================
      ! Radiance
      ! ======================================================
      CASE( 20 )
        
        ! Dimensions of output
        dim_3d(1) = 'nw' ; dim_3d(2) = 'ngeom' ; dim_3d(3) = 'noutputlevel'
        
        ! Get dimensions
        CALL match_names_in_dimlist( dim_3d, 3, tmp_3d, size_3d, dummy_err )
        
        CALL nc_fld_3d( dim_3d, size_3d(1), size_3d(2), size_3d(3),                            &
                        GC_radiances(1:size_3d(1),1:size_3d(2),1:size_3d(3),didx), 'radiance', &
                        NCID, ACTION, DO_XY                                                    )
        
        IF( do_stokes_ad20 .and. do_vector_calculation ) THEN
          
          CALL nc_fld_3d( dim_3d, size_3d(1), size_3d(2), size_3d(3),                &
                        GC_Qvalues(1:size_3d(1),1:size_3d(2),1:size_3d(3),didx), 'q',&
                        NCID, ACTION, DO_XY                                          )
          
          CALL nc_fld_3d( dim_3d, size_3d(1), size_3d(2), size_3d(3),                &
                        GC_Uvalues(1:size_3d(1),1:size_3d(2),1:size_3d(3),didx), 'u',&
                        NCID, ACTION, DO_XY                                          )
        ENDIF
        
      ! ======================================================
      ! Flux
      ! ======================================================
      CASE( 21 )
        
        ! Dimensions of output
        dim_3d(1) = 'nw' ; dim_3d(2) = 'ngeom' ; dim_3d(3) = 'noutputlevel'
        
        ! Get dimensions
        CALL match_names_in_dimlist( dim_3d, 3, tmp_3d, size_3d, dummy_err )
        
        CALL nc_fld_3d( dim_3d, size_3d(1), size_3d(2), size_3d(3),                   &
                        GC_flux(1:size_3d(1),1:size_3d(2),1:size_3d(3),didx), 'flux', &
                        NCID, ACTION, DO_XY                                           )
        
        IF( do_stokes_ad20 .and. do_vector_calculation ) THEN
          
          CALL nc_fld_3d( dim_3d, size_3d(1), size_3d(2), size_3d(3),                &
                        GC_Qflux(1:size_3d(1),1:size_3d(2),1:size_3d(3),didx), 'qflux',&
                        NCID, ACTION, DO_XY                                          )
          
          CALL nc_fld_3d( dim_3d, size_3d(1), size_3d(2), size_3d(3),                &
                        GC_Uflux(1:size_3d(1),1:size_3d(2),1:size_3d(3),didx), 'uflux',&
                        NCID, ACTION, DO_XY                                          )
        ENDIF
        
      ! ======================================================
      ! Direct Flux
      ! ======================================================
      CASE( 22 )
        
        ! Dimensions of output
        dim_3d(1) = 'nw' ; dim_3d(2) = 'ngeom' ; dim_3d(3) = 'noutputlevel'
        
        ! Get dimensions
        CALL match_names_in_dimlist( dim_3d, 3, tmp_3d, size_3d, dummy_err )
        
        CALL nc_fld_3d( dim_3d, size_3d(1), size_3d(2), size_3d(3),                                 &
                        GC_direct_flux(1:size_3d(1),1:size_3d(2),1:size_3d(3),didx), 'direct_flux', &
                        NCID, ACTION, DO_XY                                                         )
        
        IF( do_stokes_ad20 .and. do_vector_calculation ) THEN
          
          CALL nc_fld_3d( dim_3d, size_3d(1), size_3d(2), size_3d(3),                                &
                        GC_Qdirect_flux(1:size_3d(1),1:size_3d(2),1:size_3d(3),didx), 'qdirect_flux',&
                        NCID, ACTION, DO_XY                                                          )
          
          CALL nc_fld_3d( dim_3d, size_3d(1), size_3d(2), size_3d(3),                                &
                        GC_Udirect_flux(1:size_3d(1),1:size_3d(2),1:size_3d(3),didx), 'udirect_flux',&
                        NCID, ACTION, DO_XY                                                           )
        ENDIF
        
      ! ======================================================
      ! Solar Irradiance
      ! ======================================================
      CASE( 23 )
        
        ! The dimension of the output
        dim_1d(1) = 'nw'
        
        ! Get dimension
        CALL match_names_in_dimlist( dim_1d, 1, tmp_1d, size_1d, dummy_err )
        
        CALL nc_fld_1d( dim_1d, size_1d(1), solar_cspec_data(1:size_1d(1)), 'irradiance', &
                        NCID,   ACTION,     DO_XY                                         )
        
      CASE DEFAULT
        
        print*,'The selected diagnostic is not in the spectrum diags:', diag_num
        STOP
        
    ENDSELECT
   
   
  END SUBROUTINE do_spectrum_diag
  
  SUBROUTINE archive_spectrum_diag( iw, dir )
    
    USE GC_variables_module, ONLY: VLIDORT_ModIn, VLIDORT_Out,           &
                                   VLIDORT_FixIn, GC_flux,               &
                                    GC_radiances, GC_Qvalues, GC_Uvalues,&
                                    do_vector_calculation, GC_Qflux,     &
                                    GC_Uflux, GC_Qdirect_flux,           &
                                    GC_Udirect_flux, GC_direct_flux,     &
                                    do_StokesQU_output, do_stokes_ad20,  &
                                    do_stokes_ad21, do_stokes_ad22
    
    ! --------------------
    ! Subroutine arguments
    ! --------------------
    INTEGER, INTENT(IN) :: iw, dir
    
    ! ---------------
    ! Local variables
    ! ---------------
    INTEGER :: IB, UM, UA, V, q, g, n, il
    
    ! =====================================================
    ! archive_spectrum_diag starts here
    ! =====================================================
    
    ! ------------------------------------
    ! Save Radiances, flux and direct flux
    ! ------------------------------------
    DO IB = 1, VLIDORT_ModIn%MSunrays%TS_N_SZANGLES
       GC_flux(iw,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,IB,dir)        = &
            VLIDORT_Out%Main%TS_FLUX_STOKES(1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,IB,1,dir)
       IF (dir .EQ. 2) THEN
          GC_direct_flux(iw,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,IB,dir) = &
               VLIDORT_Out%Main%TS_FLUX_DIRECT(1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,IB,1)
       ELSE
          GC_direct_flux(iw,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,IB,dir) = 0.0d0
       END IF
       DO UM = 1, VLIDORT_ModIn%MUserVal%TS_N_USER_VZANGLES
          DO UA = 1, VLIDORT_ModIn%MUserVal%TS_N_USER_RELAZMS
             V = VLIDORT_Out%Main%TS_VZA_OFFSETS(IB,UM) + UA
             GC_Radiances(iw,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,V,dir) = &
                  VLIDORT_Out%Main%TS_STOKES(1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,V,1,dir)
          ENDDO
       ENDDO
    ENDDO
    
    ! -------------------------------------------------
    ! Save Q and U values (optional output, if flagged)
    ! -------------------------------------------------
    IF ( do_vector_calculation .AND. do_StokesQU_output ) THEN
       DO IB = 1, VLIDORT_ModIn%MSunrays%TS_N_SZANGLES
          IF( do_stokes_ad21 ) THEN
            GC_Qflux(iw,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,IB,dir)        = &
                VLIDORT_Out%Main%TS_FLUX_STOKES(1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,IB,2,dir)
            GC_Uflux(iw,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,IB,dir)        = &
                VLIDORT_Out%Main%TS_FLUX_STOKES(1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,IB,3,dir)
          ENDIF
          IF( do_stokes_ad22 ) THEN
            IF (dir .EQ. 2) THEN
              GC_Qdirect_flux(iw,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,IB,dir) = &
                    VLIDORT_Out%Main%TS_FLUX_DIRECT(1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,IB,2)
              GC_Udirect_flux(iw,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,IB,dir) = &
                    VLIDORT_Out%Main%TS_FLUX_DIRECT(1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,IB,3)
            ELSE
              GC_Qdirect_flux(iw,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,IB,dir) = 0.0d0
              GC_Udirect_flux(iw,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,IB,dir) = 0.0d0
            END IF
          ENDIF
          IF( do_stokes_ad20 ) THEN
            DO UM = 1, VLIDORT_ModIn%MUserVal%TS_N_USER_VZANGLES   
              DO UA = 1, VLIDORT_ModIn%MUserVal%TS_N_USER_RELAZMS
                  V = VLIDORT_Out%Main%TS_VZA_OFFSETS(IB,UM) + UA
                  GC_Qvalues(iw,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,V,dir) = &
                      VLIDORT_Out%Main%TS_STOKES(1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,V,2,dir)
                  GC_Uvalues(iw,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,V,dir) = &
                      VLIDORT_Out%Main%TS_STOKES(1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,V,3,dir)
              ENDDO
            ENDDO
          ENDIF
       END DO
    END IF
    
  END SUBROUTINE archive_spectrum_diag
  

END MODULE GC_diag_spectrum_module