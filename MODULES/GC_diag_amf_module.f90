!------------------------------------------------------------------------------
!          Harvard University Atmospheric Chemistry Modeling Group            !
!------------------------------------------------------------------------------
!BOP
!
! !MODULE: GC_diag_amf_module.f90
!
! !DESCRIPTION: This module contains routines for writing AMF diagnostics
!\\
!\\
! !INTERFACE: 
!
MODULE GC_diag_amf_module

    USE GC_parameters_module
    USE GC_variables_module, ONLY : Diag01_InpOpt
    
    IMPLICIT NONE
    INCLUDE 'netcdf.inc'
    
    CONTAINS
    
    ! ======================================================
    ! (1) AMF Diagnostics
    ! ======================================================
    SUBROUTINE diag_01( NCID, ACTION, DO_XY )
      
      USE GC_netcdf_module, ONLY    : create_ncvar, write_ncvar, &
                                      match_names_in_dimlist 
      USE GC_variables_module, ONLY : didx, GC_Scattering_weights, &
                                      gas_partialcolumns
      
      ! ACTION
      !   1 - Create NetCDF variable
      !   2 - Compute/Write variable
      
      ! ---------------------------
      ! Subroutine arguments
      ! ---------------------------
      INTEGER, INTENT(IN) :: NCID
      INTEGER, INTENT(IN) :: ACTION
      LOGICAL, INTENT(IN) :: DO_XY
      
      ! ---------------
      ! Local variables
      ! ---------------
      INTEGER :: VAR_ID, N, tot_size, g, ilev, iw, igas, ig
      LOGICAL :: error, dummy_err
      CHARACTER(LEN=max_ch_len) :: fld_name
      CHARACTER(LEN=max_ch_len) :: dim_3d(3), dim_4d(4)
      INTEGER                   :: size_3d(3), size_4d(4), tmp_3d(3), tmp_4d(4)
      
      REAL(KIND=8), ALLOCATABLE, DIMENSION(:,:,:) :: AMF
      
      ! =====================================================
      ! diag_01 starts here
      ! =====================================================
      
      ! Scattering weight nc dimensions
      dim_4d(1) = 'nw' ; dim_4d(2) = 'nlayer' ; dim_4d(3) = 'ngeom' ; dim_4d(4) = 'noutputlevel'
      
      ! AMF nc dimensions
      dim_3d(1) = 'nw' ; dim_3d(2) = 'ngeom' ; dim_3d(3) = 'noutputlevel'
      
      ! Create NetCDF variables
      IF( ACTION == 1 ) THEN
        
        ! Scattering weights
        IF( Diag01_InpOpt%output_scatterwt ) THEN
          CALL create_ncvar( NCID,    'scatweights', dim_4d,         4, &
                            ncfloat,          DO_XY, VAR_ID,     ERROR  )
        ENDIF
        
        
        ! Fields for AMF
        DO N=1,Diag01_InpOpt%n_amf
          
          fld_name = TRIM(ADJUSTL(Diag01_InpOpt%amf_spc(N))) // '_' // 'AMF'
          
          CALL create_ncvar(   NCID, TRIM(ADJUSTL(fld_name)), dim_3d,     3, &
                            ncfloat,                   DO_XY, VAR_ID, ERROR  )
          
        ENDDO
      
      ! Create and write diagnostics
      ELSEIF( ACTION == 2 ) THEN
        
        ! Get the dimensions
        CALL match_names_in_dimlist( dim_4d, 4, tmp_4d, size_4d, dummy_err )
        
        ! Compute total size
        tot_size = SIZE( GC_Scattering_Weights(:,:,1:size_4d(3),1:size_4d(4),didx) )
        
        ! Write the field
        CALL write_ncvar( NCID, 'scatweights',(/1,1,1,1/), size_4d, 4,                    &
                          tot_size, ncfloat, DO_XY, ERROR,                                &
              DATA_r8=REAL(RESHAPE( GC_Scattering_Weights(:,:,1:size_4d(3),1:size_4d(4),didx),(/tot_size/)),KIND=8) )
        
        ! Allocate array for AMF computation if needed
      IF( Diag01_InpOpt%n_amf > 0 ) THEN
        
        ! Get dimensions
        CALL match_names_in_dimlist( dim_3d, 3, tmp_3d, size_3d, dummy_err )
        
        ! Allocate AMF array
        ALLOCATE( AMF(size_3d(1),size_3d(2),size_3d(3)) )
        
        ! Loop over gases
        
        DO igas = 1,Diag01_InpOpt%n_amf
          
          ! Gas index
          g = Diag01_InpOpt%amf_spc_idx(igas)
          
          ! Compute AMF
          DO iw   = 1, size_3d(1) ! Wavelengths
          DO ig   = 1, size_3d(2) ! Geometries
          DO ilev = 1, size_3d(3) ! User output levels
            
            AMF(iw,ig,ilev) = SUM(  GC_Scattering_weights(iw, 1:size_4d(2), ilev, ig, didx)     &
                                  * gas_partialcolumns(1:size_4d(2),g)                      ) / &
                              SUM( gas_partialcolumns(1:size_4d(2),g) )
            
          ENDDO
          ENDDO
          ENDDO
          
          ! AMF field name
          fld_name = TRIM(ADJUSTL(Diag01_InpOpt%amf_spc(igas))) // '_' // 'AMF'
          
          ! Total # elements
          tot_size = SIZE( AMF )
          
          ! Write AMF
          CALL write_ncvar( NCID, TRIM(ADJUSTL(fld_name)),(/1,1,1/), size_3d, 3, &
                            tot_size, ncfloat, DO_XY, ERROR,                     &
                            DATA_r8=REAL(RESHAPE( AMF ,(/tot_size/)),KIND=8)     )
          
        ENDDO
        
        
        IF(ALLOCATED(AMF)) DEALLOCATE( AMF )
        
      ENDIF
        
        
      ELSE
        print*,'diag 01: No action for case ',ACTION
        STOP
      ENDIF
    END SUBROUTINE diag_01
    
    SUBROUTINE archive_amf_diag( iw, dir )
      
      USE GC_variables_module, ONLY : GC_scattering_weights, VLIDORT_Out, VLIDORT_FixIn, &
                                      VLIDORT_LinOut, GC_AMFs,total_gasabs,GC_radiances, &
                                      ngases,gas_partialcolumns, gaswfidx, do_jacobians, &
                                      do_AMF_calculation
      
      ! ---------------------------
      ! Subroutine arguments
      ! ---------------------------
      INTEGER, INTENT(IN) :: iw, dir
      
      ! ---------------
      ! Local variables
      ! ---------------
      INTEGER :: v, n, g, q, ilev
      
      ! =====================================================
      ! archive_amf_diag starts here
      ! =====================================================
      
      q = gaswfidx
      
      IF( do_jacobians .AND. do_AMF_calculation ) THEN
      
        DO v = 1, VLIDORT_Out%Main%TS_N_GEOMETRIES
          
          ! Compute scattering weights at wavelength w
          DO n = 1, VLIDORT_FixIn%Cont%TS_NLAYERS
            GC_Scattering_Weights(iw, n, 1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS, v,dir) = &
              -VLIDORT_LinOut%Prof%TS_PROFILEWF(q,n,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,1,dir) &
              / total_gasabs(n) / VLIDORT_Out%Main%TS_STOKES(1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,V,1,dir)
          ENDDO
              
          ! ================================================================================================
          ! CCM REMOVE-AFTER-OLD-DIAG-DEPRICATION
          ! ================================================================================================
          DO g = 1, ngases
            DO ilev = 1, VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS
              GC_AMFs(iw, ilev, v, g,dir) = &
              SUM(GC_Scattering_weights(iw, 1:VLIDORT_FixIn%Cont%TS_NLAYERS, ilev, v, dir) &
              *gas_partialcolumns(1:VLIDORT_FixIn%Cont%TS_NLAYERS,g)) /       &
              SUM(gas_partialcolumns(1:VLIDORT_FixIn%Cont%TS_NLAYERS,g))
            ENDDO
          ENDDO
          ! ================================================================================================
        ENDDO
      
      ENDIF
      
    END SUBROUTINE archive_amf_diag
    
    
END MODULE GC_diag_amf_module