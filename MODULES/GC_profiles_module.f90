MODULE GC_profiles_module

  USE GC_parameters_module, ONLY: pmaxl, pmaxc, maxaer,max_ch_len,maxcld,&
                                  GC_maxlayers, maxgases, maxlambdas,maxcld,&
                                  maxmoms, maxgksec,maxflambdas
  USE GC_variables_module,  ONLY: GC_nlayers, profile_data_filename, &
                                  profile_data, footprint_data,      &
                                  profids, profile_is_level, pnc,    &
                                  messages, nmessages,               &
                                  use_footprint_info, year, month,   &
                                  day, utc, longitude, latitude,     &
                                  GC_n_sun_positions,                &
                                  GC_sun_positions, GC_n_view_angles,&
                                  GC_sun_azimuths,                   &
                                  GC_view_angles, GC_n_azimuths,     &
                                  GC_azimuths, wind_speed, is_ocean, &
                                  use_lambertian, cfrac, cld_tops,   &
                                  ngases, which_gases, heights,      &
                                  temperatures, pressures,           &
                                  aircolumns, daircolumns_dT,        &
                                  gas_partialcolumns,                &
                                  gas_totalcolumns, do_aerosols,     &
                                  use_aerprof, naer0, aer_types,     &
                                  aer_profile0, GC_TerrainHeight,    &
                                  clon, clat, dem_fname,             &
                                  GC_do_terrain_adjust,              &
                                  DEM_interp_opt, alt_dem, lon_dem,  &
                                  lat_dem, nx_dem, ny_dem,           &
                                  rh_profile, n_cpix, cpix_cfrac,    &
                                  do_npix_cloud, cfrac, julday,      &
                                  day_of_year,profile_data_ncfile,   &
                                  yn_ascii_profile, wind_dir,        &
                                  chlorophyll_conc,salinity,         &
                                  prof_snowfrac, prof_snowdepth,     &
                                  prof_seaicefrac, prof_snowage,     &
                                  ltype_frac,yn_inr_corner,          &
                                  sif_734nm, do_clouds,naer,         &
                                  cld_uppers_npix,cld_lowers_npix,   &
                                  cld_prof_npix,cld_types,ncld,      &
                                  cpix_pcld,use_cldprof,mid_heights, &
                                  mid_temperatures,mid_pressures,    &
                                  cld_bots,cld_tops,cld_flags,       &
                                  cld_lowers,cld_uppers,cld_taus,    &
                                  cld_profile,tcld_profile,             &
                                  cld_relqext,cld_phfcn,aer_flags,      &
                                  tcldtau0,cld_opdeps,cld_ssalbs,       &
                                  aer_profile,aer_d_profile_dtau,       &
                                  aer_d_profile_dpkh,aer_d_profile_dhfw,&
                                  aer_d_profile_drel,taer_profile,      &
                                  aer_opdeps,aer_ssalbs,aer_relqext,    &
                                  aer_phfcn,taertau0,ground_ler,        &
                                  gas_xsecs,o3c1_xsecs,o3c2_xsecs,      &
                                  Rayleigh_xsecs,Rayleigh_depols,       &
                                  o2_depols,n2_depols,epsilon_air,      &
                                  o2_rrs_xsecs,n2_rrs_xsecs,nflambdas,  &
                                  nlambdas,sif_spc,do_sif,opdeps,ssalbs,&
                                  GasAbsXS,do_earth_bulk_comp,          &
                                  n_bulk_gas,bulk_gas_mixr,bulk_gas_mw, &
                                  bulk_gas_name,bulk_gas_ri_name,grav_accel, &
                                  prof_obsalt,do_T_jacobians,&
                                  write_gas_xsecs,write_xsec_deriv,&
                                  gas_xsecs_deriv, &
                                  GC_sun_positions, GC_view_angles, &
                                  GC_azimuths, GC_sun_azimuths, &
                                  do_sat_viewcalc, geometry_data,&
                                  hour, minute, second, satalt, &
                                  satlat, satlon, tmzn
                                  

  USE GC_error_module

  IMPLICIT NONE
  
CONTAINS
  
  SUBROUTINE prepare_profiles(error)

    USE GC_utilities_module, ONLY : satellite_angles
    USE GC_time_module,      ONLY : GET_DAY_OF_YEAR
    USE GC_netcdf_module,    ONLY : read_xy_nc_prof
    USE GC_variables_module, ONLY : do_diag, ad09, &
                                    ad10,    ad11, &
                                    ad14,    ad15, skip_xy_pixel
    IMPLICIT NONE
    
    LOGICAL, INTENT(INOUT)        :: error
    
    ! ---------------
    ! Local variables
    ! ---------------
    REAL(KIND=4), DIMENSION(1000) :: tmp_cf, tmp_pcld
    INTEGER                       :: n
    
    ! ====================================================================
    ! prepare_profiles starts here
    ! ====================================================================

    ! ------------------------
    ! Reading the profile file
    ! ------------------------
    
    IF( yn_ascii_profile ) THEN
    
      CALL geocape_profile_reader_1                  &
          ( profile_data_filename, pmaxl, pmaxc,     & ! input
            max_ch_len,                              & 
            profile_data, prof_snowfrac,             &
            footprint_data, profids,                 & ! Output
            profile_is_level,GC_nlayers, pnc, tmp_cf,  & ! Output
            tmp_pcld,messages(nmessages+1), error   )               ! Exception handling
      
      ! Not currently implemented
      prof_snowage    = 0.0
      prof_seaicefrac = 0.0
      prof_snowdepth  = 0.0
      ltype_frac(:)   = 0.0
      sif_734nm       = 0.0
      prof_obsalt     = 0.0
      
    ELSE
      
      
      ! XY NetCDF
      CALL read_xy_nc_prof                              &
             ( profile_data_ncfile, pmaxl, pmaxc,       & ! input
               max_ch_len,yn_inr_corner,                & 
               profile_data, prof_snowfrac,             & ! Output
               prof_snowdepth, prof_seaicefrac,         &
               prof_snowage, ltype_frac,                &
               sif_734nm,                               &
               footprint_data, profids,                 & ! Output
               profile_is_level,GC_nlayers, pnc, tmp_cf,& ! Output
               tmp_pcld, prof_obsalt,                   &
               messages(nmessages+1), error )
      
      IF( skip_xy_pixel ) RETURN
      
!       
      !STOP 'Stopping after profile read while testing'
      
    ENDIF
    
    ! ----------------------------------------------
    ! Override footprint input information if needed
    ! ----------------------------------------------
    IF (use_footprint_info) THEN
       
       year      = INT(footprint_data(1))
       month     = INT(footprint_data(2))
       day       = INT(footprint_data(3))
       utc       =     footprint_data(4)
       longitude = footprint_data(5)
       
       ! Also compute day of year from y/m/d
       day_of_year = GET_DAY_OF_YEAR(month, day, year, 0, 0, 0.0d0)
       
       IF (longitude .GT. 180.d0) longitude = longitude-360.d0  
       latitude  = footprint_data(6)
       GC_n_sun_positions  = 1
       GC_sun_positions(1) = footprint_data(7)
       GC_sun_azimuths     = footprint_data(8)
       GC_n_view_angles    = 1
       GC_view_angles(1)   = footprint_data(9)
       GC_n_azimuths       = 1
       GC_azimuths(1)      = footprint_data(10)
       wind_speed          = footprint_data(14)
       wind_dir            = footprint_data(13)
       
       ! Terrain Height
       GC_TerrainHeight = footprint_data(17)
       
       ! Pixel corner longitudes and latitudes
       clon(:) = footprint_data(18:21)
       clat(:) = footprint_data(22:25)
       
       chlorophyll_conc = footprint_data(26)
       salinity         = footprint_data(27)
       
    ENDIF
    
    ! Update cloud information if we are taking it from the profile
    IF( do_clouds .AND. use_cldprof ) THEN
    
      ! Must use footprint data for n cloud profile code
      n_cpix = INT( footprint_data(28) )
      
      IF( n_cpix .GT. 0 ) THEN
    
        ! Allocate cloud fraction array
        ALLOCATE( cpix_cfrac(n_cpix) )
        ALLOCATE( cpix_pcld(n_cpix) )
        
        ! save cloud fractions
        cpix_cfrac(1:n_cpix) = tmp_cf(1:n_cpix)
        cpix_pcld(1:n_cpix)  = tmp_pcld(1:n_cpix)
        
        ! Compute total cloud fraction
        cfrac = SUM( cpix_cfrac(1:n_cpix) )
        
      ELSE
        
        ! If there are no clouds then set values to zero
        cfrac = 0.0
        ALLOCATE(cpix_cfrac(1))
        cpix_cfrac(1) = 0.0
        
      ENDIF
      
    ENDIF
    
!     
    ! ------------------
    ! Exception handling
    ! ------------------
    IF (error) CALL write_err_message (.FALSE., messages(nmessages+1))
    
    ! -------------------------
    ! Terrain height adjustment
    ! -------------------------
    IF( GC_do_terrain_adjust ) THEN
      
      CALL terrain_height_adjust( error )
      
    ENDIF
    
    ! ----------------------------------
    ! Compute hour/min/sec from UTC hour
    ! ----------------------------------
    hour   = INT(utc)
    minute = INT((utc - hour) * 60.d0)
    second = (utc - hour - minute / 60.d0) * 3600.d0

    ! ---------------------------
    ! Recompute Viewing Geometry
    ! ---------------------------
    IF (do_sat_viewcalc) THEN

      tmzn          = 0.0
      geometry_data = satellite_angles(year, month, day, hour, minute,    &
                                       second, tmzn, latitude, longitude, &
                                       satlat, satlon, satalt)
      
      GC_n_sun_positions  = 1
      GC_n_view_angles    = 1
      GC_n_azimuths       = 1
      GC_sun_positions(1) = geometry_data(4)
      GC_view_angles(1)   = geometry_data(2)
      GC_azimuths(1)      = geometry_data(1)-(geometry_data(3) + 180.0)
      GC_sun_azimuths(1)  = geometry_data(3) ! Used in SEA Glint calc
      
      ! Vlidort requires relative azimuth bounded by [0,360)
      IF (GC_azimuths(1) .LT.   0.d0) GC_azimuths(1) = GC_azimuths(1)+360.d0
      IF (GC_azimuths(1) .GE. 360.d0) GC_azimuths(1) = GC_azimuths(1)-360.d0
      
    ENDIF

    ! ---------------------------
    ! Allocate Met Profile arrays
    ! ---------------------------
    ALLOCATE(heights(0:GC_nlayers)) ! 0:GC_maxlayers
    ALLOCATE(pressures(0:GC_nlayers)) ! 0:GC_maxlayers
    ALLOCATE(temperatures(0:GC_nlayers)) ! 0:GC_maxlayers
    ALLOCATE(mid_heights(GC_nlayers)) ! 0:GC_maxlayers
    ALLOCATE(mid_pressures(GC_nlayers)) ! 0:GC_maxlayers
    ALLOCATE(mid_temperatures(GC_nlayers)) ! 0:GC_maxlayers
    ALLOCATE(aircolumns(GC_nlayers)) ! GC_maxlayers
    ALLOCATE(daircolumns_dT(GC_nlayers)) ! GC_maxlayers
    ALLOCATE(rh_profile(GC_nlayers)) ! GC_maxlayers
    
    ! ---------------------------
    ! Allocate gas profile arrays
    ! ---------------------------
    ALLOCATE(gas_partialcolumns(GC_nlayers,ngases)) ! GC_nlayers,ngases
    ALLOCATE(gas_totalcolumns(ngases)) ! ngases
    
    ! The new cross section type
    DO n=1,ngases
       
       ! Case 1 - Constant cross section
       IF     ( GasAbsXS(n)%XSectTypeID == 1 ) THEN
          IF(.NOT. ALLOCATED(GasAbsXS(n)%Xsect)) ALLOCATE(GasAbsXS(n)%Xsect(nlambdas,1) )
          GasAbsXS(n)%NXsect = 1
          ! Case 2 - quadratic temperature param. 
       ELSE IF( GasAbsXS(n)%XSectTypeID == 2 ) THEN
          IF(.NOT. ALLOCATED(GasAbsXS(n)%Xsect))  ALLOCATE(GasAbsXS(n)%Xsect(nlambdas,3) )
          GasAbsXS(n)%NXsect = 3
          ! Case 3 - HITRAN -Online calc
       ELSE IF( GasAbsXS(n)%XSectTypeID == 3) THEN
          IF(.NOT. ALLOCATED(GasAbsXS(n)%Xsect))  ALLOCATE(GasAbsXS(n)%Xsect(nlambdas,GC_nlayers) )
          GasAbsXS(n)%NXsect = GC_nlayers
          IF( do_T_jacobians .AND. .NOT. ALLOCATED(GasAbsXS(n)%dXsectdT) ) ALLOCATE(GasAbsXS(n)%dXsectdT(nlambdas,GC_nlayers))
          ! Case 4 - Linear temperature parameterization
       ELSE IF( GasAbsXS(n)%XSectTypeID == 4 ) THEN
          IF(.NOT. ALLOCATED(GasAbsXS(n)%Xsect))  ALLOCATE(GasAbsXS(n)%Xsect(nlambdas,2) )
          GasAbsXS(n)%NXsect = 2
          ! Case 5 - HITRAN LUT
       ELSE IF( GasAbsXS(n)%XSectTypeID == 5 ) THEN
          IF(.NOT. ALLOCATED(GasAbsXS(n)%Xsect))  ALLOCATE(GasAbsXS(n)%Xsect(nlambdas,GC_nlayers) )
          GasAbsXS(n)%NXsect = GC_nlayers
          IF( do_T_jacobians .AND. .NOT. ALLOCATED(GasAbsXS(n)%dXsectdT) ) ALLOCATE(GasAbsXS(n)%dXsectdT(nlambdas,GC_nlayers))
          ! Case 6 - HITRAN LUT (preconvolved)
       ELSE IF( GasAbsXS(n)%XSectTypeID == 6 ) THEN
          IF(.NOT. ALLOCATED(GasAbsXS(n)%Xsect))  ALLOCATE(GasAbsXS(n)%Xsect(nlambdas,GC_nlayers) )
          GasAbsXS(n)%NXsect = GC_nlayers
          IF( do_T_jacobians .AND. .NOT. ALLOCATED(GasAbsXS(n)%dXsectdT) ) ALLOCATE(GasAbsXS(n)%dXsectdT(nlambdas,GC_nlayers))
       ELSE
          print*,'Invalid cross section type ID for ',&
               TRIM(ADJUSTL(GasAbsXS(n)%Name))
          STOP
       ENDIF
       
    ENDDO
    
    ! And some of the optical property arrays
    ALLOCATE(gas_xsecs(nlambdas,GC_nlayers,ngases))
    ALLOCATE(o3c1_xsecs(nlambdas))
    ALLOCATE(o3c2_xsecs(nlambdas))
    ALLOCATE(Rayleigh_xsecs(nlambdas))
    ALLOCATE(Rayleigh_depols(nlambdas))
    ALLOCATE( epsilon_air(nlambdas) )
    
    IF(write_xsec_deriv .AND. write_gas_xsecs ) THEN
      ALLOCATE(gas_xsecs_deriv(nlambdas,GC_nlayers,ngases))
      gas_xsecs_deriv = 0.0d0
    ENDIF
    
    ! -----------------------------
    ! Allocate Cloud profile arrays
    ! -----------------------------
    IF( ncld > 0 ) THEN
       ALLOCATE(cld_bots(ncld))
       ALLOCATE(cld_tops(ncld))
       ALLOCATE(cld_taus(ncld))
       ALLOCATE(cld_flags(GC_nlayers))
       ALLOCATE(cld_lowers(ncld))
       ALLOCATE(cld_uppers(ncld))
       ALLOCATE(cld_opdeps(nlambdas,GC_nlayers))
       ALLOCATE(cld_ssalbs(nlambdas,GC_nlayers))
       ALLOCATE(cld_profile(ncld,GC_nlayers))
       ALLOCATE(tcld_profile(GC_nlayers))
       ALLOCATE(cld_relqext(nlambdas,GC_nlayers))
       ALLOCATE(cld_phfcn(GC_nlayers, 0:maxmoms, maxgksec))
       
       ! Initialize some variables
       cld_profile = 0.0d0; tcld_profile = 0.0d0; tcldtau0 = 0.0d0
       cld_flags          = .FALSE.
       
       ! Arrays for average pixel diagnostics
       ! ------------------------------------
       
       ! Average cloud profile
       IF( do_diag(9) ) THEN
         ALLOCATE( ad09(GC_nlayers) )
         ad09(:) = 0.0d0
       ENDIF
       
       ! Total COD
       IF( do_diag(14) ) THEN
         ALLOCATE( ad14(nlambdas,GC_nlayers) )
         ad14(:,:) = 0.0d0
       ENDIF
       
       ! Cloud SSA
       IF( do_diag(15) ) THEN
         ALLOCATE( ad15(nlambdas,GC_nlayers) )
         ad15(:,:) = 0.0d0
       ENDIF
       
    ENDIF
    
    
    
    ! -------------------------------
    ! Allocate Aerosol profile arrays
    ! -------------------------------
    IF( naer > 0 ) THEN
       ALLOCATE( aer_profile(naer,GC_nlayers) )
       ALLOCATE( aer_d_profile_dtau(naer,GC_nlayers) )
       ALLOCATE( aer_d_profile_dpkh(naer,GC_nlayers) )
       ALLOCATE( aer_d_profile_dhfw(naer,GC_nlayers) )
       ALLOCATE( aer_d_profile_drel(naer,GC_nlayers) )
       ALLOCATE( aer_profile0(naer,GC_nlayers) )
       ALLOCATE( taer_profile(GC_nlayers) )
       ALLOCATE( aer_flags(GC_nlayers) )
       ALLOCATE( aer_opdeps(nlambdas,GC_nlayers) )
       ALLOCATE( aer_ssalbs(nlambdas,GC_nlayers) )
       ALLOCATE( aer_relqext(nlambdas,GC_nlayers))
       ALLOCATE( aer_phfcn(GC_nlayers,0:maxmoms,maxgksec) )
       
       ! Initialize some variables
       aer_profile = 0.0d0; taer_profile = 0.0d0; taertau0 = 0.0d0
       aer_flags = .FALSE.
       
    ENDIF
    
    ! ---------------------------------
    ! Allocate other optical properties
    ! ---------------------------------
    ALLOCATE( opdeps(nlambdas,GC_nlayers) )
    ALLOCATE( ssalbs(nlambdas,GC_nlayers) )
    
    ! Pixel average diagnostics
    IF( do_diag(10) .AND. .NOT. ALLOCATED(ad10) ) THEN
      ALLOCATE( ad10(nlambdas,GC_nlayers) )
      ad10(:,:) = 0.0d0
    ENDIF
    
    IF( do_diag(11) .AND. .NOT. ALLOCATED(ad11)  ) THEN
      ALLOCATE( ad11(nlambdas,GC_nlayers) )
      ad11(:,:) = 0.0d0
    ENDIF
    
    ! -----------------------
    ! Allocate Surface arrays
    ! -----------------------
    ALLOCATE( ground_ler(nlambdas) ) ! nlambdas(nclambdas?)
    
    IF( do_sif ) THEN
       ALLOCATE( sif_spc(nlambdas) )
    ENDIF
    
    ! Call the new matching routine
    CALL match_metgasprof_from_atmosprof()
    
    ! ------------------
    ! Exception handling
    ! ------------------
    IF (error) CALL write_err_message (.FALSE., messages(nmessages+1))
    
    ! ------------------------------------------------------------------
    ! Read aerosol profile before modifying the altitude grids by clouds
    ! ------------------------------------------------------------------
    IF (do_aerosols .AND. use_aerprof .AND. naer > 0) THEN
       CALL match_aerprof_from_atmosprof( )
    ELSE
       naer0 = 0 !aer_profile0 = 0.0d0
    ENDIF
    
  END SUBROUTINE prepare_profiles
  
  SUBROUTINE match_metgasprof_from_atmosprof()
    
    IMPLICIT NONE
    
    ! ---------------
    ! Local variables
    ! ---------------
    INTEGER :: n, n1, g, c, s
    INTEGER :: ngas_tot, ngas_check, nsearch
    ! Array for data fields
    CHARACTER(LEN=4), dimension(0:pmaxc) :: datanames  ! datanames(0) should not be used
    REAL(KIND=8)                         :: lay_mw, mr_tot, lay_dp2cm2
    REAL(KIND=8)                         :: temp, delp, col
    REAL(KIND=8), PARAMETER              :: N_avo = 6.022140857d23 ! Avogadros Number
    
    CHARACTER(LEN=4),     DIMENSION(2)   :: gasname
    REAL(KIND=8), ALLOCATABLE, DIMENSION(:,:) :: gasprof
    LOGICAL, DIMENSION(2)                :: search_for_gas
    ! Dobson units to CM2
    REAL(KIND=8), PARAMETER ::  DU_TO_CM2 = 2.68668D16
    
    ! ==================================================================================
    ! match_metgasprof_from_atmosprof starts here
    ! ==================================================================================
    
    ! Reverse grid from TOA -> Surface
    DO n = 0, GC_nlayers
      n1 = GC_nlayers - n + 1
      temperatures(n) = profile_data(n1,3)
      pressures(n) = profile_data(n1,1)
      heights(n) = profile_data(n1, 2)
    ENDDO
    
    ! Compute Midpoint values
    mid_pressures(1:GC_nlayers)    = EXP((LOG(pressures(0:GC_nlayers-1)) + &
                                     LOG(pressures(1:GC_nlayers)))/2.0)
    mid_temperatures(1:GC_nlayers) = (temperatures(0:GC_nlayers-1) +       &
                                      temperatures(1:GC_nlayers))/2.0
    mid_heights(1:GC_nlayers)      = (heights(0:GC_nlayers-1) +            &
                                      heights(1:GC_nlayers))/2.0


    ! Parse profile fields 
    read (profids, *) datanames(0:pnc)
    
    ! Set RH profile
    rh_profile(:) = 0.0d0 ! Assume 0 if not there
    DO g=1,pnc
      IF( TRIM(ADJUSTL(datanames(g))) .EQ. 'RH') THEN
        DO n=1,GC_nlayers
          n1 = GC_nlayers - n + 1
          rh_profile(n) = profile_data(n1,g)
        ENDDO
      ENDIF
    ENDDO
    
    ! -----------------------------------------------------------
    ! This will need to be moved inside next loop if this changes
    ! -----------------------------------------------------------
    
    ! Molecular weight for layer  [Kg/mol]
    lay_mw = 0.0d0
    mr_tot = 0.0d0
    DO g=1,n_bulk_gas
      lay_mw = lay_mw + bulk_gas_mw(g)*bulk_gas_mixr(g)
      mr_tot = mr_tot + bulk_gas_mixr(g)
    ENDDO
    lay_mw = lay_mw / mr_tot * 1.0d-3
    
    ! Conversion from pressure difference [hPa] to molec/cm2
    lay_dp2cm2 = N_avo / grav_accel / lay_mw * 1.0d-2
    
    ! -----------------------------------------------------------
    ! -----------------------------------------------------------
    
    ! Compute air density
    DO n=1, GC_nlayers
      
      n1 = n - 1
      
      ! Mean layer temperature
      temp = 0.5d0 * (temperatures(n1)+temperatures(n))
      
      ! Layer pressure difference [hPa]
      delp = pressures(n) - pressures(n1)
      
      ! Air column
      aircolumns(n) = delp * lay_dp2cm2
      
      ! Column temperature derivative
      daircolumns_dT(n) = - aircolumns(n) / temp
      
    ENDDO
    
    ! Set gas columns
    ! -------------------------------------------------
    
    ! Counter for gases found
    ngas_check = 0
    ngas_tot   = 0
    
    ! To allow for CIA
    ALLOCATE( gasprof( GC_nlayers,2) )
    
    DO g=1, ngases
      
      ! Flag to continue searching
      search_for_gas = .TRUE.
      
      ! Decide search gas names
      IF( GasAbsXS(g)%is_CIA ) THEN
        gasname(1) = GasAbsXS(g)%CIA_Name1
        gasname(2) = GasAbsXS(g)%CIA_Name2
        nsearch    = 2
        search_for_gas(1) = .TRUE.
        search_for_gas(2) = .TRUE.
        ngas_tot   = ngas_tot + nsearch
      ELSE
        gasname(1) = GasAbsXS(g)%Name
        gasname(2) = '####'
        nsearch    = 1
        ngas_tot   = ngas_tot + nsearch
        search_for_gas(1) = .TRUE.
        search_for_gas(2) = .FALSE.
      ENDIF
      
      
      ! First see if gas is in profile variables
        
      ! Loop over profile
      DO c=1,pnc
        DO s=1,nsearch
          
          ! Check for gas
          IF( TRIM(ADJUSTL(datanames(c))) .EQ. TRIM(ADJUSTL(gasname(s))) ) THEN
            
            ! Set profile if gas is found
            DO n=1,GC_nlayers
              n1 = GC_nlayers + 1 - n
              IF( profile_is_level ) THEN
                gasprof(n,s) = aircolumns(n)*(profile_data(n1,c)+profile_data(n1+1,c))/2.0d0
              ELSE
                gasprof(n,s) = aircolumns(n)*profile_data(n1,c)
              ENDIF
            ENDDO
            
            ! Increment counter
            ngas_check = ngas_check + 1
            
            ! Search for gas is now false
            search_for_gas(s) = .FALSE.
            
          ENDIF
          
        ENDDO
      ENDDO
        
      ! If we are still searching for the gas then try bulk gases
      IF( search_for_gas(1) .OR. search_for_gas(2) ) THEN
        
        DO c=1, n_bulk_gas
          DO s=1, nsearch
          
            IF( TRIM(ADJUSTL(bulk_gas_name(c))) .EQ. TRIM(ADJUSTL(gasname(s))) ) THEN
              ! Set profile if gas found
              DO n=1,GC_nlayers
                gasprof(n,s) = aircolumns(n)*bulk_gas_mixr(c)
              ENDDO
              
              print*,TRIM(ADJUSTL(gasname(s)))
              
              ! Increment counter
              ngas_check = ngas_check + 1
              
            ENDIF
            
          ENDDO
        ENDDO
          
      ENDIF
      
      ! Set gas partial column
      IF( GasAbsXS(g)%is_CIA ) THEN
        DO n=1,GC_nlayers
          gas_partialcolumns(n,g) = gasprof(n,1)*gasprof(n,2)/&
                                    (heights(n-1)-heights(n))/1.0D5
        ENDDO
      ELSE
        gas_partialcolumns(:,g) = gasprof(:,1)
      ENDIF
      
    ENDDO
    
    IF ( ngas_check .ne. ngas_tot ) THEN
      STOP 'Not all desired trace gases are present in data set: Reduce choice!'
    ENDIF
    
    ! Check gases > 0
!     DO g = 1, ngases
!     DO n = 1, GC_nlayers
!       IF(gas_partialcolumns(n,g).lt.0.0d0) gas_partialcolumns(n,g)=0.0d0
!     ENDDO
!     ENDDO
    
    ! Total columns in DU
    DO g = 1, ngases
      col = 0.0d0
      DO n = 1, GC_nlayers
         col = col + gas_partialcolumns(n,g)
      ENDDO
      gas_totalcolumns(g) = col / du_to_cm2
    ENDDO
    
    ! Deallocate array
    IF( ALLOCATED(gasprof) ) DEALLOCATE(gasprof)
    
    
    
  END SUBROUTINE match_metgasprof_from_atmosprof
  
  SUBROUTINE match_aerprof_from_atmosprof( )
    
    IMPLICIT NONE
    
    ! Local variables
    INTEGER :: i, j, n, n1
    CHARACTER(LEN=max_ch_len), DIMENSION(0:pmaxc)   :: datanames  ! datanames(0) should not be used
    
    ! ==================================================================================
    ! match_aerprof_from_atmosprof starts here
    ! ==================================================================================
    
    ! parse profile fields: datanames(1:pnc) corresponds to profile_data(:, 1:pnc)
    read (profids, *) datanames(0:pnc)
    
    ! Initialize varaibles
    naer0 = 0
    aer_profile0 = 0.0d0
    
    DO i= 1, pnc ! Profile columns
      
      ! Loop over types
      DO j=1,naer
        
        IF( TRIM(ADJUSTL(datanames(i))) .EQ.    &
            'O' // TRIM(ADJUSTL(aer_types(j))) ) THEN
          
          ! Increment count of found aerosols
          naer0 = naer0 + 1
          
          DO n = 1, GC_nlayers
            n1 = GC_nlayers + 1 - n
            aer_profile0(j, n) = profile_data(n1, i)
          ENDDO
          
        ENDIF
        
      ENDDO
      
    ENDDO
    
    IF( naer0 .ne. naer ) THEN
      
      CALL write_err_message ( .TRUE.,                                &
           "ERROR: Could not find all Aerosol fields in profile file" )
      
    ENDIF
    
  END SUBROUTINE match_aerprof_from_atmosprof
  
  SUBROUTINE match_cldprof_from_atmosprof(  )
    
    IMPLICIT NONE
    
    ! Local variables
    INTEGER :: i, j, k, n, n1
    CHARACTER(LEN=max_ch_len), DIMENSION(0:pmaxc)   :: datanames  ! datanames(0) should not be used
    character(len=5)                      :: pix_str
    character(len=max_ch_len)             :: col_name
    ! ==================================================================================
    ! match_cldprof_from_atmosprof starts here
    ! ==================================================================================
    
    ! parse profile fields: datanames(1:pnc) corresponds to profile_data(:, 1:pnc)
    read (profids, *) datanames(0:pnc)
    
    ! Allocate output arrays
    allocate(cld_prof_npix(ncld,GC_nlayers,n_cpix))
    allocate(cld_lowers_npix(ncld,n_cpix))
    allocate(cld_uppers_npix(ncld,n_cpix))
    
    ! Initialize varaibles
    cld_prof_npix(:,:,:) = 0.0
    cld_lowers_npix(:,:) = 0.0
    cld_uppers_npix(:,:) = 0.0
    
    print*,'ncld:',ncld
    
    ! Loop over cloud types and pixels
    DO i=1,ncld
    DO j=1,n_cpix
      
      ! Pixel string
      WRITE( pix_str, '(I5)' ) j
      
      ! Field name
      col_name = 'O' // TRIM(ADJUSTL(cld_types(i))) // ADJUSTL(TRIM(pix_str))
      
      ! Search for cloud name
      DO k=1,pnc
        
        IF( TRIM(ADJUSTL(col_name)) .EQ. TRIM(ADJUSTL(datanames(k))) ) THEN
          
          print*,TRIM(ADJUSTL(col_name)) // ' found'
          
          ! Store cloud profile
          do n = 1, GC_nlayers
              n1 = GC_nlayers + 1 - n
              cld_prof_npix(i, n, j) = profile_data(n1, k)
          enddo
          
          print*,cld_prof_npix(i,:,j) 
          
          ! Find cld_uppers, cld_lowers
          do n = 1, GC_nlayers
            if (cld_prof_npix(i, n, j) .gt. 0.0d0) then 
              cld_uppers_npix(i,j) = n
              exit
            endif
          enddo
            
          do n = GC_nlayers, 1, -1
            if (cld_prof_npix(i, n, j) .gt. 0.0d0) then 
              cld_lowers_npix(i,j) = n + 1
              exit
            endif
          enddo
          
        ENDIF
        
      ENDDO
      
    ENDDO
    ENDDO
    
  END SUBROUTINE match_cldprof_from_atmosprof
  
  LOGICAL FUNCTION PNPOLY( nvert, vertx, verty, testx, testy)
    
    IMPLICIT NONE
    
    ! Return variable
    ! ---------------
    !LOGICAL :: PNPOLY ! True if point is in polygon
    
    ! Input
    INTEGER :: nvert ! # Vertices in polygon
    REAL(KIND=4), DIMENSION(nvert) :: vertx ! X coordinates of the polygon vertices
    REAL(KIND=4), DIMENSION(nvert) :: verty ! Y coordinates of the polygon vertices
    REAL(KIND=4)                   :: testx ! X coordinate of test point
    REAL(KIND=4)                   :: testy ! Y coordinate of test point
    
    ! Local variables
    INTEGER :: i, j
    
    ! ================================================
    ! FUNCTION PNPOLY starts here
    ! ================================================
    
    ! Initialize PNPOLY
    PNPOLY = .FALSE.
    
    ! Initialize j
    j = nvert
    
    ! Perform crossings test (Jordan curve thm.) to see if point is in polygon
    DO i=1,nvert
      
      IF ( ((verty(i) .gt. testy) .neqv. (verty(j) .gt. testy)) .and. &
          (testx .lt. (vertx(j)-vertx(i)) * &
          (testy-verty(i)) / (verty(j)-verty(i)) + vertx(i)) ) THEN
         
         PNPOLY = .NOT. PNPOLY
         
      ENDIF
      
      j = i
      
    ENDDO
    
    RETURN
    
  END FUNCTION PNPOLY
  
  REAL(KIND=8) FUNCTION NN_SAMP_DEM( X, Y)
    
    ! Return Variable
    ! ---------------
    !REAL(KIND=8) :: NN_SAMP_DEM
    
    ! Input variables
    ! ---------------
    REAL(KIND=8), INTENT(IN) :: X, Y ! Location of point
    REAL(KIND=8) :: dx, dy
    ! Local variables
    INTEGER :: xid, yid
    
    ! ================================================
    ! FUNCTION NN_SAMP_DEM starts here
    ! ================================================
    
    ! Determine resolution of DEM
    dx = lon_dem(2)-lon_dem(1)
    dy = lat_dem(2)-lat_dem(1)
    
    ! Determine location
    xid = nint( (x-(lon_dem(1)-dx/2.0))/dx )+1
    yid = nint( (y-(lat_dem(1)-dy/2.0))/dy )+1
    
    ! Return value
    NN_SAMP_DEM = REAL(alt_dem(xid,yid),KIND=8)
    
    RETURN
    
  END FUNCTION NN_SAMP_DEM
  
  SUBROUTINE PIECELIN_INTERP(X,Y,U,V, NX, NU)
    
    ! Piecewise linear interpolation
    ! Assumes x is already sorted in ascending order
    
    ! Input arguments
    INTEGER,       INTENT(IN)                :: NX, NU
    REAL(KIND=8),  INTENT(IN), DIMENSION(NX) :: X,Y
    REAL(KIND=8),  INTENT(IN), DIMENSION(0:NU) :: U
    
    ! Output argument
    REAL(KIND=8), INTENT(OUT), DIMENSION(0:NU) :: V
    
    ! Local variables
    REAL(KIND=8), DIMENSION(NX-1) :: delta
    INTEGER,      DIMENSION(0:NU)   :: k
    INTEGER                       :: i, j
    REAL(KIND=8)                  :: s
    
    
    ! =================================================
    ! SUBROUTINE PIECELIN_INTERP
    ! =================================================
    
    ! Compute gradient
    DO i=1,nx-1
      delta(i) = (y(i+1)-y(i))/(x(i+1)-x(i))
    ENDDO
    
    ! Find subinterval indices
    k(:) = 1
    
    ! Get subintervals
    DO i=2,nx-1
      
      DO j=0,nu
        IF( x(i) .LE. u(j) ) k(j) = i
      ENDDO
    ENDDO
    
    ! Evaluate interpolant
    DO j=0,nu
      s = u(j) - x(k(j))
      v(j) = y(k(j)) + s*delta(k(j))
    ENDDO
    
    RETURN
    
  END SUBROUTINE PIECELIN_INTERP
  
  SUBROUTINE terrain_height_adjust( error )
    
    USE GC_netcdf_module, ONLY: netcdf_rd_dem
    IMPLICIT NONE
    
    !  Arguments
    ! ----------------
    LOGICAL, INTENT(INOUT)            :: error
!     CHARACTER(Len=200),   INTENT(OUT) :: message
    
    ! Local Variables
    ! ---------------
    
    ! Netcdf 
    CHARACTER(len=256) :: ncmessage
    LOGICAL            :: ncfail
    
    character(len=6), dimension(0:pmaxc)   :: datanames  ! datanames(0) should not be used
    character(len=4)                       :: gasname
    ! Eta pressure levels
    real(kind=4), dimension ( 0:GC_nlayers )  :: ap, bp, tedge0, pedge0, zedge0
    real(kind=8), dimension ( 0:GC_nlayers )  :: tedge, pedge, zedge
!     real(kind=8), dimension ( 1:GC_nlayers )  :: RH_new
    real(kind=8)                              :: p_h2o, p_sat, x0_h2o, p0_sat, Tc, x_h2o
    
    real(kind=8), dimension(1:GC_nlayers+2) :: P_int, T_int, z_int, lnP_rev, Z_rev
    integer                                 :: n_int
    
    ! To store the old profile
    REAL(kind=8), DIMENSION(pmaxl,pmaxc) :: profile_data0
    
    
    INTEGER                       :: i, j, l, n,  ii
    INTEGER(kind=4), DIMENSION(4) :: si, sj
    INTEGER(kind=4)               :: si_min, si_max, sj_min, sj_max, nsx
    INTEGER(kind=4)               :: ct, alt_sum
    REAL(kind=8)                  :: dx, dy, alt, p_eff, T_eff
    REAL(kind=4),    DIMENSION(4) :: vx, vy
    REAL(kind=4)                  :: testx, testy
    
    ! Parameters
    REAL(kind=8), PARAMETER :: g = 9.81d0 ! Grav Accel. (m/s)
    REAL(kind=8), PARAMETER :: Gamma = 6.5d0 ! Environmental lapse rate (K/km)
    REAL(kind=8), PARAMETER :: Rm = 287.0d0 ! Gas constant (J/kg/K)
    
    ! =================================================
    ! SUBROUTINE terrain_height_adjust starts here
    ! =================================================
    
    ! Store the old profile data
    profile_data0 = profile_data
    
    ! Read the digital elevation map
    CALL netcdf_rd_dem( dem_fname, ncmessage, ncfail)
    
    ! Set error
    IF( ncfail ) THEN
      error = .TRUE.
!       message = 'terrain_height_adjust: DEM Failed to load'
    ENDIF
    
    ! parse profile fields: datanames(1:pnc) corresponds to profile_data(:, 1:pnc)
    read (profids, *) datanames(0:pnc)
    
    ! Read the Eta coordinate system
    ap(:) = 0.0
    bp(:) = 0.0
    DO i = 1, pnc
      ! Eta-A coordinate
      IF ( TRIM(datanames(i)) == 'EtaA' ) THEN
        DO l=0,GC_nlayers
          ap(l) = REAL(profile_data0(l+1,i),KIND=4)
        ENDDO
      ENDIF
      
      ! Eta-B coordinate
      IF ( TRIM(datanames(i)) == 'EtaB' ) THEN
        DO l=0,GC_nlayers
          bp(l) = REAL(profile_data0(l+1,i),KIND=4)
        ENDDO
      ENDIF
      
    ENDDO
    
    ! Store profile p/T/Z
    DO n = 0, GC_nlayers
      Tedge0(n) = REAL(profile_data0(n+1,3),KIND=4)
      pedge0(n) = REAL(profile_data0(n+1,1),KIND=4)
      zedge0(n) = REAL(profile_data0(n+1,2),KIND=4)
    ENDDO
    
    ! ====================================================
    ! Compute altitude from DEM for given satellite pixel
    ! ====================================================
    
    SELECT CASE( DEM_interp_opt )
    
      CASE( 1 ) ! Nearest Neigbour sampling
        
        alt = NN_SAMP_DEM( longitude, latitude )*1e-3
        
      CASE DEFAULT ! Mean of points within footprint
        
        ! Determine resolution of DEM
        dx = lon_dem(2)-lon_dem(1)
        dy = lat_dem(2)-lat_dem(1)
        
        ! Indices for footprint corners
        DO n=1,4
          si(n) = floor( (clon(n)-(lon_dem(1)-dx/2.0))/dx )+1
          sj(n) = floor( (clat(n)-(lat_dem(1)-dy/2.0))/dy )+1
        ENDDO
        
        ! The limits for the rectangular box around the footprint
        si_min = minval(si)
        si_max = maxval(si)+1
        sj_min = minval(sj)
        sj_max = maxval(sj)+1
    !     print*,si_min,si_max,sj_min,sj_max
        
        ! Number of lon indices in subsetted region
        nsx = si_max - si_min + 1
        
        ! Copy vertices as these are modified
        DO n=1,4
          vx(n) = REAL(clon(n),KIND=4)
          vy(n) = REAL(clat(n),KIND=4)
        ENDDO
        
        ! Initialize count and altitude sum
        ct = 0
        alt_sum = 0
        
        ! Check if the limits cross the discontinuity @ 180 degrees
        IF(maxval(vx)-minval(vx) .GT. 180.0) THEN
          DO n=1,4
            IF(vx(n) .LT. 0.) vx(n)=vx(n)+360.0
          ENDDO
        ENDIF
        
        ! Can't be over 90 degrees
        IF(sj_max .gt. ny_dem) sj_max = ny_dem
        
        DO i=si_min,si_max
        DO j=sj_min,sj_max
          
          ! Check if point crosses 180E
          IF(i .gt. nx_dem) THEN
            ii = i-nx_dem
          ELSE
            ii = i
          ENDIF
          
          ! Set test longitude/latitude
          testx = lon_dem(ii) 
          testy = lat_dem(j)
          
          ! If we are crossing 180E and test lon is on the other side
          IF(maxval(vx) .gt. 180.0 .and. testx .lt. 0.0) THEN
            testx = testx + 360.0
          ENDIF
          
          ! Add point if it is in polygon
          IF( PNPOLY(4,vx,vy,testx,testy) ) THEN
            
            ! Increment count
            ct = ct + 1
            
            ! Sum altitude
            alt_sum = alt_sum + alt_dem(ii,j)
            
          ENDIF
          
        ENDDO
        ENDDO
        
        ! Complete average
        alt = 0.0
        IF(ct .gt. 0) THEN
          alt =  REAL(alt_sum,KIND=8)/REAL(ct,KIND=8)*1.0d-3
        ELSE
          ! Nearest point if none overlap
          alt = NN_SAMP_DEM( longitude, latitude )*1e-3
        ENDIF
        
    END SELECT
    
    !print*,'Alt(new,old):',alt,GC_TerrainHeight
    
    ! Deallocate DEM arrays
    IF(allocated(lon_dem)) deallocate(lon_dem)
    IF(allocated(lat_dem)) deallocate(lat_dem)
    IF(allocated(alt_dem)) deallocate(alt_dem)
    
    ! ================================================
    ! Adjust P,T,z Grid
    ! ================================================
    
    ! Effective pressure and temperature
    p_eff = pedge0(0) * (tedge0(0)/(tedge0(0)+Gamma*(GC_TerrainHeight-alt)))**&
                       (-1.0*g/(Rm*Gamma*1e-3))
    T_eff = tedge0(0) + Gamma*(GC_TerrainHeight-alt)
    
    ! The new pressure edges
    ! --------------------------------------------------------
    
    DO l=0,GC_nlayers 
      ! Compute pressure edges
      pedge(l) = ap(l) + bp(l)*p_eff
      
      ! Store in profile data array
      profile_data(l+1,1) = pedge(l)
      
    ENDDO
    
    ! Height [km]
    ! --------------------------------------------------------
    Z_int(1) = alt
    P_int(1) = p_eff
    n_int = 1
    
    ! Construct interpolation profile
    DO l=0,GC_nlayers
      
      IF( pedge0(l) .LT. P_int(n_int) .AND. &
          zedge0(l) .GE. Z_int(n_int) ) THEN
        
        ! Increase count
        n_int = n_int+1
        
        ! Store p and T
        P_int(n_int) = pedge0(l)
        Z_int(n_int) = zedge0(l)
        
        !print*,P_int(n_int),Z_int(n_int),l
      ENDIF
      
    ENDDO
    
    ! Need to reverse the order
    DO l=1,n_int
      n = n_int-l+1
      lnP_rev(n) = log(P_int(l))
      Z_rev(n) = Z_int(l)
    ENDDO
    
    ! Do the interpolation (in log(p) space)
    CALL PIECELIN_INTERP( lnP_rev(1:n_int), Z_rev(1:n_int),&
                          log(pedge), zedge, n_int, GC_nlayers  )
    
    ! Store heights
    DO l=0,GC_nlayers
      profile_data(l+1,2) = zedge(l)
      !print*,l,zedge(l)
    ENDDO
    
    ! Temperature [K]
    ! --------------------------------------------------------
    
    ! Interpolate as function of z
    
    ! Initialize interpolation array
    T_int(1) = T_eff
    Z_int(1) = zedge(0)
    n_int = 1
    
    ! Construct interpolation profile
    DO l=0,GC_nlayers
      
      IF( zedge0(l) .GT. Z_int(n_int) ) THEN
        ! Increase count
        n_int = n_int+1
        
        ! Store p and T
        Z_int(n_int) = zedge0(l)
        T_int(n_int) = tedge0(l)
        
      ENDIF
      
    ENDDO
    
    ! Do the interpolation (in log(p) space)
    CALL PIECELIN_INTERP( Z_int(1:n_int), T_int(1:n_int),&
                          zedge, tedge, n_int, GC_nlayers  )
    
    ! Store temperatures
    DO l=0,GC_nlayers
      profile_data(l+1,3) = tedge(l)
      !print*,l,tedge(l),tedge0(l)
    ENDDO
    
    ! Trace gases [v/v] - Keep partial columns in same layers
    ! --------------------------------------------------------
    
    DO n = 1, ngases
      
      ! Get one gas
      gasname = which_gases(n)
      
      ! Loop over data fields
      DO i = 1, pnc
         
         ! If we find a gas then redistribute profile
         IF ( TRIM(datanames(i)) == TRIM(gasname) ) THEN
            
            DO l=1,GC_nlayers
              profile_data(l,i) = profile_data0(l,i)*&
                                  ( pedge(l-1) -  pedge(l))/&
                                  (pedge0(l-1) - pedge0(l))
            ENDDO
         ENDIF
         
      ENDDO
      
   ENDDO
   
    ! Relative Humidity [%]
    ! --------------------------------------------------------
    
      DO i = 1, pnc
         
         ! Find RH index (can't rely on H2O field existing)
         IF ( TRIM(datanames(i)) == 'RH' ) THEN
            
            DO l=1,GC_nlayers
              
              ! Temperature [C]
              Tc = 0.5*(tedge0(l-1)+tedge0(l))-273.15
              
              ! Saturation vapor pressure [hPa] (Old profile)
              p0_sat =  6.1121*exp( (18.678-Tc/234.5)*((Tc)/(257.14+Tc))) 
              
              ! H2O mixing ratio (Old profile)
              x0_h2o =  profile_data0(l,i)*1e-2*p0_sat/&
                        (0.5*(pedge0(l-1)+pedge0(l)))
              
              ! H2O mixing ratio (new profile)
              x_h2o = x0_h2o*( pedge(l-1) -  pedge(l))/&
                             (pedge0(l-1) - pedge0(l))
              
              p_h2o = x_h2o*0.5*(pedge(l-1)+pedge(l))
              
              ! Temperature [C]
              Tc = 0.5*(tedge(l-1)+tedge(l))-273.15
              
              ! Saturation vapor pressure (new profile)
              p_sat = 6.1121*exp( (18.678-Tc/234.5)*((Tc)/(257.14+Tc)))
              
              ! Relative humidity (new profile)
              profile_data(l,i) = p_h2o / p_sat * 1e2
              
            ENDDO
            
         ENDIF
         
      ENDDO
    
    ! Clouds (Same OD per layer for now)
    ! --------------------------------------------------------
    
    
    ! Aerosols - Same OD per layer
    ! --------------------------------------------------------
    ! (Just don't overwrite)
    
    
  END SUBROUTINE terrain_height_adjust

  ! Read generic atmospheric profiles 
  ! Here is the typical header of the data, lines marked with *** will be read
  ! The line below the End_of_Headers stores the standard ID for each column
  ! Here are the list of 30 recognizable standard IDs (no space inside an ID)
  !    Lev: level or layer
  !    PRESSURE: hPa
  !    ALTITUDE: km   (not used)
  !    TATM:     K
  !    O3, NO2, HCHO, SO2, H2O, GLYX, BRO, OCLO, IO, CO, CO2, N2O, CH4, O2, NO, HNO3, OCS (VMR)
  !    OSU, OBC, OOC, OSF, OSC, ODU, OWC, OIC, RH
  ! Note the input profiles must meet the following conditions:
  ! 1. From surface to TOA
  ! 2. The first 4 fields got to be Level, pressure, altitude, and TATM in order(always at level)
  !    The number of fields and the order after TATM do not matter
  ! 3. The units for gases is VMR
  ! 4. The units for aerosol, cloud optical depth is dimensionless
  ! 5. If the data is specified for layer, Gas is for layer mean VMR between l and l + 1
  ! 6. Aerosol and cloud data is always specified at each layer
  
  !Geos-Chem, GEOS-5, 2.5x2, Hourly data
  !Data_Size = 48 x 21
  !ProfileIsLevel = 0
  !YYYY_MM_DD_HH = 2007 07 15 19
  !Lon_Lat =   -97.50  38.00
  !SZA_VZA_AZA =    17.2902   44.0700  147.4352
  !SurfaceTemperature(K) =   303.3730
  !SurfacePressure(hPa) =   961.8620
  !IsOcean =  0
  !WindSpeed(m/s) =     2.9121
  !CloudFraction =     0.1065
  !CloudTopPressure =   607.1039
  !TerrainHeight(m) = 0.0
  ! cLon = -78.0 -77.0 -77.0 -78.0
  ! cLat = 37.5 37.5 38.5 38.5
  ! NumberOfCloudPixels = 4
  ! CloudPixelFractions = 0.2 0.2 0.2 0.2
  !****** End_Of_Header ******
  !Lev Pressure Altitude   TATM   EtaA EtaB    O3         NO2       HCHO        SO2       GLYX        H2O        BRO       OCLO       OSU        OBC        OOC       OSF       OSC       ODU       OWC       OIC        RH    
  !      hPa      km        K                 VMR        VMR        VMR        VMR        VMR        VMR        VMR        VMR                                                                                            % 
  subroutine geocape_profile_reader_1 &
       ( filename, maxl, maxc, max_ch_len,                                              &          ! input
       profile_data, snowfrac, footprint_data, profids, profile_is_level, GC_nlayers, pnc,      &
       pixel_cf, pixel_pcld, &
       message, fail ) ! output
    
    !  inptu filename
    character(len=*),                intent(in)  :: filename
    
    !  Main output
    integer, intent(in)                             :: maxl, maxc, max_ch_len
    integer,                        intent(out)     :: GC_nlayers, pnc
    real(kind=8), dimension(maxl,maxc), intent(out) :: profile_data
    real(kind=8),                    intent(out)    :: snowfrac
    real(kind=8), dimension(28),     intent(out)    :: footprint_data
    real(kind=4), dimension(1000),    intent(out)    :: pixel_cf, pixel_pcld
    logical, intent(out)                            :: profile_is_level  !T: level  F: layer
    character(len=max_ch_len), intent(out)          :: profids
    
    !  Exception handling
    logical,       intent(INOUT) :: fail
    character*(*), intent(INOUT) :: message
    
    !  Local variables
    integer      :: i, j, nr, nc, np, itmp
    real(kind=8), dimension(maxl,maxc+1) :: tmp_data
    character(LEN=80) :: dummy
    
    !  initialize
    fail    = .false.
    message = ' '
    do i = 1, maxl
       do j = 1,maxc
          profile_data(i,j) = -999.
       enddo
    enddo
    
    
    np = 1
    do while (filename(np:np).ne. ' ')
       np = np + 1 
    enddo
    np = np - 1
    
    !  Open and read file    
    open(1,file=filename(1:np),err=90, status='old')
    read(1,*)
    read(1,*) dummy,dummy,nr,dummy,nc
    if (nr .ge. maxl .or. nc .ge. maxc+1) then
       message = 'Need to increase maxl or maxc in geocape_profile_reader_1'
       fail = .true.
       return
    endif
    read(1,*) dummy, dummy, itmp
    if (itmp .eq. 0) then
       profile_is_level = .FALSE.
    else
       profile_is_level = .TRUE.
    endif
    
    ! Set cloud fractions
    pixel_cf(:) = 0.0
    
    read(1,*) dummy, dummy, footprint_data(1:4)  ! Year, mon, day, utc
    read(1,*) dummy, dummy, footprint_data(5:6)  ! longitude, latitude
    read(1,*) dummy, dummy, footprint_data(7:10) ! SZA, SAA, VZA, AZA
    read(1,*) dummy, dummy, footprint_data(11)   ! Surface temperature
    read(1,*) dummy, dummy, footprint_data(12)   ! Surface pressure
    read(1,*) dummy, dummy, footprint_data(13)   ! Wind direction
    read(1,*) dummy, dummy, footprint_data(14)   ! Wind speed
    read(1,*) dummy, dummy, footprint_data(15)   ! Cloud fraction     
    read(1,*) dummy, dummy, footprint_data(16)   ! Cloud top pressure (for Lambertian clouds) ! This is now redundant 
    read(1,*) dummy, dummy, footprint_data(17)   ! Terrain Height of GC grid box (m)
    read(1,*) dummy, dummy, footprint_data(18:21)! Pixel Corner Longitudes 
    read(1,*) dummy, dummy, footprint_data(22:25)! Pixel Corner Latitudes
    read(1,*) dummy, dummy, footprint_data(26)   ! Chlorophyll Concentration
    read(1,*) dummy, dummy, footprint_data(27)   ! Ocean Salinity
    read(1,*) dummy, dummy, footprint_data(28)   ! Number of cloud pixels
    IF( INT(footprint_data(28)) .EQ. 0 ) THEN
       read(1,*) dummy
       read(1,*) dummy
    ELSE
       read(1,*) dummy, dummy, pixel_cf(1:INT(footprint_data(28))) ! Geometric Cloud fractions for cloud pixels
       read(1,*) dummy, dummy, pixel_pcld(1:INT(footprint_data(28))) ! Cloud Pressures for lambertian case
    ENDIF
    
    print*,footprint_data(27),pixel_cf(1)
    
    ! Read snow fraction
    read(1,*) dummy, dummy, snowfrac
    IF( snowfrac > 1.0 ) snowfrac = 1.0d0
    IF( snowfrac < 0.0 ) snowfrac = 0.0d0
    
    ! End of header line
    read(1,'(A)') dummy
    
    read(1,'(A)') profids
    read(1,*)
    
    print*,profids
    
    do i = 1, nr
       !      print*,i,nc
       read(1,*) (tmp_data(i,j),j=1,nc)
       do j = 1,3
          profile_data(i,j) = tmp_data(i,j+1)
       enddo
       do j = 4,nc-1
          profile_data(i,j) = tmp_data(i,j+1)
       enddo
    enddo
    
    GC_nlayers = nr-1
    pnc = nc - 1
    
    close(1)
    
    return
    
    ! error return
90  continue
    fail = .true.
    message = 'Open failure for profile file = '//filename(1:LEN(filename))
    return
    
  end subroutine geocape_profile_reader_1

  
END MODULE GC_profiles_module
