!------------------------------------------------------------------------------
!          Harvard University Atmospheric Chemistry Modeling Group            !
!------------------------------------------------------------------------------
!BOP
!
! !MODULE: GC_read_input_module.f90
!
! !DESCRIPTION: This module declares variables that control the GEOCAPE tool and
!               contains subroutines to read them or derive them from the 
!               previously readed variables. The control file name has to be
!               supplied when calling the main program.
!\\
!\\
! !INTERFACE: 
!
MODULE GC_read_input_module
!
! !USES
  USE GC_parameters_module, ONLY: max_ch_len, maxlambdas, maxflambdas, maxmoms,       &
                                  ctrunit,maxcld,maxaer, maxgases, BRDF_CHECK_NAMES
  USE GC_variables_module,  ONLY: database_dir, results_dir, profile_data_filename,   &
                                  debug_filename, albspectra_fname, aerfile, cldfile, &
                                  do_aerosols, do_clouds, do_lambertian_cld,          &
                                  use_aerprof, use_cldprof,                           &
                                  do_StokesQU_output, idix,                           &
                                  do_AMF_calculation, do_T_Jacobians,                 &
                                  do_sfcprs_Jacobians, do_normalized_WFoutput,        &
                                  do_normalized_radiance,                             &
                                  GC_n_user_altitudes,                                &
                                  GC_user_altitudes, GC_do_user_altitudes,            &
                                  lambda_start, lambda_finish, use_wavelength,        &
                                  do_effcrs, lambda_resolution, lambda_dw,            &
                                  lambda_dfw, nlambdas, lambdas, wavnums, nflambdas,  &
                                  nclambdas, edge_dw, flambdas, fwavnums, clambdas,   &
                                  cwavnums, use_footprint_info, do_debug_geocape_tool,&
                                  use_solar_photons, ngases, which_gases,             &
                                  use_lambertian, do_brdf_surface, use_fixedalbedo,   &
                                  use_albspectra, fixed_albedo, is_ocean, wavlens_um, &
                                  wind_speed, water_rn, water_cn, do_sat_viewcalc,    &
                                  year, month, day, latitude, longitude, satlon,      &
                                  satlat, satalt, utc, geometry_data,                 &
                                  solar_spec_filename, GC_TerrainHeight,              &
                                  GC_do_terrain_adjust, dem_fname, clon, clat,        &
                                  DEM_interp_opt, day_of_year, julday, use_albeofs,   &
                                  do_npix_cloud, do_eof_brdf, yn_ascii_profile,       &
                                  profile_data_ncfile,    vlidort_control_file,       &
                                  vlidort_vbrdf_control_file, nc_xids, nc_yids,       &
                                  GC_sun_positions,GC_view_angles,GC_azimuths,        &
                                  GC_n_sun_positions, GC_n_view_angles, GC_n_azimuths,&
                                  do_ocean_glint, chlorophyll_conc, gas_nxsecs,       &
                                  gas_xsecs_type,xsec_data_filenames, do_aer_columnwf,&
                                  do_aod_Jacobians, do_assa_Jacobians,                &
                                  do_aerph_Jacobians,do_aerhw_Jacobians,aer_reflambda,&
                                  use_aerprof, naer, aer_types, aer_tau0s,            &
                                  aer_z_lowerlimit, aer_z_upperlimit,aer_z_peakheight,&
                                  aer_half_width, cld_types, cld_tops, cld_bots,      &
                                  cld_taus, cfrac, cld_reflambda, do_cfrac_jacobians, &
                                  do_cld_columnwf, do_cod_jacobians,                  &
                                  do_cssa_jacobians, do_ctp_jacobians, ncld,          &
                                  lambertian_cldalb, xsec_data_path, use_zcldspec,    &
                                  do_fixed_brdf, salinity, fkern_name, fkern_idx,     &
                                  fkern_amp, fkern_npar, fkern_par,                   &
                                  write_view_geom, write_footprint_info,              &
                                  write_altitude_prof,write_pres_prof,write_temp_prof,&
                                  write_aircol_prof,write_aircol_prof,write_aod_prof, &
                                  write_assa_prof,write_cod_prof,write_cssa_prof,     &
                                  write_gas_prof,write_gas_names,write_wvl_grid,      &
                                  write_rad_spc,write_flx_spc,write_dflx_spc,         &
                                  write_irad_spc,write_alb_spc,write_fix_brdf_par,    &
                                  write_full_brdf,write_modis_eof_par,                &
                                  do_surfalb_jacobian,do_wspd_jacobian,               &
                                  do_gas_jacobians,write_aod_refwvl,write_cod_refwvl, &
                                  write_od_prof,write_ssa_prof, do_xy_nc_file_lock,   &
                                  overwrite_existing_xy_nc,                           &
                                  do_output_as_xy_nc, create_xy_nc_only, wind_dir,    &
                                  GC_sun_azimuths, prof_snowfrac ,do_raman_jacobian,  &
                                  yn_inr_corner, do_rrs_abs, do_rrs_phasefunc,        &
                                  do_sif, use_ler_climatology, ler_clim_file,         &
                                  do_ler_const_wvl, ler_clim_wvl, write_gas_xsecs,    &
                                  aer_data_filenames, yn_aer_rhdep, aer_proftype,     &
                                  aer_relaxation,cld_data_filenames,modis_fa_alb_type,&
                                  cld_tops_npix,cld_bots_npix,cld_opdeps_npix,        &
                                  cpix_cfrac,cpix_pcld,n_cpix,                        &
                                  cld_tau0s,cld_proftype,cld_z_lowerlimit,            &
                                  cld_z_upperlimit,cld_z_peakheight,cld_half_width,   &
                                  cld_relaxation,GasAbsXS,do_earth_bulk_comp,         &
                                  n_bulk_gas,bulk_gas_mixr,bulk_gas_mw,bulk_gas_name, &
                                  bulk_gas_ri_name,grav_accel, do_hitran, fa_infile,  &
                                  do_yn_obsalt,do_diag, write_sif, diag25_write_kpar, &
                                  do_oco2_sfcprs,do_total_aod_jac, do_total_assa_jac, &
                                  SVL_full_stokes_calc,SVL_n_stokes,SVL_do_debug,     &
                                  SVL_do_thermal_emiss,SVL_do_surf_emiss,SVL_nstreams,&
                                  SVL_nmoments,SVL_do_upwelling,SVL_do_downwelling,   &
                                  write_totaod_refwvl,write_xsec_deriv,do_airdens_jac,&
                                  use_brdf_clim, brdf_clim_infile, &
                                  use_cmglint, do_cm_facet_iso, do_cm_whitecap,  &
                                  do_cm_shadow

  USE VLIDORT_PARS,        ONLY : MAX_BRDF_PARAMETERS
  USE GC_error_module
! 
  IMPLICIT NONE
!
! !PUBLIC MEMBER FUNCTIONS:
!
  PUBLIC :: read_control_file
  PUBLIC :: check_input
  PUBLIC :: skip_to_filemark
!
! !PUBLIC DATA MEMBERS:
!

!
! !REVISION HISTORY:
!  April 2013 - G. Gonzalez Abad - Initial Version
!
!EOP
!------------------------------------------------------------------------------
  
  CONTAINS


    SUBROUTINE READ_INPUT_FILE( error )
      
      
      LOGICAL,       INTENT(INOUT) :: error ! Error variable
      
      
      ! ---------------
      ! Input file name
      ! ---------------
      CHARACTER(LEN=max_ch_len) :: input_filename
      INTEGER                   :: funit
      
      ! ---------------
      ! Temporal string
      ! ---------------
      CHARACTER(LEN=1)   :: TAB   = ACHAR(9)
      CHARACTER(LEN=1)   :: SPACE = ' '
      CHARACTER(LEN=max_ch_len) :: LINE
      
      ! ------
      ! Errors
      ! ------
      INTEGER     :: ios
      LOGICAL     :: EOF
      
      ! ----------------
      ! Code starts here
      ! ----------------
      error = .FALSE.
      
      ! -----------------
      ! Control file unit
      ! -----------------
      funit = ctrunit
      
      ! ---------------------------------------------------
      ! The tool will spect to get the filename as argument
      ! ---------------------------------------------------
      CALL GETARG(1, input_filename)
      
      ! -----------------
      ! Open control file
      ! -----------------
      OPEN (UNIT=funit, FILE=TRIM(ADJUSTL(input_filename)), ACTION='READ', STATUS='OLD', IOSTAT=ios )
      IF ( ios /= 0 ) THEN
        STOP 'ERROR: Unable to open input file' 
      END IF
      
      ! ----------------------------------
      ! Initialize cloud and aerosol flags
      ! ----------------------------------
      do_cld_columnwf    = .FALSE.
      do_cod_Jacobians   = .FALSE.
      do_cssa_Jacobians  = .FALSE.
      do_cfrac_Jacobians = .FALSE.
      do_ctp_Jacobians   = .FALSE.
      do_aer_columnwf    = .FALSE.
      do_aod_Jacobians   = .FALSE.
      do_assa_Jacobians  = .FALSE.
      do_aerph_Jacobians = .FALSE.
      do_aerhw_Jacobians = .FALSE.

      ! Loop over files
      DO 
      
        ! Read a line from the file, exit if EOF
        LINE = READ_ONE_LINE( funit, EOF ) 
        IF ( EOF ) EXIT
         
        ! Replace tab characters in LINE (if any) w/ spaces
        CALL STRREPL( LINE, TAB, SPACE )
        
        IF       ( INDEX( LINE, '%%% SIMULATION MENU %%%'   ) > 0 ) THEN
          CALL READ_SIMULATION_MENU( funit, error )
        ELSE IF ( INDEX( LINE, '%%% VLIDORT OPTIONS %%%'    ) > 0 ) THEN
          CALL READ_VLIDORT_OPTIONS( funit, error )
        ELSE IF ( INDEX( LINE, '%%% PROFILE OPTIONS %%%'    ) > 0 ) THEN
          CALL READ_PROFILE_OPTIONS( funit, error )
        ELSE IF ( INDEX( LINE, '%%% SPECTRUM OPTIONS %%%'   ) > 0 ) THEN
          CALL READ_SPECTRUM_OPTIONS( funit, error )
        ELSE IF ( INDEX( LINE, '%%% SURFACE OPTIONS %%%'    ) > 0 ) THEN
          CALL READ_SURFACE_OPTIONS( funit, error )
        ELSE IF ( INDEX( LINE, '%%% TRACE GAS OPTIONS %%%'  ) > 0 ) THEN
          CALL READ_TRACE_GAS_OPTIONS( funit, error )
        ELSE IF ( INDEX( LINE, '%%% AEROSOL OPTIONS %%%'    ) > 0 ) THEN
          CALL READ_AEROSOL_OPTIONS( funit, error )
        ELSE IF ( INDEX( LINE, '%%% CLOUD OPTIONS %%%'      ) > 0 ) THEN
          CALL READ_CLOUD_OPTIONS( funit, error )
        ELSE IF ( INDEX( LINE, '%%% DIAGNOSTIC OPTIONS %%%' ) > 0 ) THEN
          CALL READ_DIAGNOSTIC_OPTIONS( funit, error )
        ELSE IF ( INDEX( LINE, 'END OF FILE'                ) > 0 ) THEN 
          EXIT
        ENDIF  
        
      ENDDO
      
      ! Close input file
      CLOSE( funit )
      
    END SUBROUTINE READ_INPUT_FILE
    
    SUBROUTINE READ_SIMULATION_MENU( funit, error )
      
      INTEGER, INTENT(IN)          :: funit
      LOGICAL,       INTENT(INOUT) :: error ! Error variable
      
      CHARACTER(LEN=max_ch_len) :: SUBSTRS(max_ch_len)
      INTEGER            :: N
      
      error = .FALSE.
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_simulation_menu:1' )
      READ( SUBSTRS(1:N), '(A)' ) results_dir
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_simulation_menu:2' )
      READ( SUBSTRS(1:N), * ) yn_ascii_profile
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_simulation_menu:3' )
      READ( SUBSTRS(1:N), '(A)' ) profile_data_filename  
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_simulation_menu:4' )
      READ( SUBSTRS(1:N), '(A)' ) profile_data_ncfile  
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 2, 'read_simulation_menu:5' )
      READ( SUBSTRS(1:N), * ) nc_xids
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 2, 'read_simulation_menu:6' )
      READ( SUBSTRS(1:N), * ) nc_yids
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_simulation_menu:7' )
      READ( SUBSTRS(1:N), * ) do_output_as_xy_nc
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_simulation_menu:7' )
      READ( SUBSTRS(1:N), * ) do_xy_nc_file_lock
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_simulation_menu:7' )
      READ( SUBSTRS(1:N), * ) overwrite_existing_xy_nc
      
      ! If using ASCII / Not outputing to xync we can overwrite the existing nc file
      IF( yn_ascii_profile .OR. do_output_as_xy_nc .EQV. .FALSE. ) THEN
        overwrite_existing_xy_nc = .TRUE.
      ENDIF
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_simulation_menu:7' )
      READ( SUBSTRS(1:N), * ) create_xy_nc_only 
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_simulation_menu:7' )
      READ( SUBSTRS(1:N), * ) yn_inr_corner
 
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_simulation_menu:7' )
      READ( SUBSTRS(1:N), '(A)' ) database_dir
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_simulation_menu:8' )
      READ( SUBSTRS(1:N), * ) do_debug_geocape_tool
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_simulation_menu:9' )
      READ( SUBSTRS(1:N), * ) debug_filename
      
      ! ---------------------
      ! Trace gas source path
      ! ---------------------
      xsec_data_path = TRIM(ADJUSTL(database_dir)) // '/SAO_crosssections/'
      
      
      IF( yn_ascii_profile ) THEN
        
        ! Ignore the X-Y netcdf options
        do_output_as_xy_nc   = .FALSE.
        
        ! Ignore the X-Y iterator
        nc_xids(:) = 1
        nc_yids(:) = 1
        
      ENDIF
      
      IF( do_debug_geocape_tool ) THEN
      
        print*,'============================'
        print*,'%%% READ SIMULATION MENU %%%'
        print*,'============================'
        print*,'results_dir               :',TRIM(ADJUSTL(results_dir))
        print*,'yn_ascii_profile          :',yn_ascii_profile
        print*,'profile_data_filename     :',TRIM(ADJUSTL(profile_data_filename))
        print*,'profile_data_ncfile       :',TRIM(ADJUSTL(profile_data_ncfile))
        print*,'nc xids,yids              :',nc_xids,nc_yids
        print*,'database_dir              :',TRIM(ADJUSTL(database_dir))
        print*,'do_debug_geocape_tool     :',do_debug_geocape_tool
        print*,'debug_filename            :',TRIM(ADJUSTL(debug_filename))
        print*,'vlidort_control_file      :',TRIM(ADJUSTL(vlidort_control_file))
        print*,'vlidort_vbrdf_control_file:',TRIM(ADJUSTL(vlidort_vbrdf_control_file))
        print*,'create_xy_nc_only         :',create_xy_nc_only
        print*,'do_output_as_xy_nc        :',do_output_as_xy_nc
        print*,'do_inr_corner             :',yn_inr_corner 
      ENDIF
      
    END SUBROUTINE READ_SIMULATION_MENU
    
    SUBROUTINE READ_VLIDORT_OPTIONS( funit, error )
      
      INTEGER, INTENT(IN)          :: funit
      LOGICAL,       INTENT(INOUT) :: error ! Error variable
      
      CHARACTER(LEN=max_ch_len) :: SUBSTRS(max_ch_len)
      INTEGER            :: N
      
      error = .FALSE.
      
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_vlidort_options:1' )
      READ( SUBSTRS(1:N), * ) SVL_full_stokes_calc
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_vlidort_options:1' )
      READ( SUBSTRS(1:N), * ) SVL_n_stokes
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_vlidort_options:1' )
      READ( SUBSTRS(1:N), * ) SVL_do_thermal_emiss
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_vlidort_options:1' )
      READ( SUBSTRS(1:N), * ) SVL_do_surf_emiss
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_vlidort_options:1' )
      READ( SUBSTRS(1:N), * ) SVL_do_upwelling
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_vlidort_options:1' )
      READ( SUBSTRS(1:N), * ) SVL_do_downwelling
      
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_vlidort_options:1' )
      READ( SUBSTRS(1:N), * ) SVL_nstreams
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_vlidort_options:1' )
      READ( SUBSTRS(1:N), * ) SVL_nmoments
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_vlidort_options:1' )
      READ( SUBSTRS(1:N), * ) SVL_do_debug
      
      IF( do_debug_geocape_tool ) THEN

        print*,'============================'
        print*,'%%% READ VLIDORT OPTIONS %%%'
        print*,'============================'
        print*,'Full Stokes Vector Calc?        :',SVL_full_stokes_calc
        print*,'  - Number of Vector Components :',SVL_n_stokes
        print*,'Do thermal emission?            :',SVL_do_thermal_emiss
        print*,'Do surface emission?            :',SVL_do_surf_emiss
        print*,'Output upwelling radiation?     :',SVL_do_upwelling
        print*,'Output downwelling radiation?   :',SVL_do_downwelling
        print*,'# of half-space streams         :',SVL_nstreams
        print*,'# of p. func. exp. coeffs       :',SVL_nmoments
        print*,'Do Debug write?                 :',SVL_do_debug
        
      ENDIF
      
    END SUBROUTINE READ_VLIDORT_OPTIONS
    
    SUBROUTINE READ_PROFILE_OPTIONS( funit, error )
      
      USE GC_time_module, ONLY: GET_DAY_OF_YEAR
      INTEGER, INTENT(IN)          :: funit
      LOGICAL,       INTENT(INOUT) :: error ! Error variable
      
      CHARACTER(LEN=max_ch_len) :: SUBSTRS(max_ch_len)
      INTEGER            :: N, I
      
      
      error = .FALSE.
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_profile_options:1' )
      READ( SUBSTRS(1:N), * ) use_footprint_info
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_profile_options:2' )
      READ( SUBSTRS(1:N), * ) year
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_profile_options:3' )
      READ( SUBSTRS(1:N), * ) month
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_profile_options:4' )
      READ( SUBSTRS(1:N), * ) day
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_profile_options:5' )
      READ( SUBSTRS(1:N), * ) utc
      
      ! Also compute day of year from y/m/d
      day_of_year = GET_DAY_OF_YEAR(month,day,year,0,0,0.0d0)
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_profile_options:6' )
      READ( SUBSTRS(1:N), * ) longitude
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_profile_options:7' )
      READ( SUBSTRS(1:N), * ) latitude
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_profile_options:8' )
      READ( SUBSTRS(1:N), * ) GC_TerrainHeight
      
      ! Skip header line
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 4, 'read_profile_options:9' )
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 4, 'read_profile_options:10' )
      READ( SUBSTRS(1:N), * ) clon
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 4, 'read_profile_options:11' )
      READ( SUBSTRS(1:N), * ) clat
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_profile_options:12' )
      READ( SUBSTRS(1:N), * ) satlon
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_profile_options:13' )
      READ( SUBSTRS(1:N), * ) satlat
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_profile_options:14' )
      READ( SUBSTRS(1:N), * ) satalt
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_profile_options:15' )
      READ( SUBSTRS(1:N), * ) GC_sun_positions(1) ! SZA
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_profile_options:16' )
      READ( SUBSTRS(1:N), * ) GC_sun_azimuths(1) ! SAA
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_profile_options:17' )
      READ( SUBSTRS(1:N), * ) GC_view_angles(1) ! VZA
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_profile_options:18' )
      READ( SUBSTRS(1:N), * ) GC_azimuths(1) ! AZA
      
      GC_n_sun_positions = 1
      GC_n_view_angles   = 1
      GC_n_azimuths      = 1
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_surface_options:19' )
      READ( SUBSTRS(1:N), * ) wind_dir
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_surface_options:20' )
      READ( SUBSTRS(1:N), * ) wind_speed
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_surface_options:21' )
      READ( SUBSTRS(1:N), * ) chlorophyll_conc
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_surface_options:22' )
      READ( SUBSTRS(1:N), * ) salinity
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_surface_options:23' )
      READ( SUBSTRS(1:N), * ) prof_snowfrac
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_profile_options:24' )
      READ( SUBSTRS(1:N), * ) do_sat_viewcalc
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_profile_options:25' )
      READ( SUBSTRS(1:N), * ) GC_do_terrain_adjust
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_profile_options:26' )
      READ( SUBSTRS(1:N), '(A)' ) dem_fname
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_profile_options:27' )
      READ( SUBSTRS(1:N), * ) DEM_interp_opt
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_profile_options:28' )
      READ( SUBSTRS(1:N), * ) GC_do_user_altitudes
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_profile_options:29' )
      READ( SUBSTRS(1:N), * ) GC_n_user_altitudes
      
      DO I=1, GC_n_user_altitudes
        
        CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_profile_options:30' )
        READ( SUBSTRS(1:N), * ) GC_user_altitudes(I)
        
      ENDDO

      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_profile_options:31' )
      READ( SUBSTRS(1:N), * ) do_yn_obsalt
      IF (do_yn_obsalt) THEN
         GC_do_user_altitudes = .TRUE.
         GC_n_user_altitudes = 1
      END IF
      
      IF( do_debug_geocape_tool ) THEN
      
        print*,'============================'
        print*,'%%% READ PROFILE OPTIONS %%%'
        print*,'============================'
        print*,'use_footprint_info    :',use_footprint_info
        print*,'year/month/day, utc   :',year,month,day,utc
        print*,'satlon, satlat, satalt:',satlon,satlat,satalt
        print*,'clon                  :',clon
        print*,'clat                  :',clat
        print*,'do_sat_viewcalc       :',do_sat_viewcalc
        print*,'sza,saa,vza,raa       :',GC_sun_positions(1),GC_sun_azimuths(1),&
                                         GC_view_angles(1),GC_azimuths(1)
        print*,'wind_dir,wind_speed   :',wind_dir,wind_speed
        print*,'chlorophyll_conc      :',chlorophyll_conc
        print*,'salinity              :',salinity
        print*,'prof_snowfrac         :',prof_snowfrac
        print*,'GC_do_terrain_adjust  :',GC_do_terrain_adjust
        print*,'dem_fname             :',TRIM(ADJUSTL(dem_fname))
        print*,'DEM_interp_opt        :',DEM_interp_opt
        print*,'GC_do_user_altitudes  :',GC_do_user_altitudes
        print*,'GC_n_user_altitudes   :',GC_n_user_altitudes
        print*,'GC_user_altitudes     :',GC_user_altitudes(1:GC_n_user_altitudes)
        print*,'do_yn_obsalt          :',do_yn_obsalt
      
      ENDIF
      
    END SUBROUTINE READ_PROFILE_OPTIONS
    
    SUBROUTINE READ_SPECTRUM_OPTIONS( funit, error )
      
      INTEGER, INTENT(IN)          :: funit
      LOGICAL,       INTENT(INOUT) :: error ! Error variable
      
      CHARACTER(LEN=max_ch_len) :: SUBSTRS(max_ch_len)
      INTEGER            :: N
      
      
      error = .FALSE.
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_spectrum_options:1' )
      READ( SUBSTRS(1:N), * ) use_wavelength
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_spectrum_options:2' )
      READ( SUBSTRS(1:N), * ) lambda_start
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_spectrum_options:3' )
      READ( SUBSTRS(1:N), * ) lambda_finish
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_spectrum_options:4' )
      READ( SUBSTRS(1:N), * ) lambda_resolution
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_spectrum_options:5' )
      READ( SUBSTRS(1:N), * ) lambda_dw
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_spectrum_options:6' )
      READ( SUBSTRS(1:N), * ) lambda_dfw
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_spectrum_options:7' )
      READ( SUBSTRS(1:N), * ) do_normalized_radiance
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_spectrum_options:8' )
      READ( SUBSTRS(1:N), '(A)' ) solar_spec_filename
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_spectrum_options:9' )
      READ( SUBSTRS(1:N), * ) use_solar_photons
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_spectrum_options:10' )
      READ( SUBSTRS(1:N), * ) do_normalized_WFoutput
      
      
      IF( do_debug_geocape_tool ) THEN
        print*,'============================='
        print*,'%%% READ SPECTRUM OPTIONS %%%'
        print*,'============================='
        print*,'use_wavelength:',use_wavelength
        print*,'lambda_start:',lambda_start
        print*,'lambda_finish:',lambda_finish
        print*,'lambda_resolution:',lambda_resolution
        print*,'lambda_dw:',lambda_dw
        print*,'lambda_dfw:',lambda_dfw
        print*,'do_normalized_radiance:',do_normalized_radiance
        print*,'solar_spec_filename:',TRIM(ADJUSTL(solar_spec_filename))
        print*,'use_solar_photons:',use_solar_photons
        print*,'do_normalized_WFoutput:',do_normalized_WFoutput
      ENDIF
    
    END SUBROUTINE READ_SPECTRUM_OPTIONS
    
    SUBROUTINE READ_SURFACE_OPTIONS( funit, error )
      
      INTEGER, INTENT(IN)          :: funit
      LOGICAL,       INTENT(INOUT) :: error ! Error variable
      
      CHARACTER(LEN=max_ch_len) :: SUBSTRS(max_ch_len)
      INTEGER            :: N, I, J, surf_opt
      LOGICAL            :: modis_isotropic
      
      ! =======================================================================
      ! READ_SURFACE_OPTIONS starts here 
      ! =======================================================================
      
      ! Initialize error flag
      error = .FALSE.
      
      ! Read surface reflectance option
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_surface_options:1' )
      READ( SUBSTRS(1:N), * ) surf_opt
      
      ! Fixed Albedo Case
      ! -----------------
      
      ! Skip line
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_surface_options:2' )
      
      ! Read albedo for ficed lambertian
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_surface_options:3' )
      READ( SUBSTRS(1:N), * ) fixed_albedo
      
      ! Lambertian spectrum case
      ! ------------------------
      
      ! Skip line
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_surface_options:4' )
      
      ! File name for albedo spectrum
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_surface_options:5' )
      READ( SUBSTRS(1:N), '(A)' ) albspectra_fname
      
      ! LER climatology case
      ! --------------------
      
      ! Skip line
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_surface_options:6' )
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_surface_options:7' )
      READ( SUBSTRS(1:N), '(A)' ) ler_clim_file
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_surface_options:8' )
      READ( SUBSTRS(1:N), * ) do_ler_const_wvl
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_surface_options:9' )
      READ( SUBSTRS(1:N), * ) ler_clim_wvl(1)
      
      ! MODIS FA Case
      ! ------------------------
      
      ! Skip line
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_surface_options:10' )
      
      ! Read path to file
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_surface_options:11' )
      READ(SUBSTRS(1),'(A)') fa_infile
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_surface_options:12' )
      READ( SUBSTRS(1:N), * ) modis_isotropic
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_surface_options:13' )
      READ( SUBSTRS(1:N), * ) modis_fa_alb_type
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_surface_options:14' )
      READ( SUBSTRS(1:N), * ) do_ocean_glint
      
!       ! Isotropic MODIS FA Case
!       ! -----------------------
!       
!       ! Skip line
!       CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_surface_options:4' )
!       
!       CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_surface_options:3' )
!       READ( SUBSTRS(1:N), * ) modis_fa_alb_type
      
      ! Fixed Kernel BRDF 
      ! ------------------
      
      ! Skip 2 lines
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_surface_options:15' )
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_surface_options:16' )
      
      ! Zero kernel parameters
      fkern_par(:,:) = 0.0d0
      
      ! Read Kernel options
      DO i=1,3
        
        CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_surface_options:17' )
        
        READ(SUBSTRS(1),'(A)') fkern_name(i)
        READ(SUBSTRS(2),*    ) fkern_idx(i)
        READ(SUBSTRS(3),*    ) fkern_amp(i)
        READ(SUBSTRS(4),*    ) fkern_npar(i)
        
        ! Just use the index to properly set the kernel
        fkern_name(i) = BRDF_CHECK_NAMES( fkern_idx(i) )
        
        ! Read up to a max of 5 paramete4rs
        DO J=5,MIN(4+MAX_BRDF_PARAMETERS,N)
          READ(SUBSTRS(J),* ) fkern_par(I,J-4)
        ENDDO
        
      ENDDO
      
      ! Skip Line
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_surface_options:15' )
      
      ! Read BRDF climatology infile
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_surface_options:15' )
      READ(SUBSTRS(1),'(A)') brdf_clim_infile
      

      ! CM Glint options
      ! -----------------

      ! Skip line
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_surface_options:15' )

      ! Read options
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_surface_options:14' )
      READ( SUBSTRS(1:N), * ) do_cm_shadow
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_surface_options:14' )
      READ( SUBSTRS(1:N), * ) do_cm_whitecap
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_surface_options:14' )
      READ( SUBSTRS(1:N), * ) do_cm_facet_iso
      
      ! SIF
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_surface_options:18' )
      READ( SUBSTRS(1:N), * ) do_sif
      
      ! ------------------------------------------------------------------------
      ! Based on case - Figure out settings
      ! ------------------------------------------------------------------------
      
      ! initialize all albedo options to false
      use_lambertian      = .FALSE. ! Flag for any lambertian option
      use_fixedalbedo     = .FALSE. ! Flag for case 1
      use_albspectra      = .FALSE. ! Flag for case 2
      use_ler_climatology = .FALSE. ! Flag for case 3
      use_albeofs         = .FALSE. ! Flag for case 4
      do_fixed_brdf       = .FALSE. ! Flag for case 5
      do_eof_brdf         = .FALSE. ! Flag for case 
      use_brdf_clim       = .FALSE. ! Flag for case 6
      ! Fixed Lambertian Case
      IF( surf_opt .EQ. 1 ) THEN
        use_lambertian  = .TRUE.
        use_fixedalbedo = .TRUE.
        do_ocean_glint  = .FALSE.
        
      ! Lambertian Spectrum Case
      ELSE IF( surf_opt .EQ. 2 ) THEN
        use_lambertian  = .TRUE.
        use_albspectra  = .TRUE.
        do_ocean_glint  = .FALSE.
        
      ! LER Climatology case
      ELSE IF( surf_opt .EQ. 3 ) THEN
        use_lambertian      = .TRUE.
        use_ler_climatology = .TRUE.
        do_ocean_glint      = .FALSE.
        
      ! Isotropic MODIS-FA Case
      ELSE IF( surf_opt .EQ. 4 ) THEN
        
        IF( modis_isotropic ) THEN
        
          use_lambertian      = .TRUE.
          use_albeofs         = .TRUE.
          do_ocean_glint      = .FALSE.
          
          ! For this case we must check if the albedo type is within range
          IF( modis_fa_alb_type < 1 .OR. &
              modis_fa_alb_type > 3      ) THEN
              
              print*,'MODIS-FA Albedo type must be 1(Black),2(White), or 3(Blue)'
              print*,'User selected case:', modis_fa_alb_type
              STOP
              
          ENDIF
        
        ELSE
          
          do_eof_brdf = .TRUE.
          
        ENDIF
        
      ! Fixed Kernel BRDF Case
      ELSE IF( surf_opt .EQ. 5 ) THEN
        do_fixed_brdf       = .TRUE.
        do_ocean_glint      = .FALSE.
        
      ELSEIF( surf_opt .EQ. 6 ) THEN
        use_brdf_clim = .TRUE.
      
      ELSEIF( surf_opt .EQ. 7 ) THEN
        use_cmglint = .TRUE.

      ! Number out of range
      ELSE
        
        ! No reflectance option selected
        print*,'ERROR: No reflectance option for case number ' ,surf_opt
        STOP
        
      ENDIF
      
      IF( do_debug_geocape_tool ) THEN

        print*,'============================'
        print*,'%%% READ SURFACE OPTIONS %%%'
        print*,'============================'
        print*,'use_lambertian  :',use_lambertian
        print*,'use_fixedalbedo :',use_fixedalbedo
        print*,'fixed_albedo    :',fixed_albedo
        print*,'use_albspectra  :',use_albspectra
        print*,'albspectra_fname:',TRIM(ADJUSTL(albspectra_fname))
        print*,'use_albeofs     :',use_albeofs
        print*,'do_fixed_brdf   :',do_fixed_brdf
        print*,'fkern_name      :',fkern_name
        print*,'fkern_idx       :',fkern_idx
        print*,'fkern_amp       :',fkern_amp
        print*,'fkern_npar      :',fkern_npar
        print*,'fkern_par       :',fkern_par
        print*,'do_eof_brdf     :',do_eof_brdf
        print*,'do_ocean_glint  :',do_ocean_glint
      
      ENDIF
      
    END SUBROUTINE READ_SURFACE_OPTIONS
    
    
    SUBROUTINE READ_TRACE_GAS_OPTIONS( funit, error )
      
      INTEGER, INTENT(IN)          :: funit
      LOGICAL,       INTENT(INOUT) :: error ! Error variable
      
      CHARACTER(LEN=max_ch_len) :: SUBSTRS(max_ch_len)
      CHARACTER(LEN=max_ch_len) :: this_gas !, tmpxsfile
      INTEGER            :: N, I, NGAS_FOUND
      LOGICAL            :: NOT_LAST_XSECT
      REAL(KIND=8)       :: co2_mr
      
      error = .FALSE.
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_trace_gas_options:1' )
      READ( SUBSTRS(1:N), * ) do_effcrs
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_trace_gas_options:1a' )
      READ( SUBSTRS(1:N), * ) do_rrs_phasefunc
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_trace_gas_options:1b' )
      READ( SUBSTRS(1:N), * ) do_rrs_abs
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_trace_gas_options:3' )
      
      IF( TRIM(ADJUSTL(SUBSTRS(1))) == '' ) THEN
        ngases = 0
      ELSE
        ngases = N
        
        ! New container for cross sections
        ALLOCATE( GasAbsXS( ngases ) )
        
        ! Allocate trace gas arrays
        ALLOCATE( which_gases(ngases)         ) ! ngases
        
        ! Read gases
        DO I=1, ngases
          READ( SUBSTRS(I), * ) which_gases(I) ! gas names
          READ( SUBSTRS(I), * ) GasAbsXS(I)%Name
        ENDDO
        
      ENDIF
      
      
      

      ! Skip header
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_trace_gas_options:4' )
      not_last_xsect = .TRUE.
      ngas_found = 0 
      do_hitran  = .FALSE.
      
      DO WHILE( not_last_xsect ) 
        
        CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_trace_gas_options:5' )
        
        ! Check for eof string
        READ(SUBSTRS(1),'(A)') this_gas
        
        IF( TRIM(ADJUSTL(this_gas)) .EQ. '###END_OF_LIST###' .OR.  &
            ngas_found .EQ. ngases                                 ) THEN
          
          ! Exit loop after reading
          not_last_xsect = .FALSE.
          
        ELSE
          
          ! Check if the gas is included 
          DO i=1,ngases
            
            IF( TRIM(ADJUSTL(this_gas)) .EQ. TRIM(ADJUSTL(GasAbsXS(I)%Name)) ) THEN
              
              ! Read information
!               READ(SUBSTRS(2),   * ) gas_nxsecs(i)
!               READ(SUBSTRS(3),   * ) gas_xsecs_type(i)
!               READ(SUBSTRS(4),'(A)') tmpxsfile
              
!               READ(SUBSTRS(2),   * ) GasAbsXS(i)%nXSect
              READ(SUBSTRS(2),   * ) GasAbsXS(i)%is_CIA
              READ(SUBSTRS(3),   * ) GasAbsXS(i)%CIA_Name1
              READ(SUBSTRS(4),   * ) GasAbsXS(i)%CIA_Name2
              READ(SUBSTRS(5),   * ) GasAbsXS(i)%XSectTypeID
              READ(SUBSTRS(6),'(A)') GasAbsXS(i)%XSectDataFilename
              
              ! Increment counter
              ngas_found = ngas_found + 1
              
              ! Check if there is a HITRAN gas present
              IF( GasAbsXS(i)%XsectTypeID .EQ. 3 .OR. &
                  GasAbsXS(i)%XsectTypeID .EQ. 5 .OR. &
                  GasAbsXS(i)%XsectTypeID .EQ. 6      ) do_hitran = .TRUE.
              
            ENDIF
            
          ENDDO
          
          
        ENDIF 
        
      ENDDO
      
      ! Skip to end of list
      IF( TRIM(ADJUSTL(this_gas)) .NE. '###END_OF_LIST###' ) not_last_xsect = .TRUE.
      DO WHILE( not_last_xsect )
        CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_trace_gas_options:6' )
        READ(SUBSTRS(1),'(A)') this_gas
        IF( TRIM(ADJUSTL(this_gas)) .EQ. '###END_OF_LIST###'  ) THEN
          not_last_xsect = .FALSE.
        ENDIF
      ENDDO
      
      IF( ngas_found .NE. ngases ) STOP 'Could not find all cross sections'
      
      ! Now read bulk composition data
      ! ------------------------------
      
      ! Use Earth composition?
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_trace_gas_options:7' )
      READ( SUBSTRS(1), * ) do_earth_bulk_comp
      
      ! CO2 Mixing ratio (ppmv) when using Earth composition
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_trace_gas_options:8' )
      READ( SUBSTRS(1), * ) co2_mr
      
      IF( do_earth_bulk_comp ) THEN
        
        ! Set number of gases
        n_bulk_gas = 4
        
        ! Allocate memory
        ALLOCATE( bulk_gas_mixr(n_bulk_gas)    )
        ALLOCATE( bulk_gas_mw(n_bulk_gas)      )
        ALLOCATE( bulk_gas_name(n_bulk_gas)    )
        ALLOCATE( bulk_gas_ri_name(n_bulk_gas) )
        
        ! Set Values
        
        ! Nitrogen gas
        bulk_gas_name(1) = 'N2'      ; bulk_gas_mixr(1)    = 78.084D-2
        bulk_gas_mw(1)   = 28.0134d0 ; bulk_gas_ri_name(1) = 'N2'
        
        ! Oxygen gas
        bulk_gas_name(2) = 'O2'      ; bulk_gas_mixr(2) = 20.946D-2
        bulk_gas_mw(2)   = 31.998d0  ; bulk_gas_ri_name(2) = 'O2'
        
        ! Argon
        bulk_gas_name(3) = 'Ar'      ; bulk_gas_mixr(3) = 0.934D-2
        bulk_gas_mw(3)   = 39.948d0  ; bulk_gas_ri_name(3) = 'Ar'
        
        ! Carbon dioxide
        bulk_gas_name(4) = 'CO2'     ; bulk_gas_mixr(4) = co2_mr*1.0d-6
        bulk_gas_mw(4)   = 44.01d0   ; bulk_gas_ri_name(4) = 'CO2'
        
        ! Gravitational acceleration
        grav_accel = 9.807d0
        
      ELSE
        
        
        ! Read gravitational acceleration constant [m/s]
        CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_trace_gas_options:10' )
        READ( SUBSTRS(1), * ) grav_accel
        
        ! Read names of bulk constituents
        CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_trace_gas_options:11' )
        
        ! Number of bulk gases
        n_bulk_gas = N
        
        ! Allocate memory
        ALLOCATE( bulk_gas_mixr(n_bulk_gas)    )
        ALLOCATE( bulk_gas_mw(n_bulk_gas)      )
        ALLOCATE( bulk_gas_name(n_bulk_gas)    )
        ALLOCATE( bulk_gas_ri_name(n_bulk_gas) )
        
        DO I=1, n_bulk_gas
          READ( SUBSTRS(I), * ) bulk_gas_name(I) 
        ENDDO
        
        ! Read the list and match bulk constituents
        not_last_xsect = .TRUE.
        ngas_found = 0 
        DO WHILE( not_last_xsect ) 
        
        CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_trace_gas_options:12' )
        
          ! Check for eof string
          READ(SUBSTRS(1),'(A)') this_gas
          
          IF( TRIM(ADJUSTL(this_gas)) .EQ. '###END_OF_LIST###' .OR.  &
              ngas_found .EQ. n_bulk_gas                             ) THEN
            
            ! Exit loop after reading
            not_last_xsect = .FALSE.
            
          ELSE
          
          ! Check if the gas is included 
          DO i=1,n_bulk_gas
              
              IF( TRIM(ADJUSTL(this_gas)) .EQ. TRIM(ADJUSTL(bulk_gas_name(i))) ) THEN
                
                READ(SUBSTRS(2),   * ) bulk_gas_mw(i)
                READ(SUBSTRS(3),   * ) bulk_gas_mixr(i)
                READ(SUBSTRS(4),'(A)') bulk_gas_ri_name(i)
                
                ! Increment counter
                ngas_found = ngas_found + 1
                
              ENDIF
              
            ENDDO ! n_bulk_gas
            
          ENDIF 
          
        ENDDO ! While loop
        
        ! Check all gases found
         IF( ngas_found .NE. n_bulk_gas ) STOP 'Could not find all bulk gases!'
        
      ENDIF
      
    END SUBROUTINE READ_TRACE_GAS_OPTIONS
    
    SUBROUTINE READ_AEROSOL_OPTIONS( funit, error )
      
      INTEGER, INTENT(IN)          :: funit
      LOGICAL,       INTENT(INOUT) :: error ! Error variable
      
      CHARACTER(LEN=max_ch_len) :: SUBSTRS(max_ch_len)
      INTEGER                   :: N, I
      INTEGER                   :: naer_found
      LOGICAL                   :: not_last_aer
      CHARACTER(LEN=max_ch_len) :: this_aer, tmp_aerfile
      
      error = .FALSE.
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_aerosol_options:1' )
      READ( SUBSTRS(1:N), * ) do_aerosols
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_aerosol_options:3' )
      
      IF( TRIM(ADJUSTL(SUBSTRS(1))) == '' ) THEN
        
        ! Number of aerosols
        naer = 0
        
      ELSE
        
        ! Number of aerosols
        naer = N
        
        ! Allocate aerosol input arrays
        ALLOCATE( aer_types(naer) ) ! naer
        ALLOCATE( aer_tau0s(naer) ) ! naer
        ALLOCATE( aer_z_upperlimit(naer)) ! naer
        ALLOCATE( aer_z_lowerlimit(naer)) ! naer
        ALLOCATE( aer_z_peakheight(naer)) ! naer
        ALLOCATE( aer_half_width(naer)  ) ! naer
        ALLOCATE( aer_relaxation(naer)  ) ! naer
        ALLOCATE( yn_aer_rhdep(naer)    ) ! naer
        ALLOCATE( aer_data_filenames(naer)) ! naer
        ALLOCATE( aer_proftype(naer) ) ! naer
        DO I=1, naer
          READ( SUBSTRS(I), * ) aer_types(I) ! gas names
        ENDDO
        
      ENDIF
      
      
      
      ! Skip header
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_aerosol_options:4' )
      
      not_last_aer = .TRUE.
      naer_found = 0 
      DO WHILE( not_last_aer ) 
        
        CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_aerosol_options:5' )
        
        ! Check for eof string
        READ(SUBSTRS(1),'(A)') this_aer
        
        IF( TRIM(ADJUSTL(this_aer)) .EQ. '###END_OF_LIST###' ) THEN
          
          ! Exit loop after reading
          not_last_aer = .FALSE.
          
        ELSE
          
          ! Check if the gas is included 
          DO i=1,naer
            
            IF( TRIM(ADJUSTL(this_aer)) .EQ. TRIM(ADJUSTL(aer_types(i))) ) THEN
              
              ! Read information
              READ(SUBSTRS(2),   * ) yn_aer_rhdep(i)
              READ(SUBSTRS(3),'(A)') tmp_aerfile
              aer_data_filenames(i) = TRIM(ADJUSTL(tmp_aerfile))
              
              ! Increment counter
              naer_found = naer_found + 1
              
            ENDIF
            
          ENDDO
          
        ENDIF 
        
      ENDDO
      
      IF( naer .ne. naer_found ) THEN
        print*,'Error: Matched ',naer_found,' aerosol optical property files'
        print*,'Expected ',naer
        STOP
      ENDIF
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_aerosol_options:6' )
      READ( SUBSTRS(1:N), * ) aer_reflambda
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_aerosol_options:7' )
      READ( SUBSTRS(1:N), * ) use_aerprof
              
      ! Skip header
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_aerosol_options:8' )
      
      not_last_aer = .TRUE.
      naer_found = 0 
      DO WHILE( not_last_aer ) 
        
        CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_aerosol_options:9' )
        
        ! Check for eof string
        READ(SUBSTRS(1),'(A)') this_aer
       
        IF( TRIM(ADJUSTL(this_aer)) .EQ. '###END_OF_LIST###' ) THEN
          
          ! Exit loop after reading
          not_last_aer = .FALSE.
          
        ELSE
          
          ! Check if the gas is included 
          DO i=1,naer
            
            IF( TRIM(ADJUSTL(this_aer)) .EQ. TRIM(ADJUSTL(aer_types(i))) ) THEN
              
              ! Read information
              READ( SUBSTRS(2),   * ) aer_tau0s(i)
              READ( SUBSTRS(3),'(A)') aer_proftype(i)
              READ( SUBSTRS(4), * ) aer_z_lowerlimit(i)
              READ( SUBSTRS(5), * ) aer_z_upperlimit(i)
              READ( SUBSTRS(6), * ) aer_z_peakheight(i)
              READ( SUBSTRS(7), * ) aer_half_width(i)
              READ( SUBSTRS(8), * ) aer_relaxation(i)
              
              ! Increment counter
              naer_found = naer_found + 1
                
            ENDIF
              
          ENDDO
            
        ENDIF 
        
      ENDDO
      
      IF( .not. use_aerprof .and.  naer .ne. naer_found  ) THEN
        print*,'Error: Matched ',naer_found,' aerosol profiles'
        print*,'Expected ',naer
        STOP
      ENDIF
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_aerosol_options:10' )
      READ( SUBSTRS(1:N), * ) do_aer_columnwf
      
      
    END SUBROUTINE READ_AEROSOL_OPTIONS
    
    SUBROUTINE READ_CLOUD_OPTIONS( funit, error )
      
      INTEGER, INTENT(IN)          :: funit
      LOGICAL,       INTENT(INOUT) :: error ! Error variable
      
      CHARACTER(LEN=max_ch_len) :: SUBSTRS(max_ch_len)
      CHARACTER(LEN=max_ch_len) :: this_cld, tmp_cldfile
      INTEGER            :: N, I, ncld_found, cld_pix_id
      LOGICAL            :: not_last_cld
      
      error = .FALSE.
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_cloud_options:1' )
      READ( SUBSTRS(1:N), * ) do_clouds
      
      ! Read cloud types
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_cloud_options:3' )
      
      IF( TRIM(ADJUSTL(SUBSTRS(1))) == '' ) THEN
        
        ! Number of cloud types
        ncld = 0
        
      ELSE
      
        ! Number of cloud types
        ncld = N
        
        ! Allocate cloud input settings arrays
        ALLOCATE( cld_types(maxcld)          ) ! ncld
        ALLOCATE( cld_data_filenames(maxcld) ) ! ncld
        
        
        DO I=1, ncld
          READ( SUBSTRS(I), * ) cld_types(I) ! cloud names
        ENDDO
      
      ENDIF
      
      ! Skip line
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_cloud_options:4' )
      
      not_last_cld = .TRUE.
      ncld_found = 0 
      DO WHILE( not_last_cld ) 
        
        CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_cloud_options:5' )
        
        ! Check for eof string
        READ(SUBSTRS(1),'(A)') this_cld
        
        IF( TRIM(ADJUSTL(this_cld)) .EQ. '###END_OF_LIST###' ) THEN
          
          ! Exit loop after reading
          not_last_cld = .FALSE.
          
        ELSE
          
          ! Check if the gas is included 
          DO i=1,ncld
            
            IF( TRIM(ADJUSTL(this_cld)) .EQ. TRIM(ADJUSTL(cld_types(i))) ) THEN
              
              ! Read information
              READ(SUBSTRS(2),'(A)') tmp_cldfile
              cld_data_filenames(i) = TRIM(ADJUSTL(tmp_cldfile))
              
              ! Increment counter
              ncld_found = ncld_found + 1
              
            ENDIF
            
          ENDDO
          
        ENDIF 
        
      ENDDO
      
      IF( ncld_found .ne. ncld ) THEN
        
        print*,'Cloud optical properties found:',ncld_found
        print*,'Expected ',ncld
        STOP
      ENDIF
      
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_cloud_options:6' )
      READ( SUBSTRS(1:N), * ) cld_reflambda
      
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_cloud_options:5' )
      READ( SUBSTRS(1:N), * ) do_lambertian_cld
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_cloud_options:6' )
      READ( SUBSTRS(1:N), * ) lambertian_cldalb
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_cloud_options:7' )
      READ( SUBSTRS(1:N), * ) use_cldprof
      
      ! This is default from now, diag_gas_jac
      do_npix_cloud = .TRUE.
      
      ! Note check if these are reset elsewhere in code
      IF( .NOT. use_cldprof ) THEN
        
        ! Number of cloud subpixels
        CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_cloud_options:8' )
        READ( SUBSTRS(1:N), * ) n_cpix ! Number of cloud pixels
        
        ! Allocate cloud pixel arrays
        ALLOCATE( cpix_cfrac(n_cpix) )
        
        ! Cloud fractions
        CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_cloud_options:9' )
        DO I=1, n_cpix
          READ( SUBSTRS(I), * ) cpix_cfrac(I) ! gas names
        ENDDO
        
        ! Lambertian Case parameters
        ! --------------------------
        ALLOCATE( cpix_pcld(n_cpix)  )
        
        ! Cloud pressures
        CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_cloud_options:10' )
        DO I=1, n_cpix
          READ( SUBSTRS(I), * ) cpix_pcld(I) ! gas names
        ENDDO
        
        ! Scattering Cloud case parameters
        ! --------------------------------
        ALLOCATE(        cld_tau0s(ncld,n_cpix) )
        ALLOCATE(     cld_proftype(ncld,n_cpix) )
        ALLOCATE( cld_z_lowerlimit(ncld,n_cpix) )
        ALLOCATE( cld_z_upperlimit(ncld,n_cpix) )
        ALLOCATE( cld_z_peakheight(ncld,n_cpix) )
        ALLOCATE(   cld_half_width(ncld,n_cpix) )
        ALLOCATE(   cld_relaxation(ncld,n_cpix) )
        
        
        ! Initialize arrays
        cld_tau0s(:,:)        = 0.0d0
        cld_proftype(:,:)     = ''
        cld_z_lowerlimit(:,:) = 0.0d0
        cld_z_upperlimit(:,:) = 0.0d0
        cld_z_peakheight(:,:) = 0.0d0
        cld_half_width(:,:)   = 0.0d0
        cld_relaxation(:,:)   = 0.0d0
        
        ! Skip one line
        CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_cloud_options:5' )
        
        ! Match profile parameters
        not_last_cld = .TRUE.
        DO WHILE( not_last_cld )
          
          ! Read line
          CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_cloud_options:5' )
            
          ! Check for eof string
          READ(SUBSTRS(1),'(A)') this_cld
            
          IF( TRIM(ADJUSTL(this_cld)) .EQ. '###END_OF_LIST###' ) THEN
              ! Exit loop after reading
              not_last_cld = .FALSE.
          ENDIF
          
          DO i=1,ncld
          
            ! Match the profile
            IF( TRIM(ADJUSTL(this_cld)) .EQ. TRIM(ADJUSTL(cld_types(i))) ) THEN
                    
              ! Read sub pixel index
              READ( SUBSTRS(2), * ) cld_pix_id
              
              ! Read profile information for pixel index
              READ( SUBSTRS(3),   * ) cld_tau0s(i,cld_pix_id)
              READ( SUBSTRS(4),'(A)') cld_proftype(i,cld_pix_id)
              READ( SUBSTRS(5), * ) cld_z_lowerlimit(i,cld_pix_id)
              READ( SUBSTRS(6), * ) cld_z_upperlimit(i,cld_pix_id)
              READ( SUBSTRS(7), * ) cld_z_peakheight(i,cld_pix_id)
              READ( SUBSTRS(8), * ) cld_half_width(i,cld_pix_id)
              READ( SUBSTRS(9), * ) cld_relaxation(i,cld_pix_id)
              
            ENDIF
            
          ENDDO
          
        ENDDO
        
      ELSE
        
        ! Skip to end of profile par
        not_last_cld = .TRUE.
        DO WHILE( not_last_cld )
          
          ! Read line
          CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_cloud_options:5' )
            
          ! Check for end of list string
          READ(SUBSTRS(1),'(A)') this_cld
          IF( TRIM(ADJUSTL(this_cld)) .EQ. '###END_OF_LIST###' ) THEN
              ! Exit loop after reading
              not_last_cld = .FALSE.
          ENDIF
          
        ENDDO
        
      ENDIF
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, 1, 'read_cloud_options:18' )
      READ( SUBSTRS(1:N), * ) do_cld_columnwf
      
      ! Switch off all cloud options if we aren't doing clouds
      IF( .not. do_clouds ) THEN
        do_lambertian_cld = .FALSE.
        do_npix_cloud     = .FALSE.
        use_cldprof       = .FALSE.
        cfrac             = 0.0
        n_cpix            = 0.0
       
        IF( ALLOCATED(cpix_cfrac)) DEALLOCATE(cpix_cfrac)
 
        ! Still make it zero
        ALLOCATE(cpix_cfrac(1))
        cpix_cfrac(1) = 0.0
        
        
      ENDIF
      
      ! Hardwire zcldspec - Only allow input cloud inputs in hPa
      use_zcldspec = .FALSE.
      
      IF( do_debug_geocape_tool  ) THEN
      
        print*,'=========================='
        print*,'%%% READ CLOUD OPTIONS %%%'
        print*,'==========================='
        print*,'do_clouds:',do_clouds
        print*,'do_lambertian_cld:',do_lambertian_cld
        IF( do_lambertian_cld ) THEN
          print*,'lambertian_cldalb:',lambertian_cldalb
          print*,'use_cldprof:',use_cldprof
          IF(.NOT. use_cldprof) THEN
            print*,'cfrac:',cfrac
            !print*,'cld_tops(1):',cld_tops_npix(1)
          ENDIF
        
        ENDIF
        
        print*,'do_npix_cloud:',do_npix_cloud
        print*,'do_cld_columnwf:',do_cld_columnwf
        print*,'do_cod_Jacobians:',do_cod_Jacobians
        print*,'do_cssa_Jacobians:',do_cssa_Jacobians
        print*,'do_cfrac_Jacobians:',do_cfrac_Jacobians
        print*,'do_ctp_Jacobians:',do_ctp_Jacobians
        
      ENDIF
      
    END SUBROUTINE READ_CLOUD_OPTIONS
    
    SUBROUTINE READ_DIAGNOSTIC_OPTIONS( funit, error )
      
      USE GC_variables_module, ONLY : Diag01_InpOpt,  Diag08_InpOpt,  Diag16_InpOpt,  &
                                      Diag18_InpOpt,  Diag28_InpOpt,                  &
                                      Diag32_InpOpt,  Diag34_InpOpt,  Diag35_InpOpt,  &
                                      do_stokes_ad20, do_stokes_ad21, do_stokes_ad22, &
                                      do_stokes_ad28, do_stokes_ad29, do_stokes_ad31, &
                                      do_stokes_ad32, do_stokes_ad34, do_stokes_ad35, &
                                      do_stokes_ad36, do_stokes_ad37, do_stokes_ad38, &
                                      do_stokes_ad39
      
      INTEGER, INTENT(IN)          :: funit
      LOGICAL,       INTENT(INOUT) :: error ! Error variable
      
      CHARACTER(LEN=max_ch_len) :: SUBSTRS(max_ch_len)
      CHARACTER(LEN=max_ch_len) :: upper_str
      INTEGER            :: N, I
      
      error = .FALSE.
      
!       CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:1' )
!       READ( SUBSTRS(1), * ) do_StokesQU_output
      
      ! -----------------------------------------------------------------------
      ! AMF Options ( Diagnostic 01 )
      ! -----------------------------------------------------------------------
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      READ( SUBSTRS(1), * ) do_AMF_calculation
      do_diag(1) = do_AMF_calculation
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      IF( TRIM(ADJUSTL(SUBSTRS(1))) == '' ) THEN
        Diag01_InpOpt%n_amf = 0
      ELSE
        Diag01_InpOpt%n_amf = N
        IF( DO_ALL_SPC( SUBSTRS(1) ) ) THEN
          ALLOCATE( Diag01_InpOpt%amf_spc(ngases)     )
          ALLOCATE( Diag01_InpOpt%amf_spc_idx(ngases) )
          Diag01_InpOpt%amf_spc     =  which_gases(1:ngases)
          Diag01_InpOpt%amf_spc_idx = (/(I, I=1,ngases)/)
          Diag01_InpOpt%n_amf       = ngases
        ELSE
          ALLOCATE( Diag01_InpOpt%amf_spc(N)     )
          ALLOCATE( Diag01_InpOpt%amf_spc_idx(N) )
          DO I=1,N
            Diag01_InpOpt%amf_spc(I) = SUBSTRS(I)
          ENDDO
          CALL MATCH_DIAG_LIST( which_gases(1:ngases), ngases,             &
                              Diag01_InpOpt%amf_spc, Diag01_InpOpt%n_amf,  &
                              Diag01_InpOpt%amf_spc_idx                    )
        ENDIF
      ENDIF
      
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      READ(SUBSTRS(1), * ) Diag01_InpOpt%output_scatterwt
      
      ! -----------------------------------------------------------------------
      ! Viewing Geometry ( Diagnostic 02 )
      ! -----------------------------------------------------------------------
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      READ( SUBSTRS(1), * ) write_view_geom
      do_diag(2) = write_view_geom
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      READ( SUBSTRS(1), * ) write_footprint_info
      do_diag(3) = write_footprint_info
      
      ! Skip profile header
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      READ( SUBSTRS(1), * ) write_altitude_prof
      do_diag(4) = write_altitude_prof
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      READ( SUBSTRS(1), * ) write_pres_prof
      do_diag(5) = write_pres_prof
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      READ( SUBSTRS(1), * ) write_temp_prof
      do_diag(6) = write_temp_prof
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      READ( SUBSTRS(1), * ) write_aircol_prof
      do_diag(7) = write_aircol_prof
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      READ( SUBSTRS(1), * ) write_aod_refwvl
      do_diag(8) = write_aod_refwvl
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      READ( SUBSTRS(1), * ) write_totaod_refwvl
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      IF( TRIM(ADJUSTL(SUBSTRS(1))) == '' ) THEN
        Diag08_InpOpt%n_gas = 0
      ELSE
        Diag08_InpOpt%n_gas = N
        IF( DO_ALL_SPC( SUBSTRS(1) ) ) THEN
          ALLOCATE( Diag08_InpOpt%spc(naer)     )
          ALLOCATE( Diag08_InpOpt%spc_idx(naer) )
          Diag08_InpOpt%spc     =  aer_types(1:naer)
          Diag08_InpOpt%spc_idx = (/(I, I=1,naer)/)
          Diag08_InpOpt%n_gas   = naer
        ELSE
          ALLOCATE( Diag08_InpOpt%spc(N)     )
          ALLOCATE( Diag08_InpOpt%spc_idx(N) )
          DO I=1,N
            Diag08_InpOpt%spc(I) = SUBSTRS(I)
          ENDDO
          CALL MATCH_DIAG_LIST( aer_types(1:naer), naer,                 &
                                Diag08_InpOpt%spc, Diag08_InpOpt%n_gas,  &
                                Diag08_InpOpt%spc_idx                    )
        ENDIF
      ENDIF
      
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      READ( SUBSTRS(1), * ) write_cod_refwvl
      do_diag(9) = write_cod_refwvl
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      READ( SUBSTRS(1), * ) write_od_prof
      do_diag(10) = write_od_prof
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      READ( SUBSTRS(1), * ) write_ssa_prof
      do_diag(11) = write_ssa_prof
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      READ( SUBSTRS(1), * ) write_aod_prof
      do_diag(12) = write_aod_prof
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      READ( SUBSTRS(1), * ) write_assa_prof
      do_diag(13) = write_assa_prof
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      READ( SUBSTRS(1), * ) write_cod_prof
      do_diag(14) = write_cod_prof
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      READ( SUBSTRS(1), * ) write_cssa_prof
      do_diag(15) = write_cssa_prof
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      READ( SUBSTRS(1), * ) write_gas_prof
      do_diag(16) = write_gas_prof
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      IF( TRIM(ADJUSTL(SUBSTRS(1))) == '' ) THEN
        Diag16_InpOpt%n_gas = 0
      ELSE
        Diag16_InpOpt%n_gas = N
        IF( DO_ALL_SPC( SUBSTRS(1) ) ) THEN
          ALLOCATE( Diag16_InpOpt%spc(ngases)     )
          ALLOCATE( Diag16_InpOpt%spc_idx(ngases) )
          Diag16_InpOpt%spc     =  which_gases(1:ngases)
          Diag16_InpOpt%spc_idx = (/(I, I=1,ngases)/)
          Diag16_InpOpt%n_gas   = ngases
        ELSE
          ALLOCATE( Diag16_InpOpt%spc(N)     )
          ALLOCATE( Diag16_InpOpt%spc_idx(N) )
          DO I=1,N
            Diag16_InpOpt%spc(I) = SUBSTRS(I)
          ENDDO
          CALL MATCH_DIAG_LIST( which_gases(1:ngases), ngases,           &
                                Diag16_InpOpt%spc, Diag16_InpOpt%n_gas,  &
                                Diag16_InpOpt%spc_idx                    )
        ENDIF
      ENDIF
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      READ( SUBSTRS(1), * ) write_gas_names
      do_diag(17) = write_gas_names
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      READ( SUBSTRS(1), * ) write_gas_xsecs
      do_diag(18) = write_gas_xsecs
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      READ( SUBSTRS(1), * ) write_xsec_deriv
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      IF( TRIM(ADJUSTL(SUBSTRS(1))) == '' ) THEN
        Diag18_InpOpt%n_gas = 0
      ELSE
        Diag18_InpOpt%n_gas = N
        IF( DO_ALL_SPC( SUBSTRS(1) ) ) THEN
          ALLOCATE( Diag18_InpOpt%spc(ngases)     )
          ALLOCATE( Diag18_InpOpt%spc_idx(ngases) )
          Diag18_InpOpt%spc     =  which_gases(1:ngases)
          Diag18_InpOpt%spc_idx = (/(I, I=1,ngases)/)
          Diag18_InpOpt%n_gas   = ngases
        ELSE
          ALLOCATE( Diag18_InpOpt%spc(N)     )
          ALLOCATE( Diag18_InpOpt%spc_idx(N) )
          DO I=1,N
            Diag18_InpOpt%spc(I) = SUBSTRS(I)
          ENDDO
          CALL MATCH_DIAG_LIST( which_gases(1:ngases), ngases,           &
                                Diag18_InpOpt%spc, Diag18_InpOpt%n_gas,  &
                                Diag18_InpOpt%spc_idx                    )
        ENDIF
      ENDIF
      
      ! =======================================================================
      ! Skip spectrum output options header
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      ! =======================================================================
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      READ( SUBSTRS(1), * ) write_wvl_grid
      do_diag(19) = write_wvl_grid
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      READ( SUBSTRS(1), * ) write_rad_spc
      do_diag(20) = write_rad_spc
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      READ( SUBSTRS(1), * ) do_stokes_ad20
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      READ( SUBSTRS(1), * ) write_flx_spc
      do_diag(21) = write_flx_spc
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      READ( SUBSTRS(1), * ) do_stokes_ad21
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      READ( SUBSTRS(1), * ) write_dflx_spc
      do_diag(22) = write_dflx_spc
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      READ( SUBSTRS(1), * ) do_stokes_ad22
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      READ( SUBSTRS(1), * ) write_irad_spc
      do_diag(23) = write_irad_spc
      
      
      ! =======================================================================
      ! Skip surface output header
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      ! =======================================================================
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      READ( SUBSTRS(1), * ) write_alb_spc
      do_diag(24) = write_alb_spc
      
      ! Check for cases where this is not possible
      IF( .not. use_lambertian ) THEN
        print*,'Skipping Albedo Diagnostic - Simulation uses BRDF'
        do_diag(24) = .FALSE. ; write_alb_spc = .FALSE.
      ENDIF
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      READ( SUBSTRS(1), * ) write_fix_brdf_par
      do_diag(25) = write_fix_brdf_par
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      READ( SUBSTRS(1), * ) diag25_write_kpar
      
      ! Check for cases where this is not possible
      IF( use_lambertian ) THEN
        print*,'Skipping BRDF Diagnostic - Simulation uses lambertian albedo'
        do_diag(25) = .FALSE. ; write_fix_brdf_par = .FALSE. ; diag25_write_kpar = .FALSE.
      ENDIF
      
      write_full_brdf = .FALSE. ! This is depricated
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      READ( SUBSTRS(1), * ) write_sif
      do_diag(26) = write_sif
      
      ! This is nothing for now
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      READ( SUBSTRS(1), * ) write_modis_eof_par
      do_diag(27) = write_modis_eof_par
      
      ! =======================================================================
      ! Skip surface jacobians header
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      ! =======================================================================
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      READ( SUBSTRS(1), * ) do_diag(28) 
      
      ! Kernel Amplitude
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      READ( SUBSTRS(1), * ) do_surfalb_jacobian

      Diag28_InpOpt%do_factor_jac = do_surfalb_jacobian ! New 

      ! BRDF Kernel Parameters
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      READ( SUBSTRS(1), * ) Diag28_InpOpt%do_par_jac
      

      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      READ( SUBSTRS(1), * ) do_stokes_ad28
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      READ( SUBSTRS(1), * ) do_wspd_jacobian
      do_diag(29) = do_wspd_jacobian
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      READ( SUBSTRS(1), * ) do_stokes_ad29
      
      ! =======================================================================
      ! Skip met. profile jacobians header
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      ! =======================================================================
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:3' )
      READ( SUBSTRS(1), * ) do_T_Jacobians
      do_diag(30) = do_T_jacobians
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:4' )
      READ( SUBSTRS(1), * ) do_sfcprs_Jacobians
      do_diag(31) = do_sfcprs_Jacobians
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:4' )
      READ( SUBSTRS(1), * ) do_oco2_sfcprs
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:4' )
      READ( SUBSTRS(1), * ) do_airdens_jac

      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:4' )
      READ( SUBSTRS(1), * ) do_stokes_ad31
      
      ! =======================================================================
      ! Skip Trace gas jacobians header
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      ! =======================================================================
      
      ! Do gas jacobians?
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      READ( SUBSTRS(1), * ) do_gas_jacobians
!        = diag_gas_jac%do_diag ! While modifying set old flags
      do_diag(32) = do_gas_jacobians
      
      ! Which species?
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      IF( TRIM(ADJUSTL(SUBSTRS(1))) == '' ) THEN
        Diag32_InpOpt%n_gas = 0
      ELSE
        Diag32_InpOpt%n_gas = N
        IF( DO_ALL_SPC( SUBSTRS(1) ) ) THEN
          ALLOCATE( Diag32_InpOpt%spc(ngases)     )
          ALLOCATE( Diag32_InpOpt%spc_idx(ngases) )
          Diag32_InpOpt%spc     =  which_gases(1:ngases)
          Diag32_InpOpt%spc_idx = (/(I, I=1,ngases)/)
          Diag32_InpOpt%n_gas   = ngases
        ELSE
          ALLOCATE( Diag32_InpOpt%spc(N)     )
          ALLOCATE( Diag32_InpOpt%spc_idx(N) )
          DO I=1,N
            Diag32_InpOpt%spc(I) = SUBSTRS(I)
          ENDDO
          CALL MATCH_DIAG_LIST( which_gases(1:ngases), ngases,           &
                                Diag32_InpOpt%spc, Diag32_InpOpt%n_gas,  &
                                Diag32_InpOpt%spc_idx                    )
        ENDIF
      ENDIF
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      READ( SUBSTRS(1), * ) do_stokes_ad32
      
      ! Do raman "absorption" jacobian
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      READ( SUBSTRS(1), * ) do_raman_jacobian
      do_diag(33) = do_raman_jacobian
      
      ! =======================================================================
      ! Skip Aerosol jacobians header
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      ! =======================================================================
      
      ! ------------------------------
      ! AOD Jacobians
      ! ------------------------------
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:3' )
      READ( SUBSTRS(1), * ) do_aod_Jacobians
      do_diag(34) = do_aod_jacobians
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      READ( SUBSTRS(1), * ) do_stokes_ad34
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:3' )
      READ( SUBSTRS(1), * ) do_total_aod_jac
      
      ! Species level AOD
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      
      IF( TRIM(ADJUSTL(SUBSTRS(1))) == '' ) THEN
        Diag34_InpOpt%nspc = 0
      ELSE
        Diag34_InpOpt%nspc = N
        IF( DO_ALL_SPC( SUBSTRS(1) ) ) THEN
          ALLOCATE( Diag34_InpOpt%spc(naer)     )
          ALLOCATE( Diag34_InpOpt%spc_idx(naer) )
          Diag34_InpOpt%spc     =  aer_types(1:naer)
          Diag34_InpOpt%nspc   = naer
        ELSE
          ALLOCATE( Diag34_InpOpt%spc(N)     )
          ALLOCATE( Diag34_InpOpt%spc_idx(N) )
          DO I=1,N
            Diag34_InpOpt%spc(I) = SUBSTRS(I)
          ENDDO
        ENDIF
      ENDIF
      
      ! Profile parameters
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      IF( TRIM(ADJUSTL(SUBSTRS(1))) == '' ) THEN
        Diag34_InpOpt%npar = 0
      ELSE
        Diag34_InpOpt%npar = N
        IF( DO_ALL_SPC( SUBSTRS(1) ) ) THEN
          ALLOCATE( Diag34_InpOpt%par(naer)     )
          ALLOCATE( Diag34_InpOpt%par_idx(naer) )
          Diag34_InpOpt%par  = aer_types(1:naer)
          Diag34_InpOpt%npar = naer
        ELSE
          ALLOCATE( Diag34_InpOpt%par(N)     )
          ALLOCATE( Diag34_InpOpt%par_idx(N) )
          DO I=1,N
            Diag34_InpOpt%par(I) = SUBSTRS(I)
          ENDDO
        ENDIF
      ENDIF
      
      CALL CHECK_AEROSOL_JAC_DIAGS( Diag34_InpOpt )
      
      ! ------------------------------
      ! SSA Jacobians
      ! ------------------------------
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:4' )
      READ( SUBSTRS(1), * ) do_assa_Jacobians
      do_diag(35) = do_assa_jacobians
      
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      READ( SUBSTRS(1), * ) do_stokes_ad35
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:3' )
      READ( SUBSTRS(1), * ) do_total_assa_jac
      
      ! Species level SSA
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      IF( TRIM(ADJUSTL(SUBSTRS(1))) == '' ) THEN
        Diag35_InpOpt%nspc = 0
      ELSE
        Diag35_InpOpt%nspc = N
        IF( DO_ALL_SPC( SUBSTRS(1) ) ) THEN
          ALLOCATE( Diag35_InpOpt%spc(naer)     )
          ALLOCATE( Diag35_InpOpt%spc_idx(naer) )
          Diag35_InpOpt%spc     =  aer_types(1:naer)
          Diag35_InpOpt%nspc   = naer
        ELSE
          ALLOCATE( Diag35_InpOpt%spc(N)     )
          ALLOCATE( Diag35_InpOpt%spc_idx(N) )
          DO I=1,N
            Diag35_InpOpt%spc(I) = SUBSTRS(I)
          ENDDO
        ENDIF
      ENDIF
      
      ! Peak Height
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      IF( TRIM(ADJUSTL(SUBSTRS(1))) == '' ) THEN
        Diag35_InpOpt%npar = 0
      ELSE
        Diag35_InpOpt%npar = N
        IF( DO_ALL_SPC( SUBSTRS(1) ) ) THEN
          ALLOCATE( Diag35_InpOpt%par(naer)     )
          ALLOCATE( Diag35_InpOpt%par_idx(naer) )
          Diag35_InpOpt%par     = aer_types(1:naer)
          Diag35_InpOpt%npar     = naer
        ELSE
          ALLOCATE( Diag35_InpOpt%par(N)     )
          ALLOCATE( Diag35_InpOpt%par_idx(N) )
          DO I=1,N
            Diag35_InpOpt%par(I) = SUBSTRS(I)
          ENDDO
        ENDIF
      ENDIF
      
      CALL CHECK_AEROSOL_JAC_DIAGS( Diag35_InpOpt )
      
      
      ! =======================================================================
      ! Skip Cloud jacobians header
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      ! =======================================================================
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:3' )
      READ( SUBSTRS(1), * ) do_cfrac_jacobians
      do_diag(36) = do_cfrac_jacobians
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      READ( SUBSTRS(1), * ) do_stokes_ad36
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:4' )
      READ( SUBSTRS(1), * ) do_ctp_jacobians
      do_diag(37) = do_ctp_jacobians
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      READ( SUBSTRS(1), * ) do_stokes_ad37
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:5' )
      READ( SUBSTRS(1), * ) do_cod_jacobians
      do_diag(38) = do_cod_jacobians
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      READ( SUBSTRS(1), * ) do_stokes_ad38
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:6' )
      READ( SUBSTRS(1), * ) do_cssa_jacobians
      do_diag(39) = do_cssa_jacobians
      
      CALL SPLIT_ONE_LINE( funit, SUBSTRS, N, -1, 'read_diagnostic_options:2' )
      READ( SUBSTRS(1), * ) do_stokes_ad39
      
      ! Flag for doing stokes output
      do_StokesQU_output = do_stokes_ad20 .or. do_stokes_ad21 .or. do_stokes_ad22
      
      
      ! Hardcode RRS jacobian input for now
      !do_raman_jacobian = .FALSE.
      
    END SUBROUTINE READ_DIAGNOSTIC_OPTIONS
    
    LOGICAL FUNCTION DO_ALL_SPC( spcname )
      
      ! --------------------
      ! Subroutine arguments
      ! --------------------
      CHARACTER(LEN=max_ch_len), INTENT(IN) :: spcname
      
      ! ---------------
      ! Local variables
      ! ---------------
      CHARACTER(LEN=max_ch_len)           :: upper_str
      
      ! ============================================================
      ! DO_ALL_SPC starts here
      ! ============================================================
      
      ! Init
      DO_ALL_SPC = .FALSE.
      
      ! Check first element is not "ALL"
      upper_str = spcname
      CALL TRANUC( upper_str )
      
      IF( (TRIM(ADJUSTL(upper_str)).EQ. 'ALL' ) ) DO_ALL_SPC = .TRUE.
      
      RETURN
      
    END FUNCTION DO_ALL_SPC
    
    SUBROUTINE MATCH_DIAG_LIST( spc_list, nspc, target_list, ntrg, target_idx )
      
      ! --------------------
      ! Subroutine arguments
      ! --------------------
      INTEGER,                          INTENT(IN)  :: nspc, ntrg
      CHARACTER(LEN=*), DIMENSION(nspc),INTENT(IN)  :: spc_list
      CHARACTER(LEN=*), DIMENSION(ntrg),INTENT(IN)  :: target_list
      INTEGER,          DIMENSION(ntrg),INTENT(OUT) :: target_idx
      ! ---------------
      ! Local variables
      ! ---------------
      INTEGER                    :: s,t
      ! ============================================================
      ! MATCH_DIAG_LIST starts here
      ! ============================================================
      
      ! Initialize output
      target_idx(:) = -1
      
      ! Match names
      DO s=1,nspc
        DO t=1,ntrg
          
          IF( TRIM(ADJUSTL(target_list(t))) .EQ. &
              TRIM(ADJUSTL(spc_list(s)))         ) THEN
            
            target_idx(t) = s
            
          ENDIF
          
        ENDDO
      ENDDO
      
      ! Check all are matched
      DO t=1,ntrg
        
        IF( target_idx(t) .LE. 0 ) THEN
          print*,'Could not find all elements in species list'
          STOP
        ENDIF
        
      ENDDO
      
    END SUBROUTINE MATCH_DIAG_LIST
    
    SUBROUTINE CHECK_AEROSOL_JAC_DIAGS( AD )
      
      USE GC_variables_module, ONLY : DiagAer_InpOptType
      
      ! --------------------
      ! Subroutine arguments
      ! --------------------
      TYPE(DiagAer_InpOptType), INTENT(INOUT) :: AD
      
      ! ---------------
      ! Local variables
      ! ---------------
      INTEGER                   :: n, d, ct
      LOGICAL, DIMENSION(naer)  :: do_spc_jac
      
      ! ============================================================
      ! CHECK_AEROSOL_JAC_DIAGS starts here
      ! ============================================================
      
      ! Initialize do_spc_jac to false
      do_spc_jac(:) = .FALSE.
      
      ! First Check if we are using CTM profiles
      IF( use_aerprof ) THEN
        
        ! These jacobians only work for parameterized profiles
        AD%npar = 0
        
      ENDIF
      
      DO n=1,naer
        
        ! Species specific AOD jacobian
        DO d=1, AD%nspc
          IF( TRIM(ADJUSTL(aer_types(n)) ) == &
              TRIM(ADJUSTL(AD%spc(d)   ) )    ) do_spc_jac(n) = .TRUE.
        ENDDO
        
        ! The following works only if we have parameterized profiles
        IF( .NOT. use_aerprof ) THEN
          ! Species specific AOD jacobian
          DO d=1, AD%npar
            IF( TRIM(ADJUSTL(aer_types(n)) ) == &
                TRIM(ADJUSTL(AD%par(d)  )  )    ) do_spc_jac(n) = .TRUE.
          ENDDO
        ENDIF
      ENDDO
      
      ! Determine the number jacobians to compute
      AD%njac = 0
      DO n=1,naer
        IF( do_spc_jac(n) )  AD%njac = AD%njac + 1
      ENDDO
      
      ! Allocate array for jacobian species name
      IF( AD%njac > 0 ) THEN
        ALLOCATE( AD%jac(AD%njac)     ) ; AD%jac(:)     = ''
        ALLOCATE( AD%jac_idx(AD%njac) ) ; AD%jac_idx(:) = -1
        ALLOCATE( AD%wfidx(AD%njac)   ) ; AD%wfidx(:)   = -1
      ENDIF
      
      print*,do_spc_jac
      
      ! Get species
      ct = 0
      DO n=1,naer
        
        IF( do_spc_jac(n) ) THEN
          
          ! Add to element to the jacobian array
          ct = ct+1
          AD%jac_idx(ct) = n
          AD%jac(ct)     = aer_types(n)
          
          ! Match the index to the input arrays
          DO d=1, AD%nspc
            IF( TRIM(ADJUSTL(AD%jac(ct))   ) == &
                TRIM(ADJUSTL(AD%spc(d) )   )    ) AD%spc_idx(d) = ct
          ENDDO
          
          DO d=1, AD%npar
            IF( TRIM(ADJUSTL(AD%jac(ct))   ) == &
                TRIM(ADJUSTL(AD%par(d) )   )    ) AD%par_idx(d) = ct
          ENDDO
          
          
        ENDIF
        
      ENDDO
      
    END SUBROUTINE CHECK_AEROSOL_JAC_DIAGS
    
    SUBROUTINE TRANUC(text)
!
!     PURPOSE: Tranlate a character variable to all upper case letters.
!              Non-alphabetic characters are not affected.
!
!    COMMENTS: The original "text" is destroyed.
!
!     CODE DEPENDENCIES:
!      Routine Name                  File
!        N/A
!
!      AUTHOR: Robert D. Stewart
!        DATE: May 19, 1992
!
      CHARACTER*(*) text
      INTEGER iasc,i,ilen

      ilen = LEN(text)
      DO i=1,ilen
        iasc = ICHAR(text(i:i))
        IF ((iasc.GT.96).AND.(iasc.LT.123)) THEN
          text(i:i) = CHAR(iasc-32)
        ENDIF
      ENDDO

      ! Return to calling program
      END SUBROUTINE TRANUC
    
!------------------------------------------------------------------------------
!          Harvard University Atmospheric Chemistry Modeling Group            !
!------------------------------------------------------------------------------
!BOP
!
! !IROUTINE: read_one_line
!
! !DESCRIPTION: Subroutine READ\_ONE\_LINE reads a line from the input file.  
!  If the global variable VERBOSE is set, the line will be printed to stdout.  
!  READ\_ONE\_LINE can trap an unexpected EOF if LOCATION is passed.  
!  Otherwise, it will pass a logical flag back to the calling routine, 
!  where the error trapping will be done.
!\\
!\\
! !INTERFACE:
!
      FUNCTION READ_ONE_LINE( IU_GEOS, EOF, LOCATION ) RESULT( LINE )
!
! !USES:
!
!
! !INPUT PARAMETERS: 
!     
      INTEGER,          INTENT(IN)           :: IU_GEOS
      CHARACTER(LEN=*), INTENT(IN), OPTIONAL :: LOCATION    ! Msg to display
!
! !OUTPUT PARAMETERS:
!
      LOGICAL,          INTENT(OUT)          :: EOF         ! Denotes EOF 
! 
! !REVISION HISTORY: 
!  20 Jul 2004 - R. Yantosca - Initial version
!  27 Aug 2010 - R. Yantosca - Added ProTeX headers
!  03 Aug 2012 - R. Yantosca - Now make IU_GEOS a global module variable
!                              so that we can define it with findFreeLun
!  17 Sep 2013 - R. Yantosca - Extend line length to read in more tracers
!EOP
!------------------------------------------------------------------------------
!BOC
!
! !LOCAL VARIABLES:
!
      INTEGER            :: IOS
!------------------------------------------------------------------------------
! Prior to 9/17/13:
! Need to extend the line length for many more tracers (bmy, 9/17/13)
!      CHARACTER(LEN=255) :: LINE, MSG
!------------------------------------------------------------------------------
      CHARACTER(LEN=max_ch_len) :: LINE, MSG

      !=================================================================
      ! READ_ONE_LINE begins here!
      !=================================================================

      ! Initialize
      EOF = .FALSE.

      ! Read a line from the file
      READ( IU_GEOS, '(a)', IOSTAT=IOS ) LINE

      ! IO Status < 0: EOF condition
      IF ( IOS < 0 ) THEN
         EOF = .TRUE.

         ! Trap unexpected EOF -- stop w/ error msg if LOCATION is passed
         ! Otherwise, return EOF to the calling program
         IF ( PRESENT( LOCATION ) ) THEN
            MSG = 'READ_ONE_LINE: error at: ' // TRIM( LOCATION )
            WRITE( 6, '(a)' ) MSG
            WRITE( 6, '(a)' ) 'Unexpected end of file encountered!'
            WRITE( 6, '(a)' ) 'STOP in READ_ONE_LINE (input_mod.f)'
            WRITE( 6, '(a)' ) REPEAT( '=', 79 )
            STOP
         ELSE
            RETURN
         ENDIF
      ENDIF

      ! IO Status > 0: true I/O error condition
!       IF ( IOS > 0 ) CALL IOERROR( IOS, IU_GEOS, 'read_one_line:1' )

      ! Print the line (if necessary)
!       IF ( VERBOSE ) WRITE( 6, '(a)' ) TRIM( LINE )

      END FUNCTION READ_ONE_LINE

!EOC
!------------------------------------------------------------------------------
!          Harvard University Atmospheric Chemistry Modeling Group            !
!------------------------------------------------------------------------------
!BOP
!
! !IROUTINE: split_one_line
!
! !DESCRIPTION: Subroutine SPLIT\_ONE\_LINE reads a line from the input file 
!  (via routine READ\_ONE\_LINE), and separates it into substrings.
!\\
!\\
!  SPLIT\_ONE\_LINE also checks to see if the number of substrings found is 
!  equal to the number of substrings that we expected to find.  However, if
!  you don't know a-priori how many substrings to expect a-priori, 
!  you can skip the error check.
!\\
!\\
! !INTERFACE:
!
      SUBROUTINE SPLIT_ONE_LINE( funit, SUBSTRS, N_SUBSTRS, N_EXP, LOCATION ) 
!
! !USES:
!
!
! !INPUT PARAMETERS: 
!
      ! Number of substrings we expect to find
      INTEGER,            INTENT(IN)  :: funit, N_EXP

      ! Name of routine that called SPLIT_ONE_LINE
      CHARACTER(LEN=*),   INTENT(IN)  :: LOCATION 
!
! !OUTPUT PARAMETERS:
!
      ! Array of substrings (separated by " ")
      CHARACTER(LEN=max_ch_len), INTENT(OUT) :: SUBSTRS(max_ch_len)

      ! Number of substrings actually found
      INTEGER,            INTENT(OUT) :: N_SUBSTRS
      
      INTEGER, PARAMETER :: FIRSTCOL = 34

! 
! !REVISION HISTORY: 
!  20 Jul 2004 - R. Yantosca - Initial version
!  27 Aug 2010 - R. Yantosca - Added ProTeX headers
!  17 Sep 2013 - R. Yantosca - Extend LINE to 500 chars to allow more tracers
!EOP
!------------------------------------------------------------------------------
!BOC
!
! !LOCAL VARIABLES:
!
      LOGICAL                         :: EOF
!----------------------------------------------------------------------------
! Prior to 9/17/13:
! Extend LINE to 500 chars to allow more tracers (bmy, 9/17/13)
!      CHARACTER(LEN=255)              :: LINE, MSG
!----------------------------------------------------------------------------
      CHARACTER(LEN=max_ch_len)              :: LINE
      CHARACTER(LEN=max_ch_len)              :: MSG

      !=================================================================
      ! SPLIT_ONE_LINE begins here!
      !=================================================================      

      ! Create error msg
      MSG = 'SPLIT_ONE_LINE: error at ' // TRIM( LOCATION )

      !=================================================================
      ! Read a line from disk
      !=================================================================
      LINE = READ_ONE_LINE( funit, EOF )

      ! STOP on End-of-File w/ error msg
      IF ( EOF ) THEN
         WRITE( 6, '(a)' ) TRIM( MSG )
         WRITE( 6, '(a)' ) 'End of file encountered!' 
         WRITE( 6, '(a)' ) 'STOP in SPLIT_ONE_LINE (input_mod.f)!'
         WRITE( 6, '(a)' ) REPEAT( '=', 79 )
         STOP
      ENDIF

      !=================================================================
      ! Split the lines between spaces -- start at column FIRSTCOL
      !=================================================================
      CALL STRSPLIT( LINE(FIRSTCOL:), ' ', SUBSTRS, N_SUBSTRS )

      ! Sometimes we don't know how many substrings to expect,
      ! if N_EXP is greater than MAXDIM, then skip the error check
      IF ( N_EXP < 0 ) RETURN

      ! Stop if we found the wrong 
      IF ( N_EXP /= N_SUBSTRS ) THEN
         WRITE( 6, '(a)' ) TRIM( MSG )
         WRITE( 6, 100   ) N_EXP, N_SUBSTRS
         WRITE( 6, '(a)' ) 'STOP in SPLIT_ONE_LINE (input_mod.f)!'
         WRITE( 6, '(a)' ) REPEAT( '=', 79 )
         STOP
 100     FORMAT( 'Expected ',i2, ' substrs but found ',i3 )
      ENDIF
       
      END SUBROUTINE SPLIT_ONE_LINE

!------------------------------------------------------------------------------

  SUBROUTINE StrSplit( STR, SEP, RESULT, N_SUBSTRS )
!
!******************************************************************************
!  Subroutine STRSPLIT returns substrings in a string, separated by a 
!  separator character (similar to IDL's StrSplit function).  This is mainly
!  a convenience wrapper for CHARPAK routine TxtExt. (bmy, 7/11/02)
!
!  Arguments as Input:
!  ============================================================================
!  (1 ) STR       (CHARACTER*(*)) : String to be searched (variable length)  
!  (2 ) SEP       (CHARACTER*1  ) : Separator character
!
!  Arguments as Output:
!  ============================================================================
!  (3 ) RESULT    (CHARACTER*255) : Array containing substrings (255 elements)
!  (4 ) N_SUBSTRS (INTEGER      ) : Number of substrings returned (optional)
!
!  NOTES:
!******************************************************************************
!
      ! Arguments
      CHARACTER(LEN=*), INTENT(IN)            :: STR
      CHARACTER(LEN=1), INTENT(IN)            :: SEP
      CHARACTER(LEN=*), INTENT(OUT)           :: RESULT(max_ch_len)
      INTEGER,          INTENT(OUT), OPTIONAL :: N_SUBSTRS

      ! Local variables
      INTEGER                                 :: I, IFLAG, COL
      CHARACTER (LEN=max_ch_len)                     :: WORD

      !=================================================================
      ! STRSPLIT begins here!
      !=================================================================

      ! Initialize
      I         = 0
      COL       = 1 
      IFLAG     = 0
      RESULT(:) = ''
      
      ! Loop until all matches found, or end of string
      DO WHILE ( IFLAG == 0 )

         ! Look for strings beteeen separator string
         CALL TXTEXT ( SEP, TRIM( STR ), COL, WORD, IFLAG )

         ! Store substrings in RESULT array
         I         = I + 1
         RESULT(I) = TRIM( WORD )

      ENDDO

      ! Optional argument: return # of substrings found
      IF ( PRESENT( N_SUBSTRS ) ) N_SUBSTRS = I

      ! Return to calling program
    END SUBROUTINE StrSplit

!------------------------------------------------------------------------------

      SUBROUTINE StrRepl( STR, PATTERN, REPLTXT )

      !=================================================================
      ! Subroutine STRREPL replaces all instances of PATTERN within
      ! a string STR with replacement text REPLTXT. 
      ! (bmy, 6/25/02, 7/20/04)
      !
      ! Arguments as Input:
      ! ----------------------------------------------------------------
      ! (1 ) STR     : String to be searched
      ! (2 ) PATTERN : Pattern of characters to replace w/in STR
      ! (3 ) REPLTXT : Replacement text for PATTERN
      !
      ! Arguments as Output:
      ! ----------------------------------------------------------------
      ! (1 ) STR     : String with new replacement text 
      !
      ! NOTES
      ! (1 ) REPLTXT must have the same # of characters as PATTERN.
      ! (2 ) Replace LEN_TRIM with LEN (bmy, 7/20/04)
      !=================================================================

      ! Arguments
      CHARACTER(LEN=*), INTENT(INOUT) :: STR
      CHARACTER(LEN=*), INTENT(IN)    :: PATTERN, REPLTXT
      
      ! Local variables
      INTEGER                         :: I1, I2

      !=================================================================
      ! STRREPL begins here!
      !=================================================================

      ! Error check: make sure PATTERN and REPLTXT have the same # of chars
      IF ( LEN( PATTERN ) /= LEN( REPLTXT ) ) THEN 
         WRITE( 6, '(a)' ) REPEAT( '=', 79 )
         WRITE( 6, '(a)' ) 'STRREPL: PATTERN and REPLTXT must have same # of characters!'
         WRITE( 6, '(a)' ) 'STOP in STRREPL (charpak_mod.f)'
         WRITE( 6, '(a)' ) REPEAT( '=', 79 )
         STOP
      ENDIF

      ! Loop over all instances of PATTERN in STR
      DO 

         ! I1 is the starting location of PATTERN w/in STR  
         I1 = INDEX( STR, PATTERN )

         ! If pattern is not found, then return to calling program
         IF ( I1 < 1 ) RETURN

         ! I2 is the ending location of PATTERN w/in STR
         I2 = I1 + LEN_TRIM( PATTERN ) - 1
      
         ! Replace text
         STR(I1:I2) = REPLTXT

      ENDDO
         
      ! Return to calling program
      END SUBROUTINE StrRepl

!------------------------------------------------------------------------------


      SUBROUTINE TxtExt(ch,text,col,word,iflg)
!
!     PURPOSE: TxtExt extracts a sequence of characters from
!              text and transfers them to word.  The extraction
!              procedure uses a set of character "delimiters"
!              to denote the desired sequence of characters.
!              For example if ch=' ', the first character sequence
!              bracketed by blank spaces will be returned in word.
!              The extraction procedure begins in column, col,
!              of TEXT.  If text(col:col) = ch (any character in
!              the character string), the text is returned beginning
!              with col+1 in text (i.e., the first match with ch
!              is ignored).
!
!              After completing the extraction, col is incremented to
!              the location of the first character following the
!              end of the extracted text.
!
!              A status flag is also returned with the following
!              meaning(s)
!
!                 IF iflg = -1, found a text block, but no more characters
!                               are available in TEXT
!                    iflg = 0, task completed sucessfully (normal term)
!                    iflg = 1, ran out of text before finding a block of
!                              text.
!
!       COMMENTS: TxtExt is short for Text Extraction.  This routine
!                 provides a set of powerful line-by-line
!                 text search and extraction capabilities in
!                 standard FORTRAN.
!
!     CODE DEPENDENCIES:
!      Routine Name                  File
!        CntMat                    CHARPAK.FOR
!        TxtExt                    CHARPAK.FOR
!        FillStr                   CHARPAK.FOR
!        CopyTxt                   CHARPAK.FOR
!
!        other routines are indirectly called.
!      AUTHOR: Robert D. Stewart
!        DATE: Jan. 1st, 1995
!
!      REVISIONS: FEB 22, 1996.  Slight bug fix (introduced by a
!        (recent = FLIB 1.04) change in the CntMat routine)
!        so that TxtExt correctlyhandles groups of characters
!        delimited by blanks).
!
!      MODIFICATIONS by Bob Yantosca (6/25/02)
!        (1) Replace call to FILLSTR with F90 intrinsic REPEAT
!
      CHARACTER*(*) ch,text,word
      INTEGER col,iflg
      INTEGER Tmax,T1,T2,imat
      LOGICAL again,prev

!     Length of text
      Tmax = LEN(text)

!     Fill Word with blanks
      WORD = REPEAT( ' ', LEN( WORD ) )
      
      IF (col.GT.Tmax) THEN
!       Text does not contain any characters past Tmax.
!       Reset col to one and return flag = {error condition}
        iflg = 1
        col = 1
      ELSEIF (col.EQ.Tmax) THEN
!       End of TEXT reached
        CALL CntMat(ch,text(Tmax:Tmax),imat)
        IF (imat.EQ.0) THEN
!         Copy character into Word and set col=1
          CALL CopyTxt(1,Text(Tmax:Tmax),Word)
          col = 1
          iflg = -1
        ELSE
!         Same error condition as if col.GT.Tmax
          iflg = 1
        ENDIF
      ELSE
!       Make sure column is not less than 1
        IF (col.LT.1) col=1
        CALL CntMat(ch,text(col:col),imat)
        IF (imat.GT.0) THEN
          prev=.true.
        ELSE
          prev=.false.
        ENDIF
        T1=col
        T2 = T1

        again = .true.
        DO WHILE (again)
!         Check for a match with a character in ch
          CALL CntMat(ch,text(T2:T2),imat)
          IF (imat.GT.0) THEN
!           Current character in TEXT matches one (or more) of the
!           characters in ch.
            IF (prev) THEN
              IF (T2.LT.Tmax) THEN
!               Keep searching for a block of text
                T2=T2+1
                T1=T2
              ELSE
!               Did not find any text blocks before running
!               out of characters in TEXT.
                again=.false.
                iflg=1
              ENDIF
            ELSE
!             Previous character did not match ch, so terminate.
!             NOTE: This is "NORMAL" termination of the loop
              again=.false.
              T2=T2-1
              iflg = 0
            ENDIF
          ELSEIF (T2.LT.Tmax) THEN
!           Add a letter to the current block of text
            prev = .false.
            T2=T2+1
          ELSE
!           Reached the end of the characters in TEXT before reaching
!           another delimiting character.  A text block was identified
!           however.
            again=.false.
            iflg=-1
          ENDIF
        ENDDO

        IF (iflg.EQ.0) THEN
!         Copy characters into WORD and set col for return
          CALL CopyTxt(1,Text(T1:T2),Word)
          col = T2+1
        ELSE
!         Copy characters into WORD and set col for return
          CALL CopyTxt(1,Text(T1:T2),Word)
          col = 1
        ENDIF
      ENDIF

      ! Return to calling program
      END SUBROUTINE TxtExt

!------------------------------------------------------------------------------

      SUBROUTINE CntMat(str1,str2,imat)
!
!     Count the number of characters in str1 that match
!     a character in str2.
!
!     CODE DEPENDENCIES:
!      Routine Name                  File
!          LENTRIM                CharPak
!
!     DATE:   JAN. 6, 1995
!     AUTHOR: R.D. STEWART
!     COMMENTS: Revised slightly (2-5-1996) so that trailing
!               blanks in str1 are ignored.  Revised again
!               on 3-6-1996.
!
      CHARACTER*(*) str1,str2
      INTEGER imat
      INTEGER L1,L2,i,j
      LOGICAL again

      L1 = MAX(1,LEN_TRIM(str1))
      L2 = LEN(str2)
      imat = 0
      DO i=1,L1
        again = .true.
        j = 1
        DO WHILE (again)
          IF (str2(j:j).EQ.str1(i:i)) THEN
            imat = imat+1
            again = .false.
          ELSEIF (j.LT.L2) THEN
            j=j+1
          ELSE
            again = .false.
          ENDIF
        ENDDO
      ENDDO

      ! Return to calling program
      END SUBROUTINE CntMat

!------------------------------------------------------------------------------

      SUBROUTINE CopyTxt(col,str1,str2)
!
!     PURPOSE: Write all of the characters in str1 into variable
!              str2 beginning at column, col.  If the length of str1
!              + col is longer than the number of characters str2
!              can store, some characters will not be transfered to
!              str2.  Any characters already existing in str2 will
!              will be overwritten.
!
!     CODE DEPENDENCIES:
!      Routine Name                  File
!        N/A
!
!     DATE:   DEC. 24, 1993
!     AUTHOR: R.D. STEWART
!
      CHARACTER*(*) str2,str1
      INTEGER col,ilt1,i1,i,j,ic

      i1 = LEN(str2)
      IF (i1.GT.0) THEN
        ilt1 = LEN(str1)
        IF (ilt1.GT.0) THEN
          ic = MAX0(col,1)
          i = 1
          j = ic
          DO WHILE ((i.LE.ilt1).and.(j.LE.i1))
            str2(j:j) = str1(i:i)
            i = i + 1
            j = ic + (i-1)
          ENDDO
        ENDIF
      ENDIF

      ! Return to calling program
      END SUBROUTINE CopyTxt

!------------------------------------------------------------------------------

!BOP
!
! !IROUTINE:  read_control_file
!
! !DESCRIPTION: This routine does something to the input variable and returns
! the result in the output variable.
!\\
!\\
! !INTERFACE: read_control_file
!
    SUBROUTINE read_control_file(error)

      USE GC_time_module, ONLY: GET_DAY_OF_YEAR
      IMPLICIT NONE

!
! !INPUT/OUTPUT PARAMETERS: 
!
      LOGICAL,       INTENT(INOUT) :: error ! Error variable
!
! !REVISION HISTORY: 
!  Apri 2013 - G. Gonzalez Abad - Initial Version
!
!EOP
!BOC

      ! ---------------
      ! Input file name
      ! ---------------
      CHARACTER(LEN=max_ch_len) :: fname
      INTEGER                   :: funit

      ! ---------------
      ! Temporal string
      ! ---------------
      CHARACTER(LEN=max_ch_len) :: tmpstr

      ! ---------------
      ! Index variables
      ! ---------------
      INTEGER :: i

      ! ------
      ! Errors
      ! ------
      INTEGER :: ios

      ! ---------------------------------
      ! Strings in the Control Input file
      ! ---------------------------------
      CHARACTER(LEN=13), PARAMETER :: output_str        = 'Output folder'
      CHARACTER(LEN=18), PARAMETER :: database_str      = 'Database directory'
      CHARACTER(LEN=21), PARAMETER :: profile_str       = 'Profile data filename'
      CHARACTER(LEN=18), PARAMETER :: footprint_str     = 'Use footprint info'
      CHARACTER(LEN=13), PARAMETER :: debug_str         = 'Debug geocape'
      CHARACTER(LEN=19), PARAMETER :: normalizedrd_str  = 'Normalized radiance'
      CHARACTER(LEN=13), PARAMETER :: solarphotons_str  = 'Solar photons'
      CHARACTER(LEN=20), PARAMETER :: normalizedwf_str  = 'Normalized WF output'
      CHARACTER(LEN=13), PARAMETER :: stokes_str        = 'Stokes output'
      CHARACTER(LEN=16), PARAMETER :: airmass_str       = 'Air mass factors'
      CHARACTER(LEN=21), PARAMETER :: tjacobians_str    = 'Temperature jacobians'
      CHARACTER(LEN=26), PARAMETER :: spjacobians_str   = 'Surface pressure jacobians'
      CHARACTER(LEN=24), PARAMETER :: effcross_str      = 'Effective cross sections'
      CHARACTER(LEN= 8), PARAMETER :: spectral_str      = 'Spectral'
      CHARACTER(LEN= 5), PARAMETER :: gases_str         = 'Gases'
      CHARACTER(LEN= 6), PARAMETER :: albedo_str        = 'Albedo'
      CHARACTER(LEN= 8), PARAMETER :: aerosols_str      = 'Aerosols'
      CHARACTER(LEN= 6), PARAMETER :: clouds_str        = 'Clouds'
      CHARACTER(LEN=14), PARAMETER :: useraltitudes_str = 'User altitudes'
      CHARACTER(LEN=25), PARAMETER :: terrainht_str     = 'Terrain height adjustment'
      
      ! ----------------
      ! Code starts here
      ! ----------------
      error = .FALSE.

      ! -----------------
      ! Control file unit
      ! -----------------
      funit = ctrunit

      ! ---------------------------------------------------
      ! The tool will spect to get the filename as argument
      ! ---------------------------------------------------
      CALL GETARG(1, fname)

      ! -----------------
      ! Open control file
      ! -----------------
      OPEN (UNIT=funit, FILE=TRIM(ADJUSTL(fname)), ACTION='READ', STATUS='OLD', IOSTAT=ios )
      IF ( ios /= 0 ) THEN
         CALL write_err_message ( .TRUE., 'ERROR: Unable to open '//TRIM(ADJUSTL(fname)) )
         CALL error_exit (error)
      END IF

      ! --------------
      ! Results folder
      ! --------------
      REWIND (funit)
      CALL skip_to_filemark ( funit, output_str , tmpstr, error )
      IF ( error ) THEN
         CALL write_err_message ( .TRUE., "ERROR: Can't find "//output_str )
         CALL error_exit (error)
      END IF
      READ (UNIT=funit, FMT='(A)', IOSTAT=ios) results_dir

      ! ---------------
      ! Database folder
      ! ---------------
      REWIND (funit)
      CALL skip_to_filemark ( funit, database_str , tmpstr, error )
      IF ( error ) THEN
         CALL write_err_message ( .TRUE., "ERROR: Can't find "//database_str )
         CALL error_exit (error)
      END IF
      READ (UNIT=funit, FMT='(A)', IOSTAT=ios) database_dir

      ! ---------------------
      ! Profile data filename
      ! ---------------------
      REWIND (funit)
      CALL skip_to_filemark ( funit, profile_str , tmpstr, error )
      IF ( error ) THEN
         CALL write_err_message ( .TRUE., "ERROR: Can't find "//profile_str )
         CALL error_exit (error)
      END IF
      READ (UNIT=funit, FMT='(A)', IOSTAT=ios) profile_data_filename

      ! ------------------
      ! Use footprint info
      ! ------------------
      REWIND (funit)
      CALL skip_to_filemark ( funit, footprint_str , tmpstr, error )
      IF ( error ) THEN
         CALL write_err_message ( .TRUE., "ERROR: Can't find "//footprint_str )
         CALL error_exit (error)
      END IF
      READ (UNIT=funit, FMT=*, IOSTAT=ios) use_footprint_info
      ! ---------
      ! Footprint
      ! ---------
      READ (UNIT=funit, FMT=*, IOSTAT=ios) year, month, day, utc
      READ (UNIT=funit, FMT=*, IOSTAT=ios) longitude, latitude, GC_TerrainHeight
      READ (UNIT=funit, FMT=*, IOSTAT=ios) satlon, satlat, satalt
      READ (UNIT=funit, FMT=*, IOSTAT=ios) do_sat_viewcalc
      READ (UNIT=funit, FMT=*, IOSTAT=ios) clon
      READ (UNIT=funit, FMT=*, IOSTAT=ios) clat
      
      ! Also compute day of year from y/m/d
      day_of_year = GET_DAY_OF_YEAR(month,day,year,0,0,0.0d0)
      
      ! ---------------------------------
      ! Terrain height profile adjustment
      ! ---------------------------------
      REWIND (funit)
      CALL skip_to_filemark ( funit, terrainht_str , tmpstr, error )
      IF ( error ) THEN
         CALL write_err_message ( .TRUE., "ERROR: Can't find "//terrainht_str )
         CALL error_exit (error)
      END IF
      READ (UNIT=funit, FMT=*, IOSTAT=ios) GC_do_terrain_adjust
      READ (UNIT=funit, FMT='(A)', IOSTAT=ios) dem_fname
      READ (UNIT=funit, FMT=*, IOSTAT=ios) DEM_interp_opt
      
      ! -------------
      ! Debug Geocape
      ! -------------
      REWIND (funit)
      CALL skip_to_filemark ( funit, debug_str , tmpstr, error )
      IF ( error ) THEN
         CALL write_err_message ( .TRUE., "ERROR: Can't find "//debug_str )
         CALL error_exit (error)
      END IF
      READ (UNIT=funit, FMT=*, IOSTAT=ios) do_debug_geocape_tool
      READ (UNIT=funit, FMT='(A)', IOSTAT=ios) debug_filename

      ! -------------
      ! Stokes output
      ! -------------
      REWIND (funit)
      CALL skip_to_filemark ( funit, stokes_str , tmpstr, error )
      IF ( error ) THEN
         CALL write_err_message ( .TRUE., "ERROR: Can't find "//stokes_str )
         CALL error_exit (error)
      END IF
      READ (UNIT=funit, FMT=*, IOSTAT=ios) do_StokesQU_output

      ! ---------------
      ! AMF calculation
      ! ---------------
      REWIND (funit)
      CALL skip_to_filemark ( funit, airmass_str , tmpstr, error )
      IF ( error ) THEN
         CALL write_err_message ( .TRUE., "ERROR: Can't find "//airmass_str )
         CALL error_exit (error)
      END IF
      READ (UNIT=funit, FMT=*, IOSTAT=ios) do_AMF_calculation

      ! ---------------------
      ! Temperature jacobians
      ! ---------------------
      REWIND (funit)
      CALL skip_to_filemark ( funit, tjacobians_str , tmpstr, error )
      IF ( error ) THEN
         CALL write_err_message ( .TRUE., "ERROR: Can't find "//tjacobians_str )
         CALL error_exit (error)
      END IF
      READ (UNIT=funit, FMT=*, IOSTAT=ios) do_T_Jacobians

      ! --------------------------
      ! Surface pressure jacobians
      ! --------------------------
      REWIND (funit)
      CALL skip_to_filemark ( funit, spjacobians_str , tmpstr, error )
      IF ( error ) THEN
         CALL write_err_message ( .TRUE., "ERROR: Can't find "//spjacobians_str )
         CALL error_exit (error)
      END IF
      READ (UNIT=funit, FMT=*, IOSTAT=ios) do_sfcprs_Jacobians
      
      ! --------------------
      ! Normalized WF output
      ! --------------------
      REWIND (funit)
      CALL skip_to_filemark ( funit, normalizedwf_str , tmpstr, error )
      IF ( error ) THEN
         CALL write_err_message ( .TRUE., "ERROR: Can't find "//normalizedwf_str )
         CALL error_exit (error)
      END IF
      READ (UNIT=funit, FMT=*, IOSTAT=ios) do_normalized_WFoutput

      ! -------------------
      ! Normalized radiance
      ! -------------------
      REWIND (funit)
      CALL skip_to_filemark ( funit, normalizedrd_str , tmpstr, error )
      IF ( error ) THEN
         CALL write_err_message ( .TRUE., "ERROR: Can't find "//normalizedrd_str )
         CALL error_exit (error)
      END IF
      READ (UNIT=funit, FMT=*, IOSTAT=ios) do_normalized_radiance
      READ (UNIT=funit, FMT='(A)', IOSTAT=ios) solar_spec_filename

      ! -------------
      ! Solar photons
      ! -------------
      REWIND (funit)
      CALL skip_to_filemark ( funit, solarphotons_str , tmpstr, error )
      IF ( error ) THEN
         CALL write_err_message ( .TRUE., "ERROR: Can't find "//solarphotons_str )
         CALL error_exit (error)
      END IF
      READ (UNIT=funit, FMT=*, IOSTAT=ios) use_solar_photons

      ! ------------------------
      ! Effective cross sections
      ! ------------------------
      REWIND (funit)
      CALL skip_to_filemark ( funit, effcross_str , tmpstr, error )
      IF ( error ) THEN
         CALL write_err_message ( .TRUE., "ERROR: Can't find "//effcross_str )
         CALL error_exit (error)
      END IF
      READ (UNIT=funit, FMT=*, IOSTAT=ios) do_effcrs

      ! --------
      ! Spectral
      ! --------
      REWIND (funit)
      CALL skip_to_filemark ( funit, spectral_str , tmpstr, error )
      IF ( error ) THEN
         CALL write_err_message ( .TRUE., "ERROR: Can't find "//spectral_str )
         CALL error_exit (error)
      END IF
      READ (UNIT=funit, FMT=*, IOSTAT=ios) use_wavelength
      READ (UNIT=funit, FMT=*, IOSTAT=ios) lambda_start, lambda_finish, lambda_resolution
      READ (UNIT=funit, FMT=*, IOSTAT=ios) lambda_dw, lambda_dfw


      ! -----
      ! Gases
      ! -----
      REWIND (funit)
      CALL skip_to_filemark ( funit, gases_str , tmpstr, error )
      IF ( error ) THEN
         CALL write_err_message ( .TRUE., "ERROR: Can't find "//gases_str )
         CALL error_exit (error)
      END IF
      READ (UNIT=funit, FMT=*, IOSTAT=ios) ngases
      READ (UNIT=funit, FMT=*, IOSTAT=ios) (which_gases(i), i=1, ngases)

      ! ------
      ! Albedo
      ! ------
      REWIND (funit)
      CALL skip_to_filemark ( funit, albedo_str , tmpstr, error )
      IF ( error ) THEN
         CALL write_err_message ( .TRUE., "ERROR: Can't find "//albedo_str )
         CALL error_exit (error)
      END IF
      READ (UNIT=funit, FMT=*, IOSTAT=ios) use_lambertian, use_fixedalbedo, use_albeofs
      READ (UNIT=funit, FMT=*, IOSTAT=ios) do_eof_brdf
      READ (UNIT=funit, FMT=*, IOSTAT=ios) fixed_albedo
      READ (UNIT=funit, FMT=*, IOSTAT=ios) use_albspectra
      READ (UNIT=funit, FMT='(A)', IOSTAT=ios) albspectra_fname
      READ (UNIT=funit, FMT=*, IOSTAT=ios) wind_speed

      ! --------
      ! Aerosols
      ! --------
      REWIND (funit)
      CALL skip_to_filemark ( funit, aerosols_str , tmpstr, error )
      IF ( error ) THEN
         CALL write_err_message ( .TRUE., "ERROR: Can't find "//aerosols_str )
         CALL error_exit (error)
      END IF
      READ (UNIT=funit, FMT=*, IOSTAT=ios) do_aerosols
      READ (UNIT=funit, FMT=*, IOSTAT=ios) use_aerprof
      READ (UNIT=funit, FMT='(A)', IOSTAT=ios) aerfile
      
      ! ------
      ! Clouds
      ! ------
      REWIND (funit)
      CALL skip_to_filemark ( funit, clouds_str , tmpstr, error )
      IF ( error ) THEN
         CALL write_err_message ( .TRUE., "ERROR: Can't find "//clouds_str )
         CALL error_exit (error)
      END IF
      READ (UNIT=funit, FMT=*, IOSTAT=ios) do_clouds, do_lambertian_cld
      READ (UNIT=funit, FMT=*, IOSTAT=ios) use_cldprof, do_npix_cloud
      READ (UNIT=funit, FMT='(A)', IOSTAT=ios) cldfile

      ! --------------
      ! User altitudes
      ! --------------
      REWIND (funit)
      CALL skip_to_filemark ( funit, useraltitudes_str , tmpstr, error )
      IF ( error ) THEN
         CALL write_err_message ( .TRUE., "ERROR: Can't find "//useraltitudes_str )
         CALL error_exit (error)
      END IF
      READ (UNIT=funit, FMT=*, IOSTAT=ios) GC_do_user_altitudes
      READ (UNIT=funit, FMT=*, IOSTAT=ios) GC_n_user_altitudes
      READ (UNIT=funit, FMT=*, IOSTAT=ios) (GC_user_altitudes(i), i=1, GC_n_user_altitudes)

      CLOSE(funit)

    END SUBROUTINE read_control_file
!EOC

!------------------------------------------------------------------------------
!          Harvard University Atmospheric Chemistry Modeling Group            !
!------------------------------------------------------------------------------
!BOP
!
! !IROUTINE:  skip_to_filemark
!
! !DESCRIPTION: This routine does something to the input variable and returns
! the result in the output variable.
!\\
!\\
! !INTERFACE:
!
  SUBROUTINE skip_to_filemark ( funit, fmark, lastline, error )

    IMPLICIT NONE
!
! !INPUT PARAMETERS: 
!
    INTEGER,          INTENT(IN)  :: funit ! File unit 
    CHARACTER(LEN=*), INTENT(IN)  :: fmark ! String header
!
! !INPUT/OUTPUT PARAMETERS: 
!
    LOGICAL,          INTENT(INOUT) :: error ! Error variable
!
! !OUTPUT PARAMETERS:
!
    CHARACTER(LEN=*), INTENT(OUT) :: lastline ! Output variable
!
! !REVISION HISTORY: 
!  Apri 2013 - G. Gonzalez Abad - Initial Version
!
!EOP
!------------------------------------------------------------------------------
!BOC
    ! ---------------
    ! Local variables
    ! ---------------
    INTEGER                    :: lmlen, ios
    CHARACTER (LEN=max_ch_len) :: tmpline

    ! -------------------------------------------
    ! Determine the length of the string landmark
    ! -------------------------------------------
    lmlen = LEN(TRIM(ADJUSTL(fmark)))
  
    ! -------------------------------------------------------
    ! Read lines in the file until we either find the string,
    ! reach the end of the file, or reading fails otherwise.
    ! ----------------------------------------------------
    ios = 0
    getlm: DO WHILE ( ios == 0 )
       READ (UNIT=funit, FMT='(A)', IOSTAT=ios) tmpline
       tmpline = TRIM(ADJUSTL(tmpline))
       IF ( ios /= 0 .OR. tmpline(1:lmlen) == fmark ) EXIT getlm
    END DO getlm

    ! ---------------------------------------------------
    ! Return the last line read for the case that we need 
    ! to extract further information from it
    ! ---------------------------------------------------
    lastline = TRIM(ADJUSTL(tmpline))

    ! -----------------------------------------
    ! Set error flag if READ was not successful
    ! -----------------------------------------
    IF ( ios /= 0 ) error = .TRUE.
    IF ( ios ==  0 ) error = .FALSE.

    RETURN
  END SUBROUTINE skip_to_filemark
!EOC

!BOP
!
! !IROUTINE:  check_input
!
! !DESCRIPTION: This routine checks for some incosistencies in the input values and
!               changes values accordingly.
!\\
!\\
! !INTERFACE: check_input(error)
!
  SUBROUTINE check_input(error)

    IMPLICIT NONE
!
! !INPUT/OUTPUT PARAMETERS: 
!
    LOGICAL,       INTENT(INOUT) :: error ! Error variable
!
! !REVISION HISTORY: 
!  Apri 2013 - G. Gonzalez Abad - Initial Version
!
!EOP
!BOC

    ! ---------------
    ! Local variables
    ! ---------------
    INTEGER :: i
    
    error = .FALSE.
      
    DO i = 1, LEN(results_dir)
       IF (results_dir(i:i) == ' ') THEN
          results_dir = results_dir(1:i-1); EXIT
       ENDIF
    ENDDO

    DO i = 1, LEN(database_dir)
       IF (database_dir(i:i) == ' ') THEN
          database_dir = database_dir(1:i-1); EXIT
       ENDIF
    ENDDO

    DO i = 1, LEN(aerfile)
       IF (aerfile(i:i) == ' ') THEN
          aerfile = aerfile(1:i-1); EXIT
       ENDIF
    ENDDO

    DO i = 1, LEN(cldfile)
       IF (cldfile(i:i) == ' ') THEN
          cldfile = cldfile(1:i-1); EXIT
       ENDIF
    ENDDO

    DO i = 1, LEN(profile_data_filename)
       IF (profile_data_filename(i:i) == ' ') THEN
          profile_data_filename = profile_data_filename(1:i-1); EXIT
       ENDIF
    ENDDO

    DO i = 1, LEN(debug_filename)
       IF (debug_filename(i:i) == ' ') THEN
          debug_filename = debug_filename(1:i-1); EXIT
       ENDIF
    ENDDO
    debug_filename = TRIM(ADJUSTL(results_dir)) // debug_filename
    
    !  Exception handling this section
    IF (error) CALL error_exit(error)
    
    END SUBROUTINE check_input
!BOP
!
! !IROUTINE:  prepare_wavegrid( error )
!
! !DESCRIPTION: This routine prepares the wavelength grids used by the tool
!\\
!\\
! !INTERFACE: prepare_wavegrid(error)
    SUBROUTINE prepare_wavegrid( error )

    IMPLICIT NONE
!
! !INPUT/OUTPUT PARAMETERS: 
!
    LOGICAL,       INTENT(INOUT) :: error ! Error variable
!
! !REVISION HISTORY: 
!  Apri 2013 - G. Gonzalez Abad - Initial Version
!
!EOP
!BOC
    
    ! ---------------
    ! Local variables
    ! ---------------
    INTEGER :: du, w
    
    !  set wavelength grid
    IF (lambda_resolution < 0.0 ) THEN
       CALL write_err_message ( .FALSE., "Spectral resolution must be >= 0.0!!!")
       error = .TRUE.
    END IF
    
    IF (lambda_resolution == 0.0 .AND. lambda_dw /= lambda_dfw) THEN
       CALL write_err_message ( .FALSE., "Fine and coarse grids must be the same "// &
                                        "for high-resolution calculation!!!")
       error = .TRUE.
    END IF
    
    IF (lambda_resolution == 0.0) THEN
       do_effcrs = .FALSE.
    END IF
    
    ! Number of wavelengths on coarse grid
    nclambdas = NINT ( (lambda_finish - lambda_start ) / lambda_dw ) + 1
    
    ! Allocate coarse grid wavelength array
    ALLOCATE( clambdas(nclambdas) )
    ALLOCATE( cwavnums(nclambdas) )
    
    ! Compute grid
    clambdas(1) = lambda_start
    DO w = 2, nclambdas
       clambdas(w) = clambdas(w-1) + lambda_dw
    END DO
    
    !  For high-resolution grid, need to have extra wavelength space for convolution
    edge_dw = 2.0 * lambda_resolution  
    nflambdas = NINT ( (lambda_finish - lambda_start + 2.0 * edge_dw) / lambda_dfw ) + 1
    
    ! Allocate fine wavelength grid array
    ALLOCATE( flambdas(nflambdas) )
    ALLOCATE( fwavnums(nflambdas) ) 
    
    ! Compute grid
    flambdas(1) = lambda_start - edge_dw
    DO w = 2, nflambdas
       flambdas(w) = flambdas(w-1) + lambda_dfw
    END DO
    
    ! Wavenumber grid
    IF (.NOT. use_wavelength ) THEN
       
       ! Switch grid assignment
       cwavnums(1:nclambdas) = clambdas(1:nclambdas)
       fwavnums(1:nflambdas) = flambdas(1:nflambdas)
       
       ! Compute wavelength grid
       clambdas(1:nclambdas) = 1.0D+07 / cwavnums(1:nclambdas)
       flambdas(1:nflambdas) = 1.0D+07 / fwavnums(1:nflambdas)
    ELSE
       
       ! Compute wavenumber grid
       cwavnums(1:nclambdas) = 1.0D+07 / clambdas(1:nclambdas)
       fwavnums(1:nflambdas) = 1.0D+07 / flambdas(1:nflambdas)
    END IF
    
    
    ! ====================================================================================
    ! ====================================================================================
    
    ! Fine grid will be used for reading solar irradiance spectrum, and cross sections
    ! no matter whether do_effcrs is true or false
!     nlambdas = nflambdas
!     lambdas(1:nlambdas) = flambdas(1:nflambdas)
!     wavnums(1:nlambdas) = fwavnums(1:nflambdas)
    
    ! The above seems confusing as lambdas is reset later in the cross section code 
    ! when the effective cross section option is set. Here lets make lambdas correspond
    ! to the VLIDORT grid (i.e. optical properties), and use flambdas/clambdas where appropriate
    
    IF( do_effcrs ) THEN
      nlambdas = nclambdas
      ALLOCATE( lambdas(nlambdas) )
      ALLOCATE( wavnums(nlambdas) )
      lambdas(1:nlambdas) = clambdas(1:nclambdas)
      wavnums(1:nlambdas) = cwavnums(1:nclambdas)
    ELSE
      nlambdas = nflambdas
      ALLOCATE( lambdas(nlambdas) )
      ALLOCATE( wavnums(nlambdas) )
      lambdas(1:nlambdas) = flambdas(1:nflambdas)
      wavnums(1:nlambdas) = fwavnums(1:nflambdas)
    ENDIF
    
    ! ====================================================================================
    ! ====================================================================================
    
    IF (maxlambdas > maxflambdas) THEN
       CALL write_err_message ( .FALSE., "maxlambdas sould be <= maxflambdas!")
       error = .TRUE.
    ENDIF
    
    IF (.NOT. do_effcrs .AND. maxlambdas /= maxflambdas) THEN
       CALL write_err_message ( .FALSE., "Need to set maxlambdas = maxflambdas "// &
            "for fine-grid calculation!")
       error = .TRUE.
    END IF
    
    IF ( nclambdas > maxlambdas ) THEN
       CALL write_err_message ( .FALSE., "Need to increase maxlambdas!")
       error = .TRUE.
    END IF
    
    IF ( nflambdas > maxflambdas ) THEN
       CALL write_err_message ( .FALSE., "Need to increase maxflambdas!")
       error = .TRUE.
   END IF
   
   !  Exception handling this section
   IF (error) CALL error_exit(error)
   
   !  Open debug file if flagged. The unit number is 'du'
   IF ( do_debug_geocape_tool ) THEN
      du=66
      OPEN(du, FILE=debug_filename, STATUS = 'unknown')
   END IF
   
 END SUBROUTINE prepare_wavegrid
 !EOC

END MODULE GC_read_input_module
