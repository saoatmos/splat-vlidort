MODULE GC_netcdf_module

  USE GC_parameters_module, ONLY: max_ch_len, maxgases
  USE GC_variables_module,  ONLY: latitude, longitude, year, month, day, utc, VLIDORT_ModIn, nlambdas, &
                                  lambdas, GC_nlayers, heights, pressures, temperatures, ngases,       &
                                  which_gases, gas_partialcolumns, aircolumns, cfrac, cld_tops,        &
                                  lambertian_cldalb, taer_profile, tcld_profile, opdeps, ssalbs,       &
                                  aer_opdeps, aer_ssalbs, cld_opdeps, cld_ssalbs, ground_ler,          &
                                  wind_speed, VLIDORT_FixIn, lambda_dw, clambdas, nclambdas,           &
                                  lambda_dfw, use_wavelength, aer_reflambda, cld_reflambda,            &
                                  do_vector_calculation, do_StokesQU_output, do_Jacobians,             &
                                  do_QU_Jacobians, do_AMF_calculation, do_T_Jacobians,                 &
                                  do_sfcprs_Jacobians, do_aod_Jacobians, do_aerosols,                  &
                                  do_clouds, do_assa_Jacobians,                                        &
                                  do_cod_Jacobians, do_cssa_Jacobians, do_cfrac_Jacobians,             &
                                  do_aer_columnwf, do_cld_columnwf, do_normalized_WFoutput,            &
                                  do_normalized_radiance, use_lambertian, do_lambertian_cld, do_effcrs,&
                                  use_solar_photons, lambda_resolution, solar_cspec_data, GC_radiances,&
                                  GC_Qvalues, GC_Uvalues, GC_Tracegas_Jacobians, GC_Scattering_Weights,&
                                  GC_AMFs, GC_Tracegas_QJacobians, GC_Tracegas_UJacobians,             &
                                  GC_Temperature_Jacobians, GC_Surfalbedo_Jacobians,                   &
                                  GC_Surfalbedo_QJacobians, GC_Surfalbedo_UJacobians,                  &
                                  GC_Windspeed_Jacobians, GC_WindSpeed_QJacobians,                     &
                                  GC_Windspeed_UJacobians, GC_aod_Jacobians, GC_aod_QJacobians,        &
                                  GC_aod_UJacobians, GC_assa_Jacobians, GC_assa_QJacobians,            &
                                  GC_assa_UJacobians, GC_cod_Jacobians, GC_cod_QJacobians,             &
                                  GC_cod_UJacobians, GC_cssa_Jacobians, GC_cssa_QJacobians,            &
                                  GC_cssa_UJacobians, GC_cfrac_Jacobians, GC_cfrac_QJacobians,         &
                                  GC_cfrac_UJacobians, GC_sfcprs_Jacobians, GC_sfcprs_QJacobians,      &
                                  GC_sfcprs_UJacobians, results_dir, GC_n_sun_positions,               &
                                  GC_n_view_angles, GC_n_azimuths, VLIDORT_Out,                        &
                                  GC_flux, GC_Qflux, GC_Uflux, GC_direct_flux, GC_Qdirect_flux,        &
                                  GC_Udirect_flux, Total_brdf, NSTOKESSQ, do_brdf_surface,             &
                                  OUTPUT_WSABSA, WSA_CALCULATED, BSA_CALCULATED, didx, W,              &
                                  GC_user_altitudes, GC_n_user_levels, ilev, GC_user_levels,           &
                                  use_albeofs, temis_uv_alb, alb_eof_coeff, alb_eof_lfrac,             &
                                  alb_eof_iflg, temis_sf, temis_uv_wav, write_view_geom,               &
                                  write_footprint_info,write_altitude_prof,write_pres_prof,            &
                                  write_temp_prof,write_aircol_prof,write_aircol_prof,write_aod_prof,  &
                                  write_assa_prof,write_cod_prof,write_cssa_prof,write_gas_prof,       &
                                  write_gas_names,write_wvl_grid,write_rad_spc,write_flx_spc,          &
                                  write_dflx_spc,write_irad_spc,write_alb_spc,write_fix_brdf_par,      &
                                  write_full_brdf,write_modis_eof_par,do_surfalb_jacobian,             &
                                  do_wspd_jacobian,do_gas_jacobians,write_aod_refwvl,write_cod_refwvl, &
                                  write_od_prof,write_ssa_prof, nc_imx, nc_jmx, VBRDF_Sup_In,          &
                                  nc_xid, nc_yid, yn_ascii_profile, GC_rrs_Jacobian, do_raman_jacobian,&
                                  gas_xsecs,write_gas_xsecs, do_yn_obsalt, ncld, do_stokes_ad20,       &
                                  do_stokes_ad21, do_stokes_ad22,GC_spcaod_Jacobians,                  &
                                  GC_spcaod_QJacobians,GC_spcaod_UJacobians, GC_spcassa_Jacobians,     &
                                  GC_spcassa_QJacobians,GC_spcassa_UJacobians,skip_xy_pixel
  USE GC_error_module

  IMPLICIT NONE
  
  PUBLIC :: netcdf_handle_error

CONTAINS
  
  
  SUBROUTINE define_ncdf_dim( ncid, ncdim, error )
    
    USE GC_variables_module,  ONLY : NCDimType
    
    IMPLICIT NONE
    INCLUDE 'netcdf.inc'
    
    ! --------------------
    ! Subroutine arguments
    ! --------------------
    INTEGER,           INTENT(IN   ) :: ncid
    TYPE( NCDimType ), INTENT(INOUT) :: ncdim
    LOGICAL,           INTENT(INOUT) :: error ! Error variable
    
    ! ---------------
    ! Local variables
    ! ---------------
    
    INTEGER :: rcode
    
    !==============================================================================
    ! define_ncdf_dim starts here
    !==============================================================================
    
    ! Initialize error
    error = .FALSE.
    
    ! Create the dimensions of the dataset:
    rcode = NF_DEF_DIM(ncid, &
                       TRIM(ADJUSTL(ncdim%NAME)), &
                       ncdim%DIMSIZE,             &
                       ncdim%NCID                 )
    
    ! Handle error
    CALL netcdf_handle_error(ncdim%NAME,rcode)
    
  END SUBROUTINE define_ncdf_dim
  
  SUBROUTINE close_ncdf_file( ncid, error )
    
    USE GC_variables_module,  ONLY : nc_ndimlist, nc_dimlist
    
    IMPLICIT NONE
    INCLUDE 'netcdf.inc'
    
    ! --------------------
    ! Subroutine arguments
    ! --------------------
    INTEGER,           INTENT(IN   ) :: ncid
    LOGICAL,           INTENT(INOUT) :: error ! Error variable
    
    ! ---------------
    ! Local variables
    ! ---------------
    
    INTEGER :: rcode, n
    
    !==============================================================================
    ! close_ncdf_file starts here
    !==============================================================================
    
    ! Initialize error
    error = .FALSE.
    
    ! Create the dimensions of the dataset:
    rcode = NF_CLOSE(ncid)
    CALL netcdf_handle_error('close_ncdf_file',rcode)
    
    ! Set diagnostic dim ids to -1
    DO n=1, nc_ndimlist
      nc_dimlist(n)%NCID = -1
    ENDDO
    
  END SUBROUTINE close_ncdf_file
  
  SUBROUTINE allocate_output_arrays(error)
    
    USE GC_parameters_module, ONLY : maxlambdas,      &
                                     GC_maxuserlevels,&
                                     GC_maxgeometries,&
                                     GC_maxlayers, maxmoms, maxgksec
    USE GC_variables_module,  ONLY : GC_n_user_altitudes, &
                                     nc_dimlist, nc_ndimlist, &
                                     do_output_as_xy_nc, &
                                     nc_imx, nc_jmx, &
                                     Diag28_InpOpt,  &
                                     Diag34_InpOpt, Diag35_InpOpt, &
                                     aerdiag_phasemoms, &
                                     do_total_aod_jac, do_total_assa_jac,&
                                     do_stokes_ad31, GC_airdens_Jacobians,&
                                     GC_airdens_QJacobians,GC_airdens_UJacobians,&
                                     do_airdens_jac
                                     
    USE VLIDORT_PARS,         ONLY : MAX_BRDF_PARAMETERS
    IMPLICIT NONE
    
    ! ------------------
    ! Modified variables
    ! ------------------
    LOGICAL,       INTENT(INOUT) :: error
    INTEGER                      :: ngeom, ct
    ! ================================================================
    ! allocate_output_arrays Starts here
    ! ================================================================
    
    ! Initialize error
    error = .FALSE.
    
    ! Radiance arrays are always allocated
    ALLOCATE(GC_radiances(nlambdas,GC_n_user_altitudes,GC_maxgeometries,2))
    ALLOCATE(GC_flux(nlambdas,GC_n_user_altitudes,GC_maxgeometries,2))
    ALLOCATE(GC_direct_flux(nlambdas,GC_n_user_altitudes,GC_maxgeometries,2))
    
    ! Arrays for AMF output
    IF( do_AMF_calculation ) THEN
      ALLOCATE( GC_AMFs(nlambdas,GC_n_user_altitudes,GC_maxgeometries,maxgases,2) )
      ALLOCATE( GC_Scattering_Weights(nlambdas,GC_nlayers,GC_n_user_altitudes,GC_maxgeometries, 2) )
    ENDIF
    
    
!     IF( do_Jacobians ) THEN
      
      ! Trace gas jacobians are always archived
      ALLOCATE(GC_Tracegas_Jacobians(nlambdas,GC_nlayers,GC_n_user_altitudes,GC_maxgeometries,maxgases,2))
      
      ! Surface albedo
      IF( Diag28_InpOpt%njac .GT. 0 ) THEN
        ALLOCATE(GC_Surfalbedo_Jacobians(nlambdas,GC_n_user_altitudes,GC_maxgeometries,Diag28_InpOpt%njac,2))
      ENDIF
      ! Surface wind speed
      ALLOCATE(GC_Windspeed_Jacobians(nlambdas,GC_n_user_altitudes,GC_maxgeometries,2))
      
      IF( do_StokesQU_output ) THEN
        
        ! Q/U radiance/Flux arrays
        IF( do_stokes_ad20 ) THEN
          ALLOCATE(GC_Qvalues(nlambdas,GC_n_user_altitudes,GC_maxgeometries,2)     )
          ALLOCATE(GC_Uvalues(nlambdas,GC_n_user_altitudes,GC_maxgeometries,2)     )
        ENDIF
        IF( do_stokes_ad21 ) THEN
          ALLOCATE(GC_Qflux(nlambdas,GC_n_user_altitudes,GC_maxgeometries,2)       )
          ALLOCATE(GC_Qdirect_flux(nlambdas,GC_n_user_altitudes,GC_maxgeometries,2))
        ENDIF
        IF( do_stokes_ad22 ) THEN
          ALLOCATE(GC_Uflux(nlambdas,GC_n_user_altitudes,GC_maxgeometries,2)       )
          ALLOCATE(GC_Udirect_flux(nlambdas,GC_n_user_altitudes,GC_maxgeometries,2))
        ENDIF
        
        ! Q/U surface albedo jacobians
        ALLOCATE(GC_Surfalbedo_QJacobians(nlambdas,GC_n_user_altitudes,GC_maxgeometries,Diag28_InpOpt%njac,2))
        ALLOCATE(GC_Surfalbedo_UJacobians(nlambdas,GC_n_user_altitudes,GC_maxgeometries,Diag28_InpOpt%njac,2))
        
        ! Q/U Wind speed jacobians
        ALLOCATE(GC_Windspeed_QJacobians(nlambdas,GC_n_user_altitudes,GC_maxgeometries,2))
        ALLOCATE(GC_Windspeed_UJacobians(nlambdas,GC_n_user_altitudes,GC_maxgeometries,2))
        
      ENDIF
      
!     ENDIF
    
    ! Trace gas Q/U jacobians
    IF ( do_StokesQU_output ) THEN
      ALLOCATE(GC_Tracegas_QJacobians(nlambdas,GC_nlayers,GC_n_user_altitudes,GC_maxgeometries,maxgases,2))
      ALLOCATE(GC_Tracegas_UJacobians(nlambdas,GC_nlayers,GC_n_user_altitudes,GC_maxgeometries,maxgases,2))
    ENDIF
    
    ! Temperature jacobians
    IF( do_T_Jacobians ) THEN
      
      ALLOCATE(GC_Temperature_Jacobians(nlambdas,GC_nlayers,GC_n_user_altitudes,GC_maxgeometries, 2))
      
    ENDIF
    
    ! AOD Jacobians
    IF( do_aod_Jacobians ) THEN
      
      IF( do_total_aod_jac ) THEN
      
        ALLOCATE(GC_aod_Jacobians(nlambdas,GC_nlayers,GC_n_user_altitudes,GC_maxgeometries, 2))
        
        IF( do_StokesQU_output ) THEN
          ALLOCATE(GC_aod_QJacobians(nlambdas,GC_nlayers,GC_n_user_altitudes,GC_maxgeometries, 2))
          ALLOCATE(GC_aod_UJacobians(nlambdas,GC_nlayers,GC_n_user_altitudes,GC_maxgeometries, 2))
        ENDIF
      
      ENDIF
      
      ! Array needed for linearization of individual aerosol diagnostics
      IF( Diag34_InpOpt%njac > 0 .OR. Diag35_InpOpt%njac > 0 .OR. do_aod_Jacobians ) THEN
        IF(.NOT. ALLOCATED(aerdiag_phasemoms)) ALLOCATE( aerdiag_phasemoms( 0:maxmoms, 1:maxgksec) )
      ENDIF
      
      IF( Diag34_InpOpt%njac > 0 ) THEN
        ALLOCATE(GC_spcaod_Jacobians(nlambdas,GC_nlayers,GC_n_user_altitudes,GC_maxgeometries,Diag34_InpOpt%njac,2))
        ALLOCATE(GC_spcaod_QJacobians(nlambdas,GC_nlayers,GC_n_user_altitudes,GC_maxgeometries,Diag34_InpOpt%njac,2))
        ALLOCATE(GC_spcaod_UJacobians(nlambdas,GC_nlayers,GC_n_user_altitudes,GC_maxgeometries,Diag34_InpOpt%njac,2))
      ENDIF
      
    ENDIF
    
    ! Aerosol SSA Jacobians
    IF( do_assa_Jacobians ) THEN
      
      IF( do_total_assa_jac ) THEN
        
        ALLOCATE(GC_assa_Jacobians(nlambdas,GC_nlayers,GC_n_user_altitudes,GC_maxgeometries, 2))
        
        IF( do_StokesQU_output ) THEN
          ALLOCATE(GC_assa_QJacobians(nlambdas,GC_nlayers,GC_n_user_altitudes,GC_maxgeometries, 2))
          ALLOCATE(GC_assa_UJacobians(nlambdas,GC_nlayers,GC_n_user_altitudes,GC_maxgeometries, 2))
        ENDIF
        
      ENDIF
      
      IF( Diag35_InpOpt%njac > 0 ) THEN
        ALLOCATE(GC_spcassa_Jacobians(nlambdas,GC_nlayers,GC_n_user_altitudes,GC_maxgeometries,Diag35_InpOpt%njac,2))
        ALLOCATE(GC_spcassa_QJacobians(nlambdas,GC_nlayers,GC_n_user_altitudes,GC_maxgeometries,Diag35_InpOpt%njac,2))
        ALLOCATE(GC_spcassa_UJacobians(nlambdas,GC_nlayers,GC_n_user_altitudes,GC_maxgeometries,Diag35_InpOpt%njac,2))
      ENDIF
      
    ENDIF
    
    ! Cloud optical depth jacobians
    IF( do_cod_Jacobians ) THEN
      
      ALLOCATE(GC_cod_Jacobians(nlambdas,GC_nlayers,GC_n_user_altitudes,GC_maxgeometries, 2))
      
      IF( do_StokesQU_output ) THEN
        ALLOCATE(GC_cod_QJacobians(nlambdas,GC_nlayers,GC_n_user_altitudes,GC_maxgeometries, 2))
        ALLOCATE(GC_cod_UJacobians(nlambdas,GC_nlayers,GC_n_user_altitudes,GC_maxgeometries, 2))
      ENDIF
      
    ENDIF
    
    ! Cloud SSA jacobians
    IF( do_cssa_Jacobians ) THEN
      
      ALLOCATE(GC_cssa_Jacobians(nlambdas,GC_nlayers,GC_n_user_altitudes,GC_maxgeometries, 2))
      
      IF( do_StokesQU_output ) THEN
        ALLOCATE(GC_cssa_QJacobians(nlambdas,GC_nlayers,GC_n_user_altitudes,GC_maxgeometries, 2))
        ALLOCATE(GC_cssa_UJacobians(nlambdas,GC_nlayers,GC_n_user_altitudes,GC_maxgeometries, 2))
      ENDIF
      
    ENDIF
    
    ! Cloud fraction jacobian
    IF( do_cfrac_jacobians ) THEN
      
      ALLOCATE( GC_cfrac_Jacobians(nlambdas,GC_n_user_altitudes,GC_maxgeometries,2) )
      
      IF( do_StokesQU_output ) THEN
        ALLOCATE(GC_cfrac_QJacobians(nlambdas,GC_n_user_altitudes,GC_maxgeometries,2))
        ALLOCATE(GC_cfrac_UJacobians(nlambdas,GC_n_user_altitudes,GC_maxgeometries,2))
      ENDIF
      
    ENDIF
    
    ! Surface pressure jacobian
    IF( do_sfcprs_Jacobians ) THEN
      
      ALLOCATE( GC_sfcprs_Jacobians(nlambdas,GC_n_user_altitudes,GC_maxgeometries,2) )
      
      IF( do_stokes_ad31 ) THEN
        ALLOCATE( GC_sfcprs_QJacobians(nlambdas,GC_n_user_altitudes,GC_maxgeometries,2) )
        ALLOCATE( GC_sfcprs_UJacobians(nlambdas,GC_n_user_altitudes,GC_maxgeometries,2) )
      ENDIF
      
      IF( do_airdens_jac ) THEN

        ALLOCATE( GC_airdens_Jacobians(nlambdas,GC_nlayers,GC_n_user_altitudes,GC_maxgeometries, 2) )

        IF( do_stokes_ad31 ) THEN
          ALLOCATE( GC_airdens_QJacobians(nlambdas,GC_nlayers,GC_n_user_altitudes,GC_maxgeometries, 2) )
          ALLOCATE( GC_airdens_UJacobians(nlambdas,GC_nlayers,GC_n_user_altitudes,GC_maxgeometries, 2) )
        ENDIF
      ENDIF

    ENDIF
    
    ! Raman "absorption" jacobian
    IF( do_raman_jacobian ) THEN
      
      ALLOCATE( GC_rrs_Jacobian(nlambdas,GC_nlayers,GC_n_user_altitudes,GC_maxgeometries, 2) )
      
    ENDIF
    
    ! Now create array of dimension types - hardcode for now
    nc_ndimlist = 13
    
    ! Additional XY Fields
    IF( do_output_as_xy_nc ) nc_ndimlist = nc_ndimlist + 2
    
    ALLOCATE( nc_dimlist( nc_ndimlist ) )
    
    ! Compute number of geometries
    ngeom =  GC_n_sun_positions * GC_n_view_angles * GC_n_azimuths
    
    ! Set values
    nc_dimlist( 1)%NAME = 'one'          ;  nc_dimlist( 1)%DIMSIZE     = 1
    nc_dimlist( 2)%NAME = 'nbrdfpar'     ;  nc_dimlist( 2)%DIMSIZE     = MAX_BRDF_PARAMETERS
    nc_dimlist( 3)%NAME = 'four'         ;  nc_dimlist( 3)%DIMSIZE     = 4
    nc_dimlist( 4)%NAME = 'nsza'         ;  nc_dimlist( 4)%DIMSIZE     = GC_n_sun_positions
    nc_dimlist( 5)%NAME = 'nvza'         ;  nc_dimlist( 5)%DIMSIZE     = GC_n_view_angles
    nc_dimlist( 6)%NAME = 'naza'         ;  nc_dimlist( 6)%DIMSIZE     = GC_n_azimuths
    nc_dimlist( 7)%NAME = 'ngas'
    IF( ngases > 0 ) THEN
      nc_dimlist(7)%DIMSIZE = ngases
    ELSE
      nc_dimlist(7)%DIMSIZE = 1
    ENDIF
    nc_dimlist( 8)%NAME = 'nlevel'       ;  nc_dimlist( 8)%DIMSIZE     = GC_nlayers+1
    nc_dimlist( 9)%NAME = 'noutputlevel' ;  nc_dimlist( 9)%DIMSIZE     = MAX(GC_n_user_levels,1)
    nc_dimlist(10)%NAME = 'nlayer'       ;  nc_dimlist(10)%DIMSIZE     = GC_nlayers
    nc_dimlist(11)%NAME = 'nw'
    IF ( .NOT. do_effcrs .AND. lambda_resolution /= 0.0d0 ) THEN
      nc_dimlist(11)%DIMSIZE = nclambdas
    ELSE
      nc_dimlist(11)%DIMSIZE = nlambdas
    ENDIF
    nc_dimlist(12)%NAME = 'ngeom'        ;  nc_dimlist(12)%DIMSIZE     = ngeom
    nc_dimlist(13)%NAME = 'nstokessq'    ;  nc_dimlist(13)%DIMSIZE     = MAX(NSTOKESSQ,1)
    
    
    ! The base number of dimensions
    ct = 13
    
    IF(  do_output_as_xy_nc ) THEN
      ct = ct + 1
      nc_dimlist(ct)%NAME = 'ew_npix'    ;  nc_dimlist(ct)%DIMSIZE     = nc_imx
      ct = ct + 1
      nc_dimlist(ct)%NAME = 'ns_npix'    ;  nc_dimlist(ct)%DIMSIZE     = nc_jmx
    ENDIF
    
  END SUBROUTINE allocate_output_arrays
  
  SUBROUTINE netcdf_handle_error(location,status)
    
    IMPLICIT NONE
    INCLUDE 'netcdf.inc'

    ! ---------------
    ! Input variables
    ! ---------------
    INTEGER, INTENT(IN) :: status
    CHARACTER(*)        :: location

    ! ---------------
    ! Local variables
    !----------------
    CHARACTER(NF_MAX_NAME) :: message

    ! Code starts here
    IF (status .NE. NF_NOERR) THEN
       message = 'Error in '//TRIM(location)//': '//NF_STRERROR(status)
       CALL write_err_message(.TRUE., message)
       CALL error_exit(.TRUE.)
    ENDIF

  END SUBROUTINE netcdf_handle_error

  subroutine netcdf_rd_dem( fname, message, fail)
  
    USE GC_variables_module, ONLY: lon_dem, lat_dem, nx_dem, ny_dem, &
         alt_dem
    
    IMPLICIT none
    INCLUDE 'netcdf.inc'
  
    !=================================  
    !     Input/out variables
    !=================================
    character(len=256), intent(in) :: fname                    !Filename
    character*(*), intent(inout)   :: message
    logical, intent(out)           :: fail
    
    !=================================  
    !     Local variables
    !=================================
    integer :: ncid, rcode, astat
    integer :: xid, yid, alt_id, lat_id, lon_id
    
    character(len=31) :: lonnam, latnam
    
    ! ====================================================
    ! netcdf_rd_dem starts here
    ! ====================================================
    
    fail = .false.; message = ' '
    
    ! ---------
    ! Open File
    ! ---------
    
    ! Open file in read mode
    ncid = ncopn(trim(fname), nf_Nowrite, rcode)
    if (rcode  .eq. -1 ) then
       message =  ' error in netcdf_rd_dim: ncopn failed'
       fail = .true.; return
    endif
    
    ! ------------------------
    ! Allocate variable arrays
    ! ------------------------
    
    ! Longitude dimension ID
    xid = ncdid(ncid, 'x', RCODE)
    if (rcode  .eq. -1 ) then
       message =  ' error in netcdf_rd_dim: ncdid failed(x)'
       fail = .true.; return
    endif
    
    ! Latitude dimension ID
    yid = ncdid(ncid, 'y', RCODE)
    if (rcode  .eq. -1 ) then
       message =  ' error in netcdf_rd_dim: ncdid failed(y)'
       fail = .true.; return
    endif
    
    ! Read longitude dimension
    call ncdinq(ncid, xid, lonnam, nx_dem, rcode)
    if (rcode  .eq. -1 ) then
       message =  ' error in netcdf_rd_dim: ncdinq failed(x)'
       fail = .true.; return
    endif
    
    call ncdinq(ncid, yid, latnam, ny_dem, rcode)
    if (rcode  .eq. -1 ) then
       message =  ' error in netcdf_rd_dim: ncdinq failed(y)'
       fail = .true.; return
    endif
    
    ! Allocate arrays
    allocate(lon_dem(nx_dem), STAT=astat)
    allocate(lat_dem(ny_dem), STAT=astat)
    allocate(alt_dem(nx_dem,ny_dem), STAT=astat)
    
    ! ------------------
    ! Read the variables
    ! ------------------
    
    ! Get altitude variable ID
    alt_id = ncvid(ncid, 'Altitude', rcode)
    if (rcode  .eq. -1 ) then
       message =  ' error in netcdf_rd_dim: ncvid failed(alt)'
       fail = .true.; return
    endif
    
    ! Get longitude variable ID
    lon_id = ncvid(ncid, 'Longitude', rcode)
    if (rcode  .eq. -1 ) then
       message =  ' error in netcdf_rd_dim: ncvid failed(lon)'
       fail = .true.; return
    endif
    
    ! Get latitude variable ID
    lat_id = ncvid(ncid, 'Latitude', rcode)
    if (rcode  .eq. -1 ) then
       message =  ' error in netcdf_rd_dim: ncvid failed(lat)'
       fail = .true.; return
    endif
    
    ! Read Altitude variable
    call ncvgt(ncid, alt_id, (/1,1/), (/nx_dem,ny_dem/), alt_dem , rcode)
    if (rcode  .eq. -1 ) then
       message =  ' error in netcdf_rd_dim: ncvgt failed(alt)'
       fail = .true.; return
    endif
    
    ! Read Longitude variable
    call ncvgt(ncid, lon_id, (/1/), (/nx_dem/), lon_dem , rcode)
    if (rcode  .eq. -1 ) then
       message =  ' error in netcdf_rd_dim: ncvgt failed(lon)'
       fail = .true.; return
    endif
    
    ! Read Latitude variable
    call ncvgt(ncid, lat_id, (/1/), (/ny_dem/), lat_dem , rcode)
    if (rcode  .eq. -1 ) then
       message =  ' error in netcdf_rd_dim: ncvgt failed(lat)'
       fail = .true.; return
    endif
    
    ! ----------
    ! Close file
    ! ----------
    
    call ncclos(ncid, rcode)
    if (rcode .eq. -1) then
       message =  ' error in netcdf_rd_dim: ncclos'
       fail = .true.; return
    endif
    
    return
  end subroutine netcdf_rd_dem

  subroutine netcdf_xy_wrt ( fname, message, fail)
    
    IMPLICIT none
    INCLUDE 'netcdf.inc'
    
    !=================================
    !     Input/out variables
    !=================================
    character(len=256), intent(in) :: fname                    !Filename
    character*(*), intent(inout)   :: message
    logical, intent(out)           :: fail
    
    !=================================
    !     Local variables
    !=================================
    integer :: ncid, rcode, nwav, iout, ngeom
    integer :: radid, qid, uid, fluxid, dfluxid, &
         qfluxid, ufluxid, qdfluxid, udfluxid
    integer, dimension(5)    :: ndimstart5, ndimcount5
    
    ! To lock multiple processes from writing to the netcdf
    !   character(len=20)  :: lock_file = './netcdf_xy_lock.tmp'
    integer            :: open_stat, ierr
    logical            :: file_exists
    integer, parameter :: lckunit = 17 ! Placeholder in parameters mod
    
    character(len=13), parameter :: location='netcdf_xy_wrt'
    
    fail = .false.; message = ' '
    
    ! ==============================================================
    ! Open netCDF file only if no other process is accessing it
    ! ==============================================================
    
    file_exists = .TRUE.
    
    do while( file_exists )
       
       open(STATUS='NEW',unit=lckunit,file=TRIM(ADJUSTL(fname)) // '.lock' ,iostat=open_stat)
       
       if( open_stat .eq. 0 ) then
          
          file_exists = .FALSE.
          
          rcode = NF_OPEN(trim(fname), NF_WRITE, ncid)
          
          IF (rcode .NE. NF_NOERR) THEN
             message = 'Error in '//TRIM(location)//': '//NF_STRERROR(rcode)
             print*,message
             close(lckunit,status='DELETE',iostat=ierr)
             STOP
          ENDIF
          
          ! Fill in value for # of geometries
          ngeom = VLIDORT_Out%Main%TS_N_GEOMETRIES
          
       else
          
          print*,'Waiting...'
          call sleep( 1 )
          
       endif
       
    enddo
    
    ! ==============================================================
    ! VARIABLE IDs 
    ! ==============================================================
    
    IF(write_rad_spc ) &
         rcode = NF_INQ_VARID(ncid, 'radiance', radid)
    IF (rcode .NE. NF_NOERR) THEN
       message = 'Error in '//TRIM(location)//': '//NF_STRERROR(rcode)
       print*,message
       close(lckunit,status='DELETE',iostat=ierr)
       STOP
    ENDIF
    IF(write_flx_spc ) &
         rcode = NF_INQ_VARID(ncid, 'flux', fluxid)
    IF (rcode .NE. NF_NOERR) THEN
       message = 'Error in '//TRIM(location)//': '//NF_STRERROR(rcode)
       print*,message
       close(lckunit,status='DELETE',iostat=ierr)
       STOP
    ENDIF
    
    IF(write_dflx_spc) &
         rcode = NF_INQ_VARID(ncid, 'direct_flux', dfluxid)
    IF (rcode .NE. NF_NOERR) THEN
       message = 'Error in '//TRIM(location)//': '//NF_STRERROR(rcode)
       print*,message
       close(lckunit,status='DELETE',iostat=ierr)
       STOP
    ENDIF
    
    if (do_vector_calculation .and. do_StokesQU_output) then
       IF(write_rad_spc) THEN
          rcode = NF_INQ_VARID(ncid, 'q', qid)
          IF (rcode .NE. NF_NOERR) THEN
             message = 'Error in '//TRIM(location)//': '//NF_STRERROR(rcode)
             print*,message
             close(lckunit,status='DELETE',iostat=ierr)
             STOP
          ENDIF
          rcode = NF_INQ_VARID(ncid, 'u', uid)
          IF (rcode .NE. NF_NOERR) THEN
             message = 'Error in '//TRIM(location)//': '//NF_STRERROR(rcode)
             print*,message
             close(lckunit,status='DELETE',iostat=ierr)
             STOP
          ENDIF
       ENDIF
       IF(write_flx_spc) THEN
          rcode = NF_INQ_VARID(ncid, 'qflux', qfluxid)
          IF (rcode .NE. NF_NOERR) THEN
             message = 'Error in '//TRIM(location)//': '//NF_STRERROR(rcode)
             print*,message
             close(lckunit,status='DELETE',iostat=ierr)
             STOP
          ENDIF
          rcode = NF_INQ_VARID(ncid, 'uflux', ufluxid)
          IF (rcode .NE. NF_NOERR) THEN
             message = 'Error in '//TRIM(location)//': '//NF_STRERROR(rcode)
             print*,message
             close(lckunit,status='DELETE',iostat=ierr)
             STOP
          ENDIF
       ENDIF
       IF(write_dflx_spc) THEN
          rcode = NF_INQ_VARID(ncid, 'qdirect_flux', qdfluxid)
          IF (rcode .NE. NF_NOERR) THEN
             message = 'Error in '//TRIM(location)//': '//NF_STRERROR(rcode)
             print*,message
             close(lckunit,status='DELETE',iostat=ierr)
             STOP
          ENDIF
          rcode = NF_INQ_VARID(ncid, 'udirect_flux', udfluxid)
          IF (rcode .NE. NF_NOERR) THEN
             message = 'Error in '//TRIM(location)//': '//NF_STRERROR(rcode)
             print*,message
             close(lckunit,status='DELETE',iostat=ierr)
             STOP
          ENDIF
       ENDIF
    endif
  
    ! Work out the number of lambdas. It depends if we are doing convolved cross-sections or not
    IF (.NOT. do_effcrs .AND. lambda_resolution /= 0.0d0 ) THEN 
       nwav = nclambdas
    ELSE
       nwav = nlambdas
    END IF
    
    ! ==============================================================
    ! Write
    ! ==============================================================
    
    ! Echo 
    print*, 'Writing output #', didx
    
    DO iout = 1, GC_n_user_levels
       ndimstart5 = (/nc_xid, nc_yid,    1,     1 , iout/)
       ndimcount5 = (/     1,      1, nwav, ngeom ,    1/)
       if(write_rad_spc) then
          rcode = NF_PUT_VARA_REAL (ncid, radid,   ndimstart5, ndimcount5, &
               real(GC_radiances(1:nwav,iout,1:ngeom,didx), kind=4))
          IF (rcode .NE. NF_NOERR) THEN
             message = 'Error in '//TRIM(location)//': '//NF_STRERROR(rcode)
             print*,message
             close(lckunit,status='DELETE',iostat=ierr)
             STOP
          ENDIF
       endif
       ndimcount5 = (/nc_xid, nc_yid,   1, GC_n_sun_positions , 1/)
       if(write_flx_spc) then
          rcode = NF_PUT_VARA_REAL (ncid, fluxid,  ndimstart5, ndimcount5, &
               real(GC_flux(1:nwav,iout,1:GC_n_sun_positions,didx), kind=4))
          IF (rcode .NE. NF_NOERR) THEN
             message = 'Error in '//TRIM(location)//': '//NF_STRERROR(rcode)
             print*,message
             close(lckunit,status='DELETE',iostat=ierr)
             STOP
          ENDIF
       endif
       if(write_dflx_spc) then
          rcode = NF_PUT_VARA_REAL (ncid, dfluxid, ndimstart5, ndimcount5, &
               real(GC_direct_flux(1:nwav,iout,1:GC_n_sun_positions,didx), kind=4))
          IF (rcode .NE. NF_NOERR) THEN
             message = 'Error in '//TRIM(location)//': '//NF_STRERROR(rcode)
             print*,message
             close(lckunit,status='DELETE',iostat=ierr)
             STOP
          ENDIF
       endif
       if (do_vector_calculation .and. do_StokesQU_output) then
          ndimcount5 = (/ 1, 1,  nwav,  ngeom, 1/)
          if(write_rad_spc) then
             rcode = NF_PUT_VARA_REAL (ncid, qid, ndimstart5, ndimcount5, &
                  real(GC_Qvalues(1:nwav,iout,1:ngeom,didx), kind=4))
             IF (rcode .NE. NF_NOERR) THEN
                message = 'Error in '//TRIM(location)//': '//NF_STRERROR(rcode)
                print*,message
                close(lckunit,status='DELETE',iostat=ierr)
                STOP
             ENDIF
             rcode = NF_PUT_VARA_REAL (ncid, uid, ndimstart5, ndimcount5, &
                  real(GC_Uvalues(1:nwav,iout,1:ngeom,didx), kind=4))
             IF (rcode .NE. NF_NOERR) THEN
                message = 'Error in '//TRIM(location)//': '//NF_STRERROR(rcode)
                print*,message
                close(lckunit,status='DELETE',iostat=ierr)
                STOP
             ENDIF
          endif
          ndimcount5 = (/  1, 1,  1, GC_n_sun_positions , 1/)
          if(write_flx_spc) then
             rcode = NF_PUT_VARA_REAL (ncid, qfluxid,  ndimstart5, ndimcount5, &
                  real(GC_Qflux(1:nwav,iout,1:GC_n_sun_positions,didx), kind=4))
             IF (rcode .NE. NF_NOERR) THEN
                message = 'Error in '//TRIM(location)//': '//NF_STRERROR(rcode)
                print*,message
                close(lckunit,status='DELETE',iostat=ierr)
                STOP
             ENDIF
             rcode = NF_PUT_VARA_REAL (ncid, ufluxid,  ndimstart5, ndimcount5, &
                  real(GC_Uflux(1:nwav,iout,1:GC_n_sun_positions,didx), kind=4))
             IF (rcode .NE. NF_NOERR) THEN
                message = 'Error in '//TRIM(location)//': '//NF_STRERROR(rcode)
                print*,message
                close(lckunit,status='DELETE',iostat=ierr)
                STOP
             ENDIF
          endif
          if( write_dflx_spc ) then
             rcode = NF_PUT_VARA_REAL (ncid, qdfluxid, ndimstart5, ndimcount5, &
                  real(GC_Qdirect_flux(1:nwav,iout,1:GC_n_sun_positions,didx), kind=4))
             IF (rcode .NE. NF_NOERR) THEN
                message = 'Error in '//TRIM(location)//': '//NF_STRERROR(rcode)
                print*,message
                close(lckunit,status='DELETE',iostat=ierr)
                STOP
             ENDIF
             rcode = NF_PUT_VARA_REAL (ncid, udfluxid, ndimstart5, ndimcount5, &
                  real(GC_Udirect_flux(1:nwav,iout,1:GC_n_sun_positions,didx), kind=4))
             IF (rcode .NE. NF_NOERR) THEN
                message = 'Error in '//TRIM(location)//': '//NF_STRERROR(rcode)
                print*,message
                close(lckunit,status='DELETE',iostat=ierr)
                STOP
             ENDIF
          endif
       endif
       
    ENDDO
  
    !==============================================================================
    ! CLOSE the NetCDF file
    !==============================================================================
    
    rcode = NF_CLOSE(ncid)
    IF (rcode .NE. NF_NOERR) THEN
       message = 'Error in '//TRIM(location)//': '//NF_STRERROR(rcode)
       print*,message
       close(lckunit,status='DELETE',iostat=ierr)
       STOP
    ENDIF
    close(lckunit,status='DELETE',iostat=ierr)
    
    return
  end subroutine netcdf_xy_wrt

subroutine read_xy_nc_prof( fname, maxl, maxc, max_ch_len,yn_inr_corner,profile_data, &
                            snowfrac, snowdepth, seaicefrac, snowage, &
                            ltype_frac, sif_734nm,  &
                            footprint_data, &
                            profids, profile_is_level, &
                            GC_nlayers, pnc, pixel_cf, pixel_pcld, obsalt, &
                            message, fail)
  
  ! read_xy_nc_prof mimics the output of the ascii profile reader
  
  USE GC_variables_module, ONLY : ncld, cld_types, create_xy_nc_only, &
       aer_types, naer, use_aerprof
  
  IMPLICIT none
  INCLUDE 'netcdf.inc'
  
  !=================================  
  !     Input/out variables
  !=================================
  character(len=*),                   intent(in)     :: fname
  integer,                            intent(in)     :: maxl, maxc, max_ch_len
  logical,                            intent(in)     :: yn_inr_corner
  integer,                            intent(out)    :: GC_nlayers, pnc
  real(kind=8), dimension(maxl,maxc), intent(out)    :: profile_data
  real(kind=8),                       intent(out)    :: snowfrac, snowdepth, seaicefrac, snowage, obsalt
  real(kind=8), dimension(17),        intent(out)    :: ltype_frac 
  real(kind=8),                       intent(out)    :: sif_734nm
  real(kind=8), dimension(28),        intent(out)    :: footprint_data
  real(kind=4), dimension(1000),      intent(out)    :: pixel_cf, pixel_pcld
  logical,                            intent(out)    :: profile_is_level  !T: level  F: layer
  character(len=max_ch_len),          intent(out)    :: profids
  
  
  !  Exception handling

   logical,       intent(INOUT) :: fail
   character*(*), intent(INOUT) :: message
  
  !=================================  
  !     Local variables
  !=================================
  integer :: ncid, rcode
  integer :: xid, yid, zid, var_id
  
  integer      :: i, j, nr, nc, imx, jmx, lmx
  integer, dimension(4) :: ixc, iyc
  character(len=31) :: xdimname, ydimname, zdimname
   
!  Dummy read variables
  integer(KIND=2) :: scalar_i2
  REAL(KIND=4)    :: scalar_r4
  integer(KIND=4) :: ncpix
  REAL(KIND=4), ALLOCATABLE, DIMENSION(:) :: vmid_r4, vedge_r4
  REAL(KIND=4), ALLOCATABLE, DIMENSION(:,:) :: vmid2d_r4
  REAL(KIND=4), DIMENSION(17) :: ltype_r4
   ! These aerosol fields must be in the NC file for now
  CHARACTER(LEN=3)              :: numstr, cld_fldname
  
   ! For new NetCDF4 interface
  CHARACTER(LEN=15), parameter :: location='read_xy_nc_prof'
   
  ! ====================================================
  ! netcdf_rd_dem starts here
  ! ====================================================
  
  ! Initialize 
  fail    = .false.
  message = ' '
  do i = 1, maxl
     do j = 1,maxc
        profile_data(i,j) = -999.
     enddo
  enddo
    
  ! ==========================
  ! Open File
  ! ==========================  
  print*,trim(adjustl(fname))
  
  ! Open file in read mode
  ncid = ncopn(trim(adjustl(fname)), nf_Nowrite, rcode)
  if (rcode  .eq. -1 ) then
     message =  ' error in read_xy_nc_prof: ncopn failed'
     fail = .true.; return
  endif
    
  ! Get the X dimension
  xid = ncdid(ncid, 'x', RCODE)
  if (rcode  .eq. -1 ) then
     message =  ' error in read_xy_nc_prof: ncdid failed(x)'
     fail = .true.; return
  endif 
  
  ! Read x dimension
  call ncdinq(ncid, xid, xdimname, imx, rcode)
  if (rcode  .eq. -1 ) then
     message =  ' error in read_xy_nc_prof: ncdinq failed(x)'
     fail = .true.; return
  endif
   
  ! Save dimension
  nc_imx = imx
  
  ! Get the altitude dimension
  yid = ncdid(ncid, 'y', RCODE)
  if (rcode  .eq. -1 ) then
     message =  ' error in read_xy_nc_prof: ncdid failed(y)'
     fail = .true.; return
  endif 
   
  ! Read height dimension
  call ncdinq(ncid, yid, ydimname, jmx, rcode)
  if (rcode  .eq. -1 ) then
     message =  ' error in read_xy_nc_prof: ncdinq failed(y)'
     fail = .true.; return
  endif
   
  ! Save dimension
  nc_jmx = jmx
  
  ! Get the altitude dimension
  zid = ncdid(ncid, 'z', RCODE)
  if (rcode  .eq. -1 ) then
     message =  ' error in read_xy_nc_prof: ncdid failed(z)'
     fail = .true.; return
  endif 
  
  ! Read height dimension
  call ncdinq(ncid, zid, zdimname, lmx, rcode)
  if (rcode  .eq. -1 ) then
     message =  ' error in read_xy_nc_prof: ncdinq failed(z)'
     fail = .true.; return
  endif
  
  ! The number of rows (include level edges)
  nr = lmx + 1
  
  ! Allocate dummy variables for reading profiles
  ALLOCATE( vmid_r4(lmx) )
  ALLOCATE( vedge_r4(nr) )
  
  ! ==========================================
  ! Check if there is a calculation to be done
  ! ==========================================
  
  var_id = ncvid(ncid, 'DoCalculation', rcode)
  if (rcode  .eq. -1 ) then
     message =  ' error in netcdf_rd_dim: ncvid failed(DoCalculation)'
     fail = .true.; return
  endif
  call ncvgt(ncid, var_id, (/nc_xid,nc_yid/), (/1,1/), scalar_i2, rcode)
  if (rcode  .eq. -1 ) then
     message =  ' error in netcdf_rd_dim: ncvgt failed(DoCalculation)'
     fail = .true.; return
  endif
  
  skip_xy_pixel = .FALSE.
  IF( scalar_i2 .eq. 0 .and. .not. create_xy_nc_only  ) THEN
    
    print*,'================================================'
    print*,'No profile for this X-Y Coordinate (Normal Exit)'
    print*,'================================================'
    !STOP
    skip_xy_pixel = .TRUE.
    RETURN
  
  ENDIF
 
  
  ! ===============================
  ! Read Footprint Information
  ! ===============================
  
  ! -------------------------------
  ! Set profile midpoint logic flag
  ! -------------------------------
  var_id = ncvid(ncid, 'ProfileIsLevel', rcode)
  if (rcode  .eq. -1 ) then
     message =  ' error in netcdf_rd_dim: ncvid failed(ProfileIsLevel)'
     fail = .true.; return
  endif
  call ncvgt(ncid, var_id, (/1,1/), (/1,1/), scalar_i2, rcode)
  if (rcode  .eq. -1 ) then
     message =  ' error in netcdf_rd_dim: ncvgt failed(ProfileIsLevel)'
     fail = .true.; return
  endif

  
  ! Set profile logic
  if (scalar_i2 .eq. 0) then
    profile_is_level = .FALSE.
  else
    profile_is_level = .TRUE.
  endif
  
  ! ---------
  ! Read Year
  ! ---------
  var_id = ncvid(ncid, 'Year', rcode)
  if (rcode  .eq. -1 ) then
     message =  ' error in netcdf_rd_dim: ncvid failed(Year)'
     fail = .true.; return
  endif
  call ncvgt(ncid, var_id, (/1,1/), (/1,1/), scalar_i2, rcode)
  if (rcode  .eq. -1 ) then
     message =  ' error in netcdf_rd_dim: ncvgt failed(Year)'
     fail = .true.; return
  endif
  footprint_data(1) = REAL(scalar_i2,KIND=8)
  
  ! ----------
  ! Read Month
  ! ----------
  var_id = ncvid(ncid, 'Month', rcode)
  if (rcode  .eq. -1 ) then
     message =  ' error in netcdf_rd_dim: ncvid failed(Month)'
     fail = .true.; return
  endif
  call ncvgt(ncid, var_id, (/1,1/), (/1,1/), scalar_i2, rcode)
  if (rcode  .eq. -1 ) then
     message =  ' error in netcdf_rd_dim: ncvgt failed(Month)'
     fail = .true.; return
  endif
  footprint_data(2) = REAL(scalar_i2,KIND=8)
  
  ! --------
  ! Read Day
  ! --------
  var_id = ncvid(ncid, 'Day', rcode)
  if (rcode  .eq. -1 ) then
     message =  ' error in netcdf_rd_dim: ncvid failed(Day)'
     fail = .true.; return
  endif
  call ncvgt(ncid, var_id, (/1,1/), (/1,1/), scalar_i2, rcode)
  if (rcode  .eq. -1 ) then
     message =  ' error in netcdf_rd_dim: ncvgt failed(Day)'
     fail = .true.; return
  endif
  footprint_data(3) = REAL(scalar_i2,KIND=8)
  
  ! ---------
  ! Read hour
  ! ---------
  var_id = ncvid(ncid, 'Hour', rcode)
  if (rcode  .eq. -1 ) then
     message =  ' error in netcdf_rd_dim: ncvid failed(Hour)'
     fail = .true.; return
  endif
  call ncvgt(ncid, var_id, (/nc_xid,nc_yid/), (/1,1/), scalar_r4, rcode)
  if (rcode  .eq. -1 ) then
     message =  ' error in netcdf_rd_dim: ncvgt failed(Hour)'
     fail = .true.; return
  endif
  footprint_data(4) = REAL(scalar_r4,KIND=8)
  
  ! --------------
  ! Read Longitude
  ! --------------
  var_id = ncvid(ncid, 'Longitude', rcode)
  if (rcode  .eq. -1 ) then
     message =  ' error in netcdf_rd_dim: ncvid failed(Longitude)'
     fail = .true.; return
  endif
  call ncvgt(ncid, var_id, (/nc_xid,nc_yid/), (/1,1/), scalar_r4, rcode)
  if (rcode  .eq. -1 ) then
     message =  ' error in netcdf_rd_dim: ncvgt failed(Longitude)'
     fail = .true.; return
  endif
  footprint_data(5) = REAL(scalar_r4,KIND=8)
  
  ! --------------
  ! Read Latitude
  ! --------------
  var_id = ncvid(ncid, 'Latitude', rcode)
  if (rcode  .eq. -1 ) then
     message =  ' error in netcdf_rd_dim: ncvid failed(Latitude)'
     fail = .true.; return
  endif
  call ncvgt(ncid, var_id, (/nc_xid,nc_yid/), (/1,1/), scalar_r4, rcode)
  if (rcode  .eq. -1 ) then
     message =  ' error in netcdf_rd_dim: ncvgt failed(Latitude)'
     fail = .true.; return
  endif
  footprint_data(6) = REAL(scalar_r4,KIND=8)
  
  ! --------------
  ! Read SZA
  ! --------------
  var_id = ncvid(ncid, 'SZA', rcode)
  if (rcode  .eq. -1 ) then
     message =  ' error in netcdf_rd_dim: ncvid failed(SZA)'
     fail = .true.; return
  endif
  call ncvgt(ncid, var_id, (/nc_xid,nc_yid/), (/1,1/), scalar_r4, rcode)
  if (rcode  .eq. -1 ) then
     message =  ' error in netcdf_rd_dim: ncvgt failed(SZA)'
     fail = .true.; return
  endif
  footprint_data(7) = REAL(scalar_r4,KIND=8)
  
  ! --------------
  ! Read SAA
  ! --------------
  var_id = ncvid(ncid, 'SAA', rcode)
  if (rcode  .eq. -1 ) then
     message =  ' error in netcdf_rd_dim: ncvid failed(SAA)'
     fail = .true.; return
  endif
  call ncvgt(ncid, var_id, (/nc_xid,nc_yid/), (/1,1/), scalar_r4, rcode)
  if (rcode  .eq. -1 ) then
     message =  ' error in netcdf_rd_dim: ncvgt failed(SAA)'
     fail = .true.; return
  endif
  footprint_data(8) = REAL(scalar_r4,KIND=8)
  
  ! --------------
  ! Read VZA
  ! --------------
  var_id = ncvid(ncid, 'VZA', rcode)
  if (rcode  .eq. -1 ) then
     message =  ' error in netcdf_rd_dim: ncvid failed(VZA)'
     fail = .true.; return
  endif
  call ncvgt(ncid, var_id, (/nc_xid,nc_yid/), (/1,1/), scalar_r4, rcode)
  if (rcode  .eq. -1 ) then
     message =  ' error in netcdf_rd_dim: ncvgt failed(VZA)'
     fail = .true.; return
  endif
  footprint_data(9) = REAL(scalar_r4,KIND=8)
  
  ! --------------
  ! Read AZA
  ! --------------
  var_id = ncvid(ncid, 'AZA', rcode)
  if (rcode  .eq. -1 ) then
     message =  ' error in netcdf_rd_dim: ncvid failed(AZA)'
     fail = .true.; return
  endif
  call ncvgt(ncid, var_id, (/nc_xid,nc_yid/), (/1,1/), scalar_r4, rcode)
  if (rcode  .eq. -1 ) then
     message =  ' error in netcdf_rd_dim: ncvgt failed(AZA)'
     fail = .true.; return
  endif
  footprint_data(10) = REAL(scalar_r4,KIND=8)
  
  ! ------------------------
  ! Read Surface temperature
  ! ------------------------
  var_id = ncvid(ncid, 'SurfaceTemperature', rcode)
  if (rcode  .eq. -1 ) then
     message =  ' error in netcdf_rd_dim: ncvid failed(SurfaceTemperature)'
     fail = .true.; return
  endif
  call ncvgt(ncid, var_id, (/nc_xid,nc_yid/), (/1,1/), scalar_r4, rcode)
  if (rcode  .eq. -1 ) then
     message =  ' error in netcdf_rd_dim: ncvgt failed(SurfaceTemperature)'
     fail = .true.; return
  endif
  footprint_data(11) = REAL(scalar_r4,KIND=8)
  
  ! ---------------------
  ! Read surface pressure
  ! ---------------------
  var_id = ncvid(ncid, 'SurfacePressure', rcode)
  if (rcode  .eq. -1 ) then
     message =  ' error in netcdf_rd_dim: ncvid failed(SurfacePressure)'
     fail = .true.; return
  endif
  call ncvgt(ncid, var_id, (/nc_xid,nc_yid/), (/1,1/), scalar_r4, rcode)
  if (rcode  .eq. -1 ) then
     message =  ' error in netcdf_rd_dim: ncvgt failed(SurfacePressure)'
     fail = .true.; return
  endif
  footprint_data(12) = REAL(scalar_r4,KIND=8)
  
  
  ! ----------------------------------------
  ! Read wind direction (Clockwise from north)
  ! ------------------------------------------
  var_id = ncvid(ncid, 'WindDirection', rcode)
  if (rcode  .eq. -1 ) then
     message =  ' error in netcdf_rd_dim: ncvid failed(WindDirSAA)'
     fail = .true.; return
  endif
  call ncvgt(ncid, var_id, (/nc_xid,nc_yid/), (/1,1/), scalar_r4, rcode)
  if (rcode  .eq. -1 ) then
     message =  ' error in netcdf_rd_dim: ncvgt failed(WindDirSAA)'
     fail = .true.; return
  endif
  footprint_data(13) = REAL(scalar_r4,KIND=8)
  
  ! -------------------
  ! Read wind speed
  ! -------------------
  var_id = ncvid(ncid, 'WindSpeed', rcode)
  if (rcode  .eq. -1 ) then
     message =  ' error in netcdf_rd_dim: ncvid failed(WindSpeed)'
     fail = .true.; return
  endif
  call ncvgt(ncid, var_id, (/nc_xid,nc_yid/), (/1,1/), scalar_r4, rcode)
  if (rcode  .eq. -1 ) then
     message =  ' error in netcdf_rd_dim: ncvgt failed(WindSpeed)'
     fail = .true.; return
  endif
  footprint_data(14) = REAL(scalar_r4,KIND=8)
  
  ! -----------------------
  ! Read Cloud top pressure
  ! -----------------------  
  footprint_data(16) = 0.0d0
  
  ! -------------------
  ! Read terrain height 
  ! -------------------
  var_id = ncvid(ncid, 'TerrainHeight', rcode)
  if (rcode  .eq. -1 ) then
     message =  ' error in netcdf_rd_dim: ncvid failed(TerrainHeight)'
     fail = .true.; return
  endif
  call ncvgt(ncid, var_id, (/nc_xid,nc_yid/), (/1,1/), scalar_r4, rcode)
  if (rcode  .eq. -1 ) then
     message =  ' error in netcdf_rd_dim: ncvgt failed(TerrainHeight)'
     fail = .true.; return
  endif
  footprint_data(17) = REAL(scalar_r4,KIND=8)
  
  ! ----------------------------
  ! Read Pixel corner longitudes
  ! ----------------------------
  
  ! Corner indices
  ixc(:) = (/ nc_xid,   nc_xid, nc_xid+1, nc_xid+1 /)
  iyc(:) = (/ nc_yid, nc_yid+1, nc_yid+1, nc_yid   /)
  
  ! get variable id
  var_id = ncvid(ncid, 'CornerLongitudes', rcode)
  if (rcode  .eq. -1 ) then
    message =  ' error in netcdf_rd_dim: ncvid failed(CornerLongitudes)'
    fail = .true.; return
  endif
  
  IF( yn_inr_corner ) THEN
    
    DO i=1,4
      call ncvgt(ncid, var_id, (/nc_xid,nc_yid,i/), (/1,1,1/), scalar_r4, rcode)
      if (rcode  .eq. -1 ) then
        message =  ' error in netcdf_rd_dim: ncvgt failed(CornerLongitudes)'
        fail = .true.; return
      endif
      footprint_data(17+i) = REAL(scalar_r4,KIND=8)
    ENDDO

  ELSE
  
    DO i=1,4
      call ncvgt(ncid, var_id, (/ixc(i),iyc(i)/), (/1,1/), scalar_r4, rcode)
      if (rcode  .eq. -1 ) then
        message =  ' error in netcdf_rd_dim: ncvgt failed(CornerLongitudes)'
        fail = .true.; return
      endif
      footprint_data(17+i) = REAL(scalar_r4,KIND=8)
    ENDDO
  
  ENDIF
  
  ! ----------------------------
  ! Read Pixel corner latitudes
  ! ----------------------------
  
  ! get variable id
  var_id = ncvid(ncid, 'CornerLatitudes', rcode)
  if (rcode  .eq. -1 ) then
    message =  ' error in netcdf_rd_dim: ncvid failed(CornerLatitudes)'
    fail = .true.; return
  endif
  
  IF( yn_inr_corner ) THEN

    DO i=1,4
      call ncvgt(ncid, var_id, (/nc_xid,nc_yid,i/), (/1,1,1/), scalar_r4, rcode)
      if (rcode  .eq. -1 ) then
        message =  ' error in netcdf_rd_dim: ncvgt failed(CornerLongitudes)'
        fail = .true.; return
      endif
      footprint_data(21+i) = REAL(scalar_r4,KIND=8)
    ENDDO

  ELSE

    DO i=1,4
      call ncvgt(ncid, var_id, (/ixc(i),iyc(i)/), (/1,1/), scalar_r4, rcode)
      if (rcode  .eq. -1 ) then
        message =  ' error in netcdf_rd_dim: ncvgt failed(CornerLatitudes)'
        fail = .true.; return
      endif
      footprint_data(21+i) = REAL(scalar_r4,KIND=8)
      
    ENDDO

  ENDIF
  
  ! ---------------------
  ! Read Chlorophyll conc
  ! ----------------------
  var_id = ncvid(ncid, 'Chlorophyll', rcode)
  if (rcode  .eq. -1 ) then
     message =  ' error in netcdf_rd_dim: ncvid failed(Chlorophyll)'
     fail = .true.; return
  endif
  call ncvgt(ncid, var_id, (/nc_xid,nc_yid/), (/1,1/), scalar_r4, rcode)
  if (rcode  .eq. -1 ) then
     message =  ' error in netcdf_rd_dim: ncvgt failed(Chlorophyll)'
     fail = .true.; return
  endif
  footprint_data(26) = REAL(scalar_r4,KIND=8)
  
  ! -------------------
  ! Read ocean salinity
  ! -------------------
  var_id = ncvid(ncid, 'OceanSalinity', rcode)
  if (rcode  .eq. -1 ) then
     message =  ' error in netcdf_rd_dim: ncvid failed(OceanSalinity)'
     fail = .true.; return
  endif
  call ncvgt(ncid, var_id, (/nc_xid,nc_yid/), (/1,1/), scalar_r4, rcode)
  if (rcode  .eq. -1 ) then
     message =  ' error in netcdf_rd_dim: ncvgt failed(OceanSalinity)'
     fail = .true.; return
  endif
  footprint_data(27) = REAL(scalar_r4,KIND=8)
  
  ! ----------------------------
  ! Read number of cloud pixels
  ! ----------------------------
  var_id = ncvid(ncid, 'NumberOfCloudPixels', rcode)
  if (rcode  .eq. -1 ) then
     message =  ' error in netcdf_rd_dim: ncvid failed(NumberOfCloudPixels)'
     fail = .true.; return
  endif
  call ncvgt(ncid, var_id, (/nc_xid,nc_yid/), (/1,1/), scalar_i2, rcode)
  if (rcode  .eq. -1 ) then
     message =  ' error in netcdf_rd_dim: ncvgt failed(NumberOfCloudPixels)'
     fail = .true.; return
  endif
  footprint_data(28) = REAL(scalar_i2,KIND=8)
  ncpix = INT(scalar_i2,KIND=4) ! Also store for cloud iteration
  
  ! --------------------------
  ! Read pixel cloud fractions
  ! --------------------------
  IF( ncpix .GT. 0 ) THEN
    
    var_id = ncvid(ncid, 'CloudPixelFractions', rcode)
    if (rcode  .eq. -1 ) then
      message =  ' error in netcdf_rd_dim: ncvid failed()'
      fail = .true.; return
    endif
    call ncvgt(ncid, var_id, (/nc_xid,nc_yid,1/), (/1,1,ncpix/), pixel_cf(1:ncpix), rcode)
    if (rcode  .eq. -1 ) then
      message =  ' error in netcdf_rd_dim: ncvgt failed()'
      fail = .true.; return
    endif
    
    ! Set total cloud fraction
    footprint_data(15) = SUM(pixel_cf(1:ncpix))
    
    ! Allocate 2d arrays for later reading
    ALLOCATE(vmid2d_r4(lmx,ncpix))
    
  ELSE
    
    ! Set total cloud fraction
    footprint_data(15) = 0.0d0
    
    ! Initialize pixel_pcld
    pixel_pcld(:) = 0.0d0
    
    ! Also read cloud pressures if we are doing a lambertian cloud calculation
    IF( do_lambertian_cld ) THEN
      
      var_id = ncvid(ncid, 'CloudPixelPressures', rcode)
      if (rcode  .eq. -1 ) then
        message =  ' error in netcdf_rd_dim: ncvid failed()'
        fail = .true.; return
      endif
      call ncvgt(ncid, var_id, (/nc_xid,nc_yid,1/), (/1,1,ncpix/), pixel_pcld(1:ncpix), rcode)
      if (rcode  .eq. -1 ) then
        message =  ' error in netcdf_rd_dim: ncvgt failed()'
        fail = .true.; return
      endif
      
    ENDIF
    
  ENDIF
  
  ! ------------------
  ! Read snow fraction
  ! ------------------
  var_id = ncvid(ncid, 'SnowFraction', rcode)
  if (rcode  .eq. -1 ) then
     message =  ' error in netcdf_rd_dim: ncvid failed(SnowFraction)'
     fail = .true.; return
  endif
  call ncvgt(ncid, var_id, (/nc_xid,nc_yid/), (/1,1/), scalar_r4, rcode)
  if (rcode  .eq. -1 ) then
     message =  ' error in netcdf_rd_dim: ncvgt failed(SnowFraction)'
     fail = .true.; return
  endif
  snowfrac = REAL(scalar_r4,KIND=8)
  
  ! ------------------
  ! Read snow depth
  ! ------------------
  var_id = ncvid(ncid, 'SnowDepth', rcode)
  if (rcode  .eq. -1 ) then
     message =  ' error in netcdf_rd_dim: ncvid failed(SnowDepth)'
     fail = .true.; return
  endif
  call ncvgt(ncid, var_id, (/nc_xid,nc_yid/), (/1,1/), scalar_r4, rcode)
  if (rcode  .eq. -1 ) then
     message =  ' error in netcdf_rd_dim: ncvgt failed(SnowDepth)'
     fail = .true.; return
  endif
  snowdepth = REAL(scalar_r4,KIND=8)
  
  ! ----------------------
  ! Read sea ice fraction
  ! ----------------------
  var_id = ncvid(ncid, 'SeaIceFraction', rcode)
  if (rcode  .eq. -1 ) then
     message =  ' error in netcdf_rd_dim: ncvid failed(SeaIceFraction)'
     fail = .true.; return
  endif
  call ncvgt(ncid, var_id, (/nc_xid,nc_yid/), (/1,1/), scalar_r4, rcode)
  if (rcode  .eq. -1 ) then
     message =  ' error in netcdf_rd_dim: ncvgt failed(SeaIceFraction)'
     fail = .true.; return
  endif
  seaicefrac = REAL(scalar_r4,KIND=8)
  
  ! ----------------------
  ! Read snow age
  ! ----------------------
  var_id = ncvid(ncid, 'TopSnowAge', rcode)
  if (rcode  .eq. -1 ) then
     message =  ' error in netcdf_rd_dim: ncvid failed(TopSnowAge)'
     fail = .true.; return
  endif
  call ncvgt(ncid, var_id, (/nc_xid,nc_yid/), (/1,1/), scalar_r4, rcode)
  if (rcode  .eq. -1 ) then
     message =  ' error in netcdf_rd_dim: ncvgt failed(TopSnowAge)'
     fail = .true.; return
  endif
  snowage = REAL(scalar_r4,KIND=8)
  
  ! ----------------------
  ! Read solar induced fluorescence
  ! ----------------------
  var_id = ncvid(ncid, 'SIF_734nm', rcode)
  if (rcode  .eq. -1 ) then
     message =  ' error in netcdf_rd_dim: ncvid failed(SIF_734nm)'
     fail = .true.; return
  endif
  call ncvgt(ncid, var_id, (/nc_xid,nc_yid/), (/1,1/), scalar_r4, rcode)
  if (rcode  .eq. -1 ) then
     message =  ' error in netcdf_rd_dim: ncvgt failed(SIF_734nm)'
     fail = .true.; return
  endif
  sif_734nm = REAL(scalar_r4,KIND=8)

  ! -------------------------
  ! Read Observation Altitude
  ! -------------------------
  IF (do_yn_obsalt) THEN
     var_id = ncvid(ncid, 'ObservationAltitude', rcode)
     if (rcode  .eq. -1 ) then
        message =  ' error in netcdf_rd_dim: ncvid failed(ObservationAltitude)'
        fail = .true.; return
     endif
     call ncvgt(ncid, var_id, (/nc_xid,nc_yid/), (/1,1/), scalar_r4, rcode)
     if (rcode  .eq. -1 ) then
        message =  ' error in netcdf_rd_dim: ncvgt failed(ObservationAltitude)'
        fail = .true.; return
     endif
     obsalt = REAL(scalar_r4,KIND=8)
  END IF
  
  ! ===============================
  ! Read Profile information
  ! ===============================
  
  ! Initialize column count
  nc = 0
  
  ! -----------------------------------------
  ! The first column is just the level index
  ! -----------------------------------------
  
  ! Initialize profids
  profids = 'Lev'
  
  ! -----------------------------------------
  ! Pressure
  ! -----------------------------------------

  ! Increment column
  nc = nc + 1
  
  ! Add to profids
  profids = TRIM(ADJUSTL(profids)) // ' Pressure'
  
  ! Read netcdf file
  var_id = ncvid(ncid, 'Pressure', rcode)
  if (rcode  .eq. -1 ) then
    message =  ' error in netcdf_rd_dim: ncvid failed(Pressure)'
    fail = .true.; return
  endif
  call ncvgt(ncid, var_id, (/nc_xid,nc_yid,1/), (/1,1,nr/), vedge_r4, rcode)
  if (rcode  .eq. -1 ) then
    message =  ' error in netcdf_rd_dim: ncvgt failed(Pressure)'
    fail = .true.; return
  endif
  
  ! Add to profile data
  profile_data(1:nr,nc) = vedge_r4(1:nr)
  
  ! -----------------------------------------
  ! Altitude
  ! -----------------------------------------
  
  ! Increment column
  nc = nc + 1
  
  ! Add to profids
  profids = TRIM(ADJUSTL(profids)) // ' Altitude'
  
  ! Read from netcdf file
  var_id = ncvid(ncid, 'Altitude', rcode)
  if (rcode  .eq. -1 ) then
    message =  ' error in netcdf_rd_dim: ncvid failed(Altitude)'
    fail = .true.; return
  endif
  call ncvgt(ncid, var_id, (/nc_xid,nc_yid,1/), (/1,1,nr/), vedge_r4, rcode)
  if (rcode  .eq. -1 ) then
    message =  ' error in netcdf_rd_dim: ncvgt failed(Altitude)'
    fail = .true.; return
  endif
  
  ! Add to profile data
  profile_data(1:nr,nc) = vedge_r4(1:nr)
  
  print*,'---->',nc
  DO i=1,lmx+1
    print*,i,vedge_r4(i),profile_data(i,nc)
  ENDDO
  
  
  ! -----------------------------------------
  ! TATM
  ! -----------------------------------------
  
  ! Increment column
  nc = nc + 1
  
  ! Add to profids
  profids = TRIM(ADJUSTL(profids)) // ' TATM'
  
  ! Read netcdf file
  var_id = ncvid(ncid, 'TATM', rcode)
  if (rcode  .eq. -1 ) then
    message =  ' error in netcdf_rd_dim: ncvid failed(TATM)'
    fail = .true.; return
  endif
  call ncvgt(ncid, var_id, (/nc_xid,nc_yid,1/), (/1,1,nr/), vedge_r4, rcode)
  if (rcode  .eq. -1 ) then
    message =  ' error in netcdf_rd_dim: ncvgt failed(TATM)'
    fail = .true.; return
  endif
  
  ! Add to profile data
  profile_data(1:nr,nc) = vedge_r4(1:nr)
  
  ! -----------------------------------------
  ! EtaA
  ! -----------------------------------------
  ! Increment column
  nc = nc + 1
  
  ! Add to profids
  profids = TRIM(ADJUSTL(profids)) // ' EtaA'
  
  ! Read Pressure from netcdf file
  var_id = ncvid(ncid, 'EtaA', rcode)
  if (rcode  .eq. -1 ) then
    message =  ' error in netcdf_rd_dim: ncvid failed(EtaA)'
    fail = .true.; return
  endif
  call ncvgt(ncid, var_id, (/1/), (/nr/), vedge_r4, rcode)
  if (rcode  .eq. -1 ) then
    message =  ' error in netcdf_rd_dim: ncvgt failed(EtaA)'
    fail = .true.; return
  endif
  
  ! Add to profile data
  profile_data(1:nr,nc) = vedge_r4(1:nr)
  
  ! -----------------------------------------
  ! EtaB
  ! -----------------------------------------
  ! Increment column
  nc = nc + 1
  
  ! Adde to profids
  profids = TRIM(ADJUSTL(profids)) // ' EtaB'
  
  ! Read from netcdf file
  var_id = ncvid(ncid, 'EtaB', rcode)
  if (rcode  .eq. -1 ) then
    message =  ' error in netcdf_rd_dim: ncvid failed(EtaB)'
    fail = .true.; return
  endif
  call ncvgt(ncid, var_id, (/1/), (/nr/), vedge_r4, rcode)
  if (rcode  .eq. -1 ) then
    message =  ' error in netcdf_rd_dim: ncvgt failed(EtaB)'
    fail = .true.; return
  endif
  
  ! Add to profile data
  profile_data(1:nr,nc) = vedge_r4(1:nr)
 
  ! =====================================
  ! Add Gases to profile
  ! =====================================
  DO i=1,ngases
    
    IF( which_gases(i) .NE. 'O4  ' .AND. &
        which_gases(i) .NE. 'O2  '        ) THEN
      
      ! Increment count
      nc = nc + 1

      ! Adde to profids
      profids = TRIM(ADJUSTL(profids)) // ' ' // &
                TRIM(ADJUSTL(which_gases(i)))

      ! Read netcdf file
      var_id = ncvid(ncid,TRIM(ADJUSTL(which_gases(i))),rcode)
      if (rcode  .eq. -1 ) then
        message =  ' error in netcdf_rd_dim: ncvid failed(GAS)'
        fail = .true.; return
      endif
      call ncvgt(ncid, var_id, (/nc_xid,nc_yid,1/), (/1,1,lmx/), vmid_r4, rcode)
      if (rcode  .eq. -1 ) then
        message =  ' error in netcdf_rd_dim: ncvgt failed(GAS)'
        fail = .true.; return
      endif
      
      ! Add to profile data
      profile_data(1:lmx,nc) = vmid_r4
      profile_data(nr,nc) = 0.0d0
      
    ENDIF
  ENDDO
  
  ! =====================================
  ! Add Aerosols to profile
  ! =====================================
  
  IF (do_aerosols .AND. use_aerprof ) THEN
     DO i=1,naer
        
        ! Find optical depth field corresponding to aer type
        !var_id = ncvid(ncid, 'O' // TRIM(ADJUSTL(aer_types(i))),rcode)
        var_id = ncvid(ncid, TRIM(ADJUSTL(aer_types(i))),rcode)
        
        IF( rcode .EQ. 0 ) THEN

           ! Increment column
           nc = nc + 1

           profids = TRIM(ADJUSTL(profids)) // ' ' // &
                'O' // TRIM(ADJUSTL(aer_types(i)))

           call ncvgt(ncid, var_id, (/nc_xid,nc_yid,1/), (/1,1,lmx/), vmid_r4, rcode)
           if (rcode  .eq. -1 ) then
              message =  ' error in netcdf_rd_dim: ncvgt failed(AER)'
              fail = .true.; return
           endif

           ! Add to profile data
           profile_data(1:lmx,nc) = vmid_r4
           profile_data(nr,nc) = 0.0d0

        ELSE

           print*,'NO field for ' // TRIM(ADJUSTL(aer_types(i)))

        ENDIF


     ENDDO

  ENDIF
  
  ! =====================================
  ! Add Cloud Optical depths if necessary
  ! =====================================
  
  IF (do_clouds) THEN
     IF( ncpix .GT. 0 ) THEN
        DO i=1,ncld ! Loop over cloud types

           !cld_fldname = 'O' // TRIM(ADJUSTL(cld_types(i)))
           cld_fldname = TRIM(ADJUSTL(cld_types(i)))

           ! Read the field
           var_id = ncvid(ncid,cld_fldname,rcode)
           if (rcode  .eq. -1 ) then
              message =  ' error in netcdf_rd_dim: ncvid failed()'
              fail = .true.; return
           endif
           call ncvgt(ncid, var_id, (/nc_xid,nc_yid,1,1/), (/1,1,lmx, ncpix/), vmid2d_r4, rcode)
           if (rcode  .eq. -1 ) then
              message =  ' error in netcdf_rd_dim: ncvgt failed(AER)'
              fail = .true.; return
           endif


           DO j=1, ncpix

              ! Increment counter
              nc = nc + 1

              !         print*,i,j

              ! Convert j to strnig
              WRITE(numstr,'(I3)') j

              ! Adde to profids
              profids = TRIM(ADJUSTL(profids)) // ' ' // &
                   cld_fldname // TRIM(ADJUSTL(numstr))

              ! Add to profile data
              profile_data(1:lmx,nc) = vmid2d_r4(1:lmx,j)
              profile_data(lmx+1,nc) = 0.0d0
           ENDDO

        ENDDO
        IF (ALLOCATED(vmid2d_r4)) DEALLOCATE(vmid2d_r4)
     ENDIF
  ENDIF
  
  ! =====================================
  ! Add RH
  ! =====================================
  
  ! Increment column
  nc = nc + 1
    
  ! Adde to profids
  profids = TRIM(ADJUSTL(profids)) // ' RH'
    
  ! Read netcdf file
  var_id = ncvid(ncid,'RH',rcode)
  if (rcode  .eq. -1 ) then
    message =  ' error in netcdf_rd_dim: ncvid failed(RH)'
    fail = .true.; return
  endif
  call ncvgt(ncid, var_id, (/nc_xid,nc_yid,1/), (/1,1,lmx/), vmid_r4, rcode)
  if (rcode  .eq. -1 ) then
    message =  ' error in netcdf_rd_dim: ncvgt failed(RH)'
    fail = .true.; return
  endif
    
  ! Add to profile data
  profile_data(1:lmx,nc) = vmid_r4
  profile_data(nr,nc) = 0.0d0
  
  ! ===========================================
  ! Land Type fractions
  ! ===========================================
  ! Read netcdf file
  var_id = ncvid(ncid,'LandTypeFraction',rcode)
  if (rcode  .eq. -1 ) then
    message =  ' error in netcdf_rd_dim: ncvid failed(LandTypeFraction)'
    fail = .true.; return
  endif
  call ncvgt(ncid, var_id, (/nc_xid,nc_yid,1/), (/1,1,17/), ltype_r4, rcode)
  if (rcode  .eq. -1 ) then
    message =  ' error in netcdf_rd_dim: ncvgt failed(LandTypeFraction)'
    fail = .true.; return
  endif
  
  ltype_frac = real(ltype_r4,kind=8)
  
  ! ==========================
  ! Close file
  ! ==========================
  call ncclos(ncid, rcode)
  if (rcode .eq. -1) then
     message =  ' error in netcdf_rd_dim: ncclos'
     fail = .true.; return
  endif
  
  GC_nlayers = nr - 1
  pnc = nc !- 1
  
end subroutine read_xy_nc_prof

! ==============================================================================================================
! ==============================================================================================================
! ==============================================================================================================
! NEW ROUTINES
! ==============================================================================================================
! ==============================================================================================================
! ==============================================================================================================
    
  SUBROUTINE match_names_in_dimlist( DIMNAMES, ND, DIM_ID, DIMS, ERROR )
      
      USE GC_variables_module, ONLY : nc_dimlist, nc_ndimlist
      
      ! ---------------------------
      ! Subroutine arguments
      ! ---------------------------
      INTEGER,                         INTENT(IN)  :: ND
      CHARACTER(LEN=*), DIMENSION(ND), INTENT(IN)  :: DIMNAMES
      INTEGER,          DIMENSION(ND), INTENT(OUT) :: DIM_ID, DIMS
      LOGICAL,                         INTENT(OUT) :: ERROR
      
      ! ---------------------------
      ! Local variables
      ! ---------------------------
      INTEGER :: n,d
      
      ! =====================================================
      ! match_names_in_dimlist starts here
      ! =====================================================
      
      ! Initialize error
      ERROR = .FALSE.
      
      ! Initialize dim_id
      DIM_ID(:) = -1
      
      ! Match dimension names
      DO n=1,nc_ndimlist
        DO d=1,ND
          IF( TRIM(ADJUSTL(DIMNAMES(d))) .EQ. TRIM(ADJUSTL(nc_dimlist(n)%NAME)) ) THEN
            DIM_ID(d) = nc_dimlist(n)%NCID
            DIMS(d)   = nc_dimlist(n)%DIMSIZE
          ENDIF
        ENDDO
      ENDDO
      
      ! Error check
      DO d=1,ND
        IF( DIM_ID(d) < 0 ) ERROR = .TRUE.
      ENDDO
      
    END SUBROUTINE match_names_in_dimlist
    
    SUBROUTINE create_ncvar( NCID, VARNAME, DIMNAME, NDIM, NCTYPE, DO_XY, VAR_ID, ERROR  )
    
      IMPLICIT NONE
      INCLUDE 'netcdf.inc'
      
      ! ---------------------------
      ! Subroutine arguments
      ! ---------------------------
      INTEGER,                                    INTENT(IN)  :: NCID, NDIM, NCTYPE
      CHARACTER(LEN=*),                           INTENT(IN)  :: VARNAME
      CHARACTER(LEN=*),          DIMENSION(NDIM), INTENT(IN)  :: DIMNAME
      LOGICAL,                                    INTENT(IN)  :: DO_XY
      INTEGER,                                    INTENT(OUT) :: VAR_ID
      LOGICAL,                                    INTENT(OUT) :: ERROR
      
      ! ---------------------------
      ! Local variables
      ! ---------------------------
      INTEGER,                                DIMENSION(NDIM) :: DIM_ID, DIMS
      INTEGER,                   ALLOCATABLE, DIMENSION(:)    :: DIM_ID_XY, DIMS_XY, CHUNK
      CHARACTER(LEN=max_ch_len), ALLOCATABLE, DIMENSION(:)    :: DIMNAME_XY
      INTEGER                                                 :: NDIM_XY
      INTEGER                                                 :: n,d, rcode, var_pr
      
      ! =====================================================
      ! create_ncvar starts here
      ! =====================================================
      
      ! Initialize error
      error = .FALSE.
      
      IF( DO_XY ) THEN
        
        ! Append X-Y Dimensions
        NDIM_XY = NDIM+2
        
        ! Allocate arrays
        ALLOCATE(  DIM_ID_XY(NDIM_XY) )
        ALLOCATE(    DIMS_XY(NDIM_XY) )
        ALLOCATE(      CHUNK(NDIM_XY) )
        ALLOCATE( DIMNAME_XY(NDIM_XY) )
        
        ! Set dimension names
        DIMNAME_XY(1) = 'ew_npix'; DIMNAME_XY(2) = 'ns_npix'
        DO d=1,NDIM
          DIMNAME_XY(d+2) = DIMNAME(d)
        ENDDO
        
        ! Match the dimension names
        CALL match_names_in_dimlist( DIMNAME_XY, NDIM_XY, DIM_ID_XY, DIMS_XY, ERROR )
        
        ! Check for error
        IF( error ) THEN
          print*,'Could not find all variables for ' // TRIM(ADJUSTL(VARNAME))
          STOP
        ENDIF
        
        ! Create variable
        rcode = NF_DEF_VAR( NCID, TRIM(ADJUSTL(varname)), NCTYPE, NDIM_XY, DIM_ID_XY, VAR_ID )
        
        ! Check error 
        CALL netcdf_handle_error( 'create_ncvar', rcode )
        
        ! Set up chunking array
        CHUNK(1:2)       = 1
        CHUNK(3:NDIM_XY) = DIMS_XY(3:NDIM_XY)
        
        ! Define chunking
        rcode = NF_DEF_VAR_CHUNKING(NCID, VAR_ID, NF_CHUNKED, CHUNK)
        
        ! Check error 
        CALL netcdf_handle_error( 'create_ncvar', rcode )
        
        ! Deallocate arrays
        DEALLOCATE(  DIM_ID_XY )
        DEALLOCATE(    DIMS_XY )
        DEALLOCATE(      CHUNK )
        DEALLOCATE( DIMNAME_XY )
        
      ELSE
        
        ! Match the dimension names
        CALL match_names_in_dimlist( DIMNAME, NDIM, DIM_ID, DIMS, ERROR )
        
        ! Check for error
        IF( error ) THEN
          print*,'Could not find all variables for ' // TRIM(ADJUSTL(VARNAME))
          STOP
        ENDIF
        
        ! Create variable
        rcode = NF_DEF_VAR( NCID, TRIM(ADJUSTL(varname)), NCTYPE, NDIM, DIM_ID, VAR_ID )
        
        ! Check error
        CALL netcdf_handle_error( 'create_ncvar', rcode )
        
      ENDIF
      
    END SUBROUTINE create_ncvar
    
    SUBROUTINE write_ncvar( NCID, VARNAME, START, SUBDIM, NDIM, VDIM, NCTYPE, DO_XY, ERROR, DATA_r8, DATA_i4 )
      
      USE GC_variables_module, ONLY : nc_xid, nc_yid
      
      IMPLICIT NONE
      INCLUDE 'netcdf.inc'
      
      ! ---------------------------
      ! Subroutine arguments
      ! ---------------------------
      
      INTEGER,                                    INTENT(IN)  :: NCID, NCTYPE, NDIM, VDIM
      CHARACTER(LEN=*),                           INTENT(IN)  :: VARNAME
      INTEGER,                   DIMENSION(NDIM), INTENT(IN)  :: START, SUBDIM
      LOGICAL,                                    INTENT(IN)  :: DO_XY
      LOGICAL,                                    INTENT(OUT) :: ERROR
      REAL(KIND=8),    OPTIONAL, DIMENSION(VDIM), INTENT(IN)  :: DATA_r8
      INTEGER(KIND=4), OPTIONAL, DIMENSION(VDIM), INTENT(IN)  :: DATA_i4
      
      ! ---------------------------
      ! Local variables
      ! ---------------------------
      INTEGER                            :: rcode, var_prec, NDIM_OUT, VAR_ID
      INTEGER, ALLOCATABLE, DIMENSION(:) :: START_OUT, SUBDIM_OUT
      INTEGER, DIMENSION(7)              :: ARDIM ! For unwrapping the vector
      
      ! =====================================================
      ! create_ncvar starts here
      ! =====================================================
      
      ! Initialize Error 
      error = .FALSE.
      
      ! Figure out variable precision
      IF(     NCTYPE .EQ. nf_byte  .OR. NCTYPE .EQ. nf_char      ) THEN
        var_prec = 1
      ELSEIF( NCTYPE .EQ. nf_short .OR. NCTYPE .EQ. nf_ushort    ) THEN
        var_prec = 2
      ELSEIF( NCTYPE .EQ. nf_int   .OR. NCTYPE .EQ. nf_uint   .OR. &
              NCTYPE .EQ. nf_float                               ) THEN
        var_prec = 4
      ELSEIF( NCTYPE .EQ. nf_int64 .OR. NCTYPE .EQ. nf_uint64 .OR. &
              NCTYPE .EQ. nf_double                              ) THEN
        var_prec = 8
      ELSE
        print*, 'Error writing field for variable: ' // TRIM(ADJUSTL(VARNAME))
        print*, 'Could not find precision for NC variable type:', nctype
        STOP 
      ENDIF
      
      ! Check if we are writing to XY
      IF( DO_XY ) THEN
        
        ! Dimensions of XY output
        NDIM_OUT = NDIM + 2
        
        ! Allocate start/count arrays
        ALLOCATE(  START_OUT(NDIM_OUT) )
        ALLOCATE( SUBDIM_OUT(NDIM_OUT) )
        
        ! Fill start/count arrays
        START_OUT(1)          = nc_xid; START_OUT(2) = nc_yid
        START_OUT(3:NDIM_OUT) = START(1:NDIM)
        SUBDIM_OUT(1:2) = 1
        SUBDIM_OUT(3:NDIM_OUT) = SUBDIM(1:NDIM)
        
      ELSE
        
        ! Kludge
        NDIM_OUT = NDIM
        
        ! Allocate start/count arrays
        ALLOCATE(  START_OUT(NDIM_OUT) )
        ALLOCATE( SUBDIM_OUT(NDIM_OUT) )
        
        ! Fill start/count arrays
        START_OUT = START ; SUBDIM_OUT = SUBDIM
        
      ENDIF
      
      ! Get variable index
      rcode = NF_INQ_VARID( NCID, TRIM(ADJUSTL(VARNAME)), VAR_ID)
      
      ! Check error 
      CALL netcdf_handle_error( 'write_ncvar', rcode )
      
      ! Reshaping dimension
      ARDIM(:) = 1
      ARDIM( 1:NDIM ) = SUBDIM
      
      ! Write the variable
      IF( PRESENT(DATA_r8) ) THEN
        
        ! Write the variable
        SELECT CASE( NCTYPE )
          CASE( nf_float )
            rcode = NF_PUT_VARA_REAL(NCID, VAR_ID, START_OUT, SUBDIM_OUT,  &
                                     REAL( RESHAPE(DATA_r8,ARDIM), KIND=4) )
          CASE( nf_double )
            rcode = NF_PUT_VARA_DOUBLE(NCID, VAR_ID, START_OUT, SUBDIM_OUT, &
                                       RESHAPE(DATA_r8,ARDIM)               )
          CASE DEFAULT
            print*, 'No REAL output type for var_prec=', var_prec
            STOP
        END SELECT
        
        ! Check error 
        CALL netcdf_handle_error( 'write_ncvar', rcode )
        
        
      ELSEIF( PRESENT(DATA_i4) ) THEN
        
        ! Write the variable
        SELECT CASE( NCTYPE )
          CASE( nf_byte )
            rcode = NF_PUT_VARA_INT1( NCID, VAR_ID, START_OUT, SUBDIM_OUT,  &
                                      INT( RESHAPE(DATA_i4,ARDIM), KIND=1 ) )
          CASE( nf_int2 )
            rcode = NF_PUT_VARA_INT2( NCID, VAR_ID, START_OUT, SUBDIM_OUT,  &
                                      INT( RESHAPE(DATA_i4,ARDIM), KIND=2 ) )
          CASE( nf_int )
            rcode = NF_PUT_VARA_INT( NCID, VAR_ID, START_OUT, SUBDIM_OUT,  &
                                     RESHAPE(DATA_i4,ARDIM)                )
          CASE DEFAULT
            print*, 'No INT output type for var_prec=', var_prec
            STOP
        END SELECT
        
        ! Check error 
        CALL netcdf_handle_error( 'write_ncvar', rcode )
        
      ELSE
        
        print*,'Error: No data present for variable' // TRIM(ADJUSTL(VARNAME))
        STOP
        
      ENDIF
      
      ! Deallocate arrays
      DEALLOCATE( START_OUT  )
      DEALLOCATE( SUBDIM_OUT )
      
    END SUBROUTINE write_ncvar
    
    SUBROUTINE nc_fld_1d( DIM_1D, LMX, PROFDATA, FLD_NAME, NCID, ACTION, DO_XY )
   
      IMPLICIT NONE
      INCLUDE 'netcdf.inc'
    
      ! ---------------------------
      ! Subroutine arguments
      ! ---------------------------
      INTEGER,                                  INTENT(IN) :: LMX
      CHARACTER(LEN=max_ch_len), DIMENSION(1),  INTENT(IN) :: DIM_1D
      CHARACTER(LEN=*         ),                INTENT(IN) :: FLD_NAME
      REAL(KIND=8),              DIMENSION(LMX),INTENT(IN) :: PROFDATA
      INTEGER, INTENT(IN) :: NCID
      INTEGER, INTENT(IN) :: ACTION
      LOGICAL, INTENT(IN) :: DO_XY
      
      ! ---------------
      ! Local variables
      ! ---------------
      INTEGER                                 :: VAR_ID
      LOGICAL                                 :: error
      
      ! =====================================================
      ! nc_fld_1d starts here
      ! =====================================================
      
      ! Intialize error
      error = .FALSE.
      
      ! Define field
      IF( ACTION == 1 ) THEN
        
        CALL create_ncvar(     NCID, TRIM(ADJUSTL(fld_name)),   DIM_1D,      1, &
                           nf_float,                   DO_XY,   VAR_ID,  ERROR  )
        
        
      ! Write field
      ELSEIF( ACTION == 2 ) THEN
        
        ! Write 
        CALL write_ncvar( NCID, TRIM(ADJUSTL(fld_name)),  (/1/), (/lmx/),     1,   &
                           lmx,        nf_float,  DO_XY,  ERROR, DATA_r8=profdata  )
        
      ELSE
        
        print*,'nc_fld_1d: No action for case ', ACTION
        STOP
        
      ENDIF
      
    END SUBROUTINE nc_fld_1d
    
    SUBROUTINE nc_fld_2d( DIM_2D, IMX, JMX, PROFDATA, FLD_NAME, NCID, ACTION, DO_XY )
      
      IMPLICIT NONE
      INCLUDE 'netcdf.inc'
      
      ! ---------------------------
      ! Subroutine arguments
      ! ---------------------------
      INTEGER,                                      INTENT(IN) :: IMX,JMX
      CHARACTER(LEN=max_ch_len), DIMENSION(2),      INTENT(IN) :: DIM_2D
      CHARACTER(LEN=*         ),                    INTENT(IN) :: FLD_NAME
      REAL(KIND=8),              DIMENSION(IMX,JMX),INTENT(IN) :: PROFDATA
      INTEGER, INTENT(IN) :: NCID
      INTEGER, INTENT(IN) :: ACTION
      LOGICAL, INTENT(IN) :: DO_XY
      
      ! ---------------
      ! Local variables
      ! ---------------
      INTEGER                                 :: VAR_ID, npt
      LOGICAL                                 :: error
      
      ! =====================================================
      ! nc_fld_2d starts here
      ! =====================================================
      
      ! Intialize error
      error = .FALSE.
      
      ! Define field
      IF( ACTION == 1 ) THEN
        
        CALL create_ncvar(     NCID, TRIM(ADJUSTL(fld_name)),   DIM_2D,      2, &
                           nf_float,                   DO_XY,   VAR_ID,  ERROR  )
        
      ! Write field
      ELSEIF( ACTION == 2 ) THEN
        
        npt = imx*jmx
        
        ! Write 
        CALL write_ncvar( NCID, TRIM(ADJUSTL(fld_name)),  (/1,1/), (/imx,jmx/), 2, &
                           npt,        nf_float,  DO_XY,  ERROR,                   &
                           DATA_r8= RESHAPE(PROFDATA,(/npt/))                      )
        
      ELSE
        
        print*,'nc_fld_2d: No action for case ', ACTION
        STOP
        
      ENDIF
      
      RETURN
      
    END SUBROUTINE nc_fld_2d
    
    SUBROUTINE nc_fld_3d( DIM_3D, IMX, JMX, LMX, PROFDATA, FLD_NAME, NCID, ACTION, DO_XY )
      
      IMPLICIT NONE
      INCLUDE 'netcdf.inc'
      
      ! ---------------------------
      ! Subroutine arguments
      ! ---------------------------
      INTEGER,                                          INTENT(IN) :: IMX,JMX,LMX
      CHARACTER(LEN=max_ch_len), DIMENSION(3),          INTENT(IN) :: DIM_3D
      CHARACTER(LEN=*         ),                        INTENT(IN) :: FLD_NAME
      REAL(KIND=8),              DIMENSION(IMX,JMX,LMX),INTENT(IN) :: PROFDATA
      INTEGER, INTENT(IN) :: NCID
      INTEGER, INTENT(IN) :: ACTION
      LOGICAL, INTENT(IN) :: DO_XY
      
      ! ---------------
      ! Local variables
      ! ---------------
      INTEGER                                 :: VAR_ID, npt
      LOGICAL                                 :: error
      
      ! =====================================================
      ! nc_fld_3d starts here
      ! =====================================================
      
      ! Intialize error
      error = .FALSE.
      
      ! Define field
      IF( ACTION == 1 ) THEN
        
        CALL create_ncvar(     NCID, TRIM(ADJUSTL(fld_name)),   DIM_3D,      3, &
                           nf_float,                   DO_XY,   VAR_ID,  ERROR  )
        
      ! Write field
      ELSEIF( ACTION == 2 ) THEN
        
        npt = imx*jmx*lmx
        
        ! Write field
        CALL write_ncvar( NCID, TRIM(ADJUSTL(fld_name)), (/1,1,1/),    (/imx,jmx,lmx/), 3,   &
                           npt,                nf_float,     DO_XY,  ERROR,                  &
                          DATA_r8= RESHAPE(PROFDATA,(/npt/))                                 )
        
      ELSE
        
        print*,'nc_fld_3d: No action for case ', ACTION
        STOP
        
      ENDIF
      
      RETURN
      
    END SUBROUTINE nc_fld_3d
    
    SUBROUTINE nc_fld_4d( DIM_4D, IMX, JMX, LMX, NMX, PROFDATA, FLD_NAME, NCID, ACTION, DO_XY )
      
      IMPLICIT NONE
      INCLUDE 'netcdf.inc'
      
      ! ---------------------------
      ! Subroutine arguments
      ! ---------------------------
      INTEGER,                                              INTENT(IN) :: IMX,JMX,LMX,NMX
      CHARACTER(LEN=max_ch_len), DIMENSION(4),              INTENT(IN) :: DIM_4D
      CHARACTER(LEN=*         ),                            INTENT(IN) :: FLD_NAME
      REAL(KIND=8),              DIMENSION(IMX,JMX,LMX,NMX),INTENT(IN) :: PROFDATA
      INTEGER, INTENT(IN) :: NCID
      INTEGER, INTENT(IN) :: ACTION
      LOGICAL, INTENT(IN) :: DO_XY
      
      ! ---------------
      ! Local variables
      ! ---------------
      INTEGER                                 :: VAR_ID, npt
      LOGICAL                                 :: error
      
      ! =====================================================
      ! nc_fld_3d starts here
      ! =====================================================
      
      ! Intialize error
      error = .FALSE.
      
      ! Define field
      IF( ACTION == 1 ) THEN
        
        CALL create_ncvar(     NCID, TRIM(ADJUSTL(fld_name)),   DIM_4D,      4, &
                           nf_float,                   DO_XY,   VAR_ID,  ERROR  )
        
      ! Write field
      ELSEIF( ACTION == 2 ) THEN
        
        npt = imx*jmx*lmx*nmx
        
        ! Write field
        CALL write_ncvar( NCID, TRIM(ADJUSTL(fld_name)), (/1,1,1,1/),    (/imx,jmx,lmx,nmx/), 4, &
                           npt,                nf_float,     DO_XY,  ERROR,                      &
                          DATA_r8= RESHAPE(PROFDATA,(/npt/))                                     )
        
      ELSE
        
        print*,'nc_fld_4d: No action for case ', ACTION
        STOP
        
      ENDIF
      
      RETURN
      
    END SUBROUTINE nc_fld_4d
    
END MODULE GC_netcdf_module
