!------------------------------------------------------------------------------
!          Harvard University Atmospheric Chemistry Modeling Group            !
!------------------------------------------------------------------------------
!BOP
!
! !MODULE: GC_diag_cldaer_module.f90
!
! !DESCRIPTION: This module contains routines for writing cloud/aerosol jacobian diagnostics
!\\
!\\
! !INTERFACE: 
!
MODULE GC_diag_cldaer_module
    
    USE GC_parameters_module
    USE GC_netcdf_module,      ONLY : create_ncvar, write_ncvar, &
                                      match_names_in_dimlist,    &
                                      nc_fld_1d, nc_fld_2d,      &
                                      nc_fld_3d, nc_fld_4d
    
    IMPLICIT NONE
    INCLUDE 'netcdf.inc'
    
    CONTAINS
    
    SUBROUTINE do_cldaer_jacobians( NCID, ACTION, DO_XY, DIAG_NUM )
      
      USE GC_variables_module, ONLY : aer_opdeps, aer_ssalbs,                 &
                                      GC_cod_Jacobians, GC_cod_QJacobians,    &
                                      GC_cod_UJacobians, do_stokes_ad38,      &
                                      GC_cfrac_jacobians, GC_cfrac_Qjacobians,&
                                      GC_cfrac_Ujacobians, do_stokes_ad36,    &
                                      do_cld_columnwf, didx,                  &
                                      GC_cssa_Jacobians, GC_cssa_QJacobians,  &
                                      GC_cssa_UJacobians, do_stokes_ad39
      
      ! ---------------------------
      ! Subroutine arguments
      ! ---------------------------
      INTEGER, INTENT(IN) :: NCID
      INTEGER, INTENT(IN) :: ACTION
      LOGICAL, INTENT(IN) :: DO_XY
      INTEGER, INTENT(IN) :: DIAG_NUM
      
      ! ---------------
      ! Local variables
      ! ---------------
      INTEGER                                 :: VAR_ID
      LOGICAL                                 :: error, dummy_err
      INTEGER, DIMENSION(1)                   :: size_1d, tmp_1d
      CHARACTER(LEN=max_ch_len), DIMENSION(1) :: dim_1d
      INTEGER, DIMENSION(2)                   :: size_2d, tmp_2d
      CHARACTER(LEN=max_ch_len), DIMENSION(2) :: dim_2d
      INTEGER, DIMENSION(3)                   :: size_3d, tmp_3d
      CHARACTER(LEN=max_ch_len), DIMENSION(3) :: dim_3d
      INTEGER, DIMENSION(4)                   :: size_4d, tmp_4d
      CHARACTER(LEN=max_ch_len), DIMENSION(4) :: dim_4d
      REAL(KIND=8)                            :: cftot
      
      ! =====================================================
      ! do_profile_diag starts here
      ! =====================================================
      
      ! Initialize error
      error = .FALSE.
      
      SELECTCASE( DIAG_NUM )
      
        ! ======================================================
        ! (34) AOD Jacobians
        ! ======================================================
        CASE( 34 )
          
          CALL diag_34( NCID, ACTION, DO_XY )
          
        ! ======================================================
        ! (35) Aerosol SSA Jacobians
        ! ======================================================
        CASE( 35 )
          
          CALL diag_35( NCID, ACTION, DO_XY )
          
        ! ======================================================
        ! (36) Cloud Fraction
        ! ======================================================
        CASE( 36 )
          
          ! Dimensions
          dim_3d(1) = 'nw' ; dim_3d(2) = 'ngeom' ; dim_3d(3) = 'noutputlevel'
          
          ! Get dimensions
          CALL match_names_in_dimlist( dim_3d, 3, tmp_3d, size_3d, dummy_err )
          
          CALL nc_fld_3d( dim_3d, size_3d(1), size_3d(2), size_3d(3),                       &
                          GC_cfrac_jacobians(1:size_3d(1),1:size_3d(2),1:size_3d(3),didx),&
                          'cfrac_jac',  NCID, ACTION, DO_XY                                 )
          
          IF( do_stokes_ad36 ) THEN
            
            CALL nc_fld_3d( dim_3d, size_3d(1), size_3d(2), size_3d(3),                       &
                          GC_cfrac_Qjacobians(1:size_3d(1),1:size_3d(2),1:size_3d(3),didx),&
                          'cfrac_qjac',  NCID, ACTION, DO_XY                                 )
            
            CALL nc_fld_3d( dim_3d, size_3d(1), size_3d(2), size_3d(3),                       &
                          GC_cfrac_Ujacobians(1:size_3d(1),1:size_3d(2),1:size_3d(3),didx),&
                          'cfrac_ujac',  NCID, ACTION, DO_XY                                 )
            
          ENDIF
          
        ! ======================================================
        ! (37) Cloud Top Pressure
        ! ======================================================
        CASE( 37 )
          
          print*,'Cloud Top Pressure Jacobian has yet to be implemented!!'
          
        ! ======================================================
        ! (38) Total Cloud Optical Depth
        ! ======================================================
        CASE( 38 )
          
        
          IF( do_cld_columnwf ) THEN
              
            ! Dimensions
            dim_3d(1) = 'nw' ; dim_3d(2) = 'ngeom' ; dim_3d(3) = 'noutputlevel'
              
            CALL nc_fld_3d( dim_3d, size_3d(1), size_3d(2), size_3d(3),                     &
                            GC_cod_Jacobians(1:size_3d(1),1,1:size_3d(2),1:size_3d(3),didx),&
                            'codcolwf_jac',  NCID, ACTION, DO_XY                            )
              
            IF( do_stokes_ad38 ) THEN
              
              CALL nc_fld_3d( dim_3d, size_3d(1), size_3d(2), size_3d(3),                      &
                              GC_cod_QJacobians(1:size_3d(1),1,1:size_3d(2),1:size_3d(3),didx),&
                              'codcolwf_qjac',  NCID, ACTION, DO_XY                            )
                
              CALL nc_fld_3d( dim_3d, size_3d(1), size_3d(2), size_3d(3),                      &
                              GC_cod_UJacobians(1:size_3d(1),1,1:size_3d(2),1:size_3d(3),didx),&
                              'codcolwf_ujac',  NCID, ACTION, DO_XY                            )
                
            ENDIF
              
          ELSE
            
            ! The dimension of the output for level variables
            dim_4d(1) = 'nw' ; dim_4d(2) = 'nlayer' ; dim_4d(3) = 'ngeom' ; dim_4d(4) = 'noutputlevel'
            
            CALL nc_fld_4d( dim_4d, size_4d(1), size_4d(2), size_4d(3), size_4d(4),                    &
                            GC_cod_Jacobians(1:size_4d(1),1:size_4d(2),1:size_4d(3),1:size_4d(4),didx),&
                            'cod_jac',  NCID, ACTION, DO_XY                                            )
              
            IF( do_stokes_ad38 ) THEN
              
              CALL nc_fld_4d( dim_4d, size_4d(1), size_4d(2), size_4d(3), size_4d(4),                     &
                              GC_cod_QJacobians(1:size_4d(1),1:size_4d(2),1:size_4d(3),1:size_4d(4),didx),&
                              'cod_qjac',  NCID, ACTION, DO_XY                                            )
              
              CALL nc_fld_4d( dim_4d, size_4d(1), size_4d(2), size_4d(3), size_4d(4),                     &
                              GC_cod_UJacobians(1:size_4d(1),1:size_4d(2),1:size_4d(3),1:size_4d(4),didx),&
                              'cod_ujac',  NCID, ACTION, DO_XY                                            )
                
            ENDIF
            
          ENDIF
          
          
        ! ======================================================
        ! (36) Total Cloud SSA
        ! ======================================================
        CASE( 39 )
          
          IF( do_cld_columnwf ) THEN
              
            ! Dimensions
            dim_3d(1) = 'nw' ; dim_3d(2) = 'ngeom' ; dim_3d(3) = 'noutputlevel'
              
            CALL nc_fld_3d( dim_3d, size_3d(1), size_3d(2), size_3d(3),                     &
                            GC_cssa_Jacobians(1:size_3d(1),1,1:size_3d(2),1:size_3d(3),didx),&
                            'cssacolwf_jac',  NCID, ACTION, DO_XY                            )
              
            IF( do_stokes_ad39 ) THEN
              
              CALL nc_fld_3d( dim_3d, size_3d(1), size_3d(2), size_3d(3),                      &
                              GC_cssa_QJacobians(1:size_3d(1),1,1:size_3d(2),1:size_3d(3),didx),&
                              'cssacolwf_qjac',  NCID, ACTION, DO_XY                            )
                
              CALL nc_fld_3d( dim_3d, size_3d(1), size_3d(2), size_3d(3),                      &
                              GC_cssa_UJacobians(1:size_3d(1),1,1:size_3d(2),1:size_3d(3),didx),&
                              'cssacolwf_ujac',  NCID, ACTION, DO_XY                            )
                
            ENDIF
              
          ELSE
            
            ! The dimension of the output for level variables
            dim_4d(1) = 'nw' ; dim_4d(2) = 'nlayer' ; dim_4d(3) = 'ngeom' ; dim_4d(4) = 'noutputlevel'
            
            CALL nc_fld_4d( dim_4d, size_4d(1), size_4d(2), size_4d(3), size_4d(4),                    &
                            GC_cssa_Jacobians(1:size_4d(1),1:size_4d(2),1:size_4d(3),1:size_4d(4),didx),&
                            'cssa_jac',  NCID, ACTION, DO_XY                                            )
              
            IF( do_stokes_ad38 ) THEN
              
              CALL nc_fld_4d( dim_4d, size_4d(1), size_4d(2), size_4d(3), size_4d(4),                     &
                              GC_cssa_QJacobians(1:size_4d(1),1:size_4d(2),1:size_4d(3),1:size_4d(4),didx),&
                              'cssa_qjac',  NCID, ACTION, DO_XY                                            )
              
              CALL nc_fld_4d( dim_4d, size_4d(1), size_4d(2), size_4d(3), size_4d(4),                     &
                              GC_cod_UJacobians(1:size_4d(1),1:size_4d(2),1:size_4d(3),1:size_4d(4),didx),&
                              'cssa_ujac',  NCID, ACTION, DO_XY                                            )
                
            ENDIF
            
          ENDIF
          
          
        CASE DEFAULT
          
          print*,'The selected diagnostic is not in the aerosol/cloud jacobian diags:', diag_num
          STOP
          
      ENDSELECT
      
    END SUBROUTINE do_cldaer_jacobians
    
    SUBROUTINE archive_cldaer_diag( iw, dir )
      
      USE GC_variables_module, ONLY : do_aod_jacobians, do_total_aod_jac,    &
                                      aodwfidx, do_stokes_ad34,aer_flags,    &
                                      VLIDORT_out, VLIDORT_FixIn,            &
                                      VLIDORT_LinOut, GC_aod_Jacobians,      &
                                      GC_aod_QJacobians, GC_aod_UJacobians,  &
                                      taer_profile, do_aer_columnwf,         &
                                      do_normalized_WFoutput, taertau0,      &
                                      GC_spcaod_Jacobians,                   &
                                      GC_spcaod_QJacobians, Diag34_InpOpt,   &
                                      GC_spcaod_UJacobians, Diag35_InpOpt,   &
                                      aer_profile, do_stokes_ad35,           &
                                      GC_assa_Jacobians, aer_ssalbs,         & 
                                      aer_opdeps, do_assa_Jacobians,         &
                                      assawfidx, do_total_assa_jac,          &
                                      GC_assa_QJacobians, GC_assa_UJacobians,&
                                      do_QU_Jacobians, do_cfrac_jacobians,   &
                                      GC_cfrac_Jacobians, cfrac, cld_flags,  &
                                      stokes_clrcld, GC_cfrac_Jacobians,     &
                                      GC_cfrac_QJacobians, do_stokes_ad36,   &
                                      GC_cfrac_UJacobians, do_cld_columnwf,  &
                                      codwfidx, do_cod_Jacobians,            &
                                      GC_cod_Jacobians, GC_cod_QJacobians,   &
                                      GC_cod_UJacobians,do_stokes_ad38,      &
                                      do_stokes_ad39, tcld_profile, tcldtau0,&
                                      GC_cssa_Jacobians, GC_cssa_QJacobians, &
                                      GC_cssa_UJacobians, cssawfidx,         &
                                      do_cssa_jacobians, cld_ssalbs,         &
                                      cld_opdeps, aer_spc_ssalbs, &
                                      total_wf, total_Qwf,total_Uwf, &
                                      total_aersca, total_aertau, total_vaerssa,&
                                      total_cldsca, total_cldtau, total_vcldssa,&
                                      GC_spcassa_Jacobians,GC_spcassa_QJacobians,&
                                      GC_spcassa_UJacobians
      
      ! --------------------
      ! Subroutine arguments
      ! --------------------
      INTEGER, INTENT(IN) :: iw, dir
      
      ! ---------------
      ! Local variables
      ! ---------------
      INTEGER :: IB, UM, UA, V, q, g, n, il, z0,zf, l, ilev
      
!       REAL(KIND=8) :: 
      
      ! =====================================================
      ! archive_cldaer_diag starts here
      ! =====================================================
      
      ! ==========================================================================================
      ! DIAG 34 - AOD Jacobians
      ! ==========================================================================================
      IF( do_aod_Jacobians ) THEN
        
        ! ----------------------------------------------------------------------------------------
        ! Total AOD Jacobian
        ! ----------------------------------------------------------------------------------------
        IF( do_total_aod_jac ) THEN
          
          ! Index for weighting function
          q = aodwfidx
          
          ! Archive AOD Jacobian
          DO v = 1, VLIDORT_Out%Main%TS_N_GEOMETRIES
            IF (do_aer_columnwf) THEN
              DO ilev = 1, VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS
                total_wf = 0.0d0
                DO il = 1, VLIDORT_FixIn%Cont%TS_NLAYERS
                  IF (aer_flags(il)) THEN
                    total_wf = total_wf + VLIDORT_LinOut%Prof%TS_PROFILEWF(q,il,ilev,v,1,dir)
                  ENDIF
                END DO
                GC_aod_Jacobians(iw, 1, ilev, v,dir) = total_wf               ! Normalized output
                IF ( .NOT. do_normalized_WFoutput ) THEN                      ! Non-normalized output
                  GC_aod_Jacobians(iw, 1, ilev, v,dir) = total_wf / taertau0  ! Using tau at ref. lambda
                ENDIF
              ENDDO
            ELSE
              GC_aod_Jacobians(iw, 1:VLIDORT_FixIn%Cont%TS_NLAYERS, 1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS, v,dir) = &
                VLIDORT_LinOut%Prof%TS_PROFILEWF(q,1:VLIDORT_FixIn%Cont%TS_NLAYERS,                                    &
                1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,1,dir)
              IF ( .NOT. do_normalized_WFoutput ) THEN
                DO ilev = 1, VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS
                  WHERE( aer_flags(1:VLIDORT_FixIn%Cont%TS_NLAYERS) )
                    GC_aod_Jacobians(iw, 1:VLIDORT_FixIn%Cont%TS_NLAYERS, ilev, v, dir) =    &
                    GC_aod_Jacobians(iw, 1:VLIDORT_FixIn%Cont%TS_NLAYERS, ilev, v, dir)      &
                    / taer_profile(1:VLIDORT_FixIn%Cont%TS_NLAYERS)
                  ENDWHERE
                ENDDO
              ENDIF
            ENDIF
          ENDDO
          
          ! Also archive stokes components?
          IF( do_stokes_ad34 .and. do_QU_Jacobians ) THEN
            
            DO v = 1, VLIDORT_Out%Main%TS_N_GEOMETRIES
              IF (do_aer_columnwf) THEN
                DO ilev = 1, VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS
                  total_Qwf = 0.0d0; total_Uwf = 0.0d0
                  DO il = 1, VLIDORT_FixIn%Cont%TS_NLAYERS
                    IF (aer_flags(il)) THEN
                      total_Qwf = total_Qwf + VLIDORT_LinOut%Prof%TS_PROFILEWF(q,il,ilev,v,2,dir)
                      total_Uwf = total_Uwf + VLIDORT_LinOut%Prof%TS_PROFILEWF(q,il,ilev,v,3,dir)
                    END IF
                  ENDDO
                  GC_aod_QJacobians(iw, 1, ilev, v,dir) = total_Qwf               ! Normalized output
                  GC_aod_UJacobians(iw, 1, ilev, v,dir) = total_Uwf               !
                  IF ( .NOT. do_normalized_WFoutput ) THEN                        ! Non-normalized output
                    GC_aod_QJacobians(iw, 1, ilev, v,dir) = total_Qwf / taertau0 ! Using tau at ref. lambda
                    GC_aod_UJacobians(iw, 1, ilev, v,dir) = total_Uwf / taertau0
                  ENDIF
                ENDDO
              ELSE
                GC_aod_QJacobians(iw, 1:VLIDORT_FixIn%Cont%TS_NLAYERS, 1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS, v,dir) = &
                  VLIDORT_LinOut%Prof%TS_PROFILEWF(q,1:VLIDORT_FixIn%Cont%TS_NLAYERS,                                     &
                  1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,2,dir)
                GC_aod_UJacobians(iw, 1:VLIDORT_FixIn%Cont%TS_NLAYERS, 1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS, v,dir) = &
                  VLIDORT_LinOut%Prof%TS_PROFILEWF(q,1:VLIDORT_FixIn%Cont%TS_NLAYERS,                                     &
                  1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,3,dir)
                IF( .NOT. do_normalized_WFoutput ) THEN
                  DO ilev = 1, VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS
                    WHERE( aer_flags(1:VLIDORT_FixIn%Cont%TS_NLAYERS) )
                      GC_aod_QJacobians(iw, 1:VLIDORT_FixIn%Cont%TS_NLAYERS, ilev, v,dir) =    &
                        GC_aod_QJacobians(iw, 1:VLIDORT_FixIn%Cont%TS_NLAYERS, ilev, v,dir)    &
                        / taer_profile(1:VLIDORT_FixIn%Cont%TS_NLAYERS)
                      GC_aod_UJacobians(iw, 1:VLIDORT_FixIn%Cont%TS_NLAYERS, ilev, v,dir) =    &
                        GC_aod_UJacobians(iw, 1:VLIDORT_FixIn%Cont%TS_NLAYERS, ilev, v,dir)    &
                        / taer_profile(1:VLIDORT_FixIn%Cont%TS_NLAYERS)
                    ENDWHERE
                  ENDDO
                ENDIF
              ENDIF
            ENDDO
            
          ENDIF
          
        ENDIF
        
        ! ----------------------------------------------------------------------------------------
        ! Species specific AOD Jacobians
        ! ----------------------------------------------------------------------------------------
        DO g=1,Diag34_InpOpt%njac
          
          ! Get the weight function index
          q = Diag34_InpOpt%wfidx(g)
          
          ! Store profile jacobians
          DO v = 1, VLIDORT_Out%Main%TS_N_GEOMETRIES
            
            ! Unnormed Jacobians
            GC_spcaod_Jacobians(iw, 1:VLIDORT_FixIn%Cont%TS_NLAYERS, 1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,g,dir) = &
              VLIDORT_LinOut%Prof%TS_PROFILEWF(q,1:VLIDORT_FixIn%Cont%TS_NLAYERS,                                        &
              1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,1,dir)
            IF( do_stokes_ad34 ) THEN
              GC_spcaod_QJacobians(iw, 1:VLIDORT_FixIn%Cont%TS_NLAYERS, 1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,g,dir) = &
                VLIDORT_LinOut%Prof%TS_PROFILEWF(q,1:VLIDORT_FixIn%Cont%TS_NLAYERS,                                         &
                1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,2,dir)
              GC_spcaod_UJacobians(iw, 1:VLIDORT_FixIn%Cont%TS_NLAYERS, 1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,g,dir) = &
                VLIDORT_LinOut%Prof%TS_PROFILEWF(q,1:VLIDORT_FixIn%Cont%TS_NLAYERS,                                         &
                1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,3,dir)
            ENDIF
            
            ! Check normalization
            IF ( .NOT. do_normalized_WFoutput ) THEN
              DO ilev = 1, VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS
              DO n=1,VLIDORT_FixIn%Cont%TS_NLAYERS
                IF( aer_profile(Diag34_InpOpt%jac_idx(g),n) > 0.0d0 ) THEN
                  GC_spcaod_Jacobians(iw, n, ilev, v, g, dir) = &
                  GC_spcaod_Jacobians(iw, n, ilev, v, g, dir) / &
                  aer_profile(Diag34_InpOpt%jac_idx(g),n)
                  IF( do_stokes_ad34 ) THEN
                    GC_spcaod_QJacobians(iw, n, ilev, v, g, dir) = &
                    GC_spcaod_QJacobians(iw, n, ilev, v, g, dir) / &
                    aer_profile(Diag34_InpOpt%jac_idx(g),n)
                    GC_spcaod_UJacobians(iw, n, ilev, v, g, dir) = &
                    GC_spcaod_UJacobians(iw, n, ilev, v, g, dir) / &
                    aer_profile(Diag34_InpOpt%jac_idx(g),n)
                  ENDIF
                ENDIF
              ENDDO
              ENDDO
           ENDIF
            
          ENDDO ! TS_N_GEOMETRIES
        ENDDO
        
      ENDIF
      
      ! ==========================================================================================
      ! DIAG 35 - ASSA Jacobians
      ! ==========================================================================================
      IF ( do_assa_Jacobians ) THEN
        
        ! ----------------------------------------------------------------------------------------
        ! Total ASSA Jacobian
        ! ----------------------------------------------------------------------------------------
        IF( do_total_assa_jac ) THEN
        
          ! Jacobian Index
          q = assawfidx
          DO v = 1, VLIDORT_Out%Main%TS_N_GEOMETRIES
            IF (do_aer_columnwf) THEN
              DO ilev = 1, VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS
                total_wf = 0.0d0; total_aersca = 0.0d0; total_aertau = 0.0d0
                DO il = 1, VLIDORT_FixIn%Cont%TS_NLAYERS
                  IF (aer_flags(il)) THEN
                    total_wf = total_wf + VLIDORT_LINOUT%PROF%TS_PROFILEWF(q,il,ilev,v,1,dir) 
                    total_aersca = total_aersca + aer_opdeps(iw, il) * aer_ssalbs(iw, il) 
                    total_aertau = total_aertau + aer_opdeps(iw, il) 
                  ENDIF
                ENDDO
                total_vaerssa = total_aersca / total_aertau
                GC_assa_Jacobians(iw, 1, ilev, v,dir) = total_wf ! Normalized output
                IF ( .NOT. do_normalized_WFoutput ) THEN         ! Non-normalized output
                  GC_assa_Jacobians(iw, 1, ilev, v, dir) = total_wf / total_vaerssa
                ENDIF
              ENDDO
            ELSE
              GC_assa_Jacobians(iw, 1:VLIDORT_FixIn%Cont%TS_NLAYERS, 1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS, v,dir) = &
                VLIDORT_LINOUT%PROF%TS_PROFILEWF(q,1:VLIDORT_FixIn%Cont%TS_NLAYERS,                                     &
                1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,1,dir)
              IF ( .NOT. do_normalized_WFoutput ) THEN
                DO ilev = 1, VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS
                  WHERE( aer_flags(1:VLIDORT_FixIn%Cont%TS_NLAYERS) )
                    GC_assa_Jacobians(iw, 1:VLIDORT_FixIn%Cont%TS_NLAYERS, ilev, v,dir) =  &
                      GC_assa_Jacobians(iw, 1:VLIDORT_FixIn%Cont%TS_NLAYERS, ilev, v, dir) &
                      / aer_ssalbs(iw, 1:VLIDORT_FixIn%Cont%TS_NLAYERS)
                  ENDWHERE
                ENDDO
              ENDIF
            ENDIF
          ENDDO
          
          IF( do_stokes_ad35 .AND. do_QU_Jacobians ) THEN
            
            DO v = 1, VLIDORT_Out%Main%TS_N_GEOMETRIES
              IF (do_aer_columnwf) THEN
                DO ilev = 1, VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS
                  total_Qwf = 0.0d0; total_Uwf = 0.0d0; total_aersca = 0.0d0; total_aertau = 0.0d0
                  DO il = 1, VLIDORT_FixIn%Cont%TS_NLAYERS
                    IF (aer_flags(il)) THEN
                      total_Qwf = total_Qwf + VLIDORT_LINOUT%PROF%TS_PROFILEWF(q,il,ilev,v,2,dir) 
                      total_Uwf = total_Uwf + VLIDORT_LINOUT%PROF%TS_PROFILEWF(q,il,ilev,v,3,dir) 
                      total_aersca = total_aersca + aer_opdeps(iw, il) * aer_ssalbs(iw, il) 
                      total_aertau = total_aertau + aer_opdeps(iw, il) 
                    ENDIF
                  ENDDO
                  total_vaerssa = total_aersca / total_aertau
                  GC_assa_QJacobians(iw, 1, ilev, v,dir) = total_Qwf     ! Normalized output
                  GC_assa_UJacobians(iw, 1, ilev, v,dir) = total_Uwf     ! Normalized output
                  IF ( .NOT. do_normalized_WFoutput ) THEN                  ! Non-normalized output
                    GC_assa_QJacobians(iw, 1, ilev, v,dir) = total_Qwf / total_vaerssa
                    GC_assa_UJacobians(iw, 1, ilev, v,dir) = total_Uwf / total_vaerssa
                  ENDIF
                ENDDO
              ELSE
                GC_assa_QJacobians(iw, 1:VLIDORT_FixIn%Cont%TS_NLAYERS, 1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS, v,dir) = &
                  VLIDORT_LINOUT%PROF%TS_PROFILEWF(q,1:VLIDORT_FixIn%Cont%TS_NLAYERS,                                      &
                  1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,2,dir)
                GC_assa_UJacobians(iw, 1:VLIDORT_FixIn%Cont%TS_NLAYERS, 1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS, v,dir) = &
                  VLIDORT_LINOUT%PROF%TS_PROFILEWF(q,1:VLIDORT_FixIn%Cont%TS_NLAYERS,                                      &
                  1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,3,dir)
                IF ( .NOT. do_normalized_WFoutput ) THEN
                  DO ilev = 1, VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS
                    WHERE( aer_flags(1:VLIDORT_FixIn%Cont%TS_NLAYERS) )
                      GC_assa_QJacobians(iw, 1:VLIDORT_FixIn%Cont%TS_NLAYERS, ilev, v,dir) = &
                        GC_assa_QJacobians(iw, 1:VLIDORT_FixIn%Cont%TS_NLAYERS, ilev, v,dir) &
                        / aer_ssalbs(iw, 1:VLIDORT_FixIn%Cont%TS_NLAYERS)
                      GC_assa_UJacobians(iw, 1:VLIDORT_FixIn%Cont%TS_NLAYERS, ilev, v,dir) = &
                        GC_assa_UJacobians(iw, 1:VLIDORT_FixIn%Cont%TS_NLAYERS, ilev, v,dir) &
                        / aer_ssalbs(iw, 1:VLIDORT_FixIn%Cont%TS_NLAYERS)
                    ENDWHERE
                  ENDDO
                ENDIF
              ENDIF
            ENDDO
            
          ENDIF
        
        ENDIF
        
        ! ----------------------------------------------------------------------------------------
        ! Species Specific ASSA Jacobians
        ! ----------------------------------------------------------------------------------------
        DO g=1,Diag35_InpOpt%njac
          
          ! Get the weight function index
          q = Diag35_InpOpt%wfidx(g)
          
          ! Store profile jacobians
          DO v = 1, VLIDORT_Out%Main%TS_N_GEOMETRIES
            
            ! Unnormed Jacobians
            GC_spcassa_Jacobians(iw, 1:VLIDORT_FixIn%Cont%TS_NLAYERS, 1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,g,dir) = &
              VLIDORT_LinOut%Prof%TS_PROFILEWF(q,1:VLIDORT_FixIn%Cont%TS_NLAYERS,                                         &
              1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,1,dir)
            IF( do_stokes_ad35 ) THEN
              GC_spcassa_QJacobians(iw, 1:VLIDORT_FixIn%Cont%TS_NLAYERS, 1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,g,dir) = &
                VLIDORT_LinOut%Prof%TS_PROFILEWF(q,1:VLIDORT_FixIn%Cont%TS_NLAYERS,                                          &
                1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,2,dir)
              GC_spcassa_UJacobians(iw, 1:VLIDORT_FixIn%Cont%TS_NLAYERS, 1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,g,dir) = &
                VLIDORT_LinOut%Prof%TS_PROFILEWF(q,1:VLIDORT_FixIn%Cont%TS_NLAYERS,                                          &
                1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,3,dir)
            ENDIF
            
            ! Check normalization
            IF ( .NOT. do_normalized_WFoutput ) THEN
              DO ilev = 1, VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS
              DO n=1,VLIDORT_FixIn%Cont%TS_NLAYERS
                IF( aer_profile(Diag35_InpOpt%jac_idx(g),n) > 0.0d0 ) THEN
                  GC_spcassa_Jacobians(iw, n, ilev, v, g, dir) = &
                  GC_spcassa_Jacobians(iw, n, ilev, v, g, dir) / &
                  aer_spc_ssalbs(n,Diag35_InpOpt%jac_idx(g))
                  IF( do_stokes_ad35 ) THEN
                    GC_spcassa_QJacobians(iw, n, ilev, v, g, dir) = &
                    GC_spcassa_QJacobians(iw, n, ilev, v, g, dir) / &
                    aer_spc_ssalbs(n,Diag35_InpOpt%jac_idx(g))
                    GC_spcassa_UJacobians(iw, n, ilev, v, g, dir) = &
                    GC_spcassa_UJacobians(iw, n, ilev, v, g, dir) / &
                    aer_spc_ssalbs(n,Diag35_InpOpt%jac_idx(g))
                  ENDIF
                ENDIF
              ENDDO
              ENDDO
            ENDIF
          ENDDO
        ENDDO
      ENDIF
      
      ! ==========================================================================================
      ! DIAG 36 - Cloud Fraction Jacobian
      ! ==========================================================================================
      
      IF (do_cfrac_jacobians) THEN
        
        
        IF (cfrac .GT. 0.0d0 .AND. cfrac .LT. 1.0d0 ) THEN
          GC_cfrac_jacobians(iw, 1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS, 1:VLIDORT_Out%Main%TS_N_GEOMETRIES,dir) =  &
            stokes_clrcld(1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS, 1:VLIDORT_Out%Main%TS_N_GEOMETRIES, 1, dir, 2)    &
           -stokes_clrcld(1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS, 1:VLIDORT_Out%Main%TS_N_GEOMETRIES, 1, dir, 1)
        ELSE
          GC_cfrac_Jacobians(iw, 1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS, 1:VLIDORT_Out%Main%TS_N_GEOMETRIES,dir) = 0.0d0
        ENDIF
        
        IF( do_stokes_ad36 .and. do_QU_Jacobians ) THEN
          
          IF (cfrac .GT. 0.0d0 .AND. cfrac .LT. 1.0d0 ) THEN
            GC_cfrac_QJacobians(iw, 1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS, 1:VLIDORT_Out%Main%TS_N_GEOMETRIES,dir) = &
              stokes_clrcld(1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS, 1:VLIDORT_Out%Main%TS_N_GEOMETRIES, 2, dir, 2)    &
             -stokes_clrcld(1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS, 1:VLIDORT_Out%Main%TS_N_GEOMETRIES, 2, dir, 1)
            GC_cfrac_UJacobians(iw, 1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS, 1:VLIDORT_Out%Main%TS_N_GEOMETRIES,dir) = &
              stokes_clrcld(1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS, 1:VLIDORT_Out%Main%TS_N_GEOMETRIES, 3, dir, 2)    &
             -stokes_clrcld(1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS, 1:VLIDORT_Out%Main%TS_N_GEOMETRIES, 3, dir, 1)
          ELSE
            GC_cfrac_QJacobians(iw, 1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS, 1:VLIDORT_Out%Main%TS_N_GEOMETRIES,dir) = 0.0d0
            GC_cfrac_UJacobians(iw, 1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS, 1:VLIDORT_Out%Main%TS_N_GEOMETRIES,dir) = 0.0d0
          ENDIF
          
        ENDIF
        
      ENDIF
    
      
      ! ==========================================================================================
      ! DIAG 37 - lambertian Cloud Pressure Jacobian
      ! ==========================================================================================
      
      ! ==========================================================================================
      ! DIAG 38 - Total Cloud Optical Depth
      ! ==========================================================================================
      
      IF ( do_cod_Jacobians ) THEN
      
        ! Jacobian Index
        q = codwfidx
        DO v = 1, VLIDORT_Out%Main%TS_N_GEOMETRIES
          IF (do_cld_columnwf) THEN
            DO ilev = 1, VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS
              total_wf = 0.0d0
              DO il = 1, VLIDORT_FixIn%Cont%TS_NLAYERS
                IF (cld_flags(il)) THEN
                  total_wf = total_wf + VLIDORT_LINOUT%PROF%TS_PROFILEWF(q,il,ilev,v,1,dir)
                ENDIF
              ENDDO
              GC_cod_Jacobians(iw, 1, ilev, v,dir) = total_wf        !  Normalized output
              IF ( .NOT. do_normalized_WFoutput ) THEN               !  Non-normalized output
                GC_cod_Jacobians(iw, 1, ilev, v,dir) = total_wf / tcldtau0
              ENDIF
            ENDDO
          ELSE
            GC_cod_Jacobians(iw, 1:VLIDORT_FixIn%Cont%TS_NLAYERS, 1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS, v,dir) = &
              VLIDORT_LINOUT%PROF%TS_PROFILEWF(q,1:VLIDORT_FixIn%Cont%TS_NLAYERS,                                   &
              1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,1,dir)
            IF ( .NOT. do_normalized_WFoutput ) THEN
              DO ilev = 1, VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS
                WHERE( cld_flags(1:VLIDORT_FixIn%Cont%TS_NLAYERS) )
                  GC_cod_Jacobians(iw, 1:VLIDORT_FixIn%Cont%TS_NLAYERS, ilev, v,dir) = &
                    GC_cod_Jacobians(iw, 1:VLIDORT_FixIn%Cont%TS_NLAYERS, ilev, v,dir) &
                    / tcld_profile(1:VLIDORT_FixIn%Cont%TS_NLAYERS)
                ENDWHERE
              ENDDO
            ENDIF
          ENDIF
        ENDDO
        
        IF( do_stokes_ad38 .AND. do_QU_Jacobians ) THEN
          
          DO v = 1, VLIDORT_Out%Main%TS_N_GEOMETRIES
            IF (do_cld_columnwf) THEN
              DO ilev = 1, VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS
                total_Qwf = 0.0d0; total_Uwf = 0.0d0
                DO il = 1, VLIDORT_FixIn%Cont%TS_NLAYERS
                  IF (cld_flags(il)) THEN
                    total_Qwf = total_Qwf + VLIDORT_LINOUT%PROF%TS_PROFILEWF(q,il,ilev,v,2,dir)
                    total_Uwf = total_Uwf + VLIDORT_LINOUT%PROF%TS_PROFILEWF(q,il,ilev,v,3,dir)
                  ENDIF
                ENDDO
                GC_cod_QJacobians(iw, 1, ilev, v,dir) = total_Qwf      !  Normalized output
                GC_cod_UJacobians(iw, 1, ilev, v,dir) = total_Uwf
                IF ( .NOT. do_normalized_WFoutput ) THEN                  !  Non-normalized output
                  GC_cod_QJacobians(iw, 1, ilev, v,dir) = total_Qwf / tcldtau0 
                  GC_cod_UJacobians(iw, 1, ilev, v,dir) = total_Uwf / tcldtau0 
                END IF
              ENDDO
             ELSE
              GC_cod_QJacobians(iw, 1:VLIDORT_FixIn%Cont%TS_NLAYERS, 1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS, v,dir) = &
                VLIDORT_LINOUT%PROF%TS_PROFILEWF(q,1:VLIDORT_FixIn%Cont%TS_NLAYERS,                                     &
                1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,2,dir)
              GC_cod_UJacobians(iw, 1:VLIDORT_FixIn%Cont%TS_NLAYERS, 1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS, v,dir) = &
                VLIDORT_LINOUT%PROF%TS_PROFILEWF(q,1:VLIDORT_FixIn%Cont%TS_NLAYERS,                                     &
                1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,3,dir)
              IF ( .NOT. do_normalized_WFoutput ) THEN
                DO ilev = 1, VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS
                  WHERE( cld_flags(1:VLIDORT_FixIn%Cont%TS_NLAYERS) )
                    GC_cod_QJacobians(iw, 1:VLIDORT_FixIn%Cont%TS_NLAYERS, ilev, v,dir) = &
                      GC_cod_QJacobians(iw, 1:VLIDORT_FixIn%Cont%TS_NLAYERS, ilev, v,dir) &
                      / tcld_profile(1:VLIDORT_FixIn%Cont%TS_NLAYERS)
                    GC_cod_UJacobians(iw, 1:VLIDORT_FixIn%Cont%TS_NLAYERS, ilev, v,dir) = &
                      GC_cod_UJacobians(iw, 1:VLIDORT_FixIn%Cont%TS_NLAYERS, ilev, v,dir) &
                      / tcld_profile(1:VLIDORT_FixIn%Cont%TS_NLAYERS)
                  ENDWHERE
                ENDDO
              ENDIF
            ENDIF
          ENDDO
          
        ENDIF
        
      ENDIF
      
      ! ==========================================================================================
      ! DIAG 39 - Total Cloud SSA
      ! ==========================================================================================
      
      IF ( do_cssa_Jacobians ) THEN
        
        ! CSSA Jacobian index
        q = cssawfidx
        
        DO v = 1, VLIDORT_Out%Main%TS_N_GEOMETRIES
          IF (do_cld_columnwf) THEN
            DO ilev = 1, VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS
              total_wf = 0.0d0; total_cldsca = 0.0d0; total_cldtau = 0.0d0
              DO il = 1, VLIDORT_FixIn%Cont%TS_NLAYERS
                IF (cld_flags(il)) THEN
                  total_wf = total_wf + VLIDORT_LINOUT%PROF%TS_PROFILEWF(q,il,ilev,v,1,dir) 
                  total_cldsca = total_cldsca + cld_opdeps(iw, il)  * cld_ssalbs(iw, il) 
                  total_cldtau = total_cldtau + cld_opdeps(iw, il) 
                ENDIF
              ENDDO
              total_vcldssa = total_cldsca / total_cldtau
              GC_cssa_Jacobians(iw, 1, ilev, v,dir) = total_wf           ! Normalized output
              IF ( .NOT. do_normalized_WFoutput ) THEN                   ! Non-normalized output
                GC_cssa_Jacobians(iw, 1, ilev, v,dir) = total_wf / total_vcldssa
              ENDIF
            ENDDO
          ELSE
            GC_cssa_Jacobians(iw, 1:VLIDORT_FixIn%Cont%TS_NLAYERS, 1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS, v,dir) = &
              VLIDORT_LINOUT%PROF%TS_PROFILEWF(q,1:VLIDORT_FixIn%Cont%TS_NLAYERS,                                  &
              1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,1,dir)
            IF ( .NOT. do_normalized_WFoutput ) THEN     
              DO ilev = 1, VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS
                WHERE( cld_flags(1:VLIDORT_FixIn%Cont%TS_NLAYERS) )
                  GC_cssa_Jacobians(iw, 1:VLIDORT_FixIn%Cont%TS_NLAYERS, ilev, v,dir) =    &
                    GC_cssa_Jacobians(iw, 1:VLIDORT_FixIn%Cont%TS_NLAYERS, ilev, v,dir) &
                    / cld_ssalbs(iw, 1:VLIDORT_FixIn%Cont%TS_NLAYERS)
                ENDWHERE
              ENDDO
            ENDIF
          ENDIF
        ENDDO
        
        IF( do_stokes_ad39 .AND. do_QU_Jacobians  ) THEN
          
          DO v = 1, VLIDORT_Out%Main%TS_N_GEOMETRIES
            IF (do_cld_columnwf) THEN
              DO ilev = 1, VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS
                total_Qwf = 0.0d0; total_Uwf = 0.0d0; total_cldsca = 0.0d0; total_cldtau = 0.0d0
                DO il = 1, VLIDORT_FixIn%Cont%TS_NLAYERS
                  IF (cld_flags(il)) THEN
                    total_Qwf = total_Qwf + VLIDORT_LINOUT%PROF%TS_PROFILEWF(q,il,ilev,v,2,dir) 
                    total_Uwf = total_Uwf + VLIDORT_LINOUT%PROF%TS_PROFILEWF(q,il,ilev,v,3,dir) 
                    total_cldsca = total_cldsca + cld_opdeps(iw, il) * cld_ssalbs(iw, il) 
                    total_cldtau = total_cldtau + cld_opdeps(iw, il) 
                  ENDIF
                ENDDO
                total_vcldssa = total_cldsca / total_cldtau
                GC_cssa_QJacobians(iw, 1, ilev, v,dir) = total_Qwf     ! Normalized output
                GC_cssa_UJacobians(iw, 1, ilev, v,dir) = total_Uwf     ! Normalized output
                IF ( .NOT. do_normalized_WFoutput ) THEN               ! Non-normalized output
                  GC_cssa_QJacobians(iw, 1, ilev, v,dir) = total_Qwf / total_vcldssa
                  GC_cssa_UJacobians(iw, 1, ilev, v,dir) = total_Uwf / total_vcldssa
                ENDIF
              ENDDO
            ELSE
              GC_cssa_QJacobians(iw, 1:VLIDORT_FixIn%Cont%TS_NLAYERS, 1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS, v,dir) = &
                VLIDORT_LINOUT%PROF%TS_PROFILEWF(q,1:VLIDORT_FixIn%Cont%TS_NLAYERS,                                   &
                1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,2,dir)
              GC_cssa_UJacobians(iw, 1:VLIDORT_FixIn%Cont%TS_NLAYERS, 1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS, v,dir) = &
                VLIDORT_LINOUT%PROF%TS_PROFILEWF(q,1:VLIDORT_FixIn%Cont%TS_NLAYERS,                                   &
                1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,3,dir)
              IF( .NOT. do_normalized_WFoutput ) THEN
                DO ilev = 1, VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS
                  WHERE( cld_flags(1:VLIDORT_FixIn%Cont%TS_NLAYERS) )
                    GC_cssa_QJacobians(iw, 1:VLIDORT_FixIn%Cont%TS_NLAYERS, ilev, v,dir) =    &
                      GC_cssa_QJacobians(iw, 1:VLIDORT_FixIn%Cont%TS_NLAYERS, ilev, v,dir) &
                      / cld_ssalbs(iw, 1:VLIDORT_FixIn%Cont%TS_NLAYERS)
                    GC_cssa_UJacobians(iw, 1:VLIDORT_FixIn%Cont%TS_NLAYERS, ilev, v,dir) =    &
                      GC_cssa_UJacobians(iw, 1:VLIDORT_FixIn%Cont%TS_NLAYERS, ilev, v,dir) &
                      / cld_ssalbs(iw, 1:VLIDORT_FixIn%Cont%TS_NLAYERS)
                  ENDWHERE
                ENDDO
              ENDIF
            ENDIF
          ENDDO
          
        ENDIF
        
      ENDIF
      
    END SUBROUTINE archive_cldaer_diag
    
    ! ======================================================
    ! (34)  AOD jacobians
    ! ======================================================
    SUBROUTINE diag_34( NCID, ACTION, DO_XY )
      
      USE GC_variables_module, ONLY : Diag34_InpOpt,          &
                                      do_stokes_ad34, didx,   &
                                      GC_aod_Jacobians,       &
                                      GC_aod_QJacobians,      &
                                      GC_aod_UJacobians,      &
                                      GC_spcaod_Jacobians,    &
                                      GC_spcaod_QJacobians,   &
                                      GC_spcaod_UJacobians,   &
                                      do_stokes_ad34,         &
                                      do_total_aod_jac,       &
                                      do_aer_columnwf,        &
                                      aer_d_profile_dtau,     &
                                      aer_d_profile_dpkh,     &
                                      aer_d_profile_dhfw,     &
                                      aer_d_profile_drel,     &
                                      aer_relaxation,         &
                                      aer_profile,            &
                                      aer_z_peakheight,       &
                                      aer_half_width,         &
                                      aer_proftype,           &
                                      aer_tau0s,              &
                                      do_normalized_WFoutput
      
      ! ---------------------------
      ! Subroutine arguments
      ! ---------------------------
      INTEGER, INTENT(IN) :: NCID
      INTEGER, INTENT(IN) :: ACTION
      LOGICAL, INTENT(IN) :: DO_XY
      
      ! ---------------
      ! Local variables
      ! ---------------
      LOGICAL                                 :: error, dummy_err
      INTEGER, DIMENSION(1)                   :: size_1d, tmp_1d
      CHARACTER(LEN=max_ch_len), DIMENSION(3) :: dim_1d
      INTEGER, DIMENSION(3)                   :: size_3d, tmp_3d
      CHARACTER(LEN=max_ch_len), DIMENSION(3) :: dim_3d
      INTEGER, DIMENSION(4)                   :: size_4d, tmp_4d
      CHARACTER(LEN=max_ch_len), DIMENSION(4) :: dim_4d
      CHARACTER(LEN=max_ch_len)               :: fld_name
      INTEGER                                 :: n, g, s, l
      REAL(KIND=8), ALLOCATABLE               :: par_jac(:,:,:)
      
      
      ! =====================================================
      ! diag_34 starts here
      ! =====================================================
      
      ! Intialize error
      error = .FALSE.
      
      ! Dimensions of output for writing profile parameters
      dim_1d(1) = 'one'
      
      ! Get dimension
      CALL match_names_in_dimlist( dim_1d, 1, tmp_1d, size_1d, dummy_err )
      
      ! Dimensions of output for non-altitude dependent variables
      dim_3d(1) = 'nw' ; dim_3d(2) = 'ngeom' ; dim_3d(3) = 'noutputlevel'
      
      ! Get dimension
      CALL match_names_in_dimlist( dim_3d, 3, tmp_3d, size_3d, dummy_err )
      
      ! The dimension of the output for level variables
      dim_4d(1) = 'nw' ; dim_4d(2) = 'nlayer' ; dim_4d(3) = 'ngeom' ; dim_4d(4) = 'noutputlevel'
      
      ! Get dimension
      CALL match_names_in_dimlist( dim_4d, 4, tmp_4d, size_4d, dummy_err )
      
      ! Total AOD
      IF( do_total_aod_jac ) THEN
        
        IF( do_aer_columnwf ) THEN
          
          CALL nc_fld_3d( dim_3d, size_3d(1), size_3d(2), size_3d(3),                     &
                          GC_aod_Jacobians(1:size_3d(1),1,1:size_3d(2),1:size_3d(3),didx),&
                          'aodcolwf_jac',  NCID, ACTION, DO_XY                            )
          
          IF( do_stokes_ad34 ) THEN
           
            CALL nc_fld_3d( dim_3d, size_3d(1), size_3d(2), size_3d(3),                      &
                            GC_aod_QJacobians(1:size_3d(1),1,1:size_3d(2),1:size_3d(3),didx),&
                            'aodcolwf_qjac',  NCID, ACTION, DO_XY                            )
            
            CALL nc_fld_3d( dim_3d, size_3d(1), size_3d(2), size_3d(3),                      &
                            GC_aod_UJacobians(1:size_3d(1),1,1:size_3d(2),1:size_3d(3),didx),&
                            'aodcolwf_ujac',  NCID, ACTION, DO_XY                            )
            
          ENDIF
          
        ELSE
          
          CALL nc_fld_4d( dim_4d, size_4d(1), size_4d(2), size_4d(3), size_4d(4),                    &
                          GC_aod_Jacobians(1:size_4d(1),1:size_4d(2),1:size_4d(3),1:size_4d(4),didx),&
                          'aod_jac',  NCID, ACTION, DO_XY                                            )
          
          IF( do_stokes_ad34 ) THEN
            
            CALL nc_fld_4d( dim_4d, size_4d(1), size_4d(2), size_4d(3), size_4d(4),                     &
                            GC_aod_QJacobians(1:size_4d(1),1:size_4d(2),1:size_4d(3),1:size_4d(4),didx),&
                            'aod_qjac',  NCID, ACTION, DO_XY                                            )
            
            CALL nc_fld_4d( dim_4d, size_4d(1), size_4d(2), size_4d(3), size_4d(4),                     &
                            GC_aod_UJacobians(1:size_4d(1),1:size_4d(2),1:size_4d(3),1:size_4d(4),didx),&
                            'aod_ujac',  NCID, ACTION, DO_XY                                            )
            
          ENDIF
          
        ENDIF
      ENDIF
      
      ! AOD for individual species
      DO n=1,Diag34_InpOpt%nspc
        
        ! Jacobian index
        g = Diag34_InpOpt%spc_idx(n)
        
        ! Name of output field
        fld_name = TRIM(ADJUSTL( Diag34_InpOpt%spc(n) )) // '_aod_jac'
        
        CALL nc_fld_4d( dim_4d, size_4d(1), size_4d(2), size_4d(3), size_4d(4),                    &
                   GC_spcaod_Jacobians(1:size_4d(1),1:size_4d(2),1:size_4d(3),1:size_4d(4),g,didx),&
                   TRIM(ADJUSTL(fld_name)),  NCID, ACTION, DO_XY                                   )
        
        IF( do_stokes_ad34 ) THEN
          
          ! Name of output field
          fld_name = TRIM(ADJUSTL( Diag34_InpOpt%spc(n) )) // '_aod_qjac'
          
          CALL nc_fld_4d( dim_4d, size_4d(1), size_4d(2), size_4d(3), size_4d(4),                    &
                    GC_spcaod_QJacobians(1:size_4d(1),1:size_4d(2),1:size_4d(3),1:size_4d(4),g,didx),&
                    TRIM(ADJUSTL(fld_name)),  NCID, ACTION, DO_XY                                    )
                    
          ! Name of output field
          fld_name = TRIM(ADJUSTL( Diag34_InpOpt%spc(n) )) // '_aod_ujac'
          
          CALL nc_fld_4d( dim_4d, size_4d(1), size_4d(2), size_4d(3), size_4d(4),                    &
                    GC_spcaod_UJacobians(1:size_4d(1),1:size_4d(2),1:size_4d(3),1:size_4d(4),g,didx),&
                    TRIM(ADJUSTL(fld_name)),  NCID, ACTION, DO_XY                                    )
          
        ENDIF
        
      ENDDO
      
      ! Allocate working array 
      ALLOCATE( par_jac(size_3d(1), size_3d(2), size_3d(3)) )
      
      ! Parameters -> Apply Chain rule to AOD profile jacobians
      DO n=1,Diag34_InpOpt%npar
        
        ! Jacobian index
        g = Diag34_InpOpt%spc_idx(n)
        
        ! Species index
        s = Diag34_InpOpt%jac_idx(g)
        
        ! Column AOD (all cases)
        ! -------------------------------
        
        ! Field Name
        fld_name = TRIM(ADJUSTL( Diag34_InpOpt%par(n) )) // '_aod_tau_jac'
          
        ! Zero Output jacobian array
        par_jac(:,:,:) = 0.0d0
          
        IF( ACTION == 2 ) THEN
          
          ! Sum layers
          IF( do_normalized_WFoutput ) THEN
            DO l=1,size_4d(2)
              IF( aer_profile(s,l) > 0.0d0 ) THEN
                par_jac(:,:,:) = par_jac(:,:,:) + &
                  GC_spcaod_Jacobians(1:size_4d(1),l,1:size_4d(3),1:size_4d(4),g,didx) &
                  *aer_d_profile_dtau(s,l)/aer_profile(s,l)*aer_tau0s(s)
              ENDIF
            ENDDO
          ELSE
            DO l=1,size_4d(2)
              par_jac(:,:,:) = par_jac(:,:,:) + &
                GC_spcaod_Jacobians(1:size_4d(1),l,1:size_4d(3),1:size_4d(4),g,didx)*aer_d_profile_dtau(s,l)
            ENDDO
          ENDIF
            
        ENDIF
          
        ! Write
        CALL nc_fld_3d( dim_3d, size_3d(1), size_3d(2), size_3d(3),     &
                        par_jac(1:size_3d(1),1:size_3d(2),1:size_3d(3)),&
                        TRIM(ADJUSTL(fld_name)),  NCID, ACTION, DO_XY   )
        
        ! Write tau
        fld_name = TRIM(ADJUSTL( Diag34_InpOpt%par(n) )) // '_aod_tau'
        CALL nc_fld_1d( dim_1d, size_1d(1), aer_tau0s(s),               &
                        TRIM(ADJUSTL(fld_name)),  NCID, ACTION, DO_XY   )
        
        
        
        ! Individual cases
        SELECTCASE( TRIM(ADJUSTL(aer_proftype(s))) )
        
        ! Column AOD for all cases
        
        CASE( 'GDF' )
          
          ! ----------------------------------
          ! Peak Height
          ! ----------------------------------
          
          ! Field Name
          fld_name = TRIM(ADJUSTL( Diag34_InpOpt%par(n) )) // '_aod_pkh_jac'
          
          ! Zero Output jacobian array
          par_jac(:,:,:) = 0.0d0
          
          IF( ACTION == 2 ) THEN
            
            ! Sum layers
            IF( do_normalized_WFoutput ) THEN
              DO l=1,size_4d(2)
                IF( aer_profile(s,l) > 0.0d0 ) THEN
                  par_jac(:,:,:) = par_jac(:,:,:) + &
                    GC_spcaod_Jacobians(1:size_4d(1),l,1:size_4d(3),1:size_4d(4),g,didx) &
                    *aer_d_profile_dpkh(s,l)/aer_profile(s,l)*aer_z_peakheight(s)
                ENDIF
              ENDDO
            ELSE
              DO l=1,size_4d(2)
                par_jac(:,:,:) = par_jac(:,:,:) + &
                  GC_spcaod_Jacobians(1:size_4d(1),l,1:size_4d(3),1:size_4d(4),g,didx)*aer_d_profile_dpkh(s,l)
              ENDDO
            ENDIF
            
          ENDIF
          
          ! Write
          CALL nc_fld_3d( dim_3d, size_3d(1), size_3d(2), size_3d(3),     &
                          par_jac(1:size_3d(1),1:size_3d(2),1:size_3d(3)),&
                          TRIM(ADJUSTL(fld_name)),  NCID, ACTION, DO_XY   )
          
          ! Write peak height
          fld_name = TRIM(ADJUSTL( Diag34_InpOpt%par(n) )) // '_aod_pkh'
          CALL nc_fld_1d( dim_1d, size_1d(1), aer_z_peakheight(s),        &
                          TRIM(ADJUSTL(fld_name)),  NCID, ACTION, DO_XY   )
          
          IF( do_stokes_ad34 ) THEN
            
            ! Field Name
            fld_name = TRIM(ADJUSTL( Diag34_InpOpt%par(n) )) // '_aod_pkh_qjac'
            
            ! Zero Output jacobian array
            par_jac(:,:,:) = 0.0d0
            
            IF( ACTION == 2 ) THEN
              
              ! Sum layers
              IF( do_normalized_WFoutput ) THEN
                DO l=1,size_4d(2)
                  IF( aer_profile(s,l) > 0.0d0 ) THEN
                    par_jac(:,:,:) = par_jac(:,:,:) + &
                      GC_spcaod_QJacobians(1:size_4d(1),l,1:size_4d(3),1:size_4d(4),g,didx) &
                      *aer_d_profile_dpkh(s,l)/aer_profile(s,l)*aer_z_peakheight(s)
                  ENDIF
                ENDDO
              ELSE
                DO l=1,size_4d(2)
                  par_jac(:,:,:) = par_jac(:,:,:) + &
                    GC_spcaod_QJacobians(1:size_4d(1),l,1:size_4d(3),1:size_4d(4),g,didx)*aer_d_profile_dpkh(s,l)
                ENDDO
              ENDIF
              
            ENDIF
            
            ! Write
            CALL nc_fld_3d( dim_3d, size_3d(1), size_3d(2), size_3d(3),     &
                            par_jac(1:size_3d(1),1:size_3d(2),1:size_3d(3)),&
                            TRIM(ADJUSTL(fld_name)),  NCID, ACTION, DO_XY   )
            
            ! Field Name
            fld_name = TRIM(ADJUSTL( Diag34_InpOpt%par(n) )) // '_aod_pkh_ujac'
            
            ! Zero Output jacobian array
            par_jac(:,:,:) = 0.0d0
            
            IF( ACTION == 2 ) THEN
              
              ! Sum layers
              IF( do_normalized_WFoutput ) THEN
                DO l=1,size_4d(2)
                  IF( aer_profile(s,l) > 0.0d0 ) THEN
                    par_jac(:,:,:) = par_jac(:,:,:) + &
                      GC_spcaod_UJacobians(1:size_4d(1),l,1:size_4d(3),1:size_4d(4),g,didx) &
                      *aer_d_profile_dpkh(s,l)/aer_profile(s,l)*aer_z_peakheight(s)
                  ENDIF
                ENDDO
              ELSE
                DO l=1,size_4d(2)
                  par_jac(:,:,:) = par_jac(:,:,:) + &
                    GC_spcaod_UJacobians(1:size_4d(1),l,1:size_4d(3),1:size_4d(4),g,didx)*aer_d_profile_dpkh(s,l)
                ENDDO
              ENDIF
              
            ENDIF
            
            ! Write
            CALL nc_fld_3d( dim_3d, size_3d(1), size_3d(2), size_3d(3),     &
                            par_jac(1:size_3d(1),1:size_3d(2),1:size_3d(3)),&
                            TRIM(ADJUSTL(fld_name)),  NCID, ACTION, DO_XY   )
            
          ENDIF
          
          ! ----------------------------------
          ! Aerosol half width
          ! ----------------------------------
          
          ! Field Name
          fld_name = TRIM(ADJUSTL( Diag34_InpOpt%par(n) )) // '_aod_hfw_jac'
          
          ! Zero Output jacobian array
          par_jac(:,:,:) = 0.0d0
          
          IF( ACTION == 2 ) THEN
            
            ! Sum layers
            IF( do_normalized_WFoutput ) THEN
              DO l=1,size_4d(2)
                IF( aer_profile(s,l) > 0.0d0 ) THEN
                  par_jac(:,:,:) = par_jac(:,:,:) + &
                    GC_spcaod_Jacobians(1:size_4d(1),l,1:size_4d(3),1:size_4d(4),g,didx) &
                    *aer_d_profile_dhfw(s,l)/aer_profile(s,l)*aer_half_width(s)
                ENDIF
              ENDDO
            ELSE
              DO l=1,size_4d(2)
                par_jac(:,:,:) = par_jac(:,:,:) + &
                  GC_spcaod_Jacobians(1:size_4d(1),l,1:size_4d(3),1:size_4d(4),g,didx)*aer_d_profile_dhfw(s,l)
              ENDDO
            ENDIF
            
          ENDIF
          
          ! Write
          CALL nc_fld_3d( dim_3d, size_3d(1), size_3d(2), size_3d(3),     &
                          par_jac(1:size_3d(1),1:size_3d(2),1:size_3d(3)),&
                          TRIM(ADJUSTL(fld_name)),  NCID, ACTION, DO_XY   )
          
          ! Write half width
          fld_name = TRIM(ADJUSTL( Diag34_InpOpt%par(n) )) // '_aod_hfw'
          CALL nc_fld_1d( dim_1d, size_1d(1), aer_half_width(s),          &
                          TRIM(ADJUSTL(fld_name)),  NCID, ACTION, DO_XY   )
          
          IF( do_stokes_ad34 ) THEN
            
            ! Field Name
            fld_name = TRIM(ADJUSTL( Diag34_InpOpt%par(n) )) // '_aod_hfw_qjac'
            
            ! Zero Output jacobian array
            par_jac(:,:,:) = 0.0d0
            
            IF( ACTION == 2 ) THEN
              
              ! Sum layers
              IF( do_normalized_WFoutput ) THEN
                DO l=1,size_4d(2)
                  IF( aer_profile(s,l) > 0.0d0 ) THEN
                    par_jac(:,:,:) = par_jac(:,:,:) + &
                      GC_spcaod_QJacobians(1:size_4d(1),l,1:size_4d(3),1:size_4d(4),g,didx) &
                      *aer_d_profile_dhfw(s,l)/aer_profile(s,l)*aer_half_width(s)
                  ENDIF
                ENDDO
              ELSE
                DO l=1,size_4d(2)
                  par_jac(:,:,:) = par_jac(:,:,:) + &
                    GC_spcaod_QJacobians(1:size_4d(1),l,1:size_4d(3),1:size_4d(4),g,didx)*aer_d_profile_dhfw(s,l)
                ENDDO
              ENDIF
              
            ENDIF
            
            ! Write
            CALL nc_fld_3d( dim_3d, size_3d(1), size_3d(2), size_3d(3),     &
                            par_jac(1:size_3d(1),1:size_3d(2),1:size_3d(3)),&
                            TRIM(ADJUSTL(fld_name)),  NCID, ACTION, DO_XY   )
            
            ! Field Name
            fld_name = TRIM(ADJUSTL( Diag34_InpOpt%par(n) )) // '_aod_hfw_ujac'
            
            ! Zero Output jacobian array
            par_jac(:,:,:) = 0.0d0
            
            IF( ACTION == 2 ) THEN
              
              ! Sum layers
              IF( do_normalized_WFoutput ) THEN
                DO l=1,size_4d(2)
                  IF( aer_profile(s,l) > 0.0d0 ) THEN
                    par_jac(:,:,:) = par_jac(:,:,:) + &
                      GC_spcaod_UJacobians(1:size_4d(1),l,1:size_4d(3),1:size_4d(4),g,didx) &
                      *aer_d_profile_dhfw(s,l)/aer_profile(s,l)*aer_half_width(s)
                  ENDIF
                ENDDO
              ELSE
                DO l=1,size_4d(2)
                  par_jac(:,:,:) = par_jac(:,:,:) + &
                    GC_spcaod_UJacobians(1:size_4d(1),l,1:size_4d(3),1:size_4d(4),g,didx)*aer_d_profile_dhfw(s,l)
                ENDDO
              ENDIF
              
            ENDIF
            
            ! Write
            CALL nc_fld_3d( dim_3d, size_3d(1), size_3d(2), size_3d(3),     &
                            par_jac(1:size_3d(1),1:size_3d(2),1:size_3d(3)),&
                            TRIM(ADJUSTL(fld_name)),  NCID, ACTION, DO_XY   )
            
          ENDIF
          
        CASE( 'EXP' )
          
          ! ----------------------------------
          ! Relaxation Length scale
          ! ----------------------------------
          
          ! Field Name
          fld_name = TRIM(ADJUSTL( Diag34_InpOpt%par(n) )) // '_aod_rel_jac'
          
          ! Zero Output jacobian array
          par_jac(:,:,:) = 0.0d0
          
          IF( ACTION == 2 ) THEN
            
            ! Sum layers
            IF( do_normalized_WFoutput ) THEN
              DO l=1,size_4d(2)
                IF( aer_profile(s,l) > 0.0d0 ) THEN
                  par_jac(:,:,:) = par_jac(:,:,:) + &
                    GC_spcaod_Jacobians(1:size_4d(1),l,1:size_4d(3),1:size_4d(4),g,didx) &
                    *aer_d_profile_drel(s,l)/aer_profile(s,l)*aer_relaxation(s)
                ENDIF
              ENDDO
            ELSE
              DO l=1,size_4d(2)
                par_jac(:,:,:) = par_jac(:,:,:) + &
                  GC_spcaod_Jacobians(1:size_4d(1),l,1:size_4d(3),1:size_4d(4),g,didx)*aer_d_profile_drel(s,l)
              ENDDO
            ENDIF
            
          ENDIF
          
          ! Write
          CALL nc_fld_3d( dim_3d, size_3d(1), size_3d(2), size_3d(3),     &
                          par_jac(1:size_3d(1),1:size_3d(2),1:size_3d(3)),&
                          TRIM(ADJUSTL(fld_name)),  NCID, ACTION, DO_XY   )
          
          ! Write relaxation
          fld_name = TRIM(ADJUSTL( Diag34_InpOpt%par(n) )) // '_aod_rel'
          CALL nc_fld_1d( dim_1d, size_1d(1), aer_relaxation(s),          &
                          TRIM(ADJUSTL(fld_name)),  NCID, ACTION, DO_XY   )
          
          IF( do_stokes_ad34 ) THEN
            
            ! Field Name
            fld_name = TRIM(ADJUSTL( Diag34_InpOpt%par(n) )) // '_aod_rel_qjac'
            
            ! Zero Output jacobian array
            par_jac(:,:,:) = 0.0d0
            
            IF( ACTION == 2 ) THEN
              
              ! Sum layers
              IF( do_normalized_WFoutput ) THEN
                DO l=1,size_4d(2)
                  IF( aer_profile(s,l) > 0.0d0 ) THEN
                    par_jac(:,:,:) = par_jac(:,:,:) + &
                      GC_spcaod_QJacobians(1:size_4d(1),l,1:size_4d(3),1:size_4d(4),g,didx) &
                      *aer_d_profile_drel(s,l)/aer_profile(s,l)*aer_relaxation(s)
                  ENDIF
                ENDDO
              ELSE
                DO l=1,size_4d(2)
                  par_jac(:,:,:) = par_jac(:,:,:) + &
                    GC_spcaod_QJacobians(1:size_4d(1),l,1:size_4d(3),1:size_4d(4),g,didx)*aer_d_profile_drel(s,l)
                ENDDO
              ENDIF
              
            ENDIF
            
            ! Write
            CALL nc_fld_3d( dim_3d, size_3d(1), size_3d(2), size_3d(3),     &
                            par_jac(1:size_3d(1),1:size_3d(2),1:size_3d(3)),&
                            TRIM(ADJUSTL(fld_name)),  NCID, ACTION, DO_XY   )
            
            ! Field Name
            fld_name = TRIM(ADJUSTL( Diag34_InpOpt%par(n) )) // '_aod_rel_ujac'
            
            ! Zero Output jacobian array
            par_jac(:,:,:) = 0.0d0
            
            IF( ACTION == 2 ) THEN
              
              ! Sum layers
              IF( do_normalized_WFoutput ) THEN
                DO l=1,size_4d(2)
                  IF( aer_profile(s,l) > 0.0d0 ) THEN
                    par_jac(:,:,:) = par_jac(:,:,:) + &
                      GC_spcaod_UJacobians(1:size_4d(1),l,1:size_4d(3),1:size_4d(4),g,didx) &
                      *aer_d_profile_drel(s,l)/aer_profile(s,l)*aer_relaxation(s)
                  ENDIF
                ENDDO
              ELSE
                DO l=1,size_4d(2)
                  par_jac(:,:,:) = par_jac(:,:,:) + &
                    GC_spcaod_UJacobians(1:size_4d(1),l,1:size_4d(3),1:size_4d(4),g,didx)*aer_d_profile_drel(s,l)
                ENDDO
              ENDIF
              
            ENDIF
            
            ! Write
            CALL nc_fld_3d( dim_3d, size_3d(1), size_3d(2), size_3d(3),     &
                            par_jac(1:size_3d(1),1:size_3d(2),1:size_3d(3)),&
                            TRIM(ADJUSTL(fld_name)),  NCID, ACTION, DO_XY   )
            
          ENDIF
          
        CASE( 'BOX' )
          
          ! No pars
          
        CASE DEFAULT
          
          print*,'DIAG34: No aertype: ' // TRIM(ADJUSTL(aer_proftype(s)))
          
        END SELECT
        
      ENDDO
      
      ! Deallocate par_jac
      DEALLOCATE( par_jac )
        
      
      
      
    END SUBROUTINE diag_34

    ! ======================================================
    ! (35)  ASSA jacobians
    ! ======================================================
    SUBROUTINE diag_35( NCID, ACTION, DO_XY )
      
      USE GC_variables_module, ONLY : Diag35_InpOpt,          &
                                      do_stokes_ad35, didx,   &
                                      GC_assa_Jacobians,       &
                                      GC_assa_QJacobians,      &
                                      GC_assa_UJacobians,      &
                                      GC_spcassa_Jacobians,    &
                                      GC_spcassa_QJacobians,   &
                                      GC_spcassa_UJacobians,   &
                                      do_stokes_ad35,         &
                                      do_total_assa_jac,      &
                                      do_aer_columnwf,        &
                                      aer_d_profile_dpkh,     &
                                      aer_d_profile_dhfw,     &
                                      aer_d_profile_drel,     &
                                      aer_relaxation,         &
                                      aer_profile,            &
                                      aer_z_peakheight,       &
                                      aer_half_width,         &
                                      aer_proftype,           &
                                      do_normalized_WFoutput, &
                                      aer_profile_ssa
      
      ! ---------------------------
      ! Subroutine arguments
      ! ---------------------------
      INTEGER, INTENT(IN) :: NCID
      INTEGER, INTENT(IN) :: ACTION
      LOGICAL, INTENT(IN) :: DO_XY
      
      ! ---------------
      ! Local variables
      ! ---------------
      LOGICAL                                 :: error, dummy_err
      INTEGER, DIMENSION(3)                   :: size_3d, tmp_3d
      CHARACTER(LEN=max_ch_len), DIMENSION(3) :: dim_3d
      INTEGER, DIMENSION(4)                   :: size_4d, tmp_4d
      CHARACTER(LEN=max_ch_len), DIMENSION(4) :: dim_4d
      CHARACTER(LEN=max_ch_len)               :: fld_name
      INTEGER                                 :: n, g, s, l
      REAL(KIND=8), ALLOCATABLE               :: par_jac(:,:,:)
      
      
      ! =====================================================
      ! diag_35 starts here
      ! =====================================================
      
      ! Intialize error
      error = .FALSE.
      
      ! Dimensions of output for non-altitude dependent variables
      dim_3d(1) = 'nw' ; dim_3d(2) = 'ngeom' ; dim_3d(3) = 'noutputlevel'
      
      ! Get dimension
      CALL match_names_in_dimlist( dim_3d, 3, tmp_3d, size_3d, dummy_err )
      
      ! The dimension of the output for level variables
      dim_4d(1) = 'nw' ; dim_4d(2) = 'nlayer' ; dim_4d(3) = 'ngeom' ; dim_4d(4) = 'noutputlevel'
      
      ! Get dimension
      CALL match_names_in_dimlist( dim_4d, 4, tmp_4d, size_4d, dummy_err )
      
       ! Total ASSA
      IF( do_total_assa_jac ) THEN
        
        IF( do_aer_columnwf ) THEN
          
          CALL nc_fld_3d( dim_3d, size_3d(1), size_3d(2), size_3d(3),                      &
                          GC_assa_Jacobians(1:size_3d(1),1,1:size_3d(2),1:size_3d(3),didx),&
                          'assacolwf_jac',  NCID, ACTION, DO_XY                            )
          
          IF( do_stokes_ad35 ) THEN
           
            CALL nc_fld_3d( dim_3d, size_3d(1), size_3d(2), size_3d(3),                       &
                            GC_assa_QJacobians(1:size_3d(1),1,1:size_3d(2),1:size_3d(3),didx),&
                            'assacolwf_qjac',  NCID, ACTION, DO_XY                            )
            
            CALL nc_fld_3d( dim_3d, size_3d(1), size_3d(2), size_3d(3),                       &
                            GC_assa_UJacobians(1:size_3d(1),1,1:size_3d(2),1:size_3d(3),didx),&
                            'assacolwf_ujac',  NCID, ACTION, DO_XY                            )
            
          ENDIF
          
        ELSE
          
          CALL nc_fld_4d( dim_4d, size_4d(1), size_4d(2), size_4d(3), size_4d(4),                    &
                          GC_assa_Jacobians(1:size_4d(1),1:size_4d(2),1:size_4d(3),1:size_4d(4),didx),&
                          'assa_jac',  NCID, ACTION, DO_XY                                            )
          
          IF( do_stokes_ad35 ) THEN
            
            CALL nc_fld_4d( dim_4d, size_4d(1), size_4d(2), size_4d(3), size_4d(4),                     &
                            GC_assa_QJacobians(1:size_4d(1),1:size_4d(2),1:size_4d(3),1:size_4d(4),didx),&
                            'assa_qjac',  NCID, ACTION, DO_XY                                            )
            
            CALL nc_fld_4d( dim_4d, size_4d(1), size_4d(2), size_4d(3), size_4d(4),                     &
                            GC_assa_UJacobians(1:size_4d(1),1:size_4d(2),1:size_4d(3),1:size_4d(4),didx),&
                            'assa_ujac',  NCID, ACTION, DO_XY                                            )
            
          ENDIF
          
        ENDIF
      
      ENDIF
      
      ! ASSA for individual species
      DO n=1,Diag35_InpOpt%nspc
          
        ! Jacobian index
        g = Diag35_InpOpt%spc_idx(n)
          
        ! Name of output field
        fld_name = TRIM(ADJUSTL( Diag35_InpOpt%spc(n) )) // '_assa_jac'
          
        CALL nc_fld_4d( dim_4d, size_4d(1), size_4d(2), size_4d(3), size_4d(4),                    &
                     GC_spcassa_Jacobians(1:size_4d(1),1:size_4d(2),1:size_4d(3),1:size_4d(4),g,didx),&
                     TRIM(ADJUSTL(fld_name)),  NCID, ACTION, DO_XY                                   )
          
        IF( do_stokes_ad35 ) THEN
            
          ! Name of output field
          fld_name = TRIM(ADJUSTL( Diag35_InpOpt%spc(n) )) // '_assa_qjac'
            
          CALL nc_fld_4d( dim_4d, size_4d(1), size_4d(2), size_4d(3), size_4d(4),                    &
                    GC_spcassa_QJacobians(1:size_4d(1),1:size_4d(2),1:size_4d(3),1:size_4d(4),g,didx),&
                    TRIM(ADJUSTL(fld_name)),  NCID, ACTION, DO_XY                                    )
                      
          ! Name of output field
          fld_name = TRIM(ADJUSTL( Diag35_InpOpt%spc(n) )) // '_assa_ujac'
            
          CALL nc_fld_4d( dim_4d, size_4d(1), size_4d(2), size_4d(3), size_4d(4),                    &
                    GC_spcassa_UJacobians(1:size_4d(1),1:size_4d(2),1:size_4d(3),1:size_4d(4),g,didx),&
                    TRIM(ADJUSTL(fld_name)),  NCID, ACTION, DO_XY                                    )
            
        ENDIF
          
      ENDDO
      
      ! Allocate working array 
      ALLOCATE( par_jac(size_3d(1), size_3d(2), size_3d(3)) )
      
      ! Parameters -> Apply Chain rule to AOD profile jacobians
      DO n=1,Diag35_InpOpt%npar
          
        ! Jacobian index
        g = Diag35_InpOpt%spc_idx(n)
          
        ! Species index
        s = Diag35_InpOpt%jac_idx(g)
          
        ! Individual cases
        SELECTCASE( TRIM(ADJUSTL(aer_proftype(s))) )
          
        CASE( 'GDF' )
            
          ! ----------------------------------
          ! Peak Height
          ! ----------------------------------
            
          ! Field Name
          fld_name = TRIM(ADJUSTL( Diag35_InpOpt%par(n) )) // '_assa_pkh_jac'
          
          ! Zero Output jacobian array
          par_jac(:,:,:) = 0.0d0
            
          IF( ACTION == 2 ) THEN
              
            ! Sum layers
            IF( do_normalized_WFoutput ) THEN
              DO l=1,size_4d(2)
                IF( aer_profile(s,l) > 0.0d0 ) THEN
                  par_jac(:,:,:) = par_jac(:,:,:) + &
                    GC_spcassa_Jacobians(1:size_4d(1),l,1:size_4d(3),1:size_4d(4),g,didx) &
                    *aer_d_profile_dpkh(s,l)/aer_profile_ssa(s,l)*aer_z_peakheight(s)
                ENDIF
              ENDDO
            ELSE
              DO l=1,size_4d(2)
                par_jac(:,:,:) = par_jac(:,:,:) + &
                  GC_spcassa_Jacobians(1:size_4d(1),l,1:size_4d(3),1:size_4d(4),g,didx)*aer_d_profile_dpkh(s,l)
              ENDDO
            ENDIF
            
          ENDIF
          
          ! Write
          CALL nc_fld_3d( dim_3d, size_3d(1), size_3d(2), size_3d(3),     &
                          par_jac(1:size_3d(1),1:size_3d(2),1:size_3d(3)),&
                          TRIM(ADJUSTL(fld_name)),  NCID, ACTION, DO_XY   )
            
          IF( do_stokes_ad35 ) THEN
            
            ! Field Name
            fld_name = TRIM(ADJUSTL( Diag35_InpOpt%par(n) )) // '_assa_pkh_qjac'
              
            ! Zero Output jacobian array
            par_jac(:,:,:) = 0.0d0
              
            IF( ACTION == 2 ) THEN
                
              ! Sum layers
              IF( do_normalized_WFoutput ) THEN
                DO l=1,size_4d(2)
                  IF( aer_profile(s,l) > 0.0d0 ) THEN
                    par_jac(:,:,:) = par_jac(:,:,:) + &
                      GC_spcassa_QJacobians(1:size_4d(1),l,1:size_4d(3),1:size_4d(4),g,didx) &
                      *aer_d_profile_dpkh(s,l)/aer_profile_ssa(s,l)*aer_z_peakheight(s)
                  ENDIF
                ENDDO
              ELSE
                DO l=1,size_4d(2)
                  par_jac(:,:,:) = par_jac(:,:,:) + &
                    GC_spcassa_QJacobians(1:size_4d(1),l,1:size_4d(3),1:size_4d(4),g,didx)*aer_d_profile_dpkh(s,l)
                ENDDO
              ENDIF
                
            ENDIF
              
            ! Write
            CALL nc_fld_3d( dim_3d, size_3d(1), size_3d(2), size_3d(3),     &
                            par_jac(1:size_3d(1),1:size_3d(2),1:size_3d(3)),&
                            TRIM(ADJUSTL(fld_name)),  NCID, ACTION, DO_XY   )
            
            ! Field Name
            fld_name = TRIM(ADJUSTL( Diag35_InpOpt%par(n) )) // '_assa_pkh_ujac'
              
            ! Zero Output jacobian array
            par_jac(:,:,:) = 0.0d0
              
            IF( ACTION == 2 ) THEN
                
              ! Sum layers
              IF( do_normalized_WFoutput ) THEN
                DO l=1,size_4d(2)
                  IF( aer_profile(s,l) > 0.0d0 ) THEN
                    par_jac(:,:,:) = par_jac(:,:,:) + &
                      GC_spcassa_UJacobians(1:size_4d(1),l,1:size_4d(3),1:size_4d(4),g,didx) &
                      *aer_d_profile_dpkh(s,l)/aer_profile_ssa(s,l)*aer_z_peakheight(s)
                  ENDIF
                ENDDO
              ELSE
                DO l=1,size_4d(2)
                  par_jac(:,:,:) = par_jac(:,:,:) + &
                    GC_spcassa_UJacobians(1:size_4d(1),l,1:size_4d(3),1:size_4d(4),g,didx)*aer_d_profile_dpkh(s,l)
                ENDDO
              ENDIF
                
            ENDIF
              
            ! Write
            CALL nc_fld_3d( dim_3d, size_3d(1), size_3d(2), size_3d(3),     &
                            par_jac(1:size_3d(1),1:size_3d(2),1:size_3d(3)),&
                            TRIM(ADJUSTL(fld_name)),  NCID, ACTION, DO_XY   )
                            
          ENDIF
          
          ! ----------------------------------
          ! Aerosol half width
          ! ----------------------------------
          
          ! Field Name
          fld_name = TRIM(ADJUSTL( Diag35_InpOpt%par(n) )) // '_assa_hfw_jac'
            
          ! Zero Output jacobian array
          par_jac(:,:,:) = 0.0d0
            
          IF( ACTION == 2 ) THEN
              
            ! Sum layers
            IF( do_normalized_WFoutput ) THEN
              DO l=1,size_4d(2)
                IF( aer_profile(s,l) > 0.0d0 ) THEN
                  par_jac(:,:,:) = par_jac(:,:,:) + &
                    GC_spcassa_Jacobians(1:size_4d(1),l,1:size_4d(3),1:size_4d(4),g,didx) &
                    *aer_d_profile_dhfw(s,l)/aer_profile_ssa(s,l)*aer_half_width(s)
                ENDIF
              ENDDO
            ELSE
              DO l=1,size_4d(2)
                par_jac(:,:,:) = par_jac(:,:,:) + &
                  GC_spcassa_Jacobians(1:size_4d(1),l,1:size_4d(3),1:size_4d(4),g,didx)*aer_d_profile_dpkh(s,l)
              ENDDO
            ENDIF
              
          ENDIF
            
          ! Write
          CALL nc_fld_3d( dim_3d, size_3d(1), size_3d(2), size_3d(3),     &
                          par_jac(1:size_3d(1),1:size_3d(2),1:size_3d(3)),&
                          TRIM(ADJUSTL(fld_name)),  NCID, ACTION, DO_XY   )
          
          IF( do_stokes_ad35 ) THEN
            
            ! Field Name
            fld_name = TRIM(ADJUSTL( Diag35_InpOpt%par(n) )) // '_assa_hfw_qjac'
              
            ! Zero Output jacobian array
            par_jac(:,:,:) = 0.0d0
              
            IF( ACTION == 2 ) THEN
                
              ! Sum layers
              IF( do_normalized_WFoutput ) THEN
                DO l=1,size_4d(2)
                  IF( aer_profile(s,l) > 0.0d0 ) THEN
                    par_jac(:,:,:) = par_jac(:,:,:) + &
                      GC_spcassa_QJacobians(1:size_4d(1),l,1:size_4d(3),1:size_4d(4),g,didx) &
                      *aer_d_profile_dhfw(s,l)/aer_profile_ssa(s,l)*aer_half_width(s)
                  ENDIF
                ENDDO
              ELSE
                DO l=1,size_4d(2)
                  par_jac(:,:,:) = par_jac(:,:,:) + &
                    GC_spcassa_QJacobians(1:size_4d(1),l,1:size_4d(3),1:size_4d(4),g,didx)*aer_d_profile_dpkh(s,l)
                ENDDO
              ENDIF
                
            ENDIF
              
            ! Write
            CALL nc_fld_3d( dim_3d, size_3d(1), size_3d(2), size_3d(3),     &
                            par_jac(1:size_3d(1),1:size_3d(2),1:size_3d(3)),&
                            TRIM(ADJUSTL(fld_name)),  NCID, ACTION, DO_XY   )
            
            ! Field Name
            fld_name = TRIM(ADJUSTL( Diag35_InpOpt%par(n) )) // '_assa_hfw_ujac'
              
            ! Zero Output jacobian array
            par_jac(:,:,:) = 0.0d0
              
            IF( ACTION == 2 ) THEN
                
              ! Sum layers
              IF( do_normalized_WFoutput ) THEN
                DO l=1,size_4d(2)
                  IF( aer_profile(s,l) > 0.0d0 ) THEN
                    par_jac(:,:,:) = par_jac(:,:,:) + &
                      GC_spcassa_UJacobians(1:size_4d(1),l,1:size_4d(3),1:size_4d(4),g,didx) &
                      *aer_d_profile_dhfw(s,l)/aer_profile_ssa(s,l)*aer_half_width(s)
                  ENDIF
                ENDDO
              ELSE
                DO l=1,size_4d(2)
                  par_jac(:,:,:) = par_jac(:,:,:) + &
                    GC_spcassa_UJacobians(1:size_4d(1),l,1:size_4d(3),1:size_4d(4),g,didx)*aer_d_profile_dpkh(s,l)
                ENDDO
              ENDIF
                
            ENDIF
              
            ! Write
            CALL nc_fld_3d( dim_3d, size_3d(1), size_3d(2), size_3d(3),     &
                            par_jac(1:size_3d(1),1:size_3d(2),1:size_3d(3)),&
                            TRIM(ADJUSTL(fld_name)),  NCID, ACTION, DO_XY   )
            
            
          ENDIF
          
        CASE( 'EXP' )
            
          ! ----------------------------------
          ! Relaxation Length scale
          ! ----------------------------------
          
          ! Field Name
          fld_name = TRIM(ADJUSTL( Diag35_InpOpt%par(n) )) // '_assa_rel_jac'
          
          ! Zero Output jacobian array
          par_jac(:,:,:) = 0.0d0
            
          IF( ACTION == 2 ) THEN
            
            ! Sum layers
            IF( do_normalized_WFoutput ) THEN
              DO l=1,size_4d(2)
                IF( aer_profile(s,l) > 0.0d0 ) THEN
                  par_jac(:,:,:) = par_jac(:,:,:) + &
                    GC_spcassa_Jacobians(1:size_4d(1),l,1:size_4d(3),1:size_4d(4),g,didx) &
                    *aer_d_profile_drel(s,l)/aer_profile_ssa(s,l)*aer_relaxation(s)
                ENDIF
              ENDDO
            ELSE
              DO l=1,size_4d(2)
                par_jac(:,:,:) = par_jac(:,:,:) + &
                  GC_spcassa_Jacobians(1:size_4d(1),l,1:size_4d(3),1:size_4d(4),g,didx)*aer_d_profile_dpkh(s,l)
              ENDDO
            ENDIF
            
          ENDIF
            
          ! Write
          CALL nc_fld_3d( dim_3d, size_3d(1), size_3d(2), size_3d(3),     &
                          par_jac(1:size_3d(1),1:size_3d(2),1:size_3d(3)),&
                          TRIM(ADJUSTL(fld_name)),  NCID, ACTION, DO_XY   )
          
          IF( do_stokes_ad35 ) THEN
            
            ! Field Name
            fld_name = TRIM(ADJUSTL( Diag35_InpOpt%par(n) )) // '_assa_rel_qjac'
            
            ! Zero Output jacobian array
            par_jac(:,:,:) = 0.0d0
              
            IF( ACTION == 2 ) THEN
              
              ! Sum layers
              IF( do_normalized_WFoutput ) THEN
                DO l=1,size_4d(2)
                  IF( aer_profile(s,l) > 0.0d0 ) THEN
                    par_jac(:,:,:) = par_jac(:,:,:) + &
                      GC_spcassa_QJacobians(1:size_4d(1),l,1:size_4d(3),1:size_4d(4),g,didx) &
                      *aer_d_profile_drel(s,l)/aer_profile_ssa(s,l)*aer_relaxation(s)
                  ENDIF
                ENDDO
              ELSE
                DO l=1,size_4d(2)
                  par_jac(:,:,:) = par_jac(:,:,:) + &
                    GC_spcassa_QJacobians(1:size_4d(1),l,1:size_4d(3),1:size_4d(4),g,didx)*aer_d_profile_dpkh(s,l)
                ENDDO
              ENDIF
              
            ENDIF
              
            ! Write
            CALL nc_fld_3d( dim_3d, size_3d(1), size_3d(2), size_3d(3),     &
                            par_jac(1:size_3d(1),1:size_3d(2),1:size_3d(3)),&
                            TRIM(ADJUSTL(fld_name)),  NCID, ACTION, DO_XY   )
            
            
            ! Field Name
            fld_name = TRIM(ADJUSTL( Diag35_InpOpt%par(n) )) // '_assa_rel_ujac'
            
            ! Zero Output jacobian array
            par_jac(:,:,:) = 0.0d0
              
            IF( ACTION == 2 ) THEN
              
              ! Sum layers
              IF( do_normalized_WFoutput ) THEN
                DO l=1,size_4d(2)
                  IF( aer_profile(s,l) > 0.0d0 ) THEN
                    par_jac(:,:,:) = par_jac(:,:,:) + &
                      GC_spcassa_UJacobians(1:size_4d(1),l,1:size_4d(3),1:size_4d(4),g,didx) &
                      *aer_d_profile_drel(s,l)/aer_profile_ssa(s,l)*aer_relaxation(s)
                  ENDIF
                ENDDO
              ELSE
                DO l=1,size_4d(2)
                  par_jac(:,:,:) = par_jac(:,:,:) + &
                    GC_spcassa_UJacobians(1:size_4d(1),l,1:size_4d(3),1:size_4d(4),g,didx)*aer_d_profile_dpkh(s,l)
                ENDDO
              ENDIF
              
            ENDIF
              
            ! Write
            CALL nc_fld_3d( dim_3d, size_3d(1), size_3d(2), size_3d(3),     &
                            par_jac(1:size_3d(1),1:size_3d(2),1:size_3d(3)),&
                            TRIM(ADJUSTL(fld_name)),  NCID, ACTION, DO_XY   )
            
          ENDIF
          
          
        CASE( 'BOX' )
          
          ! No pars
          
        CASE DEFAULT
          
          print*,'DIAG34: No aertype: ' // TRIM(ADJUSTL(aer_proftype(s)))
          
        END SELECT
        
      ENDDO
        
      ! Deallocate par_jac
      DEALLOCATE( par_jac )
      
      
      
      
    END SUBROUTINE diag_35
      
END MODULE GC_diag_cldaer_module
