!------------------------------------------------------------------------------
!          Harvard University Atmospheric Chemistry Modeling Group            !
!------------------------------------------------------------------------------
!BOP
!
! !MODULE: GC_aerosols_module.f90
!
! !DESCRIPTION: This module declares variables needed for the aerosols calculations
!               and contains routines related to its usage.
!\\
!\\
! !INTERFACE: 
!
MODULE GC_aerosols_module
! 
!  !USES:
!
  USE GC_parameters_module, ONLY: GC_maxlayers, aerunit, max_ch_len
  USE GC_variables_module,  ONLY: aerfile, use_aerprof, do_aerosols,      &
                                  do_aod_jacobians, do_assa_jacobians,    &
                                  do_aerph_jacobians, do_aerhw_jacobians, &
                                  do_aer_columnwf, naer, naer0,           &
                                  aer_reflambda, taertau0, aer_tau0s,     &
                                  aer_z_lowerlimit, aer_z_upperlimit,     &
                                  aer_z_peakheight, aer_half_width,       &
                                  aer_profile, aer_d_profile_dtau,        &
                                  aer_d_profile_dpkh, aer_d_profile_dhfw, &
                                  aer_profile0, taer_profile, aer_types,  &
                                  aer_flags, aer_opdeps, aer_ssalbs,      &
                                  aer_relqext, aer_phfcn, GC_nlayers,     &
                                  heights, nmessages, messages, naer0,    &
                                  aer_d_profile_drel,aer_relaxation,      &
                                  aer_proftype, first_xy
  USE GC_read_input_module, ONLY: skip_to_filemark
  USE GC_error_module
  
  ! New aerosol plumes 
  USE GC_Aerloading_routines_m, ONLY: generate_plume
!
  IMPLICIT NONE
!
! !PUBLIC MEMBER FUNCTIONS:
!

  PUBLIC :: aerosol_profiles
!
! !PUBLIC DATA MEMBERS:
!
  ! -------------------
  ! File names for data
  ! -------------------
! 
! !REVISION HISTORY:
!  April 2013 - G. Gonzalez Abad - Initial Version
!
!EOP
!------------------------------------------------------------------------------
  CONTAINS


    SUBROUTINE aerosol_profiles(error)
      
      IMPLICIT NONE
   
      LOGICAL, INTENT(INOUT) :: error ! Error variable

      ! ---------------
      ! Index variables
      ! ---------------
      INTEGER :: i

      ! ===========================================================
      ! aerosol_profiles starts here
      ! ===========================================================
      
      error = .FALSE.
      
      IF (use_aerprof) THEN
      
         aer_profile = aer_profile0 !; naer = naer0
         
         IF (naer == 0) THEN
            do_aerosols        = .FALSE.
            do_aer_columnwf    = .FALSE.
            do_aod_Jacobians   = .FALSE.
            do_assa_Jacobians  = .FALSE.
            do_aerph_Jacobians = .FALSE.
            do_aerhw_Jacobians = .FALSE.
         END IF

      ELSE
         
         DO i = 1, naer

            IF (aer_tau0s(i) <= 0.d0) THEN
               CALL write_err_message ( .TRUE., &
                    "ERROR: Aerosol optical depth must be greater than 0")
            ENDIF
            IF (aer_z_lowerlimit(i) >= aer_z_upperlimit(i) ) THEN
               CALL write_err_message ( .TRUE., &
                    "ERROR: Aerosol bottom must be lower than aerosol top")
            ENDIF
            IF (aer_z_peakheight(i) > aer_z_upperlimit(i)) THEN
               CALL write_err_message ( .TRUE., &
                    "ERROR: Aerosol peak height should be <= upperlimit")
            END IF
            IF (aer_z_peakheight(i) < aer_z_lowerlimit(i)) THEN
               CALL write_err_message ( .TRUE., &
                    "ERROR: Aerosol peak height should be >= lowerlimit")
            ENDIF
            
            IF (aer_z_lowerlimit(i) < heights(GC_nlayers)) &
                 aer_z_lowerlimit(i) = heights(GC_nlayers)
            IF (aer_z_upperlimit(i) > heights(0)) &
                 aer_z_upperlimit(i) = heights(0)
            
            ! ----------------------------------------------
            ! Generate aerosol plume/profiles based on input
            ! ----------------------------------------------
            
            ! New call (GC_aerloading_routines_m.f90)
            CALL generate_plume(                                     &
                  GC_nlayers, GC_nlayers, heights(0:GC_nlayers),     & ! input
                  aer_proftype(i), aer_z_upperlimit(i),              & ! input
                  aer_z_lowerlimit(i), aer_z_peakheight(i),          & ! input
                  aer_tau0s(i), aer_half_width(i),                   & ! input
                  aer_relaxation(i),                                 & ! input
                  aer_profile(i, 1:GC_nlayers),                      & ! output
                  aer_d_profile_dtau(i, 1:GC_nlayers),               & ! output
                  aer_d_profile_dpkh(i,1:GC_nlayers),                & ! output
                  aer_d_profile_dhfw(i,1:GC_nlayers),                & ! output
                  aer_d_profile_drel(i,1:GC_nlayers),                & ! output
                  error, messages(nmessages+1) )
            
            ! ------
            ! Errors
            ! ------
            IF (error) CALL write_err_message (.TRUE., messages(nmessages+1))

         END DO
      END IF
      
      DO i = 1, naer
         WHERE (aer_profile(i, 1:GC_nlayers) > 0.0d0)
            aer_flags(1:GC_nlayers) = .TRUE.
         END WHERE
      END DO
      
      DO i = 1, GC_nlayers
         IF (aer_flags(i)) taer_profile(i) = SUM(aer_profile(1:naer, i))
      END DO
      
      ! Compute total AOD at reference wavelength
      taertau0 = SUM( taer_profile )
      
      ! Load Aerosol Optical Properties on first call
      IF( first_xy ) THEN
        CALL load_aerosol_optical_properties( error )
      ENDIF
      
    END SUBROUTINE aerosol_profiles
    
    SUBROUTINE load_aerosol_optical_properties( error )
      
      USE GC_parameters_module
      USE GC_variables_module,  ONLY : naer, AerOptProp, &
           database_dir,aer_data_filenames
      
      IMPLICIT NONE
      INCLUDE 'netcdf.inc'
      
      ! ----------------------
      ! Input/Output variables
      ! ----------------------
      LOGICAL, INTENT(OUT) :: error ! Error variable
      
      
      ! ---------------
      ! Local Variables
      ! ---------------
      INTEGER :: n, nrh_i, nmom_i, nwvl_i
      CHARACTER(LEN=max_ch_len) :: infile
      
      INTEGER :: rcode, ncid, vid
!       REAL(KIND=8), DIMENSION(mwl) :: wtmp
      
      ! ===========================================================
      ! load_aerosol_optical_properties starts here
      ! ===========================================================
      
      ! Initialize Error
      error = .FALSE.
      
      ! Allocate 
      ALLOCATE( AerOptProp( naer ) )
      
      DO n=1, naer
        
        infile = TRIM(ADJUSTL(database_dir)) // '/AerCldProp/' // &
                 TRIM(ADJUSTL( aer_data_filenames(n) ))
        
        ! Open file
        rcode = NF_OPEN(trim(adjustl(infile)), NF_SHARE, ncid)
        
        ! Read number of wavelengths
        rcode = NF_INQ_VARID (ncid, 'nwls', vid)
        rcode = NF_GET_VAR(ncid, vid, nwvl_i)
        AerOptProp(n)%nWavelength = nwvl_i
        
        ! Read Number of relative humidities
        rcode = NF_INQ_VARID (ncid, 'nrh', vid)
        rcode = NF_GET_VAR(ncid, vid, nrh_i)
        AerOptProp(n)%nRH = nrh_i
        
        ! Read number of aerosol moments
        rcode = NF_INQ_VARID (ncid, 'nmom', vid)
        rcode = NF_GET_VAR(ncid, vid, nmom_i)
        AerOptProp(n)%nMoments = nmom_i-1
        
        ! Allocate arrays
        ALLOCATE( AerOptProp(n)%PhFcn0(1:nwvl_i,0:nmom_i-1,1:6,1:nrh_i) )
        ALLOCATE( AerOptProp(n)%QExt0(nwvl_i,nrh_i)                     )
        ALLOCATE( AerOptProp(n)%SSA0(nwvl_i,nrh_i)                      )
        ALLOCATE( AerOptProp(n)%Wavelength(nwvl_i)                      )
        ALLOCATE( AerOptProp(n)%RH(nrh_i)                               )
        
        ! Read wavelength grid
        rcode = NF_INQ_VARID(ncid, 'wls0', vid)
        rcode = NF_GET_VAR(ncid, vid, AerOptProp(n)%Wavelength )
        
        ! Read relative humidity grid
        rcode = NF_INQ_VARID(ncid, 'rh', vid)
        rcode = NF_GET_VAR(ncid, vid, AerOptProp(n)%RH )
        
        ! Read Extinction
        rcode = NF_INQ_VARID(ncid, 'qext0', vid)
        rcode = NF_GET_VAR(ncid, vid, AerOptProp(n)%QExt0 )
        
        ! Read SSA
        rcode = NF_INQ_VARID(ncid, 'ssa0', vid)
        rcode = NF_GET_VAR(ncid, vid, AerOptProp(n)%SSA0 )
        
        ! Read phase function expansion
        rcode = NF_INQ_VARID(ncid, 'phfcn0', vid)
        rcode = NF_GET_VAR(ncid, vid,                                             &
                           AerOptProp(n)%PhFcn0(1:nwvl_i,0:nmom_i-1,1:6,1:nrh_i) ,&
                           (/1,1,1,1/),(/nwvl_i,nmom_i,6,nrh_i/)                  )
        
        ! Close file
        rcode = NF_CLOSE(ncid)
        
        
        
      ENDDO
      
    END SUBROUTINE load_aerosol_optical_properties
    
    SUBROUTINE interp_aerosol_optics( wvl_out, nlayers, nmoms, ngkout, &
                                      opdeps, ssas, qexts, phfcn, error )
      
      USE GC_variables_module,  ONLY : naer, AerOptProp, aer_reflambda, &
                                       rh_profile, aer_profile, yn_aer_rhdep, &
                                       Diag35_InpOpt, aer_spc_ssalbs, aer_profile_ssa
      
      IMPLICIT NONE
      
      ! ----------------------
      ! Input/Output variables
      ! ----------------------
      INTEGER,      INTENT(IN)                                        :: nlayers,nmoms,ngkout
      REAL(KIND=8), INTENT(IN)                                        :: wvl_out
      REAL(KIND=8), DIMENSION (nlayers), INTENT(OUT)                  :: opdeps, ssas, qexts
      REAL(KIND=8), DIMENSION (nlayers, 0:nmoms, ngkout), INTENT(OUT) :: phfcn
      
      
      LOGICAL, INTENT(OUT) :: error ! Error variable
      
      REAL(KIND=8) :: wmin,wmax,rhmin,rhmax
      REAL(KIND=8) :: ywt0, ywt1, xwt0, xwt1
      REAL(KIND=8) :: xwt0_r, xwt1_r ! Reference wavelength
      INTEGER      :: idy0, idx0, idx0_r, moms_loop
      INTEGER      :: n, j, l, g, m
      REAL(KIND=8) :: scaods
      REAL(KIND=8), DIMENSION(nlayers) :: tsca
      REAL(KIND=8) :: qext_wvl, qext_ref, qext_rel
      REAL(KIND=8) :: ssa_wvl, phfcn_int

      ! ===========================================================
      ! interp_aerosol_optics starts here
      ! ===========================================================
      
      ! Initialize Error
      error = .FALSE.
      
      ! Zero output arrays
      opdeps(:)     = 0.0d0
      ssas(:)       = 0.0d0
      qexts(:)      = 0.0d0
      phfcn(:,:,:)  = 0.0d0
     
      ! And some local ones
      tsca(:) = 0.0
      
      ! Diagnostic 35 requires saving species level SSAlbs
      IF( Diag35_InpOpt%njac > 0 ) THEN
        IF( .NOT. ALLOCATED(aer_spc_ssalbs)  ) ALLOCATE( aer_spc_ssalbs(nlayers,naer)  )
        IF( .NOT. ALLOCATED(aer_profile_ssa) ) ALLOCATE( aer_profile_ssa(naer,nlayers) )
      ENDIF
      
      ! Loop over aerosol types
      DO n=1,naer
        
        ! Wavelength/RH bounds
        wmin  = AerOptProp(n)%Wavelength(1)
        wmax  = AerOptProp(n)%Wavelength(AerOptProp(n)%nWavelength)
        rhmin = AerOptProp(n)%RH(1)
        rhmax = AerOptProp(n)%RH( AerOptProp(n)%nRH )
        
        ! Determine max number of moments to loop
        moms_loop = MIN(AerOptProp(n)%nMoments,nmoms) 
        
        ! Get weights for output wavelength interpolation
        ! ------------------------------------------------------------
        IF( wvl_out .LE. wmin ) THEN
          idx0 = 1
          xwt0 = 1.0
          xwt1 = 0.0
        ELSEIF( wvl_out .GE. wmax ) THEN
          idx0 = AerOptProp(n)%nWavelength-1
          xwt0 = 0.0
          xwt1 = 1.0
        ELSE 
          idx0 = -1
          j    =  1
          DO WHILE (idx0 .LT. 0)
            IF( AerOptProp(n)%Wavelength( j ) .GT. wvl_out ) THEN
              idx0 = j-1
              xwt0 = (AerOptProp(n)%Wavelength(idx0+1)-wvl_out)/&
                    (AerOptProp(n)%Wavelength(idx0+1)-AerOptProp(n)%Wavelength(idx0))
              xwt1 = (wvl_out-AerOptProp(n)%Wavelength(idx0)  )/&
                    (AerOptProp(n)%Wavelength(idx0+1)-AerOptProp(n)%Wavelength(idx0))
            ENDIF
            j = j+1
          ENDDO
        ENDIF
        
        ! Get weights for reference wavelength interpolation
        ! ------------------------------------------------------------
        IF( aer_reflambda.LE. wmin ) THEN
          idx0_r = 1
          xwt0_r = 1.0
          xwt1_r = 0.0
        ELSEIF( aer_reflambda .GE. wmax ) THEN
          idx0_r = AerOptProp(n)%nWavelength-1
          xwt0_r = 0.0
          xwt1_r = 1.0
        ELSE 
          idx0_r = -1
          j    =  1
          DO WHILE (idx0_r .LT. 0)
            IF( AerOptProp(n)%Wavelength( j ) .GT. aer_reflambda ) THEN
              idx0_r = j-1
              xwt0_r = (AerOptProp(n)%Wavelength(idx0_r+1)-aer_reflambda)/&
                      (AerOptProp(n)%Wavelength(idx0_r+1)-AerOptProp(n)%Wavelength(idx0_r))
              xwt1_r = (aer_reflambda-AerOptProp(n)%Wavelength(idx0_r)  )/&
                      (AerOptProp(n)%Wavelength(idx0_r+1)-AerOptProp(n)%Wavelength(idx0_r))
            ENDIF
            j = j+1
          ENDDO
        ENDIF
        
       
        IF( yn_aer_rhdep(n) .AND. AerOptProp(n)%nRH > 1  ) THEN ! RH dep then bilinearly interpolaet 
          
          DO l=1,nlayers
            
            
            ! Get RH weights
            ! -------------------------------------------------------
            IF( rh_profile(l) .LE. rhmin ) THEN
              idy0 = 1
              ywt0 = 1.0
              ywt1 = 0.0
            ELSEIF( rh_profile(l) .GE. rhmax ) THEN 
              idy0 = AerOptProp(n)%nRH-1
              ywt0 = 0.0
              ywt1 = 1.0
            ELSE 
              idy0 = -1
              j    =  1
              DO WHILE ( idy0 .LT. 0 )
                IF( AerOptProp(n)%RH( j ) .GT. rh_profile(l)) THEN
                  idy0 = j-1
                  ywt0 = (AerOptProp(n)%RH(idy0+1)-rh_profile(l))/&
                        (AerOptProp(n)%RH(idy0+1)-AerOptProp(n)%RH(idy0))
                  ywt1 = (rh_profile(l)-AerOptProp(n)%RH(idy0)  )/&
                        (AerOptProp(n)%RH(idy0+1)-AerOptProp(n)%RH(idy0))
                ENDIF
                j = j+1
              ENDDO
            ENDIF
 
            ! Add to output profile
            qext_ref = ywt0 * (  xwt0_r*AerOptProp(n)%QExt0(idx0_r,idy0)       &
                               + xwt1_r*AerOptProp(n)%QExt0(idx0_r+1,idy0)   ) & 
                     + ywt1 * (  xwt0_r*AerOptProp(n)%QExt0(idx0_r,idy0+1)     &
                               + xwt1_r*AerOptProp(n)%QExt0(idx0_r+1,idy0+1) )
            qext_wvl = ywt0 * (  xwt0*AerOptProp(n)%QExt0(idx0,idy0)         &
                              + xwt1*AerOptProp(n)%QExt0(idx0+1,idy0)   )   &
                     + ywt1 * (  xwt0*AerOptProp(n)%QExt0(idx0,idy0+1)       &
                               + xwt1*AerOptProp(n)%QExt0(idx0+1,idy0+1) )
              
            ! Ratio of extinction at output wavelength relative to reference wavelength
            qext_rel = qext_wvl/qext_ref
              
            ! Update total extinction
            opdeps(l) = opdeps(l) + aer_profile(n,l)*qext_rel
              
            ! Interpolate single scattering albedo
            ssa_wvl = ywt0 * (  xwt0*AerOptProp(n)%SSA0(idx0,idy0)         &
                              + xwt1*AerOptProp(n)%SSA0(idx0+1,idy0)   )   &
                    + ywt1 * (  xwt0*AerOptProp(n)%SSA0(idx0,idy0+1)       &
                              + xwt1*AerOptProp(n)%SSA0(idx0+1,idy0+1) )
            
            ! Scattering Optical depth
            scaods = aer_profile(n,l) * qext_rel * ssa_wvl
              
            ! Update total scattering optical depth
            tsca(l) = tsca(l) + scaods
            
            
            ! SSA diagnostic
            IF( Diag35_InpOpt%njac > 0 ) THEN
              
              ! Save SSA
              aer_spc_ssalbs(l,n) = ssa_wvl
              
              ! Jacobians referenced to SSA at reference wavelength
               aer_profile_ssa(n,l) = ywt0 * (  xwt0_r*AerOptProp(n)%SSA0(idx0_r,idy0)       &
                                              + xwt1_r*AerOptProp(n)%SSA0(idx0_r+1,idy0)   ) & 
                                    + ywt1 * (  xwt0_r*AerOptProp(n)%SSA0(idx0_r,idy0+1)     &
                                              + xwt1_r*AerOptProp(n)%SSA0(idx0_r+1,idy0+1) )
              
            ENDIF
            
            
            ! Compute phase moments
            DO g=1,ngkout
              DO m=0,moms_loop
                
                ! Compute interpolated value
                phfcn_int = ywt0 * (  xwt0*AerOptProp(n)%PhFcn0(idx0,m,g,idy0)         &
                                    + xwt1*AerOptProp(n)%PhFcn0(idx0+1,m,g,idy0)   )   &
                          + ywt1 * (  xwt0*AerOptProp(n)%PhFcn0(idx0,m,g,idy0+1)       &
                                    + xwt1*AerOptProp(n)%PhFcn0(idx0+1,m,g,idy0+1) )
                  
                ! Weight momemnts by scattering optical depth
                phfcn(l,m,g) = phfcn(l,m,g) + scaods*phfcn_int
              
              ENDDO
            ENDDO
          
        ENDDO ! Layers
        
      ELSE ! No RH dependence
          
          ! Interpolate extinction
          qext_ref = xwt0_r*AerOptProp(n)%QExt0(idx0_r,1) &
                   + xwt1_r*AerOptProp(n)%QExt0(idx0_r+1,1)
          qext_wvl = xwt0*AerOptProp(n)%QExt0(idx0,1) &
                   + xwt1*AerOptProp(n)%QExt0(idx0+1,1)
          
          ! Ratio of extinction at output wavelength relative to reference wavelength
          qext_rel = qext_wvl/qext_ref
          
          ! Update total extinction
          DO l=1,nlayers
            opdeps(l) = opdeps(l) + aer_profile(n,l)*qext_rel
          ENDDO
         
          ! Interpolate single scattering albedo
          ssa_wvl = xwt0*AerOptProp(n)%SSA0(idx0,1)    &
                  + xwt1*AerOptProp(n)%SSA0(idx0+1,1)  
          
          
          IF( Diag35_InpOpt%njac > 0 ) THEN
            
            ! Save SSA at wavelength
            aer_spc_ssalbs(:,n) = ssa_wvl
            
            ! SSA at refrence
            aer_profile_ssa(n,:) = xwt0_r*AerOptProp(n)%SSA0(idx0_r,1) &
                                 + xwt1_r*AerOptProp(n)%SSA0(idx0_r+1,1)
          ENDIF
          
          ! Update
          DO l=1,nlayers

            ! Scattering optical depth
            scaods = aer_profile(n,l) * qext_rel * ssa_wvl
             
            ! Update total scattering
            tsca(l) = tsca(l) + scaods

            ! Compute phase moment
            DO g=1,ngkout
              DO m=0,moms_loop
                
                ! Compute interpolated value
                phfcn_int = xwt0*AerOptProp(n)%PhFcn0(idx0,m,g,1) &
                          + xwt1*AerOptProp(n)%PhFcn0(idx0+1,m,g,1)

                ! Weight momemnts by scattering optical depth
                phfcn(l,m,g) = phfcn(l,m,g) + scaods*phfcn_int
                
              ENDDO
            ENDDO

          ENDDO
            
          
        ENDIF
        
      ENDDO ! Aerosol types
      
      ! Complete calculation
      DO l=1,nlayers
        
        IF( opdeps(l) .GT. 0.0 ) THEN
 
          ! Extinction relative to reference wavelength
          qexts(l) = opdeps(l) / SUM( aer_profile(1:naer,l) )
          
          ! Compute SSA for layer
          ssas(l) = tsca(l) / opdeps(l)
        
          ! Renormalize phase function after weighting
          IF( tsca(l) .GT. 0.0 ) THEN
            DO g=1,ngkout
              DO m=0,nmoms
                phfcn(l,m,g) = phfcn(l,m,g) / tsca(l)
              ENDDO
            ENDDO
          ENDIF

        ENDIF
     
      ENDDO
      
    END SUBROUTINE interp_aerosol_optics
    
    SUBROUTINE interp_aerosol_spc_optics( wvl_out,     n,     l,  nmoms, ngkout, &
                                          opdeps,   ssas, qexts,  phfcn,   error )
      
      USE GC_variables_module,  ONLY : AerOptProp, aer_reflambda,           &
                                       rh_profile, aer_profile, yn_aer_rhdep
      IMPLICIT NONE
      ! This works like the above scheme but for a single species in 
      ! a single layer
      ! n - Aerosol species index
      ! l - Layer index
      
      ! ----------------------
      ! Input/Output variables
      ! ----------------------
      INTEGER,      INTENT(IN)                               :: n, l, nmoms, ngkout
      REAL(KIND=8), INTENT(IN)                               :: wvl_out
      REAL(KIND=8),                              INTENT(OUT) :: opdeps, ssas, qexts
      REAL(KIND=8), DIMENSION (0:nmoms, ngkout), INTENT(OUT) :: phfcn
      
      
      LOGICAL, INTENT(OUT) :: error ! Error variable
      
      REAL(KIND=8) :: wmin,wmax,rhmin,rhmax
      REAL(KIND=8) :: ywt0, ywt1, xwt0, xwt1
      REAL(KIND=8) :: xwt0_r, xwt1_r ! Reference wavelength
      INTEGER      :: idy0, idx0, idx0_r, moms_loop
      INTEGER      :: j, g, m
      REAL(KIND=8) :: qext_wvl, qext_ref, qext_rel
      REAL(KIND=8) :: ssa_wvl, phfcn_int
      
      
      ! =============================================================================
      ! interp_aerosol_spc_optics starts here
      ! =============================================================================
      
      ! Initialize variables
      phfcn(:,:) = 0.0d0
      opdeps     = 0.0d0
      ssas       = 0.0d0
      qexts      = 0.0d0
      
      ! Wavelength/RH bounds
      wmin  = AerOptProp(n)%Wavelength(1)
      wmax  = AerOptProp(n)%Wavelength(AerOptProp(n)%nWavelength)
      rhmin = AerOptProp(n)%RH(1)
      rhmax = AerOptProp(n)%RH( AerOptProp(n)%nRH )
      
      ! Determine max number of moments to loop
      moms_loop = MIN(AerOptProp(n)%nMoments,nmoms)
      
      ! Get weights for output wavelength interpolation
      ! ------------------------------------------------------------
      IF( wvl_out .LE. wmin ) THEN
        idx0 = 1
        xwt0 = 1.0
        xwt1 = 0.0
      ELSEIF( wvl_out .GE. wmax ) THEN
        idx0 = AerOptProp(n)%nWavelength-1
        xwt0 = 0.0
        xwt1 = 1.0
      ELSE 
        idx0 = -1
        j    =  1
        DO WHILE (idx0 .LT. 0)
          IF( AerOptProp(n)%Wavelength( j ) .GT. wvl_out ) THEN
            idx0 = j-1
            xwt0 = (AerOptProp(n)%Wavelength(idx0+1)-wvl_out)/&
                  (AerOptProp(n)%Wavelength(idx0+1)-AerOptProp(n)%Wavelength(idx0))
            xwt1 = (wvl_out-AerOptProp(n)%Wavelength(idx0)  )/&
                  (AerOptProp(n)%Wavelength(idx0+1)-AerOptProp(n)%Wavelength(idx0))
          ENDIF
          j = j+1
        ENDDO
      ENDIF
      
      ! Get weights for reference wavelength interpolation
      ! ------------------------------------------------------------
      IF( aer_reflambda.LE. wmin ) THEN
        idx0_r = 1
        xwt0_r = 1.0
        xwt1_r = 0.0
      ELSEIF( aer_reflambda .GE. wmax ) THEN
        idx0_r = AerOptProp(n)%nWavelength-1
        xwt0_r = 0.0
        xwt1_r = 1.0
      ELSE 
        idx0_r = -1
        j    =  1
        DO WHILE (idx0_r .LT. 0)
          IF( AerOptProp(n)%Wavelength( j ) .GT. aer_reflambda ) THEN
            idx0_r = j-1
            xwt0_r = (AerOptProp(n)%Wavelength(idx0_r+1)-aer_reflambda)/&
                    (AerOptProp(n)%Wavelength(idx0_r+1)-AerOptProp(n)%Wavelength(idx0_r))
            xwt1_r = (aer_reflambda-AerOptProp(n)%Wavelength(idx0_r)  )/&
                    (AerOptProp(n)%Wavelength(idx0_r+1)-AerOptProp(n)%Wavelength(idx0_r))
          ENDIF
          j = j+1
        ENDDO
      ENDIF
      
      IF( yn_aer_rhdep(n) .AND. AerOptProp(n)%nRH > 1  ) THEN 
        
        ! Get RH weights
        ! -------------------------------------------------------
        IF( rh_profile(l) .LE. rhmin ) THEN
          idy0 = 1
          ywt0 = 1.0
          ywt1 = 0.0
        ELSEIF( rh_profile(l) .GE. rhmax ) THEN 
          idy0 = AerOptProp(n)%nRH-1
          ywt0 = 0.0
          ywt1 = 1.0
        ELSE 
          idy0 = -1
          j    =  1
          DO WHILE ( idy0 .LT. 0 )
            IF( AerOptProp(n)%RH( j ) .GT. rh_profile(l)) THEN
              idy0 = j-1
              ywt0 = (AerOptProp(n)%RH(idy0+1)-rh_profile(l))/&
                    (AerOptProp(n)%RH(idy0+1)-AerOptProp(n)%RH(idy0))
              ywt1 = (rh_profile(l)-AerOptProp(n)%RH(idy0)  )/&
                    (AerOptProp(n)%RH(idy0+1)-AerOptProp(n)%RH(idy0))
            ENDIF
            j = j+1
          ENDDO
        ENDIF
        
        ! Add to output profile
        qext_ref = ywt0 * (  xwt0_r*AerOptProp(n)%QExt0(idx0_r,idy0)       &
                           + xwt1_r*AerOptProp(n)%QExt0(idx0_r+1,idy0)   ) & 
                 + ywt1 * (  xwt0_r*AerOptProp(n)%QExt0(idx0_r,idy0+1)     &
                           + xwt1_r*AerOptProp(n)%QExt0(idx0_r+1,idy0+1) )
        qext_wvl = ywt0 * (  xwt0*AerOptProp(n)%QExt0(idx0,idy0)         &
                          + xwt1*AerOptProp(n)%QExt0(idx0+1,idy0)   )   &
                 + ywt1 * (  xwt0*AerOptProp(n)%QExt0(idx0,idy0+1)       &
                           + xwt1*AerOptProp(n)%QExt0(idx0+1,idy0+1) )
        qexts = qext_wvl
        
        ! Ratio of extinction at output wavelength relative to reference wavelength
        qext_rel = qext_wvl/qext_ref
          
        ! Update total extinction
        opdeps = aer_profile(n,l)*qext_rel
          
        ! Interpolate single scattering albedo
        ssas = ywt0 * (  xwt0*AerOptProp(n)%SSA0(idx0,idy0)         &
                        + xwt1*AerOptProp(n)%SSA0(idx0+1,idy0)   )   &
             + ywt1 * (  xwt0*AerOptProp(n)%SSA0(idx0,idy0+1)       &
                        + xwt1*AerOptProp(n)%SSA0(idx0+1,idy0+1) )
        
        
        ! Compute phase moments
        DO g=1,ngkout
        DO m=0,moms_loop

          ! Weight momemnts by scattering optical depth
          phfcn(m,g) = ywt0 * (  xwt0*AerOptProp(n)%PhFcn0(idx0,m,g,idy0)         &
                               + xwt1*AerOptProp(n)%PhFcn0(idx0+1,m,g,idy0)   )   &
                     + ywt1 * (  xwt0*AerOptProp(n)%PhFcn0(idx0,m,g,idy0+1)       &
                               + xwt1*AerOptProp(n)%PhFcn0(idx0+1,m,g,idy0+1) )
          
        ENDDO
        ENDDO
        
      ELSE
        
        ! Interpolate extinction
        qext_ref = xwt0_r*AerOptProp(n)%QExt0(idx0_r,1) &
                 + xwt1_r*AerOptProp(n)%QExt0(idx0_r+1,1)
        qext_wvl = xwt0*AerOptProp(n)%QExt0(idx0,1) &
                 + xwt1*AerOptProp(n)%QExt0(idx0+1,1)
        qexts = qext_wvl
        
        ! Ratio of extinction at output wavelength relative to reference wavelength
        qext_rel = qext_wvl/qext_ref
        
        ! Update total extinction
        opdeps = opdeps + aer_profile(n,l)*qext_rel
        
        ! Interpolate single scattering albedo
        ssas = xwt0*AerOptProp(n)%SSA0(idx0,1)    &
             + xwt1*AerOptProp(n)%SSA0(idx0+1,1)
     
         
        ! Compute phase moment
        DO g=1,ngkout
        DO m=0,moms_loop
        
          ! Weight momemnts by scattering optical depth
          phfcn(m,g) = xwt0*AerOptProp(n)%PhFcn0(idx0,m,g,1) &
                     + xwt1*AerOptProp(n)%PhFcn0(idx0+1,m,g,1)
              
        ENDDO
        ENDDO
        
      ENDIF
        
    END SUBROUTINE interp_aerosol_spc_optics
    
END MODULE GC_aerosols_module