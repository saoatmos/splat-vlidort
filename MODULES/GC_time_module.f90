! DESCRIPTION: Module GC_TIME_MODULE contains routines used to convert from 
!  month/day/year to Astronomical Julian Date and back again.
MODULE GC_TIME_MODULE

  IMPLICIT NONE
  PRIVATE

  PUBLIC :: JULDAY
  PUBLIC :: CALDATE
  PUBLIC :: GET_DAY_OF_YEAR
  PUBLIC :: MINT
!
! !REVISION HISTORY:
!  (1 ) Moved JULDAY, MINT, CALDATE here from "bpch2_mod.f" (bmy, 11/20/01)
!  (2 ) Bug fix: now compute NHMS correctly.  Also use REAL*4 variables to
!        avoid roundoff errors. (bmy, 11/26/01)
!  (3 ) Updated comments (bmy, 5/28/02)
!  (4 ) Renamed arguments for clarity (bmy, 6/26/02)
!  20 Nov 2009 - R. Yantosca - Added ProTeX Headers
!  20 Aug 2013 - R. Yantosca - Removed "define.h", this is now obsolete
!  02 Dec 2014 - M. Yannetti - Added PRECISION_MOD
CONTAINS

! NAME:
!	JULDAY
!
! PURPOSE:
!	Calculate the Julian Day Number for a given month, day, and year.
!	This is the inverse of the library function CALDAT.
!	See also caldat, the inverse of this function.
!
! INPUTS:
!	MONTH:	Number of the desired month (1 = January, ..., 12 = December).
!
!	DAY:	Number of day of the month.
!
!	YEAR:	Number of the desired year. Year parameters must be valid
!              values from the civil calendar. Years B.C.E. are represented
!              as negative integers. Years in the common era are represented
!              as positive integers. In particular, note that there is no
!              year 0 in the civil calendar. 1 B.C.E. (-1) is followed by
!              1 C.E. (1).
!
!	HOUR:	Number of the hour of the day.
!
!	MINUTE:	Number of the minute of the hour.
!
!	SECOND:	Number of the second of the minute (double precision number).
!
! OUTPUTS:
!	JULDAY returns the Julian Day Number (which begins at noon) of the
!	specified calendar date.
!
! RESTRICTIONS:
!	Accuracy using IEEE double precision numbers is approximately
!      1/10000th of a second, with higher accuracy for smaller (earlier)
!      Julian dates.
!
! MODIFICATION HISTORY:
!	Translated from "Numerical Recipies in C", by William H. Press,
!	Brian P. Flannery, Saul A. Teukolsky, and William T. Vetterling.
!	Cambridge University Press, 1988 (second printing).
!
!	AB, September, 1988
!	DMS, April, 1995, Added time of day.
!
!      Translated into Fortran90 by Vijay Natraj, JPL, February 24 2012

  function JULDAY(MONTH, DAY, YEAR, Hour, Minute, Second)
    
    implicit none
    
    !  Inputs
    
    integer(kind=4), intent(in)  :: MONTH
    integer(kind=4), intent(in)  :: DAY
    integer(kind=4), intent(in)  :: YEAR
    integer(kind=4), intent(in)  :: HOUR
    integer(kind=4), intent(in)  :: MINUTE
    real(kind=8),    intent(in)  :: SECOND
    
    !  Outputs
    
    real(kind=8)                 :: JULDAY
    
    !  Local variables
    
    integer(kind=4)              :: GREG
    integer(kind=4)              :: min_calendar
    integer(kind=4)              :: max_calendar
    integer(kind=4)              :: bc
    integer(kind=4)              :: L_YEAR
    integer(kind=4)              :: inJanFeb
    integer(kind=4)              :: JY
    integer(kind=4)              :: JM
    integer(kind=4)              :: JA
    integer(kind=4)              :: JUL
    real(kind=8)                 :: eps
    
    ! Gregorian Calendar was adopted on Oct. 15, 1582
    ! skipping from Oct. 4, 1582 to Oct. 15, 1582
    
    GREG = 2299171  ! incorrect Julian day for Oct. 25, 1582
    
    ! check if date is within allowed range
    
    min_calendar = -4716
    max_calendar = 5000000
    IF ((YEAR .LT. min_calendar) .OR. (YEAR .GT. max_calendar)) &
         write(0,*) 'Value of Julian date is out of allowed range'
    
    IF (YEAR .LT. 0) THEN
       bc = 1
    ELSE
       bc = 0
    ENDIF
    L_YEAR = YEAR + bc
    
    IF (MONTH .LE. 2) THEN
       inJanFeb = 1
    ELSE
       inJanFeb = 0
    ENDIF
    
    JY = YEAR - inJanFeb
    JM = MONTH + 1 + 12*inJanFeb
    
    JUL = FLOOR(365.25d0 * JY) + FLOOR(30.6001d0 * JM) + DAY + 1720995
    
    ! Test whether to change to Gregorian Calendar.
    
    IF (JUL .GE. GREG) THEN
       JA = FLOOR(0.01d0 * JY)
       JUL = JUL + 2 - JA + FLOOR(0.25d0 * JA)
    ENDIF
    
    ! Add a small offset so we get the hours, minutes, & seconds back correctly
    ! if we convert the Julian dates back. This offset is proportional to the
    ! Julian date, so small dates (a long, long time ago) will be "more" accurate.
    
    ! eps = (MACHAR(/DOUBLE)).eps
    
    eps = 2.2204460d-16 ! For Ganesha, calculated from IDL output of above statement
    
    IF (ABS(JUL) .GE. 1) THEN
       eps = eps*ABS(JUL)
    ENDIF
    
    ! For Hours, divide by 24, then subtract 0.5, in case we have unsigned integers.
    
    JULDAY = real(JUL,kind=8) + real(Hour,kind=8)/24.d0 - &
         0.5d0 + real(Minute,kind=8)/1440.d0 + &
         Second/86400.d0 + eps
    
    RETURN
  END FUNCTION JULDAY
  
  ! !DESCRIPTION: Function MINT is the modified integer function.
  FUNCTION MINT( X ) RESULT ( VALUE )
    !
    ! !INPUT PARAMETERS: 
    !
    REAL(KIND=8), INTENT(IN) :: X
    !
    ! !RETURN VALUE:
    !
    REAL(KIND=8) :: VALUE
    !
    ! !REMARKS:
    !  The modified integer function is defined as follows:
    !
    !            { -INT( ABS( X ) )   for X < 0
    !     MINT = {
    !            {  INT( ABS( X ) )   for X >= 0
    !
    ! !REVISION HISTORY: 
    !  20 Nov 2001 - R. Yantosca - Initial version
    !  20 Nov 2009 - R. Yantosca - Added ProTeX headers
    IF ( X < 0d0 ) THEN 
       VALUE = -INT( ABS( X ) )        
    ELSE
       VALUE =  INT( ABS( X ) )        
    ENDIF
    
  END FUNCTION MINT
  ! !DESCRIPTION: Subroutine CALDATE converts an astronomical Julian day to 
  !  the YYYYMMDD and HHMMSS format.
  SUBROUTINE CALDATE( JULIANDAY, YYYYMMDD, HHMMSS )
    !
    ! !INPUT PARAMETERS: 
    !
    ! Arguments
    REAL(KIND=8), INTENT(IN) :: JULIANDAY  ! Astronomical Julian Date 
    !
    ! !OUTPUT PARAMETERS: 
    !
    INTEGER, INTENT(OUT) :: YYYYMMDD   ! Date in YYYY/MM/DD format
    INTEGER, INTENT(OUT) :: HHMMSS     ! Time in hh:mm:ss format
    !
    ! !REMARKS:
    !   Algorithm taken from "Practical Astronomy With Your Calculator",
    !   Third Edition, by Peter Duffett-Smith, Cambridge UP, 1992.
    !
    ! !REVISION HISTORY: 
    !  (1 ) Now compute HHMMSS correctly.  Also use REAL*4 variables HH, MM, SS
    !        to avoid roundoff errors. (bmy, 11/21/01)
    !  (2 ) Renamed NYMD to YYYYMMDD and NHMS to HHMMSS for documentation
    !        purposes (bmy, 6/26/02)
    !  20 Nov 2009 - R. Yantosca - Added ProTeX header
    !
    ! !LOCAL VARIABLES:
    !   
    REAL(KIND=4) :: HH, MM, SS
    REAL(KIND=8) :: A, B, C, D, DAY, E, F 
    REAL(KIND=8) :: FDAY, G, I, JD, M, Y
    
    !=================================================================
    ! CALDATE begins here!
    ! See "Practical astronomy with your calculator", Peter Duffett-
    ! Smith 1992, for an explanation of the following algorithm.
    !=================================================================
    JD = JULIANDAY + 0.5d0
    I  = INT( JD )
    F  = JD - INT( I )
    
    IF ( I > 2299160d0 ) THEN
       A = INT( ( I - 1867216.25d0 ) / 36524.25d0 )
       B = I + 1 + A - INT( A / 4 )
    ELSE
       B = I
    ENDIF
    
    C = B + 1524d0
    
    D = INT( ( C - 122.1d0 ) / 365.25d0 )
    
    E = INT( 365.25d0 * D )
    
    G = INT( ( C - E ) / 30.6001d0 )
    
    ! DAY is the day number
    DAY  = C - E + F - INT( 30.6001d0 * G ) 
    
    ! FDAY is the fractional day number
    FDAY = DAY - INT( DAY )
    
    ! M is the month number
    IF ( G < 13.5d0 ) THEN
       M = G - 1d0
    ELSE
       M = G - 13d0
    ENDIF
    
    ! Y is the year number
    IF ( M > 2.5d0 ) THEN
       Y = D - 4716d0
    ELSE
       Y = D - 4715d0
    ENDIF
    
    ! Year-month-day value
    YYYYMMDD = ( INT( Y ) * 10000 ) + ( INT( M ) * 100 ) + INT( DAY )
    
    ! Hour-minute-second value
    ! NOTE: HH, MM, SS are REAL*4 to avoid numerical roundoff errors
    HH     = REAL(FDAY * 24d0,KIND=4)
    MM     = REAL(( HH - INT( HH ) ) * 60d0,KIND=4)
    SS     = REAL(( MM - INT( MM ) ) * 60d0,KIND=4)
    HHMMSS = ( INT( HH ) * 10000 ) + ( INT( MM ) * 100 ) + INT( SS )
    
  END SUBROUTINE CALDATE
  
  FUNCTION GET_DAY_OF_YEAR(MONTH,DAY,YEAR,HOUR,MINUTE,SECOND) RESULT( DOY )
    
    integer(kind=4), intent(in)  :: MONTH
    integer(kind=4), intent(in)  :: DAY
    integer(kind=4), intent(in)  :: YEAR
    integer(kind=4), intent(in)  :: HOUR
    integer(kind=4), intent(in)  :: MINUTE
    real(kind=8),    intent(in)  :: SECOND

    !
    ! !RETURN VALUE:
    ! 
    INTEGER :: DOY   ! Astronomical Julian Date       
        
    ! Compute DOY using difference in julian day
    DOY = INT(JULDAY(MONTH,DAY,YEAR,HOUR,MINUTE,SECOND) - JULDAY(1,0,YEAR,0,0,0d0) )
    
    
  END FUNCTION GET_DAY_OF_YEAR
  
END MODULE GC_TIME_MODULE
