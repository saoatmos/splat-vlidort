MODULE GC_solar_module

  USE GC_parameters_module, ONLY: maxflambdas, maxlambdas
  USE GC_variables_module,  ONLY: messages, nmessages, hour, minute, &
                                  second, nspec, use_wavelength,     &
                                  flambdas, nflambdas, database_dir, &
                                  use_solar_photons,fwavnums, month, &
                                  day, year, lambda_resolution,      &
                                  clambdas, nclambdas, cwavnums,     &
                                  jday, jday0, rdis, julday,         &
                                  solar_spec_data, solar_cspec_data, &
                                  solar_spec_filename,               &
                                  lambda_dfw, do_hitran,             &
                                  hitran_sol_wvl, hitran_sol_spc,    &
                                  n_hitran_solar
  USE GC_error_module
  USE GC_netcdf_module,    ONLY : netcdf_handle_error
  USE GC_utilities_module, ONLY : reverse
  
  IMPLICIT NONE
  
  CONTAINS
    
    SUBROUTINE prepare_solar_spectrum(error)
      
      USE GC_time_module,      ONLY: julday
      USE GC_utilities_module, ONLY: gauss_f2c, BSPLINE
      IMPLICIT NONE
      INCLUDE 'netcdf.inc'
      
      
      LOGICAL, INTENT(INOUT) :: error
      INTEGER                :: n, rcode, vid, ncid,vdim,dimid,i,n_below,n_within,nf_low,nf_hi,hi0
      REAL(KIND=8), ALLOCATABLE, DIMENSION(:) :: tmp_wvl, tmp_spc!, flambdas_sorted, sspec_data_sorted
      INTEGER,      ALLOCATABLE, DIMENSION(:) :: r_fidx, r_idx
      CHARACTER(LEN=22), PARAMETER :: location = 'prepare_solar_spectrum'
      CHARACTER(LEN=max_ch_len) :: tmpchar
      REAL(KIND=8)              :: max_wvl,min_wvl,wav,dwvl
      
      ! ----------------
      ! Code starts here
      ! ----------------
      error = .FALSE.

      ! ------------------------------------------------------------------------
      ! Flux Factor
      !   -- Set to 1.0 if you want sun-normalized only (solar, NO thermal)
      !   -- Set to PHYSICAL UNIT W/sqm (Same as BLackBody) with Solar + Thermal
      !   -- Not used for thermal only options
      ! ------------------------------------------------------------------------
      
      ! Allocate solar arrays
      ALLOCATE( solar_spec_data(nflambdas) ) ! maxflambdas
      
      ! Open file
      rcode = nf_open(TRIM(ADJUSTL(solar_spec_filename)), NF_NOWRITE, ncid)
      CALL netcdf_handle_error(location,rcode)
      
      ! ------------------------------------------------------------------
      ! Read the lower resolution spectrum with sampling resolution
      ! appropriate for the fine grid
      ! ------------------------------------------------------------------
      
      ! Find the dimension of the spectrum
      rcode = nf_inq_varid( ncid, 'WavelengthLR', vid )
      rcode = nf_inq_vardimid(ncid,vid,dimid )
      rcode = NF_INQ_DIM(ncid,dimid,tmpchar, vdim )
      
      ! read the whole wavelength array
      ALLOCATE( tmp_wvl( 1:vdim ) )
      rcode = nf_get_var_double( ncid, vid, tmp_wvl(1:vdim) )
      
      ! Check that the high resolution solar spectra covers the spectral
      ! region of interest. If not print warning message to screen and
      ! continue
      IF (tmp_wvl(1)    .GT. flambdas(1)    .OR.  &
          tmp_wvl(vdim) .LT. flambdas(nflambdas)  ) THEN
         
         print*, '!!!Warning: Spectral window not covered by high resolution solar spectrum:'
         print 1000, '    High resolution solar spectrum:', tmp_wvl(1),' nm -',  &
              tmp_wvl(vdim),' nm'
         print 1000, '    Spectral window calculation:', flambdas(1),' nm -', &
              flambdas(vdim),' nm'
         
      1000 FORMAT (A,F9.2,A,F9.2,A)
      ENDIF
      
      ! Read spectrum [photons/cm^2/nm/s]
      ALLOCATE( tmp_spc( 1:vdim ) )
      rcode = nf_inq_varid( ncid, 'IrradianceLR', vid )
      rcode = nf_get_var_double( ncid, vid, tmp_spc(1:vdim) )
      
      ! Interpolate to fine grid 
      CALL BSPLINE(  tmp_wvl,         tmp_spc,      vdim, &
                    flambdas, solar_spec_data, nflambdas, &
                    rcode                                 )
      IF( rcode .lt. 0 ) THEN
        WRITE(*,*) location, ': BSPLINE error, errstat = ', rcode
        STOP
      ENDIF
      
      ! ------------------------------
      ! Convert from photons if needed
      ! ------------------------------
      IF( .not. use_solar_photons ) THEN  ! W / m ^2 / cm^-1
         solar_spec_data(1:nflambdas) = flambdas(1:nflambdas)*solar_spec_data(1:nflambdas) &
                                      / 5.0341174716D+18
      ENDIF
      
      ! ------------------------------
      ! Correct for sun-earth distance
      ! ------------------------------
      jday  = julday ( month, day, year, hour, minute, second)
      jday0 = julday (     1,   1, year,    0,      0,  0.0d0)
      jday  = jday - jday0
      CALL sun_earth_distance (jday, rdis)
      solar_spec_data(1:nflambdas) = solar_spec_data(1:nflambdas) / rdis / rdis
      
      ! -------------------------------
      ! Exception handling this section
      ! -------------------------------
      IF (error) CALL write_err_message ( .TRUE., messages(nmessages+1))
      
      ! -----------------------------------------------------
      ! Get solar irradiance spectrum at coarse spectral grid
      ! -----------------------------------------------------
      
      ALLOCATE( solar_cspec_data(nclambdas) ) ! maxlambdas
      nspec = 1
      
      IF (lambda_resolution > 0.0) THEN
         IF (use_wavelength) THEN
            CALL gauss_f2c (flambdas(1:nflambdas), solar_spec_data(1:nflambdas), &
                            nflambdas, nspec, lambda_resolution, clambdas,     &
                            solar_cspec_data(1:nclambdas), nclambdas)
            
         ELSE
            CALL gauss_f2c (fwavnums(1:nflambdas), solar_spec_data(1:nflambdas), &
                            nflambdas, nspec, lambda_resolution, cwavnums,     &
                            solar_cspec_data(1:nclambdas), nclambdas)
         END IF
         
      ELSE
         
         ! ---------------------------------------
         ! Get spectrum on coarse wavelength grid
         ! ---------------------------------------
         ! Interpolate to fine grid via linear interpolation
         CALL BSPLINE(  tmp_wvl,           tmp_spc,      vdim, &
                        clambdas, solar_cspec_data, nclambdas, &
                        rcode                                  )
         IF( rcode .lt. 0 ) THEN
           WRITE(*,*) location, ': BSPLINE error, errstat = ', rcode
           STOP
         ENDIF
                
         ! ------------------------------
         ! Convert from photons if needed
         ! ------------------------------
         IF ( .not. use_solar_photons ) THEN  ! W / m ^2 / cm^-1
           solar_cspec_data(1:nclambdas) = clambdas(1:nclambdas)*solar_cspec_data(1:nclambdas) &
                                         / 5.0341174716D+18
         END IF
        
         ! Sun earth distance correction
         solar_cspec_data(1:nclambdas) = solar_spec_data(1:nclambdas)  / rdis / rdis
         
      END IF
      
      ! Deallocate the temporary arrays
      DEALLOCATE( tmp_wvl )
      DEALLOCATE( tmp_spc )
      
      !----------------------------------------------------------------------------------------
      ! A note when using HITRAN
      ! ------------------------
      ! 
      ! Load a high-resolution solar reference at 0.001 nm, only for performing solar-I0 effect
      ! when convolving with cross sections from hitran line-by-line calculations, append it
      ! with solar_spec_data if it does not cover adequate spectral range
      ! ---------------------------------------------------------------------------------------
      IF( do_hitran ) THEN
        
        ! Find the dimension of the spectrum
        rcode = nf_inq_varid( ncid, 'Wavelength', vid )
        rcode = nf_inq_vardimid(ncid,vid,dimid )
        rcode = NF_INQ_DIM(ncid,dimid,tmpchar, vdim )
        
        ! read the whole wavelength array
        ALLOCATE( tmp_wvl( 1:vdim ) )
        rcode = nf_get_var_double( ncid, vid, tmp_wvl(1:vdim) )
        
        ! Read spectrum
        ALLOCATE( tmp_spc( 1:vdim ) )
        rcode = nf_inq_varid( ncid, 'Irradiance', vid )
        rcode = nf_get_var_double( ncid, vid, tmp_spc(1:vdim) )
        
        ! Load wavelengths with a 40nm buffer
        min_wvl = flambdas(1)-40.0
        max_wvl = flambdas(nflambdas)+40.0
        
        ! Determine indices within range
        n_below = 0
        n_within= 0
        DO i=1,vdim
          
          IF( tmp_wvl(i) .LT. min_wvl ) THEN
            n_below = n_below + 1
          ELSEIF( tmp_wvl(i) .GT. max_wvl ) THEN
            EXIT
          ELSE
            n_within = n_within + 1
          ENDIF
          
        ENDDO
        
        ! Deallocate arrays if needed
        IF( ALLOCATED( hitran_sol_wvl ) ) DEALLOCATE( hitran_sol_wvl )
        IF( ALLOCATED( hitran_sol_spc ) ) DEALLOCATE( hitran_sol_spc )
        
        ! Allocate output arrays
        n_hitran_solar = n_within
        ALLOCATE( hitran_sol_wvl(n_hitran_solar) )
        ALLOCATE( hitran_sol_spc(n_hitran_solar) )
        
        hitran_sol_wvl = tmp_wvl(n_below+1:n_below+n_within)
        hitran_sol_spc = tmp_spc(n_below+1:n_below+n_within)
        
        ! Check bounds
        dwvl = 2.0*( hitran_sol_wvl(2)-hitran_sol_wvl(1) )
        IF( MINVAL(hitran_sol_wvl) > min_wvl + dwvl ) THEN
          print*,'HR solar spectrum does not cover window lower bound'
          STOP
        ENDIF
        IF( MAXVAL(hitran_sol_wvl) < max_wvl - dwvl ) THEN
          print*,'HR solar spectrum does not cover window upper bound'
          STOP
        ENDIF
        
      ENDIF
      
      ! Close file
      rcode = NF_CLOSE(ncid)
      CALL netcdf_handle_error(location,rcode)
      
      ! Free memory
      IF( ALLOCATED(tmp_wvl) ) DEALLOCATE( tmp_wvl )
      IF( ALLOCATED(tmp_spc) ) DEALLOCATE( tmp_spc )
      
    END SUBROUTINE prepare_solar_spectrum
    
    ! Calculate sun-earth distance correction factor ratio between daily value 
    ! of the sun-earth distance and its mean yearly value (1 AU)
    subroutine sun_earth_distance (jday, rdis)
      
      implicit none
      !=================
      ! input variables
      !=================
      real(kind=8), intent (in)  :: jday
      
      ! ================
      ! output variables
      ! ================
      real(kind=8), intent (out) :: rdis
      
      !local variables
      real(KIND=8), PARAMETER :: pi = 3.14159265358979d0
      real(kind=8)            :: phi
      
      phi = 2.0 * pi * (jday - 1.d0) / 365.0
      rdis = sqrt(1.0d0/(1.00011d0+0.034221d0 * cos(phi) + 0.00128d0 * sin(phi) + &
           0.000719d0 * cos(2.d0*phi) + 0.000077d0 * sin(2.0 * phi)))
      return
  
    end subroutine sun_earth_distance

    !xliu, 5/1/2013, remove nr=49951 to nwmax=80093 
    ! as one of the solar irradiance has 80093 lines
    ! nr will be determined during the reading process
    subroutine solarspec_prep_newkur &
         ( filename, maxwavnums, nwavnums, wavnums,  & ! input
         solar_spec_data, message, fail )              ! output
      
      !  The reader to read in solar spectrum data in a .dat file
      
      !  input filename
      
      character(len=*),  intent(in)  :: filename
      
      !  other inputs
      
      !  Dimensioning
      
      integer, intent(in)       :: maxwavnums
      
      !  wave numbers
      
      integer, intent(in)       :: nwavnums
      real(kind=8), intent(in)  :: wavnums ( maxwavnums )
      
      !  Main output
      
      real(kind=8), dimension(maxwavnums), intent(out) :: solar_spec_data
      
      !  Exception handling
      
      logical,       intent(INOUT) :: fail
      character*(*), intent(INOUT) :: message
      
      !  Local variables
      integer, parameter             :: nwmax = 2000000
      integer                        :: i, nr, io
      character(LEN=10)              :: charnw
      real(kind=8), dimension(nwmax) :: solar_spec_raw_data   
      real(kind=8), dimension(nwmax) :: wn_raw_data           
      real(kind=8)                   :: grad, wav
      integer                        :: m1, m2, n    
      
      !  initialize
      
      fail    = .false.
      message = ' '
      !nr = 49951  !xliu
      solar_spec_data(:) = 0.d0
      
      !    np = 1
      !    do while (filename(np:np).ne. ' ')
      !      np = np + 1 
      !    enddo
      !    np = np - 1
      ! 
      ! !  Open and read file
      !    open(1,file=filename(1:np),err=90, status='old')
      !    
      ! New way
      open(1,file=trim(adjustl(filename)),err=90,status='old')
      
      do i = 1,2
         read(1,*)
      enddo
      
      ! xliu, disable the use of fixed nr here as file may have different # of wavelengths
      !   do i = 1, nr
      !     read(1,*) wn_raw_data(i),solar_spec_raw_data(i)
      !   enddof
      io = 0
      i = 1
      do 
         read(1, *, iostat=io) wn_raw_data(i),solar_spec_raw_data(i)
         if (io .lt. 0) then
            exit
         else if (io .gt. 0) then
            go to 90
         else
            i = i + 1
         endif
      enddo
      nr = i - 1
      close(1)
      
      if (nr .gt. nwmax) then 
         fail = .true.
         write(charnw, '(I10)') nr
         message = 'Increase nwmax in solarspec_prep_newkur to ' // charnw
         return
      endif
      !xliu */
      
      ! Check that the high resolution solar spectra covers the spectral
      ! region of interest. If not print warning message to screen and
      ! continue
      IF (wn_raw_data(1) .GT. wavnums(nwavnums) .OR. &
           wn_raw_data(nr) .LT. wavnums(1) ) THEN
         
         print*, '!!!Warning: Spectral window not covered by high resolution solar spectrum:'
         print 1000, '    High resolution solar spectrum:', wn_raw_data(1),' nm -',  &
              wn_raw_data(nr),' nm'
         print 1000, '    Spectral window calculation:', wavnums(nwavnums),' nm -', &
              wavnums(1),' nm'
         
      1000 FORMAT (A,F9.2,A,F9.2,A)
      ENDIF
      
      !  Wavelength assignments (linear interpolation)
      do n = 1, nwavnums
         wav = wavnums(n)  
         if ( wav.le.wn_raw_data(1) ) then
            solar_spec_data(n) = solar_spec_raw_data(1)
         else if ( wav.ge.wn_raw_data(nr) ) then
            solar_spec_data(n) = solar_spec_raw_data(nr)
         else
            m2 = 1
            do while (wav .gt. wn_raw_data(m2) )
               m2 = m2 + 1
            enddo
            m1 = m2 - 1
            grad = ( solar_spec_raw_data(m2) - solar_spec_raw_data(m1) ) / &
                 ( wn_raw_data(m2) - wn_raw_data(m1) )
            solar_spec_data(n) = solar_spec_raw_data(m1) + grad * ( wav - wn_raw_data(m1) )
         endif
      enddo
      
      ! Convert it to W/ m^2 / cm^-1, added by xliu
      solar_spec_data(1:nwavnums) = solar_spec_data(1:nwavnums) * 1.0d4
      
      return
      
      ! error return
      
90    continue
      fail = .true.
      message = 'Open/read failure for solarspec file = '//filename(1:LEN(filename))
      return
      
    end subroutine solarspec_prep_newkur


END MODULE GC_solar_module
