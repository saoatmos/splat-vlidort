!------------------------------------------------------------------------------
!          Harvard University Atmospheric Chemistry Modeling Group            !
!------------------------------------------------------------------------------
!BOP
!
! !MODULE: GC_parameters_module.f90
!
! !DESCRIPTION: This module contains dimensioning parameters,
! angle conversion and PI definition.
!\\
!\\
! !INTERFACE: 
!
MODULE GC_parameters_module
!
  IMPLICIT NONE
!
! !PUBLIC DATA MEMBERS:
!
!  Dimensioning
!  ------------
! ----------------------------------------------------------------
! Dimensions with 'GC_' prefix, distinguish from VLIDORT variables
! ----------------------------------------------------------------
  INTEGER, PARAMETER :: GC_maxlayers      = 100
  INTEGER, PARAMETER :: GC_maxgeometries  = 2
  INTEGER, PARAMETER :: GC_maxuserlevels  = 2
  
! -------------------------------
! Diagnostics
! -------------------------------
  INTEGER, PARAMETER :: n_total_diag = 41! Number of total diagnostics
  
! ----------------------------  
! Wavelengths, gases, messages
! ----------------------------
  INTEGER, PARAMETER :: maxgases      = 12
  INTEGER, PARAMETER :: maxaer        = 6
  INTEGER, PARAMETER :: maxcld        = 3
  
  ! Option for effective cross section
!   INTEGER, PARAMETER :: maxflambdas   = 100001
!   INTEGER, PARAMETER :: maxlambdas    = 10001
  
  ! Options for line-by-line
  INTEGER, PARAMETER :: maxflambdas   = 1000001
  INTEGER, PARAMETER :: maxlambdas    = 1000001
  
  INTEGER, PARAMETER :: maxmessages   = 100
  INTEGER, PARAMETER :: maxmoms       = 1000 !32
  INTEGER, PARAMETER :: maxgksec      = 6, maxgkmatc = 8
  INTEGER, PARAMETER :: maxscatter    = 3  ! Molecules, aerosols, clouds
  INTEGER, PARAMETER :: malbspc       = 5  ! maximum number of albedo spectra/EOF
  INTEGER, DIMENSION(maxgkmatc), PARAMETER :: &
           greekmat_idxs = (/1, 2, 5, 6, 11, 12, 15, 16/), &
           phasmoms_idxs = (/1, 5, 5, 2, 3, 6, 6, 4/)

! --------------
! Some constants
! --------------
  REAL(KIND=8), PARAMETER :: pi      = 3.14159265358979d0
  REAL(KIND=8), PARAMETER :: deg2rad = pi / 180.d0,    &
                             rad2deg = 180.d0 / pi
  INTEGER,      PARAMETER :: max_ch_len = 512

! ------------------------------------
! File units for different input files
! ------------------------------------
  INTEGER, PARAMETER :: ctrunit = 11
  INTEGER, PARAMETER :: aerunit = 12
  INTEGER, PARAMETER :: cldunit = 13
  INTEGER, PARAMETER :: errunit = 15
  INTEGER, PARAMETER :: eofunit = 16
  INTEGER, PARAMETER :: sifunit = 18
  INTEGER, PARAMETER :: lckunit = 17

! ------------------
! Profile PARAMETERS
! ------------------
  INTEGER, PARAMETER         :: pmaxl = GC_maxlayers+1, pmaxc = 32
  

! SCIAMACHY EOFs
  INTEGER, PARAMETER :: scia_nwvl = 27
  INTEGER, PARAMETER :: scia_nsub = 16 ! wavelengths > 400 nm

! Short names for brdf kernels (used for output)
CHARACTER(LEN=5), DIMENSION(16), PARAMETER ::  short_kern_name =(/  &
              'isotr', 'rthin', 'rthck', 'lsprs', 'ldens', 'hapke', &
              'rjean', 'rhman', 'cxmnk', 'gcmnk', 'gcmcr', 'bpdfv', &
              'bpdfs', 'bpdfn', 'ncmgl', 'hpke5'/)

! Full BRDF names checked in VLIDORT supplement
CHARACTER (LEN=10), DIMENSION(16), PARAMETER :: BRDF_CHECK_NAMES = (/ &
  'Lambertian', 'Ross-thin ', 'Ross-thick', 'Li-sparse ', 'Li-dense  ', 'Hapke     ', &
  'Roujean   ', 'Rahman    ', 'Cox-Munk  ', 'GissCoxMnk', 'GCMcomplex', 'BPDF-Vegn ', &
  'BPDF-Soil ', 'BPDF-NDVI ', 'NewCMGlint', 'Hapke5Par '/)

! Number of parameters for each kernel
INTEGER, DIMENSION(16), PARAMETER :: vl_brdf_npar = &
                                    (/0,0,0,2,2,3,0,3,2,2,2,2,2,2,3,5/)

!
! !REVISION HISTORY:
!  April 2013 - G. Gonzalez Abad - Initial Version
!
!EOP
!------------------------------------------------------------------------------
!EOC

END MODULE GC_parameters_module
