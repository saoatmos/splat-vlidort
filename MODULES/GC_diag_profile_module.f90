!------------------------------------------------------------------------------
!          Harvard University Atmospheric Chemistry Modeling Group            !
!------------------------------------------------------------------------------
!BOP
!
! !MODULE: GC_diag_profile_module.f90
!
! !DESCRIPTION: This module contains routines for writing profile diagnostics
!\\
!\\
! !INTERFACE: 
!
MODULE GC_diag_profile_module
    
    USE GC_parameters_module
    USE GC_netcdf_module,      ONLY : create_ncvar, write_ncvar, &
                                      match_names_in_dimlist,    &
                                      nc_fld_1d, nc_fld_2d,      &
                                      nc_fld_3d, nc_fld_4d
    
    IMPLICIT NONE
    INCLUDE 'netcdf.inc'
    
    CONTAINS
    
    SUBROUTINE do_profile_diag( NCID, ACTION, DO_XY, DIAG_NUM )
      
      USE GC_variables_module, ONLY : heights, pressures, temperatures,   &
                                      aircolumns, taer_profile,           &
                                      aer_opdeps, aer_ssalbs, cpix_cfrac, &
                                      ad09, ad10, ad11, ad14, ad15,       &
                                      write_totaod_refwvl
      
      ! ---------------------------
      ! Subroutine arguments
      ! ---------------------------
      INTEGER, INTENT(IN) :: NCID
      INTEGER, INTENT(IN) :: ACTION
      LOGICAL, INTENT(IN) :: DO_XY
      INTEGER, INTENT(IN) :: DIAG_NUM
      
      ! ---------------
      ! Local variables
      ! ---------------
      INTEGER                                 :: VAR_ID
      LOGICAL                                 :: error, dummy_err
      INTEGER, DIMENSION(1)                   :: size_1d, tmp_1d
      CHARACTER(LEN=max_ch_len), DIMENSION(1) :: dim_1d
      INTEGER, DIMENSION(2)                   :: size_2d, tmp_2d
      CHARACTER(LEN=max_ch_len), DIMENSION(2) :: dim_2d
      REAL(KIND=8)                            :: cftot
      
      ! =====================================================
      ! do_profile_diag starts here
      ! =====================================================
      
      ! Initialize error
      error = .FALSE.
      
      SELECTCASE( DIAG_NUM )
      
        ! ======================================================
        ! (4) Altitude Profile
        ! ======================================================
        CASE( 4 )
        
          ! The dimension of the output
          dim_1d(1) = 'nlevel'
          
          ! Get dimension
          CALL match_names_in_dimlist( dim_1d, 1, tmp_1d, size_1d, dummy_err )
          
          CALL nc_fld_1d( dim_1d, size_1d(1), heights(0:size_1d(1)-1), 'zs', &
                          NCID,   ACTION,     DO_XY                          )
        
        ! ======================================================
        ! (5) Pressure Profile
        ! ======================================================
        CASE( 5 )
          
          ! The dimension of the output
          dim_1d(1) = 'nlevel'
          
          ! Get dimension
          CALL match_names_in_dimlist( dim_1d, 1, tmp_1d, size_1d, dummy_err )
          
          CALL nc_fld_1d( dim_1d, size_1d(1), pressures(0:size_1d(1)-1), 'ps', &
                          NCID,   ACTION,     DO_XY                            )
        
        ! ======================================================
        ! (6) Temperature Profile
        ! ======================================================
        CASE( 6 )
          
          ! The dimension of the output
          dim_1d(1) = 'nlevel'
          
          ! Get dimension
          CALL match_names_in_dimlist( dim_1d, 1, tmp_1d, size_1d, dummy_err )
          
          CALL nc_fld_1d( dim_1d, size_1d(1), temperatures(0:size_1d(1)-1), 'ts', &
                          NCID,   ACTION,     DO_XY                               )
        
        ! ======================================================
        ! (7) Air Column
        ! ======================================================
        CASE( 7 )
        
          ! The dimension of the output
          dim_1d(1) = 'nlayer'
          
          ! Get dimension
          CALL match_names_in_dimlist( dim_1d, 1, tmp_1d, size_1d, dummy_err )
          
          CALL nc_fld_1d( dim_1d, size_1d(1), aircolumns(1:size_1d(1)), 'aircol', &
                          NCID,   ACTION,     DO_XY                               )
        
        ! ======================================================
        ! (8) AOD at reference wavelength
        ! ======================================================
        CASE( 8 )
          
          CALL diag_08( NCID, ACTION, DO_XY )
          
        ! ======================================================
        ! (9) COD at reference wavelength
        ! ======================================================
        CASE( 9 )
          
          ! The dimension of the output
          dim_1d(1) = 'nlayer'
          
          ! Normalize output by cloudy pixel fraction
          cftot = SUM( cpix_cfrac )
          IF( cftot > 0.0 ) ad09(:) = ad09(:) / cftot
          
          ! Get dimension
          CALL match_names_in_dimlist( dim_1d, 1, tmp_1d, size_1d, dummy_err )
          
          CALL nc_fld_1d( dim_1d, size_1d(1), ad09(1:size_1d(1)), 'cods0',  &
                              NCID,   ACTION,              DO_XY             )
        ! ======================================================
        ! (10) Total optical depth profile
        ! ======================================================
        CASE( 10 )
          
          ! The dimension of the output
          dim_2d(1) = 'nw' ; dim_2d(2) = 'nlayer'
          
          ! Get dimension
          CALL match_names_in_dimlist( dim_2d, 2, tmp_2d, size_2d, dummy_err )
          
          CALL nc_fld_2d( dim_2d, size_2d(1), size_2d(2),         &
                          ad10(1:size_2d(1),1:size_2d(2)), 'ods', &
                          NCID, ACTION, DO_XY                     )
        ! ======================================================
        ! (11) Total SSA Profile
        ! ======================================================
        CASE( 11 )
          
          ! The dimension of the output
          dim_2d(1) = 'nw' ; dim_2d(2) = 'nlayer'
          
          ! Get dimension
          CALL match_names_in_dimlist( dim_2d, 2, tmp_2d, size_2d, dummy_err )
          
          CALL nc_fld_2d( dim_2d, size_2d(1), size_2d(2),          &
                          ad11(1:size_2d(1),1:size_2d(2)), 'ssas', &
                          NCID, ACTION, DO_XY                      )
        
        ! ======================================================
        ! (12) Total AOD Profile
        ! ======================================================
        CASE( 12 )
          
          ! The dimension of the output
          dim_2d(1) = 'nw' ; dim_2d(2) = 'nlayer'
          
          ! Get dimension
          CALL match_names_in_dimlist( dim_2d, 2, tmp_2d, size_2d, dummy_err )
          
          CALL nc_fld_2d( dim_2d, size_2d(1), size_2d(2),                &
                          aer_opdeps(1:size_2d(1),1:size_2d(2)), 'aods', &
                          NCID, ACTION, DO_XY                            )
        ! ======================================================
        ! (13) Total Aerosol SSA Profile
        ! ======================================================
        CASE( 13 )
          
          ! The dimension of the output
          dim_2d(1) = 'nw' ; dim_2d(2) = 'nlayer'
          
          ! Get dimension
          CALL match_names_in_dimlist( dim_2d, 2, tmp_2d, size_2d, dummy_err )
          
          CALL nc_fld_2d( dim_2d, size_2d(1), size_2d(2),                 &
                          aer_ssalbs(1:size_2d(1),1:size_2d(2)), 'assas', &
                          NCID, ACTION, DO_XY                             )
        ! ======================================================
        ! (14) Total COD Profile
        ! ======================================================
        CASE( 14 )
          
          ! The dimension of the output
          dim_2d(1) = 'nw' ; dim_2d(2) = 'nlayer'
          
          ! Normalize output by cloudy pixel fraction
          cftot = SUM( cpix_cfrac )
          IF( cftot > 0.0 ) ad14(:,:) = ad14(:,:) / cftot
          
          ! Get dimension
          CALL match_names_in_dimlist( dim_2d, 2, tmp_2d, size_2d, dummy_err )
          
          CALL nc_fld_2d( dim_2d, size_2d(1), size_2d(2),           &
                          ad14(1:size_2d(1),1:size_2d(2)), 'cods',  &
                          NCID, ACTION, DO_XY                       )
        ! ======================================================
        ! (15) Total Cloud SSA Profile
        ! ======================================================
        CASE( 15 )
          
          ! The dimension of the output
          dim_2d(1) = 'nw' ; dim_2d(2) = 'nlayer'
          
          ! Normalize output by cloudy pixel fraction
          cftot = SUM( cpix_cfrac )
          IF( cftot > 0.0 ) ad15(:,:) = ad15(:,:) / cftot
          
          ! Get dimension
          CALL match_names_in_dimlist( dim_2d, 2, tmp_2d, size_2d, dummy_err )
          
          CALL nc_fld_2d( dim_2d, size_2d(1), size_2d(2),           &
                          ad15(1:size_2d(1),1:size_2d(2)), 'cssas', &
                          NCID, ACTION, DO_XY                       )
                          
        ! ======================================================
        ! (16) Gas Partial Columns
        ! ======================================================
        CASE( 16 )
          
          CALL diag_16( NCID, ACTION, DO_XY )
        
        ! ======================================================
        ! (17) Empty
        ! ======================================================
        CASE( 17 )
          print*,'DIAG 17 is currently empty'
        
        ! ======================================================
        ! (18) Gas Cross Sections
        ! ======================================================
        CASE( 18 )
          
          CALL diag_18( NCID, ACTION, DO_XY )
          
        CASE DEFAULT
          
          print*,'The selected diagnostic is not in the profile diags:', diag_num
          STOP
          
      ENDSELECT
      
    END SUBROUTINE do_profile_diag
    
    SUBROUTINE do_profile_jacobians( NCID, ACTION, DO_XY, DIAG_NUM )
      
      USE GC_variables_module, ONLY : GC_temperature_Jacobians, didx, &
                                      GC_sfcprs_Jacobians,            &
                                      GC_sfcprs_QJacobians,           &
                                      GC_sfcprs_UJacobians,           &
                                      do_QU_Jacobians, do_stokes_ad31,&
                                      GC_rrs_Jacobian,                &
                                      do_airdens_jac,                 &
                                      GC_airdens_Jacobians,           &
                                      GC_airdens_QJacobians,          &
                                      GC_airdens_UJacobians
      
      ! ---------------------------
      ! Subroutine arguments
      ! ---------------------------
      INTEGER, INTENT(IN) :: NCID
      INTEGER, INTENT(IN) :: ACTION
      LOGICAL, INTENT(IN) :: DO_XY
      INTEGER, INTENT(IN) :: DIAG_NUM
      
      ! ---------------
      ! Local variables
      ! ---------------
      INTEGER                                 :: VAR_ID
      LOGICAL                                 :: error, dummy_err
      INTEGER, DIMENSION(1)                   :: size_1d, tmp_1d
      CHARACTER(LEN=max_ch_len), DIMENSION(1) :: dim_1d
      INTEGER, DIMENSION(2)                   :: size_2d, tmp_2d
      CHARACTER(LEN=max_ch_len), DIMENSION(2) :: dim_2d
      
      INTEGER, DIMENSION(3)                   :: size_3d, tmp_3d
      CHARACTER(LEN=max_ch_len), DIMENSION(3) :: dim_3d
      INTEGER, DIMENSION(4)                   :: size_4d, tmp_4d
      CHARACTER(LEN=max_ch_len), DIMENSION(4) :: dim_4d
      
      ! =====================================================
      ! do_profile_jacobians starts here
      ! =====================================================
      
      SELECTCASE( DIAG_NUM )
    
        ! ======================================================
        ! Temperature profile Jacobian
        ! ======================================================
        CASE( 30 )
          
          ! Dimensions of output
          dim_4d(1) = 'nw' ; dim_4d(2) = 'nlayer' ; dim_4d(3) = 'ngeom' ; dim_4d(4) = 'noutputlevel'
          
          ! Get dimensions
          CALL match_names_in_dimlist( dim_4d, 4, tmp_4d, size_4d, dummy_err )
          
          CALL nc_fld_4d( dim_4d, size_4d(1), size_4d(2), size_4d(3), size_4d(4),                      &
                    GC_temperature_Jacobians(1:size_4d(1),1:size_4d(2),1:size_4d(3),1:size_4d(4),didx),&
                    't_jac', NCID,   ACTION,     DO_XY                                                 )
        
        ! ======================================================
        ! Surface Pressure Jacobian
        ! ======================================================
        CASE( 31 )
          
          ! Dimensions of output
          dim_3d(1) = 'nw' ; dim_3d(2) = 'ngeom' ; dim_3d(3) = 'noutputlevel'
          
          ! Get dimensions
          CALL match_names_in_dimlist( dim_3d, 3, tmp_3d, size_3d, dummy_err )
          
          ! Surface pressure jacobian
          CALL nc_fld_3d( dim_3d, size_3d(1), size_3d(2), size_3d(3),                       &
                          GC_sfcprs_Jacobians(1:size_3d(1),1:size_3d(2),1:size_3d(3),didx), &
                          'sfcprs_jac', NCID,   ACTION,     DO_XY                           )
          
          
          ! Q+U
          IF( do_QU_Jacobians .AND. do_stokes_ad31 ) THEN
            CALL nc_fld_3d( dim_3d, size_3d(1), size_3d(2), size_3d(3),                        &
                            GC_sfcprs_QJacobians(1:size_3d(1),1:size_3d(2),1:size_3d(3),didx), &
                            'sfcprs_qjac', NCID,   ACTION,     DO_XY                           )
            CALL nc_fld_3d( dim_3d, size_3d(1), size_3d(2), size_3d(3),                        &
                            GC_sfcprs_UJacobians(1:size_3d(1),1:size_3d(2),1:size_3d(3),didx), &
                            'sfcprs_ujac', NCID,   ACTION,     DO_XY                           )
          ENDIF

          ! Check for air density Jacobians
          IF( do_airdens_jac ) THEN

            ! Dimensions
            dim_4d(1) = 'nw' ; dim_4d(2) = 'nlayer' ; dim_4d(3) = 'ngeom' ; dim_4d(4) = 'noutputlevel'

            ! Get dimensions
            CALL match_names_in_dimlist( dim_4d, 4, tmp_4d, size_4d, dummy_err )

            ! Create/write field
            CALL nc_fld_4d( dim_4d, size_4d(1), size_4d(2), size_4d(3), size_4d(4),                 &
                    GC_airdens_Jacobians(1:size_4d(1),1:size_4d(2),1:size_4d(3),1:size_4d(4),didx), &
                    'airdens_jac', NCID,   ACTION,     DO_XY                                        )

            ! Check for Q/U 
            IF( do_QU_Jacobians .AND. do_stokes_ad31 ) THEN
              CALL nc_fld_4d( dim_4d, size_4d(1), size_4d(2), size_4d(3), size_4d(4),                &
                    GC_airdens_QJacobians(1:size_4d(1),1:size_4d(2),1:size_4d(3),1:size_4d(4),didx), &
                    'airdens_qjac', NCID,   ACTION,     DO_XY                                        )
              CALL nc_fld_4d( dim_4d, size_4d(1), size_4d(2), size_4d(3), size_4d(4),                &
                    GC_airdens_UJacobians(1:size_4d(1),1:size_4d(2),1:size_4d(3),1:size_4d(4),didx), &
                    'airdens_ujac', NCID,   ACTION,     DO_XY                                        )
            ENDIF
          ENDIF
        ! ======================================================
        ! Trace Gas Jacobian
        ! ======================================================
        CASE( 32 )
          
          CALL diag_32( NCID, ACTION, DO_XY )
        
        ! ======================================================
        ! RRS Pseudoabsorption Jacobian
        ! ======================================================
        CASE( 33 )
          
          ! The dimension of the output
          dim_4d(1) = 'nw' ; dim_4d(2) = 'nlayer' ; dim_4d(3) = 'ngeom' ; dim_4d(4) = 'noutputlevel'
          
          ! Get dimension
          CALL match_names_in_dimlist( dim_4d, 4, tmp_4d, size_4d, dummy_err )
          
          CALL nc_fld_4d( dim_4d, size_4d(1), size_4d(2), size_4d(3), size_4d(4),                    &
                          GC_rrs_Jacobian(1:size_4d(1),1:size_4d(2),1:size_4d(3),1:size_4d(4),didx), &
                          'rrswf_jac',  NCID, ACTION, DO_XY                                          )
          
        CASE DEFAULT
        
          print*,'The selected diagnostic is not in the profile jacobian  diags:', diag_num
          STOP
            
        ENDSELECT
        
      
    END SUBROUTINE do_profile_jacobians
    
    SUBROUTINE archive_prof_pixavg_diags( iw, pix_frac )
      
      USE GC_variables_module, ONLY : tcld_profile,            &
                                      opdeps,       ssalbs,    &
                                      cld_opdeps,   cld_ssalbs,&
                                      ad09,                    &
                                      ad10,         ad11,      &
                                      ad14,         ad15,      &
                                      do_diag
      
      ! ---------------------------
      ! Subroutine arguments
      ! ---------------------------
      INTEGER,      INTENT(IN) :: iw
      REAL(KIND=8), INTENT(IN) :: pix_frac
      
      ! ---------------
      ! Local variables
      ! ---------------
      
      ! =====================================================
      ! archive_cldtot_diag starts here
      ! =====================================================
      
      ! Archive the total cloud profile on first wavelength 
      IF( iw == 1 .and. do_diag(9) ) ad09(:) = ad09(:) + pix_frac*tcld_profile(:)
      
      ! Total optical depth
      IF( do_diag(10) ) ad10(iw,:) = ad10(iw,:) + pix_frac*opdeps(iw,:)
      
      ! Total single scatter albedo
      IF( do_diag(11) ) ad11(iw,:) = ad11(iw,:) + pix_frac*ssalbs(iw,:)
      
      ! Cloud optical depth
      IF( do_diag(14) ) ad14(iw,:) = ad14(iw,:) + pix_frac*cld_opdeps(iw,:)
      
      ! Cloud SSA
      IF( do_diag(15) ) ad15(iw,:) = ad15(iw,:) + pix_frac*cld_ssalbs(iw,:)
      
    END SUBROUTINE archive_prof_pixavg_diags
    
    SUBROUTINE archive_profile_diag( iw, dir )
    
      USE GC_variables_module, ONLY : do_T_Jacobians, twfidx, VLIDORT_FixIn, &
                                      VLIDORT_Out, VLIDORT_LinOut,           &
                                      GC_temperature_Jacobians,              &
                                      mid_temperatures,                      &
                                      do_normalized_WFoutput, do_stokes_ad31,&
                                      do_oco2_sfcprs, sfcprswfidx,pressures, &
                                      do_QU_jacobians, do_sfcprs_jacobians,  &
                                      GC_sfcprs_Jacobians,                   &
                                      GC_sfcprs_QJacobians,                  &
                                      GC_sfcprs_UJacobians, gasabs,          &
                                      total_gasabs, gas_partialcolumns,      &
                                      do_stokes_ad32, GC_Tracegas_Jacobians, &
                                      GC_Tracegas_QJacobians, ngases,        &
                                      GC_Tracegas_UJacobians, gaswfidx,      &
                                      do_jacobians, Diag32_InpOpt,           &
                                      do_gas_jacobians, do_rrs_abs,          &
                                      do_raman_jacobian, GC_rrs_Jacobian,    &
                                      aircolumns, rrswfidx, total_dgasabsdT, &
                                      GC_airdens_Jacobians,do_airdens_jac,   &
                                      GC_airdens_QJacobians,                 &
                                      GC_airdens_UJacobians
                                      
      
      ! --------------------
      ! Subroutine arguments
      ! --------------------
      INTEGER, INTENT(IN) :: iw, dir
      
      ! ---------------
      ! Local variables
      ! ---------------
      INTEGER :: IB, UM, UA, V, q, g, n, il, z0,zf, l, gidx
      
      REAL(KIND=8) :: deltp, ratio
      
      ! =====================================================
      ! archive_surface_diag starts here
      ! =====================================================
      
      ! ==========================================================================================
      ! DIAG 30 - Temperature Jacobian
      ! ==========================================================================================
      
      IF ( do_T_Jacobians ) THEN
        
        q = gaswfidx
        DO n = 1, VLIDORT_FixIn%Cont%TS_NLAYERS
        DO v = 1, VLIDORT_Out%Main%TS_N_GEOMETRIES
          
          IF( total_gasabs(n) .EQ. 0.0 ) THEN
            GC_Temperature_Jacobians(iw,n,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,dir) = 0.0d0
          ELSE
            IF ( do_normalized_WFoutput ) THEN !  Normalized output
              ratio = total_dgasabsdT(n) / total_gasabs(n) * mid_temperatures(n)
            ELSE !  Non-normalized output
              ratio = total_dgasabsdT(n) / total_gasabs(n)
            ENDIF
          ENDIF
          GC_Temperature_Jacobians(iw,n,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,dir) = &
            VLIDORT_LinOut%Prof%TS_PROFILEWF(q,n,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,1,dir)*ratio
        ENDDO
        ENDDO
        
      ENDIF
      
      ! ------------------------------
      ! Save surface pressure Jacobian
      ! ------------------------------ 
      IF ( do_sfcprs_Jacobians ) THEN
        
        ! Jacobian index
        q = sfcprswfidx
        
        ! I
        DO v = 1, VLIDORT_Out%Main%TS_N_GEOMETRIES
          
          ! Zero value
          GC_sfcprs_Jacobians(iw,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,dir) = 0.0
          
          IF( do_oco2_sfcprs ) THEN
            
            ! Normalization based on total pressure
            deltp = pressures(VLIDORT_FixIn%Cont%TS_NLAYERS)
            
            ! Sum layers
            z0 = 1 ; zf = VLIDORT_FixIn%Cont%TS_NLAYERS
          ELSE
            
            ! Pressure diff of bottom layer
            deltp = pressures(VLIDORT_FixIn%Cont%TS_NLAYERS) - &
                    pressures(VLIDORT_FixIn%Cont%TS_NLAYERS-1)
            
            ! Just add last layer
            z0 = VLIDORT_FixIn%Cont%TS_NLAYERS ; zf = z0
            
          ENDIF
          
          DO l=z0,zf
            
            ! I
            IF ( do_normalized_WFoutput ) THEN         !  Normalized output
              GC_sfcprs_Jacobians(iw,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,dir) =   &
                GC_sfcprs_Jacobians(iw,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,dir) + &
                VLIDORT_LINOUT%PROF%TS_PROFILEWF(q,l,                                    &
                1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,1,dir) 
              IF( do_airdens_jac ) THEN
                GC_airdens_Jacobians(iw,l,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,dir) =  &
                  VLIDORT_LINOUT%PROF%TS_PROFILEWF(q,l,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,1,dir) 
              ENDIF
            ELSE                                       !  Non-normalized output
              GC_sfcprs_Jacobians(iw,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,dir) =   &
                GC_sfcprs_Jacobians(iw,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,dir) + &
                VLIDORT_LINOUT%PROF%TS_PROFILEWF(q,l,                                    &
                1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,1,dir) / deltp
              IF( do_airdens_jac ) THEN
                GC_airdens_Jacobians(iw,l,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,dir) =  &
                  VLIDORT_LINOUT%PROF%TS_PROFILEWF(q,l,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,1,dir)/deltp
              ENDIF
            ENDIF
            
            ! Q+U
            IF( do_QU_Jacobians .AND. do_stokes_ad31 ) THEN
            
              IF ( do_normalized_WFoutput ) THEN        !  Normalized output
                GC_sfcprs_QJacobians(iw,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,dir) = &
                  VLIDORT_LINOUT%PROF%TS_PROFILEWF(q,l,                                   &
                  1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,2,dir) 
                GC_sfcprs_UJacobians(iw,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,dir) = &
                  VLIDORT_LINOUT%PROF%TS_PROFILEWF(q,l,                                   &
                  1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,3,dir) 
                IF( do_airdens_jac ) THEN
                  GC_airdens_QJacobians(iw,l,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,dir) =  &
                    VLIDORT_LINOUT%PROF%TS_PROFILEWF(q,l,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,2,dir) 
                  GC_airdens_UJacobians(iw,l,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,dir) =  &
                    VLIDORT_LINOUT%PROF%TS_PROFILEWF(q,l,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,3,dir) 
                ENDIF
              ELSE                                      !  Non-normalized output
                GC_sfcprs_QJacobians(iw,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,dir) = &
                  VLIDORT_LINOUT%PROF%TS_PROFILEWF(q,l,                                   &
                  1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,2,dir)                       &
                  / deltp
                GC_sfcprs_UJacobians(iw,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,dir) = &
                  VLIDORT_LINOUT%PROF%TS_PROFILEWF(q,l,                                   &
                  1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,3,dir)                       &
                  / deltp
                IF( do_airdens_jac ) THEN
                  GC_airdens_QJacobians(iw,l,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,dir) =  &
                    VLIDORT_LINOUT%PROF%TS_PROFILEWF(q,l,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,2,dir)/ deltp 
                  GC_airdens_UJacobians(iw,l,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,dir) =  &
                    VLIDORT_LINOUT%PROF%TS_PROFILEWF(q,l,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,3,dir)/ deltp 
                ENDIF
              ENDIF
              
            ENDIF


          ENDDO

        ENDDO




      ENDIF
      
      
      ! ------------------------------
      ! Save trace gas Jacobian
      ! ------------------------------ 
      IF ( do_Jacobians .and. do_gas_jacobians ) THEN
        q = gaswfidx
        DO g = 1, Diag32_InpOpt%n_gas
          
          gidx = Diag32_InpOpt%spc_idx(g)
          
          
          
          DO n = 1, VLIDORT_FixIn%Cont%TS_NLAYERS
            IF ( gas_partialcolumns(n,gidx) .EQ. 0.0d0 .OR. gasabs(n, gidx) .EQ. 0.d0 ) THEN
              GC_Tracegas_Jacobians(iw,n,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS, &
                                    1:VLIDORT_Out%Main%TS_N_GEOMETRIES,g,dir) = 0.0d0
            ELSE
              IF ( do_normalized_WFoutput ) THEN !  Normalized output
                ratio = gasabs(n, gidx) / total_gasabs(n)
              ELSE !  Non-normalized output
                ratio = gasabs(n, gidx) / gas_partialcolumns(n,gidx) / total_gasabs(n)
              ENDIF
              DO v = 1, VLIDORT_Out%Main%TS_N_GEOMETRIES
                GC_Tracegas_Jacobians(iw,n,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,g,dir) = &
                  VLIDORT_LinOut%Prof%TS_PROFILEWF(q,n,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,1,dir) * ratio
              ENDDO
            ENDIF
          ENDDO
        ENDDO
        
        
        
        IF( do_QU_jacobians .AND. do_stokes_ad32 ) THEN
          
          IF ( do_QU_Jacobians ) THEN
            q = gaswfidx
            DO g = 1, Diag32_InpOpt%n_gas
              gidx = Diag32_InpOpt%spc_idx(g)
              DO n = 1, VLIDORT_FixIn%Cont%TS_NLAYERS
                IF ( gas_partialcolumns(n,gidx) .EQ. 0.0d0 .OR. gasabs(n, gidx) == 0.d0) THEN
                  GC_Tracegas_QJacobians(iw,n,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS, &
                                        1:VLIDORT_Out%Main%TS_N_GEOMETRIES,g,dir) = 0.0d0
                  GC_Tracegas_UJacobians(iw,n,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS, &
                                        1:VLIDORT_Out%Main%TS_N_GEOMETRIES,g,dir) = 0.0d0
                ELSE
                  IF ( do_normalized_WFoutput ) THEN ! Normalized output
                    ratio = gasabs(n,gidx) / total_gasabs(n)
                  ELSE ! Non-normalized output
                    ratio = gasabs(n,gidx) / gas_partialcolumns(n, gidx) / total_gasabs(n)
                  ENDIF
                    DO v = 1, VLIDORT_Out%Main%TS_N_GEOMETRIES
                      GC_Tracegas_QJacobians(iw,n,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,g,dir) = ratio * &
                        VLIDORT_LinOut%Prof%TS_PROFILEWF(q,n,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,2,dir)
                      GC_Tracegas_UJacobians(iw,n,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,g,dir) = ratio * &
                        VLIDORT_LinOut%Prof%TS_PROFILEWF(q,n,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,3,dir)
                  ENDDO
                ENDIF
              ENDDO
            ENDDO
          ENDIF
        ENDIF
      ENDIF
      
      ! --------------------------------------------
      ! Save RRS optical thickness Jacobians
      ! --------------------------------------------
      IF ( do_raman_jacobian .and. do_rrs_abs ) THEN
        
        ! RRS index
        q = rrswfidx
        
        IF( do_normalized_WFoutput ) THEN
          DO n = 1, VLIDORT_FixIn%Cont%TS_NLAYERS
          DO v = 1, VLIDORT_Out%Main%TS_N_GEOMETRIES
            GC_rrs_Jacobian(iw, n, 1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS, v, dir ) = &
              VLIDORT_LinOut%Prof%TS_PROFILEWF(q,n,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,1,dir)
          ENDDO
          ENDDO
        ELSE
          DO n = 1, VLIDORT_FixIn%Cont%TS_NLAYERS
          DO v = 1, VLIDORT_Out%Main%TS_N_GEOMETRIES
            GC_rrs_Jacobian(iw, n, 1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS, v, dir ) = &
              VLIDORT_LinOut%Prof%TS_PROFILEWF(q,n,1:VLIDORT_FixIn%UserVal%TS_N_USER_LEVELS,v,1,dir)/&
            aircolumns(n)
          ENDDO
          ENDDO
        ENDIF
        
      ENDIF
      
    END SUBROUTINE archive_profile_diag
    
    ! ======================================================
    ! ( 8) AOD at reference wavelength
    ! ======================================================
    SUBROUTINE diag_08( NCID, ACTION, DO_XY )
      
      USE GC_variables_module, ONLY : Diag08_InpOpt, write_totaod_refwvl, &
                                      taer_profile,  aer_profile
      
      ! ---------------------------
      ! Subroutine arguments
      ! ---------------------------
      INTEGER, INTENT(IN) :: NCID
      INTEGER, INTENT(IN) :: ACTION
      LOGICAL, INTENT(IN) :: DO_XY
      
      ! ---------------
      ! Local variables
      ! ---------------=
      LOGICAL                                 :: error, dummy_err
      INTEGER, DIMENSION(1)                   :: size_1d, tmp_1d
      CHARACTER(LEN=max_ch_len), DIMENSION(1) :: dim_1d
      CHARACTER(LEN=max_ch_len)               :: fld_name
      INTEGER                                 :: n,g
      
      ! =====================================================
      ! diag_08 starts here
      ! =====================================================
      
      ! The dimension of the output
      dim_1d(1) = 'nlayer'
      
      ! Get dimension
      CALL match_names_in_dimlist( dim_1d, 1, tmp_1d, size_1d, dummy_err )
      
      ! Write the total profile
      IF( write_totaod_refwvl ) THEN
        
        CALL nc_fld_1d( dim_1d, size_1d(1), taer_profile(1:size_1d(1)), 'aods0',  &
                        NCID,   ACTION,     DO_XY                                 )
        
      ENDIF
      
      ! Species level profiles
      DO n=1,Diag08_InpOpt%n_gas
        
        ! Species index
        g = Diag08_InpOpt%spc_idx(n)
        
        ! Name of aod field
        fld_name = TRIM(ADJUSTL(Diag08_InpOpt%spc(n))) // '_aods0'
        
        CALL nc_fld_1d( dim_1d, size_1d(1), aer_profile(g,1:size_1d(1)), fld_name,  &
                            NCID,   ACTION,     DO_XY                               )
        
      ENDDO
      
    END SUBROUTINE diag_08 
    
    ! ======================================================
    ! (16) Gas partial columns
    ! ======================================================
    SUBROUTINE diag_16( NCID, ACTION, DO_XY )
      
      USE GC_variables_module, ONLY : Diag16_InpOpt, gas_partialcolumns
      
      ! ---------------------------
      ! Subroutine arguments
      ! ---------------------------
      INTEGER, INTENT(IN) :: NCID
      INTEGER, INTENT(IN) :: ACTION
      LOGICAL, INTENT(IN) :: DO_XY
      
      ! ---------------
      ! Local variables
      ! ---------------=
      LOGICAL                                 :: error, dummy_err
      INTEGER, DIMENSION(1)                   :: size_1d, tmp_1d
      CHARACTER(LEN=max_ch_len), DIMENSION(1) :: dim_1d
      CHARACTER(LEN=max_ch_len)               :: fld_name
      INTEGER                                 :: n,g
      
      ! =====================================================
      ! diag_16 starts here
      ! =====================================================
      
      ! Intialize error
      error = .FALSE.
      
      ! The dimension of the output
      dim_1d(1) = 'nlayer'
      
      ! Get dimension
      CALL match_names_in_dimlist( dim_1d, 1, tmp_1d, size_1d, dummy_err )
      
      DO n=1,Diag16_InpOpt%n_gas
        
        ! Gas Index
        g = Diag16_InpOpt%spc_idx(n)
        
        ! Name of gas field
        fld_name = TRIM(ADJUSTL(Diag16_InpOpt%spc(n))) // '_gascol'
        
        CALL nc_fld_1d( dim_1d, size_1d(1), gas_partialcolumns(1:size_1d(1),g), fld_name,  &
                            NCID,   ACTION,     DO_XY                                      )
      
      ENDDO
      
      
    END SUBROUTINE diag_16
    
    ! ======================================================
    ! (18)  Gas cross sections
    ! ======================================================
    SUBROUTINE diag_18( NCID, ACTION, DO_XY )
      
      USE GC_variables_module, ONLY : Diag18_InpOpt, gas_xsecs, &
                                      write_xsec_deriv,         &
                                      gas_xsecs_deriv 
           
      
      ! ---------------------------
      ! Subroutine arguments
      ! ---------------------------
      INTEGER, INTENT(IN) :: NCID
      INTEGER, INTENT(IN) :: ACTION
      LOGICAL, INTENT(IN) :: DO_XY
      
      ! ---------------
      ! Local variables
      ! ---------------=
      LOGICAL                                 :: error, dummy_err
      INTEGER, DIMENSION(2)                   :: size_2d, tmp_2d
      CHARACTER(LEN=max_ch_len), DIMENSION(2) :: dim_2d
      CHARACTER(LEN=max_ch_len)               :: fld_name
      INTEGER                                 :: n,g
      
      
      ! =====================================================
      ! diag_ starts here
      ! =====================================================
      
      ! Intialize error
      error = .FALSE.
      
      ! The dimension of the output
      dim_2d(1) = 'nw' ; dim_2d(2) = 'nlayer'
      
      ! Get dimension
      CALL match_names_in_dimlist( dim_2d, 2, tmp_2d, size_2d, dummy_err )
      
      DO n=1,Diag18_InpOpt%n_gas
        
        ! Gas Index
        g = Diag18_InpOpt%spc_idx(n)
        
        ! Name of gas field
        fld_name = TRIM(ADJUSTL(Diag18_InpOpt%spc(n))) // '_gas_xsec'
      
        CALL nc_fld_2d( dim_2d, size_2d(1), size_2d(2),                  &
                        gas_xsecs(1:size_2d(1),1:size_2d(2),g),fld_name,  &
                        NCID, ACTION, DO_XY                               )
        
        IF( write_xsec_deriv ) THEN
          
          ! Name of gas field
          fld_name = TRIM(ADJUSTL(Diag18_InpOpt%spc(n))) // '_gas_xsec_deriv'
          
          CALL nc_fld_2d( dim_2d, size_2d(1), size_2d(2),                         &
                          gas_xsecs_deriv(1:size_2d(1),1:size_2d(2),g),fld_name,  &
                          NCID, ACTION, DO_XY                                     )
           
          
        ENDIF
        
      ENDDO
      
    END SUBROUTINE diag_18
    
    ! ======================================================
    ! (32)  Trace gas jacobians
    ! ======================================================
    SUBROUTINE diag_32( NCID, ACTION, DO_XY )
      
      USE GC_variables_module, ONLY : Diag32_InpOpt,          &
                                      do_stokes_ad32, didx,   &
                                      GC_Tracegas_Jacobians,  &
                                      GC_Tracegas_QJacobians, &
                                      GC_Tracegas_UJacobians
      
      ! ---------------------------
      ! Subroutine arguments
      ! ---------------------------
      INTEGER, INTENT(IN) :: NCID
      INTEGER, INTENT(IN) :: ACTION
      LOGICAL, INTENT(IN) :: DO_XY
      
      ! ---------------
      ! Local variables
      ! ---------------
      LOGICAL                                 :: error, dummy_err
      INTEGER, DIMENSION(4)                   :: size_4d, tmp_4d
      CHARACTER(LEN=max_ch_len), DIMENSION(4) :: dim_4d
      CHARACTER(LEN=max_ch_len)               :: fld_name
      INTEGER                                 :: n!,g
      
      
      ! =====================================================
      ! diag_32 starts here
      ! =====================================================
      
      ! Intialize error
      error = .FALSE.
      
      ! The dimension of the output
      dim_4d(1) = 'nw' ; dim_4d(2) = 'nlayer' ; dim_4d(3) = 'ngeom' ; dim_4d(4) = 'noutputlevel'
      
      ! Get dimension
      CALL match_names_in_dimlist( dim_4d, 4, tmp_4d, size_4d, dummy_err )
      
      DO n=1,Diag32_InpOpt%n_gas
        
        ! Name of gas field
        fld_name = TRIM(ADJUSTL(Diag32_InpOpt%spc(n))) // '_gas_jac'
        
        CALL nc_fld_4d( dim_4d, size_4d(1), size_4d(2), size_4d(3), size_4d(4),                           &
                        GC_Tracegas_Jacobians(1:size_4d(1),1:size_4d(2),1:size_4d(3),1:size_4d(4),n,didx),&
                        fld_name,  NCID, ACTION, DO_XY                                                    )
        
        IF( do_stokes_ad32 ) THEN
          
          fld_name = TRIM(ADJUSTL(Diag32_InpOpt%spc(n))) // '_gas_qjac'
          CALL nc_fld_4d( dim_4d, size_4d(1), size_4d(2), size_4d(3), size_4d(4),                           &
                        GC_Tracegas_QJacobians(1:size_4d(1),1:size_4d(2),1:size_4d(3),1:size_4d(4),n,didx), &
                        fld_name,  NCID, ACTION, DO_XY                                                      )
          
          fld_name = TRIM(ADJUSTL(Diag32_InpOpt%spc(n))) // '_gas_ujac'
          CALL nc_fld_4d( dim_4d, size_4d(1), size_4d(2), size_4d(3), size_4d(4),                          &
                        GC_Tracegas_UJacobians(1:size_4d(1),1:size_4d(2),1:size_4d(3),1:size_4d(4),n,didx),&
                        fld_name,  NCID, ACTION, DO_XY                                                     )
        ENDIF
        
      ENDDO
      
    END SUBROUTINE diag_32
    

    
END MODULE GC_diag_profile_module