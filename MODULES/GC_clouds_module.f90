MODULE GC_clouds_module

  USE GC_parameters_module, ONLY: maxcld, GC_maxlayers, cldunit,         &
                                  max_ch_len, maxgases, maxaer
  USE GC_variables_module,  ONLY: cldfile, do_clouds, do_lambertian_cld, &
                                  use_cldprof, ngases, nmessages,        &
                                  messages, GC_nlayers, pressures,       &
                                  heights, pmaxl, pmaxc, pnc, profids,   &
                                  profile_data, temperatures, aircolumns,&
                                  daircolumns_dT, gas_partialcolumns,    &
                                  naer0, aer_profile0, cld_types,        &
                                  cld_bots, cld_profile, cld_tops,       &
                                  cld_flags, cld_taus, tcld_profile,     &
                                  tcldtau0, cld_uppers, cld_lowers, ncld,&
                                  cfrac, cld_reflambda, use_zcldspec,    &
                                  lambertian_cldalb, do_ctp_Jacobians,   &
                                  do_cfrac_Jacobians, do_cssa_Jacobians, &
                                  do_cod_Jacobians, do_cld_columnwf,     &
                                  n_cpix, cld_prof_npix, cld_lowers_npix,&
                                  cld_uppers_npix, do_npix_cloud, cpix_cfrac, &
                                  do_debug_geocape_tool, first_xy
  USE GC_profiles_module,   ONLY: match_cldprof_from_atmosprof
  USE GC_read_input_module, ONLY: skip_to_filemark
  USE GC_error_module

  IMPLICIT NONE

  PUBLIC :: cloud_profiles
  
CONTAINS

  SUBROUTINE cloud_profiles(error)
    
    IMPLICIT NONE
    
    LOGICAL, INTENT(INOUT) :: error ! Error variable
    INTEGER                :: i
    
    ! -------------------------------------------------------
    ! Code starts here
    ! -------------------------------------------------------
    
    error = .FALSE.
    

    ! Cloud top jacobians are currently not implemented 
    do_ctp_jacobians = .FALSE. 

    ! Check if there are valid cloods
    IF ( ( cfrac .le. 0.0d0 .and. sum(cpix_cfrac) .le. 0.0) &
         .OR. .NOT.do_clouds                                   ) THEN
       
       ncld               = 0
       cfrac              = 0.0d0
       do_cld_columnwf    = .FALSE.
       do_clouds          = .FALSE.
       do_cod_Jacobians   = .FALSE.
       do_cssa_Jacobians  = .FALSE.
       do_ctp_Jacobians   = .FALSE. 
       do_cfrac_Jacobians = .FALSE.
       
    ELSE
       
       IF (cfrac .ge. 1.0d0) THEN
          cfrac = 1.0d0
          do_cfrac_Jacobians = .FALSE.
       END IF
       
       IF (do_lambertian_cld) THEN
          do_cod_Jacobians  = .FALSE.
          do_cssa_jacobians = .FALSE.
          ncld = 1
       ENDIF
       
       
       
       ! Initialize some other variables
       if( ALLOCATED( cld_flags ) ) THEN
          tcld_profile(:) = 0.0d0 ! Computation of the total profile weighted by CF done later
          cld_flags(:)    = .FALSE.
       ENDIF
       tcldtau0        = 0.0d0
       
       ! ---------------------------------------------------------------------
       ! convert from cloud specification in pressure to altitude if necessary
       ! ---------------------------------------------------------------------
       
         ! Lambertian cloud setup
       IF( do_lambertian_cld ) THEN
          
          ! prepare_profiles overwrites the input cloud pressure if need be
          ! Here we just need to determine the height coordinate
          CALL set_lambertian_cldprof( error )
          
          ! Scattering cloud setup
       ELSE
          
          ! Load cloud optical properties on first iteration
          IF( first_xy ) THEN
            CALL load_cloud_optical_properties( error )
          ENDIF
          
          ! Get optical depths from file
          IF( use_cldprof ) THEN
             
             ! Match the cloud profiles
             CALL match_cldprof_from_atmosprof()
             
             ! Set clouds based on input file parameters
          ELSE
             
             ! Compute cloud profiles from 
             CALL compute_cldprof_from_inpfile( error )
             
          ENDIF
          
       ENDIF
       
    ENDIF
    
  END SUBROUTINE cloud_profiles
  
  SUBROUTINE set_lambertian_cldprof( error )
    
    USE GC_utilities_module, ONLY: spline, splint 
    USE GC_variables_module, ONLY: cpix_pcld,&
         cpix_lam_zlvl, cpix_lam_lvlfrac
    
    IMPLICIT NONE
    
    ! ----------------------
    ! Input/Output variables
    ! ----------------------
    LOGICAL, INTENT(OUT) :: error ! Error variable
    
    ! ----------------
    ! Local variables
    ! ----------------
    integer :: n, l
    REAL(KIND=8) :: zcld, log_pcld
    real(kind=8), dimension(0:GC_nlayers) :: y2, plog
    
    ! ==============================================================
    ! set_lambertian_cldprof starts here
    ! ==============================================================
    
    ! Initialize error
    error = .FALSE.
    
    ! Allocate arrays 
    IF( ALLOCATED(cpix_lam_zlvl)    ) DEALLOCATE( cpix_lam_zlvl    )
    IF( ALLOCATED(cpix_lam_lvlfrac) ) DEALLOCATE( cpix_lam_lvlfrac )
    ALLOCATE( cpix_lam_zlvl(n_cpix)    )
    ALLOCATE( cpix_lam_lvlfrac(n_cpix) )
    
    ! Initialize arrays
    cpix_lam_zlvl(:)    = 0
    cpix_lam_lvlfrac(:) = 0.0d0
    
    ! For now mimic original code, which determined layer 
    ! in height coordinates
    
    ! Compute spline of height vs logP
    plog = log10(pressures(0:GC_nlayers))
    call spline(plog(0:GC_nlayers), heights(0:GC_nlayers),&
         GC_nlayers+1,0.0d0,0.0d0,y2               )
    
    ! Loop over cloud pixels
    DO n=1,n_cpix
       
       ! Compute log pcld
       log_pcld = log10( REAL(cpix_pcld(n),KIND=8) )
       
       ! Convert layer to height coordinates
       call splint(plog(0:GC_nlayers), heights(0:GC_nlayers), &
            y2, GC_nlayers+1, log_pcld, zcld) 
       
       
       ! Determine layer
       DO l=1,GC_nlayers
          
          ! merge levels within 100 m
          IF( abs(zcld-heights(l)) < 1.0E-1 ) THEN
             
             ! Set the level
             zcld = heights(l)
             cpix_lam_zlvl(n) = l!+1 ! cld_uppers in insert_clouds
             
             ! Full Column
             cpix_lam_lvlfrac(n) = 1.0d0
             
             ! Exit loop
             EXIT
             
             ! Cloud is now above level
          ELSE IF( zcld > heights(l) ) THEN
             
             ! Set the level
             cpix_lam_zlvl(n) = l!+1 ! cld_uppers in insert_clouds
  	      	
             ! Compute column fraction
             cpix_lam_lvlfrac(n) = (cpix_pcld(n)-pressures(l-1)) / &
                  (pressures(l)-pressures(l-1))
             
             ! Exit loop
             EXIT
             
          ENDIF
         
       ENDDO
       
       ! Check if cloud below surface
       IF( zcld < heights(GC_nlayers) ) THEN
          
          cpix_lam_zlvl(n) = GC_nlayers
          cpix_lam_lvlfrac(n) = 1.0d0
          print*,'Warning - Cloud below surface (setting cloud bottom at surface)'
          
       ENDIF
       
       print*,'===================================='
       print*,n,cpix_pcld(n),pressures(l-1),'-',pressures(l)
       print*,l,cpix_lam_zlvl(n),cpix_lam_lvlfrac(n)
       print*,'===================================='
       
    ENDDO
    
  END SUBROUTINE set_lambertian_cldprof
  
  SUBROUTINE compute_cldprof_from_inpfile( error )
  	
    USE GC_Aerloading_routines_m, ONLY: generate_plume
    USE GC_variables_module, ONLY     : cld_prof_npix, &
         cld_lowers_npix,&
         cld_uppers_npix,&
         cld_d_profile_dtau,&
         cld_d_profile_dpkh,&
         cld_d_profile_dhfw,&
         cld_d_profile_drel,&
         cld_tau0s,&
         cld_proftype,&
         cld_z_upperlimit,&
         cld_z_lowerlimit,&
         cld_z_peakheight,&
         cld_half_width,&
         cld_relaxation
    
    IMPLICIT NONE
  	
    ! ----------------------
    ! Input/Output variables
    ! ----------------------
    LOGICAL, INTENT(OUT) :: error ! Error variable
    
    ! ----------------
    ! Local variables
    ! ----------------
    integer :: i, j, n
    
    ! ==============================================================
    ! compute_cldprof_from_inpfile starts here
    ! ==============================================================
    
    ! Initialize error
    error = .FALSE.
    
    ! Check that arrays have not been allocated
    IF( ALLOCATED( cld_prof_npix      ) ) DEALLOCATE( cld_prof_npix      )
    IF( ALLOCATED( cld_lowers_npix    ) ) DEALLOCATE( cld_lowers_npix    )
    IF( ALLOCATED( cld_uppers_npix    ) ) DEALLOCATE( cld_uppers_npix    )
    IF( ALLOCATED( cld_d_profile_dtau ) ) DEALLOCATE( cld_d_profile_dtau )
    IF( ALLOCATED( cld_d_profile_dpkh ) ) DEALLOCATE( cld_d_profile_dpkh )
    IF( ALLOCATED( cld_d_profile_dhfw ) ) DEALLOCATE( cld_d_profile_dhfw )
    IF( ALLOCATED( cld_d_profile_drel ) ) DEALLOCATE( cld_d_profile_drel )
    
    
    ! Allocate output arrays
    allocate(cld_prof_npix(ncld,GC_nlayers,n_cpix))
    allocate(cld_lowers_npix(ncld,n_cpix))
    allocate(cld_uppers_npix(ncld,n_cpix))
    
    ! For implementation of jacobians
    allocate(cld_d_profile_dtau(ncld,GC_nlayers,n_cpix))
    allocate(cld_d_profile_dpkh(ncld,GC_nlayers,n_cpix))
    allocate(cld_d_profile_dhfw(ncld,GC_nlayers,n_cpix))
    allocate(cld_d_profile_drel(ncld,GC_nlayers,n_cpix))
    
    ! Initialize varaibles
    cld_prof_npix(:,:,:) = 0.0
    cld_lowers_npix(:,:) = 0.0
    cld_uppers_npix(:,:) = 0.0
    cld_d_profile_dtau(:,:,:) = 0.0
    cld_d_profile_dpkh(:,:,:) = 0.0
    cld_d_profile_dhfw(:,:,:) = 0.0
    cld_d_profile_drel(:,:,:) = 0.0
    
    ! Loop over archived clouds
    DO i=1, ncld
       DO j=1, n_cpix
          ! Check if cloud exists
          IF( TRIM(ADJUSTL(cld_proftype(i,j))) .NE. '' .AND. &
               cld_tau0s(i,j) .GT. 0.0 ) THEN
        
             ! Call the profile param. 
             CALL generate_plume(                          &
                  GC_nlayers, GC_nlayers, heights(0:GC_nlayers), & ! input
                  cld_proftype(i,j), cld_z_upperlimit(i,j),      & ! input
                  cld_z_lowerlimit(i,j), cld_z_peakheight(i,j),  & ! input
                  cld_tau0s(i,j), cld_half_width(i,j),           & ! input
                  cld_relaxation(i,j),                           & ! input
                  cld_prof_npix(i,1:GC_nlayers,j),               & ! output
                  cld_d_profile_dtau(i,1:GC_nlayers,j),          & ! output   
                  cld_d_profile_dpkh(i,1:GC_nlayers,j),          & ! output
                  cld_d_profile_dhfw(i,1:GC_nlayers,j),          & ! output
                  cld_d_profile_drel(i,1:GC_nlayers,j),          & ! output
                  error, messages(nmessages+1)                   )
             
             ! Find cld_uppers, cld_lowers
             do n = 1, GC_nlayers
                if (cld_prof_npix(i, n, j) .gt. 0.0d0) then 
                   cld_uppers_npix(i,j) = n
                   exit
                endif
             enddo
             
             do n = GC_nlayers, 1, -1
                if (cld_prof_npix(i, n, j) .gt. 0.0d0) then 
                   cld_lowers_npix(i,j) = n + 1
                   exit
                endif
             enddo
             
          ENDIF
          
       ENDDO
    ENDDO
    
  END SUBROUTINE compute_cldprof_from_inpfile
  
  
  SUBROUTINE convert_cldprof_p2z( error )

    USE GC_utilities_module, ONLY: spline
    IMPLICIT NONE
    
    ! ----------------------
    ! Input/Output variables
    ! ----------------------
    LOGICAL, INTENT(OUT) :: error ! Error variable
    
    ! ----------------
    ! Local variables
    ! ----------------
    integer                               :: n
    real(kind=8), dimension(0:GC_nlayers) :: y2, plog
    ! ==============================================================
    ! convert_cldprof_p2z starts here
    ! ==============================================================
    
    ! Initialize error
    error = .FALSE.
    
    ! Don't do conversion if we are using the cloud profile
    IF (.not. do_lambertian_cld .and. use_cldprof) RETURN
    
    ! Compute spline of height vs logP
    plog = log10(pressures(0:GC_nlayers))
    call spline(plog(0:GC_nlayers), heights(0:GC_nlayers),&
         GC_nlayers+1,0.0d0,0.0d0,y2               )
    
    ! Loop over sub pixels
    DO n=1, n_cpix
       
       IF( do_lambertian_cld ) THEN
        
       ELSE
          
       ENDIF
       
    ENDDO
    
  END SUBROUTINE convert_cldprof_p2z
  
  SUBROUTINE load_cloud_optical_properties( error )
    
    ! Code mirrors aerosol module
    
    USE GC_parameters_module
    USE GC_variables_module,  ONLY : ncld, CldOptProp, &
         database_dir,cld_data_filenames
    
    IMPLICIT NONE
    INCLUDE 'netcdf.inc'
    
    ! ----------------------
    ! Input/Output variables
    ! ----------------------
    LOGICAL, INTENT(OUT) :: error ! Error variable
    
    ! ---------------
    ! Local Variables
    ! ---------------
    INTEGER :: n, nrh_i, nmom_i, nwvl_i
    CHARACTER(LEN=max_ch_len) :: infile
    
    INTEGER :: rcode, ncid, vid
    
    ! ===========================================================
    ! load_cloud_optical_properties starts here
    ! ===========================================================
    
    ! Initialize Error
    error = .FALSE.
    
    ! Allocate 
    ALLOCATE( CldOptProp( ncld ) )
    
    DO n=1, ncld
       
       infile = TRIM(ADJUSTL(database_dir)) // '/AerCldProp/' // &
            TRIM(ADJUSTL( cld_data_filenames(n) ))
       
       ! Open file
       rcode = NF_OPEN(trim(adjustl(infile)), NF_SHARE, ncid)
       
       ! Read number of wavelengths
       rcode = NF_INQ_VARID (ncid, 'nwls', vid)
       rcode = NF_GET_VAR(ncid, vid, nwvl_i)
       CldOptProp(n)%nWavelength = nwvl_i
       
       ! Read Number of relative humidities
       rcode = NF_INQ_VARID (ncid, 'nrh', vid)
       rcode = NF_GET_VAR(ncid, vid, nrh_i)
       CldOptProp(n)%nRH = nrh_i
       
       ! Read number of aerosol moments
       rcode = NF_INQ_VARID (ncid, 'nmom', vid)
       rcode = NF_GET_VAR(ncid, vid, nmom_i)
       CldOptProp(n)%nMoments = nmom_i-1 ! nmom_i is the dimension
       
       ! Allocate arrays
       ALLOCATE( CldOptProp(n)%PhFcn0(1:nwvl_i,0:nmom_i,1:6,1:nrh_i) )
       ALLOCATE( CldOptProp(n)%QExt0(nwvl_i,nrh_i)                   )
       ALLOCATE( CldOptProp(n)%SSA0(nwvl_i,nrh_i)                    )
       ALLOCATE( CldOptProp(n)%Wavelength(nwvl_i)                    )
       ALLOCATE( CldOptProp(n)%RH(nrh_i)                             )
       
       ! Read wavelength grid
       rcode = NF_INQ_VARID(ncid, 'wls0', vid)
       rcode = NF_GET_VAR(ncid, vid, CldOptProp(n)%Wavelength )
       
       ! Read relative humidity grid
       rcode = NF_INQ_VARID(ncid, 'rh', vid)
       rcode = NF_GET_VAR(ncid, vid, CldOptProp(n)%RH )
       
       ! Read Extinction
       rcode = NF_INQ_VARID(ncid, 'qext0', vid)
       rcode = NF_GET_VAR(ncid, vid, CldOptProp(n)%QExt0 )
       
       ! Read SSA
       rcode = NF_INQ_VARID(ncid, 'ssa0', vid)
       rcode = NF_GET_VAR(ncid, vid, CldOptProp(n)%SSA0 )
       
       ! Read phase function expansion
       rcode = NF_INQ_VARID(ncid, 'phfcn0', vid)
       rcode = NF_GET_VAR(ncid, vid,                                           &
            CldOptProp(n)%PhFcn0(1:nwvl_i,0:nmom_i-1,1:6,1:nrh_i) ,&
            (/1,1,1,1/),(/nwvl_i,nmom_i,6,nrh_i/)              )
       
       ! Close file
       rcode = NF_CLOSE(ncid)
       
    ENDDO
    
    
  END SUBROUTINE load_cloud_optical_properties
  
  SUBROUTINE interp_cloud_optics( wvl_out, nlayers, nmoms, ngkout, &
       opdeps, ssas, qexts, phfcn, error )
    
    USE GC_variables_module,  ONLY : ncld, CldOptProp, &
         cld_reflambda, cld_profile
    
    IMPLICIT NONE
    
    ! ----------------------
    ! Input/Output variables
    ! ----------------------
    INTEGER,      INTENT(IN)                                        :: nlayers,nmoms,ngkout
    REAL(KIND=8), INTENT(IN)                                        :: wvl_out
    REAL(KIND=8), DIMENSION (nlayers), INTENT(OUT)                  :: opdeps, ssas, qexts
    REAL(KIND=8), DIMENSION (nlayers, 0:nmoms, ngkout), INTENT(OUT) :: phfcn
    
    
    LOGICAL, INTENT(OUT) :: error ! Error variable
    
    REAL(KIND=8) :: wmin,wmax
    REAL(KIND=8) :: xwt0, xwt1
    REAL(KIND=8) :: xwt0_r, xwt1_r ! Reference wavelength
    INTEGER      :: idx0, idx0_r, moms_loop
    INTEGER      :: n, j, l, g, m
    REAL(KIND=8) :: scaods
    REAL(KIND=8), DIMENSION(nlayers) :: tsca
    REAL(KIND=8) :: qext_wvl, qext_ref, qext_rel
    REAL(KIND=8) :: ssa_wvl, phfcn_int
    
    ! Initialize Error
    error = .FALSE.
    
    ! Zero output arrays
    opdeps(:)     = 0.0d0
    ssas(:)       = 0.0d0
    qexts(:)      = 0.0d0
    phfcn(:,:,:)  = 0.0d0
    
    ! And some local ones
    tsca(:) = 0.0
    
    ! Loop over cloud types
    DO n=1,ncld
       
       ! Wavelength/RH bounds
       wmin  = CldOptProp(n)%Wavelength(1)
       wmax  = CldOptProp(n)%Wavelength(CldOptProp(n)%nWavelength)
       
       ! Determine max number of moments to loop
       moms_loop = MIN(CldOptProp(n)%nMoments,nmoms)
       
       ! Get weights for output wavelength interpolation
       ! ------------------------------------------------------------
       IF( wvl_out .LE. wmin ) THEN
          idx0 = 1
          xwt0 = 1.0
          xwt1 = 0.0
       ELSEIF( wvl_out .GE. wmax ) THEN
          idx0 = CldOptProp(n)%nWavelength-1
          xwt0 = 0.0
          xwt1 = 1.0
       ELSE 
          idx0 = -1
          j    =  1
          DO WHILE (idx0 .LT. 0)
             IF( CldOptProp(n)%Wavelength( j ) .GT. wvl_out ) THEN
                idx0 = j-1
                xwt0 = (CldOptProp(n)%Wavelength(idx0+1)-wvl_out)/&
                     (CldOptProp(n)%Wavelength(idx0+1)-CldOptProp(n)%Wavelength(idx0))
                xwt1 = (wvl_out-CldOptProp(n)%Wavelength(idx0)  )/&
                     (CldOptProp(n)%Wavelength(idx0+1)-CldOptProp(n)%Wavelength(idx0))
             ENDIF
             j = j+1
          ENDDO
       ENDIF
       
       ! Get weights for reference wavelength interpolation
       ! ------------------------------------------------------------
       IF( cld_reflambda.LE. wmin ) THEN
          idx0_r = 1
          xwt0_r = 1.0
          xwt1_r = 0.0
       ELSEIF( cld_reflambda .GE. wmax ) THEN
          idx0_r = CldOptProp(n)%nWavelength-1
          xwt0_r = 0.0
          xwt1_r = 1.0
       ELSE 
          idx0_r = -1
          j    =  1
          DO WHILE (idx0_r .LT. 0)
             IF( CldOptProp(n)%Wavelength( j ) .GT. cld_reflambda ) THEN
                idx0_r = j-1
                xwt0_r = (CldOptProp(n)%Wavelength(idx0_r+1)-cld_reflambda)/&
                     (CldOptProp(n)%Wavelength(idx0_r+1)-CldOptProp(n)%Wavelength(idx0_r))
                xwt1_r = (cld_reflambda-CldOptProp(n)%Wavelength(idx0_r)  )/&
                     (CldOptProp(n)%Wavelength(idx0_r+1)-CldOptProp(n)%Wavelength(idx0_r))
             ENDIF
             j = j+1
          ENDDO
       ENDIF
       
       ! Interpolate extinction
       qext_ref = xwt0_r*CldOptProp(n)%QExt0(idx0_r,1) &
            + xwt1_r*CldOptProp(n)%QExt0(idx0_r+1,1)
       qext_wvl = xwt0*CldOptProp(n)%QExt0(idx0,1) &
            + xwt1*CldOptProp(n)%QExt0(idx0+1,1)
       
       ! Ratio of extinction at output wavelength relative to reference wavelength
       qext_rel = qext_wvl/qext_ref
       
       ! Update total extinction
       DO l=1,nlayers
          opdeps(l) = opdeps(l) + cld_profile(n,l)*qext_rel
       ENDDO
       
       ! Interpolate single scattering albedo
       ssa_wvl = xwt0*CldOptProp(n)%SSA0(idx0,1)    &
            + xwt1*CldOptProp(n)%SSA0(idx0+1,1)  
       
       ! Update
       DO l=1,nlayers
          
          ! Scattering optical depth
          scaods = cld_profile(n,l) * qext_rel * ssa_wvl
          
          ! Update total scattering
          tsca(l) = tsca(l) + scaods
          
          ! Compute phase momemnts
          DO g=1,ngkout
             DO m=0,moms_loop
                
                ! Compute interpolated value
                phfcn_int = xwt0*CldOptProp(n)%PhFcn0(idx0,m,g,1) &
                     + xwt1*CldOptProp(n)%PhFcn0(idx0+1,m,g,1)
                
                ! Weight momemnts by scattering optical depth
                phfcn(l,m,g) = phfcn(l,m,g) + scaods*phfcn_int
                
             ENDDO
          ENDDO
          
       ENDDO
       
    ENDDO ! Cloud types
    
    ! Complete calculation
    DO l=1,nlayers
       
       IF( opdeps(l) .GT. 0.0 ) THEN
          
          ! Extinction relative to reference wavelength
          qexts(l) = opdeps(l) / SUM( cld_profile(1:ncld,l) )
          
          ! Compute SSA for layer
          ssas(l) = tsca(l) / opdeps(l)
          
          ! Renormalize phase function after weighting
          IF( tsca(l) .GT. 0.0 ) THEN
             DO g=1,ngkout
                DO m=0,nmoms
                   phfcn(l,m,g) = phfcn(l,m,g) / tsca(l)
                ENDDO
             ENDDO
          ENDIF
          
       ENDIF
       
    ENDDO
    
  END SUBROUTINE interp_cloud_optics
  
END MODULE GC_clouds_module
