MODULE GC_SciaUSGS_FA_module
  
  USE GC_variables_module,  ONLY: fa_mu, fa_W, fa_G, fa_wvl, &
                                  fa_xidx, fa_yidx, nlambdas, &
                                  fa_tmp, fa_tmp_int,fa_mu_m, fa_mu_int, &
                                  fa_exist, fa_W_int, fa_lat, fa_lon, &
                                  fa_wvl_r8, database_dir, nlambdas, lambdas,&
                                  usgs_water_int, usgs_snowf_int, usgs_snowc_int,&
                                  VBRDF_Sup_In, do_debug_geocape_tool,alb_eof_lfrac,&
                                  latitude,longitude,prof_snowdepth,prof_snowfrac_eff,&
                                  ltype_frac,day_of_year,prof_snowfrac,out_snowspec,&
                                  prof_snowage,prof_seaicefrac,fa_infile,&
                                  fa_nwvl, fa_nmod, fa_nfa, fa_imx, fa_jmx
  USE GC_parameters_module, ONLY: eofunit,deg2rad, max_ch_len
  
  IMPLICIT NONE
  
  INTEGER fa_ncid
  
  CONTAINS
  
  SUBROUTINE init_SciaUSGS_FA( centerLon, centerLat )
    
    USE GC_utilities_module, ONLY: BSPLINE
    INCLUDE 'netcdf.inc'
    
    ! Inputs
    REAL(KIND=8), INTENT(IN) :: centerLon
    REAL(KIND=8), INTENT(IN) :: centerLat
    
    ! ---------------
    ! Local variables
    ! ---------------
    INTEGER :: rcode, varid, usgs_ncid, dimid, vid
    INTEGER :: dimid2(2)
    REAL(KIND=4), ALLOCATABLE, DIMENSION(:)   :: usgs_wvl, usgs_spc
    
    CHARACTER(LEN=max_ch_len)    ::  usgs_infile, tmpchar !,fa_infile
    character(len=16), parameter :: location='init_SciaUSGS_FA'

    ! ==============================================================
    ! init_SciaUSGS_FA Starts here
    ! ==============================================================
    
    ! Path to USGS water/snow spectra
    usgs_infile = trim(adjustl(database_dir)) // '/BRDF_EOF/AlbSpec/' // &
                'usgs_snow_water.nc'
    
    ! Allocate arrays for water/snow spectra
    ALLOCATE( usgs_wvl(480)   )
    ALLOCATE( usgs_spc(480)   )
    ALLOCATE( usgs_water_int(nlambdas))
    ALLOCATE( usgs_snowf_int(nlambdas))
    ALLOCATE( usgs_snowc_int(nlambdas))
    ALLOCATE( out_snowspec(nlambdas)  )
    
    ! ---------------------------------------------------------------
    ! Attach file
    ! ---------------------------------------------------------------
    rcode = nf_open( trim(adjustl(usgs_infile)), nf_Nowrite, usgs_ncid )
    CALL fa_nc_handle_error(location,rcode)
    
    ! --------------------------------------------------------------
    ! Read data
    ! --------------------------------------------------------------
    
    ! (1) Wavelength
    ! --------------
    
    ! get variable index
    rcode = nf_inq_varid( usgs_ncid, 'Wavelength', varid )
    CALL fa_nc_handle_error(location,rcode)
    
    ! Read array
    rcode = nf_get_var_real( usgs_ncid, varid, usgs_wvl )
    CALL fa_nc_handle_error(location,rcode)
    
    ! (2) Water spectrum
    ! --------------
    
    ! get variable index
    rcode = nf_inq_varid( usgs_ncid, 'Water', varid )
    CALL fa_nc_handle_error(location,rcode)
    
    ! Read array
    rcode = nf_get_var_real( usgs_ncid, varid, usgs_spc )
    CALL fa_nc_handle_error(location,rcode)
    
    ! Interpolatae
    CALL BSPLINE( REAL(usgs_wvl,KIND=8),REAL(usgs_spc,KIND=8), 480,  &
                  lambdas(1:nlambdas),  usgs_water_int,    nlambdas, &
                  rcode                                              )
    IF( rcode .lt. 0 ) THEN
      WRITE(*,*) location, ': BSPLINE error, errstat = ', rcode
      STOP
    ENDIF
    
    ! (3) Fine Snow Spectrum
    ! ----------------------
    
    ! get variable index
    rcode = nf_inq_varid( usgs_ncid, 'SnowFine', varid )
    CALL fa_nc_handle_error(location,rcode)
    
    ! Read array
    rcode = nf_get_var_real( usgs_ncid, varid, usgs_spc )
    CALL fa_nc_handle_error(location,rcode)
    
    ! Interpolatae
    CALL BSPLINE( REAL(usgs_wvl,KIND=8),REAL(usgs_spc,KIND=8), 480,  &
                  lambdas(1:nlambdas),  usgs_snowf_int,    nlambdas, &
                  rcode                                              )
    IF( rcode .lt. 0 ) THEN
      WRITE(*,*) location, ': BSPLINE error, errstat = ', rcode
      STOP
    ENDIF
    
    ! (4) Coarse Snow Spectrum
    ! ------------------------
    
    ! get variable index
    rcode = nf_inq_varid( usgs_ncid, 'SnowCoarse', varid )
    CALL fa_nc_handle_error(location,rcode)
    
    ! Read array
    rcode = nf_get_var_real( usgs_ncid, varid, usgs_spc )
    CALL fa_nc_handle_error(location,rcode)
    
    ! Interpolatae
    CALL BSPLINE( REAL(usgs_wvl,KIND=8),REAL(usgs_spc,KIND=8), 480,  &
                  lambdas(1:nlambdas),  usgs_snowc_int,    nlambdas, &
                  rcode                                              )
    IF( rcode .lt. 0 ) THEN
      WRITE(*,*) location, ': BSPLINE error, errstat = ', rcode
      STOP
    ENDIF
    
    ! Close file
    rcode = nf_close( usgs_ncid )
    CALL fa_nc_handle_error(location,rcode)
    
    ! Deallocate arrays
    IF( ALLOCATED(usgs_wvl) ) DEALLOCATE(usgs_wvl)
    IF( ALLOCATED(usgs_spc) ) DEALLOCATE(usgs_spc)
    
    ! --------------------------------------------------------------
    ! Factor analysis results
    ! --------------------------------------------------------------
    
    ! Attach file
    rcode = nf_open( trim(adjustl(fa_infile)), nf_Nowrite, fa_ncid )
    
    ! Check for error
    CALL fa_nc_handle_error(location,rcode)
    
    ! ---------------------------------------------------------------
    ! Get the dimensions to allocate arrays
    ! ---------------------------------------------------------------
    
    ! Find the dimension of the spectrum
    rcode =    nf_inq_varid( fa_ncid, 'wvl',    vid         )
    rcode = nf_inq_vardimid( fa_ncid,   vid,  dimid         )
    rcode =      nf_inq_dim( fa_ncid, dimid,tmpchar,fa_nwvl )
    
    ! Find the dimension of MODIS
    rcode =    nf_inq_varid( fa_ncid, 'mu_m_global',    vid         )
    rcode = nf_inq_vardimid( fa_ncid,           vid,  dimid         )
    rcode =      nf_inq_dim( fa_ncid,         dimid,tmpchar,fa_nmod )
    
    ! Find the dimension of longitudinal dimension
    rcode =    nf_inq_varid( fa_ncid, 'lon',    vid        )
    rcode = nf_inq_vardimid( fa_ncid,   vid,  dimid        )
    rcode =      nf_inq_dim( fa_ncid, dimid,tmpchar,fa_imx )
    
    ! Find the dimension of latitudinal dimension
    rcode =    nf_inq_varid( fa_ncid, 'lat',    vid        )
    rcode = nf_inq_vardimid( fa_ncid,   vid,  dimid        )
    rcode =      nf_inq_dim( fa_ncid, dimid,tmpchar,fa_jmx )
    
    ! Find the number of factors
    rcode =    nf_inq_varid( fa_ncid, 'W_global',    vid       )
    rcode = nf_inq_vardimid( fa_ncid,   vid,  dimid2           )
    rcode =      nf_inq_dim( fa_ncid, dimid2(1),tmpchar,fa_nfa )
    
    ! Allocate arrays
    ALLOCATE( fa_mu(fa_nwvl)            )
    ALLOCATE( fa_mu_m(fa_nmod)          )
    ALLOCATE( fa_wvl(fa_nwvl)           )
    ALLOCATE( fa_wvl_r8(fa_nwvl)        )
    ALLOCATE( fa_W(fa_nfa,fa_nwvl)      )
    ALLOCATE( fa_G(fa_nmod,fa_nfa)      )
    ALLOCATE( fa_lon(fa_imx)            )
    ALLOCATE( fa_lat(fa_jmx)            )
    ALLOCATE( fa_exist(fa_jmx,fa_imx)   )
    ALLOCATE( fa_mu_int(nlambdas)       )
    ALLOCATE( fa_W_int(fa_nfa,nlambdas) )
    ALLOCATE( fa_tmp(fa_nwvl)           )
    ALLOCATE( fa_tmp_int(nlambdas)      )
    
    ! Initialize x/y indices
    fa_xidx = -1
    fa_yidx = -1
    
    ! --------------------------------------------------------------
    ! Read data
    ! --------------------------------------------------------------
    
    ! (1) Wavelength
    ! --------------
    
    ! get variable index
    rcode = nf_inq_varid( fa_ncid, 'wvl', varid )
    CALL fa_nc_handle_error(location,rcode)
    
    ! Read array
    rcode = nf_get_var_real( fa_ncid, varid, fa_wvl )
    CALL fa_nc_handle_error(location,rcode)
    fa_wvl_r8(:) = REAL(fa_wvl(:),KIND=8)
    
    ! (2) Longitudes
    ! --------------
    
    ! get variable index
    rcode = nf_inq_varid( fa_ncid, 'lon', varid )
    CALL fa_nc_handle_error(location,rcode)
    
    ! Read array
    rcode = nf_get_var_real( fa_ncid, varid, fa_lon )
    CALL fa_nc_handle_error(location,rcode)
    
    ! (2) Latitudes
    ! --------------
    
    ! get variable index
    rcode = nf_inq_varid( fa_ncid, 'lat', varid )
    CALL fa_nc_handle_error(location,rcode)
    
    ! Read array
    rcode = nf_get_var_real( fa_ncid, varid, fa_lat )
    CALL fa_nc_handle_error(location,rcode)
    
    ! (3) Array to check if FA exists for given lon/lat
    ! -------------------------------------------------
    
    ! get variable index
    rcode = nf_inq_varid( fa_ncid, 'yn_fa', varid )
    CALL fa_nc_handle_error(location,rcode)
    
    ! Read array
    rcode = nf_get_var_int2( fa_ncid, varid, fa_exist )
    CALL fa_nc_handle_error(location,rcode)
    
    ! Now updat the FA with the local values
    CALL update_SciaUSGS_FA( centerLon, centerLat )
    
  END SUBROUTINE init_SciaUSGS_FA
  
  SUBROUTINE update_SciaUSGS_FA( centerLon, centerLat )

    USE GC_utilities_module, ONLY: BSPLINE
    IMPLICIT NONE
    INCLUDE 'netcdf.inc'
    
    ! Inputs
    REAL(KIND=8), INTENT(IN) :: centerLon
    REAL(KIND=8), INTENT(IN) :: centerLat
    
    INTEGER :: rcode, varid
    INTEGER, DIMENSION(1) :: tmp
    INTEGER :: i,j, n
    character(len=18), parameter :: location='update_SciaUSGS_FA'
    
    ! ==============================================================
    ! cleanup_SciaUSGS_FA Starts here
    ! ==============================================================
    
    ! Check if center is out of range
    IF( ABS(centerLon) .GT. 180.0 .OR. ABS(centerLat) .GT. 90.0 ) THEN
      
      ! Load global factors
      i = 0
      j = 0
      
    ELSE
      
      ! Find the nearest index
      tmp = MINLOC( ABS(fa_lon-centerLon) ) ; i = tmp(1)
      tmp = MINLOC( ABS(fa_lat-centerLat) ) ; j = tmp(1)
      !print*,i,j,fa_exist(j,i),centerLon,centerLat
      
      ! Check if local FA results exist
      IF( fa_exist(j,i) .EQ. 0 ) THEN 
        i = 0
        j = 0
      ENDIF
      
    ENDIF
    
    ! Load factors (if they change)
    IF( fa_xidx .ne. i .or. fa_yidx .ne. j ) THEN
      
      IF( i .eq. 0 .or. j .eq. 0 ) THEN ! Global factor case
        
        ! (1) Mean spectrum
        ! -----------------
        
        ! get variable index
        rcode = nf_inq_varid( fa_ncid, 'mu_global', varid )
        CALL fa_nc_handle_error(location,rcode)
        
        ! Read array
        rcode = nf_get_var_real( fa_ncid, varid, fa_mu )
        CALL fa_nc_handle_error(location,rcode)
        
        ! Mean at the MODIS bands
        rcode = nf_inq_varid( fa_ncid, 'mu_m_global', varid )
        CALL fa_nc_handle_error(location,rcode)
        
        ! Read array
        rcode = nf_get_var_real( fa_ncid, varid, fa_mu_m )
        CALL fa_nc_handle_error(location,rcode)
        
        ! (2) Factor loading matrix
        ! -------------------------
        
        ! get variable index
        rcode = nf_inq_varid( fa_ncid, 'W_global', varid )
        CALL fa_nc_handle_error(location,rcode)
        
        ! Read array
        rcode = nf_get_var_real( fa_ncid, varid, fa_W )
        CALL fa_nc_handle_error(location,rcode)
        
        ! (3) MODIS Gain matrix
        ! ----------------------
        
        ! get variable index
        rcode = nf_inq_varid( fa_ncid, 'Gm_global', varid )
        CALL fa_nc_handle_error(location,rcode)
        
        ! Read array
        rcode = nf_get_var_real( fa_ncid, varid, fa_G )
        CALL fa_nc_handle_error(location,rcode)
        
      ELSE ! Local factor case
        
        ! (1) Mean spectrum
        ! -----------------
        
        ! get variable index
        rcode = nf_inq_varid( fa_ncid, 'mu', varid )
        CALL fa_nc_handle_error(location,rcode)
        
        ! Read array
        rcode = nf_get_vara_real( fa_ncid, varid,            &
                                  (/j,i,1/),(/1,1,fa_nwvl/), &
                                  fa_mu(:)                   )
        CALL fa_nc_handle_error(location,rcode)
        
        ! Mean at the MODIS bands
        rcode = nf_inq_varid( fa_ncid, 'mu_m', varid )
        CALL fa_nc_handle_error(location,rcode)
        
        ! Read array
        rcode = nf_get_vara_real( fa_ncid, varid,            &
                                  (/j,i,1/),(/1,1,fa_nmod/), &
                                  fa_mu_m(:)                 )
        CALL fa_nc_handle_error(location,rcode)
        
        ! (2) Factor loading matrix
        ! -------------------------
        
        ! get variable index
        rcode = nf_inq_varid( fa_ncid, 'W', varid )
        CALL fa_nc_handle_error(location,rcode)
        
        ! Read array
        rcode = nf_get_vara_real( fa_ncid, varid,                     &
                                  (/j,i,1,1/),(/1,1,fa_nfa,fa_nwvl/), &
                                  fa_W(:,:)                           )
        CALL fa_nc_handle_error(location,rcode)
        
        ! (3) MODIS Gain matrix
        ! ----------------------
        
        ! get variable index
        rcode = nf_inq_varid( fa_ncid, 'Gm', varid )
        CALL fa_nc_handle_error(location,rcode)
        
        ! Read array
        rcode = nf_get_vara_real( fa_ncid, varid,                     &
                                  (/j,i,1,1/),(/1,1,fa_nmod,fa_nfa/), &
                                  fa_G(:,:)                           )
        CALL fa_nc_handle_error(location,rcode)
        
      ENDIF
      
      ! Update loaded index
      fa_xidx = i
      fa_yidx = j
      
    ENDIF
    
    ! Interpolate to the input wavelength grid
    ! ----------------------------------------
    
    ! mean
    CALL BSPLINE( fa_wvl_r8,           REAL(fa_mu,KIND=8), fa_nwvl,  &
                  lambdas(1:nlambdas), fa_mu_int,          nlambdas, &
                  rcode                                              )
    
    IF( rcode .lt. 0 ) THEN
      WRITE(*,*) location, ': BSPLINE error, errstat = ', rcode
      STOP
    ENDIF
    
    ! Factor loadings
    DO n=1,fa_nfa
      
      ! Get factor loading
      fa_tmp(:) = REAL(fa_W(n,:),KIND=8)
      
      ! Do interpolation
      CALL BSPLINE( fa_wvl_r8,           fa_tmp,     fa_nwvl,  &
                    lambdas(1:nlambdas), fa_tmp_int, nlambdas, &
                    rcode                                      )
    
      IF( rcode .lt. 0 ) THEN
        WRITE(*,*) location, ': BSPLINE error, errstat = ', rcode
        STOP
      ENDIF
      
      ! Store result
      fa_W_int(n,:) = fa_tmp_int(:)
      
    ENDDO
    
  END SUBROUTINE update_SciaUSGS_FA
  
  SUBROUTINE load_MODIS_pixel(dayofyear,snowfrac,error)
    
    USE GC_variables_module,     ONLY: clon, clat, nx_modis, ny_modis, &
                                       modis_brdfs, modis_lfrac,&
                                       modis_lon, modis_lat, modis_pix_lfrac,&
                                       modis_pix_kern
    
    
    ! Inputs
    INTEGER (KIND=4), INTENT(IN)        :: dayofyear
    REAL (KIND=8), INTENT(IN)           :: snowfrac
    LOGICAL,       INTENT(OUT)          :: error
    ! Dimensions
    INTEGER, PARAMETER       :: njday = 48, nband=4, nkernel=3, neof = 4
    INTEGER, PARAMETER       :: npos = 2501, nsnowpos=480
    
    ! MODIS days
    CHARACTER(LEN=3), DIMENSION(njday), PARAMETER :: modis_daystrs = (/        &
        '353', '361', '001', '009', '017', '025', '033', '041', '049', '057',  &
        '065', '073', '081', '089', '097', '105', '113', '121', '129', '137',  &
        '145', '153', '161', '169', '177', '185', '193', '201', '209', '217',  &
        '225', '233', '241', '249', '257', '265', '273', '281', '289', '297',  &
        '305',  '313', '321', '329', '337', '345', '353', '361'/)
    REAL (KIND=8), DIMENSION(njday), PARAMETER :: modis_days = (/ &
        -5.0,  3.0,   8.0,   16.0,  24.0,  32.0,  40.0,  48.0,  56.0,  64.0,  &
        72.0,  80.0,  88.0,  96.0,  104.0, 112.0, 120.0, 128.0, 136.0, 144.0, &
        152.0, 160.0, 168.0, 176.0, 184.0, 192.0, 200.0, 208.0, 216.0, 224.0, &
        232.0, 240.0, 248.0, 256.0, 264.0, 272.0, 280.0, 288.0, 296.0, 304.0, &
        312.0, 320.0, 328.0, 336.6, 344.0, 352.0, 360.0, 368.0/)
    
    REAL (KIND=8), DIMENSION(njday), PARAMETER :: modis_maxlat = (/ &
        46.56, 46.67, 46.96, 47.79, 49.03, 50.67, 52.68, 55.02, 57.64, 60.49, &
        63.52, 66.68, 69.90, 73.11, 76.28, 79.32, 82.19, 83.55, 83.55, 83.55, &
        83.55, 83.55, 83.55, 83.55, 83.55, 83.55, 83.55, 83.55, 83.55, 83.55, &  
        83.55, 81.84, 78.95, 75.89, 72.71, 69.49, 66.28, 63.14, 60.12, 57.30, &
        54.71, 52.41, 50.45, 48.85, 47.66, 46.90, 46.56, 46.67 /)
    
      ! Masking depth values[m] for IGBP land types
    REAL(KIND=8), DIMENSION(17) :: ds_star = (/ 1e9,& !  0 - Water
                                               12.0,& !  1 - Evergreen Needleleaf
                                                8.0,& !  2 - Evergreen Broadleaf
                                                8.0,& !  3 - Deciduous Needleleaf
                                                8.0,& !  4 - Deciduous Broadleaf
                                                8.0,& !  5 - Mixed Forest (Use evergreen broadleaf factor)
                                                1.0,& !  6 - Closed shrublands
                                                1.0,& !  7 - Open shrublands
                                               0.15,& !  8 - Woody savannas
                                               0.15,& !  9 - Savannas
                                                0.1,& ! 10 - Grasslands
                                               0.01,& ! 11 - Permanent Wetlands
                                                0.1,& ! 12 - Croplands
                                                4.0,& ! 13 - Urban and buit up
                                                0.1,& ! 14 - Cropland/ Natural vegetation mosaic
                                               0.01,& ! 15 - Snow and Ice
                                               0.01 /)! 16 - Barren or Sparsely vegetated
    
    REAL (KIND=8), DIMENSION(2)    :: elons, elats
    INTEGER, DIMENSION(2)          :: mlats, mlons
    REAL (KIND=8), DIMENSION(2)    :: fracs
    INTEGER, DIMENSION(2)          :: idxs
    REAL (KIND=8), DIMENSION(nband, nkernel)  :: brdf_par
    INTEGER :: i,j, b, n
    
    LOGICAL       :: valid_brdf
    ! Day differences
    REAL(KIND=8), DIMENSION(njday) :: day_dist
    
    ! Vertices for pnpoly
    REAL(KIND=8), DIMENSION(4)     :: vx, vy
    REAL(KIND=8)                   :: snowage_wt 
    CHARACTER(LEN=16), PARAMETER :: modulename = 'load_MODIS_pixel'
    
    ! ==============================================================
    ! load_MODIS_pixel starts here
    ! ==============================================================
  
    ! Initialize error
    error = .FALSE.   
 
    ! Set MODIS wavelengths
    VBRDF_Sup_In%BS_MODIS_channels(1) = 469.0d0
    VBRDF_Sup_In%BS_MODIS_channels(2) = 555.0d0
    VBRDF_Sup_In%BS_MODIS_channels(3) = 645.0d0
    VBRDF_Sup_In%BS_MODIS_channels(4) = 858.5d0
    
    ! Compute effective snow fraction (accounting for masking depths of different land types)
    prof_snowfrac_eff = 0.0d0
    DO i=1,17
      prof_snowfrac_eff = prof_snowfrac_eff &
                        + ltype_frac(i)*( 1.0d0-exp(-1.0*prof_snowdepth/ds_star(i)) )
    ENDDO
    prof_snowfrac_eff = prof_snowfrac_eff * snowfrac
    
    ! Compute snow age spectrum weighting
    snowage_wt = exp( -0.2d0*prof_snowage )
    
    ! Compute snow spectrum
    out_snowspec = snowage_wt*usgs_snowf_int + (1.0d0-snowage_wt)*usgs_snowc_int
    
    ! Lat/Lon box around pixel 
    elons(1) = minval( clon )
    elons(2) = maxval( clon )
    elats(1) = minval( clat )
    elats(2) = maxval( clat )
    
    ! Create vertices for pnpoly
    DO n=1,4
      vx(n) = clon(n)
      vy(n) = clat(n)
    ENDDO
    
    ! Find out modis files to be read out
    idxs(1) = MINVAL(MAXLOC(modis_days, MASK=(modis_days < dayofyear - 0.5)))
    idxs(2) = idxs(1) + 1
    fracs(2) = (dayofyear - 0.5 - modis_days(idxs(1))) / (modis_days(idxs(2)) - modis_days(idxs(1)))
    fracs(1) = 1.0 - fracs(2)
    
    ! First check if Reflectivity for this observation
    IF( elats(2) > modis_maxlat(idxs(1)) .OR. &
        elats(2) > modis_maxlat(idxs(2))      ) THEN
        
      ! Compute day distances
      DO i=1,njday
        day_dist(i) = MIN( ABS(modis_days(i)-dayofyear),        &
                           ABS(365.0d0-dayofyear)+modis_days(i) )
      ENDDO
      
      
      ! Find the nearest valid latitude
      idxs(1) = MINVAL( MINLOC( day_dist, MASK=( modis_maxlat > elats(2) ) ) )
      fracs(1) = 1.0d0
      fracs(2) = 0.0d0
      
    ENDIF
    
    ! Call the routine to read from the original MODIS resolution
    CALL READ_MODIS_BRDF_HR (elons, elats, &
         (/modis_daystrs(idxs(1)), modis_daystrs(idxs(2))/), fracs )
    
    ! We subset the appropriate data when reading the hdf file
    mlons(1) = 1
    mlons(2) = nx_modis
    mlats(1) = 1
    mlats(2) = ny_modis
    
    ! New Calc
    n = 0
    brdf_par(:, :) = 0.0d0
    alb_eof_lfrac    = 0.0d0
    DO i=mlons(1),mlons(2)
    DO j=mlats(1),mlats(2)
      
      ! Add point if it is in polygon
      IF( PNPOLY_r8(4,vx,vy,modis_lon(i),modis_lat(j)) ) THEN
        
        ! Increment count
        n = n + 1
        
        ! Check isotropic kernel value for each band
        valid_brdf = .TRUE.
        DO b=1,4
          IF( modis_brdfs(i,j,b,1) < 0.0 .OR. &
              modis_brdfs(i,j,b,1) > 1000.0   ) THEN
            valid_brdf = .FALSE.
          ENDIF
        ENDDO
        
        IF( modis_lfrac(i,j) > 0.0 .AND. valid_brdf ) THEN ! Its either 0 or 1
          brdf_par = brdf_par + modis_brdfs(i, j, :, :)/1000.0
          
          alb_eof_lfrac = alb_eof_lfrac + 1.0d0
        ENDIF
      ENDIF
    ENDDO
    ENDDO
    
    IF( n > 0 ) THEN 
    
      ! Complete average
      alb_eof_lfrac = alb_eof_lfrac / (1.0d0*n)
      brdf_par = brdf_par / (1.d0 * n)
      
    ELSE
      
      ! Nearest neighbour sampling
      i = MINVAL( MINLOC( ABS(modis_lon(1:nx_modis)-longitude) ) )
      j = MINVAL( MINLOC( ABS(modis_lat(1:ny_modis)-latitude ) ) )
      
      ! Check valid
      valid_brdf = .TRUE.
      DO b=1,4
        IF( modis_brdfs(i,j,b,1) < 0.0 .OR. &
            modis_brdfs(i,j,b,1) > 1000.0   ) THEN
          valid_brdf = .FALSE.
        ENDIF
      ENDDO
      
      IF( modis_lfrac(i,j) > 0.0 .AND. valid_brdf ) THEN
        
        alb_eof_lfrac = 1.0d0
        brdf_par = modis_brdfs(i,j,:,:)/1000.0
        
      ENDIF
      
    ENDIF
    
    IF(do_debug_geocape_tool) print*,'LFRAC:',alb_eof_lfrac
    
    ! Archive for diagnostics
    modis_pix_kern(:,:) = brdf_par(:,:)
    modis_pix_lfrac     = alb_eof_lfrac
    
    ! Store land fraction
    VBRDF_Sup_In%BS_MODISPCA_landfrac = alb_eof_lfrac
    
    ! Store kernels if land exists
    IF( alb_eof_lfrac > 0.0 ) THEN
      
      VBRDF_Sup_In%BS_MODIS_FIso(1:4) = brdf_par(1:4,1)
      VBRDF_Sup_In%BS_MODIS_FVol(1:4) = brdf_par(1:4,2)
      VBRDF_Sup_In%BS_MODIS_FGeo(1:4) = brdf_par(1:4,3)
    
    ELSE
      
      VBRDF_Sup_In%BS_MODIS_FIso(1:4) = 0.0
      VBRDF_Sup_In%BS_MODIS_FVol(1:4) = 0.0
      VBRDF_Sup_In%BS_MODIS_FGeo(1:4) = 0.0
      
    ENDIF
    
    IF( snowfrac .GT. 0.0 ) THEN
      VBRDF_Sup_In%BS_MODISPCA_is_snow = .TRUE.
    ELSE
      VBRDF_Sup_In%BS_MODISPCA_is_snow = .FALSE.
    ENDIF
    
  END SUBROUTINE load_MODIS_pixel
  
  SUBROUTINE set_SciaUSGS_vl_kernels( error )

    USE GC_variables_module,  ONLY : fkern_idx, fkern_par, fkern_amp_wvl, &
                                     first_xy,  nlambdas, Diag28_InpOpt,  &
                                     fkern_npar, do_diag, BRDF_Sup_ModOut,&
                                     modis_pix_kern, VBRDF_Sup_In,        &
                                     VBRDF_LinSup_In, gc_sun_azimuths,    &
                                     salinity, wind_dir, wind_speed
    USE GC_parameters_module, ONLY : BRDF_CHECK_NAMES, short_kern_name
    USE VLIDORT_PARS,         ONLY : NewCMGLINT_IDX, MAX_BRDF_PARAMETERS
    ! ====================
    ! Subroutine arguments
    ! ====================
    LOGICAL, INTENT(INOUT) :: error

    ! ===============
    ! Local variables
    ! ===============
    REAL(KIND=8), DIMENSION(nlambdas,3)          :: land_kern
    INTEGER                                      :: n, ct, i, j, idx, npar
    REAL(KIND=8), DIMENSION(MAX_BRDF_PARAMETERS) :: par
    LOGICAL                                      :: land_only
    REAL(KIND=8)                                 :: f_land, f_landice, &
                                                    f_sea,  f_seaice
    CHARACTER(LEN=max_ch_len)                    :: numstr

    ! ========================================================
    ! set_SciaUSGS_vl_kernels starts here
    ! ========================================================

    ! --------------------------------------------------------------
    ! Compute the kernel coefficients for the land part of the scene
    ! --------------------------------------------------------------

    ! Allocate kernel amplitudes
    IF( first_xy ) ALLOCATE( fkern_amp_wvl(nlambdas,4) )

    
    ! Compute wavelength dependent kernel amplitudes
    land_only = .TRUE. ! Don't compute weighting when calculating coefficients
    DO n=1,3
      CALL compute_SciaUSGS_Coeff( modis_pix_kern(:,n), fkern_amp_wvl(:,n), nlambdas, land_only )   
    ENDDO

    ! Modify Kernel amplitudes by land cover
    ! --------------------------------------

    ! Compute land cover fractions
    f_land    = VBRDF_Sup_In%BS_MODISPCA_landfrac*(1.0-prof_snowfrac_eff)
    f_landice = VBRDF_Sup_In%BS_MODISPCA_landfrac*prof_snowfrac_eff
    f_sea     = (1.0-VBRDF_Sup_In%BS_MODISPCA_landfrac)*(1.0-prof_seaicefrac)
    f_seaice  = (1.0-VBRDF_Sup_In%BS_MODISPCA_landfrac)*prof_seaicefrac

    ! Isotropic Kernel
    fkern_amp_wvl(:,1) = fkern_amp_wvl(:,1)*f_land + out_snowspec*(f_landice+f_seaice)
    
    ! Volumetric (RossThick)
    fkern_amp_wvl(:,2) = fkern_amp_wvl(:,2)*f_land
!     fkern_amp_wvl(:,2) = fkern_amp_wvl(:,2)*f_land*0.5d0 ! Validation of integrated BSA suggests VL kernel is a factor of two different
    
    ! Geometric (LiSparse)
    fkern_amp_wvl(:,3) = fkern_amp_wvl(:,3)*f_land
!     fkern_amp_wvl(:,3) = fkern_amp_wvl(:,3)*f_land*0.5d0
    
    ! Ocean Kernel (New CoxMunk + Glint)
    fkern_amp_wvl(:,4) = f_sea

    ! The land kernels also need to be scaled by land fraction
    DO n=1,3
      fkern_amp_wvl(:,n) = fkern_amp_wvl(:,n)*VBRDF_Sup_In%BS_MODISPCA_landfrac
    ENDDO
    
    ! Generic Options
    VBRDF_Sup_In%BS_N_BRDF_KERNELS         = 4

    ! Don't do the old precomputation
    VBRDF_Sup_In%BS_MODISPCA_choice        = .FALSE.
    BRDF_Sup_ModOut%BS_DO_MODISPCA_KERNELS = .FALSE.

    ! --------------------------------------------------------
    ! Kernel 1 - Isotropic
    ! --------------------------------------------------------  
    VBRDF_Sup_In%BS_BRDF_NAMES(1)             = BRDF_CHECK_NAMES(1)
    VBRDF_Sup_In%BS_WHICH_BRDF(1)             = 1
    VBRDF_Sup_In%BS_N_BRDF_PARAMETERS(1)      = 0
    VBRDF_Sup_In%BS_BRDF_PARAMETERS(1,:)      = 0.0d0 ! (kernel,par)
    VBRDF_Sup_In%BS_LAMBERTIAN_KERNEL_FLAG(1) = .TRUE. 
    VBRDF_Sup_In%BS_BRDF_FACTORS(1)           = 0.0 ! <- Set later with fkern_amp_wvl

    ! --------------------------------------------------------
    ! Kernel 2 - Vol RossThick
    ! --------------------------------------------------------  
    VBRDF_Sup_In%BS_BRDF_NAMES(2)             = BRDF_CHECK_NAMES(3)
    VBRDF_Sup_In%BS_WHICH_BRDF(2)             = 3
    VBRDF_Sup_In%BS_N_BRDF_PARAMETERS(2)      = 0
    VBRDF_Sup_In%BS_BRDF_PARAMETERS(2,:)      = 0.0d0 ! (kernel,par)
    VBRDF_Sup_In%BS_LAMBERTIAN_KERNEL_FLAG(2) = .FALSE. 
    VBRDF_Sup_In%BS_BRDF_FACTORS(2)           = 0.0 ! <- Set later with fkern_amp_wvl


    ! --------------------------------------------------------
    ! Kernel 3 - Geo LiSparse
    ! --------------------------------------------------------  
    VBRDF_Sup_In%BS_BRDF_NAMES(3)             = BRDF_CHECK_NAMES(4)
    VBRDF_Sup_In%BS_WHICH_BRDF(3)             = 4
    VBRDF_Sup_In%BS_N_BRDF_PARAMETERS(3)      = 2
    VBRDF_Sup_In%BS_BRDF_PARAMETERS(3,1:2)    = (/2.0,1.0/) ! (kernel,par)
    VBRDF_Sup_In%BS_LAMBERTIAN_KERNEL_FLAG(3) = .FALSE. 
    VBRDF_Sup_In%BS_BRDF_FACTORS(3)           = 0.0 ! <- Set later with fkern_amp_wvl

    ! --------------------------------------------------------
    ! Kernel 4 - Ocean Glint
    ! --------------------------------------------------------

    VBRDF_Sup_In%BS_BRDF_NAMES(4)             = 'NewCMGlint'
    VBRDF_Sup_In%BS_WHICH_BRDF(4)             = NewCMGLINT_IDX
    VBRDF_Sup_In%BS_N_BRDF_PARAMETERS(4)      = 2
    VBRDF_Sup_In%BS_BRDF_PARAMETERS(4,1:2)    = (/wind_speed,salinity/)
    VBRDF_Sup_In%BS_LAMBERTIAN_KERNEL_FLAG(4) = .FALSE. 
    VBRDF_Sup_In%BS_BRDF_FACTORS(4)           = 0.0 ! <- Set later with fkern_amp_wvl

    ! I think this prevents looping over all BRDF types when set to true
    VBRDF_Sup_In%BS_DO_NewCMGLINT    = .FALSE. ! .true.

    ! Parameters specific to Cox-Munk param.
    VBRDF_Sup_In%BS_SALINITY         = salinity
    VBRDF_Sup_In%BS_WAVELENGTH       = 0.0d0 ! Set later
    VBRDF_Sup_In%BS_WINDSPEED        = wind_speed
    VBRDF_Sup_In%BS_WINDDIR(1)       = wind_dir - GC_sun_azimuths(1)
    IF( VBRDF_Sup_In%BS_WINDDIR(1) .LT. 0.0 ) THEN
      VBRDF_Sup_In%BS_WINDDIR(1) = VBRDF_Sup_In%BS_WINDDIR(1) + 360.0d0
    ENDIF
    VBRDF_Sup_In%BS_DO_GlintShadow    = .TRUE.
    VBRDF_Sup_In%BS_DO_FoamOption     = .TRUE.
    VBRDF_Sup_In%BS_DO_FacetIsotropy  = .FALSE.
    
    ! Set up linearizations
    IF( do_diag(28) ) THEN

      ! Count Jacobians
      Diag28_InpOpt%njac = 0

      ! Get number of weighting functions
      VBRDF_LinSup_In%BS_N_KERNEL_FACTOR_WFS = 0 
      VBRDF_LinSup_In%BS_N_KERNEL_PARAMS_WFS = 0
      IF( Diag28_InpOpt%do_factor_jac  ) VBRDF_LinSup_In%BS_N_KERNEL_FACTOR_WFS = 4
      IF( Diag28_InpOpt%do_par_jac     ) VBRDF_LinSup_In%BS_N_KERNEL_PARAMS_WFS = 4 
      VBRDF_LinSup_In%BS_N_SURFACE_WFS = VBRDF_LinSup_In%BS_N_KERNEL_FACTOR_WFS &
                                       + VBRDF_LinSup_In%BS_N_KERNEL_PARAMS_WFS
      Diag28_InpOpt%njac = VBRDF_LinSup_In%BS_N_SURFACE_WFS 

      ! Allocate arrays
      IF( Diag28_InpOpt%njac > 0 ) THEN
        ALLOCATE( Diag28_InpOpt%jac_name( Diag28_InpOpt%njac ) )
        ALLOCATE( Diag28_InpOpt%var(nlambdas,Diag28_InpOpt%njac ) )
      ENDIF

      ! Now loop over species to set jacobian names
      ct = 0
      DO i=1,4
        
        ! Get the index
        idx    = VBRDF_Sup_In%BS_WHICH_BRDF(i)
        npar   = VBRDF_Sup_In%BS_N_BRDF_PARAMETERS(i)
        par(:) = VBRDF_Sup_In%BS_BRDF_PARAMETERS(i,:) 

        ! Add factor jacobian
        IF( Diag28_InpOpt%do_factor_jac ) THEN
          ct = ct + 1
          Diag28_InpOpt%jac_name(ct)   = 'BRDF_f_' // short_kern_name( idx )
          Diag28_InpOpt%var(:,ct) = fkern_amp_wvl(:,i)

          ! Turn kernel factor linearization on
          VBRDF_LinSup_In%BS_DO_KERNEL_FACTOR_WFS(i) = .TRUE.

        ENDIF

        ! Add parameter jacobians
        IF( Diag28_InpOpt%do_par_jac ) THEN

          ! Set parameter jacobians 
          VBRDF_LinSup_In%BS_DO_KERNEL_PARAMS_WFS(i,1:npar) = .TRUE.
          VBRDF_LinSup_In%BS_DO_KPARAMS_DERIVS(i) = .TRUE.

          DO j=1,npar

            ct = ct + 1

            ! Write string
            WRITE(numstr,'(I500)') j

            ! Create output name
            Diag28_InpOpt%jac_name(ct) = 'BRDF_p' // TRIM(ADJUSTL(numstr)) // '_' &
                                       // short_kern_name( idx )
            Diag28_InpOpt%var(:,ct) = par(j)

          ENDDO

        ENDIF
        
      ENDDO
    
    ENDIF
    
  END SUBROUTINE set_SciaUSGS_vl_kernels

  SUBROUTINE compute_RTLS_albedo( sza, aza, vza, what_albedo, refl, nw, error  )
    
    ! ======================
    ! Input/Output variables
    ! ======================
    
    REAL (KIND=8), INTENT(IN)                       :: sza, aza, vza
    INTEGER (KIND=4), INTENT(IN)                    :: what_albedo, nw
    REAL (KIND=8), INTENT(OUT), DIMENSION(nw)       :: refl
    LOGICAL, INTENT(INOUT)  :: error
    
    ! Local variables
    
    INTEGER, PARAMETER       :: nband=4, nkernel=3, neof = 4
    INTEGER, PARAMETER       :: nsza0 = 10, nsnowpos = 480
    
    REAL (KIND=8), DIMENSION(nkernel), PARAMETER    :: wsg = (/1.0d0, 0.18984d0, -1.377622d0/)
    REAL (KIND=8), DIMENSION(3, nkernel), PARAMETER :: bsg = reshape ( (/ &
          1.0d0,       0.d0,        0.d0,      &
        -0.007574d0, -0.070987d0, 0.307588d0, &
        -1.284909d0, -0.166314d0, 0.041840d0 /), (/3,nkernel/) )
    
    INTEGER                                   :: i
    REAL(KIND=8)                              :: kgeo, kvol, sza2, sza3, wtemp,&
                                                 secsza, szafrac
    REAL(KIND=8), DIMENSION(nband)            :: mBRDF, dirfrac, fracslp
    
    REAL(KIND=8), DIMENSION(nsza0), SAVE        :: secsza0
    REAL(KIND=8), DIMENSION(nband, nsza0), SAVE :: dirfrac0, fracslp0
    
    LOGICAL, SAVE :: FIRST = .TRUE.
    
    ! ==============================================================
    ! compute_RTLS_albed starts here
    ! ==============================================================
    
    error = .FALSE.
    
    IF( FIRST ) THEN
    
      ! Read LUT of ratio of direct irradiance to total downwelling irradiance
      OPEN(UNIT=eofunit, file=trim(database_dir)//"/BRDF_EOF/AlbSpec/directflux_to_totdnflux_modis_band1-4.txt", status='old')
      READ (eofunit, *)
      READ (eofunit, *) secsza0
      secsza0 = 1.0/COS(secsza0 * deg2rad)
      DO i = 1, 4
        READ(eofunit, *)
      ENDDO
      DO i = 1, nband
        READ(eofunit, *) wtemp, dirfrac0(i, 1:nsza0), fracslp0(i, 1:nsza0)
      ENDDO
      CLOSE(UNIT = eofunit)
      
    ENDIF
    
    ! Reflectance/Albedo 
    mBRDF(:) = 0.0
    IF (VBRDF_Sup_In%BS_MODISPCA_landfrac > 0.0) THEN ! At least partial land
      
      IF (what_albedo == 3) THEN
          
          secsza = 1.0/COS(sza * deg2rad)
          
          IF (secsza <= secsza0(1)) THEN
            dirfrac = dirfrac0(:, 1)
            fracslp = fracslp0(:, 1)
          ELSE IF (secsza >= secsza0(nsza0)) THEN
            dirfrac = dirfrac0(:, nsza0)
            fracslp = fracslp0(:, nsza0)
          ELSE
            DO i = 2, nsza0
                IF (secsza < secsza0(i)) EXIT
            ENDDO
            szafrac = (secsza - secsza0(i-1)) / (secsza0(i) - secsza0(i-1))
            dirfrac = dirfrac0(:, i-1) * (1.0 - szafrac) + dirfrac0(:, i) * szafrac
            fracslp = fracslp0(:, i-1) * (1.0 - szafrac) + fracslp0(:, i) * szafrac
          ENDIF
      ENDIF
      
      IF (what_albedo == 0) THEN   ! Directional albedo
          !Get BRDF kernels at MODIS points for viewing geometry
          CALL BRDF_kernels(sza, aza, vza, kvol, kgeo)
      ELSE IF (what_albedo == 1 .OR. what_albedo == 3) THEN  ! For Black/Blue albedo
          sza2 = (sza * deg2rad) ** 2
          sza3 = sza2 * sza * deg2rad
          kvol = bsg(1, 2) + sza2 * bsg(2, 2) + sza3 * bsg(3, 2)
          kgeo = bsg(1, 3) + sza2 * bsg(2, 3) + sza3 * bsg(3, 3)
      ELSE IF (what_albedo == 2) THEN  ! White albedo
          kvol = wsg(2); kgeo = wsg(3)
      ENDIF
      
      !Calculate BRDF/Directional Reflectance at MODIS points
      IF (what_albedo <= 2) THEN
          DO i = 1, nband
            mBRDF(i) = VBRDF_Sup_In%BS_MODIS_FIso(i) &
                     + VBRDF_Sup_In%BS_MODIS_FVol(i)*kvol &
                     + VBRDF_Sup_In%BS_MODIS_FGeo(i)*kgeo
          ENDDO
      ELSE IF (what_albedo == 3) THEN
          CALL calculate_blueskyalb(nband, &
                                    VBRDF_Sup_In%BS_MODIS_FIso(:),&
                                    VBRDF_Sup_In%BS_MODIS_FVol(:),&
                                    VBRDF_Sup_In%BS_MODIS_FGeo(:),&
                                    kvol, kgeo, wsg(2), wsg(3), &
                                    dirfrac, fracslp, mBRDF)
      ENDIF
      
    ENDIF
    
    ! Compute hyperspectral reflectance
    CALL compute_SciaUSGS_Coeff( mBRDF, refl, nw )
      
  END SUBROUTINE compute_RTLS_albedo
  
  SUBROUTINE BRDF_kernels(sza, aza, vza, kvol, kgeo)
  
    USE GC_parameters_module, ONLY: pi, deg2rad
    
    IMPLICIT NONE

    ! ======================
    ! Input/Output variables
    ! ======================
    REAL (KIND=8), INTENT(IN)  :: sza, aza, vza
    REAL (KIND=8), INTENT(OUT) :: kvol, kgeo

    ! ======================
    ! Local variables
    ! ======================
    !Ratio of elevation to height of (spherical) tree crowns
    REAL (KIND=8), PARAMETER   :: hb = 2.0d0 
    
    REAL (KIND=8) :: sza1, aza1, vza1, cossza, cosaza, cosvza, &
        sinsza, sinaza, sinvza, tansza, tanvza, secsza, secvza, &
        cosxi, xi, sinxi, d, o, t, cost
    
    sza1 = sza * deg2rad
    !aza1 = aza * deg2rad  
    aza1 = (180.0 - aza) * deg2rad  ! VLIDORT convention is different from MODIS BRDF convention 
    vza1 = vza * deg2rad

    cossza = COS(sza1)
    cosaza = COS(aza1)
    cosvza = COS(vza1)

    sinsza = SIN(sza1)
    sinaza = SIN(aza1)
    sinvza = SIN(vza1)

    tansza = TAN(sza1)
    tanvza = TAN(vza1)

    secsza = 1.d0 / cossza
    secvza = 1.d0 / cosvza
    
    cosxi = cossza * cosvza + sinsza * sinvza * cosaza 

    xi = ACOS(cosxi)

    sinxi = SIN(xi)
    
    ! RossThick
    kvol = ( ( (pi /2.0d0 - xi) * cosxi + sinxi ) / (cossza + cosvza)) - (pi / 4.0d0)

    d = SQRT( tansza**2 + tanvza**2 - 2 * tansza * tanvza * cosaza)

    cost = hb * SQRT(d**2 + (tansza * tanvza * sinaza)**2) / (secsza + secvza)

    IF (cost > 1) THEN
      cost = 1.0d0
    ENDIF

    t = ACOS(cost)

    o = (1.0d0 / pi) * (t - SIN(t) * cost) * (secsza + secvza)
    
    ! LiSparse
    kgeo = o - secsza - secvza + (1 + cosxi) * secsza * secvza / 2.d0

  END SUBROUTINE BRDF_kernels

  SUBROUTINE calculate_blueskyalb(nw, fiso,fvol,fgeo, kvol1, kgeo1, kvol2, kgeo2, dirfrac, fracslp, alb) 
  
    
    IMPLICIT NONE
    
    ! ======================
    ! Input/Output variables
    ! ======================
    INTEGER (KIND=4), INTENT(IN)                      :: nw
    REAL (KIND=8), INTENT(IN)                         :: kgeo1, kvol1, kgeo2, kvol2
    REAL (KIND=8), DIMENSION(nw), INTENT(IN)          :: fiso,fvol,fgeo
    REAL (KIND=8), DIMENSION(nw), INTENT(IN)          :: dirfrac, fracslp
    REAL (KIND=8), DIMENSION(nw), INTENT(OUT)         :: alb

    ! ======================
    !    Local variables
    ! ======================
    INTEGER                       :: i
    REAL (KIND=8), DIMENSION(nw) :: balb, walb, dalb, frac

    DO i = 1, nw
      balb(i) = fiso(i) + kvol1 * fvol(i) + kgeo1 * fgeo(i)
      walb(i) = fiso(i) + kvol2 * fvol(i) + kgeo2 * fgeo(i)
    enddo

    alb = balb * dirfrac + walb * ( 1.0 - dirfrac) ! use dirfrac (i.e., albedo=0.001)
    dalb = 1.0
    ! Iterative Derivation of blue-sky albedo as weighting depends on surface albedo
    DO WHILE (ANY(dalb > 1.0E-4) ) 
      frac = dirfrac + fracslp * (alb - 0.001) ! Derive actual fraction of direct irradiance for given alb
      dalb = balb * frac + walb * (1.0 - frac) - alb
      alb = alb + dalb
    ENDDO
    
    RETURN
  END SUBROUTINE calculate_blueskyalb
  
   SUBROUTINE READ_MODIS_BRDF_HR (elons, elats, daystr, dayfrac)
  
    !     USE HDF5
    USE GC_variables_module, ONLY : modis_brdfs, modis_lfrac, &
                                    nx_modis, ny_modis, modis_field_i2, &
                                    modis_field_i1, &
                                    modis_lon, modis_lat
    
    IMPLICIT NONE
    
    ! Input arguments
    REAL(KIND=8), DIMENSION(2),     INTENT(IN) :: elons, elats, dayfrac
    CHARACTER(LEN=3),DIMENSION(2),  INTENT(IN) :: daystr
    
    ! Parameter and function declarations, and local variables to read hdf data
    INTEGER, PARAMETER :: DFACC_READ = 1
    INTEGER            :: sfstart, sfn2index, sfselect, sfrdata, sfendacc, sfend
    INTEGER            :: sd_id, sds_id, sds_index, status
    integer            :: start(32), edges(32), stride(32)
    
    LOGICAL, SAVE :: FIRST = .TRUE.
    INTEGER                        :: n, i, j, k, nx_new, ny_new, nx_aloc, ny_aloc
    INTEGER, DIMENSION(2)          :: mlats, mlons
    CHARACTER (LEN=max_ch_len)     :: modis_fname
    
    ! Resolution is 30 arcsec
    REAL(KIND=8), PARAMETER  :: modis_dlon = 0.5d0/60.0d0, modis_dlat = 0.5d0/60.d0
    
    ! BRDF types
    CHARACTER (LEN=3), DIMENSION(3), PARAMETER :: brdf_name = (/'iso','vol','geo'/)
    
    ! Band Band1=648nm Band2=858nm, Band3=470nm, Band4=555nm 
    ! Output goes from low to high
    CHARACTER (LEN=1), DIMENSION(4), PARAMETER :: band_name = (/'3','4','1','2'/)
    
    INTEGER, PARAMETER       :: nband = 4, nkernel = 3!, nmlon = 2160, nmlat = 1080
    ! Return values
    ! modis_brdfs
    ! modis_land
    
    ! ==============================================================================
    
    IF( do_debug_geocape_tool ) THEN
      print*,'elats:',elats
      print*,'elons:',elons
    ENDIF
    
  !   !Find MODIS for this lon/lat range
  !   mlats = NINT((90.0d0 - elats ) / modis_dlat) + 1   !To agree with MODIS convention
  !   mlons = NINT((elons + 180.0d0) / modis_dlon) + 1
    
    ! Expanded range now that we are using PNPOLY
    mlats(1) = CEILING((90.0d0 - elats(1) ) / modis_dlat)  + 1 
    mlats(2) =   FLOOR((90.0d0 - elats(2) ) / modis_dlat)  + 1 
    mlons(1) =   FLOOR((elons(1) + 180.0d0 ) / modis_dlon) + 1 
    mlons(2) = CEILING((elons(2) + 180.0d0 ) / modis_dlon) + 1 
    
    IF( do_debug_geocape_tool ) THEN
      print*,'mlats:',mlats
      print*,'mlons:',mlons
      print*,  90.0d0 - REAL( (mlats(1)-1), KIND=8)*modis_dlat 
      print*,  90.0d0 - REAL( (mlats(2)-1), KIND=8)*modis_dlat
      print*,-180.0d0 + REAL( (mlons(1)-1), KIND=8)*modis_dlon
      print*,-180.0d0 + REAL( (mlons(2)-1), KIND=8)*modis_dlon
    ENDIF
    
    ! Sort lon, lat indices in increasing order
    IF (mlons(2) < mlons(1)) THEN
      n = mlons(1)
      mlons(1) = mlons(2)
      mlons(2) = n
    ENDIF
    
    IF (mlats(2) < mlats(1)) THEN
      n = mlats(1)
      mlats(1) = mlats(2)
      mlats(2) = n
    ENDIF
    
    
    ! Allocate arrays on first pass
    IF( FIRST ) THEN 
      
      ! Points in field
      nx_modis = mlons(2)-mlons(1)+1
      ny_modis = mlats(2)-mlats(1)+1
      
      ALLOCATE(modis_field_i2(nx_modis, ny_modis))
      ALLOCATE(modis_field_i1(nx_modis, ny_modis))
      modis_field_i2 = 0
      modis_field_i1 = 0
      
      ! Allocate modis BRDF arrays
      ALLOCATE(modis_brdfs(nx_modis, ny_modis, nband, nkernel))
      ALLOCATE(modis_lfrac(nx_modis, ny_modis)                )
      
      ! Allocate modis lon/lat
      ALLOCATE(modis_lon(nx_modis) )
      ALLOCATE(modis_lat(ny_modis) )
      
      FIRST = .FALSE.
    
    ELSE
      
      ! Check dimensions
      nx_new = mlons(2)-mlons(1)+1
      ny_new = mlats(2)-mlats(1)+1
      
      ! Expand array dimension if necessary
      IF( nx_new > nx_modis .OR. ny_new > ny_modis ) THEN
        
        ! Dimensions for array reallocation
        nx_aloc = max( nx_new, nx_modis )
        ny_aloc = max( ny_new, ny_modis )
        
        IF(ALLOCATED(modis_field_i2)) DEALLOCATE(modis_field_i2)
        IF(ALLOCATED(modis_field_i1)) DEALLOCATE(modis_field_i1)
        IF(ALLOCATED(modis_brdfs)   ) DEALLOCATE(modis_brdfs   )
        IF(ALLOCATED(modis_lfrac)   ) DEALLOCATE(modis_lfrac   )
        IF(ALLOCATED(modis_lon)     ) DEALLOCATE(modis_lon     )
        IF(ALLOCATED(modis_lat)     ) DEALLOCATE(modis_lat     )
        
        ! Allocate fields
        ALLOCATE(modis_field_i2(nx_aloc, ny_aloc))
        ALLOCATE(modis_field_i1(nx_aloc, ny_aloc))
        modis_field_i2 = 0
        modis_field_i1 = 0
        
        ! Allocate modis BRDF arrays
        ALLOCATE(modis_brdfs(nx_aloc, ny_aloc, nband, nkernel))
        ALLOCATE(modis_lfrac(nx_aloc, ny_aloc)                )
        
        ! Allocate modis lon/lat
      ALLOCATE(modis_lon(nx_aloc) )
      ALLOCATE(modis_lat(ny_aloc) )
        
      ENDIF
      
      nx_modis = nx_new
      ny_modis = ny_new
      
    ENDIF
    
    ! Compute lon/lat coordinates
    DO i=1,nx_modis
      modis_lon(i) = REAL( (mlons(1)+i-2), KIND=8 )*modis_dlon-180.0d0
  !     print*,i,modis_lon(i)
    ENDDO
    DO j=1,ny_modis
      modis_lat(j) = 90.0d0 - REAL( (mlats(1)+j-2), KIND=8 )*modis_dlat
  !     print*,j,modis_lat(j)
    ENDDO
    
    start(:) = 0
    edges(:) = 0
    stride(:) = 0
    
    start(1) = mlons(1)-1
    start(2) = mlats(1)-1
    edges(1) = nx_modis! mlons(2)
    edges(2) = ny_modis! mlats(2)
    stride(1:2) = 1
    
    ! Make sure array is zeroed
    modis_brdfs(:,:,:,:) = 0.0d0
    
    DO i=1,2
      
      DO j=1,3
        
        DO k=1,4
        
          ! Load Reflectivities
          modis_fname = trim(database_dir)//'/BRDF_EOF/AlbSpec/MODIS_AVG_BRDF/MCD43GF_AVG_' &
                      // brdf_name(j) // '_Band' // band_name(k) // '_' &
                      // daystr(i) // '_2002-2011.hdf'
        
          IF( do_debug_geocape_tool ) print*,TRIM(modis_fname)
          
          ! Open HDF4
          sd_id = sfstart(TRIM(modis_fname), DFACC_READ)
  !           
          ! Create dataset id for BRDF
          sds_index = sfn2index(sd_id, 'Weighted_Mean')
          sds_id = sfselect(sd_id, sds_index)
          
          ! Read reflectivities
          status = sfrdata(sds_id, start, stride, edges, modis_field_i2(1:nx_modis,1:ny_modis))
          status = sfendacc(sds_id)  ! Terminate the access to the dataset
          
          ! Close HDF file
          status = sfend(sd_id)
          
          ! Store data
          modis_brdfs(1:nx_modis,1:ny_modis,k,j)       &
            = modis_brdfs(1:nx_modis,1:ny_modis,k,j)  &
            + REAL( modis_field_i2(1:nx_modis,1:ny_modis) ,KIND=8 ) * dayfrac(i)
          
          
        ENDDO
      ENDDO
    ENDDO
    
    ! Load Land cover
    modis_fname = trim(database_dir)//'BRDF_EOF/AlbSpec/Land_Water_Mask_7Classes_UMD.hdf'
    
    IF( do_debug_geocape_tool ) print*,TRIM(modis_fname)
    
    ! Open HDF4
    sd_id = sfstart(TRIM(modis_fname), DFACC_READ)
    
    ! Create dataset id for BRDF
    sds_index = sfn2index(sd_id, 'LW_MASK_UMD')
    sds_id = sfselect(sd_id, sds_index)
    
    ! Read Landcover
    status = sfrdata(sds_id, start, stride, edges, modis_field_i1(1:nx_modis,1:ny_modis))
    status = sfendacc(sds_id)  ! Terminate the access to the dataset
    
    ! Close HDF file
    status = sfend(sd_id)
    
    ! Flag definitions
    ! 0 Shallow ocean
    ! 1 Land only
    ! 2 Ocean coastlines and lake shores
    ! 3 Shallow inland water
    ! 4 Ephemeral water
    ! 5 Deep inland water
    ! 6 Moderate or continental ocean
    ! 7 Deep ocean
    
    modis_lfrac(:,:) = 0.0d0
    DO i=1,nx_modis
    DO j=1,ny_modis
      IF( modis_field_i1(i,j) .EQ. 1 .OR. &
          modis_field_i1(i,j) .EQ. 2 ) modis_lfrac(i,j) = 1.0d0
    ENDDO
    ENDDO
    
  END SUBROUTINE READ_MODIS_BRDF_HR 
  
  LOGICAL FUNCTION PNPOLY_r8( nvert, vertx, verty, testx, testy)
    
    IMPLICIT NONE
    
    ! Return variable
    ! ---------------
    !LOGICAL :: PNPOLY ! True if point is in polygon
    
    ! Input
    INTEGER :: nvert ! # Vertices in polygon
    REAL(KIND=8), DIMENSION(nvert) :: vertx ! X coordinates of the polygon vertices
    REAL(KIND=8), DIMENSION(nvert) :: verty ! Y coordinates of the polygon vertices
    REAL(KIND=8)                   :: testx ! X coordinate of test point
    REAL(KIND=8)                   :: testy ! Y coordinate of test point
    
    ! Local variables
    INTEGER :: i, j
    
    ! ================================================
    ! FUNCTION PNPOLY starts here
    ! ================================================
    
    ! Initialize PNPOLY
    PNPOLY_r8 = .FALSE.
    
    ! Initialize j
    j = nvert
    
    ! Perform crossings test (Jordan curve thm.) to see if point is in polygon
    DO i=1,nvert
      
      IF ( ((verty(i) .gt. testy) .neqv. (verty(j) .gt. testy)) .and. &
          (testx .lt. (vertx(j)-vertx(i)) * &
          (testy-verty(i)) / (verty(j)-verty(i)) + vertx(i)) ) THEN
         
         PNPOLY_r8 = .NOT. PNPOLY_r8
         
      ENDIF
      
      j = i
      
    ENDDO
    
    RETURN
    
  END FUNCTION PNPOLY_r8
  
  SUBROUTINE compute_SciaUSGS_Coeff( modis_refl, hyprefl, nw, land_only )
    
    ! Input/output variables
    REAL(KIND=8), INTENT(IN), DIMENSION(4)           :: modis_refl
    INTEGER,      INTENT(IN)                         :: nw
    REAL(KIND=8), INTENT(OUT), DIMENSION(nw)         :: hyprefl
    LOGICAL,      INTENT(IN),  OPTIONAL              :: land_only
    ! Local variables
    REAL(KIND=8), DIMENSION(4)      :: refl_diff
    REAL(KIND=8), DIMENSION(fa_nfa) :: coeff
    
    INTEGER :: i,j
    LOGICAL :: weight_land

    ! ==============================================================
    ! compute_SciaUSGS_Coeff starts here
    ! ==============================================================
    
    IF( PRESENT(land_only) ) THEN
      weight_land = .NOT. land_only
    ELSE
      weight_land = .TRUE.
    ENDIF

    IF( VBRDF_Sup_In%BS_MODISPCA_landfrac .GT. 0.0 ) THEN
    
      ! Subtract mean
      refl_diff = modis_refl - fa_mu_m
      
      ! Compute coefficients
      coeff(:) = 0.0
      DO i=1,fa_nfa
      DO j=1,fa_nmod
        coeff(i) = coeff(i) + fa_G(j,i)*refl_diff(j)
      ENDDO
      ENDDO
      
      ! Output reflectance
      hyprefl(:) = 0.0
      
      ! Initialize with mean
      hyprefl = fa_mu_int
      
      ! Add factor loadings
      DO i=1,fa_nfa
        hyprefl = hyprefl + coeff(i)*fa_W_int(i,:)
      ENDDO
      
      ! Now weight by snow/water fractions
      IF( weight_land ) THEN
        hyprefl(:) = VBRDF_Sup_In%BS_MODISPCA_landfrac*&
                     ( hyprefl(:)*(1.0d0-prof_snowfrac_eff)+prof_snowfrac_eff*out_snowspec(:)) &
                   + (1.0d0-VBRDF_Sup_In%BS_MODISPCA_landfrac)*&
                     (usgs_water_int(:)*(1.0d0-prof_seaicefrac)+prof_seaicefrac*out_snowspec(:))
      ENDIF
    ELSE
      IF( weight_land ) THEN
        hyprefl(:) = usgs_water_int(:)*(1.0d0-prof_seaicefrac)+prof_seaicefrac*out_snowspec(:)
      ELSE
        hyprefl(:) = 0.0d0
      ENDIF
    ENDIF

  END SUBROUTINE compute_SciaUSGS_Coeff
  
  SUBROUTINE compute_SciaUSGS_Coeff_widx( modis_refl, hyprefl, widx )
    
    ! Input/output variables
    REAL(KIND=8), INTENT(IN), DIMENSION(4)           :: modis_refl
    REAL(KIND=8), INTENT(OUT)                        :: hyprefl
    INTEGER,      INTENT(IN)                         :: widx
    ! Local variables
    REAL(KIND=8), DIMENSION(4)      :: refl_diff
    REAL(KIND=8), DIMENSION(fa_nfa) :: coeff
    
    INTEGER :: i,j
    ! ==============================================================
    ! compute_SciaUSGS_Coeff starts here
    ! ==============================================================
    
    IF( VBRDF_Sup_In%BS_MODISPCA_landfrac .GT. 0.0 ) THEN
    
      ! Subtract mean
      refl_diff = modis_refl - fa_mu_m
      
      ! Compute coefficients
      coeff(:) = 0.0
      DO i=1,fa_nfa
      DO j=1,fa_nmod
        coeff(i) = coeff(i) + fa_G(j,i)*refl_diff(j)
      ENDDO
      ENDDO
      
      ! Output reflectance
      hyprefl = 0.0
      
      ! Initialize with mean
      hyprefl = fa_mu_int(widx)
      
      ! Add factor loadings
      DO i=1,fa_nfa
        hyprefl = hyprefl + coeff(i)*fa_W_int(i,widx)
      ENDDO
      
      ! Now weight by snow/water fractions
      hyprefl = VBRDF_Sup_In%BS_MODISPCA_landfrac*&
              ( hyprefl*(1.0d0-prof_snowfrac_eff)+prof_snowfrac_eff*out_snowspec(widx)) &
              + (1.0d0-VBRDF_Sup_In%BS_MODISPCA_landfrac)*&
              (usgs_water_int(widx)*(1.0d0-prof_seaicefrac)+prof_seaicefrac*out_snowspec(widx))
    ELSE
      hyprefl =  usgs_water_int(widx)*(1.0d0-prof_seaicefrac)+prof_seaicefrac*out_snowspec(widx)
    ENDIF
    
  END SUBROUTINE compute_SciaUSGS_Coeff_widx
  
  SUBROUTINE precompute_VLIDORT_BRDF_arrays(DO_DEBUG_RESTORATION, &
                                            NMOMENTS_INPUT, error )
    
    
        USE GC_variables_module, ONLY : VBRDF_Sup_In,        &
                                        BRDF_Sup_ModOut,     &
                                        BS_DBOUNCE_BRDFUNC, &
                                        BS_BRDF_F,&
                                        BS_BRDF_F_0,&
                                        BS_USER_BRDF_F_0,&
                                        BS_USER_BRDF_F,&
                                        BS_EMISSIVITY,&
                                        BS_USER_EMISSIVITY, &
                                        BS_NBEAMS, &
                                        BS_N_USER_RELAZMS, &
                                        BS_N_USER_STREAMS, &
                                        BS_NMOMENTS
        USE VLIDORT_PARS
        USE vbrdf_sup_inputs_def
        USE vbrdf_sup_outputs_def

        USE vbrdf_sup_aux_m, only : BRDF_GAULEG,              &
                                    BRDF_QUADRATURE_Gaussian, &
                                    BRDF_QUADRATURE_Trapezoid

        USE vbrdf_sup_kernels_m
        USE vbrdf_sup_routines_m
        
        IMPLICIT NONE
  
!  Inputs
!  ------
  

!  Debug flag for restoration

      LOGICAL, INTENT(IN) ::              DO_DEBUG_RESTORATION

!  Input number of moments (only used for restoration debug)
      
      INTEGER, INTENT(IN) ::              NMOMENTS_INPUT
    
      LOGICAL, INTENT(INOUT) :: error
    
!  Input structure
!  ---------------

!       TYPE(VBRDF_Sup_Inputs), INTENT(IN)   :: VBRDF_Sup_In

!  Output structure
!  ----------------

!       TYPE(VBRDF_Sup_Outputs), INTENT(OUT) :: VBRDF_Sup_Out

!  Exception handling introduced 02 April 2014 for Version 2.7

!       TYPE(VBRDF_Output_Exception_Handling), INTENT(OUT) :: VBRDF_Sup_OutputStatus
  
 !VLIDORT local variables
 !++++++++++++++++++++++

!  Input arguments
!  ===============

!  User stream Control

      LOGICAL ::          DO_USER_STREAMS

!  Surface emission

      LOGICAL ::          DO_SURFACE_EMISSION

!  number of Stokes components

      INTEGER ::          NSTOKES

!   Number and index-list of bidirectional functions

      INTEGER ::          N_BRDF_KERNELS
      INTEGER ::          WHICH_BRDF ( MAX_BRDF_KERNELS )

!  Parameters required for Kernel families

      INTEGER ::          N_BRDF_PARAMETERS ( MAX_BRDF_KERNELS )
      DOUBLE PRECISION :: BRDF_PARAMETERS &
          ( MAX_BRDF_KERNELS, MAX_BRDF_PARAMETERS )

!  BRDF names

      CHARACTER (LEN=10) :: BRDF_NAMES ( MAX_BRDF_KERNELS )

!  Lambertian Surface control

      LOGICAL ::          LAMBERTIAN_KERNEL_FLAG ( MAX_BRDF_KERNELS )

!  Input kernel amplitude factors

      DOUBLE PRECISION :: BRDF_FACTORS ( MAX_BRDF_KERNELS )

!  WSA and BSA scaling options.
!   Revised, 14-15 April 2014, first introduced 02 April 2014, Version 2.7
!      WSA = White-sky albedo. BSA = Black-sky albedo.
!  Rob Fix 9/27/14. Output variable added

      LOGICAL   :: DO_WSABSA_OUTPUT
      LOGICAL   :: DO_WSA_SCALING
      LOGICAL   :: DO_BSA_SCALING
      REAL(fpk) :: WSA_VALUE, BSA_VALUE

!  Number of azimuth quadrature streams for BRDF

      INTEGER ::          NSTREAMS_BRDF

!  Shadowing effect flag (only for Cox-Munk type kernels)

      LOGICAL ::          DO_SHADOW_EFFECT

!   !@@ Solar sources + Observational Geometry flag !@@

      LOGICAL ::          DO_SOLAR_SOURCES
      LOGICAL ::          DO_USER_OBSGEOMS

!  Rob Fix 9/25/14. Two variables replaced
!   Flag for the Direct-bounce term, replaces former "EXACT" variables 
!   Exact flag (!@@) and Exact only flag --> no Fourier term calculations
!      LOGICAL ::          DO_EXACT
!      LOGICAL ::          DO_EXACTONLY
      LOGICAL ::          DO_DBONLY

!  Multiple reflectance correction for Glitter kernels

      LOGICAL ::          DO_MSRCORR
      LOGICAL ::          DO_MSRCORR_DBONLY   !  name change
      INTEGER ::          MSRCORR_ORDER
      INTEGER ::          N_MUQUAD, N_PHIQUAD

!  Local angle control

      INTEGER ::          NSTREAMS
      INTEGER ::          NBEAMS
      INTEGER ::          N_USER_STREAMS
      INTEGER ::          N_USER_RELAZMS

!  Local angles

      DOUBLE PRECISION :: BEAM_SZAS   (MAXBEAMS)
      DOUBLE PRECISION :: USER_RELAZMS(MAX_USER_RELAZMS)
      DOUBLE PRECISION :: USER_ANGLES (MAX_USER_STREAMS)

!  !@@ Local Observational Geometry control and angles

      INTEGER ::          N_USER_OBSGEOMS
      DOUBLE PRECISION :: USER_OBSGEOMS (MAX_USER_OBSGEOMS,3)

!  New Cox-Munk Glint reflectance options (bypasses the usual Kernel system)
!  -------------------------------------------------------------------------

!  Overall flag for this option

      LOGICAL   :: DO_NewCMGLINT

!  Input Salinity in [ppt]

      REAL(fpk) :: SALINITY

!  Input wavelength in [Microns]

      REAL(fpk) :: WAVELENGTH

!  Input Wind speed in m/s, and azimuth directions relative to Sun positions

      REAL(fpk) :: WINDSPEED, WINDDIR ( MAXBEAMS )

!  Flags for glint shadowing, Foam Correction, facet Isotropy

      LOGICAL   :: DO_GlintShadow
      LOGICAL   :: DO_FoamOption
      LOGICAL   :: DO_FacetIsotropy

!  BRDF External functions
!  =======================

!  lambertian

!      EXTERNAL         LAMBERTIAN_VFUNCTION

!  Modis-type kernels

!      EXTERNAL         ROSSTHIN_VFUNCTION
!      EXTERNAL         ROSSTHICK_VFUNCTION
!      EXTERNAL         LISPARSE_VFUNCTION
!      EXTERNAL         LIDENSE_VFUNCTION
!      EXTERNAL         HAPKE_VFUNCTION
!      EXTERNAL         ROUJEAN_VFUNCTION
!      EXTERNAL         RAHMAN_VFUNCTION

!  Cox-munk types

!      EXTERNAL         COXMUNK_VFUNCTION
!      EXTERNAL         COXMUNK_VFUNCTION_DB
!      EXTERNAL         GISSCOXMUNK_VFUNCTION
!      EXTERNAL         GISSCOXMUNK_VFUNCTION_DB

!  GCM CRI is not an external call
!      EXTERNAL         GCMCRI_VFUNCTION
!      EXTERNAL         GCMCRI_VFUNCTION_DB

!  new for Version 2.4R, introduced 30 April 2009, 6 May 2009
!    2009 function is final Kernel supplied by Breon, May 5 2009.

!      EXTERNAL         BPDF2009_VFUNCTION

!  Local BRDF functions
!  ====================

! !  at quadrature (discrete ordinate) angles
! 
!       DOUBLE PRECISION :: BRDFUNC &
!           ( MAXSTOKES_SQ, MAXSTREAMS, MAXSTREAMS, MAXSTREAMS_BRDF, nlambdas )
!       DOUBLE PRECISION :: BRDFUNC_0 &
!           ( MAXSTOKES_SQ, MAXSTREAMS, MAXBEAMS, MAXSTREAMS_BRDF, nlambdas )
! 
! !  at user-defined stream directions
! 
!       DOUBLE PRECISION :: USER_BRDFUNC &
!           ( MAXSTOKES_SQ, MAX_USER_STREAMS, MAXSTREAMS, MAXSTREAMS_BRDF, nlambdas )
!       DOUBLE PRECISION :: USER_BRDFUNC_0 &
!           ( MAXSTOKES_SQ, MAX_USER_STREAMS, MAXBEAMS, MAXSTREAMS_BRDF, nlambdas )

!  at quadrature (discrete ordinate) angles

      DOUBLE PRECISION :: BRDFUNC &
          ( MAXSTOKES_SQ, MAXSTREAMS, MAXSTREAMS, MAXSTREAMS_BRDF )
      DOUBLE PRECISION :: BRDFUNC_0 &
          ( MAXSTOKES_SQ, MAXSTREAMS, MAXBEAMS, MAXSTREAMS_BRDF )

!  at user-defined stream directions

      DOUBLE PRECISION :: USER_BRDFUNC &
          ( MAXSTOKES_SQ, MAX_USER_STREAMS, MAXSTREAMS, MAXSTREAMS_BRDF )
      DOUBLE PRECISION :: USER_BRDFUNC_0 &
          ( MAXSTOKES_SQ, MAX_USER_STREAMS, MAXBEAMS, MAXSTREAMS_BRDF )

!  DB Kernel values

!       DOUBLE PRECISION :: DBKERNEL_BRDFUNC &
!           ( MAXSTOKES_SQ, MAX_USER_STREAMS, MAX_USER_RELAZMS, MAXBEAMS )

!  Values for Emissivity

!       DOUBLE PRECISION :: EBRDFUNC &
!           ( MAXSTOKES_SQ, MAXSTREAMS, MAXSTHALF_BRDF, MAXSTREAMS_BRDF)

!  Local angles, and cosine/sines/weights
!  ======================================

!  Azimuths

      DOUBLE PRECISION :: PHIANG(MAX_USER_RELAZMS)
      DOUBLE PRECISION :: COSPHI(MAX_USER_RELAZMS)
      DOUBLE PRECISION :: SINPHI(MAX_USER_RELAZMS)

!  SZAs

      DOUBLE PRECISION :: SZASURCOS(MAXBEAMS)
      DOUBLE PRECISION :: SZASURSIN(MAXBEAMS)

!  Discrete ordinates (output)

      DOUBLE PRECISION :: QUAD_STREAMS(MAXSTREAMS)
      DOUBLE PRECISION :: QUAD_WEIGHTS(MAXSTREAMS)
      DOUBLE PRECISION :: QUAD_SINES  (MAXSTREAMS)

!  Viewing zenith streams

      DOUBLE PRECISION :: USER_STREAMS(MAX_USER_STREAMS)
      DOUBLE PRECISION :: USER_SINES  (MAX_USER_STREAMS)

!  BRDF azimuth quadrature streams

      INTEGER ::          NBRDF_HALF
      DOUBLE PRECISION :: X_BRDF  ( MAXSTREAMS_BRDF )
      DOUBLE PRECISION :: CX_BRDF ( MAXSTREAMS_BRDF )
      DOUBLE PRECISION :: SX_BRDF ( MAXSTREAMS_BRDF )
      DOUBLE PRECISION :: A_BRDF  ( MAXSTREAMS_BRDF )

!  BRDF azimuth quadrature streams For emission calculations

      DOUBLE PRECISION :: BAX_BRDF ( MAXSTHALF_BRDF )
      DOUBLE PRECISION :: CXE_BRDF ( MAXSTHALF_BRDF )
      DOUBLE PRECISION :: SXE_BRDF ( MAXSTHALF_BRDF )

!  Azimuth factors

!       DOUBLE PRECISION :: BRDF_COSAZMFAC(MAXSTREAMS_BRDF)
!       DOUBLE PRECISION :: BRDF_SINAZMFAC(MAXSTREAMS_BRDF)

!  Local arrays for MSR quadrature

      DOUBLE PRECISION :: X_MUQUAD (max_msrs_muquad)
      DOUBLE PRECISION :: W_MUQUAD (max_msrs_muquad)
      DOUBLE PRECISION :: SX_MUQUAD (max_msrs_muquad)
      DOUBLE PRECISION :: WXX_MUQUAD (max_msrs_muquad)

      DOUBLE PRECISION :: X_PHIQUAD (max_msrs_phiquad)
      DOUBLE PRECISION :: W_PHIQUAD (max_msrs_phiquad)

!  Local kernel Fourier components
!  ===============================

!  at quadrature (discrete ordinate) angles

      DOUBLE PRECISION :: LOCAL_BRDF_F &
          ( MAXSTOKES_SQ, MAXSTREAMS, MAXSTREAMS )
      DOUBLE PRECISION :: LOCAL_BRDF_F_0 &
          ( MAXSTOKES_SQ, MAXSTREAMS, MAXBEAMS   )

!  at user-defined stream directions

      DOUBLE PRECISION :: LOCAL_USER_BRDF_F &
          ( MAXSTOKES_SQ, MAX_USER_STREAMS, MAXSTREAMS )
      DOUBLE PRECISION :: LOCAL_USER_BRDF_F_0 &
          ( MAXSTOKES_SQ, MAX_USER_STREAMS, MAXBEAMS   )

!  Exception handling. New code, 02 April 2014. Version 2.7
!     Message Length should be at least 120 Characters

      INTEGER ::             STATUS
      INTEGER ::             NMESSAGES
      CHARACTER (LEN=120) :: MESSAGES ( 0:MAX_MESSAGES )

!  Other local variables
!  =====================

!  Discrete ordinates (local, for Albedo scaling). Version 2.7.

      INTEGER            :: SCALING_NSTREAMS
      DOUBLE PRECISION   :: SCALING_QUAD_STREAMS(MAXSTREAMS_SCALING)
      DOUBLE PRECISION   :: SCALING_QUAD_WEIGHTS(MAXSTREAMS_SCALING)
      DOUBLE PRECISION   :: SCALING_QUAD_SINES  (MAXSTREAMS_SCALING)
      DOUBLE PRECISION   :: SCALING_QUAD_STRMWTS(MAXSTREAMS_SCALING)

!  White-sky and Black-sky albedos. Version 2.7.

      LOGICAL          :: DO_LOCAL_WSA, DO_LOCAL_BSA
      DOUBLE PRECISION :: WSA_CALC (MAX_BRDF_KERNELS), TOTAL_WSA_CALC
      DOUBLE PRECISION :: BSA_CALC (MAX_BRDF_KERNELS), TOTAL_BSA_CALC

!  Local check of Albedo, for all regular kernel options

      LOGICAL :: DO_CHECK_ALBEDO
      
!       LOGICAL :: error

!  Variables for the MODIS PCA application
!  ---------------------------------------

!  Kernels

      REAL(fpk), dimension  ( MAXSTOKES_SQ, MAXSTREAMS, MAXSTREAMS, MAXSTREAMS_BRDF )       :: RTBRDF_I_J,    LSBRDF_I_J 
      REAL(fpk), dimension  ( MAXSTOKES_SQ, MAXSTREAMS, MAXBEAMS,   MAXSTREAMS_BRDF )       :: RTBRDF_I_Mu0,  LSBRDF_I_Mu0 
      REAL(fpk), dimension  ( MAXSTOKES_SQ, MAX_USER_STREAMS, MAXSTREAMS, MAXSTREAMS_BRDF ) :: RTBRDF_Mu_J,   LSBRDF_Mu_J
      REAL(fpk), dimension  ( MAXSTOKES_SQ, MAX_USER_STREAMS, MAXBEAMS,   MAXSTREAMS_BRDF ) :: RTBRDF_Mu_Mu0, LSBRDF_Mu_Mu0
      REAL(fpk), dimension  ( MAXSTOKES_SQ, MAX_USER_STREAMS, MAX_USER_RELAZMS, MAXBEAMS )  :: RTBRDF_Exact,  LSBRDF_Exact
      
!  Azimuth factors

      REAL(fpk)  :: BRDF_AZMFAC(MAXSTREAMS_BRDF)
      REAL(fpk)  :: DM
      
!  MODIS reflectances

      REAL(fpk) :: MODIS_4R(4)

!  Proxies for data input

      LOGICAL   :: MODISPCA_choice

!  Actual wavelength to use

      REAL(fpk) :: MODISPCA_lambda


!  12 amplitude factors for 3-kernel MODIS reflectances at 4 channels

      REAL(fpk) :: MODIS_FIso(4)
      REAL(fpk) :: MODIS_FGeo(4)
      REAL(fpk) :: MODIS_FVol(4)
      
      INTEGER   :: w

!  help

      INTEGER ::          K, I, I1, J, IB, UM, IA, M
      INTEGER ::          NMOMENTS, NSTOKESSQ, N_phiquad_HALF
!       DOUBLE PRECISION :: PARS ( MAX_BRDF_PARAMETERS )
      DOUBLE PRECISION :: MUX, DELFAC, XM

      INTEGER, PARAMETER :: LUM = 1   !@@
      INTEGER, PARAMETER :: LUA = 1   !@@
      
      LOGICAL :: DO_EXACT, DO_EXACTONLY
      
!  Default, use Gaussian quadrature

      LOGICAL, PARAMETER :: DO_BRDFQUAD_GAUSSIAN = .true.
      
      
      !  Initialize Exception handling
      !  -----------------------------

      STATUS = VLIDORT_SUCCESS
      MESSAGES(1:MAX_MESSAGES) = ' '
      NMESSAGES       = 0
      MESSAGES(0)     = 'Successful Execution of VLIDORT BRDF Sup Master'
      
      ! Set error
      error = .FALSE.
    
    ! Allocate output array
!     ALLOCATE(MODIS_VBRDF_Sup_Out(nlambdas))
    
  !  Copy from input type structure
!  ------------------------------

!  Copy Control inputs

      DO_USER_STREAMS     = VBRDF_Sup_In%BS_DO_USER_STREAMS
      !DO_BRDF_SURFACE     = VBRDF_Sup_In%BS_DO_BRDF_SURFACE
      DO_SURFACE_EMISSION = VBRDF_Sup_In%BS_DO_SURFACE_EMISSION
      
      ! 
      DO_EXACT = VBRDF_Sup_In%BS_DO_DIRECTBOUNCE_ONLY
      DO_EXACTONLY = .FALSE.
      
!  Set number of stokes elements and streams

      NSTOKES  = VBRDF_Sup_In%BS_NSTOKES
      NSTREAMS = VBRDF_Sup_In%BS_NSTREAMS

!  Copy Geometry results

!  !@@ New lines

      DO_SOLAR_SOURCES = VBRDF_Sup_In%BS_DO_SOLAR_SOURCES
      DO_USER_OBSGEOMS = VBRDF_Sup_In%BS_DO_USER_OBSGEOMS
      
!   !@@ Observational Geometry + Solar sources Optionalities
!   !@@ Either set from User Observational Geometry
!          Or Copy from Usual lattice input

      IF ( DO_USER_OBSGEOMS ) THEN
        N_USER_OBSGEOMS = VBRDF_Sup_In%BS_N_USER_OBSGEOMS
        USER_OBSGEOMS   = VBRDF_Sup_In%BS_USER_OBSGEOMS
        IF ( DO_SOLAR_SOURCES ) THEN
          NBEAMS          = N_USER_OBSGEOMS
          N_USER_STREAMS  = N_USER_OBSGEOMS
          N_USER_RELAZMS  = N_USER_OBSGEOMS
          BEAM_SZAS   (1:N_USER_OBSGEOMS) = USER_OBSGEOMS(1:N_USER_OBSGEOMS,1)
          USER_ANGLES (1:N_USER_OBSGEOMS) = USER_OBSGEOMS(1:N_USER_OBSGEOMS,2)
          USER_RELAZMS(1:N_USER_OBSGEOMS) = USER_OBSGEOMS(1:N_USER_OBSGEOMS,3)
        ELSE
          NBEAMS         = 1 ; BEAM_SZAS      = ZERO
          N_USER_RELAZMS = 1 ; USER_RELAZMS   = ZERO
          N_USER_STREAMS = N_USER_OBSGEOMS
          USER_ANGLES(1:N_USER_OBSGEOMS) = USER_OBSGEOMS(1:N_USER_OBSGEOMS,2)
        ENDIF
      ELSE
        IF ( DO_SOLAR_SOURCES ) THEN
          NBEAMS            = VBRDF_Sup_In%BS_NBEAMS
          BEAM_SZAS         = VBRDF_Sup_In%BS_BEAM_SZAS
          N_USER_RELAZMS    = VBRDF_Sup_In%BS_N_USER_RELAZMS
          USER_RELAZMS      = VBRDF_Sup_In%BS_USER_RELAZMS
          N_USER_STREAMS    = VBRDF_Sup_In%BS_N_USER_STREAMS
          USER_ANGLES = VBRDF_Sup_In%BS_USER_ANGLES_INPUT
        ELSE
          NBEAMS         = 1 ; BEAM_SZAS      = ZERO
          N_USER_RELAZMS = 1 ; USER_RELAZMS   = ZERO
          N_USER_STREAMS    = VBRDF_Sup_In%BS_N_USER_STREAMS
          USER_ANGLES = VBRDF_Sup_In%BS_USER_ANGLES_INPUT
        ENDIF
      ENDIF
      
!  Copy BRDF inputs

      N_BRDF_KERNELS         = VBRDF_Sup_In%BS_N_BRDF_KERNELS
      BRDF_NAMES             = VBRDF_Sup_In%BS_BRDF_NAMES
      WHICH_BRDF             = VBRDF_Sup_In%BS_WHICH_BRDF
      N_BRDF_PARAMETERS      = VBRDF_Sup_In%BS_N_BRDF_PARAMETERS
      BRDF_PARAMETERS        = VBRDF_Sup_In%BS_BRDF_PARAMETERS
      LAMBERTIAN_KERNEL_FLAG = VBRDF_Sup_In%BS_LAMBERTIAN_KERNEL_FLAG
      BRDF_FACTORS           = VBRDF_Sup_In%BS_BRDF_FACTORS
      NSTREAMS_BRDF          = VBRDF_Sup_In%BS_NSTREAMS_BRDF
      DO_SHADOW_EFFECT       = VBRDF_Sup_In%BS_DO_SHADOW_EFFECT

!  Rob Fix 9/25/14. Replaces DO_EXACT and DO_EXACTONLY

      DO_DBONLY              = VBRDF_Sup_In%BS_DO_DIRECTBOUNCE_ONLY

!  WSA and BSA scaling options.
!   Revised, 14 April 2014, first introduced 02 April 2014, Version 2.7
!      WSA = White-sky albedo. BSA = Black-sky albedo.
!  Rob Fix 9/27/14. Output variable added

      DO_WSABSA_OUTPUT    = VBRDF_Sup_In%BS_DO_WSABSA_OUTPUT
      DO_WSA_SCALING      = VBRDF_Sup_In%BS_DO_WSA_SCALING
      DO_BSA_SCALING      = VBRDF_Sup_In%BS_DO_BSA_SCALING
      WSA_VALUE           = VBRDF_Sup_In%BS_WSA_VALUE
      BSA_VALUE           = VBRDF_Sup_In%BS_BSA_VALUE

!  NewCM options

      DO_NewCMGLINT = VBRDF_Sup_In%BS_DO_NewCMGLINT
      SALINITY     = VBRDF_Sup_In%BS_SALINITY
      WAVELENGTH   = VBRDF_Sup_In%BS_WAVELENGTH

      WINDSPEED = VBRDF_Sup_In%BS_WINDSPEED
      WINDDIR   = VBRDF_Sup_In%BS_WINDDIR

      DO_GlintShadow   = VBRDF_Sup_In%BS_DO_GlintShadow
      DO_FoamOption    = VBRDF_Sup_In%BS_DO_FoamOption
      DO_FacetIsotropy = VBRDF_Sup_In%BS_DO_FacetIsotropy 
      
!  New MODISPCA choice. Version 3.7A, May 23rd, 2014

      MODISPCA_choice = VBRDF_Sup_In%BS_MODISPCA_choice
      MODISPCA_lambda = VBRDF_Sup_In%BS_MODISPCA_lambda
!       MODIS_channels  = VBRDF_Sup_In%BS_MODIS_channels
      MODIS_FIso      = VBRDF_Sup_In%BS_MODIS_FIso
      MODIS_FGeo      = VBRDF_Sup_In%BS_MODIS_FGeo
      MODIS_FVol      = VBRDF_Sup_In%BS_MODIS_FVol
!       MODISPCA_ndata          = VBRDF_Sup_In%BS_MODISPCA_ndata
!       MODISPCA_datalambdas    = VBRDF_Sup_In%BS_MODISPCA_datalambdas
  
!  Local check of albedo

      DO_CHECK_ALBEDO = .not. DO_NewCMGLINT

!  Local flags
!  Rob Fix 9/27/14. Output variable included

      DO_LOCAL_WSA = ( DO_WSA_SCALING .or. DO_WSABSA_OUTPUT ) .or.  DO_CHECK_ALBEDO
      DO_LOCAL_BSA = ( DO_BSA_SCALING .or. DO_WSABSA_OUTPUT ) .and. DO_SOLAR_SOURCES

!  Copy MSR inputs

      DO_MSRCORR             = VBRDF_Sup_In%BS_DO_GLITTER_MSRCORR
      DO_MSRCORR_DBONLY      = VBRDF_Sup_In%BS_DO_GLITTER_MSRCORR_DBONLY 
      MSRCORR_ORDER          = VBRDF_Sup_In%BS_GLITTER_MSRCORR_ORDER
      N_MUQUAD               = VBRDF_Sup_In%BS_GLITTER_MSRCORR_NMUQUAD
      N_PHIQUAD              = VBRDF_Sup_In%BS_GLITTER_MSRCORR_NPHIQUAD
      
      
!   Defaults for MODIS options
!   --------------------------

     IF ( MODISPCA_choice ) then
        N_BRDF_KERNELS         = 3
        WHICH_BRDF(1) = LAMBERTIAN_IDX ; N_BRDF_PARAMETERS(1) = 0
        WHICH_BRDF(2) = ROSSTHICK_IDX  ; N_BRDF_PARAMETERS(2) = 0
        WHICH_BRDF(3) = LISPARSE_IDX   ; N_BRDF_PARAMETERS(3) = 2
        BRDF_PARAMETERS(1:2,:) = zero
        BRDF_PARAMETERS(3,1) = 2.0     ! default Li-sparse value
        BRDF_PARAMETERS(3,2) = 1.0     ! default Li-sparse value
     ENDIF

!  turn off some flags. Could add to this list for safety.

     IF ( MODISPCA_choice ) then
        DO_LOCAL_WSA  = .false.  ; DO_WSA_SCALING = .false.
        DO_LOCAL_BSA  = .false.  ; DO_BSA_SCALING = .false.
        DO_NewCMGLINT = .false.  ; DO_MSRCORR     = .false.
     ENDIF



!  Main code
!  ---------

!  Set up Quadrature streams for output
!    QUAD_STRMWTS dropped for Version 2.7 (now redefined for local WSA/BSA scaling)

      CALL BRDF_GAULEG ( 0.0d0, 1.0d0, QUAD_STREAMS, QUAD_WEIGHTS, NSTREAMS )
      DO I = 1, NSTREAMS
        QUAD_SINES(I) = SQRT(1.0d0-QUAD_STREAMS(I)*QUAD_STREAMS(I))
      enddo

!  Set up Quadrature streams for WSA/BSA Scaling. New code, Version 2.7

      IF ( DO_LOCAL_WSA .or. DO_LOCAL_BSA ) THEN
         SCALING_NSTREAMS = MAXSTREAMS_SCALING
         CALL BRDF_GAULEG ( 0.0d0, 1.0d0, SCALING_QUAD_STREAMS, SCALING_QUAD_WEIGHTS, SCALING_NSTREAMS )
         DO I = 1, SCALING_NSTREAMS
            SCALING_QUAD_SINES(I)   = SQRT(1.0d0-SCALING_QUAD_STREAMS(I)*SCALING_QUAD_STREAMS(I))
            SCALING_QUAD_STRMWTS(I) = SCALING_QUAD_STREAMS(I) * SCALING_QUAD_WEIGHTS(I)
         enddo
      ENDIF

!  Number of Stokes components squared
!    ** Bookkeeping for surface kernel Cox-Munk types
!    ** Only the Giss CoxMunk kernel is vectorized (as of 19 January 2009)

!  Rob Extension 12/2/14. BPDF Kernels (replace BPDF2009)
!  Rob Fix, 14 March 2014. NSTOKESSQ > 1 for BPDF. Now, the BPDF2009 kernel is vectorized
!  Rob Extension 12/2/14. BPDF Kernels (replace BPDF2009)
!    ** Now, the BPDFVEGN, BPDFSOIL, BPDFNDVI kernels are vectorized

!   Additional code for complex RI Giss Cox-Munk, 15 march 2010.
!     3 parameters are PARS(1) = sigma_sq
!                      PARS(2) = Real (RI)
!                      PARS(3) = Imag (RI)

      NSTOKESSQ  = 1
      DO K = 1, N_BRDF_KERNELS
         IF ( BRDF_NAMES(K) .EQ. 'Cox-Munk  ' .OR. &
              BRDF_NAMES(K) .EQ. 'GissCoxMnk' ) THEN
            N_BRDF_PARAMETERS(K) = 3
            IF ( DO_SHADOW_EFFECT ) THEN
              BRDF_PARAMETERS(K,3) = ONE
            ELSE
              BRDF_PARAMETERS(K,3) = ZERO
            ENDIF
         ELSE IF ( BRDF_NAMES(K) .EQ. 'GCMcomplex' ) THEN
            N_BRDF_PARAMETERS(K) = 3
         ENDIF
         IF ( BRDF_NAMES(K) .EQ. 'GissCoxMnk'  .OR. &
              BRDF_NAMES(K) .EQ. 'GCMcomplex'  .OR. &
              BRDF_NAMES(K) .EQ. 'BPDF-Vegn '  .OR. &
              BRDF_NAMES(K) .EQ. 'BPDF-Soil '  .OR. &
              BRDF_NAMES(K) .EQ. 'BPDF-NDVI ' ) THEN
            NSTOKESSQ = NSTOKES * NSTOKES
         ENDIF
      ENDDO

!  Number of Fourier components to calculate

      IF ( DO_DEBUG_RESTORATION ) THEN
        NMOMENTS = NMOMENTS_INPUT
      ELSE
        NMOMENTS = 2 * NSTREAMS - 1
      ENDIF

!  Half number of moments

      NBRDF_HALF = NSTREAMS_BRDF / 2

!  Usable solar beams. !@@ Optionality, added 12/31/12
!    Warning, this should be the BOA angle. OK for the non-refractive case.

      IF ( DO_SOLAR_SOURCES ) THEN
        DO IB = 1, NBEAMS
          MUX =  COS(BEAM_SZAS(IB)*DEG_TO_RAD)
          SZASURCOS(IB) = MUX
          SZASURSIN(IB) = SQRT(1.0D0-MUX*MUX)
        ENDDO
      ELSE
        SZASURCOS = 0.0D0 ; SZASURSIN = 0.0D0
      ENDIF

!  Viewing angles

      DO UM = 1, N_USER_STREAMS
        USER_STREAMS(UM) = COS(USER_ANGLES(UM)*DEG_TO_RAD)
        USER_SINES(UM)   = SQRT(ONE-USER_STREAMS(UM)*USER_STREAMS(UM))
      ENDDO

! Optionality, added 12/31/12
!   Rob Fix 9/25/14. Removed DO_EXACT; Contribution always required now....

      IF ( DO_SOLAR_SOURCES ) THEN
        DO IA = 1, N_USER_RELAZMS
          PHIANG(IA) = USER_RELAZMS(IA)*DEG_TO_RAD
          COSPHI(IA) = COS(PHIANG(IA))
          SINPHI(IA) = SIN(PHIANG(IA))
        ENDDO
      ENDIF

!  BRDF quadrature
!  ---------------

!  Save these quantities for efficient coding

      IF ( DO_BRDFQUAD_GAUSSIAN ) then
        CALL BRDF_QUADRATURE_Gaussian &
           ( DO_SURFACE_EMISSION, NSTREAMS_BRDF, NBRDF_HALF, &
             X_BRDF, CX_BRDF, SX_BRDF, A_BRDF, &
             BAX_BRDF, CXE_BRDF, SXE_BRDF )
      ELSE
        CALL BRDF_QUADRATURE_Trapezoid &
           ( DO_SURFACE_EMISSION, NSTREAMS_BRDF, NBRDF_HALF, &
             X_BRDF, CX_BRDF, SX_BRDF, A_BRDF, &
             BAX_BRDF, CXE_BRDF, SXE_BRDF )
      ENDIF

!  Set up the MSR points
!  ---------------------

!  Air to water, Polar quadrature

      if ( DO_MSRCORR  ) THEN
         CALL brdf_gauleg ( ZERO, ONE, X_muquad, W_muquad, n_muquad )
         DO I = 1, N_MUQUAD
            XM = X_MUQUAD(I)
            SX_MUQUAD(I) = SQRT(ONE-XM*XM)
            WXX_MUQUAD(I) = XM * XM * W_MUQUAD(I)
         ENDDO
      endif

!  Azimuth quadrature

      if ( DO_MSRCORR  ) THEN
         N_phiquad_HALF = N_PHIQUAD / 2
         CALL brdf_gauleg ( ZERO, ONE, X_PHIQUAD, W_PHIQUAD, N_PHIQUAD_HALF )
         DO I = 1, N_PHIQUAD_HALF
           I1 = I + N_PHIQUAD_HALF
           X_PHIQUAD(I1) = - X_PHIQUAD(I)
           W_PHIQUAD(I1) =   W_PHIQUAD(I)
         ENDDO
         DO I = 1, N_PHIQUAD
            X_PHIQUAD(I)  = PIE * X_PHIQUAD(I)
         ENDDO
      ENDIF


!  Initialise ALL outputs
!  ----------------------
    
    
    ! Echo some information
    IF( do_debug_geocape_tool ) THEN
      print*,''
      print*,'DO_USER_STREAMS :',DO_USER_STREAMS
      print*,'DO_USER_OBSGEOMS:',DO_USER_OBSGEOMS
      print*,'DO_EXACT        :',DO_EXACT
      print*,'DO_EXACTONLY    :',DO_EXACTONLY
      print*,'NSTREAMS_BRDF   :',NSTREAMS_BRDF
      print*,'NSTREAMS        :',NSTREAMS
      print*,'NBEAMS          :',NBEAMS
      print*,'N_USER_STREAMS  :',N_USER_STREAMS
      print*,'N_USER_RELAZMS  :',N_USER_RELAZMS
    ENDIF
    
    DO w=1, nlambdas
      
      
! !  Zero Direct-Bounce BRDF   (Commented names are from the brdf test
! 
!       MODIS_VBRDF_Sup_Out(w)%BS_DBOUNCE_BRDFUNC = ZERO ! EXACTDB_BRDFUNC
! 
! !  zero the BRDF Fourier components
! 
!       MODIS_VBRDF_Sup_Out(w)%BS_BRDF_F_0 = ZERO         ! BRDF
!       MODIS_VBRDF_Sup_Out(w)%BS_BRDF_F   = ZERO         ! BRDF_F
!       MODIS_VBRDF_Sup_Out(w)%BS_USER_BRDF_F_0 = ZERO    ! USER_BRDF_F_0
!       MODIS_VBRDF_Sup_Out(w)%BS_USER_BRDF_F   = ZERO    ! USER_BRDF_F

!  Initialize surface emissivity
!    Set to zero if you are using Albedo Scaling

      
!       if ( do_wsa_scaling .or. do_bsa_scaling ) then
!          MODIS_VBRDF_Sup_Out(w)%BS_EMISSIVITY      = ZERO ! EMISSIVITY
!          MODIS_VBRDF_Sup_Out(w)%BS_USER_EMISSIVITY = ZERO ! USER_EMISSIVITY
!       else
!          MODIS_VBRDF_Sup_Out(w)%BS_EMISSIVITY      = ONE 
!          MODIS_VBRDF_Sup_Out(w)%BS_USER_EMISSIVITY = ONE
!       endif
      
      if ( do_wsa_scaling .or. do_bsa_scaling ) then
         BS_EMISSIVITY      = ZERO ! EMISSIVITY
         BS_USER_EMISSIVITY = ZERO ! USER_EMISSIVITY
      else
         BS_EMISSIVITY      = ONE 
         BS_USER_EMISSIVITY = ONE
      endif
      
   ENDDO
!  Initialize WSA/BSA albedos

      WSA_CALC = zero ; TOTAL_WSA_CALC = zero
      BSA_CALC = zero ; TOTAL_BSA_CALC = zero
  
!   IF  ( MODISPCA_choice ) THEN
  
!  Get the kernels if you have not done so

         IF ( BRDF_Sup_ModOut%BS_DO_MODISPCA_KERNELS ) then

!  Get the Ross-Thick kernels; copy to output
          
          CALL VBRDF_REDUCED_MAKER ( &
                  ROSSTHICK_VFUNCTION, 1, &
                  DO_USER_STREAMS, DO_USER_OBSGEOMS, DO_EXACT, DO_EXACTONLY,                 & ! Inputs
                  N_BRDF_PARAMETERS(2), BRDF_PARAMETERS(2,:),                                & ! Inputs
                  NSTREAMS_BRDF, NSTREAMS, NBEAMS, N_USER_STREAMS, N_USER_RELAZMS,           & ! Inputs
                  QUAD_STREAMS, QUAD_SINES, USER_STREAMS, USER_SINES,  SZASURCOS, SZASURSIN, & ! Inputs
                  PHIANG, COSPHI, SINPHI, X_BRDF, CX_BRDF, SX_BRDF,                          & ! Inputs
                  RTBRDF_Exact, RTBRDF_I_J, RTBRDF_Mu_J, RTBRDF_I_Mu0, RTBRDF_Mu_Mu0  )        ! output
                  
            BRDF_Sup_ModOut%BS_RTBRDF_Exact  = RTBRDF_Exact
            BRDF_Sup_ModOut%BS_RTBRDF_I_J    = RTBRDF_I_J
            BRDF_Sup_ModOut%BS_RTBRDF_I_Mu0  = RTBRDF_I_Mu0
            BRDF_Sup_ModOut%BS_RTBRDF_Mu_J   = RTBRDF_Mu_J
            BRDF_Sup_ModOut%BS_RTBRDF_Mu_Mu0 = RTBRDF_Mu_Mu0
            
!             print*,DO_USER_STREAMS, DO_USER_OBSGEOMS, DO_EXACT, DO_EXACTONLY
!             print*,'N_USER_STREAMS:',N_USER_STREAMS
!             print*,'NBEAMS:',NBEAMS
!             print*,'NSTREAMS:',NSTREAMS
!             print*,'NSTREAMS_BRDF:',NSTREAMS_BRDF
!             print*,'QUAD_SINES:',QUAD_SINES(1:NSTREAMS)
!             print*,'USER_SINES:',USER_SINES(1:N_USER_STREAMS)
!             print*,'SZASURCOS:',SZASURCOS(1:NBEAMS)
!             print*,'SZASURSIN:',SZASURSIN(1:NBEAMS)
!             print*,'PHIANG:',PHIANG(1:N_USER_STREAMS)
!             
!             print*,'RTBRDF_Exact',RTBRDF_Exact(1,1,1,1)
!             print*,'RTBRDF_Mu_Mu0:',RTBRDF_Mu_Mu0(1,1:N_USER_STREAMS, 1:NBEAMS,1:NSTREAMS_BRDF)
 
            
            CALL VBRDF_REDUCED_MAKER ( &
                  LISPARSE_VFUNCTION, 1, &
                  DO_USER_STREAMS, DO_USER_OBSGEOMS, DO_EXACT, DO_EXACTONLY,                 & ! Inputs
                  N_BRDF_PARAMETERS(3), BRDF_PARAMETERS(3,:),                                & ! Inputs
                  NSTREAMS_BRDF, NSTREAMS, NBEAMS, N_USER_STREAMS, N_USER_RELAZMS,           & ! Inputs
                  QUAD_STREAMS, QUAD_SINES, USER_STREAMS, USER_SINES,  SZASURCOS, SZASURSIN, & ! Inputs
                  PHIANG, COSPHI, SINPHI, X_BRDF, CX_BRDF, SX_BRDF,                          & ! Inputs
                  LSBRDF_Exact, LSBRDF_I_J, LSBRDF_Mu_J, LSBRDF_I_Mu0, LSBRDF_Mu_Mu0  )        ! output
            
!             CALL BRDF_REDUCED_MAKER &
!            ( DO_USER_STREAMS, DO_USER_OBSGEOMS, DO_EXACT, DO_EXACTONLY,                   & ! Inputs !@@
!              LISPARSE_IDX, N_BRDF_PARAMETERS(3), BRDF_PARAMETERS(3,:),                    & ! Inputs
!              NSTREAMS_BRDF, NSTREAMS, NBEAMS, N_USER_STREAMS, N_USER_RELAZMS,             & ! Inputs
!              QUAD_STREAMS, QUAD_SINES, USER_STREAMS, USER_SINES,  SZASURCOS, SZASURSIN,   & ! Inputs
!              PHIANG, COSPHI, SINPHI, X_BRDF, CX_BRDF, SX_BRDF,                            & ! Inputs
!              LSBRDF_Exact, LSBRDF_I_J, LSBRDF_Mu_J, LSBRDF_I_Mu0, LSBRDF_Mu_Mu0  )          ! output

            BRDF_Sup_ModOut%BS_LSBRDF_Exact  = LSBRDF_Exact
            BRDF_Sup_ModOut%BS_LSBRDF_I_J    = LSBRDF_I_J
            BRDF_Sup_ModOut%BS_LSBRDF_I_Mu0  = LSBRDF_I_Mu0
            BRDF_Sup_ModOut%BS_LSBRDF_Mu_J   = LSBRDF_Mu_J
            BRDF_Sup_ModOut%BS_LSBRDF_Mu_Mu0 = LSBRDF_Mu_Mu0
            
!             print*,'LSBRDF_Exact',LSBRDF_Exact(1,1,1,1)
!             print*,'LSBRDF_Mu_Mu0:',LSBRDF_Mu_Mu0(1,1:N_USER_STREAMS, 1:NBEAMS,1:NSTREAMS_BRDF)
!             STOP
            
!  Turn off flag

            BRDF_Sup_ModOut%BS_DO_MODISPCA_KERNELS = .false.

!  Otherwise copy already-calculated RT and LS kernels to local arrays
         ELSE

            RTBRDF_Exact  = BRDF_Sup_ModOut%BS_RTBRDF_Exact
            if ( .not. do_exactonly ) then
               RTBRDF_I_J    = BRDF_Sup_ModOut%BS_RTBRDF_I_J
               RTBRDF_I_Mu0  = BRDF_Sup_ModOut%BS_RTBRDF_I_Mu0
               RTBRDF_Mu_J   = BRDF_Sup_ModOut%BS_RTBRDF_Mu_J
               RTBRDF_Mu_Mu0 = BRDF_Sup_ModOut%BS_RTBRDF_Mu_Mu0
            endif

            LSBRDF_Exact  = BRDF_Sup_ModOut%BS_LSBRDF_Exact
            if ( .not. do_exactonly ) then
               LSBRDF_I_J    = BRDF_Sup_ModOut%BS_LSBRDF_I_J
               LSBRDF_I_Mu0  = BRDF_Sup_ModOut%BS_LSBRDF_I_Mu0
               LSBRDF_Mu_J   = BRDF_Sup_ModOut%BS_LSBRDF_Mu_J
               LSBRDF_Mu_Mu0 = BRDF_Sup_ModOut%BS_LSBRDF_Mu_Mu0
            endif

         ENDIF
         
!  Calculation
!  ===========

!  For each geometrical configuration....
!  1. Develop the Four-channel geometry-adjusted MODIS reflectances (MODIS_4R)
!  2. Call to the "APPLY_MODISPCA" routine to get final result
    
    ! Preallocate smooth spline coeff arrays to reduce computation time
    ! Allocate BRDF arrays with fourier expansions and direct bounce
    ! ------------------------------------------------------------------
    
    ! Exact BRDFs
    IF( DO_USER_OBSGEOMS ) THEN
      ALLOCATE( BS_DBOUNCE_BRDFUNC(LUM,LUA,NBEAMS,nlambdas) )
    ELSE
      ALLOCATE( BS_DBOUNCE_BRDFUNC(N_USER_STREAMS,N_USER_RELAZMS,NBEAMS,nlambdas) )
    ENDIF
    
    ! Incident and reflected discrete-ordinate streams
    ALLOCATE( BS_BRDF_F(0:NMOMENTS,NSTREAMS,NSTREAMS,nlambdas)   )
    
    !  Incident solar and reflected discrete-ordinate streams
    ALLOCATE( BS_BRDF_F_0(0:NMOMENTS,NSTREAMS,NBEAMS,nlambdas)    )
    
    !  User-geometry outgoin with discrete-ordinate inputs
    IF( DO_USER_STREAMS ) THEN
      ALLOCATE(BS_USER_BRDF_F(0:NMOMENTS,N_USER_STREAMS,NSTREAMS,nlambdas)  )
    ENDIF
    
    IF ( DO_USER_STREAMS .and. DO_SOLAR_SOURCES ) THEN
      
      IF( DO_USER_OBSGEOMS ) THEN
        ALLOCATE( BS_USER_BRDF_F_0(0:NMOMENTS,LUM,NBEAMS,nlambdas) )
      ELSE
        ALLOCATE( BS_USER_BRDF_F_0(0:NMOMENTS,N_USER_STREAMS,NBEAMS,nlambdas) )
      ENDIF
      
    ENDIF
    
    
    DO w=1,nlambdas
        
        
!  Exact BRDFs
        IF ( DO_USER_OBSGEOMS ) THEN   
          DO IB = 1, NBEAMS
            MODIS_4R(1:4) = MODIS_FIso(1:4) &  
                          + MODIS_FVol(1:4) * RTBRDF_Exact(1,LUM,LUA,IB) &
                          + MODIS_FGeo(1:4) * LSBRDF_Exact(1,LUM,LUA,IB)

            CALL compute_SciaUSGS_Coeff_widx( MODIS_4R, BS_DBOUNCE_BRDFUNC(LUM,LUA,IB,w), w )
            
          ENDDO
        ELSE
          DO IA = 1, N_USER_RELAZMS
          DO IB = 1, NBEAMS
          DO UM = 1, N_USER_STREAMS

            MODIS_4R(1:4) = MODIS_FIso(1:4) &
                          + MODIS_FVol(1:4) * RTBRDF_Exact(1,UM,IA,IB) &
                          + MODIS_FGeo(1:4) * LSBRDF_Exact(1,UM,IA,IB)
            
            CALL compute_SciaUSGS_Coeff_widx( MODIS_4R, BS_DBOUNCE_BRDFUNC(UM,IA,IB,w), w )

          ENDDO
          ENDDO
          ENDDO
          
          
        ENDIF
        
!  Incident and reflected discrete-ordinate streams
        
        BRDFUNC = zero
        DO I=1,NSTREAMS
        DO J=1,NSTREAMS
        DO K=1,NSTREAMS_BRDF
          
          ! Compute BRDF at given geometry
          MODIS_4R(1:4) = MODIS_FIso(1:4) &
                        + MODIS_FVol(1:4) * RTBRDF_I_J(1,I,J,K) &
                        + MODIS_FGeo(1:4) * LSBRDF_I_J(1,I,J,K)
          
          CALL compute_SciaUSGS_Coeff_widx( MODIS_4R, BRDFUNC(1,I,J,K), w )
          
        ENDDO
        ENDDO
        ENDDO
        
        
        
!  Incident solar and reflected discrete-ordinate streams
        BRDFUNC_0 = zero
        DO IB=1, NBEAMS
        DO  J=1, NSTREAMS
        DO  K=1, NSTREAMS_BRDF
          
          MODIS_4R(1:4) = MODIS_FIso(1:4) &
                        + MODIS_FVol(1:4) * RTBRDF_I_Mu0(1,J,IB,K) &
                        + MODIS_FGeo(1:4) * LSBRDF_I_Mu0(1,J,IB,K)
          
          CALL compute_SciaUSGS_Coeff_widx( MODIS_4R, BRDFUNC_0(1,J,IB,K), w )
          
        ENDDO
        ENDDO
        ENDDO
        
!  User-geometry outgoing with discrete-ordinate inputs
        
        IF( DO_USER_STREAMS ) THEN
          
          USER_BRDFUNC = zero
          DO UM = 1, N_USER_STREAMS
          DO J = 1, NSTREAMS
          DO K = 1, NSTREAMS_BRDF
            MODIS_4R(1:4) = MODIS_FIso(1:4) &
                          + MODIS_FVol(1:4) * RTBRDF_Mu_J(1,UM,J,K) &
                          + MODIS_FGeo(1:4) * LSBRDF_Mu_J(1,UM,J,K)
            CALL compute_SciaUSGS_Coeff_widx( MODIS_4R, USER_BRDFUNC(1,UM,J,K), w )
          ENDDO
          ENDDO
          ENDDO
        ENDIF
          
!  Kernel combinations (for Solar-to-Userstream reflectance)
!   !@@ Generally only required for a MS + SS Truncated calculation
!   !@@ Observational Goemetry and Solar sources, Optionalities 12/31/12
        
        IF ( DO_USER_STREAMS.and.DO_SOLAR_SOURCES ) THEN
        
          USER_BRDFUNC_0 = zero
          IF ( DO_USER_OBSGEOMS ) THEN
            DO IB = 1, NBEAMS
            DO K = 1, NSTREAMS_BRDF
            
              MODIS_4R(1:4) = MODIS_FIso(1:4) &
                            + MODIS_FVol(1:4) * RTBRDF_Mu_Mu0(1,LUM,IB,K) &
                            + MODIS_FGeo(1:4) * LSBRDF_Mu_Mu0(1,LUM,IB,K)
              CALL compute_SciaUSGS_Coeff_widx( MODIS_4R, USER_BRDFUNC_0(1,LUM,IB,K), w )
            ENDDO
            ENDDO
            
          ELSE
          
            DO UM = 1, N_USER_STREAMS
            DO IB = 1, NBEAMS
            DO K = 1, NSTREAMS_BRDF
              MODIS_4R(1:4) = MODIS_FIso(1:4) &
                            + MODIS_FVol(1:4) * RTBRDF_Mu_Mu0(1,UM,IB,K) &
                            + MODIS_FGeo(1:4) * LSBRDF_Mu_Mu0(1,UM,IB,K)
              CALL compute_SciaUSGS_Coeff_widx( MODIS_4R, USER_BRDFUNC_0(1,UM,IB,K), w )
            ENDDO
            ENDDO
            ENDDO
          ENDIF
          
        ENDIF
        
        ! Do Fourier as normal 
        ! --------------------
        
        DO M = 0, NMOMENTS
          
          !  surface reflectance factors, Weighted Azimuth factors
          IF ( M .EQ. 0 ) THEN
            DELFAC   = ONE
            DO I = 1, NSTREAMS_BRDF
              BRDF_AZMFAC(I) = A_BRDF(I)
            ENDDO
          ELSE
            DELFAC   = TWO ; DM = DBLE(M)
            DO I = 1, NSTREAMS_BRDF
              BRDF_AZMFAC(I) = A_BRDF(I) * COS (DM * X_BRDF(I) )
            ENDDO
          ENDIF
          
          ! Call reduced fourier routine
          CALL VBRDF_REDUCED_FOURIER  &
              ( DO_USER_STREAMS, DO_USER_OBSGEOMS, DELFAC,                     & ! Inputs
                NBEAMS, NSTREAMS, N_USER_STREAMS, NSTREAMS_BRDF,               & ! Inputs
                BRDFUNC, USER_BRDFUNC, BRDFUNC_0, USER_BRDFUNC_0, BRDF_AZMFAC, & ! Inputs
                LOCAL_BRDF_F, LOCAL_BRDF_F_0, LOCAL_USER_BRDF_F, LOCAL_USER_BRDF_F_0 )   ! Outputs
          
          
          ! Copy arrays
          DO I = 1, NSTREAMS 
!             MODIS_VBRDF_Sup_Out(w)%BS_BRDF_F(M,1,I,1:NSTREAMS) = LOCAL_BRDF_F(1,I,1:NSTREAMS)
            BS_BRDF_F(M,I,1:NSTREAMS,w) = LOCAL_BRDF_F(1,I,1:NSTREAMS)
            IF( DO_SOLAR_SOURCES ) THEN
!               MODIS_VBRDF_Sup_Out(w)%BS_BRDF_F_0(M,1,I,1:NBEAMS) = LOCAL_BRDF_F_0(1,I,1:NBEAMS)
              BS_BRDF_F_0(M,I,1:NBEAMS,w) = LOCAL_BRDF_F_0(1,I,1:NBEAMS)
            ENDIF
  !                BRDF_F(M,I,1:NSTREAMS) = LOCAL_BRDF_F(I,1:NSTREAMS)
  !                IF ( DO_SOLAR_SOURCES ) BRDF_F_0(M,I,1:NBEAMS) = LOCAL_BRDF_F_0(I,1:NBEAMS)
          ENDDO
          
          IF ( DO_USER_STREAMS ) THEN
            DO UM = 1, N_USER_STREAMS
!               MODIS_VBRDF_Sup_Out(w)%BS_USER_BRDF_F(M,1,UM,1:NSTREAMS) = &
!                 LOCAL_USER_BRDF_F(1,UM,1:NSTREAMS)
              BS_USER_BRDF_F(M,UM,1:NSTREAMS,w)= LOCAL_USER_BRDF_F(1,UM,1:NSTREAMS)
              
              IF (DO_SOLAR_SOURCES ) THEN
                IF (DO_USER_OBSGEOMS) THEN 
!                   MODIS_VBRDF_Sup_Out(w)%BS_USER_BRDF_F_0(M,1,LUM,1:NBEAMS) = &
!                     LOCAL_USER_BRDF_F_0(1,LUM,1:NBEAMS)
                  BS_USER_BRDF_F_0(M,LUM,1:NBEAMS,w) = LOCAL_USER_BRDF_F_0(1,LUM,1:NBEAMS)
                ENDIF
                IF (.not.DO_USER_OBSGEOMS) THEN
!                   MODIS_VBRDF_Sup_Out(w)%BS_USER_BRDF_F_0(M,1,UM,1:NBEAMS) = &
!                     LOCAL_USER_BRDF_F_0(1,UM,1:NBEAMS)
                  BS_USER_BRDF_F_0(M,UM,1:NBEAMS,w) = LOCAL_USER_BRDF_F_0(1,UM,1:NBEAMS)
                ENDIF
              ENDIF
              
            ENDDO
          ENDIF
          
        ENDDO ! M
        
     ENDDO ! w
     
     ! Save the dimensions 
     BS_NBEAMS = NBEAMS
     BS_N_USER_RELAZMS = N_USER_RELAZMS
     BS_N_USER_STREAMS = N_USER_STREAMS
     BS_NMOMENTS = NMOMENTS
     
  END SUBROUTINE precompute_VLIDORT_BRDF_arrays
  
  SUBROUTINE cleanup_SciaUSGS_FA
    
    INCLUDE 'netcdf.inc'
    
    INTEGER :: rcode
    character(len=19), parameter :: location='cleanup_SciaUSGS_FA'
    
    ! ==============================================================
    ! cleanup_SciaUSGS_FA Starts here
    ! ==============================================================
    
    ! Deallocate arrays
    IF(ALLOCATED(fa_mu)     ) DEALLOCATE( fa_mu      )
    IF(ALLOCATED(fa_mu_m)   ) DEALLOCATE( fa_mu_m    )
    IF(ALLOCATED(fa_wvl)    ) DEALLOCATE( fa_wvl     )
    IF(ALLOCATED(fa_wvl_r8) ) DEALLOCATE( fa_wvl_r8  )
    IF(ALLOCATED(fa_W)      ) DEALLOCATE( fa_W       )
    IF(ALLOCATED(fa_G)      ) DEALLOCATE( fa_G       )
    IF(ALLOCATED(fa_lon)    ) DEALLOCATE( fa_lon     )
    IF(ALLOCATED(fa_lat)    ) DEALLOCATE( fa_lat     )
    IF(ALLOCATED(fa_exist)  ) DEALLOCATE( fa_exist   )
    IF(ALLOCATED(fa_mu_int) ) DEALLOCATE( fa_mu_int  )
    IF(ALLOCATED(fa_W_int)  ) DEALLOCATE( fa_W_int   )
    IF(ALLOCATED(fa_tmp)    ) DEALLOCATE( fa_tmp     )
    IF(ALLOCATED(fa_tmp_int)) DEALLOCATE( fa_tmp_int )
    IF( ALLOCATED(usgs_water_int) ) DEALLOCATE(usgs_water_int)
    IF( ALLOCATED(usgs_snowf_int) ) DEALLOCATE(usgs_snowf_int)
    IF( ALLOCATED(usgs_snowc_int) ) DEALLOCATE(usgs_snowc_int)
    
    ! Close file
    rcode = nf_close( fa_ncid )
    
    ! Check for error
    CALL fa_nc_handle_error(location,rcode)
    
    
  END SUBROUTINE cleanup_SciaUSGS_FA
  
  SUBROUTINE fa_nc_handle_error(location,status)
    
    IMPLICIT NONE
    INCLUDE 'netcdf.inc'

    ! ---------------
    ! Input variables
    ! ---------------
    INTEGER, INTENT(IN) :: status
    CHARACTER(*)        :: location

    ! ---------------
    ! Local variables
    !----------------
    CHARACTER(NF_MAX_NAME) :: message

    ! Code starts here
    IF (status .NE. NF_NOERR) THEN
       message = 'Error in '//TRIM(location)//': '//NF_STRERROR(status)
       print*, message
       STOP
!        CALL write_err_message(.TRUE., message)
!        CALL error_exit(.TRUE.)
    ENDIF

  END SUBROUTINE fa_nc_handle_error
  
  ! SUBROUTINE load_MODIS
  
  ! SUBROUTINE update_VLIDORT_arrays
  
  ! 
  
END 
