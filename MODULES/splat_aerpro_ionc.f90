MODULE splat_aerpro_ionc

  IMPLICIT NONE
  PRIVATE
  PUBLIC :: create_aerpro_nc

CONTAINS

  SUBROUTINE create_aerpro_nc(filename, nwls, ngk, nmom, nrh, &
       phfcn0, qext0, reff, rh, ssa0, wls0)

    IMPLICIT NONE
    INCLUDE 'netcdf.inc'

    ! ---------------
    ! Input variables
    ! ---------------
    CHARACTER(LEN=*), INTENT(IN):: filename
    INTEGER, INTENT(IN):: nwls,ngk,nmom,nrh
    REAL(KIND=8), INTENT(IN), DIMENSION(nwls) :: wls0
    REAL(KIND=8), INTENT(IN), DIMENSION(nrh) :: reff, rh
    REAL(KIND=8), INTENT(IN), DIMENSION(nwls,nrh) :: qext0, ssa0
    REAL(KIND=8), INTENT(IN), DIMENSION(nwls,nmom,ngk,nrh) :: phfcn0
    
    ! ---------------
    ! Local variables
    ! ---------------
    CHARACTER(LEN=256) :: message
    INTEGER :: rcode, id, onedid, wdid, mdid, gdid, rdid, vid
    INTEGER, PARAMETER :: att_len = 50
    INTEGER, DIMENSION(4) :: pdim
    INTEGER, DIMENSION(2) :: qdim

    ! ----------------
    ! Code starts here
    ! ----------------
    ! Create file
    message = '...creating '//filename
    rcode = NF_CREATE(trim(adjustl(filename)), NF_NETCDF4, id)
    CALL netcdf_handle_error(message,.true.,rcode)
    
    ! Define dimensions
    message = 'defining dimension one'
    rcode = NF_DEF_DIM(id,'one',1,onedid)
    CALL netcdf_handle_error(message,.false.,rcode)
    message = 'defining dimension w in '//filename
    rcode = NF_DEF_DIM(id,'w',nwls,wdid)
    CALL netcdf_handle_error(message,.false.,rcode)
    message = 'defining dimension m in '//filename
    rcode = NF_DEF_DIM(id,'m',nmom,mdid)
    CALL netcdf_handle_error(message,.false.,rcode)
    message = 'defining dimension g in '//filename
    rcode = NF_DEF_DIM(id,'g',ngk,gdid)
    CALL netcdf_handle_error(message,.false.,rcode)
    message = 'defining dimension r in '//filename
    rcode = NF_DEF_DIM(id,'r',nrh,rdid)
    CALL netcdf_handle_error(message,.false.,rcode)

    ! Define  variables
    rcode = NF_DEF_VAR(id,'nmom', NF_INT, 1, onedid, vid)
    message = 'creating nmom variable in '//filename
    CALL netcdf_handle_error(message,.false.,rcode)
    message = 'Number of phase moments'
    rcode = NF_PUT_ATT(id, vid, 'description', NF_CHAR, LEN(TRIM(message)), TRIM(message))

    rcode = NF_DEF_VAR(id,'nrh', NF_INT, 1, onedid, vid)
    message = 'creating nrh variable in '//filename
    CALL netcdf_handle_error(message,.false.,rcode)
    message = 'Number of relative humidities'
    rcode = NF_PUT_ATT(id, vid, 'description', NF_CHAR, LEN(TRIM(message)), TRIM(message))

    rcode = NF_DEF_VAR(id,'nwls', NF_INT, 1, onedid, vid)
    message = 'creating nwls variable in '//filename
    CALL netcdf_handle_error(message,.false.,rcode)
    message = 'Number of wavelengths'
    rcode = NF_PUT_ATT(id, vid, 'description', NF_CHAR, LEN(TRIM(message)), TRIM(message))

    pdim(1) = wdid; pdim(2)=mdid; pdim(3)=gdid; pdim(4)=rdid
    rcode = NF_DEF_VAR(id,'phfcn0', NF_DOUBLE, 4, pdim, vid)
    message = 'creating phfcn0 variable in '//filename
    CALL netcdf_handle_error(message,.false.,rcode)
    message = 'Phase moments'
    rcode = NF_PUT_ATT(id, vid, 'description', NF_CHAR, LEN(TRIM(message)), TRIM(message))

    qdim(1) = wdid; qdim(2)=rdid
    rcode = NF_DEF_VAR(id,'qext0', NF_DOUBLE, 2, qdim, vid)
    message = 'creating qext0 variable in '//filename
    CALL netcdf_handle_error(message,.false.,rcode)
    message = 'Extinction efficiency'
    rcode = NF_PUT_ATT(id, vid, 'description', NF_CHAR, LEN(TRIM(message)), TRIM(message))

    rcode = NF_DEF_VAR(id,'reff', NF_DOUBLE, 1, rdid, vid)
    message = 'creating reff variable in '//filename
    CALL netcdf_handle_error(message,.false.,rcode)
    message = 'Effective radius'
    rcode = NF_PUT_ATT(id, vid, 'description', NF_CHAR, LEN(TRIM(message)), TRIM(message))

!     rcode = NF_DEF_VAR(id,'reff0', NF_DOUBLE, 1, rdid, vid)
!     message = 'creating reff0 variable in '//filename
!     CALL netcdf_handle_error(message,.false.,rcode)
!     message = 'Effective radius0'
!     rcode = NF_PUT_ATT(id, vid, 'description', NF_CHAR, LEN(TRIM(message)), TRIM(message))

    rcode = NF_DEF_VAR(id,'rh', NF_DOUBLE, 1, rdid, vid)
    message = 'creating rh variable in '//filename
    CALL netcdf_handle_error(message,.false.,rcode)
    message = 'Relative humidity'
    rcode = NF_PUT_ATT(id, vid, 'description', NF_CHAR, LEN(TRIM(message)), TRIM(message))

    rcode = NF_DEF_VAR(id,'ssa0', NF_DOUBLE, 2, qdim, vid)
    message = 'creating ssa0 variable in '//filename
    CALL netcdf_handle_error(message,.false.,rcode)
    message = 'Single scattering albedo'
    rcode = NF_PUT_ATT(id, vid, 'description', NF_CHAR, LEN(TRIM(message)), TRIM(message))

    rcode = NF_DEF_VAR(id,'wls0', NF_DOUBLE, 1, wdid, vid)
    message = 'creating wls0 variable in '//filename
    CALL netcdf_handle_error(message,.false.,rcode)
    message = 'Wavelengths [nm]'
    rcode = NF_PUT_ATT(id, vid, 'description', NF_CHAR, LEN(TRIM(message)), TRIM(message))

    rcode = NF_ENDDEF(id)
    message = 'endding define mode in file '//filename
    CALL netcdf_handle_error(message,.false.,rcode)

    ! Write variables
    rcode = NF_INQ_VARID(id,'nmom',vid)
    rcode = NF_PUT_VAR(id,vid,nmom)
    message = 'writting nmom variable in file '//filename
    CALL netcdf_handle_error(message,.false.,rcode)

    rcode = NF_INQ_VARID(id,'nrh',vid)
    rcode = NF_PUT_VAR(id,vid,nrh)
    message = 'writting nrh variable in file '//filename
    CALL netcdf_handle_error(message,.false.,rcode)

    rcode = NF_INQ_VARID(id,'nwls',vid)
    rcode = NF_PUT_VAR(id,vid,nwls)
    message = 'writting nwls variable in file '//filename
    CALL netcdf_handle_error(message,.false.,rcode)

    rcode = NF_INQ_VARID(id,'phfcn0',vid)
    rcode = NF_PUT_VAR(id,vid,phfcn0)
    message = 'writting phfcn0 variable in file '//filename
    CALL netcdf_handle_error(message,.false.,rcode)

    rcode = NF_INQ_VARID(id,'qext0',vid)
    rcode = NF_PUT_VAR(id,vid,qext0)
    message = 'writting qext0 variable in file '//filename
    CALL netcdf_handle_error(message,.false.,rcode)

    rcode = NF_INQ_VARID(id,'reff',vid)
    rcode = NF_PUT_VAR(id,vid,reff)
    message = 'writting reff variable in file '//filename
    CALL netcdf_handle_error(message,.false.,rcode)

!     rcode = NF_INQ_VARID(id,'reff0',vid)
!     rcode = NF_PUT_VAR(id,vid,reff0)
!     message = 'writting reff0 variable in file '//filename
!     CALL netcdf_handle_error(message,.false.,rcode)

    rcode = NF_INQ_VARID(id,'rh',vid)
    rcode = NF_PUT_VAR(id,vid,rh)
    message = 'writting rh variable in file '//filename
    CALL netcdf_handle_error(message,.false.,rcode)

    rcode = NF_INQ_VARID(id,'ssa0',vid)
    rcode = NF_PUT_VAR(id,vid,ssa0)
    message = 'writting ssa0 variable in file '//filename
    CALL netcdf_handle_error(message,.false.,rcode)

    rcode = NF_INQ_VARID(id,'wls0',vid)
    rcode = NF_PUT_VAR(id,vid,wls0)
    message = 'writting wls0 variable in file '//filename
    CALL netcdf_handle_error(message,.false.,rcode)

    rcode = NF_CLOSE(id)
    message = '...closing file '//filename
    CALL netcdf_handle_error(message,.true.,rcode)

  END SUBROUTINE create_aerpro_nc

  SUBROUTINE netcdf_handle_error(string,verb,status)
    IMPLICIT NONE
    INCLUDE 'netcdf.inc'

    ! ---------------
    ! Input variables
    ! ---------------
    LOGICAL, INTENT(IN) :: verb
    INTEGER, INTENT(IN) :: status
    CHARACTER(*), INTENT(IN) :: string

    ! ---------------
    ! Local variables
    !----------------
    CHARACTER(NF_MAX_NAME) :: message

    ! Code starts here
    IF (status .NE. NF_NOERR) THEN
       message = 'Error '//TRIM(string)//': '//NF_STRERROR(status)
       WRITE(*,FMT='(A)') TRIM(ADJUSTL(message))
       STOP 1
    ELSE IF (verb) THEN
       message = TRIM(string)
       WRITE(*,FMT='(A)') TRIM(ADJUSTL(message))
    END IF
  END SUBROUTINE netcdf_handle_error

END MODULE splat_aerpro_ionc
